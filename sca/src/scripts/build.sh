#!/bin/sh

set -xe

SRCDIR=$(dirname $(readlink -f "$0"))
BUILDDIR=${TARGETDIR}/build
mkdir -p ${BUILDDIR}
cd ${BUILDDIR}

export RELSTR=beta_
export ISBETA=1
make -f ${SRCDIR}/Makefile all
