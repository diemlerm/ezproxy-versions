#ifndef __USRSIP_H__
#define __USRSIP_H__

#include "common.h"

enum FINDUSERRESULT FindUserSip(struct TRANSLATE *t,
                                char *sipHost,
                                struct USERINFO *uip,
                                char *file,
                                int f,
                                struct FILEREADLINEBUFFER *frb,
                                struct sockaddr_storage *pInterface);

#endif  // __USRSIP_H__
