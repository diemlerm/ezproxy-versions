/**
 * Microsoft Windows ODBC user authentication (win32 only)
 *
 * This module implements Microsoft Windows ODBC user authentication.  This feature
 * is limited to win32 builds of EZproxy since it is a Microsoft-Windows
 * specific functionality.
 *
 * This authentication method supports common conditions and actions.
 */

#define __UUFILE__ "usrodbc.c"

#include "common.h"

#ifdef WIN32
/* io.h, fcntl.h and share.h needed for _sopen in Win32 */
#include <io.h>
#include <fcntl.h>
#include <share.h>
#include <lm.h>
#include <sql.h>
#include <sqlext.h>

#include <sys/stat.h>

#include "obscure.h"

#include "usr.h"
#include "usrodbc.h"

#define MAXODBCVALS 10

struct BINDPARAM {
    struct BINDPARAM *next;
    char *val;
    SQLINTEGER bufferLen;
    SQLLEN StrLen_or_IndPtr;
};

struct ODBCCTX {
    SQLHDBC hdbc;
    char dsn[256];
    char user[256];
    char pass[256];
    struct BINDPARAM *bp;
    char *vals[MAXODBCVALS];
};

char *UsrODBCAuthGetValue(struct TRANSLATE *t,
                          struct USERINFO *uip,
                          void *context,
                          const char *var,
                          const char *index) {
    struct ODBCCTX *odbcCtx = (struct ODBCCTX *)context;
    char *val = NULL;
    int i;

    if (IsOneOrMoreDigits(var) && (i = atoi(var)) < MAXODBCVALS) {
        val = UUStrDup(odbcCtx->vals[i]);
    }

    return val;
}

void UsrODBCLogError(SQLHENV henv,
                     SQLHDBC hdbc,
                     SQLHSTMT hstmt,
                     struct USERINFO *uip,
                     char *where) {
    UCHAR sqlstate[10];
    UCHAR errmsg[SQL_MAX_MESSAGE_LENGTH];
    SDWORD nativeerr;
    SWORD actualmsglen;
    RETCODE rc = SQL_SUCCESS;

    for (;;) {
        rc = SQLError(henv, hdbc, hstmt, sqlstate, &nativeerr, errmsg, SQL_MAX_MESSAGE_LENGTH - 1,
                      &actualmsglen);

        if (rc == SQL_NO_DATA_FOUND)
            break;

        if (rc == SQL_ERROR) {
            UsrLog(uip, "SQLError failed!");
            return;
        }

        errmsg[actualmsglen] = 0;
        UsrLog(uip, "ODBC error in %s SQLSTATE = %s NATIVE ERROR = %d MSG=%s\n", where, sqlstate,
               nativeerr, errmsg);
    }
}

void UsrODBCFreeParameters(struct ODBCCTX *odbcCtx) {
    struct BINDPARAM *bp, *next;

    for (bp = odbcCtx->bp; bp; bp = next) {
        next = bp->next;
        FreeThenNull(bp->val);
        FreeThenNull(bp);
    }
    odbcCtx->bp = NULL;
}

int UsrODBCDSN(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct ODBCCTX *odbcCtx = (struct ODBCCTX *)context;

    StrCpy3(odbcCtx->dsn, arg, sizeof(odbcCtx->dsn));
    return 0;
}

int UsrODBCDBUser(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct ODBCCTX *odbcCtx = (struct ODBCCTX *)context;

    StrCpy3(odbcCtx->user, arg, sizeof(odbcCtx->user));
    return 0;
}

int UsrODBCDBPassword(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct ODBCCTX *odbcCtx = (struct ODBCCTX *)context;
    char *obscure, *unobscure;

    if (obscure = StartsIWith(arg, "-Obscure ")) {
        obscure = SkipWS(obscure);
        if (!(unobscure = UnobscureString(obscure))) {
            UsrLog(uip, "ODBC DBPassword -Obscure references an invalid password");
            uip->skipping = 1;
            return 1;
        }
        StrCpy3(odbcCtx->pass, unobscure, sizeof(odbcCtx->pass));
        FreeThenNull(unobscure);
        return 0;
    }

    StrCpy3(odbcCtx->pass, arg, sizeof(odbcCtx->pass));
    return 0;
}

int UsrODBCParameter(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct ODBCCTX *odbcCtx = (struct ODBCCTX *)context;
    struct BINDPARAM *bp, *last, *n;
    char *val = NULL;
    char ipBuffer[INET6_ADDRSTRLEN];
    BOOL expression = FALSE;
    char *p;

    if (p = StartsIWith(arg, "-expr ")) {
        val = ExpressionValue(t, uip, context, p);
        expression = TRUE;
    }

    if (stricmp(arg, "user") == 0) {
        val = uip->user;
    }

    if (strlen(arg) >= 4 && strnicmp(arg, "password", strlen(arg)) == 0) {
        val = uip->pass;
    }

    if (stricmp(arg, "ip") == 0) {
        ipToStr(&t->sin_addr, ipBuffer, sizeof ipBuffer);
        val = ipBuffer;
    }

    if (stricmp(arg, "reset") == 0) {
        UsrODBCFreeParameters(odbcCtx);
        return 0;
    }

    if (val == NULL) {
        UsrLog(uip, "Unrecognized ODBC Parameter: %s", arg);
        return 1;
    }

    for (last = NULL, bp = odbcCtx->bp; bp; last = bp, bp = bp->next)
        ;

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;

    if (last == NULL)
        odbcCtx->bp = n;
    else
        last->next = n;

    if (expression)
        n->val = val;
    else {
        if (*val == 0) {
            if (!(n->val = calloc(2, 1)))
                PANIC;
        } else {
            if (!(n->val = strdup(val)))
                PANIC;
        }
    }

    n->next = NULL;

    return 0;
}

int UsrODBCSQL(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct ODBCCTX *odbcCtx = (struct ODBCCTX *)context;
    static SQLHENV henv = NULL;
    SQLHSTMT hstmt = NULL;
    SQLRETURN retcode;
    char action[16];
    SQLLEN actionLen;
    char group[256];
    SQLLEN groupLen;
    struct BINDPARAM *bp;
    SQLUSMALLINT bpi;
    BOOL anyGroup = 0;
    BOOL firstGroup = 1;
    int count;

    if (henv == NULL) {
        UUAcquireMutex(&mActive);

        if (henv == NULL) {
            /*Allocate environment handle */
            if (!SQL_SUCCEEDED(retcode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv))) {
                UsrLog(uip, "SQLAllocHandle failed %d", GetLastError());
                UUReleaseMutex(&mActive);
                goto Finished;
            }
            retcode = SQLSetEnvAttr(henv, SQL_ATTR_ODBC_VERSION, (void *)SQL_OV_ODBC3, 0);
        }
        UUReleaseMutex(&mActive);
    }

    if (odbcCtx->hdbc == NULL) {
        if (!SQL_SUCCEEDED(retcode = SQLAllocHandle(SQL_HANDLE_DBC, henv, &odbcCtx->hdbc))) {
            UsrLog(uip, "SQLAllocHandle 2 failed %d", GetLastError());
            goto Finished;
        }

        if (!SQL_SUCCEEDED(
                retcode = SQLConnect(odbcCtx->hdbc, (SQLCHAR *)odbcCtx->dsn, SQL_NTS,
                                     (SQLCHAR *)odbcCtx->user ? odbcCtx->user : NULL, SQL_NTS,
                                     (SQLCHAR *)odbcCtx->pass ? odbcCtx->pass : NULL, SQL_NTS))) {
            UsrODBCLogError(henv, odbcCtx->hdbc, SQL_NULL_HSTMT, uip, "SQLConnect");
            goto Finished;
        }
    }

    if (!SQL_SUCCEEDED(retcode = SQLAllocHandle(SQL_HANDLE_STMT, odbcCtx->hdbc, &hstmt))) {
        UsrLog(uip, "SQLAllocHandle 3 failed %d", GetLastError());
        goto Finished;
    }

    for (bp = odbcCtx->bp, bpi = 1; bp; bp = bp->next, bpi++) {
        bp->bufferLen = strlen(bp->val);
        if (bp->bufferLen == 0) {
            bp->bufferLen = 1;
            bp->StrLen_or_IndPtr = SQL_NULL_DATA;
        } else {
            bp->StrLen_or_IndPtr = bp->bufferLen;
        }
        retcode = SQLBindParameter(hstmt, bpi, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR,
                                   (SQLUINTEGER)bp->bufferLen, 0, bp->val, bp->bufferLen,
                                   &bp->StrLen_or_IndPtr);
    }

    if (!SQL_SUCCEEDED(SQLExecDirect(hstmt, arg, SQL_NTS))) {
        UsrODBCLogError(henv, odbcCtx->hdbc, hstmt, uip, "SQLExecDirect");
        goto Finished;
    }

    retcode = SQLBindCol(hstmt, 1, SQL_C_CHAR, action, sizeof(action) - 1, &actionLen);
    retcode = SQLBindCol(hstmt, 2, SQL_C_CHAR, group, sizeof(group) - 1, &groupLen);

    groupLen = SQL_NULL_DATA;
    count = 0;

    while (SQL_SUCCEEDED(retcode = SQLFetch(hstmt))) {
        count++;
        if (actionLen == SQL_NULL_DATA) {
            if (uip->debug)
                UsrLog(uip, "SQL returned NULL");
            continue;
        }
        if (actionLen >= 0) {
            action[actionLen] = 0;
        }

        if (groupLen == SQL_NULL_DATA)
            group[0] = 0;
        else if (groupLen >= 0) {
            group[groupLen] = 0;
            Trim(group, TRIM_LEAD | TRIM_TRAIL);
        }

        if (uip->debug) {
            UsrLog(uip, "SQL returned *%s*%s*", action, group);
        }

        if (stricmp(action, "deny") == 0) {
            UsrDeny(t, uip, context, group);
            break;
        }

        if (stricmp(action, "expired") == 0) {
            uip->result = resultExpired;
            continue;
        }

        if (stricmp(action, "allow") != 0)
            continue;

        if (uip->result == resultExpired)
            continue;

        uip->result = resultValid;

        if (group[0]) {
            if (firstGroup) {
                GroupMaskClear(uip->gm);
                firstGroup = 0;
            }
            MakeGroupMask(uip->gm, group, 0, '+');
        }
    }
    if (uip->debug)
        UsrLog(uip, "SQL returned %d row%s", count, SIfNotOne(count));

Finished:

    if (hstmt) {
        SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
        hstmt = NULL;
    }

    return 0;
}

enum FINDUSERRESULT FindUserODBC(struct TRANSLATE *t,
                                 struct USERINFO *uip,
                                 char *file,
                                 int f,
                                 struct FILEREADLINEBUFFER *frb,
                                 struct sockaddr_storage *pInterface) {
    struct USRDIRECTIVE udc[] = {
  //      { "",           UsrODBCInit,           0 },
        {"DSN",        UsrODBCDSN,        0},
        {"DBUser",     UsrODBCDBUser,     0},
        {"DBPassword", UsrODBCDBPassword, 0},
        {"Parameter",  UsrODBCParameter,  0},
        {"SQL",        UsrODBCSQL,        0},
        {NULL,         NULL,              0}
    };
    struct ODBCCTX odbcCtx;
    enum FINDUSERRESULT result;
    char saveContinuationChar;

    memset(&odbcCtx, 0, sizeof(odbcCtx));

    saveContinuationChar = frb->continuationChar;
    frb->continuationChar = '\\';
    uip->flags = 0;
    uip->authGetValue = UsrODBCAuthGetValue;
    result = UsrHandler(t, "ODBC", udc, &odbcCtx, uip, file, f, frb, pInterface, NULL);
    frb->continuationChar = saveContinuationChar;

    if (odbcCtx.hdbc) {
        SQLDisconnect(odbcCtx.hdbc);
        SQLFreeHandle(SQL_HANDLE_DBC, odbcCtx.hdbc);
        odbcCtx.hdbc = NULL;
    }

    UsrODBCFreeParameters(&odbcCtx);

    return result;
}
#endif
