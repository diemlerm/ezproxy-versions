#ifndef __USRLDAP2_H__
#define __USRLDAP2_H__

#define LDAP_DEPRECATED 1
#include "common.h"
#include <ldap.h>

enum STARTTLSTYPE { STDISABLED, STENABLED, STLDAPS };

/* Prototypes for usrldap2.c */

char *LDAPFilter(char *baseFilter, char *attr, char *user);

int FindUserLDAP2(struct TRANSLATE *t,
                  struct USERINFO *uip,
                  char *file,
                  int f,
                  struct FILEREADLINEBUFFER *frb,
                  struct sockaddr_storage *pInterface);

void FindUserLDAP2SetSSLCipherSuite(LDAP *ld, SSL *ssl, SSL_CTX *ctx, char *sslCipherSuite);

#endif  // __USRLDAP2_H__
