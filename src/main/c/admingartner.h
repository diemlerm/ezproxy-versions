#ifndef __ADMINGARTNER_H__
#define __ADMINGARTNER_H__

#include "common.h"

void AdminGartner(struct TRANSLATE *t, char *query, char *post);
void AdminGartnerKey(struct TRANSLATE *t, char *query, char *post);
void AdminGartnerLoadKey(struct GARTNERCONFIG *gc);

#endif  // __ADMINGARTNER_H__
