#define __UUFILE__ "restart.c"

#include "common.h"
#ifdef WIN32
#include "service.h"
#else
#include "unix.h"
#endif

BOOL Patricide(int delay) {
    time_t start, now;
    BOOL stopped = 0;

    UUtime(&start);
    for (;;) {
        UUtime(&now);
        if (stopped) {
            if (now - start > 30)
                return FALSE;
        } else if (now - start >= delay) {
            if (ProcessKill(guardianPid) || ProcessKill(chargePid))
                return FALSE;
            stopped = 1;
            UUtime(&start);
        }
        if ((guardianPid && ProcessAlive(guardianPid)) || (chargePid && ProcessAlive(chargePid))) {
            SubSleep(1, 0);
            continue;
        }
        return TRUE;
    }
}

/* The following RequestAudience and ReleaseAudience are part of a locking mechanism so that only
 * one client has access to the "guardian" shared memory object at once. */
BOOL RequestAudience(BOOL abortIfDown) {
    time_t start, now;
    int me;

    if (guardian == NULL)
        return FALSE;

    me = getpid();

    UUtime(&start);
    guardian->clientRequestPid = me;

    for (;;) {
        if (guardian->serverGrantPid == me)
            return TRUE;

        UUtime(&now);
        if (ProcessAlive(guardianPid) == 0 || now - start > 30) {
            if (abortIfDown) {
                printf("Unable to communicate\n");
                exit(1);
            }
            return FALSE;
        }

        if (guardian->clientRequestPid != me)
            guardian->clientRequestPid = me;
        SubSleep(0, 500);
    }
}

void ReleaseAudience(void) {
    if (guardian && guardian->serverGrantPid == getpid()) {
        if (guardian->clientRequestPid == getpid())
            guardian->clientRequestPid = 0;
        guardian->serverGrantPid = 0;
    }
}

void EZproxyControl(int option, char *cmd, char *arg) {
    int i;
    int delay;
    int result;
    char *errorMsg;
#ifndef WIN32
    pid_t child;
    int status;
#endif
    BOOL guardianAlive = 0;

    LoadIPC(&guardianPid, &chargePid, &guardianSharedMemoryID, &result);

    if (result && result != ENOENT) {
        printf("Unable to open %s: %s", IPCFILE, errorMsg = ErrorMessage(GetLastError()));
        FreeThenNull(errorMsg);
        exit(1);
    }

    if (guardianAlive = ProcessAlive(guardianPid))
        MapGuardian(1);

    /* stop */
    if (option == 0) {
        if (RequestAudience(FALSE)) {
            guardian->chargeStatus = chargeStopping;
            guardian->chargeStop = 1;
            ReleaseAudience();
            delay = 15;
        } else {
            delay = 0;
        }
        if (!Patricide(delay)) {
            printf("Unable to terminate existing EZproxy\n");
            exit(1);
        }
        exit(0);
    }

    /* start */
    if (option == 1 || option == 2) {
        if (RequestAudience(FALSE)) {
            if (option == 2) {
                guardian->chargeStop = 1;
                printf(EZPROXYNAMEPROPER " restart requested.\n");

            } else {
                printf(EZPROXYNAMEPROPER " is already running.  No action taken.\n");
            }
            ReleaseAudience();
            exit(0);
        }
        if (!Patricide(0)) {
            printf("Unable to control the existing " EZPROXYNAMEPROPER ", aborting\n");
            exit(1);
        }
#ifdef WIN32
        if (InstalledAsService(NULL)) {
            StartMyService();
            exit(0);
        }

        return;
#else

        /* Detach from the the invoking process and its terminal (a double-fork avoids creating a
         * zombie) */
        child = fork();
        if (child == 0) {
            child = fork();
            if (child == 0) {
                ttyDetach = TRUE;
                setsid();
                return;
            }
            exit(0);
        }
        waitpid(child, &status, 0); /* avoids creating a zombie */
        exit(0);
#endif
    }

    /* log */
    if (option == 3) {
        if (guardian == NULL) {
            if (guardianAlive)
                fprintf(stderr, "This account is not authorized to control EZproxy\n");
            else
                fprintf(stderr, EZPROXYNAMEPROPER " not running or unable to communicate\n");
            exit(1);
        }
        RequestAudience(TRUE);
        if (arg == NULL)
            arg = "";
        StrCpy3(guardian->renameLog, arg, sizeof(guardian->renameLog));
        guardian->dontReopenAccessFile = 0;
        for (i = 0; guardian->dontReopenAccessFile == 0; i++) {
            if (i >= 15) {
                ReleaseAudience();
                fprintf(stderr, "Log file rename timed-out\n");
                exit(1);
            }
            SubSleep(1, 0);
        }
        result = guardian->renameStatus;
        ReleaseAudience();
        if (result != 0) {
            if (result == EEXIST)
                printf("File already exists\n");
            else
                printf("Log file rename failed: %d\n", result);
            exit(1);
        }
        exit(0);
    }

    /* status */
    if (option == 4) {
        if (guardianAlive && guardian == NULL) {
            fprintf(stderr, "This account is not authorized to control EZproxy\n");
            exit(1);
        }
        if (RequestAudience(FALSE)) {
            ReleaseAudience();
            printf("EZproxy is running: guardian pid %d, server pid %d\n", guardian->gpid,
                   guardian->cpid);
        } else {
            printf("EZproxy is not running\n");
        }
        exit(0);
    }

    /* kill session */
    if (option == 5) {
        if (arg == NULL) {
            printf("Session must be specified\n");
            exit(1);
        }

        if (!RequestAudience(FALSE)) {
            printf("EZproxy is not running\n");
            exit(2);
        }
        StrCpy3(guardian->arg, arg, sizeof(guardian->arg));
        guardian->killSession = 1;
        for (i = 0; guardian->killSession == 1; i++) {
            if (i >= 15) {
                fprintf(stderr, "Session kill timed-out\n");
                ReleaseAudience();
                exit(1);
            }
            SubSleep(1, 0);
        }

        result = guardian->killSession;
        ReleaseAudience();

        switch (result) {
        case 0:
            printf("Session %s terminated\n", arg);
            break;

        case 3:
            printf("Session %s does not exist\n", arg);
            break;

        case 4:
            printf("Session %s is an anonymous session; termination not allowed\n", arg);
            break;

        default:
            printf("Session termination status %d ???\n", guardian->killSession);
        }

        exit(result);
    }

    /* license change */
    if (option == 6) {
        if (guardianAlive && guardian == NULL) {
            fprintf(stderr, "This account is not authorized to control EZproxy\n");
            exit(1);
        }
        if (RequestAudience(FALSE)) {
            guardian->licenseChange = 1;
            ReleaseAudience();
        }
        return;
    }

    fprintf(stderr, "How did we get here?");
    exit(1);
}
