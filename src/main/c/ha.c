#define __UUFILE__ "ha.c"

#include "common.h"
#include <stdarg.h>

static enum HASTATE { HAIDLE, HAACQUIRING, HAACQUIRED, HAVOTING, HADENIED } HAState, lastState;

SOCKET haSocket = INVALID_SOCKET;
int haPeerHello = 3;
int haPeerStale = 8;
int haPeerDown = 20;
int haMsgid = 0;

int StartHASocket(void) {
    int rc;
    int one = 1;

    haSocket = MoveFileDescriptorHigh(socket(AF_INET, SOCK_DGRAM, 0));
    if (debugLevel > 5)
        Log("%" SOCKET_FORMAT " Open socket, errno=%d", haSocket,
            (haSocket == INVALID_SOCKET) ? errno : 0);
    if (haSocket == INVALID_SOCKET) {
        goto Error;
    }

    if (setsockopt(haSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&one, sizeof(one)))
        goto Error;

    if (bind(haSocket, (struct sockaddr *)&haPeers->haPeerAddress,
             get_size(&haPeers->haPeerAddress)))
        goto Error;

    return 0;

Error:
    rc = socket_errno;
    if (haSocket != INVALID_SOCKET) {
        closesocket(haSocket);
        haSocket = INVALID_SOCKET;
    }
    return rc;
}

void HABad(char *rtn, struct HAPEER *hap, struct HAPEERMSG *msg) {
    int len;

    if (debugLevel == 0)
        return;

    if (msg->datalen > 10)
        len = 10;
    else
        len = msg->datalen;

    msg->data[len] = 0;
    Log("Invalid HA msg from %s:%d of %s: %c %c %d: %s", hap->haPeerName,
        get_port(&hap->haPeerAddress), rtn, msg->type, msg->cmd, msg->msgid, msg->data);
}

int HASendRawMsg(struct HAPEER *hap, struct HAPEERMSG *msg) {
    int msglen;
    int rc;
    short datalen;

    datalen = msg->datalen;
    if (datalen > MAXCLUSTERDATA)
        datalen = MAXCLUSTERDATA;
    msglen = HAPEERMSGHDRSIZE + datalen;
    msg->datalen = htons(datalen);
    msg->version = htons(HAVERSION);
    if (msg->msgid == 0)
        msg->msgid = htonl(++haMsgid);
    rc = sendto(haSocket, (char *)msg, msglen, 0, (struct sockaddr *)&hap->haPeerAddress,
                sizeof(hap->haPeerAddress));
    if (rc == SOCKET_ERROR) {
        Log("ClusterSendRawMsg sendto failed: %d", socket_errno);
    }
    msg->datalen = datalen;
    return rc;
}

void HASendMsg(struct HAPEER *hap, struct HAPEERMSG *msg) {
    /*  if (msg->cmd != 'T')
            Log("Send msg to %s: %c %c %c\n", hap->haPeerName, msg->type, msg->cmd, msg->data[0]);
     */
    HASendRawMsg(hap, msg);
}

void HASendMsgAll(struct HAPEERMSG *msg) {
    struct HAPEER *hap;

    for (hap = haPeers->next; hap; hap = hap->next)
        HASendMsg(hap, msg);
}

void HASendSimpleMsg(struct HAPEER *hap, char *simple, int msgid) {
    struct HAPEERMSG msg;
    short len;

    msg.type = *simple;
    msg.cmd = *(simple + 1);
    msg.msgid = msgid;
    len = strlen(simple) + 1;
    if (len <= 2)
        msg.datalen = 0;
    else {
        len -= 2;
        if (len > MAXCLUSTERDATA)
            len = MAXCLUSTERDATA;
        msg.datalen = len;
        StrCpy3(msg.data, simple + 2, len);
    }
    if (hap == NULL)
        HASendMsgAll(&msg);
    else
        HASendMsg(hap, &msg);
}

struct HAPEER *FindHAPeerByAddress(struct sockaddr_storage *addr) {
    struct HAPEER *hap;

    for (hap = haPeers; hap; hap = hap->next)
        if (is_ip_equal(&hap->haPeerAddress, addr) &&
            get_port(addr) == get_port(&hap->haPeerAddress))
            return hap;
    return NULL;
}

void HAInitMsg(struct HAPEERMSG *msg, char type, char cmd, int msgid) {
    memset(msg, 0, sizeof(*msg));
    msg->type = type;
    msg->cmd = cmd;
    msg->msgid = msgid;
}

void HAHello(struct HAPEER *hap) {
    struct HAPEERMSG msg;

    HAInitMsg(&msg, 'I', 'T', 0);
    msg.datalen = 2;
    msg.data[0] = (haPeers->haPeerStatus == HAPEERUP ? 'U' : 'B');
    msg.data[1] = 0;
    HASendMsg(hap, &msg);
}

void HAHellos(time_t now) {
    static time_t nextHello = 0;
    struct HAPEER *hap;

    if (nextHello > now)
        return;
    for (hap = haPeers->next; hap; hap = hap->next)
        HAHello(hap);
    nextHello = now + haPeerHello;
}

void HACheckPeers(time_t now) {
    static time_t startup = 0;
    time_t stale, down;
    struct HAPEER *hap;
    enum HAPEERSTATUS saveStatus;
    struct HAPEER *up = NULL;
    BOOL unknowns;
    static BOOL first = 1;

    if (startup == 0)
        startup = now;
    stale = now - haPeerStale;
    down = now - haPeerDown;
    unknowns = 0;

    for (hap = haPeers->next; hap; hap = hap->next) {
        saveStatus = hap->haPeerStatus;
        switch (hap->haPeerStatus) {
        case HAPEERDOWN:
            unknowns++;
            break;

        case HAPEERSTARTING:
            if (hap->lastHeardFrom < down) {
                hap->haPeerStatus = HAPEERDOWN;
                break;
            }
            break;

        case HAPEERUP:
            if (hap->lastHeardFrom < stale) {
                hap->haPeerStatus = HAPEERSTALE;
                break;
            }
            if (haPeers->haPeerStatus < HAPEERUP)
                HASendSimpleMsg(hap, "RL", 0);
            if (up == NULL) {
                up = hap;
            }
            break;

        case HAPEERSTALE:
            unknowns++;
            if (hap->lastHeardFrom < down)
                hap->haPeerStatus = HAPEERDOWN;
            break;

        default:
            break;
        }

        if (saveStatus != hap->haPeerStatus)
            Log("%s:%d changed to %s\n", hap->haPeerName, get_port(&hap->haPeerAddress),
                haPeerStatusName[hap->haPeerStatus]);

        if (hap->smsg.type == 'A' && hap->sAck == HAWAITING)
            if (hap->haPeerStatus >= HAPEERUP)
                HASendMsg(hap, &hap->smsg);
            else
                hap->sAck = HADIED;
    }

    if (haPeers->haPeerStatus != HAPEERSTARTING)
        return;
    if (up != NULL) {
        /*      peers->haPeerStatus = PEERUP; */
        HAHellos(now);
        /*      ClusterSendSimpleMsg(up, "AL", 0); */
        return;
    }
    if (unknowns > 0 && startup + 10 > now)
        return;
    if (first) {
        Log("I must now propose to become the master\n");
        haPeers->haPeerStatus = HAPEERUP;
        HAHellos(now);
        first = 0;
    }
}

void DoHA(void *v) {
    int rc;
    struct sockaddr_storage sf;
    socklen_t_M sfl;
    struct HAPEERMSG msg;
    int msglen;
#ifdef USEPOLL
    struct pollfd pfds;
#else
    fd_set sfds;
    struct timeval tv;
#endif
    struct HAPEER *hap;
    time_t now;
    char ipBuffer[INET6_ADDRSTRLEN];

    if (rc = StartHASocket()) {
        Log("Unable to create HA socket: %d", rc);
        exit(1);
    }
    haPeers->haPeerStatus = HAPEERSTARTING;
    Log("HA coordination starting\n");
    SocketNonBlocking(haSocket);
    for (;;) {
        ticksCluster++;
        /*
        if (lastHaveMutex != HAHaveMutex) {
            Log(clusterHaveMutex ? "+" : "-");
            lastHaveMutex = HAHaveMutex;
        }
        if (lastState != HAState) {
            Log("**%d%d ", HAState, HAHaveMutex);
            lastState = HAState;
        }
        */
        UUtime(&now);
        HAHellos(now);
        HACheckPeers(now);

#ifdef USEPOLL
        pfds.fd = haSocket;
        pfds.events = POLLIN;
        rc = poll(&pfds, 1, 1000);
        if (rc <= 0)
            continue;
#else
        FD_ZERO(&sfds);
        FD_SET(haSocket, &sfds);
        tv.tv_sec = 1;
        tv.tv_usec = 0;
        rc = select(haSocket + 1, &sfds, NULL, NULL, &tv);
        if (rc <= 0)
            continue;
#endif

        init_storage(&sf, UUDEFAULT_FAMILY, TRUE, 0);
        sfl = sizeof(sf);
        msglen = recvfrom(haSocket, (char *)&msg, (size_t_M)sizeof(msg), 0, (struct sockaddr *)&sf,
                          &sfl);
        hap = FindHAPeerByAddress(&sf);
        if (msglen < 0) {
            /* Linux indicates that there is data on the socket, but then returns this error? */
            /*
            #ifdef ECONNREFUSED
                        if (socket_errno != ECONNREFUSED)
            #endif
            */
            if (hap)
                Log("Error from %s:%d ", hap->haPeerName, get_port(&hap->haPeerAddress));
            Log("Error receiving: %d\n", socket_errno);
            continue;
        }
        hap = FindHAPeerByAddress(&sf);
        /* If this message is "from me", it is probably telling me to get on with checking status */
        if (hap == haPeers)
            continue;
        if (hap == NULL || msglen < HAPEERMSGHDRSIZE ||
            msglen != HAPEERMSGHDRSIZE + (msg.datalen = ntohs(msg.datalen))) {
            Log("Invalid HA message from %s on port %d", ipToStr(&sf, ipBuffer, sizeof ipBuffer),
                get_port(&sf));
            continue;
        }
        msg.version = ntohs(msg.version);
        if (msg.version > HAVERSION) {
            Log("I use %04x high availability coordination, %s:%d uses %04x, packet ignored",
                HAVERSION, msg.version, hap->haPeerName, get_port(&hap->haPeerAddress));
            continue;
        }
        msg.msgid = msg.msgid;
        UUtime(&hap->lastHeardFrom);
        msg.data[msg.datalen] = 0;
        /*      if (0 && msg.cmd != 'T') // Filter out all the hellos */
        Log("%s:%d sends type %c command %c id %d len %d data %s\n", hap->haPeerName,
            get_port(&hap->haPeerAddress), msg.type, msg.cmd, ntohl(msg.msgid), msg.datalen,
            msg.data);

        if (msg.msgid == hap->smsg.msgid && msg.type != 'A' && msg.type != 'I')
            hap->sAck = HAACKED;

        if (msg.cmd == 'T') {
            UUtime(&hap->lastHeardFrom);
            hap->haPeerStatus = HAPEERUP;
        }
    }
}
