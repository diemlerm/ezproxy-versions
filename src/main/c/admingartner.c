#define __UUFILE__ "admingartner.c"

#include "common.h"
#include "uustring.h"
#include "usertoken.h"

#include "admingartner.h"

static void AdminGartnerKeyFile(struct GARTNERCONFIG *gc, char *fn) {
    sprintf(fn, "%s%s.pem", GARTNERDIR, gc->shortName);
}

static void AdminGartnerKeyURL(struct UUSOCKET *s,
                               struct GARTNERCONFIG *gc,
                               char *action,
                               char *desc,
                               BOOL confirm) {
    WriteLine(s, "<a href='/gartnerkey?name=%s&action=%s'", gc->shortName, action);
    /*
    if (confirm)
        WriteLine(s, " onclick=\"return confirm('This may disrupt your service with Gartner. Do you
    really want to %s this key?')\"", action);
    */
    WriteLine(s, ">%s</a> ", desc);
}

static void AdminGartnerKeySubheader(struct TRANSLATE *t) {
    AdminBaseHeader(t, 0, "View Gartner Key", " | <a href='/gartnerkey'>Manage Gartner Keys</a>");
}

void AdminGartnerKey(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    DSA *dsa = NULL;
    FILE *f = NULL;
    struct GARTNERCONFIG *gc;
    char fn[MAX_PATH];
    const int AGNAME = 0;
    const int AGACTION = 1;
    const int AGCONFIRM = 2;
    struct FORMFIELD ffs[] = {
        {"name",    NULL, 0, 0, 0},
        {"action",  NULL, 0, 0, 0},
        {"confirm", NULL, 0, 0, 0},
        {NULL,      NULL, 0, 0, 0}
    };
    char *action;
    BIO *mem = NULL;
    BOOL download = 0;
    char *name;
    BOOL any = 0;
    char *confirm;
    BOOL showGroups;

    FindFormFields(query, post, ffs);

    action = ffs[AGACTION].value;
    if (action == NULL)
        action = "";
    else
        AllLower(action);

    confirm = ffs[AGCONFIRM].value;
    if (confirm == NULL)
        confirm = "";

    if (name = StartsIWith(t->urlCopy, "/gartnerkey/")) {
        char *period;
        size_t len;
        if (period = strchr(name, '.'))
            len = period - name;
        else
            len = strlen(name);
        for (gc = gartnerConfigs; gc; gc = gc->next) {
            if (strnicmp(name, gc->shortName, len) == 0 && len == strlen(gc->shortName) &&
                gc->secret == NULL) {
                download = 1;
                goto View;
            }
        }
    }

    if (ffs[AGNAME].value == NULL)
        gc = NULL;
    else {
        for (gc = gartnerConfigs; gc; gc = gc->next) {
            if (stricmp(ffs[AGNAME].value, gc->shortName) == 0)
                if (gc->secret == NULL)
                    break;
        }
    }

    if (gc) {
        if (stricmp(action, "create") == 0) {
            goto Create;
        }
        if (stricmp(action, "replace") == 0) {
            if (stricmp(action, confirm) == 0)
                goto Create;
            goto Confirm;
        }
        if (stricmp(action, "view") == 0 && gc->dsa) {
            goto View;
        }
        if (stricmp(action, "delete") == 0 && gc->dsa) {
            if (stricmp(action, confirm) == 0)
                goto Delete;
            goto Confirm;
        }
    }

    AdminHeader(t, 0, "Manage Gartner Keys");

    showGroups = GroupMaskCardinality(gartnerGm) != 1 || GroupMaskIsSet(gartnerGm, 0) == 0;

    UUSendZ(s, "<table class='bordered-table'>\n");

    UUSendZ(s, "<tr><th scope='col'>Name</th><th scope='col'>Actions</th>");
    if (showGroups)
        UUSendZ(s, "<th scope='col'>Authorized groups</th>");
    UUSendZ(s, "</tr>");

    for (gc = gartnerConfigs; gc; gc = gc->next) {
        WriteLine(s, "<tr><td>%s&nbsp;</td><td>", gc->shortName);
        if (gc->secret) {
            UUSendZ(s, "Uses MD5 secret");
        } else {
            if (gc->dsa) {
                WriteLine(s, "<a href='/gartnerkey/%s.pem'>Download</a> ", gc->shortName);
                AdminGartnerKeyURL(s, gc, "view", "View", 0);
                AdminGartnerKeyURL(s, gc, "replace", "Replace", 1);
                AdminGartnerKeyURL(s, gc, "delete", "Delete", 1);
                any = 1;
            } else {
                AdminGartnerKeyURL(s, gc, "create", "Create", 0);
            }
        }
        if (showGroups) {
            UUSendZ(s, "</td><td>");
            GroupMaskWrite(gc->gm, s, NULL);
        }
        UUSendZ(s, "</td></tr>\n");
    }

    UUSendZ(s, "</table>\n");
    goto Finished;

Confirm:
    AdminGartnerKeySubheader(t);

    UUSendZ(s, "<form action='gartnerkey' method='post'>\n");
    WriteLine(s, "<input type='hidden' name='name' value='%s'>\n", gc->shortName);
    WriteLine(s, "<input type='hidden' name='action' value='%s'>\n", action);
    UUSendZ(s,
            "<p>This action may disrupt your  access to Gartner until you create a new key, submit "
            "it, and wait while the key is applied to your account.</p>\n");
    WriteLine(
        s,
        "<p><label for='confirm'>To %s the %s key, type %s </label><input type='text' "
        "name='confirm' id='confirm' size='%d'> and click <input type='submit' value='here'>.",
        action, gc->shortName, action, strlen(action));
    UUSendZ(s, "</p></form>\n");
    goto Finished;

Delete:
    AdminGartnerKeyFile(gc, fn);
    unlink(fn);
    dsa = gc->dsa;
    gc->dsa = NULL;
    DSA_free(dsa);
    dsa = NULL;
    goto Feedback;

Create:
    UUmkdir(GARTNERDIR);

    AdminGartnerKeyFile(gc, fn);

    f = fopen(fn, "w");

    if (f == NULL) {
        Log("Unable to create %s: %d", fn, errno);
        goto Finished;
    }

    dsa = DSA_generate_parameters(512, NULL, 0, NULL, NULL, NULL, NULL);
    if (dsa == NULL) {
        LogSsl("Couldn't generate DSA keypair");
        goto Finished;
    }

    if (DSA_generate_key(dsa) == 0) {
        LogSsl("DSA_generate_key failed");
        goto Finished;
    }

    /*
    if (!PEM_write_DSA_PUBKEY(f, dsa)) {
        LogSsl("PEM_write_DSA_PUBKEY failed");
        goto Finished;
    }
    */

    if (!PEM_write_DSAPrivateKey(f, dsa, NULL, NULL, 0, NULL, NULL)) {
        LogSsl("PEM_write_DSAPrivateKey failed");
        goto Finished;
    }

    gc->dsa = dsa;

    goto Feedback;

View:
    if (download) {
        HTTPCode(t, 200, 0);
        WriteLine(s, "Content-Disposition: attachment; filename='%s'\r\n", name);
        HTTPContentType(t, "application/octet-stream", 1);
    } else {
        AdminGartnerKeySubheader(t);
    }

    if (mem = BIO_new(BIO_s_mem())) {
        char line[128];
        PEM_write_bio_DSA_PUBKEY(mem, gc->dsa);
        if (!download)
            WriteLine(s, "<p>Gartner key for %s</p>\n<pre>\n", gc->shortName);
        while (BIO_gets(mem, line, sizeof(line)) > 0) {
            Trim(line, TRIM_TRAIL);
            if (download)
                WriteLine(s, "%s", line);
            else
                SendHTMLEncoded(s, line);
            UUSendCRLF(s);
        }
        if (!download)
            UUSendZ(s, "</pre>\n");
    }
    goto Finished;

Feedback:
    AdminGartnerKeySubheader(t);

    WriteLine(s, "<p>Gartner key %s %sd.</p>\n", gc->shortName, action);
    goto Finished;

Finished:
    if (f) {
        fclose(f);
        f = NULL;
    }

    if (mem) {
        BIO_free(mem);
        mem = NULL;
    }

    if (download == 0)
        AdminFooter(t, 0);
}

void AdminGartnerLoadKey(struct GARTNERCONFIG *gc) {
    FILE *f = NULL;
    char fn[MAX_PATH];

    if (gc == NULL)
        return;

    AdminGartnerKeyFile(gc, fn);

    if (gc->dsa) {
        DSA_free(gc->dsa);
        gc->dsa = NULL;
    }

    if (f = fopen(fn, "r")) {
        gc->dsa = PEM_read_DSAPrivateKey(f, NULL, NULL, NULL);
        if (gc->dsa == NULL) {
            Log("Gartner private key load failed for %s", fn);
            LogSsl("Gartner PEM_read_DSAPrivateKey failed");
        }
    }

    if (f) {
        fclose(f);
        f = NULL;
    }
}

void AdminGartner(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    unsigned char *sigret = NULL;
    unsigned int siglen = 0;
    char *msg64 = NULL;
    char *sig64 = NULL;
    char *userToken = NULL;
    EVP_MD_CTX *mdctx = NULL;
    unsigned char md_value[EVP_MAX_MD_SIZE];
    unsigned int md_len;
    struct SESSION *e = t->session;
    struct GARTNERCONFIG *gc;
    time_t now;
    GROUPMASK requiredGm = NULL;

    if (gartnerConfigs == NULL || e == NULL) {
        HTTPCode(t, 404, 1);
        return;
    }

    for (gc = gartnerConfigs;; gc = gc->next) {
        if (gc == NULL) {
            AdminLogup(t, TRUE, NULL, NULL, requiredGm, FALSE);
            goto Finished;
        }
        if (GroupMaskOverlap(e->gm, gc->gm))
            break;
        if (requiredGm)
            GroupMaskOr(requiredGm, gc->gm);
        else
            requiredGm = GroupMaskCopy(NULL, gc->gm);
    }

    if (e->autoLoginBy != autoLoginByNone || e->logUserBrief == NULL) {
        AdminLogup(t, TRUE, NULL, NULL, requiredGm, FALSE);
    }

    if (gc->secret != NULL) {
        char *secretStart;

        userToken = UserToken2("g", (t->session)->logUserBrief, 1);

        if (userToken == NULL) {
            AdminNoToken(t);
            goto Finished;
        }

        UUtime(&now);
        /* Technically, uid should be URL safe, but why take chances */
        strcpy(t->buffer, "uid=");
        AddEncodedField(t->buffer, userToken, NULL);
        sprintf(strchr(t->buffer, 0), "&dt=%" PRIdMAX "&", (intmax_t)now);

        secretStart = strchr(t->buffer, 0);
        strcpy(secretStart, gc->secret);

        if (!(mdctx = EVP_MD_CTX_new()))
            PANIC;
        EVP_DigestInit(mdctx, EVP_md5());
        EVP_DigestUpdate(mdctx, t->buffer, strlen(t->buffer));
        EVP_DigestFinal(mdctx, md_value, &md_len);
        EVP_MD_CTX_free(mdctx);
        mdctx = NULL;

        /* The secret was at this point, but now we overwrite to cancel it out */
        strcpy(secretStart, "md5=");
        MD5DigestToHex(strchr(secretStart, 0), md_value, FALSE);

        msg64 = Encode64(NULL, t->buffer);

        sprintf(t->buffer, "%s?msg=", gc->url);
        AddEncodedField(t->buffer, msg64, NULL);
        strcat(t->buffer, "&comp=");
        AddEncodedField(t->buffer, gc->shortName, NULL);
        SimpleRedirect(t, t->buffer, 0);
        goto Finished;
    }

    if (gc->dsa == NULL) {
        AdminGartnerLoadKey(gc);
        if (gc->dsa == NULL) {
            HTMLHeader(t, htmlHeaderOmitCache);
            UUSendZ(s,
                    "Gartner configuration is incomplete.  Encryption key has not been generated.");
            goto Finished;
        }
    }

    userToken = UserToken2("g", (t->session)->logUserBrief, 1);

    if (userToken == NULL) {
        AdminNoToken(t);
        goto Finished;
    }

    UUtime(&now);
    /* Technically, uid and comp should all be URL safe, but why take chances */
    strcpy(t->buffer, "uid=");
    AddEncodedField(t->buffer, userToken, NULL);
    strcat(t->buffer, "&comp=");
    AddEncodedField(t->buffer, gc->shortName, NULL);
    /* The Java version is to the thousands, so through in a few zeros at the end */
    sprintf(strchr(t->buffer, 0), "&dt=%" PRIdMAX "000", (intmax_t)now);

    if (!(mdctx = EVP_MD_CTX_new()))
        PANIC;
    EVP_DigestInit(mdctx, EVP_sha1());
    EVP_DigestUpdate(mdctx, t->buffer, strlen(t->buffer));
    EVP_DigestFinal(mdctx, md_value, &md_len);
    EVP_MD_CTX_free(mdctx);
    mdctx = NULL;

    if (!(sigret = malloc(DSA_size(gc->dsa))))
        PANIC;

    if (DSA_sign(0, md_value, md_len, sigret, &siglen, gc->dsa) != 1) {
        LogSsl("DSA_sign failed");
        goto Finished;
    }

    msg64 = Encode64(NULL, t->buffer);
    sig64 = Encode64Binary(NULL, sigret, siglen);

    sprintf(t->buffer, "%s?msg=%s&sg=%s&comp=%s", gc->url, msg64, sig64, gc->shortName);
    SimpleRedirect(t, t->buffer, 0);

    /*
    if (DSA_verify(0, "Hello", 5, sigret, siglen, dsa) != 1) {
        LogSsl("DSA_verify failed");
        goto Finished;
    }
    */

Finished:
    FreeThenNull(userToken);
    FreeThenNull(msg64);
    FreeThenNull(sig64);
    FreeThenNull(sigret);
    GroupMaskFree(&requiredGm);
}
