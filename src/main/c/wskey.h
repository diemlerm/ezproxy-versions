/**
 * wskey.h
 *
 *  Created on: Jul 11, 2013
 *      Author: smelserw
 *
 * This encapsulates making a request to wskey server.  It provides 2 mechanisms,
 * either through proxies configured in the config.txt or by using standard
 * sockets.
 *
 * It is up to the caller to determine which mechanism to use.
 *
 * This does not do any verification of the license, only makes the call
 * to wskey and sets the signature returned from wskey.  This signature
 * must still be verified.
 *
 * Success condition is when the license->signature has bee successfully
 * set.  In this instance a result of WSKEY_SIG_SET is returned.
 *
 * Failure conditions can be any of the other WSKEY_STATES.
 *
 */

#ifndef WSKEY_H_
#define WSKEY_H_

#include <string.h>
#include <time.h>

#include <libxml/globals.h>
#include <libxml/xmlmemory.h>
#include <libxml/tree.h>
#include <libxml/xmlreader.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "common.h"
#include "httpparser.h"
#include "uustring.h"
#include "uusocket.h"
#include "wskeylicense.h"

// used by wskey, included by common or something
// #include "ssl.c"
// #include "misc.c"

/**
 * Attempt to verify a wskey directly with the wiskey service.
 * @param s Already initialized socket which will be used
 * @param host The wiskey host that socket is linked to
 * @param port The port sockey is talking to wskey host on
 * @param key The wskey key to test
 *
 * @return WSKEY_SIG_SET on success
 */
enum WSKEY_STATES WSKeyVerify(struct UUSOCKET *s,
                              const char *host,
                              const PORT port,
                              const BOOL ssl,
                              struct WSKEYLICENSE *license);

/**
 * Attempt to verify a wskey through known proxies
 * @param s Already initialized socket which will be used
 * @param host The wiskey host that socket is linked to
 * @param port The port sockey is talking to wskey host on
 * @param ssl Wether to tunnel ssl connection
 * @param key The wskey key to test
 *
 * @return WSKEY_SIG_SET on success
 */
enum WSKEY_STATES WSKeyVerifyUsingProxies(struct UUSOCKET *s,
                                          const char *host,
                                          const PORT port,
                                          const BOOL ssl,
                                          struct WSKEYLICENSE *license);

#define WSKEY_FACILITY "WSkey License Validation"

#endif /* WSKEY_H_ */
