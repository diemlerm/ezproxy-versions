#ifndef __USRINSIGNIA_H__
#define __USRINSIGNIA_H__

#include "common.h"

enum FINDUSERRESULT FindUserInsignia(struct TRANSLATE *t,
                                     struct USERINFO *uip,
                                     char *file,
                                     int f,
                                     struct FILEREADLINEBUFFER *frb,
                                     struct sockaddr_storage *pInterface);

#endif  // __USRINSIGNIA_H__
