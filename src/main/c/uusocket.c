#define __UUFILE__ "uusocket.c"

#include "common.h"
#include "openssl/err.h"

#include "cvtutf.h"

#include "wskey.h"

#define DNSCACHE 300

const float growthFactor = 1.25F;

struct TRACKSOCKET *ts = NULL;

struct UUAVLTREE avlIpPortStatuses;

// missing in mingw, bah!
#ifdef WIN32
int inet_pton(int af, const char *src, void *dst) {
    struct sockaddr_storage ss;
    int size = sizeof(ss);
    char src_copy[INET6_ADDRSTRLEN + 1];

    ZeroMemory(&ss, sizeof(ss));
    /* stupid non-const API */
    strncpy(src_copy, src, INET6_ADDRSTRLEN + 1);
    src_copy[INET6_ADDRSTRLEN] = 0;

    if (WSAStringToAddress(src_copy, af, NULL, (struct sockaddr *)&ss, &size) == 0) {
        switch (af) {
        case AF_INET:
            *(struct in_addr *)dst = ((struct sockaddr_in *)&ss)->sin_addr;
            return 1;
        case AF_INET6:
            *(struct in6_addr *)dst = ((struct sockaddr_in6 *)&ss)->sin6_addr;
            return 1;
        }
    }
    return 0;
}

const char *inet_ntop(int af, const void *src, char *dst, socklen_t size) {
    struct sockaddr_storage ss;
    unsigned long s = size;

    ZeroMemory(&ss, sizeof(ss));
    ss.ss_family = af;

    switch (af) {
    case AF_INET:
        ((struct sockaddr_in *)&ss)->sin_addr = *(struct in_addr *)src;
        break;
    case AF_INET6:
        ((struct sockaddr_in6 *)&ss)->sin6_addr = *(struct in6_addr *)src;
        break;
    default:
        return NULL;
    }
    /* cannot direclty use &size because of strict aliasing rules */
    return (WSAAddressToString((struct sockaddr *)&ss, sizeof(ss), NULL, dst, &s) == 0) ? dst
                                                                                        : NULL;
}

#endif

BOOL UUIsRFC952Hostname(char *n, BOOL allowUnderscores, BOOL allowColons) {
    /* First character must be alpha */
    /* Of course, we use digits at the start of proxy by hostname entries... */
    if (!(IsAlpha(*n) || IsDigit(*n)))
        return 0;

    n++;

    for (; *n; n++) {
        if (IsAlpha(*n) || IsDigit(*n) || (*n == '_' && allowUnderscores) || *n == '.' ||
            *n == '-' || (*n == ':' && allowColons))
            continue;
        return 0;
    }

    /* The last may not be . or - */
    n--;
    if (*n == '.' || *n == '-')
        return 0;

    return 1;
}

int cTrackSockets = 0;

/*
Some systems have the restrictions of needing to keep file descriptors 0-255 available for certain
routines like gethostbyname on Solaris, or some other stdio routines on Solaris.
*/
#ifndef WIN32
SOCKET MoveFileDescriptorHigh(SOCKET s) {
    SOCKET n;

    if ((s == SOCKET_ERROR) || (s >= 256)) {
        return s;
    }

    SetErrno(0);
    n = fcntl(s, F_DUPFD, 256);
    if (n == SOCKET_ERROR) {
        /*
        [EINVAL]
        The cmd argument is invalid, or the cmd argument is F_DUPFD and arg is negative or greater
        than or equal to {OPEN_MAX}, or the cmd argument is F_GETLK, F_SETLK, or F_SETLKW and the
        data pointed to by arg is not valid, or fildes refers to a file that does not support
        locking.
        */
        if (debugLevel > 5 && errno != EINTR) {
            Log("%" SOCKET_FORMAT " Failed to move FD, errno=%d", s, errno);
        }
        return s;
    }
    if (debugLevel > 19) {
        Log("%" SOCKET_FORMAT " Moved to %" SOCKET_FORMAT, s, n);
    }
    UUcloseFD(s);
    return n;
}
#else
/* On Windows platforms, MoveFileDescriptorHigh is a macro in uusocket.h that does nothing.
   It is set up this way for Windows so it can wrap socket calls that are unsigned, and
   file calls that are signed, without disturbing the sign since Windows routines don't have
   the restrictions of needing to keep file descriptors 0-255 available for certain routines
   (e.g. gethostbyname on Solaris, other stdio routines on Solaris)
*/
#endif

void TSInit(void) {
    if (!(ts = calloc(TSMAXSOCKET, sizeof(struct TRACKSOCKET))))
        PANIC;
}

void TSOpenSocket(struct UUSOCKET *s, const char *host, const char *facility) {
    struct TRACKSOCKET *tsp;
    static int reported = 0;
    char line[256];
    char dopened[MAXASCIIDATE], dstopped[MAXASCIIDATE];
    int f;
    int i;
    char fn[20];

    if (ts == NULL || s->o < 0 || s->o >= TSMAXSOCKET)
        return;

    if (debugLevel > 500) {
        Log("TSOpenSocket for %s on %" SOCKET_FORMAT ", %s", host, s->o, facility);
    }

    UUAcquireMutex(&mTrackSocket);

    tsp = ts + s->o;

    tsp->s = s;
    StrCpy3(tsp->host, (char *)host, sizeof(tsp->host));
    tsp->facility = facility;
    tsp->stopped = 0;
    UUtime(&tsp->opened);
    cTrackSockets++;
    if (reported + 50 > cTrackSockets) {
        UUReleaseMutex(&mTrackSocket);
        return;
    }

    reported = cTrackSockets;

    sprintf(fn, "sockets.%d", reported);

    f = UUopenCreateFD(fn, O_CREAT | O_WRONLY | O_TRUNC, defaultFileMode);
    if (f < 0) {
        UUReleaseMutex(&mTrackSocket);
        Log("Unable to create %s: %d", fn, errno);
        return;
    }

    for (i = 0, tsp = ts; i < TSMAXSOCKET; i++, tsp++) {
        if (tsp->opened == 0)
            continue;
        ASCIIDate(&(tsp->opened), dopened);
        ASCIIDate(&(tsp->stopped), dstopped);
        sprintf(line, "%d %s %s %s ", i, dopened, dstopped, tsp->facility);
        write(f, line, strlen(line));
        write(f, tsp->host, strlen(tsp->host));
        write(f, "\n", 1);
    }
    UUcloseFD(f);

    UUReleaseMutex(&mTrackSocket);
}

void TSStopSocket(struct UUSOCKET *s) {
    struct TRACKSOCKET *tsp;

    if (ts == NULL || s->o < 0 || s->o >= TSMAXSOCKET)
        return;

    UUAcquireMutex(&mTrackSocket);
    tsp = ts + s->o;
    UUtime(&(tsp->stopped));
    tsp->s = NULL;
    UUReleaseMutex(&mTrackSocket);
}

void TSCloseSocket(SOCKET o) {
    struct TRACKSOCKET *tsp;

    if (ts == NULL || o < 0 || o >= TSMAXSOCKET)
        return;

    tsp = ts + o;

    UUAcquireMutex(&mTrackSocket);
    if (tsp->opened == 0) {
        Log("TSCloseSocket: %" SOCKET_FORMAT " already closed %s %s", o,
            (tsp->facility ? tsp->facility : ""), tsp->host);
    }
    tsp->opened = 0;
    tsp->s = NULL;
    tsp->opened = 0;
    cTrackSockets--;
    UUReleaseMutex(&mTrackSocket);
}

void UUInitSocketRecvChunking(struct UUSOCKET *s, intmax_t contentLength) {
    if (debugLevel > 1)
        Log("init socket %" SOCKET_FORMAT " for chunking %" PRIdMAX, s->o, contentLength);

    if (contentLength >= 0) {
        s->recvChunking = 20;
        s->recvChunkRemain = contentLength;
    } else {
        s->recvChunking = 1;
        s->recvChunkRemain = 0;
    }
}

void UUInitSocketCore(struct UUSOCKET *s, enum UUSOCKETTYPE socketType) {
    s->o = INVALID_SOCKET;
    s->mb = NULL;
    UUtime(&s->lastActivity);
    s->expires = s->lastActivity + 3600;
    s->timeout = 60;
    s->lastRecvWant = s->lastSendWant = 0;
    s->sendCount = 0;
    s->recvCount = 0;
    s->recvBufferEob = s->recvBufferOfs = s->sendBufferEob = 0;
    s->autoFlushOnRecv = 1;
    s->sendOK = 1;
    s->recvOK = 1;
    s->savedSendErrno = 0;
    s->savedRecvErrno = 0;
    s->shut_wr = s->shut_rd = 0;
    s->socketType = socketType;
    s->logFile = -1;
    s->fiddlerRecvFile = -1;
    s->fiddlerSendFile = -1;
    s->logFileActivity = 0;
    s->logFileDirection = (optionLogSockets ? LFLOG : LFNOLOG);
    s->recvChunking = 0;
    s->sendChunking = 0;
    s->utf16 = 0;
    s->lastSendBlocked = 0;
    s->sendZip = 0;
    s->recvEof = 0;
    s->recvZip = UURECVZIPNONE;
    s->recvZEof = 0;
    s->recvMarkSet = 0;
    s->readyToReuse = 0;
}

void UUInitSocketStream(struct UUSOCKET *s, SOCKET o) {
    UUInitSocketCore(s, STSTREAM);
    s->o = o;
}

void UUInitSocket(struct UUSOCKET *s, SOCKET o) {
    UUInitSocketStream(s, o);
}

void MemoryBufferInit(struct UUMEMORYBUFFER *mb, char *buffer, int readEob, int maxLen) {
    mb->buffer = buffer;
    mb->maxLen = maxLen;
    mb->readOfs = mb->writeOfs = 0;
    mb->readEob = readEob;
    mb->reallocFailed = 0;
}

void UUInitSocketMemory(struct UUSOCKET *s,
                        struct UUMEMORYBUFFER *mb,
                        enum UUSTMEMORYACCESS mbAccess) {
    UUInitSocketCore(s, STMEMORY);
    s->mb = mb;
    s->mbAccess = mbAccess;
}

void UUInitSocketNull(struct UUSOCKET *s) {
    UUInitSocketCore(s, STNULL);
}

int MemoryBufferRead(struct UUMEMORYBUFFER *mb, char *ubuffer, int len) {
    int remain;

    remain = mb->readEob - mb->readOfs;

    if (remain <= 0)
        return 0;

    if (len > remain)
        len = remain;

    memcpy(ubuffer, mb->buffer + mb->readOfs, len);
    mb->readOfs += len;
    return len;
}

int MemoryBufferWrite(struct UUMEMORYBUFFER *mb, char *ubuffer, int len) {
    int gap;
    int overlap;
    int slack;
    int newMaxLen;
    int readLen;
    int newReadOfs;
    char *newBuffer;

    if (mb->reallocFailed)
        return -1;

    gap = mb->readOfs - mb->writeOfs;

    overlap = len - gap;
    if (overlap > 0) {
        slack = mb->maxLen - mb->readEob;
        if (slack < overlap) {
            newMaxLen = (int)((float)(mb->maxLen + overlap) * growthFactor);
            newBuffer = realloc(mb->buffer, newMaxLen);
            if (newBuffer == NULL) {
                mb->reallocFailed = 1;
                return -1;
            }
            mb->buffer = newBuffer;
            mb->maxLen = newMaxLen;
        }
        readLen = mb->readEob - mb->readOfs;
        newReadOfs = mb->maxLen - readLen;
        memmove(mb->buffer + newReadOfs, mb->buffer + mb->readOfs, readLen);
        mb->readOfs = newReadOfs;
        /* we shifted the read part to the end, so readEob must be at the end now */
        mb->readEob = mb->maxLen;
    }

    memcpy(mb->buffer + mb->writeOfs, ubuffer, len);
    mb->writeOfs += len;
    return len;
}

/**
 * Get the in_addr or in6_addr pointer
 */
void *get_in_addr(const struct sockaddr *sa) {
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in *)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

/**
 * Get the in_addr or in6_addr pointer
 */
void *get_in_addr2(const struct sockaddr_storage *sa) {
    return get_in_addr((struct sockaddr *)sa);
}

void set_port(struct sockaddr_storage *sa, PORT port) {
    if (sa->ss_family == AF_INET) {
        ((struct sockaddr_in *)sa)->sin_port = htons(port);
    } else {
        ((struct sockaddr_in6 *)sa)->sin6_port = htons(port);
    }
}

/**
 * Check if this is an ipv4 address stored in ipv6 notation
 */
BOOL is_ipv4_as_ipv6(const struct sockaddr_storage *sa) {
    if (sa->ss_family == AF_INET)
        return FALSE;

    unsigned char *addr = ((struct sockaddr_in6 *)sa)->sin6_addr.s6_addr;

    // byte order does not matter, this probably gets optimized well by compiler
    if (
        /* check first 10 bytes are 0 */
        addr[0] == 0 && addr[1] == 0 && addr[2] == 0 && addr[3] == 0 && addr[4] == 0 &&
        addr[5] == 0 && addr[6] == 0 && addr[7] == 0 && addr[8] == 0 && addr[9] == 0 &&
        addr[10] == 0) {
        /* check next 2 bytes */
        // ipv4 written as ipv6 for ipv6 capable device
        if (addr[11] == 0xFF && addr[12] == 0xFF) {
            return TRUE;
            // non-ipv6 capable device's ipv4 address written in ipv6
        } else if (addr[11] == 0 && addr[12] == 0) {
            return TRUE;
        }
    }

    return FALSE;
}

/**
 * Set the IP on sockaddr_storage by passing in 4 32 bit values.  This presumes sa->ss_family has
 * been properly set.  So if you you are using ipv4, then only b32_1 value is used.
 */
void set_ip(struct sockaddr_storage *sa,
            unsigned long b32_1,
            unsigned long b32_2,
            unsigned long b32_3,
            unsigned long b32_4) {
    if (sa->ss_family == AF_INET) {
        ((struct sockaddr_in *)sa)->sin_addr.s_addr = htonl(b32_1);
    } else {
        unsigned long nb32_1, nb32_2, nb32_3, nb32_4;
        nb32_1 = htonl(b32_1);
        nb32_2 = htonl(b32_2);
        nb32_3 = htonl(b32_3);
        nb32_4 = htonl(b32_4);

        unsigned char *addr = ((struct sockaddr_in6 *)sa)->sin6_addr.s6_addr;
        memcpy(&addr[0], &nb32_1, 4);
        memcpy(&addr[4], &nb32_2, 4);
        memcpy(&addr[8], &nb32_3, 4);
        memcpy(&addr[12], &nb32_4, 4);
    }
}

/**
 * Returns TRUE if set bit operation was legal, FALSE otherwise.
 */
BOOL set_ip_bit(struct sockaddr_storage *sa, int bit, int value) {
    unsigned char *base;
    int max;
    int offset;
    unsigned char mask;

    if (sa->ss_family == AF_INET) {
        base = (unsigned char *)&((struct sockaddr_in *)sa)->sin_addr.s_addr;
        max = 32;
    } else {
        base = (unsigned char *)&((struct sockaddr_in6 *)sa)->sin6_addr.s6_addr;
        max = 128;
    }

    if (bit < 0 || bit >= max || value < 0 || value > 1) {
        return FALSE;
    }

    offset = bit >> 3;
    mask = 1 << (7 - (bit & 7));
    if (value == 0) {
        *(base + offset) &= ~mask;
    } else {
        *(base + offset) |= mask;
    }

    return TRUE;
}

void increment_ip(struct sockaddr_storage *sa, int amount) {
    unsigned long *ptr;
    if (sa->ss_family == AF_INET)
        ptr = (unsigned long *)&((struct sockaddr_in *)sa)->sin_addr.s_addr;
    else
        ptr = (unsigned long *)&(((struct sockaddr_in6 *)sa)->sin6_addr.s6_addr)[12];
    *ptr = htonl(ntohl(*ptr) + amount);
}

/**
 * Wrapper for inet_pton to take sockaddr_storage as input.
 * @return Failure: -1, Success: 1.  also, global errno is set.
 */
int inet_pton_st(const int family, const char *host, struct sockaddr_storage *sa) {
    if (family == AF_INET) {
        char altHost[strlen(host) + 1];
        char *p;
        BOOL front = 1;
        sa->ss_family = AF_INET;
        strcpy(altHost, host);
        for (p = altHost; *p;) {
            if (*p == '.') {
                front = 1;
                p++;
            } else if (front && *p == '0' && isdigit(*(p + 1))) {
                StrCpyOverlap(p, p + 1);
            } else {
                front = 0;
                p++;
            }
        }
        return inet_pton(family, altHost, &((struct sockaddr_in *)sa)->sin_addr);
    } else {
        sa->ss_family = AF_INET6;
        return inet_pton(family, host, &((struct sockaddr_in6 *)sa)->sin6_addr);
    }
}

/**
 * wrapper for inet_pton_st that will check the family for you
 * prior to calling inet_pton_st()
 * @return Failure: -1, Success: 1.  also, global errno is set.
 */
int inet_pton_st2(const char *addr, struct sockaddr_storage *sa) {
    int family = (NumericHostIp4(addr)) ? AF_INET : AF_INET6;
    return inet_pton_st(family, addr, sa);
}

void set_inaddr_any(struct sockaddr_storage *sa) {
    if (sa->ss_family == AF_INET) {
        ((struct sockaddr_in *)sa)->sin_addr.s_addr = INADDR_ANY;
    } else {
        ((struct sockaddr_in6 *)sa)->sin6_addr = in6addr_any;
    }
}

// TODO: look at UUinet_ntoa, since it hides 192.* addresses
const char *ipToStr(const struct sockaddr_storage *sa, char *ipBuffer, const size_t size) {
    const char *result = inet_ntop(sa->ss_family, get_in_addr2(sa), ipBuffer, size);
    if (result == NULL) {
        ipBuffer[0] = 0;
        result = ipBuffer;
    }
    return result;
}

const char *ipToStrV6Bracketed(const struct sockaddr_storage *sa,
                               char *ipBuffer,
                               const size_t size) {
    if (sa->ss_family != AF_INET6) {
        return ipToStr(sa, ipBuffer, size);
    }
    // [::] is the shortest possibility, requiring at least 5 bytes
    if (size < 5) {
        return NULL;
    }
    // Start by trying to put IPv6 address at one beyond the buffer
    if (ipToStr(sa, ipBuffer + 1, size) == NULL) {
        return NULL;
    }
    // That worked, so add a bracket at the start
    *ipBuffer = '[';
    int len = strlen(ipBuffer);
    // Make sure there is enough room to add a bracket and keep the \0 at the end
    if (len + 1 < size) {
        strcat(ipBuffer, "]");
        return ipBuffer;
    }
    // Ran out of room, so return NULL
    return NULL;
}

int get_size(const struct sockaddr_storage *sa) {
    return (sa->ss_family == AF_INET) ? sizeof(struct sockaddr_in) : sizeof(struct sockaddr_in6);
}

void init_storage(struct sockaddr_storage *sa,
                  const int family,
                  const BOOL inAddrAny,
                  const PORT port) {
    memset(sa, 0, sizeof(struct sockaddr_storage));
    if (family == 0)
        sa->ss_family = UUDEFAULT_FAMILY;
    else
        sa->ss_family = family;

    if (inAddrAny) {
        if (sa->ss_family == AF_INET) {
            ((struct sockaddr_in *)sa)->sin_addr.s_addr = INADDR_ANY;
        } else {
            ((struct sockaddr_in6 *)sa)->sin6_addr = in6addr_any;
        }
    }

    if (port > 0) {
        set_port(sa, port);
    }
}

BOOL is_addrnone(const struct sockaddr_storage *sa) {
    BOOL result = FALSE;
    if (sa->ss_family == AF_INET) {
        struct sockaddr_in *temp = (struct sockaddr_in *)sa;
        if ((temp->sin_addr).s_addr == INADDR_NONE) {
            result = TRUE;
        }
    } else {
        struct sockaddr_storage sa2;
        unsigned long max = 0xffffffff;

        sa2.ss_family = AF_INET6;
        set_ip(&sa2, max, max, max, max);

        if (compare_ip(sa, &sa2) == 0)
            result = TRUE;
    }
    return result;
}

BOOL is_inet(struct sockaddr_storage *sa) {
    return (sa->ss_family == AF_INET || sa->ss_family == AF_INET6);
}

/**
 * Alias of is_unspecified.
 */
BOOL is_ip_zero(struct sockaddr_storage *sa) {
    return is_unspecified(sa);
}

BOOL is_unspecified(struct sockaddr_storage *sa) {
    BOOL result = FALSE;
    if (sa->ss_family == AF_INET) {
        struct sockaddr_in *temp = (struct sockaddr_in *)sa;
        if ((temp->sin_addr).s_addr == INADDR_ANY) {
            result = TRUE;
        }
    } else {
        struct sockaddr_in6 *temp = (struct sockaddr_in6 *)sa;
        if (IN6_IS_ADDR_UNSPECIFIED(&temp->sin6_addr)) {
            result = TRUE;
        }
    }
    return result;
}

BOOL is_ip_equal(struct sockaddr_storage *sa1, struct sockaddr_storage *sa2) {
    return compare_ip(sa1, sa2) == 0;
}

/**
 * test a lower and upper bound for max ip range.
 *
 * @return -1 if families don't match or 1 on max range, 0 on not max range
 */
int is_max_range(struct sockaddr_storage *lower, struct sockaddr_storage *upper) {
    if (lower->ss_family != upper->ss_family)
        return -1;

    struct sockaddr_storage t1, t2;
    set_ip(&t1, 0, 0, 0, 0);
    set_ip(&t2, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff);

    t1.ss_family = lower->ss_family;
    t2.ss_family = upper->ss_family;

    return (compare_ip(&t1, lower) == 0 && compare_ip(&t2, upper)) ? 1 : 0;
}

int compare_ip(const struct sockaddr_storage *sa1, const struct sockaddr_storage *sa2) {
    if (sa1->ss_family == sa2->ss_family) {
        // ipv4
        if (sa1->ss_family == AF_INET) {
            struct in_addr *t1 = &((struct sockaddr_in *)sa1)->sin_addr;
            struct in_addr *t2 = &((struct sockaddr_in *)sa2)->sin_addr;
            return memcmp(t1, t2, 4);
            // ipv6
        } else {
            struct in6_addr *t1 = &((struct sockaddr_in6 *)sa1)->sin6_addr;
            struct in6_addr *t2 = &((struct sockaddr_in6 *)sa2)->sin6_addr;
            return memcmp(t1, t2, 16);
        }
    } else if (sa1->ss_family < sa2->ss_family) {
        return -1;
    } else {
        return 1;
    }
}

/**
 * Calculate the last 32 bit range in base 10 of 2 addresses.  Basically this will
 * work for ipv4 and ipv6.  If the range is larger than 32 bits, then the buffer
 * will be populated with "overflow".
 *
 * Buffer should be capable of holding 2^32 or "4294967296".  A length of 11 with
 * null byte.
 *
 * If the families are not equal then buffer is set to 'err-family'
 *
 * @return 0 on error the range on success.
 */
unsigned long get_range32(struct sockaddr_storage *lower,
                          struct sockaddr_storage *upper,
                          char *buffer) {
    if (lower->ss_family != upper->ss_family) {
        strcpy(buffer, "err-family");
        return 0;
    }

    unsigned int result = 0;

    // ip4 calculate range
    if (lower->ss_family == AF_INET) {
        unsigned long t1 = *((unsigned long *)&((struct sockaddr_in *)lower)->sin_addr);
        unsigned long t2 = *((unsigned long *)&((struct sockaddr_in *)upper)->sin_addr);
        result = ((ntohl(t2) - ntohl(t1)) + 1L);
        sprintf(buffer, "%lu", result);

        // we will try a little for ipv6
    } else {
        struct in6_addr *t1 = &((struct sockaddr_in6 *)lower)->sin6_addr;
        struct in6_addr *t2 = &((struct sockaddr_in6 *)upper)->sin6_addr;

        unsigned long *temp1, *temp2;

        // test first 96 bits of 128 bits
        BOOL fail = FALSE;
        int pos;
        for (pos = 0; pos < 12; pos = pos + 4) {
            temp1 = (unsigned long *)&(t1->s6_addr)[pos];
            temp2 = (unsigned long *)&(t2->s6_addr)[pos];

            if (*temp1 != *temp2) {
                fail = TRUE;
                break;
            }
        }
        if (fail) {
            strcpy(buffer, "overflow");

        } else {
            temp1 = (unsigned long *)&(t1->s6_addr)[12];
            temp2 = (unsigned long *)&(t2->s6_addr)[12];
            result = ((ntohl(*temp2) - ntohl(*temp1)) + 1L);
            sprintf(buffer, "%lu", result);
        }
    }

    return result;
}

struct sockaddr_storage *get_max_ip(struct sockaddr_storage *sa1, struct sockaddr_storage *sa2) {
    if (sa1->ss_family != sa2->ss_family)
        return NULL;

    return (compare_ip(sa1, sa2) >= 0) ? sa1 : sa2;
}

struct sockaddr_storage *get_min_ip(struct sockaddr_storage *sa1, struct sockaddr_storage *sa2) {
    if (sa1->ss_family != sa2->ss_family)
        return NULL;

    return (compare_ip(sa2, sa1) >= 0) ? sa1 : sa2;
}

uint16_t get_port(const struct sockaddr_storage *sa) {
    uint16_t port = (sa->ss_family == AF_INET) ? ((struct sockaddr_in *)sa)->sin_port
                                               : ((struct sockaddr_in6 *)sa)->sin6_port;
    return ntohs(port);
}

unsigned int get_port2(const struct sockaddr *ai) {
    uint16_t port = (ai->sa_family == AF_INET) ? ((struct sockaddr_in *)ai)->sin_port
                                               : ((struct sockaddr_in6 *)ai)->sin6_port;
    return ntohs(port);
}

char *UUGetHostByAddr(const struct sockaddr_storage *sa, char *host, size_t hostlen) {
    char service[20];
    if (getnameinfo((struct sockaddr *)sa, get_size(sa), host, hostlen, service, sizeof service,
                    0) != 0) {
        *host = 0;
    }
    return host;
}

struct GHBNADDR {
    struct GHBNADDR *flink, *blink;
    struct sockaddr_storage ia;
    int status;
};

struct GHBNTREE {
    struct GHBNTREE *left, *right;
    char *host;
    time_t lastUpdated;
    struct GHBNADDR *sas;
    UUMUTEX mutex;
};

/* Returns pointer to tree node if new host or entry need update
   or NULL with sa fields updated if valid cache entry found
*/
struct GHBNTREE *UUGetHostByNameCache(const char *host) {
    int result = -1;
    static struct GHBNTREE *root = NULL;
    struct GHBNTREE *tree, *last, *n;

    UUAcquireMutex(&mGetHostByNameTree);

    last = NULL;
    tree = root;

    while (tree != NULL) {
        result = stricmp(tree->host, host);
        if (result == 0) {
            UUReleaseMutex(&mGetHostByNameTree);
            return tree;
        }
        last = tree;
        if (result < 0) {
            tree = tree->left;
        } else {
            tree = tree->right;
        }
    }

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;

    if (!(n->host = strdup(host)))
        PANIC;

    if (!(n->sas = calloc(1, sizeof(struct GHBNADDR))))
        PANIC;
    (n->sas)->flink = (n->sas)->blink = n->sas;

    if (last == NULL) {
        root = n;
    } else if (result < 0) {
        last->left = n;
    } else {
        last->right = n;
    }

    UUInitMutex(&n->mutex, host);

    UUReleaseMutex(&mGetHostByNameTree);
    return n;
}

/* This routine assumes that its caller already holds the mGetHostByNameTree mutex */

struct GHBNADDR *UUGetHostByNameCacheAddress(struct GHBNTREE *g,
                                             struct sockaddr_storage *ia,
                                             BOOL create) {
    struct GHBNADDR *a;

    for (a = (g->sas)->flink; a != g->sas; a = a->flink) {
        if (compare_ip(&(a->ia), (struct sockaddr_storage *)ia) == 0) {
            return a;
        }
    }

    if (!create) {
        return NULL;
    }

    if (!(a = calloc(1, sizeof(*a))))
        PANIC;
    a->flink = g->sas;
    a->blink = (g->sas)->blink;
    (a->flink)->blink = (a->blink)->flink = a;

    memcpy(&(a->ia), ia, get_size(ia));
    return a;
}

int UUGetHostByNameFailedAddress(const char *host, struct sockaddr_storage *sa, int family) {
    struct GHBNTREE *g;
    struct GHBNADDR *head, *a;
    int count;

    g = UUGetHostByNameCache(host);
    if (g == NULL) {
        return 0;
    }

    UUAcquireMutex(&mGetHostByNameTree);
    head = g->sas;
    a = UUGetHostByNameCacheAddress(g, sa, 0);
    /* If it's already at the end, no need to move, otherwise move to end */
    if (a && a->flink != head) {
        /* remove from current position */
        (a->flink)->blink = a->blink;
        (a->blink)->flink = a->flink;
        /* and place at end */
        a->flink = head;
        a->blink = head->blink;
        (a->flink)->blink = (a->blink)->flink = a;
    }
    for (count = 0, a = head->flink; a != head; a = a->flink) {
        if (family == AF_UNSPEC || family == a->ia.ss_family) {
            count++;
        }
    }
    UUReleaseMutex(&mGetHostByNameTree);
    return count;
}

struct GHBNADDR *UUGetHostByNameCacheFirstByFamily(struct GHBNTREE *cache, int family) {
    struct GHBNADDR *a = cache->sas->flink;

    if (family == AF_UNSPEC && a != cache->sas) {
        return a;
    }

    for (; a != cache->sas; a = a->flink) {
        if (family == a->ia.ss_family) {
            return a;
        }
    }
    return NULL;
}

/**
 * Add getaddrinfo results to the cache in reverse order so that any new
 * addresses are added to the end of the cache in the same order they were
 * provided from the DNS server.
 */
void UUGetHostByNameCacheAddResults(struct GHBNTREE *cache, struct addrinfo *ai) {
    if (ai) {
        UUGetHostByNameCacheAddResults(cache, ai->ai_next);
        struct GHBNADDR *a =
            UUGetHostByNameCacheAddress(cache, (struct sockaddr_storage *)ai->ai_addr, 1);
        a->status = 1;
    }
}

int UUGetHostByName(const char *host, struct sockaddr_storage *sa, PORT port, int family) {
    int tries;
    struct GHBNTREE *cache;
    time_t now;
    struct GHBNADDR *a, *na;
    struct addrinfo *aiResult = NULL;
    int result = 1;

    memset(sa, 0, sizeof(*sa));

    if (host == NULL) {
        return UUSEHOSTNOTFOUND;
    }

    if (NumericHostIp4(host)) {
        init_storage(sa, AF_INET, 0, 0);
        inet_pton_st(AF_INET, host, sa);
        set_port(sa, port);
        return 0;
    }

    if (NumericHostIp6(host)) {
        init_storage(sa, AF_INET6, 0, 0);
        inet_pton_st(AF_INET6, host, sa);
        set_port(sa, port);
        return 0;
    }

    // Unless Option IPv6 is active, if unspecified, force family to IPv4
    if (family == AF_UNSPEC && UUDEFAULT_FAMILY == AF_INET) {
        family = AF_INET;
    }

    cache = UUGetHostByNameCache(host);

    if (!UUTryAcquireMutex(&cache->mutex)) {
        /* If we don't immediately get the host-specific mutex, someone else is performing a lookup,
           so we wait for the other thread to finish, then use its results
        */
        UUAcquireMutex(&cache->mutex);
        a = NULL;
        /* If there is anything in the cache, try to use it */
        if (cache->lastUpdated && cache->sas != (cache->sas)->flink) {
            UUAcquireMutex(&mGetHostByNameTree);
            a = UUGetHostByNameCacheFirstByFamily(cache, family);
            if (a) {
                result = 0;
                memcpy(sa, &(a->ia), sizeof(*sa));
                set_port(sa, port);
                if (debugLevel > 5)
                    Log("thread cache used for %s", host);
            }
            UUReleaseMutex(&mGetHostByNameTree);
        }
        if (a == NULL && debugLevel > 5) {
            Log("thread cache failed for %s", host);
        }
        UUReleaseMutex(&cache->mutex);
        return result;
    }

    for (tries = 0;; tries++) {
        /* If lastUpdated is 0, we've never completed a getaddrinfo so there's no cache */
        if (cache->lastUpdated) {
            UUtime(&now);
            /* Accept cached entries if last updated within 5 minutes, or if we've tried
               but failed a getaddrinfo, as long as there is at least one entry around
            */
            if (cache->sas != (cache->sas)->flink &&
                (now < cache->lastUpdated + DNSCACHE || tries == 1)) {
                UUAcquireMutex(&mGetHostByNameTree);
                a = UUGetHostByNameCacheFirstByFamily(cache, family);
                if (a) {
                    result = 0;
                    memcpy(sa, &(a->ia), sizeof(*sa));
                    set_port(sa, port);
                    if (debugLevel > 5) {
                        Log("cache used for %s", host);
                    }
                }
                UUReleaseMutex(&mGetHostByNameTree);
                UUReleaseMutex(&cache->mutex);
                return result;
            }
        }

        int status;
        struct addrinfo hints;
        memset(&hints, 0, sizeof(hints));
        // in windows, for some reason AF_INET6 lookups fail, but we get ipv6 with AF_UNSPEC
        hints.ai_family = AF_UNSPEC;
        // AI_ADDRCONFIG indicates that we only want IPv4 results if there
        // is a local IPv4 interface and IPv6 results if there is a local
        // IPv6 interface, but this hint is not always available
#ifdef AI_ADDRCONFIG
        hints.ai_flags |= AI_ADDRCONFIG;
#endif

        if (debugLevel > 9000) {
            Log("Using getaddrinfo() for %s lookup", host);
        }

        if ((status = getaddrinfo(host, NULL, &hints, &aiResult)) != 0) {
            if (tries < 1) {
                if (status == EAI_AGAIN || status == EAI_NODATA) {
                    Log("getaddrinfo error %s %d, retrying", host, status);
                    continue;
                }
            }
            UUReleaseMutex(&cache->mutex);
            Log("getaddrinfo error %s %d", host, status);
            return 1;
        }

        break;
    }

    UUAcquireMutex(&mGetHostByNameTree);
    UUtime(&cache->lastUpdated);

    /* Mark the entries as unseen so we can detect which disappeared */
    for (a = (cache->sas)->flink; a != cache->sas; a = a->flink) {
        a->status = 0;
    }

    /* Set status of existing entries to 1 and add in any newly discovered addresses */
    UUGetHostByNameCacheAddResults(cache, aiResult);

    /* Delete those entries that didn't appear in these getaddrinfo results */
    for (a = (cache->sas)->flink; a != cache->sas; a = na) {
        na = a->flink;
        if (a->status == 0) {
            (a->flink)->blink = a->blink;
            (a->blink)->flink = a->flink;
            FreeThenNull(a);
        }
    }

    a = UUGetHostByNameCacheFirstByFamily(cache, family);
    /* If there is a valid entry within the requested family, use it; otherwise, result will
     * indicate error */
    if (a) {
        result = 0;
        memcpy(sa, &(a->ia), get_size(&(a->ia)));
        set_port(sa, port);
    }

    UUReleaseMutex(&mGetHostByNameTree);

    UUReleaseMutex(&cache->mutex);

    freeaddrinfo(aiResult);
    aiResult = NULL;

    return result;
}

/**
 * Lookup a local ip interface by host name
 */
int UUGetIpInterfaceByName(char *host, struct sockaddr_storage *sa) {
    struct sockaddr_storage newsock;
    SOCKET s;
    int result;
    int one = 1;

    init_storage(&newsock, 0, 0, 0);
    if ((result = UUGetHostByName(host, &newsock, 0, 0))) {
        return result;
    }

    s = MoveFileDescriptorHigh(socket(newsock.ss_family, SOCK_STREAM, 0));
    if (debugLevel > 19) {
        Log("%" SOCKET_FORMAT " Open socket, errno=%d", s, (s == INVALID_SOCKET) ? errno : 0);
    }
    if (s == INVALID_SOCKET) {
        Log("Unable to create socket for UUGetLocalInterfaceByName: %d", socket_errno);
        PANIC;
    }

    setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *)&one, sizeof(one));

    result = bind(s, (struct sockaddr *)&newsock, get_size(&newsock));
    closesocket(s);

    if (result == 0) {
        memcpy(sa, &newsock, sizeof(*sa));
    }

    return result;
}

/**
 * In effect IPv4 and IPv6 will branch from the root element.  We
 * only compare IPv6 to other IPv6 addresses.  IPv4 address will
 * always be considered to be "smaller"
 */
int UUAvlTreeCmpIpPortStatuses(const void *v1, const void *v2) {
    struct IPPORTSTATUS *ips1 = (struct IPPORTSTATUS *)v1;
    struct IPPORTSTATUS *ips2 = (struct IPPORTSTATUS *)v2;

    // same family so we compare IPs
    if (ips1->netIp.ss_family == ips2->netIp.ss_family) {
        int cmp = compare_ip(&ips1->netIp, &ips2->netIp);
        if (cmp == 0) {
            return ((int)ips1->netPort) - ((int)ips2->netPort);
        }

        return (cmp > 0) ? 1 : -1;
    }

    return (ips1->netIp.ss_family > ips2->netIp.ss_family) ? 1 : -1;
}

void UUOpenLogFile(struct UUSOCKET *s, enum LOGFILEDIRECTION logFileDirection) {
    static int nextLogFileIdx = 1;
    char fn[MAX_PATH];

    if (s->logFileDirection != LFLOG || s->logFile != -1)
        goto Finished;

    UUAcquireMutex(&mActive);
    for (;;) {
        sprintf(fn, "sl%04d.log", nextLogFileIdx++);
        s->logFile = UUopenCreateFD(fn, O_CREAT | O_EXCL | O_WRONLY, defaultFileMode);
        if (s->logFile >= 0) {
            break;
        }
        if (errno == EEXIST)
            continue;
        Log("Unable to create %s: %d", fn, errno);
        break;
    }
    UUReleaseMutex(&mActive);

Finished:
    if (s->logFile != -1) {
        if (s->logFileDirection != logFileDirection || optionLogSockets == 2) {
            char msg[32];
            sprintf(msg, "\n%c", logFileDirection);
            /*
            logFileDirection == LFSEND ? 'S' :
                             logFileDirection == LFRECV ? 'R' :
                             logFileDirection == LFOPEN ? 'O' :
                             logFileDirection == LFEOF  ? 'E' :
                             logFileDirection == LFNOTE ? 'N' :
                             logFileDirection == LFCLOSE ? 'C' : '?');
            */
            if (optionLogSockets == 2) {
#ifdef WIN32
                DWORD ticks = GetTickCount();
                DWORD elapsed = s->logFileActivity ? ticks - s->logFileActivity : 0;
                s->logFileActivity = ticks;
#else
                time_t now;
                int elapsed;
                UUtime(&now);
                elapsed = s->logFileActivity ? now - s->logFileActivity : 0;
                s->logFileActivity = now;
#endif
                sprintf(strchr(msg, 0), " %d", elapsed);
            }
            strcat(msg, ":\n");
            if (s->logFileDirection != logFileDirection || optionLogSockets == 2)
                write(s->logFile, msg, strlen(msg));
            s->logFileDirection = logFileDirection;
        }
    }
}

struct IPPORTSTATUS *FindIpPortStatus(struct sockaddr_storage netIp, PORT netPort) {
    struct IPPORTSTATUS *ips, fips;
    static int sequence = 0;
    int currentSequence;

    fips.netIp = netIp;
    fips.netPort = netPort;

    RWLAcquireRead(&rwlIpPortStatuses);
    currentSequence = sequence;
    ips = UUAvlTreeFind(&avlIpPortStatuses, &fips);
    RWLReleaseRead(&rwlIpPortStatuses);

    if (ips)
        goto Finished;

    RWLAcquireWrite(&rwlIpPortStatuses);
    /* If the sequence number changed since we entered,
       check again to see if someone else was checking
       concurrently
    */
    if (sequence != currentSequence) {
        ips = UUAvlTreeFind(&avlIpPortStatuses, &fips);
    }
    if (ips == NULL) {
        if (!(ips = calloc(1, sizeof(*ips))))
            PANIC;
        ips->netIp = netIp;
        ips->netPort = netPort;
        UUAvlTreeInsert(&avlIpPortStatuses, ips);
        sequence++;
    }
    RWLReleaseWrite(&rwlIpPortStatuses);

Finished:
    return ips;
}

int GetIpPortStatus(struct IPPORTSTATUS *ips, BOOL haveMutex) {
    int status;

    if (ips->lastFailure == 0)
        return 1;

    if (ips->failures < 10)
        return 0;

    status = 0;

    if (!haveMutex)
        UUAcquireMutex(&mIpPortStatuses);

    if (ips->failures >= 10) {
        time_t now;
        UUtime(&now);
        /* Adjust in case the clock has changed wildly */
        if (ips->lastFailure > now)
            ips->lastFailure = now;
        if (ips->firstFailure > ips->lastFailure)
            ips->firstFailure = ips->lastFailure;
        if (now - ips->lastFailure < 30) {
            status = -1;
        }
    }

    if (!haveMutex)
        UUReleaseMutex(&mIpPortStatuses);

    return status;
}

void SetIpPortStatus(struct IPPORTSTATUS *ips, BOOL success, int waitingAdjustment) {
    time_t now;

    UUAcquireMutex(&mIpPortStatuses);
    ips->waiting += waitingAdjustment;
    UUtime(&now);

    if (success > 0) {
        ips->failures = 0;
        ips->lastSuccess = now;
        ips->firstFailure = ips->lastFailure = 0;
    } else {
        if (ips->firstFailure == 0)
            ips->firstFailure = now;
        ips->lastFailure = now;
        ips->failures++;
    }

    UUReleaseMutex(&mIpPortStatuses);
}

int UUConnectWithSource5(struct UUSOCKET *s,
                         const char *host,
                         PORT port,
                         const char *facility,
                         struct sockaddr_storage *sl,
                         int useSsl,
                         int sslIdx,
                         int maxTries,
                         int timeout) {
    struct sockaddr_storage sa;
    char *errorMsg;
    int tries = 0;
    BOOL timedOut;
    int hostCount;
    char ipServer[INET6_ADDRSTRLEN];
    struct IPPORTSTATUS *ips = NULL;
    int rc;
    struct timeval tv;
    time_t connectStart;
    time_t connectStop;
    int family = AF_UNSPEC;

    if (facility == NULL) {
        facility = "unknown";
    }

    if (maxTries == 0) {
        maxTries = 6;
    }

    if (timeout == 0) {
        timeout = 10; /* default to 10 seconds */
    }

    if (sl != NULL && !is_unspecified(sl)) {
        if (debugLevel > 9000)
            Log("Requested make connection using family: %d", sl->ss_family);
        family = sl->ss_family;
    } else {
        if (debugLevel > 9000)
            Log("Requested make connection using family: ANY");
    }

Again:
    /* *** FUTURE *** It would be great to monitor and see if the remote client shut down the
       connection, since we might be able to terminate retries if the remote user already gave up on
       us
    */
    if (UUGetHostByName(host, &sa, port, family)) {
        Log("UUConnect host '%s' not found in %s", host, facility);
        return UUSEHOSTNOTFOUND;
    }

    inet_ntop(sa.ss_family, get_in_addr((struct sockaddr *)(&sa)), ipServer, sizeof ipServer);
    unsigned int testport = get_port2((struct sockaddr *)(&sa));

    if (debugLevel > 9000) {
        Log("About to connect to %s on port %" PORT_FORMAT, ipServer, testport);
    }

    ips = FindIpPortStatus(sa, port);

    UUAcquireMutex(&mIpPortStatuses);
    if (ips->lastFailure) {
        for (;;) {
            int status = GetIpPortStatus(ips, 1);
            if (status < 0) {
                UUReleaseMutex(&mIpPortStatuses);
                Log("UUconnect remote host %s (%s) considered down in %s", host, ipServer,
                    facility);
                return UUSEBINDFAIL;
            }
            if (ips->lastFailure && ips->waiting > 5) {
                UUReleaseMutex(&mIpPortStatuses);
                SubSleep(1, 0);
                UUAcquireMutex(&mIpPortStatuses);
                continue;
            }
            break;
        }
    }
    ips->waiting++;
    dprintf("Waiting %d", ips->waiting);
    UUReleaseMutex(&mIpPortStatuses);

    s->o = MoveFileDescriptorHigh(socket(sa.ss_family, SOCK_STREAM, 0));
    if (debugLevel > 19) {
        Log("%" SOCKET_FORMAT " Opened socket, errno=%d", s->o,
            (s->o == INVALID_SOCKET) ? errno : 0);
    }
    if (s->o == INVALID_SOCKET) {
        Log("UUconnect socket failed %d in %s", socket_errno, facility);
        return UUSESOCKETFAIL;
    }

    tv.tv_sec = timeout;
    tv.tv_usec = 0;
    rc = setsockopt(s->o, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(tv));
    if (rc) {
        if (debugLevel > 8000) {
            /* NOTE: On non-POSIX systems, this setting is just a suggestion to the OS, and on some
             * OSs it even returns an error. */
            Log("UUconnect socket failed to set timeout, errno %d in %s, error ignored",
                socket_errno, facility);
        }
    }

    if (sl != NULL && !is_unspecified(sl)) {
        char ipLocal[INET6_ADDRSTRLEN];
        if (debugLevel > 9000) {
            ipToStr(sl, ipLocal, sizeof ipLocal);
            Log("UUconnect bind to local address %s in %s", ipLocal, facility);
        }

        if (bind(s->o, (struct sockaddr *)sl, get_size(sl)) == SOCKET_ERROR) {
            ipToStr(sl, ipLocal, sizeof ipLocal);
            Log("UUconnect bind to local address %s failed in %s", ipLocal, facility);

            closesocket(s->o);
            s->o = INVALID_SOCKET;
            return UUSEBINDFAIL;
        }
    }

    SocketSetBufferSize(s->o);

    /* On high volume NT servers, WSAEADDRINUSE was coming up, presumably by two threads or
       processes trying to use the same local socket number, so treat like WSAECONNREFUSED
    */
    if (debugLevel > 8000) {
        Log("UUconnect connect try #%d of %d with timeout %d seconds on %s:%d in %s", tries + 1,
            maxTries, timeout, host, port, facility);
        UUtime(&connectStart);
    }

    if (connect(s->o, (struct sockaddr *)&sa, get_size(&sa)) == SOCKET_ERROR) {
        /* Note that this address failed so we'll get another next time if there's more than 1 */
        SetIpPortStatus(ips, 0, -1);

        hostCount = UUGetHostByNameFailedAddress(host, &sa, family);
        timedOut = socket_errno == WSAETIMEDOUT;
        if ((timedOut || hostCount > 1 || socket_errno == WSAECONNREFUSED
#ifdef WIN32
             || socket_errno == WSAEADDRINUSE
#endif
             ) &&
            tries + 1 < maxTries) {
            /* No need to tie up a socket while we wait to retry */
            closesocket(s->o);
            s->o = INVALID_SOCKET;
            tries++;
            /* Use progressive back-off to avoid becoming really obnoxious if a lot of people
               queue up, but don't use for timeout since it already backed off anyway
            */
            if (!timedOut)
                SubSleep(tries, 0);
            goto Again;
        }

        errorMsg = ErrorMessage(socket_errno);
        Log("UUconnect connect refused by %s(%s):%" PORT_FORMAT " (%s) in %s", host, ipServer, port,
            errorMsg, facility);
        FreeThenNull(errorMsg);
        closesocket(s->o);
        s->o = INVALID_SOCKET;
        return UUSECONNECTFAIL;
    }
    if (debugLevel > 8000) {
        UUtime(&connectStop);
        Log("UUconnect connect try #%d responded in less than %f seconds on %s:%d in %s", tries + 1,
            difftime(connectStop, connectStart) + 1.0f, host, port, facility);
    }

    SetIpPortStatus(ips, 1, -1);

    SocketNonBlocking(s->o);
    if (optionDisableNagle) {
        SocketDisableNagle(s->o);
    }

    if (useSsl) {
        UUInitSocketSsl(s, s->o, UUSSLCONNECT, sslIdx, host);
        if (s->ssl == NULL) {
            closesocket(s->o);
            s->o = INVALID_SOCKET;
            return UUSESSLFAIL;
        }
    } else {
        UUInitSocketStream(s, s->o);
    }

    TSOpenSocket(s, host, facility);
    UUOpenLogFile(s, LFOPEN);

    return 0;
}

int UUConnectWithSource4(struct UUSOCKET *s,
                         const char *host,
                         PORT port,
                         const char *facility,
                         struct sockaddr_storage *sl,
                         int useSsl,
                         int sslIdx,
                         int maxTries) {
    return UUConnectWithSource5(s, host, port, facility, sl, useSsl, sslIdx, maxTries, 0);
}

int UUConnectWithSource3(struct UUSOCKET *s,
                         const char *host,
                         PORT port,
                         const char *facility,
                         struct sockaddr_storage *sl,
                         int useSsl,
                         int sslIdx) {
    return UUConnectWithSource4(s, host, port, facility, sl, useSsl, sslIdx, 0);
}

int UUConnectWithSource2(struct UUSOCKET *s,
                         const char *host,
                         PORT port,
                         const char *facility,
                         struct sockaddr_storage *sl,
                         int useSsl) {
    return UUConnectWithSource3(s, host, port, facility, sl, useSsl, 0);
}

int UUConnectWithSource(struct UUSOCKET *s,
                        const char *host,
                        PORT port,
                        const char *facility,
                        struct sockaddr_storage *sl) {
    return UUConnectWithSource2(s, host, port, facility, sl, 0);
}

int UUConnect2(struct UUSOCKET *s, const char *host, PORT port, const char *facility, int useSsl) {
    return UUConnectWithSource2(s, host, port, facility, NULL, useSsl);
}

int UUConnect(struct UUSOCKET *s, const char *host, PORT port, const char *facility) {
    return UUConnectWithSource2(s, host, port, facility, NULL, 0);
}

int UURecvDataWaiting(struct UUSOCKET *s) {
    return s->recvBufferEob != s->recvBufferOfs;
}

int UUSendDataWaiting(struct UUSOCKET *s) {
    return (s->sendZip == 0 && s->sendBufferEob != 0) ||
           (s->sendZip == 2 && s->sendZStream.avail_out != sizeof(s->sendBuffer));
}

int UUSocketBuffersNormalize(struct UUSOCKETBUFFERS *sbsi) {
    size_t len;
    struct SOCKETBUFFER *sbp, *sbpe;

    /* short-circuit the most common case */
    if (sbsi->count == 1) {
        if (sbsi->sb->len == 0)
            sbsi->count = 0;
        /* Even if we reduced count to 0, sbsi->sb->len is still valid as 0 */
        return sbsi->sb->len;
    }

    len = 0;

    for (sbp = sbpe = sbsi->sb + sbsi->count - 1; sbp >= sbsi->sb; sbp--) {
        if (sbp->msg && sbp->len) {
            len += sbp->len;
            continue;
        }
        if (sbp < sbpe) {
            memcpy(sbp, sbp + 1, sizeof(*sbp) * (sbpe - sbp));
        }
        sbsi->count--;
        sbpe--;
    }

    return len;
}

size_t UUSocketBuffersTotalLen(const struct UUSOCKETBUFFERS *sbsi) {
    size_t len = 0;
    const struct SOCKETBUFFER *sbp;

    for (sbp = sbsi->sb + sbsi->count - 1; sbp >= sbsi->sb; sbp--) {
        len += sbp->len;
    }

    return len;
}

void UUSocketBuffersRemoveSendBytes(struct UUSOCKETBUFFERS *sbsi, int len) {
    struct SOCKETBUFFER *sbp;

    if (len <= 0)
        return;

    for (sbp = sbsi->sb + sbsi->count - 1; sbp >= sbsi->sb; sbp--) {
        if (sbp->len <= (size_t)len) {
            len -= sbp->len;
            sbsi->count--;
        } else {
            sbp->msg = (char *)sbp->msg + len;
            sbp->len -= len;
            break;
        }
    }
}

int UUSocketBuffersLimitRecv(struct UUSOCKETBUFFERS *sbsi, size_t len) {
    struct SOCKETBUFFER *sbp;
    int totalResult = 0;

    if (len == 0) {
        sbsi->count = 0;
        return 0;
    }

    for (sbp = sbsi->sb; sbp < sbsi->sb + sbsi->count; sbp++) {
        if (sbp->len < len) {
            len -= sbp->len;
            totalResult += sbp->len;
        } else {
            sbp->len = len;
            sbsi->count = (sbp - sbsi->sb) + 1;
            totalResult += sbp->len;
            break;
        }
    }

    return totalResult;
}

void UUSocketBuffersCopy(struct UUSOCKETBUFFERS *dest, const struct UUSOCKETBUFFERS *src) {
    const struct SOCKETBUFFER *end = src->sb + src->count;
    memcpy(dest, src, (unsigned char *)end - (unsigned char *)src);
}

void UULogFileNote(struct UUSOCKET *s, char *format, ...) {
    va_list ap;
    char buffer[512];

    if (s->logFileDirection != LFNOLOG && format && *format) {
        va_start(ap, format);
        _SNPRINTF_BEGIN(buffer, sizeof(buffer));
        _vsnprintf(buffer, sizeof(buffer), format, ap);
        _SNPRINTF_END(buffer, sizeof(buffer));
        va_end(ap);

        UUOpenLogFile(s, LFNOTE);
        write(s->logFile, buffer, strlen(buffer));
        write(s->logFile, "\n", 1);
    }
}

int UURecvMemory(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    const struct SOCKETBUFFER *sbp;
    int result;
    int totalResult = 0;

    /* If only write access is allowed, fail as though on EOF */
    if (s->mbAccess == STMEMORYWRITE) {
        s->recvOK = 0;
        s->savedRecvErrno = EACCES;
        return 0;
    }

    for (sbp = sbsi->sb + sbsi->count - 1; sbp >= sbsi->sb; sbp--) {
        if (sbp->len == 0)
            continue;
        result = MemoryBufferRead(s->mb, sbp->msg, sbp->len);
        if (result < 0) {
            s->recvOK = 0;
            s->savedRecvErrno = errno;
            if (totalResult == 0) {
                SetErrno(s->savedRecvErrno);
                return result;
            }
            break;
        }
        if (result == 0)
            break;
        totalResult += result;
    }

    return totalResult;
}

/*
 * Upon successful completion, recvmsg(), and UURecvStream(), shall return the
 * length of the message in bytes. If no messages are available to be received and
 * the peer has performed an orderly shutdown, recvmsg() shall return 0. Otherwise,
 * -1 shall be returned and errno set to indicate the error.
 *
 */
int UURecvStream(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    int status;
    int buffs;
    const struct SOCKETBUFFER *sbp;
    int remain;
#ifdef WIN32
    DWORD len;
    DWORD flags;
    WSABUF buffptr[MAXUUSOCKETBUFFERS];
#else
    int len;
    struct msghdr mh;
    struct iovec iov[MAXUUSOCKETBUFFERS];
#endif

    remain = UUSocketBuffersTotalLen(sbsi);

Again:
    buffs = 0;

#ifdef WIN32
    for (sbp = sbsi->sb; sbp < sbsi->sb + sbsi->count; sbp++) {
        if (sbp->msg && sbp->len) {
            buffptr[buffs].buf = sbp->msg;
            buffptr[buffs++].len = sbp->len;
        }
    }
    if (buffs == 0)
        return 0;

    flags = 0;
    status = WSARecv(s->o, &buffptr[0], buffs, &len, &flags, NULL, NULL);

#else
    for (sbp = sbsi->sb; sbp < sbsi->sb + sbsi->count; sbp++) {
        if (sbp->msg && sbp->len) {
            iov[buffs].iov_base = sbp->msg;
            iov[buffs++].iov_len = sbp->len;
        }
    }
    if (buffs == 0)
        return 0;

    memset(&mh, 0, sizeof(mh));
    mh.msg_iov = iov;
    mh.msg_iovlen = buffs;
    TEMP_FAILURE_RETRY_INT(status = interrupted_neg_int(recvmsg(s->o, &mh, 0)));
    if (status >= 0) {
        len = status;
        status = 0;
    } else {
        status = -1;
        len = 0;
    }
#endif

    if (status < 0) {
        if (socket_errno == WSAEWOULDBLOCK) {
            if (uusflags & UUSNOWAIT) {
                s->lastRecvWant = UUWANTRECV;
                return -1;
            }
            if (UUWant(s->o, UUWANTRECV, s->timeout)) {
                UULogFileNote(s, "Receive timeout %d", s->timeout);
                s->recvOK = 0;
                s->savedRecvErrno = errno;
                SetErrno(s->savedRecvErrno);
                return -1;
            }
            goto Again;
        }
        UULogFileNote(s, "Receive error %d", socket_errno);
        s->recvOK = 0;
        s->savedRecvErrno = socket_errno;
        SetErrno(s->savedRecvErrno);
        return -1;
    }

    if (len <= 0)
        return len;

    s->recvCount += len;
    return len;
}

int UURecvRoute(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    int result;

    if (s->recvEof) {
        s->recvOK = 0;
    }

    if (s->recvOK == 0) {
        SetErrno(s->savedRecvErrno);
        s->savedRecvErrno = EINVAL; /* next will be invalid state */
        return -1;
    }

    s->lastRecvWant = 0;

    if (s->autoFlushOnRecv && UUSendDataWaiting(s))
        UUFlush(s);

    switch (s->socketType) {
    case STSTREAM:
        result = UURecvStream(s, sbsi, uusflags);
        break;

    case STSSL:
        result = UURecvSsl(s, sbsi, uusflags);
        break;

    case STNULL:
        s->recvOK = 0;
        s->recvEof = 1;
        s->savedRecvErrno = EPIPE; /* next will be seek past EOF */
        result = 0;
        break;

    case STMEMORY:
        result = UURecvMemory(s, sbsi, uusflags);
        break;

    default:
        Log("Unknown UURecv %d", s->socketType);
        result = -1;
        break;
    }

    if (result == 0) {
        if (s->logFileDirection != LFNOLOG) {
            UUOpenLogFile(s, LFEOF);
        }
        s->recvEof = 1;
        s->savedRecvErrno = EPIPE; /* next will be seek past EOF */
    }

    return result;
}

int UURecvLogging(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    int result;
    const struct SOCKETBUFFER *sbp;
    size_t remain;
    size_t len;

    result = UURecvRoute(s, sbsi, uusflags);

    if (result > 0 && s->logFileDirection != LFNOLOG) {
        if (s->logFileDirection != LFRECV || optionLogSockets == 2) {
            UUOpenLogFile(s, LFRECV);
        }

        for (remain = result, sbp = sbsi->sb; remain > 0 && sbp < sbsi->sb + sbsi->count; sbp++) {
            len = UUMIN(remain, sbp->len);
            if (len)
                write(s->logFile, sbp->msg, len);
            remain -= len;
        }
    }

    if (result > 0 && s->fiddlerRecvFile >= 0) {
        for (remain = result, sbp = sbsi->sb; remain > 0 && sbp < sbsi->sb + sbsi->count; sbp++) {
            len = UUMIN(remain, sbp->len);
            if (len)
                write(s->fiddlerRecvFile, sbp->msg, len);
            remain -= len;
        }
    }

    return result;
}

int UURecvBuffering(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    size_t bufferRemain;
    char *pBuffer;
    size_t bytesToMove;
    int totalResult = 0;
    struct UUSOCKETBUFFERS sbs;
    const struct SOCKETBUFFER *sbp;
    int toReceive;
    int result;

    while (s->recvBufferEob > 0 || s->recvMarkSet) {
        bufferRemain = s->recvBufferEob - s->recvBufferOfs;

        if (bufferRemain == 0) {
            if (s->recvMarkSet == 0) {
                s->recvBufferEob = s->recvBufferOfs = 0;
                break;
            }
            sbs.sb[0].len = sizeof(s->recvBuffer) - s->recvBufferEob;
            /* If we don't have any more room to take incoming while we are marked, return EOF */
            if (sbs.sb[0].len == 0)
                return 0;
            sbs.count = 1;
            sbs.sb[0].msg = s->recvBuffer + s->recvBufferEob;
            result = UURecvLogging(s, &sbs, uusflags);
            if (result <= 0)
                return result;
            s->recvBufferEob += result;
            bufferRemain = result;
        }

        pBuffer = s->recvBuffer + s->recvBufferOfs;
        for (sbp = sbsi->sb; sbp < sbsi->sb + sbsi->count && bufferRemain > 0; sbp++) {
            bytesToMove = UUMIN(sbp->len, bufferRemain);
            if (bytesToMove > 0) {
                memmove(sbp->msg, pBuffer, bytesToMove);
                pBuffer += bytesToMove;
                bufferRemain -= bytesToMove;
                totalResult += bytesToMove;
            }
        }

        if (bufferRemain == 0 && s->recvMarkSet == 0)
            s->recvBufferOfs = s->recvBufferEob = 0;
        else
            s->recvBufferOfs += totalResult;
        return totalResult;
    }

    UUSocketBuffersCopy(&sbs, sbsi);

    toReceive = UUSocketBuffersTotalLen(&sbs);

    sbs.sb[sbs.count].msg = s->recvBuffer;
    sbs.sb[sbs.count++].len = sizeof(s->recvBuffer);

    result = UURecvLogging(s, &sbs, uusflags);

    if (result > toReceive) {
        s->recvBufferEob = result - toReceive;
        result = toReceive;
    }

    return result;
}

int UURecvChunking(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    int result;
    struct UUSOCKETBUFFERS sbs;
    char c;
    struct UUSOCKETBUFFERS sbs1;

    if (s->recvChunking == 0)
        return UURecvBuffering(s, sbsi, uusflags);

    sbs1.count = 1;
    sbs1.sb[0].msg = &c;
    sbs1.sb[0].len = 1;

    /* s->recvChunking values
       0  not chunking
       1  at start of new chunk look for first length byte
       2  received at least one valid hex digit for length
       3  received ; so ignore to end of line
       10 received newline, checkRemain contains remaining length
       20 chunking based an absolute length from Content-Length header
       99 reached the end of chunked data
    */
    while (s->recvChunking < 10) {
        result = UURecvBuffering(s, &sbs1, uusflags);
        if (result < 1)
            return result;
        if (c == 10) {
            if (s->recvChunking > 1) {
                s->recvChunking = 10;
            }
        } else {
            if (s->recvChunking < 3) {
                if (isupper(c))
                    c = tolower(c);
                if (c >= '0' && c <= '9') {
                    s->recvChunkRemain = (s->recvChunkRemain << 4) + (c - '0');
                    s->recvChunking = 2;
                } else if (c >= 'a' && c <= 'f') {
                    s->recvChunkRemain = (s->recvChunkRemain << 4) + (c - 'a' + 10);
                    s->recvChunking = 2;
                } else if (s->recvChunking == 2 && c == ';') {
                    s->recvChunking = 3;
                }
            }
        }
    }

    if (s->recvChunkRemain == 0) {
        s->recvChunking = 99;
        return 0;
    }

    if (s->recvChunking == 99)
        return 0;

    if (debugLevel > 3) {
        Log("Socket %" SOCKET_FORMAT " chunk remain %lx", s->o, (unsigned long)s->recvChunkRemain);
    }

    UUSocketBuffersCopy(&sbs, sbsi);

    UUSocketBuffersLimitRecv(&sbs, s->recvChunkRemain);

    result = UURecvBuffering(s, &sbs, uusflags);

    if (result > 0) {
        s->recvChunkRemain -= result;
        if (s->recvChunkRemain == 0) {
            if (s->recvChunking == 20)
                s->recvChunking = 99;
            else
                s->recvChunking = 1;
        }
    }

    return result;
}

int UURecvZip(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    int result;
    int zResult;
    int totalResult = 0;
    struct UUSOCKETBUFFERS sbs;
    const struct SOCKETBUFFER *sbp;

    if (s->recvZip == UURECVZIPNONE) {
        return UURecvChunking(s, sbsi, uusflags);
    }

    if (s->recvZip == UURECVZIPGZIP || s->recvZip == UURECVZIPDEFLATE) {
        if (s->recvZip == UURECVZIPGZIP) {
            s->recvZip = UURECVZIPPROCESSING;
        } else {
            s->recvZip = UURECVZIPINITDEFLATE;
        }
        s->recvZStream.zalloc = Z_NULL;
        s->recvZStream.zfree = Z_NULL;
        s->recvZStream.opaque = Z_NULL;
        s->recvZStream.avail_in = 0;
        s->recvZStream.next_in = Z_NULL;
        // Adding 32 enables detection of gzip and zlib deflate headers
        zResult = inflateInit2(&s->recvZStream, MAX_WBITS + 32);
        if (zResult != Z_OK) {
            Log("inflateInit2 returned %d", zResult);
            PANIC;
        }
        s->recvZEof = 0;
        s->recvZNeedRead = 1;
    }

    if (s->recvZEof == 2) {
        return totalResult;
    }

    for (sbp = sbsi->sb; sbp < sbsi->sb + sbsi->count; sbp++) {
        char *msg;
        size_t len;
        for (msg = sbp->msg, len = sbp->len; len > 0;) {
            if (!s->recvZNeedRead) {
                s->recvZStream.next_out = (Bytef *)msg;
                s->recvZStream.avail_out = len;
                zResult = inflate(&s->recvZStream, Z_NO_FLUSH);
            } else {
                for (;;) {
                    /* if next_in doesn't point to the start of the receive buffer, shift
                       all of the contents back so they are a the start of the receive buffer
                    */
                    if (s->recvZStream.avail_in &&
                        s->recvZStream.next_in != (Bytef *)s->recvZBuffer) {
                        memcpy(s->recvZBuffer, s->recvZStream.next_in, s->recvZStream.avail_in);
                    }

                    s->recvZStream.next_in = (Bytef *)s->recvZBuffer;
                    sbs.count = 1;
                    sbs.sb[0].msg = s->recvZBuffer + s->recvZStream.avail_in;
                    sbs.sb[0].len = sizeof(s->recvZBuffer) - s->recvZStream.avail_in;
                    result = UURecvChunking(s, &sbs, uusflags);
                    if (result < 0) {
                        if (totalResult > 0)
                            return totalResult;
                        return result;
                    }

                    if (result > 0) {
                        s->recvZStream.avail_in += result;
                    } else {
                        s->recvZEof = 1;
                    }

                    // If we are still initializing deflate detection of zlib or raw,
                    // require that we've either hit eof or have at least 64 bytes
                    // so there is enough to detect if data stream is raw and retry
                    if (s->recvZip == UURECVZIPINITDEFLATE && s->recvZEof == 0 &&
                        s->recvZStream.avail_in < 64) {
                        continue;
                    }

                    int save_avail_in = s->recvZStream.avail_in;
                    s->recvZStream.next_out = (Bytef *)msg;
                    s->recvZStream.avail_out = len;
                    zResult = inflate(&s->recvZStream, Z_NO_FLUSH);

                    if (s->recvZip == UURECVZIPINITDEFLATE) {
                        s->recvZip = UURECVZIPPROCESSING;
                        // If we got back Z_DATA_ERROR when trying to inflate initial deflate
                        // content with zlib header, retry with header processing disabled
                        if (zResult == Z_DATA_ERROR) {
                            inflateEnd(&s->recvZStream);
                            // Using -MAX_WBITS indicates we want raw deflate instead of zlib
                            // deflate
                            zResult = inflateInit2(&s->recvZStream, -MAX_WBITS);
                            if (zResult != Z_OK) {
                                Log("inflateInit2 returned %d", zResult);
                                PANIC;
                            }
                            s->recvZStream.next_in = s->recvZBuffer;
                            s->recvZStream.avail_in = save_avail_in;
                            s->recvZStream.next_out = (Bytef *)msg;
                            s->recvZStream.avail_out = len;
                            zResult = inflate(&s->recvZStream, Z_NO_FLUSH);
                        }
                    }

                    break;
                }
                s->recvZNeedRead = 0;
            }

            /* Z_BUF_ERROR just means we can't make any progress with what is available;
               it's not actually serious; Z_OK and Z_STREAM_END are obviously expected
            */
            /* Le Grand Robert was returning gzip content that was slightly invalid at the
             * end, but good enough to return what matters.  When this happened, the result
             * was Z_DATA_ERROR.  Now, if we see that error, let through any data that
             * decompressed but then shutdown.
             */
            if (zResult != Z_OK && zResult != Z_STREAM_END && zResult != Z_BUF_ERROR &&
                zResult != Z_DATA_ERROR) {
                UULogFileNote(s, "inflate result error %d", zResult);
                s->recvZEof = 2;
                if (totalResult > 0)
                    return totalResult;
                return -1;
            }

            result = len - s->recvZStream.avail_out;
            totalResult += result;
            msg += result;
            len -= result;
            if (zResult == Z_STREAM_END || zResult == Z_DATA_ERROR ||
                (zResult = Z_BUF_ERROR && s->recvZEof == 1)) {
                s->recvZEof = 2;
                return totalResult;
            }
            /* If the entire buffer wasn't filled and we haven't hit eof, we need to read more */
            if (len > 0 && s->recvZEof == 0)
                s->recvZNeedRead = 1;
        }
    }

    return totalResult;
}

int UURecvUTF16(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    struct UUSOCKETBUFFERS sbs;
    int remain;
    int result;
    ConversionResult cr;
    unsigned char *start;
    unsigned char *end;
    char *msg1End;
    size_t converted;
    int totalResult = 0;
    char *msg1;
    int len1;
    struct UUSOCKETBUFFERS sbs16;

    UUSocketBuffersCopy(&sbs, sbsi);

    /* Here is the aforementioned normalization that happens here since we are the first level
     * handler */
    remain = UUSocketBuffersNormalize(&sbs);

    if (s->utf16 == 0)
        return UURecvZip(s, &sbs, uusflags);

    if (sbs.count > 0) {
        msg1 = sbs.sb[0].msg;
        len1 = sbs.sb[0].len;
    } else {
        msg1 = NULL;
        len1 = 0;
    }

    /* utf16 values
       0  not utf16
       1  detect need to initialize
       2  detecting fffe or feff
       3  wasn't utf16 need to return both bytes at utf16BufferEob
       4  wasn't utf16 need to return the byte at utf16BufferEob[1]
       10 utf16 native
       11 utf16 non-native (need to swap incoming)
    */

    if (s->utf16 < 10) {
        if (s->utf16 == 1) {
            s->utf16BufferEob = 0;
            s->utf16Eof = 0;
            s->utf16 = 2;
        }

        if (s->utf16 == 3) {
            result = UUMIN(len1, 2);
            if (result > 0) {
                memcpy(msg1, s->utf16Buffer, result);
                s->utf16 = result == 2 ? 0 : 4;
            }
            return result;
        }

        if (s->utf16 == 4) {
            result = UUMIN(len1, 1);
            if (result > 0) {
                memcpy(msg1, s->utf16Buffer + 1, 1);
                s->utf16 = 0;
            }
            return result;
        }

        for (; s->utf16BufferEob < 2;) {
            sbs16.count = 1;
            sbs16.sb[0].msg = s->utf16Buffer + s->utf16BufferEob;
            sbs16.sb[0].len = 2 - s->utf16BufferEob;
            result = UURecvZip(s, &sbs16, uusflags);
            /* result = UURecv2Chunking(s, s->utf16Buffer + s->utf16BufferEob, 2 -
             * s->utf16BufferEob, NULL, 0, uusflags); */
            if (result <= 0)
                return result;
            s->utf16BufferEob += result;
        }

        if ((s->utf16Buffer[0] == 0xff && s->utf16Buffer[1] == 0xfe) ||
            (s->utf16Buffer[0] == 0xfe && s->utf16Buffer[1] == 0xff)) {
            union {
                struct {
                    unsigned char b0;
                    unsigned char b1;
                } b;
                unsigned short s0;
            } endian;
            endian.b.b0 = s->utf16Buffer[0];
            endian.b.b1 = s->utf16Buffer[1];
            s->utf16 = endian.s0 == 0xfeff ? 10 : 11;
            s->utf16BufferEob = 0;
        } else {
            if (len1 > 1) {
                s->utf16 = 0;
                memcpy(msg1, s->utf16Buffer, 2);
                return 2;
            }
            if (len1 == 1) {
                s->utf16 = 4;
                memcpy(msg1, s->utf16Buffer, 1);
                return 1;
            }
            s->utf16 = 3;
            return 0;
        }
    }

    totalResult = 0;

    for (; len1 > 0;) {
        if (s->utf16BufferEob > 1) {
            start = s->utf16Buffer;
            /* Use >> 1, << 1 to hide any final odd byte */
            end = s->utf16Buffer + ((s->utf16BufferEob >> 1) << 1);
            msg1End = msg1;
            cr = ConvertUTF16toUTF8((const UTF16 **)&start, (const UTF16 *)end, (UTF8 **)&msg1End,
                                    (UTF8 *)msg1 + len1, lenientConversion);
            converted = (msg1End - (char *)msg1);
            len1 -= converted;
            totalResult += converted;
            msg1 = msg1End;

            /* sourceIllegal result should not come back since we are performing lenient conversion
             */

            /* Computer how much left over in source buffer;
               the match is slightly strange to account for case when
               source has odd number of bytes
            */
            if ((s->utf16BufferEob = (s->utf16Buffer + s->utf16BufferEob) - start))
                memcpy(s->utf16Buffer, start, s->utf16BufferEob);

            if (cr == targetExhausted || len1 == 0)
                break;
        }

        if (s->utf16Eof)
            break;

        sbs16.count = 1;
        sbs16.sb[0].msg = s->utf16Buffer + s->utf16BufferEob;
        sbs16.sb[0].len = sizeof(s->utf16Buffer) - s->utf16BufferEob;
        result = UURecvZip(s, &sbs16, uusflags);
        /* result = UURecv2Chunking(s, s->utf16Buffer + s->utf16BufferEob, sizeof(s->utf16Buffer) -
         * s->utf16BufferEob, NULL, 0, uusflags); */

        /* need to swap bytes here when relevant */

        if (result < 0) {
            if (totalResult)
                return totalResult;
            else
                return result;
        }

        if (result == 0)
            s->utf16Eof = 1;
        else {
            if (s->utf16 == 11) {
                /* Swap the bytes just-received around to address endian issues */
                /* We may receive a single byte leaving us one byte less than a short,
                   so we adapt to insure it will be held around for the next time
                   we receive
                */
                start = s->utf16Buffer + s->utf16BufferEob;
                /* If the start is on an odd-byte, we really need to start one byte back */
                if (s->utf16BufferEob & 1)
                    start--;
                end = s->utf16Buffer + s->utf16BufferEob + result;
                /* If the end is on an odd byte, we really need to end one byte back */
                if ((s->utf16BufferEob + result) & 1)
                    end--;
                for (; start < end; start += 2) {
                    unsigned char save = *start;
                    *start = *(start + 1);
                    *(start + 1) = save;
                }
            }
            s->utf16BufferEob += result;
        }
    }

    return totalResult;
}

int UURecv2(struct UUSOCKET *s, void *msg1, size_t len1, void *msg2, size_t len2, int uusflags) {
    struct UUSOCKETBUFFERS sbs;

    sbs.count = 0;
    if (msg1 && len1) {
        sbs.sb[sbs.count].msg = msg1;
        sbs.sb[sbs.count].len = len1;
        sbs.count++;
    }
    if (msg2 && len2) {
        sbs.sb[sbs.count].msg = msg2;
        sbs.sb[sbs.count].len = len2;
        sbs.count++;
    }

    return UURecvUUSocketBuffers(s, &sbs, uusflags);
}

/**
 * Read from a socket some length at a time
 * @param UUSOCKET *s The socket
 * @param void *msg A pointer to some type to write msg to
 * @param site_t len The amound of data to read off socket
 * @param int uusflags Don't know what this is, use 0
 * @return int While there is remaining data 1, not 1 otherwise
 */
int UURecv(struct UUSOCKET *s, void *msg, size_t len, int uusflags) {
    struct UUSOCKETBUFFERS sbs;

    if (msg && len) {
        sbs.sb[0].msg = msg;
        sbs.sb[0].len = len;
        sbs.count = 1;
    } else {
        sbs.count = 0;
    }

    return UURecvUUSocketBuffers(s, &sbs, uusflags);
}

int UUSendMemory(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    int result;
    int totalResult = 0;
    const struct SOCKETBUFFER *sbp;

    if (s->mbAccess == STMEMORYREAD)
        return UUSocketBuffersTotalLen(sbsi);

    for (sbp = sbsi->sb + sbsi->count - 1; sbp >= sbsi->sb; sbp--) {
        result = MemoryBufferWrite(s->mb, sbp->msg, sbp->len);
        if (result < 0)
            return result;
        totalResult += result;
    }
    return totalResult;
}

int UUSendStream(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    int status;
    int buffs;
    size_t remain;
    int totalResult = 0;
    struct SOCKETBUFFER *sbp;
#ifdef WIN32
    DWORD len;
    DWORD flags;
    WSABUF buffptr[MAXUUSOCKETBUFFERS];
#else
    int len;
    struct msghdr mh;
    struct iovec iov[MAXUUSOCKETBUFFERS];
#endif
    struct UUSOCKETBUFFERS sbs;

    UUSocketBuffersCopy(&sbs, sbsi);
    remain = UUSocketBuffersTotalLen(&sbs);

    for (; remain > 0;) {
        if (s->lastSendBlocked) {
            if ((uusflags & UUSNOWAIT) == 0) {
                if (UUWant(s->o, UUWANTSEND, s->timeout)) {
                    s->sendOK = 0;
                    s->savedSendErrno = errno;
                    if (totalResult > 0)
                        break;
                    SetErrno(s->savedSendErrno);
                    return -1;
                }
            }
        }

        buffs = 0;

#ifdef WIN32
        for (sbp = sbs.sb + sbs.count - 1; sbp >= sbs.sb; sbp--) {
            if (sbp->msg && sbp->len) {
                buffptr[buffs].buf = sbp->msg;
                buffptr[buffs++].len = sbp->len;
            }
        }
        if (buffs == 0)
            break;

        flags = 0;
        status = WSASend(s->o, &buffptr[0], buffs, &len, flags, NULL, NULL);
#else
        for (sbp = sbs.sb + sbs.count - 1; sbp >= sbs.sb; sbp--) {
            if (sbp->msg && sbp->len) {
                iov[buffs].iov_base = sbp->msg;
                iov[buffs++].iov_len = sbp->len;
            }
        }
        if (buffs == 0)
            break;

        memset(&mh, 0, sizeof(mh));
        mh.msg_iov = iov;
        mh.msg_iovlen = buffs;
        TEMP_FAILURE_RETRY_INT(status = interrupted_neg_int(sendmsg(s->o, &mh, 0)));
        if (status >= 0) {
            len = status;
            status = 0;
        } else {
            status = -1;
            len = 0;
        }
#endif

        if (status < 0) {
            if (socket_errno == WSAEWOULDBLOCK) {
                s->lastSendBlocked = 1;
                if (uusflags & UUSNOWAIT) {
                    s->lastSendWant = UUWANTSEND;
                    if (totalResult > 0)
                        break;
                    return -1;
                }
                continue;
            }
            s->sendOK = 0;
            s->savedSendErrno = socket_errno;
            if (totalResult > 0)
                break;
            SetErrno(s->savedSendErrno);
            return -1;
        }

        s->lastSendBlocked = 0;

        if (len > 0) {
            UUtime(&s->lastActivity);
            totalResult += len;
            remain -= len;
            s->sendCount += len;
            UUSocketBuffersRemoveSendBytes(&sbs, len);
            break;
        }
    }

    return totalResult;
}

int UUSendRoute(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    s->lastSendWant = 0;

    switch (s->socketType) {
    case STSTREAM:
        return UUSendStream(s, sbsi, uusflags);

    case STSSL:
        return UUSendSsl(s, sbsi, uusflags);

    case STNULL:
        return UUSocketBuffersTotalLen(sbsi);

    case STMEMORY:
        return UUSendMemory(s, sbsi, uusflags);
    }

    Log("Unknown UUSend %d", s->socketType);
    return -1;
}

int UUSendLogging(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    int result;
    size_t remain;
    size_t len;
    const struct SOCKETBUFFER *sbp;

    result = UUSendRoute(s, sbsi, uusflags);

    if (result > 0 && s->logFileDirection != LFNOLOG) {
        if (s->logFileDirection != LFSEND || optionLogSockets == 2) {
            UUOpenLogFile(s, LFSEND);
        }

        for (remain = result, sbp = sbsi->sb + sbsi->count - 1; remain > 0 && sbp >= sbsi->sb;
             sbp--) {
            len = UUMIN(remain, sbp->len);
            if (len) {
                write(s->logFile, sbp->msg, len);
                remain -= len;
            }
        }
    }

    if (result > 0 && s->fiddlerSendFile >= 0) {
        for (remain = result, sbp = sbsi->sb + sbsi->count - 1; remain > 0 && sbp >= sbsi->sb;
             sbp--) {
            len = UUMIN(remain, sbp->len);
            if (len) {
                write(s->fiddlerSendFile, sbp->msg, len);
                remain -= len;
            }
        }
    }

    return result;
}

int UUSendChunking(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    int result;
    int totalResult;
    size_t remain, remain2;
    const struct SOCKETBUFFER *sbpAll;
    struct SOCKETBUFFER *sbpChunk;
    struct UUSOCKETBUFFERS sbsChunk;
    size_t headerLen = 0, headerLen2 = 0;
    char sendChunkHeader2[16];
    size_t limit;

    if (s->sendChunking == 0) {
        return UUSendLogging(s, sbsi, uusflags);
    }

    remain = UUSocketBuffersTotalLen(sbsi);

    sbpChunk = sbsChunk.sb + MAXUUSOCKETBUFFERS;
    sbpAll = sbsi->sb + sbsi->count - 1;

    if (s->sendChunkRemain == 0) {
        s->sendChunkRemain = remain;
        if (s->sendChunking == 1) {
            // C99 allows %zx to be used with a size_t variable, but
            // the MinGW compiler used for EZproxy does not support this
            // convention so use the C89 compatible version of %lx with
            // a cast of the size_t variable to (unsigned long)
            sprintf(s->sendChunkHeader, "%lx\r\n", (unsigned long)remain);
            s->sendChunking = 2;
        } else {
            /* Add the CRLF that should follow the last chunk */
            // See comment about about %lx
            sprintf(s->sendChunkHeader, "\r\n%lx\r\n", (unsigned long)remain);
        }
    }

    if (s->sendChunkHeader[0]) {
        sbpChunk--;
        sbpChunk->msg = s->sendChunkHeader;
        sbpChunk->len = headerLen = strlen(s->sendChunkHeader);
    }

    remain2 = remain - s->sendChunkRemain;
    sendChunkHeader2[0] = 0;

    for (limit = s->sendChunkRemain; sbpAll >= sbsi->sb; sbpAll--) {
        if (sbpAll->len == 0)
            continue;
        if (limit >= sbpAll->len) {
            sbpChunk--;
            sbpChunk->msg = sbpAll->msg;
            sbpChunk->len = sbpAll->len;
            limit -= sbpAll->len;
            continue;
        }

        if (limit > 0) {
            sbpChunk--;
            sbpChunk->msg = sbpAll->msg;
            sbpChunk->len = limit;
        }

        /* For SSL, the data stream sent may grow, but may not change.
           The secondary length may well change, which would alter what is sent,
           so if this is an SSL connection, don't try to send the secondary
           length.
        */
        if (s->socketType == STSSL)
            break;

        if (remain2 > 0) {
            sprintf(sendChunkHeader2, "\r\n%zx\r\n", remain2);
            sbpChunk--;
            sbpChunk->msg = sendChunkHeader2;
            sbpChunk->len = headerLen2 = strlen(sendChunkHeader2);
            if (debugLevel > 50) {
                Log("headerlen2 %d %d", headerLen2, remain2);
            }
            sbpChunk--;
            sbpChunk->msg = (char *)sbpAll->msg + limit;
            sbpChunk->len = sbpAll->len - limit;
            sbpAll--;
            for (; sbpAll >= sbsi->sb; sbpAll--) {
                if (sbpAll->len == 0)
                    continue;
                sbpChunk--;
                sbpChunk->msg = sbpAll->msg;
                sbpChunk->len = sbpAll->len;
            }
        }
        break;
    }

    if ((sbsChunk.count = (sbsChunk.sb + MAXUUSOCKETBUFFERS) - sbpChunk))
        memcpy(sbsChunk.sb, sbpChunk, sbsChunk.count * sizeof(*sbpChunk));

    result = UUSendLogging(s, &sbsChunk, uusflags);
    if (result < 0) {
        return result;
    }

    if (s->sendChunkHeader[0]) {
        if ((size_t)result < headerLen) {
            StrCpyOverlap(s->sendChunkHeader, s->sendChunkHeader + result);
            result = 0;
        } else {
            s->sendChunkHeader[0] = 0;
            result -= headerLen;
        }
    }

    if ((size_t)result <= s->sendChunkRemain) {
        s->sendChunkRemain -= result;
        totalResult = result;
    } else {
        totalResult = s->sendChunkRemain;
        result -= s->sendChunkRemain;

        if ((size_t)result < headerLen2) {
            StrCpyOverlap(s->sendChunkHeader, sendChunkHeader2 + result);
            s->sendChunkRemain = remain2;
            result = 0;
        } else {
            s->sendChunkHeader[0] = 0;
            result -= headerLen2;
            s->sendChunkRemain -= remain2 - result;
        }

        totalResult += result;
    }

    return totalResult;
}

int UUSendZStream(struct UUSOCKET *s, int uusflags) {
    struct UUSOCKETBUFFERS sbsZip;
    int result;

    sbsZip.count = 1;
    sbsZip.sb[0].msg = s->sendZipBuffer;
    sbsZip.sb[0].len = sizeof(s->sendZipBuffer) - s->sendZStream.avail_out;
    result = UUSendChunking(s, &sbsZip, uusflags);
    if (result > 0) {
        int remain = sbsZip.sb[0].len - result;
        if (remain > 0) {
            memcpy(s->sendZipBuffer, s->sendZipBuffer + result, remain);
        }
        s->sendZStream.avail_out = sizeof(s->sendZipBuffer) - remain;
        s->sendZStream.next_out = (Bytef *)s->sendZipBuffer + remain;
        s->sendZipBytesSinceLastSend = 0;
    }
    return result;
}

int UUSendZip(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    struct SOCKETBUFFER *sbp;
    struct UUSOCKETBUFFERS sbs;
    int result;
    int zResult;
    int totalResult = 0;
    int remain;
    BOOL flushing;

    if (s->sendZip == 0) {
        return UUSendChunking(s, sbsi, uusflags);
    }

    UUSocketBuffersCopy(&sbs, sbsi);

    uusflags = uusflags & ~UUSNOWAIT;

    UUSocketBuffersCopy(&sbs, sbsi);

    if (s->sendZip == 1) {
        s->sendZStream.zalloc = Z_NULL;
        s->sendZStream.zfree = Z_NULL;
        s->sendZStream.opaque = Z_NULL;
        /* We use reduced values for window size and memory level since we are forcing a buffer sync
        every 4k, in which case the larger values don't buy us anything except much larger memory
        footprint
        */
        /* zResult = deflateInit2(&s->sendZStream, optionAllowSendGzip, Z_DEFLATED, MAX_WBITS+16, 8,
         * Z_DEFAULT_STRATEGY); */
        zResult = deflateInit2(&s->sendZStream, optionAllowSendGzip, Z_DEFLATED, 12 + 16, 5,
                               Z_DEFAULT_STRATEGY);
        if (zResult != Z_OK) {
            Log("defaultInit2 failed %d", zResult);
            PANIC;
        }
        s->sendZip = 2;
        s->sendZipBytesSinceLastSend = 0;
        s->sendZStream.avail_out = sizeof(s->sendZipBuffer);
        s->sendZStream.next_out = (Bytef *)s->sendZipBuffer;
    }

    for (sbp = sbs.sb + sbs.count - 1; sbp >= sbs.sb; sbp--) {
        s->sendZStream.next_in = sbp->msg;
        s->sendZStream.avail_in = sbp->len;
        s->sendZipBytesSinceLastSend += sbp->len;

        for (; s->sendZStream.avail_in > 0;) {
            remain = s->sendZStream.avail_in;
            zResult = deflate(&s->sendZStream, Z_NO_FLUSH);
            if (zResult == Z_STREAM_ERROR)
                PANIC;
            totalResult = remain - s->sendZStream.avail_in;
            if (s->sendZStream.avail_out == 0) {
                result = UUSendZStream(s, uusflags);
                if (result < 0) {
                    if (totalResult > 0)
                        return totalResult;
                    return result;
                }
            }
        }
    }

    if ((flushing = (uusflags & UUSFLUSH) != 0) || (0 && s->sendZipBytesSinceLastSend > 4096)) {
        if (s->sendZipBytesSinceLastSend > 4096) {
            s->sendZipBytesSinceLastSend = 0;
            dprintf("more than 4k\n");
        }
        s->sendZStream.next_in = NULL;
        s->sendZStream.avail_in = 0;
        for (; s->sendZStream.avail_out < sizeof(s->sendZipBuffer);) {
            zResult = deflate(&s->sendZStream, Z_SYNC_FLUSH);
            if (zResult == Z_STREAM_ERROR)
                PANIC;
            result = UUSendZStream(s, uusflags);
            if (result < 0) {
                if (totalResult > 0)
                    return totalResult;
                return result;
            }
            if (!flushing)
                break;
        }
    }

    return totalResult;
}

/* UUSendBuffering is also UUSendUUSocketBuffers by macro, making it the first in
   the chain of send functions.  As such, it is also responsible for verifying sendOK
   and for normalizing the first copy of UUSOCKETBUFFERS so nothing deeper has to
   do any of these checks
*/
int UUSendBuffering(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    size_t remain;
    int result;
    struct SOCKETBUFFER *sbp;
    int totalResult = 0;
    struct UUSOCKETBUFFERS sbs;

    /* The sendOK check should always be part of the first level handler */
    if (s->sendOK == 0) {
        /* Make sure no one thinks we're buffering anything any more */
        s->sendBufferEob = 0;
        s->sendZip = 0;

        SetErrno(s->savedSendErrno);
        s->savedSendErrno = EINVAL;
        return -1;
    }

    UUSocketBuffersCopy(&sbs, sbsi);

    /* Here is the aforementioned normalization that happens here since we are the first level
     * handler */
    remain = UUSocketBuffersNormalize(&sbs);

    if (s->sendZip) {
        return UUSendZip(s, &sbs, uusflags);
    }

    for (; remain > 0 || s->sendBufferEob > 0;) {
        /* We don't buffer on NOWAIT since NOWAIT usually implies someone's trying for partial write
           and tracking the results.
           If FLUSH is present, we have to clear any buffer, so don't build up more.
        */
        if ((uusflags & (UUSFLUSH | UUSNOWAIT)) == 0) {
            size_t bufferRemain;
            /* If everything is already buffered, we're done */
            if (remain == 0)
                break;

            bufferRemain = sizeof(s->sendBuffer) - s->sendBufferEob;
            /* If we have enough room, buffer remaining and leave here */
            if (bufferRemain >= remain) {
                char *pBuffer = s->sendBuffer + s->sendBufferEob;

                for (sbp = sbs.sb + sbs.count - 1; sbp >= sbs.sb; sbp--) {
                    if (sbp->len > 0) {
                        memmove(pBuffer, sbp->msg, sbp->len);
                        s->sendBufferEob += sbp->len;
                        pBuffer += sbp->len;
                        totalResult += sbp->len;
                    }
                }
                break;
            }
        }

        /* If nothing buffered, just pass along what we got */
        if (s->sendBufferEob == 0) {
            result = UUSendZip(s, &sbs, uusflags);
        } else {
            /* Handle when we have buffered content */
            sbs.sb[sbs.count].msg = s->sendBuffer;
            sbs.sb[sbs.count].len = s->sendBufferEob;
            sbs.count++;
            result = UUSendZip(s, &sbs, uusflags);
            sbs.count--;
            if (result > 0) {
                /* We sent part of the buffer, none of the unbuffered, so shift */
                if ((size_t)result < s->sendBufferEob) {
                    s->sendBufferEob -= result;
                    memcpy(s->sendBuffer, s->sendBuffer + result, s->sendBufferEob);
                    result = 0;
                } else {
                    /* We sent the entire buffer; reset and adjust result amount */
                    result = result - s->sendBufferEob;
                    s->sendBufferEob = 0;
                }
            }
        }

        if (result < 0) {
            if (totalResult > 0)
                break;
            return result;
        }

        UUSocketBuffersRemoveSendBytes(&sbs, result);

        totalResult += result;
        if ((size_t)result <= remain)
            remain -= result;
        else {
            Log("*** result > remain? %d %u", result, remain);
            remain = 0;
        }
    }

    return totalResult;
}

int UUSend2(struct UUSOCKET *s, void *msg1, size_t len1, void *msg2, size_t len2, int uusflags) {
    struct UUSOCKETBUFFERS sbs;

    sbs.count = 0;
    if (msg2 && len2) {
        sbs.sb[sbs.count].msg = msg2;
        sbs.sb[sbs.count].len = len2;
        sbs.count++;
    }
    if (msg1 && len1) {
        sbs.sb[sbs.count].msg = msg1;
        sbs.sb[sbs.count].len = len1;
        sbs.count++;
    }

    return UUSendUUSocketBuffers(s, &sbs, uusflags);
}

int UUSend(struct UUSOCKET *s, const void *msg, size_t len, int uusflags) {
    struct UUSOCKETBUFFERS sbs;

    if (msg && len) {
        sbs.sb[0].msg = (void *)msg;
        sbs.sb[0].len = len;
        sbs.count = 1;
    } else {
        sbs.count = 0;
    }

    return UUSendUUSocketBuffers(s, &sbs, uusflags);
}

int UUSendCRLF(struct UUSOCKET *s) {
    const static struct UUSOCKETBUFFERS sbs = {
        1, {"\r\n", 2}
    };

    return UUSendUUSocketBuffers(s, &sbs, 0);
}

int UUSendHR(struct UUSOCKET *s) {
    const static struct UUSOCKETBUFFERS sbs = {
        1, {"<hr />\n", 7}
    };

    return UUSendUUSocketBuffers(s, &sbs, 0);
}

int UUSendZ(struct UUSOCKET *s, const char *str) {
    struct UUSOCKETBUFFERS sbs;

    sbs.sb[0].msg = (void *)str;
    sbs.sb[0].len = strlen(str);
    sbs.count = 1;

    return UUSendUUSocketBuffers(s, &sbs, 0);
}

int UUSendZCRLF(struct UUSOCKET *s, const char *str) {
    struct UUSOCKETBUFFERS sbs;

    sbs.sb[0].msg = "\r\n";
    sbs.sb[0].len = 2;

    sbs.sb[1].msg = (void *)str;
    sbs.sb[1].len = strlen(str);

    sbs.count = 2;

    return UUSendUUSocketBuffers(s, &sbs, 0);
}

int UUFlush(struct UUSOCKET *s) {
    struct UUSOCKETBUFFERS sbs = {0};

    return UUSendUUSocketBuffers(s, &sbs, UUSFLUSH);
}

void UUSendZipEnd(struct UUSOCKET *s, int uusflags) {
    int result;
    int zResult;

    if (s->sendZip) {
        if (uusflags & UUSSTOPNICE) {
            if (s->sendZip == 2) {
                s->sendZStream.avail_in = 0;
                s->sendZStream.next_in = NULL;
                for (;;) {
                    zResult = deflate(&s->sendZStream, Z_FINISH);
                    if (zResult == Z_STREAM_ERROR)
                        PANIC;
                    if (s->sendZStream.avail_out < sizeof(s->sendZipBuffer)) {
                        result = UUSendZStream(s, 0);
                        if (result < 0)
                            break;
                    }
                    if (zResult == Z_STREAM_END)
                        break;
                    if (zResult != Z_OK)
                        PANIC;
                }
            }
        }
        deflateEnd(&s->sendZStream);
        s->sendZip = 0;
    }
}

void UUSendChunkingEnd(struct UUSOCKET *s) {
    if (s->sendChunking) {
        /* Note that the very last \r\n actually comes after any trailing headers
           but since we aren't forwarding trailing headers at this point, we
           need it there as the final indication that this HTTP message is over
        */
        char *stop = "\r\n0\r\n\r\n";
        UUFlush(s);
        /* If we never sent anything, don't need initial \r\n so skip over it */
        if (s->sendChunking == 1)
            stop += 2;
        s->sendChunking = 0;
        UUSendZ(s, stop);
    }
}

void UUSendReset(struct UUSOCKET *s, int uusflags) {
    UUSendZipEnd(s, uusflags);

    if (uusflags & UUSSTOPNICE) {
        UUSendChunkingEnd(s);
    }
    UUFlush(s);
}

void UURecvReset(struct UUSOCKET *s, int uusflags) {
    /* This is also called by UURecvMarkRewind,
       so cross-check any changes if needed
    */
    if (s->recvZip == UURECVZIPINITDEFLATE || s->recvZip == UURECVZIPPROCESSING) {
        inflateEnd(&s->recvZStream);
    }

    s->recvZip = UURECVZIPNONE;
    s->recvZEof = 0;

    s->recvChunking = 0;
}

int UUStopSocket(struct UUSOCKET *s, int uusflags) {
    struct STOPSOCKET *ss;
    int i = 0;

    if (s->o == INVALID_SOCKET)
        return 0;

    UUSendReset(s, uusflags);
    UURecvReset(s, uusflags);

    if (s->logFile != -1) {
        UUOpenLogFile(s, LFCLOSE);
        UUcloseFD(s->logFile);
        s->logFile = -1;
    }

    if (s->fiddlerRecvFile != -1) {
        UUcloseFD(s->fiddlerRecvFile);
        s->fiddlerRecvFile = -1;
    }

    if (s->fiddlerSendFile != -1) {
        UUcloseFD(s->fiddlerSendFile);
        s->fiddlerSendFile = -1;
    }

    if (!(ss = (struct STOPSOCKET *)calloc(1, sizeof(struct STOPSOCKET))))
        PANIC;
    ss->hasStopped = (UUShutdown(s, SHUT_WR) == 0);
    ss->socket = s->o;
    ss->socketType = s->socketType;
    ss->ssl = s->ssl;
    ss->sslShutdowns = 0;
    UUtime(&ss->lastActivity);
    ss->expires =
        ss->lastActivity + (uusflags & UUSSTOPNICE ? 30 : (ss->socketType == STSSL ? 5 : 1));
    UUAcquireMutex(&mStopSockets);
    ss->next = stopSockets;
    stopSockets = ss;
    if (debugLevel > 19) {
        for (; ss != NULL; i++, ss = ss->next)
            ;
    }
    UUReleaseMutex(&mStopSockets);
    if (debugLevel > 19) {
        Log("%" SOCKET_FORMAT " UUStopSocket enqueued, queue length %d", s->o, i);
    }
    TSStopSocket(s);
    s->o = INVALID_SOCKET;
    s->recvOK = s->sendOK = 0;
    s->savedSendErrno = EINVAL;
    s->savedRecvErrno = EINVAL;
    return 0;
}

int UUStopNormalSocket(SOCKET s) {
    struct UUSOCKET us;

    UUInitSocket(&us, s);
    return UUStopSocket(&us, 0);
}

void UURecvMarkSet(struct UUSOCKET *s) {
    int len;

    len = s->recvBufferEob - s->recvBufferOfs;
    if (len > 0) {
        /* If we've been receiving from part-way into the buffer, shift it to the front
           to make as much room as available.  This also makes it possible to rewind
           by joint point s->recvBufferOfs back to 0
        */
        if (s->recvBufferOfs > 0) {
            memcpy(s->recvBuffer, s->recvBuffer + s->recvBufferOfs, len);
            s->recvBufferOfs = 0;
            s->recvBufferEob = len;
        }
    } else {
        s->recvBufferOfs = s->recvBufferEob = 0;
    }

    s->recvMarkChunking = s->recvChunking;
    s->recvMarkChunkRemain = s->recvChunkRemain;
    s->recvMarkUtf16 = s->utf16;
    s->recvMarkSet = 1;
}

void UURecvMarkRewind(struct UUSOCKET *s) {
    if (s->recvMarkSet) {
        /* Take us back to the beginning of the buffer */
        s->recvBufferOfs = 0;
        /* This will clear up any zip processing */
        UURecvReset(s, 0);
        /* Reset chunking and utf16 */
        s->recvChunking = s->recvMarkChunking;
        s->recvChunkRemain = s->recvMarkChunkRemain;
        s->utf16 = s->recvMarkUtf16;
        s->recvMarkSet = 0;
    }
}

BOOL UUValidSocketRecv(struct UUSOCKET *s) {
    if (s->recvOK == 0 && s->recvBufferEob == s->recvBufferOfs)
        return 0;

    switch (s->socketType) {
    case STSSL:
        return s->ssl != NULL && s->o != INVALID_SOCKET;

    case STSTREAM:
        return s->o != INVALID_SOCKET;

    case STMEMORY:
        return (s->mb != NULL) && ((s->mb)->reallocFailed == 0);

    case STNULL:
        return 1;
    }

    return 0;
}

BOOL UUSocketEof(struct UUSOCKET *s) {
    /* If the socket is no longer valid, claim EOF */
    if (!UUValidSocketRecv(s))
        return 1;

    /* If data is waiting, we are not EOF */
    if (UURecvDataWaiting(s))
        return 0;

    /* Otherwise, if the EOF flag is set or if we are at a keep-alive EOF, claim EOF */
    return s->recvEof || s->recvChunking == 99;
}

BOOL UUValidSocketSend(struct UUSOCKET *s) {
    if (s->sendOK == 0)
        return 0;

    switch (s->socketType) {
    case STSSL:
        return s->ssl != NULL && s->o != INVALID_SOCKET;

    case STSTREAM:
        return s->o != INVALID_SOCKET;

    case STMEMORY:
        return (s->mb != NULL) && ((s->mb)->reallocFailed == 0);

    case STNULL:
        return 1;
    }

    return 0;
}

int UUShutdown(struct UUSOCKET *s, int how) {
    int rc;

    if (s->socketType == STSSL)
        return 1;

    if (s->socketType != STSTREAM)
        return 0;

    if (s->o == INVALID_SOCKET) {
        SetErrno(WSAENOTSOCK); /* invalid socket */
        return -1;
    }

    if (how == SHUT_RDWR && s->shut_rd)
        how = SHUT_WR;

    if (how == SHUT_WR && s->shut_wr)
        return 0;

    if (how == SHUT_RD && s->shut_rd)
        return 0;

    rc = shutdown(s->o, how);

    if (debugLevel > 19)
        Log("%" SOCKET_FORMAT " Shutdown socket, rc=%d", s->o, rc);

    if (rc == 0) {
        if (how != SHUT_WR)
            s->shut_rd = 1;
        if (how != SHUT_RD)
            s->shut_wr = 1;
    }

    return rc;
}

void DoStopSockets(void *v) {
    struct STOPSOCKET *ss, *ssnext, *sslast, *ssstart;
    int status1, status2;
    time_t now;
    char dummy[256];
    int status;
    BOOL anyRead;
    BOOL anyDelete;
    BOOL clearErrors = 0;
    /*
    struct linger linger;
    */
    static int maxQueueDepth = 0;
    int queueDepth, j, k;

    queueDepth = 0;
    for (anyRead = 1;;) {
        ChargeAlive(GCSTOPSOCKETS, &now);
        guardian->stopSocketsLocation = 0;
        if (clearErrors) {
            ERR_clear_error();
            clearErrors = 0;
        }
        ticksStopSockets++;
        if (anyRead) {
            guardian->stopSocketsLocation = 1;
            anyRead = 0;
        } else {
            guardian->stopSocketsLocation = 2;
            SetErrno(0);
            SubSleep(0, 100);
#ifndef WIN32
            if ((errno == ECANCELED)) {
                break;
            }
#endif
        }
        guardian->stopSocketsLocation = 222;
        UUAcquireMutex(&mStopSockets);
        guardian->stopSocketsLocation = 3;
        ss = ssstart = stopSockets;
        if (queueDepth > maxQueueDepth) {
            maxQueueDepth = queueDepth;
            /* Use this as a flag to report the new depth after mutex released */
            queueDepth = -1;
        }
        UUReleaseMutex(&mStopSockets);
        guardian->stopSocketsLocation = 4;
        if (debugLevel > 1 && queueDepth == -1) {
            Log("stopsocket, maxQueueDepth %d", maxQueueDepth);
        }
        for (anyDelete = 0, queueDepth = 0; ss != NULL; ss = ss->next, queueDepth++) {
            guardian->stopSocketsLocation = 5;
            if (ss->socket == INVALID_SOCKET)
                continue;
            if (ss->expires > now && ss->lastActivity + 3 > now) {
                guardian->stopSocketsLocation = 6;
                if (ss->socketType == STSSL) {
                    guardian->stopSocketsLocation = 7;
                    if (ss->ssl) {
                        guardian->stopSocketsLocation = 8;
                        status1 = SSL_shutdown(ss->ssl);
                        if (status1 <= 0 && ss->lastActivity + 30 > now) {
                            guardian->stopSocketsLocation = 9;
                            clearErrors = 1;
                            status2 = SSL_get_error(ss->ssl, status1);
                            if (status2 == SSL_ERROR_WANT_READ || status2 == SSL_ERROR_WANT_WRITE)
                                continue;
                            guardian->stopSocketsLocation = 10;
                            if (status1 == 0) {
                                guardian->stopSocketsLocation = 11;
                                /* One reference suggests that the socket should be manually shut
                                   down after a 0 return to avoid a problem with some Netscape
                                   browser implementations */
                                if ((ss->sslShutdowns)++ == 0) {
                                    guardian->stopSocketsLocation = 12;
                                    ss->hasStopped = shutdown(ss->socket, SHUT_WR) == 0;
                                }
                                /* Another suggests that it takes as many as four tries to get a
                                 * shutdown to really happen */
                                if ((ss->sslShutdowns)++ < 4) {
                                    guardian->stopSocketsLocation = 13;
                                    continue;
                                }
                            }
                            /* Barring those tries, give up and shut it down the hard way */
                        }
                        guardian->stopSocketsLocation = 14;
                        ss->lastActivity = now;
                        ERR_clear_error();
                        SSL_free(ss->ssl);
                        ERR_clear_error();
                        ss->ssl = NULL;
                    }
                    guardian->stopSocketsLocation = 15;
                    /* Since SSL processing is done, now treat like a stream socket for rest of
                     * shutdown */
                    ss->socketType = STSTREAM;
                }
                if (ss->hasStopped == 0 && shutdown(ss->socket, SHUT_WR) == 0) {
                    guardian->stopSocketsLocation = 16;
                    ss->hasStopped = 1;
                    ss->lastActivity = now;
                    if (debugLevel > 19)
                        Log("%d Deferred shutdown", ss->socket);
                }
                if (ss->hasEOF == 0) {
                    guardian->stopSocketsLocation = 17;
                    /*
                     * On Windows in the debugger, this function can trigger the error
                     * warning: Invalid parameter passed to C runtime function.
                     * to appear in the gdb window.  End-users do not see this, but
                     * it can be confusing when debugging EZproxy.
                     */
                    status = UUreadFD(ss->socket, &dummy, sizeof(dummy));
                    /* This means there was nothing to read, but we're not at EOF yet */
                    if (status < 0 && socket_errno == EAGAIN)
                        continue;
                    if (status > 0) {
                        guardian->stopSocketsLocation = 18;
                        ss->lastActivity = now;
                        anyRead = 1;
                        continue;
                    }
                    guardian->stopSocketsLocation = 19;
                    /* We flag EOF either on true EOF (status == 0) or an unknown read error */
                    ss->hasEOF = 1;
                }
                if (ss->hasStopped == 0 || ss->hasEOF == 0) {
                    guardian->stopSocketsLocation = 20;
                    continue;
                }
            }

            /* Now we get the graceless-death, be sure to clean up if SSL */
            if (ss->socketType == STSSL && ss->ssl) {
                guardian->stopSocketsLocation = 21;
                ERR_clear_error();
                SSL_free(ss->ssl);
                ERR_clear_error();
                ss->ssl = NULL;
                ss->socketType = STSTREAM;
            }

            /* LINGER disabled since it was driving UMich crazy, and seems irrelevant now */
            /* Force the close operation to be a graceful close that will return immediately
            linger.l_onoff = 0;
            linger.l_linger = 0;
            setsockopt(ss->socket, SOL_SOCKET, SO_LINGER, (char *) &linger, sizeof(linger));
            */

            if (closesocket(ss->socket) != 0 || debugLevel > 19) {
                Log("%" SOCKET_FORMAT " Closesocket returned %d", ss->socket, socket_errno);
            }
            guardian->stopSocketsLocation = 22;
            TSCloseSocket(ss->socket);
            guardian->stopSocketsLocation = 23;
            /* Note that this node needs to be deleted after all closing is done */
            ss->socket = INVALID_SOCKET;
            anyDelete = 1;
        }
        if (anyDelete) {
            guardian->stopSocketsLocation = 24;
            /* We delete any socket == INVALID_SOCKET separately to minimize the time we hold the
             * mutex */
            UUAcquireMutex(&mStopSockets);
            guardian->stopSocketsLocation = 224;
            for (sslast = NULL, ss = stopSockets, j = 0, k = 0; ss != NULL; ss = ssnext, j++) {
                guardian->stopSocketsLocation = 25;
                ssnext = ss->next;
                if (ss->socket != INVALID_SOCKET)
                    sslast = ss;
                else {
                    if (sslast)
                        sslast->next = ss->next;
                    else
                        stopSockets = ss->next;
                    FreeThenNull(ss);
                    k++;
                }
            }
            UUReleaseMutex(&mStopSockets);
            if (j > 0 && debugLevel > 19) {
                Log("dequeue stopped sockets, sockets reviewed %d, sockets removed %d", j, k);
            }
            guardian->stopSocketsLocation = 26;
        }
        guardian->stopSocketsLocation = 27;
    }
    guardian->stopSocketsLocation = 28;
    ChargeStopped(GCSTOPSOCKETS, NULL);
}

/* Special case: if stopTime is less than 3600, treat as relative to now instead of absolute time */
int UUWant(SOCKET o, int want, time_t stopTime) {
    int status;
    int timeout;
#ifdef USEPOLL
    struct pollfd pfds;
    int pWant;
#else
    fd_set fds;
    struct timeval tv;
#endif
    time_t now;

    if (want != UUWANTRECV && want != UUWANTSEND) {
        SetErrno(EINVAL); /* Nothing is wanted, invalid.  */
        return -1;
    }

    UUtime(&now);

    if (stopTime < 3600) {
        stopTime += now;
    }

    do {
        timeout = (stopTime > now ? stopTime - now : 0);
#ifdef USEPOLL
        pfds.fd = o;
        pWant = (want == UUWANTRECV ? POLLIN : POLLOUT);
        pfds.events = pWant;
        TEMP_FAILURE_RETRY_INT(status = interrupted_neg_int(poll(&pfds, 1, timeout * 1000)));
        if (status < 0) {
            return -1;
        }
        if (status > 0 && pfds.revents & pWant)
            return 0;
        SetErrno(WSAEWOULDBLOCK);
        return -1;
#else
        FD_ZERO(&fds);
        FD_SET(o, &fds);
        tv.tv_sec = timeout;
        tv.tv_usec = 0;
        TEMP_FAILURE_RETRY_INT(
            status = interrupted_neg_int(select(o + 1, (want == UUWANTRECV ? &fds : NULL),
                                                (want == UUWANTSEND ? &fds : NULL), NULL, &tv)));
        if (status < 0) {
            return -1;
        }
        if (status > 0)
            return 0;
        SetErrno(WSAEWOULDBLOCK);
        return -1;
#endif
    } while (timeout != 0 && (UUtime(&now), now < stopTime));
    /*
     * EINVAL: The sum of the iov_len values overflows a ssize_t,
     * or the MSG_OOB flag is set and no out-of-band data is available.
     * or attempt to read past EOF, or timeout, invalid socket state.
     */
    SetErrno(WSAETIMEDOUT); /* Nothing is wanted, invalid.  */
    return -1;
}

int UUPoll(struct UUPOLLFD *fds, int nfds, int timeout) {
#ifdef USEPOLL
    return poll(fds, nfds, timeout);
#else
    fd_set rfds, wfds;
    struct timeval tv;
    SOCKET max = INVALID_SOCKET;
    int i;
    int status;

    tv.tv_sec = timeout / 1000;
    tv.tv_usec = timeout % 1000;
    FD_ZERO(&rfds);
    FD_ZERO(&wfds);
    for (i = 0; i < nfds; i++) {
        fds[i].revents = 0;
        if (fds[i].events & UUPOLLIN) {
            FD_SET(fds[i].fd, &rfds);
            if (max == INVALID_SOCKET || max < fds[i].fd)
                max = fds[i].fd;
        }
        if (fds[i].events & UUPOLLOUT) {
            FD_SET(fds[i].fd, &wfds);
            if (max == INVALID_SOCKET || max < fds[i].fd)
                max = fds[i].fd;
        }
    }

    if (max == INVALID_SOCKET) {
        /* Windows doesn't under the concept of nothing to do is a timeout, so we help it along */
        SubSleep(tv.tv_sec, tv.tv_usec);
        return 0;
    }

    status = select(max + 1, &rfds, &wfds, NULL, &tv);
    if (status < 0) {
        return status;
    }

    status = 0;
    for (i = 0; i < nfds; i++) {
        fds[i].revents = (FD_ISSET(fds[i].fd, &rfds) ? UUPOLLIN : 0) +
                         (FD_ISSET(fds[i].fd, &wfds) ? UUPOLLOUT : 0);
        if (fds[i].revents)
            status++;
    }

    return status;
#endif
} /* UUPoll() */
