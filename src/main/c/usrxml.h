#ifndef __USRXML_H__
#define __USRXML_H__

#include "common.h"
#include "usr.h"
#include <libxml/xmlschemas.h>
#include <libxml/valid.h>

struct XMLNS {
    struct XMLNS *next;
    char *ns;  /* the name space id */
    char *urn; /* the name its self */
};

struct XMLBINDINGHIERARCHY {
    struct XMLBINDINGHIERARCHY *next; /* The next path binding point in the hierarchy. */
    char *pathExpr;                   /* The path binding point in the hierarchy. */
    int depth;
    int index;
};

struct XMLBINDING {
    struct XMLBINDING *next;
    char *ezpName; /* The EZproxy variable name to which the path hierarchy is to be bound. */
    int xPathDepth;
    struct XMLBINDINGHIERARCHY
        *pathHierarchy; /* The path hierarchy to which the name is to be bound. */
    BOOL varComplexity; /* TRUE means scalar, FALSE means vector */
    BOOL nullable;
    BOOL inDir;
    BOOL outDir;
};

#define XMLCTX_STRUCTUREIDENTIFIER "XMLCTXq"
struct XMLCTX {
    char *inServiceOf;
    xmlDocPtr docIn;
    xmlDocPtr docOut; /* XML-data for a POST */
    xmlXPathContextPtr xpathCtxIn;
    xmlXPathContextPtr xpathCtxOut;
    BOOL newXMLDocOut;
    char *schemaInName;
    char *schemaOutName;
    xmlSchemaPtr schemaIn;
    xmlSchemaPtr schemaOut;
    BOOL schemaInValidation;
    BOOL schemaOutValidation;
    char *dtdInName;
    char *dtdOutName;
    xmlDtdPtr dtdIn;
    xmlDtdPtr dtdOut;
    BOOL dtdInValidation;
    BOOL dtdOutValidation;
    char *url;
    int urlMemMax;
    char *data; /* form-data for a POST */
    int dataMemMax;
    char *contentTypeForPOST;
    char *defaultContentTypeForPOST;
    char *contentTypeForDocument;
    char *defaultContentTypeForDocument;
    char *incomingHeaders; /* a null-string terminated list of "C" strings. */
    char *outgoingHeaders; /* a null-string terminated list of "C" strings. */
    int outgoingHeadersMemMax;
    int sslIdx;
    int sslIdxSave;
    struct sockaddr_storage *pInterface;
    struct XMLNS *xmlNs;
    struct XMLBINDING *bindings;
    BOOL anyOutBindings;
    int triesSave;
    int timeoutSave;
    char structureIdentifier[8];
};

enum FINDUSERRESULT FindUserXML(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                char *file,
                                int f,
                                struct FILEREADLINEBUFFER *frb,
                                struct sockaddr_storage *pInterface);
void UserXMLInit(struct XMLCTX *xmlCtx,
                 struct sockaddr_storage *pInterface,
                 char *defaultContentTypeForPOST,
                 char *defaultContentTypeForDocument,
                 char *inServiceOf);
void UserXMLDestroy(struct XMLCTX *xmlCtx);
void UsrXMLClearXMLOut(struct XMLCTX *xmlCtx);
void UsrXMLClearXMLIn(struct XMLCTX *xmlCtx);
STATEMENT_RESULT_TYPE UsrXMLNewXpathCtx(struct TRANSLATE *t,
                                        struct USERINFO *uip,
                                        struct XMLCTX *xmlCtx,
                                        xmlDocPtr doc,
                                        xmlXPathContextPtr *xpathCtx);
STATEMENT_RESULT_TYPE UsrXMLBinding(struct TRANSLATE *t,
                                    struct USERINFO *uip,
                                    void *context,
                                    char **argValList);
STATEMENT_RESULT_TYPE UsrXMLSchemaValidation(struct TRANSLATE *t,
                                             struct USERINFO *uip,
                                             void *context,
                                             char **argList);
STATEMENT_RESULT_TYPE UsrXMLDTDValidation(struct TRANSLATE *t,
                                          struct USERINFO *uip,
                                          void *context,
                                          char **argList);
STATEMENT_RESULT_TYPE UsrXMLPOSTContentType(struct TRANSLATE *t,
                                            struct USERINFO *uip,
                                            void *context,
                                            char *arg);
STATEMENT_RESULT_TYPE UsrXMLDataStart(struct TRANSLATE *t,
                                      struct USERINFO *uip,
                                      void *context,
                                      char *arg);
STATEMENT_RESULT_TYPE UsrXMLDataCat(struct TRANSLATE *t,
                                    struct USERINFO *uip,
                                    void *context,
                                    char *arg);
STATEMENT_RESULT_TYPE UsrXMLGET(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                void *context,
                                char *arg);
STATEMENT_RESULT_TYPE UsrXMLMsgAuth(struct TRANSLATE *t,
                                    struct USERINFO *uip,
                                    void *context,
                                    char **argList);
struct XMLNS *UsrXMLDeclareNamespace(struct TRANSLATE *t,
                                     struct USERINFO *uip,
                                     void *context,
                                     const char *ns,
                                     const char *urn);
STATEMENT_RESULT_TYPE UsrXMLNamespace(struct TRANSLATE *t,
                                      struct USERINFO *uip,
                                      void *context,
                                      char **argList);
STATEMENT_RESULT_TYPE UsrXMLPOST(struct TRANSLATE *t,
                                 struct USERINFO *uip,
                                 void *context,
                                 char *arg);
STATEMENT_RESULT_TYPE UsrXMLPUT(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                void *context,
                                char *arg);
STATEMENT_RESULT_TYPE UsrXMLURLStart(struct TRANSLATE *t,
                                     struct USERINFO *uip,
                                     void *context,
                                     char *arg);
STATEMENT_RESULT_TYPE UsrXMLURLCat(struct TRANSLATE *t,
                                   struct USERINFO *uip,
                                   void *context,
                                   char *arg);
STATEMENT_RESULT_TYPE UsrXMLConnectionTimeout(struct TRANSLATE *t,
                                              struct USERINFO *uip,
                                              void *context,
                                              char *arg);
STATEMENT_RESULT_TYPE UsrXMLConnectionTries(struct TRANSLATE *t,
                                            struct USERINFO *uip,
                                            void *context,
                                            char *arg);
char *UsrXMLGetValue(struct TRANSLATE *t,
                     struct USERINFO *uip,
                     void *context,
                     const char *var,
                     const char *index);
BOOL UsrXMLIsReadOnly(struct TRANSLATE *t,
                      struct USERINFO *uip,
                      void *context,
                      const char *var,
                      const char *index);
void UsrXMLSetValue(struct TRANSLATE *t,
                    struct USERINFO *uip,
                    void *context,
                    const char *var,
                    const char *index,
                    const char *val);
BOOL UsrXMLAggregateTest(struct TRANSLATE *t,
                         struct USERINFO *uip,
                         void *context,
                         const char *var,
                         const char *testVal,
                         enum AGGREGATETESTOPTIONS ato,
                         struct VARIABLES *rev);
#endif  // __USRXML_H__
