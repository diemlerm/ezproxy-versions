/**
 * III (ILS) user authentication
 *
 * This module implements user authentication against III's Patron API, an
 * optional component of the III ILS.
 *
 * This authentication method supports common conditions and actions.
 */

#define __UUFILE__ "usriii.c"

#include "common.h"
#include "uustring.h"

#include "usr.h"
#include "usriii.h"

enum IIIMATCH { matchLast, matchPin, matchBoth, matchNone, matchFullname };

struct IIIFIELD {
    struct IIIFIELD *next;
    char *desc;
    char *var;
    char *val;
};

struct IIICTX {
    struct IIIFIELD *fields;
    enum IIIMATCH match;
    BOOL partialNameMatch;
    BOOL seenHost;
    enum DATEFORMAT dateFormat;
};

static BOOL FindUserIIIAggregateTest(struct TRANSLATE *t,
                                     struct USERINFO *uip,
                                     void *context,
                                     const char *var,
                                     const char *testVal,
                                     enum AGGREGATETESTOPTIONS ato,
                                     struct VARIABLES *rev) {
    struct AGGREGATETESTCONTEXT atc;
    struct IIICTX *iiiCtx = (struct IIICTX *)context;
    struct IIIFIELD *field;
    BOOL stop;

    if (!ExpressionAggregateTestInitialize(&atc, uip, testVal, ato)) {
        return FALSE;
    } else {
        for (field = iiiCtx->fields; field != NULL; field = field->next) {
            if (!(field->var))
                PANIC;
            if (stricmp(field->var, var) == 0) {
                char *val;
                if (!(field->val))
                    PANIC;
                if (!(val = strdup(field->val)))
                    PANIC;
                stop = !(ExpressionAggregateTestValue(&atc, (char *)val));
                xmlFreeThenNull(val);
                if (stop)
                    break;
            }
        }
    }

    return ExpressionAggregateTestTerminate(&atc, rev);
}

char *FindUserIIIAuthGetValue(struct TRANSLATE *t,
                              struct USERINFO *uip,
                              void *context,
                              const char *var,
                              const char *indexStr) {
    struct IIICTX *iiiCtx = (struct IIICTX *)context;
    struct IIIFIELD *field;
    int i = 0;
    int index;

    index = indexStr ? atoi(indexStr) : 0;
    for (field = iiiCtx->fields; field != NULL; field = field->next) {
        if (!(field->var))
            PANIC;
        if (stricmp(field->var, var) == 0) {
            if (i == index) {
                char *val;
                if (!(field->val))
                    PANIC;
                if (!(val = strdup(field->val)))
                    PANIC;
                return val;
            }
            i++;
        }
    }

    return NULL;
}

static int UsrIIIDate(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct IIICTX *iiiCtx = (struct IIICTX *)context;

    if (iiiCtx->seenHost)
        UsrLog(uip, "For III, DATE must appear before HOST in " EZPROXYUSR);
    if (stricmp(arg, "MDY") == 0) {
        iiiCtx->dateFormat = dateMDY;
    } else if (stricmp(arg, "DMY") == 0) {
        iiiCtx->dateFormat = dateDMY;
    } else if (stricmp(arg, "YMD") == 0) {
        iiiCtx->dateFormat = dateYMD;
    } else {
        UsrLog(uip, "Invalid III DATE option %s", arg);
        return 1;
    }
    return 0;
}

static int UsrIIIHost(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct IIICTX *iiiCtx = (struct IIICTX *)context;
    struct UUSOCKET rr, *r = &rr;
    enum FINDUSERRESULT result;
    PORT iiiPort;
    char line[100];
    char *p;
    char *equal;
    int v1, v2, v3;
    int month = 0, day = 0, year = 0;
    time_t expires = 0, now = 0;
    struct tm tm;
    char *comma;
    BOOL lastMatched, pinMatched, noneMatched;
    char query[256];
    char *lbracket, *rbracket, *var;
    struct IIIFIELD *nextField;
    char useSsl;
    char *slash, *colon;
    BOOL expired = 0;
    char *iiiHost = arg;

    if (iiiCtx->seenHost) {
        UsrLog(uip, "For III, HOST may only appear once");
        return 1;
    }

    iiiCtx->seenHost = 1;

    if (uip->debug) {
        UsrLog(uip, "Call UsrIIIHost with %s %s %s %d", arg, uip->user, uip->pass, iiiCtx->match);
    }

    UUInitSocket(r, INVALID_SOCKET);

    iiiCtx->fields = NULL;

    uip->result = resultInvalid;

    lastMatched = pinMatched = noneMatched = 0;

    if (iiiCtx->match == matchNone && uip->pass == NULL)
        uip->pass = "";

    if (iiiCtx->match == matchPin && (uip->pin == NULL || *(uip->pin) == 0)) {
        uip->pin = uip->pass;
        uip->pass = "";
    }

    if (iiiHost == NULL || uip->user == NULL || *(uip->user) == 0 || uip->pass == NULL)
        goto Finished;

    for (p = uip->user; *p; p++)
        if (*p < ' ' || *p > '~')
            goto Finished;

    result = resultRefused;

    iiiHost = SkipHTTPslashesAt(iiiHost, &useSsl);

    ParseHost(iiiHost, &colon, &slash, TRUE);

    if (slash)
        *slash = 0;

    iiiPort = 0;
    if (colon) {
        *colon++ = 0;
        iiiPort = atoi(colon);
    }
    if (iiiPort == 0)
        iiiPort = 4500;

    if (UUConnectWithSource2(r, iiiHost, iiiPort, "III", uip->pInterface, useSsl))
        goto Finished;

    if (uip->debug)
        UsrLog(uip, "III Connect to %s:%d", iiiHost, iiiPort);

    strcpy(query, "GET /PATRONAPI/");
    /* It looks like they want it unencoded, so leave it alone */
    /*  AddEncodedField(query, user); */
    strcat(query, uip->user);
    strcat(query, "/dump HTTP/1.0");
    UUSendZ(r, query);
    UUSendZ(r, "\r\nHost: ");
    UUSendZ(r, iiiHost);
    WriteLine(r, ":%d\r\n\r\n", iiiPort);

    if (uip->debug)
        UsrLog(uip, "III Sent %s", query);

    result = resultRefused;
    expires = 0;
    UUtime(&now);

    for (; ReadLine(r, line, sizeof(line));) {
        if (uip->debug)
            UsrLog(uip, "III Received %s", line);
        p = strchr(line, 0) - 4;
        TrimTags(line);

        equal = strchr(line, '=');
        if (equal == NULL)
            continue;
        /* The API responds ERRNUM=1 when the user can't be found, so if that happens, we know
           that the connection wasn't refused
        */
        if (result == resultRefused &&
            (strnicmp(line, "ERRNUM=1", 8) == 0 || StrIStr(line, "400 Bad Request") != NULL))
            result = resultInvalid;
        lbracket = strchr(line, '[');
        if (lbracket == NULL)
            continue;
        *equal++ = 0;
        rbracket = strchr(line, ']');
        if (rbracket == NULL)
            continue;
        *rbracket = 0;
        /* If we have a bracketed code, then again, we can't have been refused */
        if (result == resultRefused)
            result = resultInvalid;

        *lbracket++ = 0;
        var = lbracket;

        if (!(nextField = (struct IIIFIELD *)calloc(1, sizeof(*nextField))))
            PANIC;
        nextField->next = iiiCtx->fields;
        if (!(nextField->desc = strdup(line)))
            PANIC;
        if (!(nextField->var = strdup(var)))
            PANIC;
        if (!(nextField->val = strdup(equal)))
            PANIC;
        iiiCtx->fields = nextField;

        if (strcmp(var, "p43") == 0) {
            if (sscanf(equal, "%d-%d-%d", &v1, &v2, &v3) != 3) {
                expires = now + 1;
                continue;
            }
            if (v1 == 0 && v2 == 0 && v3 == 0) {
                expires = now + 1;
                continue;
            }

            if (iiiCtx->dateFormat == dateMDY) {
                month = v1;
                day = v2;
                year = v3;
            }
            if (iiiCtx->dateFormat == dateDMY) {
                day = v1;
                month = v2;
                year = v3;
            }
            if (iiiCtx->dateFormat == dateYMD) {
                year = v1;
                month = v2;
                day = v3;
            }
            if (year >= 1900)
                year -= 1900;
            if (year < 80)
                year += 100;
            memset(&tm, 0, sizeof(tm));
            tm.tm_mon = month - 1;
            tm.tm_mday = day;
            tm.tm_year = year;
            tm.tm_isdst = -1;
            expires = mktime(&tm);
        }

        if (strcmp(var, "pn") == 0) {
            noneMatched = 1;
            if (iiiCtx->partialNameMatch) {
                Trim(equal, TRIM_COMPRESS);
                if (strnicmp(equal, "Mac ", 4) == 0)
                    StrCpyOverlap(equal + 3, equal + 4);
                if (strnicmp(equal, "Mc ", 3) == 0)
                    StrCpyOverlap(equal + 2, equal + 3);
                if (CompareNameParts(equal, 1, uip->pass, 0))
                    lastMatched = 1;
            } else {
                if (iiiCtx->match != matchFullname)
                    if (comma = strchr(equal, ','))
                        *comma = 0;
                /* III started sending name in UTF-8, so NameMatch comparison changed 2/18/03 to
                 * reflect */
                if (NameMatch(equal, uip->pass, iiiCtx->partialNameMatch))
                    lastMatched = 1;
            }
        }
    }

    UUStopSocket(r, 0);

    if (iiiCtx->match != matchPin && iiiCtx->match != matchBoth)
        goto Finished;

    if (*(uip->pin) == 0)
        goto Finished;

    if (UUConnectWithSource2(r, iiiHost, iiiPort, "III", uip->pInterface, useSsl))
        goto Finished;

    strcpy(query, "GET /PATRONAPI/");
    /* see above */
    /* AddEncodedField(query, user); */
    strcat(query, uip->user);
    strcat(query, "/");
    AddEncodedField(query, uip->pin, NULL);
    strcat(query, "/pintest HTTP/1.0");
    UUSendZ(r, query);
    UUSendZ(r, "\r\nHost: ");
    UUSendZ(r, iiiHost);
    WriteLine(r, ":%d\r\n\r\n", iiiPort);

    if (uip->debug)
        UsrLog(uip, "III Sent %s", query);

    for (; ReadLine(r, line, sizeof(line));) {
        if (uip->debug)
            UsrLog(uip, "III Received %s", line);
        TrimTags(line);
        Trim(line, TRIM_COLLAPSE);
        if (strcmp(line, "RETCOD=0") == 0)
            pinMatched = 1;
    }

    UUStopSocket(r, 0);

Finished:
    if ((iiiCtx->match == matchNone && noneMatched) || (iiiCtx->match == matchPin && pinMatched) ||
        ((iiiCtx->match == matchLast || iiiCtx->match == matchFullname) && lastMatched) ||
        (iiiCtx->match == matchBoth && pinMatched && lastMatched)) {
        result = resultValid;
        /* Valid up to expiration date; hence the +86400 (one day in secs) in the test */
        if (expires != 0 && expires + 86400 < now)
            expired = 1;
    }

    if (result == resultValid && expired)
        result = resultExpired;
    if (uip->debug)
        UsrLog(uip, "III result %d", result);

    uip->result = result;

    return 0;
}

static int UsrIIIIfTestOrType(struct TRANSLATE *t,
                              struct USERINFO *uip,
                              void *context,
                              char *arg,
                              BOOL isType) {
    struct IIICTX *iiiCtx = (struct IIICTX *)context;
    char *arg2;
    char compareType;
    char compareOperation, saveCompareOperation;
    struct IIIFIELD *field;
    char *fieldValue;
    BOOL anyMatch;
    char *str;
    char *nextArg;

    if (isType) {
        arg2 = arg;
        arg = "p47";
        compareType = 'i';
    } else {
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        compareType = 's';
    }

    for (field = iiiCtx->fields; field != NULL; field = field->next) {
        if (!(field->var))
            PANIC;
        if (!(field->val))
            PANIC;
        if (stricmp(field->var, arg) == 0)
            break;
    }

    if (field == NULL) {
        return 1;
    }

    compareOperation = 0;

    if (strchr("<=>!~", *arg2) && strchr("idsIDS", *(arg2 + 1)) && IsWS(*(arg2 + 2))) {
        compareOperation = *arg2;
        compareType = tolower(*(arg2 + 1));
        arg2 = SkipWS(arg2 + 2);
    }

    saveCompareOperation = compareOperation;
    for (anyMatch = 0, str = NULL, nextArg = StrTok_R(arg2, ",", &str); anyMatch == 0 && nextArg;
         nextArg = StrTok_R(NULL, ",", &str)) {
        compareOperation = saveCompareOperation;
        if (stricmp(nextArg, "^p") == 0) {
            nextArg = uip->pass;
            if (compareOperation == 0)
                compareOperation = '=';
        }
        if (compareOperation == 0)
            compareOperation = '~';

        fieldValue = field->val;
        if (compareType != 's') {
            for (; *fieldValue && !IsDigit(*fieldValue) && *fieldValue != '.'; fieldValue++)
                ;
            if (*fieldValue == 0)
                continue;
        }

        if (compareType == 'i') {
            switch (compareOperation) {
            case '<':
                if (atoi(fieldValue) < atoi(nextArg))
                    anyMatch = 1;
                break;
            case '=':
            case '~':
                if (atoi(fieldValue) == atoi(nextArg))
                    anyMatch = 1;
                break;
            case '>':
                if (atoi(fieldValue) > atoi(nextArg))
                    anyMatch = 1;
                break;
            case '!':
                if (atoi(fieldValue) != atoi(nextArg))
                    anyMatch = 1;
                break;
            }
        } else if (compareType == 'd') {
            switch (compareOperation) {
            case '<':
                if (atof(fieldValue) < atof(nextArg))
                    anyMatch = 1;
                break;
            case '=':
                if (atof(fieldValue) == atof(nextArg))
                    anyMatch = 1;
                break;
            case '>':
                if (atof(fieldValue) > atof(nextArg))
                    anyMatch = 1;
                break;
            case '!':
                if (atof(fieldValue) != atof(nextArg))
                    anyMatch = 1;
                break;
            }
        } else {
            switch (compareOperation) {
            case '<':
                if (strcmp(fieldValue, nextArg) < 0)
                    anyMatch = 1;
                break;
            case '=':
                if (strcmp(fieldValue, nextArg) == 0)
                    anyMatch = 1;
                break;
            case '>':
                if (strcmp(fieldValue, nextArg) > 0)
                    anyMatch = 1;
                break;
            case '!':
                if (strcmp(fieldValue, nextArg) != 0)
                    anyMatch = 1;
                break;
            case '~':
                if (WildCompare(fieldValue, nextArg))
                    anyMatch = 1;
            }
        }
    }

    if (anyMatch) {
        return 0;
    }

    return 1;
}

static int UsrIIIIfTest(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return UsrIIIIfTestOrType(t, uip, context, arg, FALSE);
}

static int UsrIIIIfType(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return UsrIIIIfTestOrType(t, uip, context, arg, TRUE);
}

static int UsrIIIMsgAuth(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct IIICTX *iiiCtx = (struct IIICTX *)context;
    struct IIIFIELD *field;

    UsrLog(uip, "III auth variables");

    for (field = iiiCtx->fields; field != NULL; field = field->next) {
        UsrLog(uip, "%s=%s (%s)", field->var, field->val, field->desc);
    }

    return 0;
}

static int UsrIIIPartialNameMatch(struct TRANSLATE *t,
                                  struct USERINFO *uip,
                                  void *context,
                                  char *arg) {
    struct IIICTX *iiiCtx = (struct IIICTX *)context;

    iiiCtx->partialNameMatch = 1;
    return 0;
}

static int UsrIIIPassword(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct IIICTX *iiiCtx = (struct IIICTX *)context;

    if (iiiCtx->seenHost) {
        UsrLog(uip, "For III, Password must appear before Host in " EZPROXYUSR);
        return 1;
    }

    if (stricmp(arg, "LAST") == 0)
        iiiCtx->match = matchLast;
    else if (stricmp(arg, "PIN") == 0)
        iiiCtx->match = matchPin;
    else if (stricmp(arg, "BOTH") == 0)
        iiiCtx->match = matchBoth;
    else if (stricmp(arg, "NONE") == 0)
        iiiCtx->match = matchNone;
    else if (stricmp(arg, "FULLNAME") == 0)
        iiiCtx->match = matchFullname;
    else {
        UsrLog(uip, "Invalid III Password option %s", arg);
        return 1;
    }

    return 0;
}

/* Returns 0 if valid, 1 if not, 3 if unable to connect to iii host */
int FindUserIii(struct TRANSLATE *t,
                struct USERINFO *uip,
                char *file,
                int f,
                struct FILEREADLINEBUFFER *frb,
                struct sockaddr_storage *pInterface) {
    struct IIICTX iiiCtxBuffer, *iiiCtx;
    struct IIIFIELD *field, *nextField;
    struct USRDIRECTIVE udc[] = {
        {"Date",             UsrIIIDate,             0},
        {"Host",             UsrIIIHost,             0},
        {"IfTest",           UsrIIIIfTest,           0},
        {"IfType",           UsrIIIIfType,           0},
        {"MsgAuth",          UsrIIIMsgAuth,          0},
        {"PartialNameMatch", UsrIIIPartialNameMatch, 0},
        {"Password",         UsrIIIPassword,         0},
        {"Test",             UsrIIIIfTest,           0},
        {"Type",             UsrIIIIfType,           0},
        {NULL,               NULL,                   0}
    };

    memset(&iiiCtxBuffer, 0, sizeof(iiiCtxBuffer));
    iiiCtx = &iiiCtxBuffer;

    iiiCtx->match = matchLast;
    iiiCtx->dateFormat = dateMDY;

    uip->result = resultInvalid;

    uip->skipping = uip->user == NULL || *uip->user == 0;

    uip->authGetValue = FindUserIIIAuthGetValue;
    uip->authAggregateTest = FindUserIIIAggregateTest;
    uip->result = UsrHandler(t, "III", udc, iiiCtx, uip, file, f, frb, pInterface, NULL);

    for (field = iiiCtx->fields; field; field = nextField) {
        nextField = field->next;
        FreeThenNull(field->desc);
        FreeThenNull(field->var);
        FreeThenNull(field->val);
        FreeThenNull(field);
    }

    return uip->result;
}
