#define __UUFILE__ "uustring.c"

#include "common.h"
#include <stdarg.h>

#include "uustring.h"

void ArrayOfStringsInit(struct ARRAYOFSTRINGS *aos) {
    aos->count = 0;
}

int ArrayOfStringsCount(struct ARRAYOFSTRINGS *aos) {
    return aos->count;
}

void ArrayOfStringsTokenizeString(struct ARRAYOFSTRINGS *aos, char *values, char delim) {
    char *p, *n;

    ArrayOfStringsInit(aos);

    if (values == NULL || *values == 0)
        return;

    for (p = values; p; p = n) {
        if (n = strchr(p, delim))
            *n++ = 0;
        if (aos->count == MAXARRAYOFSTRINGSIDX)
            PANIC;
        aos->value[aos->count++] = p;
    }
}

char *ArrayOfStringsGetValue(struct ARRAYOFSTRINGS *aos, int idx) {
    return idx < aos->count ? aos->value[idx] : NULL;
}

int ArrayOfStringsGetIndex(struct ARRAYOFSTRINGS *aos, char *value) {
    int i;

    for (i = 0; i < aos->count; i++)
        if (strcmp(aos->value[i], value) == 0)
            return i;

    return -1;
}

int ArrayOfStringsGetIIndex(struct ARRAYOFSTRINGS *aos, char *value) {
    int i;

    for (i = 0; i < aos->count; i++)
        if (stricmp(aos->value[i], value) == 0)
            return i;

    return -1;
}

void ArrayOfStringsSetValue(struct ARRAYOFSTRINGS *aos, char *value, int idx) {
    if (idx >= MAXARRAYOFSTRINGSIDX)
        PANIC;

    while (aos->count <= idx)
        aos->value[aos->count++] = NULL;

    aos->value[idx] = value;
}

void ArrayOfStringsInsertValue(struct ARRAYOFSTRINGS *aos, char *value, int idx) {
    int i;

    if (idx < 0) {
        /* remap -1 to insert at end */
        if (idx == -1)
            idx = aos->count;
        else
            PANIC;
    }

    if (idx >= aos->count) {
        ArrayOfStringsSetValue(aos, value, idx);
        return;
    }

    if (aos->count == MAXARRAYOFSTRINGSIDX)
        PANIC;

    for (i = aos->count; i > idx; i--)
        aos->value[i] = aos->value[i - 1];
    aos->value[idx] = value;
    aos->count++;
}

void ArrayOfStringsDeleteValue(struct ARRAYOFSTRINGS *aos, int idx) {
    int i;

    if (idx < 0) {
        if (idx == -1) {
            if (aos->count == 0)
                return;
            idx = aos->count - 1;
        } else
            PANIC;
    }

    if (idx < aos->count) {
        aos->count--;
        for (i = idx; i < aos->count; i++)
            aos->value[i] = aos->value[i + 1];
    }
}

BOOL IsWS(char c) {
    return c != 0 && strchr(WS, c) != NULL;
}

BOOL IsAlpha(char c) {
    return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
}

BOOL IsDigit(char c) {
    return c >= '0' && c <= '9';
}

BOOL IsGraphic(char c) {
    return (c >= '!' && c <= '/') || (c >= '[' && c <= '`') || (c >= '{' && c <= '~');
}

BOOL IsAllDigits(const char *s) {
    for (; *s; s++)
        if (!IsDigit(*s))
            return 0;
    return 1;
}

BOOL IsOneOrMoreDigits(const char *s) {
    return s && *s && IsAllDigits(s);
}

BOOL IsAllAlphameric(const char *s) {
    for (; *s; s++)
        if (!IsDigit(*s) && !IsAlpha(*s))
            return 0;
    return 1;
}

BOOL NextKEV(char *s, char **key, char **value, char **next, BOOL dontUnescapeValue) {
    char *amp, *equal;

    if (key)
        *key = NULL;

    if (value)
        *value = NULL;

    if (next)
        *next = NULL;

    if (s == NULL || *s == 0)
        return 0;

    amp = strchr(s, '&');

    if (amp)
        *amp++ = 0;

    if (next)
        *next = amp;

    equal = strchr(s, '=');

    if (equal)
        *equal++ = 0;

    UnescapeString(s);
    if (key)
        *key = s;

    if (equal) {
        if (dontUnescapeValue == 0)
            UnescapeString(equal);
        if (value)
            *value = equal;
    }

    return 1;
}

static int HexDigit(char c) {
    if (IsDigit(c))
        return c - '0';
    if (c >= 'A' && c <= 'F')
        return (c - 'A') + 10;
    if (c >= 'a' && c <= 'f')
        return (c - 'a') + 10;
    return -1;
}

void UnescapeString(char *s) {
    int d1, d2;

    for (; *s; s++) {
        if (*s == '+') {
            *s = ' ';
            continue;
        }
        if (*s != '%')
            continue;

        d1 = HexDigit(*(s + 1));
        if (d1 < 0)
            continue;
        d2 = HexDigit(*(s + 2));
        if (d2 < 0)
            continue;

        *s = d1 * 16 + d2;
        StrCpyOverlap(s + 1, s + 3);
    }
}

size_t Encode64BytesNeeded(size_t len) {
    /* During base64 encoding, we need
       every 3 bytes becoming 4, or len * 4/3
       possibility of trailing padding of 3 characters
       an extra LF added every 64 encoded or 48 unencoded, so len / 48 + 1
       1 byte for trailing null
       This makes the exact size formula
       len * 4/3 + 3 + (len / 48 + 1) + 1 OR
       len * (4/3 + 1/48) + 5 OR
       len * (65/48) + 5 OR
       len * 1.3541666... + 5 OR
       len * 1.375 + 8 to be extra conservative
    */

    return (size_t)(len * 1.375 + 8);
}

char *Encode64Binary(char *dst, unsigned char *src, size_t srcLen) {
    EVP_ENCODE_CTX *ctx = NULL;
    unsigned char *dstPtr;
    int dstPartLen;

    if (dst == NULL) {
        if (!(dst = malloc(Encode64BytesNeeded(srcLen))))
            PANIC;
    }

    dstPtr = (unsigned char *)dst;
    dstPartLen = 0;

    if (!(ctx = EVP_ENCODE_CTX_new()))
        PANIC;
    EVP_EncodeInit(ctx);
    EVP_EncodeUpdate(ctx, dstPtr, &dstPartLen, src, srcLen);
    dstPtr += dstPartLen;
    EVP_EncodeFinal(ctx, dstPtr, &dstPartLen);
    dstPtr += dstPartLen;
    /* printf(" needed %d\n", dstPtr - dst); */
    *dstPtr = 0;
    if (ctx) {
        EVP_ENCODE_CTX_free(ctx);
        ctx = NULL;
    }

    Trim(dst, TRIM_COLLAPSE);

    return dst;
}

char *Encode64(char *dst, char *src) {
    return Encode64Binary(dst, (unsigned char *)src, strlen(src));
}

/* EncodeUU64 is base 64 encoding with URL reserved characters remapped to safe characters */
char *EncodeUU64(char *dst, char *src) {
    char *s = Encode64(dst, src);
    char *p;

    for (p = s; *p; p++) {
        switch (*p) {
        case '+':
            *p = '_';
            break;
        case '/':
            *p = '.';
            break;
        case '=':
            *p = '-';
            break;
        }
    }

    return s;
}

unsigned char *Decode64Binary(unsigned char *dst, size_t *dstLen, char *src) {
    EVP_ENCODE_CTX *ctx = NULL;
    unsigned char *dstPtr;
    int dstPartLen;
    int result;
    int srcLen, srcPartLen;
    char *p;

    if (src == NULL)
        return NULL;

    /* This leave space for a final null as well, so Decode64 has room without resizing */
    if (dst == NULL) {
        if (!(dst = malloc(strlen(src) + 1)))
            PANIC;
    }

    dstPtr = dst;
    dstPartLen = 0;

    srcLen = strlen(src);

    /* EVP_Decode can only deal with chunks up to 76 bytes at a time.
       Some things (e.g. Shibboleth assertions) chunk automatically with
       newlines between, but others (e.g. EVP_Encode anyone) don't, so
       we scan through things looking for chunking indicators, and if not
       finding them, manually chunk things up
    */

    /* Yes, src is signed, so > ' ' treats 128-255 characters odd,
       but valid base64 are all in the 33-127 so this is actually
       ignoring invalid characters as well
    */
    for (;;) {
        for (; srcLen > 0; src++, srcLen--) {
            if (*src > ' ')
                break;
        }
        if (srcLen <= 0)
            break;

        for (p = src, srcPartLen = 0; srcPartLen < 76; p++, srcPartLen++) {
            if (*p <= ' ')
                break;
        }

        if (!(ctx = EVP_ENCODE_CTX_new()))
            PANIC;
        EVP_DecodeInit(ctx);
        result = EVP_DecodeUpdate(ctx, dstPtr, &dstPartLen, (unsigned char *)src, srcPartLen);
        src += srcPartLen;
        srcLen -= srcPartLen;
        dstPtr += dstPartLen;
        result = EVP_DecodeFinal(ctx, dstPtr, &dstPartLen);
        dstPtr += dstPartLen;
        EVP_ENCODE_CTX_free(ctx);
        ctx = NULL;
    }

    *dstLen = dstPtr - dst;

    return dst;
}

char *Decode64(char *dst, char *src) {
    size_t dstLen;

    dst = (char *)Decode64Binary((unsigned char *)dst, &dstLen, src);
    *(dst + dstLen) = 0;
    return dst;
}

/* DecodeUU64 is base 64 decoding with URL reserved characters remapped to safe characters */
char *DecodeUU64(char *dst, char *src) {
    char *t;
    char *p;
    char *s;

    if (!(t = strdup(src)))
        PANIC;

    for (p = t; *p; p++) {
        switch (*p) {
        case '_':
            *p = '+';
            break;
        case '.':
            *p = '/';
            break;
        case '-':
            *p = '=';
            break;
        }
    }

    /* printf("%s\n", t); */
    s = Decode64(dst, t);

    FreeThenNull(t);

    return s;
}

char *SkipWS(const char *s) {
    if (s) {
        while (IsWS(*s))
            s++;
    }
    return (char *)s;
}

char *SkipNonWS(const char *s) {
    if (s) {
        while (*s != 0 && !IsWS(*s))
            s++;
    }
    return (char *)s;
}

char *NextWSArg(char *s) {
    s = SkipNonWS(s);
    if (*s) {
        *s++ = 0;
        s = SkipWS(s);
    }
    return s;
}

void Trim(char *s, int flags) {
    char *p;
    char *e;

    /* If this is a blank string, return immediately */
    if (s == NULL || *s == 0)
        return;

    if (flags & TRIM_NBSP) {
        unsigned char *u;
        for (u = (unsigned char *)s; *u; u++) {
            if (*u == 194 && *(u + 1) == 160) {
                *u = ' ';
                StrCpyOverlap((char *)u + 1, (char *)u + 2);
            }
        }
    }

    if (flags & TRIM_LDAP_DN)
        flags |= TRIM_LEAD | TRIM_TRAIL;

    if (flags & TRIM_DECIMAL)
        flags |= TRIM_TRAIL;

    if (flags & TRIM_PEM_CLEANUP)
        flags |= TRIM_LEAD | TRIM_TRAIL | TRIM_CRTOLF;

    if (flags & TRIM_SEMI) {
        if (p = strchr(s, ';'))
            *p = 0;
    }

    if (flags & TRIM_CRTOLF) {
        for (p = s; p = strstr(p, "\r\n");)
            StrCpyOverlap(p, p + 1);
        for (p = s; p = strchr(p, '\r');)
            *p = '\n';
    }

    if (flags & (TRIM_COMPRESS | TRIM_COLLAPSE))
        flags |= TRIM_LEAD | TRIM_TRAIL;

    if (flags & TRIM_TRAIL) {
        for (p = e = strchr(s, 0) - 1; p >= s && IsWS(*p); p--)
            ;
        if (p != e)
            *(p + 1) = 0;
    }

    if (flags & TRIM_LEAD) {
        for (p = s; IsWS(*p); p++)
            ;
        if (p != s)
            StrCpyOverlap(s, p);
    }

    if (flags & TRIM_TABTOSPACE) {
        for (p = s; p = strchr(p, '\t');)
            *p = ' ';
    }

    /* At first glance, it might seem that TRIM_COLLAPSE and TRIM_COMPRESS
       could cause p to advance to the final null, then allowing the p++ to
       step beyond the end of the string.  This can't happen, though, since
       both of these have already trimmed trailing spaces, so we must hit
       some non-white-space before the null, guarding against such a
       possibility */

    /* TRIM_COLLAPSE removes all whitespace */
    if (flags & TRIM_COLLAPSE) {
        for (p = s; *p; p++) {
            if (IsWS(*p))
                Trim(p, TRIM_LEAD);
        }
    }

    /* TRIM_COMPRESS changes all blocks of whitespace into a single space */
    if (flags & TRIM_COMPRESS) {
        for (p = s; *p; p++) {
            if (IsWS(*p)) {
                *p = ' ';
                Trim(p + 1, TRIM_LEAD);
            }
        }
    }

    if (flags & TRIM_LDAP_DN) {
        for (p = strchr(s, 0) - 1; p >= s; p--) {
            if (*p == ',') {
                if (IsWS(*(p + 1)))
                    Trim(p + 1, TRIM_LEAD);
                while (p > s && IsWS(*(p - 1))) {
                    p--;
                    StrCpyOverlap(p, p + 1);
                }
            }
        }
    }

    if (flags & TRIM_CTRL) {
        for (p = strchr(s, 0) - 1; p >= s; p--) {
            if (*p > 0 && *p < ' ')
                StrCpyOverlap(p, p + 1);
        }
    }

    if (flags & TRIM_PEM_CLEANUP) {
        for (p = s; *p;) {
            /* Trim trailing spaces within lines */
            if (*p == '\n' && p > s && *(p - 1) == ' ') {
                StrCpyOverlap(p - 1, p);
                p--;
                continue;
            }
            /* Trim leading spaces and double new lines */
            if (*p == '\n' && (*(p + 1) == '\n' || *(p + 1) == ' ')) {
                StrCpyOverlap(p + 1, p + 2);
                continue;
            }
            p++;
        }
    }

    if (flags & TRIM_DECIMAL) {
        char *nz = NULL;

        for (p = strchr(s, 0) - 1; p >= s; p--) {
            if (*p == '.') {
                if (nz == NULL)
                    *p = 0;
                else if (*nz)
                    *(nz) = 0;
                break;
            } else if (IsDigit(*p)) {
                if (nz == NULL && *p != '0')
                    nz = p + 1;
            } else {
                break;
            }
        }
    }
}

void AllUpper(char *s) {
    if (s) {
        for (; *s; s++)
            if (islower(*s))
                *s = toupper(*s);
    }
}

void AllLower(char *s) {
    if (s) {
        for (; *s; s++)
            if (isupper(*s))
                *s = tolower(*s);
    }
}

char *StartsWithNumeric(const char *s) {
    if (s == NULL) {
        return NULL;
    }
    for (; *s; s++) {
        if (!IsDigit(*s)) {
            return (char *)s;
        }
    }
    return (char *)s;
}

char *StartsWith(const char *str, const char *starts) {
    size_t len;

    if (str == NULL || starts == NULL)
        return NULL;

    len = strlen(starts);

    if (strncmp(str, starts, len) == 0)
        return (char *)str + len;
    else
        return NULL;
}

/**
 * Check if a string begins with given string
 * @param str String to be searched
 * @param starts String to be found
 * @return Pointer to the end pf matched string + 1 on success.  NULL on failure
 */
char *StartsIWith(const char *str, const char *starts) {
    size_t len;

    if (str == NULL || starts == NULL)
        return NULL;

    len = strlen(starts);

    if (strnicmp(str, starts, len) == 0)
        return (char *)str + len;
    else
        return NULL;
}

char *EndsWith(const char *str, const char *ends) {
    char *e;

    if (str == NULL || ends == NULL)
        return NULL;

    e = AtEnd(str) - strlen(ends);

    if (e >= str && strcmp(e, ends) == 0)
        return e;
    else
        return NULL;
}

char *EndsIWith(const char *str, const char *ends) {
    char *e;

    if (str == NULL || ends == NULL)
        return NULL;

    e = AtEnd(str) - strlen(ends);

    if (e >= str && stricmp(e, ends) == 0)
        return e;
    else
        return NULL;
}

char *AtEnd(const char *str) {
    return strchr(str, 0);
}

void StrCpy3(char *dst, char *src, int max) {
    if (src == NULL || max <= 1) {
        *dst = 0;
    } else {
        int srcLen = strlen(src);
        if (srcLen >= max) {
            srcLen = max - 1;
        }
        memmove(dst, src, srcLen);
        dst[srcLen] = 0;
    }
}

void StrCCat3(char *dst, char c, size_t max) {
    size_t len = strlen(dst);

    if (len + 1 < max) {
        *(dst + len) = c;
        *(dst + len + 1) = 0;
    }
}

char *StrIStr(const char *main, const char *sub) {
    int mainLen, subLen;

    if (main == NULL || sub == NULL)
        return NULL;

    subLen = strlen(sub);
    if (subLen == 0)
        return (char *)main;
    mainLen = strlen(main);

    for (; mainLen >= subLen; main++, mainLen--) {
        if (strnicmp(main, sub, subLen) == 0)
            return (char *)main;
    }

    return NULL;
}

char *StrTok_R(char *s, char *d, char **p) {
    char *d1;
    int delim;
    char *start;
    char *s1;

    if (s == NULL) {
        s = *p;
        if (s == NULL)
            return NULL;
    }

    for (start = NULL, delim = 0, s1 = s; *s1; s1++) {
        for (d1 = d; *d1; d1++) {
            if (*d1 == *s1) {
                delim = 1;
                break;
            }
        }
        if (delim) {
            if (start == NULL) {
                delim = 0;
                continue;
            }
            *s1++ = 0;
            *p = s1;
            return start;
        }
        if (start == NULL)
            start = s1;
    }
    *p = NULL;
    return start;
}

char *DecodeUTF8(char *s, char *c) {
    unsigned char *us = (unsigned char *)s;
    unsigned char uc;

    if ((*us & 224) == 192 && (*(us + 1) & 192) == 128) {
        uc = (*us & 31) << 6;
        us++;
        uc = uc | (*us & 63);
        *c = (char)uc;
    } else {
        *c = (char)*us;
    }
    return (char *)us;
}

void ReplaceString(char *start, int origLen, char *replace) {
    int len, replaceLen, diff;

    len = strlen(start) + 1;
    replaceLen = strlen(replace);
    diff = replaceLen - origLen;

    if (diff) {
        memmove(start + origLen + diff, start + origLen, len - origLen);
    }
    memmove(start, replace, replaceLen);
}

void InsertAtFront(char *s, size_t smax, char *i) {
    size_t slen, ilen;

    if (*i == 0)
        return;

    slen = strlen(s);
    ilen = strlen(i);

    if (slen + 1 + ilen > smax)
        return;

    /* Need to copy NULL also */
    memmove(s + ilen, s, slen + 1);
    memmove(s, i, ilen);
}

void EraseMemory(char *p, size_t len) {
    memset(p, 0, len);
}

void EraseString(char *p) {
    for (; *p; p++)
        *p = 0;
}

char StripISO88591Diacritics(char c) {
    unsigned char uc = (unsigned char)c;

    if (uc < 192)
        return c;

    if (uc >= 192 && uc <= 197)
        return 'A';

    if (uc == 199)
        return 'C';

    if (uc >= 200 && uc <= 203)
        return 'E';

    if (uc >= 204 && uc <= 207)
        return 'I';

    if (uc == 209)
        return 'N';

    if (uc >= 210 && uc <= 214)
        return 'O';

    if (uc >= 217 && uc <= 220)
        return 'U';

    if (uc == 221)
        return 'Y';

    if (uc >= 224 && uc <= 229)
        return 'a';

    if (uc == 231)
        return 'c';

    if (uc >= 232 && uc <= 235)
        return 'e';

    if (uc >= 236 && uc <= 239)
        return 'i';

    if (uc == 241)
        return 'n';

    if (uc >= 242 && uc <= 246)
        return 'o';

    if (uc >= 249 && uc <= 252)
        return 'u';

    if (uc == 253 || uc == 255)
        return 'y';

    return c;
}

char *DigestToHex(char *digestHex,
                  unsigned char *digestBinary,
                  size_t digestBinaryLen,
                  BOOL upper) {
    size_t i;
    char *p;
    char *x = (upper ? "%02X" : "%02x");

    if (digestHex == NULL) {
        if (!(digestHex = malloc(digestBinaryLen * 2 + 1)))
            PANIC;
    }

    for (i = 0, p = digestHex; i < digestBinaryLen; digestBinary++, i++, p += 2)
        sprintf(p, x, *digestBinary);

    return digestHex;
}

char *MD5DigestToHex(char *md5DigestHex, unsigned char *md5DigestBinary, BOOL upper) {
    return DigestToHex(md5DigestHex, md5DigestBinary, 16, upper);
}

char *IndefiniteArticle(char *s, BOOL upper) {
    char c;
    BOOL vowel;
    char *vowelLike[] = {"hour", "honor", NULL};
    char **vl;
    char *p;

    c = *s;
    if (isupper(c))
        c = tolower(c);

    vowel = strchr("aeiou", c) != NULL;

    if (!vowel) {
        for (vl = vowelLike; *vl; vl++) {
            if ((p = StartsIWith(s, *vl)) && !IsAlpha(*p)) {
                vowel = TRUE;
                break;
            }
        }
    }

    if (upper)
        return (vowel ? "An" : "A");
    else
        return (vowel ? "an" : "a");
}

char *SIfNotOne(int i) {
    return (i == 1 ? "" : "s");
}

char *EsIfNotOne(int i) {
    return (i == 1 ? "" : "es");
}

char *HasHave(int i, BOOL upper) {
    if (upper)
        return (i == 1 ? "Has" : "Have");
    else
        return (i == 1 ? "has" : "have");
}

char *IsAre(int i, BOOL upper) {
    if (upper)
        return (i == 1 ? "Is" : "Are");
    else
        return (i == 1 ? "is" : "are");
}

char *WasWere(int i, BOOL upper) {
    if (upper)
        return (i == 1 ? "Was" : "Were");
    else
        return (i == 1 ? "was" : "were");
}

char *ThisThese(int i, BOOL upper) {
    if (upper)
        return (i == 1 ? "This" : "These");
    else
        return (i == 1 ? "this" : "these");
}

char *StaticStringCopy(const char *s) {
    struct STATICSTRINGCOPY **p, *n;
    int comp;

    if (s == NULL)
        return NULL;

    if (*s == 0)
        return "";

    for (p = &staticStringCopies;;) {
        if (*p == NULL) {
            /* Acquire mutex and recheck to see if another thread
               just added something at this location */
            UUAcquireMutex(&mActive);
            if (*p == NULL) {
                if (!(n = calloc(1, sizeof(*n))))
                    PANIC;
                n->left = n->right = NULL;
                if (!(n->string = strdup(s)))
                    PANIC;
                *p = n;
                UUReleaseMutex(&mActive);
                return n->string;
            }
            UUReleaseMutex(&mActive);
        }
        comp = strcmp(s, (*p)->string);
        if (comp == 0)
            return (*p)->string;
        if (comp < 0)
            p = &((*p)->left);
        else
            p = &((*p)->right);
    }
}

char *UUStrRealloc(char *str) {
    if (str) {
        if (!(str = realloc(str, strlen(str) + 1)))
            PANIC;
    }
    return str;
}

char *UUStrCpyToWide(char *wide, char *narrow) {
    for (;;) {
        *wide++ = *narrow;
        *wide++ = 0;
        if (*narrow == 0)
            break;
        narrow++;
    }

    return wide - 2;
}

char *UUStrDup(const char *str) {
    char *newStr;

    if (str) {
        if (!(newStr = strdup(str)))
            PANIC;
        return newStr;
    }

    return NULL;
}

BOOL TrimBr(char *s) {
    char *br;
    char *e;
    BOOL any;

    any = 0;

    for (br = s; br = StrIStr(br, "<br");) {
        e = br + 3;
        if ((*e == '/' || IsWS(*e) || *e == '>') && (e = strchr(e, '>'))) {
            any = 1;
            StrCpyOverlap(br, e + 1);
        } else
            br++;
    }
    if (any)
        Trim(s, TRIM_TRAIL);

    return any;
}

/**
 * Remove any text surrounded in by angle brackets.  Logic assumes that
 * within a tag the closing bracket will always signal the end, with no
 * attempt to detect constructs like <tag attr=">">
 */
BOOL TrimTags(char *s) {
    char *o, *c;
    BOOL any;

    any = 0;

    // Looking for tag opening
    for (o = s; (o = strchr(o, '<'));) {
        any = 1;
        // Found tag opening, find closing and snip out
        c = strchr(o, '>');
        if (c) {
            StrCpyOverlap(o, c + 1);
        } else {
            // If there is no closing tag, just snip out the rest
            *o = 0;
        }
    }
    if (any) {
        Trim(s, TRIM_TRAIL);
    }
    return any;
}

time_t ParseDate(char *sdate) {
    struct tm tm;
    int i = 0;
    BOOL get = FALSE;

    memset(&tm, 0, sizeof(tm));
    tm.tm_mday = 1;
    tm.tm_isdst = -1;

    if (sscanf(sdate, "%u-%u-%u", &tm.tm_year, &tm.tm_mon, &tm.tm_mday) == 3) {
        if (tm.tm_year < 100) {
            tm.tm_year = tm.tm_year + 100;
        } else {
            tm.tm_year = tm.tm_year - 1900;
        }
        if (tm.tm_year < 100 || tm.tm_year >= 200) {
            return 0;
        }
        tm.tm_mon--;
        return mktime(&tm);
    } else {
        return 0;
    }
}

/**
 * Separate an int into groups of three digits separated by commas.
 * Separator could change later depending on l10n work.
 * @param n Number to format.
 * @param buffer Buffer of size SEPARATE_INT_BUFFER_LEN to store formatted string.
 *               If NULL, a buffer will be malloc'd and will need to be free'd by the caller.
 * @return The formatted string.
 */
char *SeparateInt(int n, char *buffer) {
    int i;

    if (buffer == NULL) {
        if (!(buffer = malloc(SEPARATE_INT_BUFFER_LEN)))
            PANIC;
    }
    sprintf(buffer, "%d", n);
    char *digits = buffer;
    if (*digits == '+' || *digits == '-') {
        digits++;
    }
    for (i = strlen(digits) - 3; i > 0; i -= 3) {
        StrCpyOverlap(digits + i + 1, digits + i);
        *(digits + i) = ',';
    }
    return buffer;
}
