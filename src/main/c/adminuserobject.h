#ifndef __ADMINUSEROBJECT_H__
#define __ADMINUSEROBJECT_H__

#include "common.h"

#define SESSIONDEFERUSEROBJECT "session:deferUserObject"

void AdminUserObject(struct TRANSLATE *t, char *query, char *post);

void ReadConfigLocalWSKey(char *arg, GROUPMASK gm);

#endif  // __ADMINUSEROBJECT_H__
