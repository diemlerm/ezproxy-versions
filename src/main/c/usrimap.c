/**
 * IMAP (email protocol) user authentication
 *
 * This authentication method DOES NOT SUPPORT common conditions and actions.
 */

#define __UUFILE__ "usrimap.c"

#include "common.h"
#include "uustring.h"

#include "usr.h"
#include "usrimap.h"

/* Returns 0 if valid, 1 if not, 3 if unable to connect to imap host */
int FindUserImap(char *imapHost,
                 char *user,
                 char *pass,
                 struct sockaddr_storage *pInterface,
                 char useSsl) {
    struct UUSOCKET rr, *r = &rr;
    int result;
    int state;
    char line[100];

    UUInitSocket(r, INVALID_SOCKET);

    result = 3;

    /* If a username has a space, disallow it since the next part would look like a pwd */
    if (imapHost == NULL || user == NULL || strchr(user, ' ') != NULL || pass == NULL)
        goto Finished;

    if (UUConnectWithSource2(r, imapHost, (PORT)(useSsl ? 993 : 143), "IMAP", pInterface, useSsl))
        goto Finished;

    result = 1;
    for (state = 0;;) {
        if (ReadLine(r, line, sizeof(line)) == 0) {
            result = 3;
            break;
        }
        /* Ignore intermediate information lines from IMAP server after login command */
        if (state == 1 && line[0] == '*')
            continue;
        if (strlen(line) < 4 || strncmp(line + 2, "OK", 2) != 0)
            break;
        if (state == 0) {
            UUSendZ(r, "a login ");
            SendQuotedString(r, user);
            UUSendZ(r, " ");
            SendQuotedString(r, pass);
            UUSendCRLF(r);
        } else if (state == 1) {
            result = 0;
            break;
        }
        state++;
    }
    if (result != 3) {
        UUSendZ(r, "a logout\r\n");
        UUFlush(r);
    }
    UUStopSocket(r, 0);

Finished:
    return result;
}
