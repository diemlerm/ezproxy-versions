#ifndef __FILES_H__
#define __FILES_H__

#include "tags.h"

struct EZFILE {
    char *filename;
    char *filetext;
};

// clang-format off

// The HTML tags for the DRA Web2 files purposefully avoid the inclusion
// of DOCTYPE and lang="en" as they are part of an old integration
// method with an old ILS. It is unlikely they are still in production
// anywhere, so it is impossible to test if the change would be breaking.

struct EZFILE ezfiles[] = {
{ "config.txt",
  "##  For more information on this file, see:\n"
  "##          http://www.oclc.org/support/documentation/ezproxy/cfg/\n"
  "##                                   AND  \n"
  "##          http://www.oclc.org/support/documentation/ezproxy/db/\n"
  "\n"
  "##  In this file, lines starting with # are comments.  The lines starting\n"
  "##  with ##  are meant solely as comments, whereas the lines starting with\n"
  "##  only # are followed by example entries.\n"
  "\n"
  "\n"
  "##  PLEASE NOTE: IF YOU CHANGE THIS FILE, you must restart EZproxy to make\n"
  "##  those changes take effect. == see http://www.oclc.org/support/documentation/ezproxy/restart.htm\n"
  "\n"
  "\n"
  "# ****************************EZproxy options *****************************************\n"
  "\n"
  "## After running 'EZproxy -c' to test connectivity as instructed in the install guides\n"
  "## (http://www.oclc.org/us/en/support/documentation/ezproxy/download/), \n"
  "##  If necessary, Name your EZproxy server \n"
  "## == http://www.oclc.org/support/documentation/ezproxy/cfg/name/\n"
  "\n"
  "# Name My.ezproxy.server\n"
  "\n"
  "\n"
  "##  EZproxy's default proxy mode is proxy by hostname.  Use of proxy by port is deprecated.\n"
  "\n"
  "Option ProxyByHostname\n"
  "\n"
  "##  By default, EZproxy listens on port 2048. You can specify a different port here\n"
  "\n"
  "# LoginPort 80\n"
  "\n"
  "\n"
  "##  SSL == see: http://www.oclc.org/support/documentation/ezproxy/cfg/ssl/\n"
  "##  If using Proxy by HostName you must use a wildcard SSL certificate\n"
  "##  Please review http://www.oclc.org/support/documentation/ezproxy/cfg/ssl/certopts.htm before implementing SSL \n"
  "\n"
  "# LoginPortSSL 443\n"
  "\n"
  "\n"
  "##  Connection limits. == see: http://www.oclc.org/support/documentation/ezproxy/cfg/limits.htm\n"
  "\n"
  "MaxLifetime 120\n"
  "MaxSessions 500\n"
  "MaxVirtualHosts 1000\n"
  "\n"
  "\n"
  "##  When using Safari 2.0 and later with only three hostname components, Safari defaults to blocking the cookie. With EZproxy 3.6c GA\n"
  "## (2006-03-10) or later, you can add add the following to work around the restriction.\n"
  "# Option SafariCookiePatch\n"
  "\n"
  "\n"
  "##  Securing EZproxy == see: http://www.oclc.org/support/documentation/ezproxy/example/securing.htm \n"
  "\n"
  "Audit Most\n"
  "AuditPurge 7\n"
  "Option StatusUser\n"
  "Option LogSession\n"
  "IntruderIPAttempts -interval=5 -expires=15 20\n"
  "IntruderUserAttempts -interval=5 -expires=15 10\n"
  "UsageLimit -enforce -interval=15 -expires=120 -MB=100 Global\n"
  "\n"
  "##  Logging == see: http://www.oclc.org/support/documentation/ezproxy/cfg/logformat/\n"
  "##                                            AND\n"
  "##                  http://www.oclc.org/support/documentation/ezproxy/cfg/logfile/\n"
  "\n"
  "\n"
  "LogFormat %h %l %u %t \"%r\" %s %b\n"
  "LogFile -strftime ezp%Y%m.log\n"
  "\n"
  "# **************************** Database Definitions *****************************************?\n"
  "##  See http://www.oclc.org/support/documentation/ezproxy/db/default.htm\n"
  "\n"
  "##  The following databases require no authentication,\n"
  "##  but demonstrate EZproxy functionality, and can be used for testing.\n"
  "##  Remove as desired.\n"
  "\n"
  "T Worldcat.org\n"
  "U http://worldcat.org\n"
  "DJ worldcat.org\n"
  "\n"
  "T WhatIsMyIP\n"
  "U http://whatismyip.com\n"
  "DJ whatismyip.com\n"
  "\n"
  "##  Many sites use dx.doi.org when linking from one database to another.\n"
  "##  This definition is recommended for all configurations.?\n"
  "##  See http://www.oclc.org/support/documentation/ezproxy/db/doi.htm for details \n"
  "\n"
  "Title -hide DOI System\n"
  "URL http://dx.doi.org\n"
  "Domain doi.org\n"
  "\n"
  "\n" },
{ "config.txt",
  "## **************************** OCLC Services *************************************************\n"
  "## Some OCLC services have been pre-configured with default settings below. remove the '#' from each line \n"
  "## associated with the desired service, and add additional options as desired \n"
  "\n"
  "## -----------------------------  OCLC FirstSearch ----------------------------------------------\n"
  "# T OCLC FirstSearch\n"
  "# U http://firstsearch.oclc.org/FSIP\n"
  "# DJ oclc.org\n"
  "\n"
  "## ---------------------------- Camio and Archive Grid ---------------------------------------------\n"
  "\n"
  "# T ArchiveGrid\n"
  "# U http://archivegrid.org\n"
  "# DJ archivegrid.org\n"
  "\n"
  "# T CAMIO\n"
  "# U http://camio.oclc.org\n"
  "# DJ camio.oclc.org\n"
  " \n"
  "## --------------------------- NetLibrary ---------------------------------------------------------\n"
  "## Netlibrary can be configured for Single Sign on (SSO) or standard IP recognition access.\n"
  "## Edit the appropriate stanza for your isntitution. See http://www.oclc.org/support/documentation/ezproxy/db/netlibrary.htm/ for details\n"
  "\n"
  "\n"
  "## -- Standard IP recognition --\n"
  "# Title NetLibrary\n"
  "# URL http://www.netlibrary.com/\n"
  "# Domain netlibrary.com\n"
  "\n"
  "## -- Single Sign On (SSO) --\n"
  "\n"
  "# Title NetLibrary\n"
  "# URL http://www.netlibrary.com/\n"
  "# NetLibrary [libraryid]\n"
  " \n"
  "## NOTE : Replace [libraryid] in the line above with the appropriate ID\n"
  "\n"
  "## ************************ USER DEFINED DATABASES *************************************************\n"
  "\n"
  "## The section below is for all other user-defined databases  \n"
  "## Please see: http://www.oclc.org/support/documentation/ezproxy/db/default.htm for configuration\n"
  "## information for common and supported stanzas\n"
  "\n"
  "## You may also want to join the EZproxy listserv\n"
  "## To subscribe see: http://www.oclc.org/support/documentation/ezproxy/list.htm \n" },
{ "user.txt",
  "# For more information on this file and the options for user\n"
  "# authentication, see:\n"
  "#      http://www.oclc.org/us/en/support/documentation/ezproxy/usr/\n"
  "#\n"
  "# Lines starting with # are comments\n"
  "#\n"
  "# This file may be updated while EZproxy is running without the need to\n"
  "# stop and restart the program.\n"
  "#\n"
  "# This file contains three fields separated by colons (:) of the form\n"
  "#\n"
  "#      username:password:options\n"
  "#\n"
  "#\n"
  "# A basic entry consists of a username and password, such as\n"
  "#\n"
  "#      someuser:somepass\n"
  "#\n"
  "# To create an administrative user, place :admin after the password.\n"
  "# For example, the following line would create a username of rdoe with\n"
  "# a password of keepsafe that has administrative access to the EZproxy server.\n"
  "#\n"
  "#      rdoe:keepsafe:admin\n"
  "\n" },
{ "license.txt",
  "EZproxy (TM) License Agreement\n"
  EZPROXYCOPYRIGHTSTATEMENT "\n"
  "\n"
  "You should carefully read the terms and conditions before using this\n"
  "software. By using this software, you indicate your acceptance of this\n"
  "license agreement and warranty.\n"
  "\n"
  "The terms and conditions are available at\n"
  "\n"
  "     http://www.oclc.org/support/forms/pdf/ezproxy_terms.pdf\n"
},
{ "mimetype",
  "text/html                      html htm shtml\n"
  "text/css                       css\n"
  "text/plain                     txt log\n"
  "image/gif                      gif\n"
  "image/jpeg                     jpeg jpg jpe\n"
  "image/png                      png\n"
  "image/bmp                      bmp\n"
  "image/tiff                     tiff tif\n"
  "application/pdf                pdf\n"
  "application/x-javascript       js\n"
  "application/msword             doc\n"
  "application/vnd.ms-powerpoint  ppt\n"
  "application/vnd.ms-excel       xls\n"
  "application/vnd.openxmlformats docx pptx xlsx\n"
  "application/octet-stream       bin exe\n"
  "application/zip                zip\n"
  "audio/mpeg                     mp3\n" },
{ "docs/cookie.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<title>Cookie Required</title>\n"
  "</head>\n"
  "<body>\n"
  "<p>\n"
  "Licensing agreements for these databases require that access be extended\n"
  "only to authorized users.  Once you have been validated by this system,\n"
  "a \"cookie\" is sent to your browser as an ongoing indication of your authorization to\n"
  "access these databases.  This cookie only needs to be set once during login.\n"
  "</p>\n"
  "<p>\n"
  "If you are using a firewall or network privacy program, you may need\n"
  "reconfigure it to allow cookies to be set from this server.\n"
  "</p>\n"
  "<p>\n"
  "As you access databases, they may also use cookies.  Your ability to use those databases\n"
  "may depend on whether or not you allow those cookies to be set.\n"
  "</p>\n"
  "<p>\n"
  "To login again, click <a href='/login'>here</a>.\n"
  "</p>\n"
  "</body>\n"
  "</html>\n"
  "\n" },
{ "docs/login.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<title>Login</title>"
  "<style type='text/css'>\n"
  ":focus {\n\n"
  "  background-color: yellow;\n"
  "}\n"
  "</style>\n"
  "<!--\n"
  "     NOTE:\n"
  "     To include graphics on this page, place the graphics in the public subdirectory\n"
  "     and use links such as:\n"
  "     <img src='/public/logo.gif'>\n"
  "     It is important to use /public/ instead of just public/ since some EZproxy\n"
  "     login URLs are not be relative to /\n"
  "-->\n"
  "</head>\n"
  "<body>\n"
  "<p>\n"
  "This is login.htm from the docs subdirectory.  Review\n"
  "<a href='http://www.oclc.org/us/en/support/documentation/ezproxy/url/admin/'>http://www.oclc.org/us/en/support/documentation/ezproxy/url/admin/</a>\n"
  "for information on how to create an administrative user account and\n"
  "<a href='http://www.oclc.org/us/en/support/documentation/ezproxy/usr/'>www.oclc.org/us/en/support/documentation/ezproxy/usr/</a>\n"
  "for information on user authentication options.\n"
  "</p>\n"
  "<hr>\n"
  "<form action='/login' method='post'>\n"
  "^F"
  "<input type='hidden' name='url' value='^U'>\n"
  "<div><label for='user'>Please enter your username:</label>     <input type='text'     name='user' id='user'></div>\n"
  "<div><label for='password'>Please enter your password:</label> <input type='password' name='pass' id='pass'></div>\n"
  "<input type='submit' value='Login'>\n"
  "</form>\n"
  "</body>\n"
  "</html>\n" },
{ "docs/loginbu.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<title>Login</title>"
  "<style type='text/css'>\n"
  ":focus {\n\n"
  "  background-color: yellow;\n"
  "}\n"
  "</style>\n"
  "</head>\n"
  "<body>\n"
  "<p>\n"
  "This is loginbu.htm from the docs subdirectory.  This file is sent if the\n"
  "username/password provided is not valid.\n"
  "</p>\n"
  "<hr>\n"
  "<p>\n"
  "That username or password was incorrect.  Please try again.\n"
  "</p>\n"
  "<form action='/login' method='post'>\n"
  "^F"
  "<input type='hidden' name='url' value='^U'>\n"
  "<div><label for='user'>Please enter your username:</label>     <input type='text'     name='user' id='user'></div>\n"
  "<div><label for='password'>Please enter your password:</label> <input type='password' name='pass' id='pass'></div>\n"
  "<input type='submit' value='Login'>\n"
  "</form>\n"
  "</body>\n"
  "</html>\n" },
{ "docs/logout.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<title>Logout</title>"
  "</head>\n"
  "<body>\n"
  "<p>This is logout.htm from the docs subdirectory.</p>\n"
  "<hr>\n"
  "<p>You are now logged out.</p>\n"
  "</body></html>\n" },
{ "docs/menu.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<title>Database Menu</title>\n"
  "</head>\n"
  "<body>\n"
  "<p>\n"
  "This is menu.htm from the docs subdirectory.\n"
  "</p>\n"
  "<hr>\n"
  "<p>\n"
  "This page is the default menu of databases.  It is typically used for testing,\n"
  "but not for deployment.  See <a\n"
  "href='https://www.oclc.org/support/services/ezproxy/documentation/cfg/config-txt-intro.en.html'>https://www.oclc.org/support/services/ezproxy/documentation/cfg/config-txt-intro.en.html</a> for more information.\n"
  "</p>\n"
  "<p align=center>\n"
  "<strong>Database Menu</strong>\n"
  "</p>\n"
  "<p>\n"
  "\n"
  "^B\n"
  "<a href='/login?url=^V'>^T</a><br>\n"
  "^E\n"
  "\n"
  "</p>\n"
  "\n"
  "<p>\n"
  "If you are a library user who is having problems accessing these resources, please contact your\n"
  "library for assistance.\n"
  "</p>\n"
  "<p>\n"
  "If you are the person who installed EZproxy and have any questions regarding it,\n"
  "e-mail <a href='mailto:" EZPROXYHELPEMAIL "'>" EZPROXYHELPEMAIL "</a>\n"
  "</p>\n"
  "</body>\n"
  "</html>\n" },
{ "docs/https.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<title>https:// required</title>\n"
  "</head>\n"
  "<body>\n"
  "<p>\n"
  "http:// specified on a URL that requires https://\n"
  "</p>\n"
  "<p>\n"
  "Please edit the URL and try again.\n"
  "</p>\n"
  "</body>\n"
  "</html>\n" },
{ "docs/public/", NULL },
{ "docs/limited/", NULL },
{ "docs/loggedin/", NULL },
  { NULL, NULL }
};
struct EZFILE ezfilesg[] = {
{ "docs/logup.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<style type='text/css'>\n"
  ":focus {\n\n"
  "  background-color: yellow;\n"
  "}\n"
  "</style>\n"
  "</head>\n"
  "<body>\n"
  "<p>\n"
  "This is logup.htm from the docs subdirectory.  This page is sent when a virtual web server requires group membership lacked by the current user.  If the user provides another username/password, the group memberships of that user are merged with the current user.\n"
  "</p>\n"
  "<hr>\n"
  "<form action='/login' method='post'>\n"
  "^F"
  "<input type='hidden' name='url' value='^U'>\n"
  "<div><label for='user'>Please enter your username:</label>     <input type='text'     name='user' id='user'></div>\n"
  "<div><label for='password'>Please enter your password:</label> <input type='password' name='pass' id='pass'></div>\n"
  "<input type='submit' value='Login'>\n"
  "</form>\n"
  "</body>\n"
  "</html>\n" },
  { NULL, NULL }
};
struct EZFILE ezfilesi[] = {
{ "docs/irefused.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<title>III Refused</title>"
  "</head>\n"
  "<body>\n"
  "<p>This is irefused.htm from the docs subdirectory.</p>\n"
  "<hr>\n"
  "<p>The library system is down, please try again later</p>.\n"
  "</body></html>\n" },
{ "docs/iexpired.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<title>III Expired</title>"
  "</head>\n"
  "<body>\n"
  "<p>This is iexpired.htm from the docs subdirectory.</p>\n"
  "<hr>\n"
  "<p>Your library card has expired.</p>\n"
  "</body></html>\n" },
  { NULL, NULL }
};
struct EZFILE ezfilesh[] = {
{ "docs/needhost.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<title>Need Host</title>"
  "</head>\n"
  "<body>\n"
  "<p>To allow ^P/login?url=^V to work, your EZproxy administrator must first authorize this within the " EZPROXYCFG " file.</p>\n"
  "<p>Within this database's section of the file, the following line must be added:\n</p>"
  "<tt>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;H ^H</tt>\n"
  "<p>The EZproxy server must then be restarted to make this change take effect.</p>\n"
  "</body></html>\n" },
  { NULL, NULL }
};
struct EZFILE ezfilesd[] = {
{ "dra/ezp1.html",
  "<HTML>\n"
  "\n"
  "<!--Copyright 1996-1998 Data Research Associates, Inc. All rights reserved. Removal of this notice violates applicable copyright laws.-->\n"
  "<!--English/ezp1.html-->\n"
  "<!--DRA_PATRON_REQUIRED GUEST_SCREEN=\"ezp2.html\" EXPIRED_SCREEN=\"PINValidate.html\" FAIL_SCREEN=\"FailedLogin.html\" TIMEOUT=\"120\"-->\n"
  "\n"
  "<HEAD>\n"
  "DRA_SESSION_ID=<!--DRA_SESSION_ID--><br>\n"
  "PATCLASS=<!--DRA_PATRON :patclass--><br>\n"
  "CARDEXPIRATIONDATE=<!--DRA_PATRON :cardexpirationdate--><br>\n"
  "LIBRARYSYSTEM=<!--DRA_PATRON :librarysystem--><br>\n"
  "<FORM name='Logout' action='<!--DRA_COMMAND form-->' method='POST'>\n"
  "<!--DRA_BUTTON name='Logout' COMMAND=\"log_out\" CAPTION=\"Logout\"-->\n"
  "</form>\n"
  "</BODY>\n"
  "</HTML>\n" },
{ "dra/ezp2.html",
  "<html>\n"
  "\n"
  "<!--Copyright 1996-1998 Data Research Associates, Inc. All rights reserved. Removal of this notice violates applicable copyright laws.-->\n"
  "<!--/English/ezp2.html-->\n"
  "<head>\n"
  "<style type='text/css'>\n"
  ":focus {\n\n"
  "  background-color: yellow;\n"
  "}\n"
  "</style>\n"
  "<!--DRA_DEFINE *this_screen VALUE='login'-->\n"
  "DRA_SESSION_ID=<!--DRA_SESSION_ID--><br>\n"
  "<FORM name='login' action='<!--DRA_COMMAND login_when_needed-->' method='POST' ENCTYPE='application/x-www-form-urlencoded'>\n"
  "<INPUT TYPE='hidden' name='screen' VALUE='<!--DRA_POP_SCREEN-->'>\n"
  "<INPUT name='userid' VALUE='' MAXLENGTH='256' SIZE='20'>\n"
  "<INPUT TYPE='password' name='pin' VALUE='' MAXLENGTH='256' SIZE='20'>\n"
  "<INPUT TYPE='submit' VALUE='Login' >\n"
  "</form>\n"
  "</body>\n"
  "</html>\n" },
{ "web2/classic/ezp1.html",
  "<HTML>\n"
  "\n"
  "<!--Copyright 1996-1998 Data Research Associates, Inc. All rights reserved. Removal of this notice violates applicable copyright laws.-->\n"
  "<!--English/ezp1.html-->\n"
  "<!--WEB2_PATRON_REQUIRED GUEST_SCREEN=\"ezp2.html\" EXPIRED_SCREEN=\"PINValidate.html\" FAIL_SCREEN=\"FailedLogin.html\" TIMEOUT=\"120\"-->\n"
  "\n"
  "<HEAD>\n"
  "DRA_SESSION_ID=<!--WEB2_SESSION_ID--><br>\n"
  "PATCLASS=<!--WEB2_PATRON :patclass--><br>\n"
  "CARDEXPIRATIONDATE=<!--WEB2_PATRON :cardexpirationdate--><br>\n"
  "LIBRARYSYSTEM=<!--WEB2_PATRON :librarysystem--><br>\n"
  "<FORM name='Logout' action='<!--WEB2_COMMAND form-->' method='POST'>\n"
  "<!--WEB2_BUTTON name='Logout' COMMAND=\"log_out\" CAPTION=\"Logout\"-->\n"
  "</form>\n"
  "</BODY>\n"
  "</HTML>\n" },
{ "web2/classic/ezp2.html",
  "<html>\n"
  "\n"
  "<!--Copyright 1996-1998 Data Research Associates, Inc. All rights reserved. Removal of this notice violates applicable copyright laws.-->\n"
  "<!--/English/ezp2.html-->\n"
  "<head>\n"
  "<style type='text/css'>\n"
  ":focus {\n\n"
  "  background-color: yellow;\n"
  "}\n"
  "</style>\n"
  "<!--WEB2_DEFINE *this_screen VALUE='login'-->\n"
  "DRA_SESSION_ID=<!--WEB2_SESSION_ID--><br>\n"
  "<FORM name='login' action='<!--WEB2_COMMAND login_when_needed-->' method='POST' ENCTYPE='application/x-www-form-urlencoded'>\n"
  "<INPUT TYPE='hidden' name='screen' VALUE='<!--WEB2_POP_SCREEN-->'>\n"
  "<INPUT name='userid' VALUE='' MAXLENGTH='256' SIZE='20'>\n"
  "<INPUT TYPE='password' name='pin' VALUE='' MAXLENGTH='256' SIZE='20'>\n"
  "<INPUT TYPE='submit' VALUE='Login' >\n"
  "</form>\n"
  "</body>\n"
  "</html>\n" },
{ "web2/unicorn/ezp1.html",
  "<HTML>\n"
  "\n"
  "<!--Copyright 1996-1998 Data Research Associates, Inc. All rights reserved. Removal of this notice violates applicable copyright laws.-->\n"
  "<!--/U_ENG_IFiles/ezp1.html-->\n"
  "<!--WEB2_PATRON_REQUIRED GUEST_SCREEN=\"ezp2.html\" EXPIRED_SCREEN=\"PINValidate.html\" FAIL_SCREEN=\"FailedLogin.html\" TIMEOUT=\"120\"-->\n"
  "\n"
  "<HEAD>\n"
  "DRA_SESSION_ID=<!--WEB2_SESSION_ID--><br>\n"
  "PATCLASS=<!--WEB2_PATRON profile--><br>\n"
  "CARDEXPIRATIONDATE=<!--WEB2_PATRON date_user_privilege_expires--><br>\n"
  "LIBRARYSYSTEM=<!--WEB2_PATRON library--><br>\n"
  "<FORM name='Logout' action='<!--WEB2_COMMAND form-->' method='POST'>\n"
  "<!--WEB2_BUTTON name='Logout' COMMAND=\"log_out\" CAPTION=\"Logout\"-->\n"
  "</form>\n"
  "<!--WEB2_SESSION_STATE-->\n"
  "</BODY>\n"
  "</HTML>\n" },
{ "web2/unicorn/ezp2.html",
  "<html>\n"
  "\n"
  "<!--Copyright 1996-1998 Data Research Associates, Inc. All rights reserved. Removal of this notice violates applicable copyright laws.-->\n"
  "<!--/U_ENG_iFiles/ezp2.html-->\n"
  "<head>\n"
  "<style type='text/css'>\n"
  ":focus {\n\n"
  "  background-color: yellow;\n"
  "}\n"
  "</style>\n"
  "<!--WEB2_DEFINE *this_screen VALUE='login'-->\n"
  "DRA_SESSION_ID=<!--WEB2_SESSION_ID--><br>\n"
  "<FORM name='login' action='<!--WEB2_COMMAND login_when_needed-->' method='POST' ENCTYPE='application/x-www-form-urlencoded'>\n"
  "<INPUT TYPE='hidden' name='screen' VALUE='<!--WEB2_POP_SCREEN-->'>\n"
  "<INPUT name='userid' VALUE='' MAXLENGTH='256' SIZE='20'>\n"
  "<INPUT TYPE='password' name='pin' VALUE='' MAXLENGTH='256' SIZE='20'>\n"
  "<INPUT TYPE='submit' VALUE='Login' >\n"
  "</form>\n"
  "</body>\n"
  "</html>\n" },
{ "docs/expired.htm",
  "This is expired.htm from the docs subdirectory.\n"
  "<p>\n"
  "<hr>\n"
  "<p>\n"
  "Your library card has expired.\n" },
{ "docs/refused.htm",
  "This is refused.htm from the docs subdirectory.\n"
  "<p>\n"
  "<hr>\n"
  "<p>\n"
  "The library system is down, please try again later.\n" },
  { NULL, NULL }
};
struct EZFILE ezfilesa[] = {
{ "docs/logincp.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<title>Change Password</title>"
  "</head>\n"
  "<body>\n"
  "<p>This is logincp.htm from the docs subdirectory.</p>\n"
  "<hr>\n"
  "<p>^T</p>\n"
  "<form action='/password' method='post'>\n"
  "<input type='hidden' name='password' value='1'>\n"
  "<input type='hidden' name='url' value='^U'>\n"
  "<table cellpadding=0 cellspacing=0>\n"
  "<tr><td><label for='oldpass'>Old password:</label> <td> <input type='password' name='oldpass' id='oldpass'></tr>\n"
  "<tr><td><label for='newpass'>New password:</label> <td><input type='password' name='newpass' id='newpass'></tr>\n"
  "<tr><td><label for='verifypass'>New password again:</label> <td><input type='password' name='verifypass' name='verifypass'></tr>\n"
  "<tr><td>&nbsp;<td><input type='submit' value='Change password'></tr>\n"
  "</table>\n"
  "</form>\n"
  "</body></html>\n" },
{ "docs/logincp2.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<title>Change Password</title>"
  "</head>\n"
  "<p>This is logincp2.htm from the docs subdirectory.</p>\n"
  "<hr>\n"
  "<p>Your password has been changed.</p>\n"
  "<form action='/login' method='post'>\n"
  "<input type='hidden' name='url' value='^U'>\n"
  "<input type='submit' value='Continue'>\n"
  "</form>\n"
  "</body></html>\n" },
{ "docs/logincp3.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<title>Change Password</title>"
  "</head>\n"
  "<p>This is logincp3.htm from the docs subdirectory.</p>\n"
  "<hr>\n"
  "<p>The password for this account cannot be changed.</p>\n"
  "<form action='/login' method='post'>\n"
  "<input type='hidden' name='url' value='^U'>\n"
  "<input type='submit' value='Continue'>\n"
  "</form>\n"
  "</body></html>\n" },
{ "docs/aadd.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<title>Add Account</title>"
  "</head>\n"
  "<p>This is aadd.htm from the docs subdirectory.</p>\n"
  "<hr>\n"
  "<form action='/account-add' type='post'>\n"
  "<table>\n"
  "<tr><td><label for='user'>Username:</label> <td><input type='text' name='user' id='user'></tr>\n"
  "<tr><td>&nbsp;<td><input type='submit' value='Add account'></tr>\n"
  "</table>\n"
  "</form>\n"
  "</body></html>\n" },
{ "docs/areset.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<title>Reset Account</title>"
  "</head>\n"
  "<p>This is areset.htm from the docs subdirectory.</p>\n"
  "<hr>\n"
  "<form action='/account-reset' type='post'>\n"
  "<table>\n"
  "<tr><td><label for='user'>Username:</label> <td><input type='text' name='user' id='user'></tr>\n"
  "<tr><td>&nbsp;<td><input type='submit' value='Reset password'></tr>\n"
  "</table>\n"
  "</form>\n"
  "</body></html>\n" },
  { NULL, NULL }
};
struct EZFILE ezfilesw[] = {
{ "docs/wexpired.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<title>Windows Expired Account</title>"
  "</head>\n"
  "<p>This is wexpired.htm from the docs subdirectory.</p>\n"
  "<hr>\n"
  "<p>^0</p>\n"
  "<form action='/login' method='post'>\n"
  "<input type='hidden' name='url' value='^U'>\n"
  "<input type='hidden' name='user' value='^1'>\n"
  "<input type='hidden' name='domain' value='^2'>\n"
  "<input type='hidden' name='auth' value='^3'>\n"
  "<table cellpadding='0' cellspacing='0'>\n"
  "<tr><td><label for='pass'>Old password:</label> </td><td><input type='password' name='pass' id='pass'></td></tr>\n"
  "<tr><td><label for='newpass'>New password:</label> </td><td><input type='password' name='newpass' id='newpass'></td></tr>\n"
  "<tr><td><label for='verifypass'>New password again:</label> </td><td><input type='password' name='verifypass' id='verifypass'></td></tr>\n"
  "<tr><td>&nbsp;</td><td><input type='submit' value='Change password'></td></tr>\n"
  "</table>\n"
  "</form>\n"
  "</body></html>\n" },
  { NULL, NULL }
};
struct EZFILE ezfilesl[] = {
{ "docs/ldappass.htm",
  TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT
  "<title>LDAP Password</title>"
  "</head>\n"
  "<p>This is ldappass.htm from the docs subdirectory.</p>\n"
  "<hr>\n"
  "<p>^0</p>\n"
  "<form action='/login' method='post'>\n"
  "<input type='hidden' name='url' value='^U'>\n"
  "<input type='hidden' name='user' value='^1'>\n"
  "<input type='hidden' name='auth' value='^2'>\n"
  "<input type='hidden' name='context' value='^3'>\n"
  "<table cellpadding=0 cellspacing=0>\n"
  "<tr><td><label for='pass'>Old password:</label> <td> <input type='password' name='pass' id='pass'></tr>\n"
  "<tr><td><label for='newpass'>New password:</label> <td><input type='password' name='newpass' id='newpass'></tr>\n"
  "<tr><td><label for='verifypass'>New password again:</label> <td><input type='password' name='verifypass' id='verifypass'></tr>\n"
  "<tr><td>&nbsp;<td><input type='submit' value='Change password'></tr>\n"
  "</table>\n"
  "</form>\n"
  "</body></html>\n" },
  { NULL, NULL }
};

struct EZFILE ezfiless[] = {
{ "security/000-default.txt",
  "# The rules in this file are loaded before those in any other file (V1.0)\n"
  "EnforceOCLCByteLimit\tif\tbytes_transferred\tover\t2000000000\tper\t60\tthen\tblock\n"
  "OCLCByteLimit1G\t\tif\tbytes_transferred\tover\t1000000000\tper\t60\tthen\tlog\n"
  "OCLCCountryLimit\tif\tcountry\t\t\tover\t2\t\tper\t1440\tthen\tlog\n"
  "EnforceOCLCCountryLImit\tif\tcountry\t\t\tover\t4\t\tper\t1440\tthen\tblock\n"
  "OCLCLoginFailureLimit\tif\tlogin_failure\t\tover\t10\t\tper\t60\tthen\tlog\n"
  "EnforceOCLCIPLImit\tif\tnetwork_address\t\tover\t20\t\tper\t60\tthen\tblock\n"
  "OCLCIPLimit10day\tif\tnetwork_address\t\tover\t10\t\tper\t1440\tthen\tlog\n"
  "OCLCIPLimit10\t\tif\tnetwork_address\t\tover\t10\t\tper\t60\tthen\tlog\n"
  "OCLCPDFByteLimit\tif\tpdf_bytes_transferred\tover\t500000000\tper\t60\tthen\tlog\n"
  "OCLCPDFByteLimitlong\tif\tpdf_bytes_transferred\tover\t500000000\tper\t1440\tthen\tlog\n"
  "OCLCPDFLimit2\t\tif\tpdf_download\t\tover\t150\t\tper\t60\tthen\tlog\n"
  "OCLCPDFLimitshort\tif\tpdf_download\t\tover\t50\t\tper\t5\tthen\tlog\n"
  "OCLCPDFLimitlong\tif\tpdf_download\t\tover\t300\t\tper\t1440\tthen\tlog\n"
},
{ NULL, NULL }
};
#endif // __FILES_H__
