#define __UUFILE__ "adminmessages.c"

#include "common.h"
#include "uustring.h"

#include "adminmessages.h"

void AdminMessages(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    const int AMLAST = 0;
    const int AMFIND = 1;
    const int AMSINCE = 2;
    const int AMFROM = 3;
    struct FORMFIELD ffs[] = {
        {"last",  NULL, 0, 0, 64},
        {"find",  NULL, 0, 0, 0 },
        {"since", NULL, 0, 0, 64},
        {"from",  NULL, 0, 0, 0 },
        {NULL,    NULL, 0, 0, 0 }
    };
    time_t since;
    BOOL headerShown = 0;
    int last;
    char lastBuffer[SEPARATE_INT_BUFFER_LEN];

    FindFormFields(query, post, ffs);

    since = ffs[AMSINCE].value ? ParseRelativeDate(ffs[AMSINCE].value) : 0;
    if (ffs[AMFIND].value || since) {
        last = -1;
    } else {
        last = atoi(ffs[AMLAST].value ? ffs[AMLAST].value : "0");
    }

    if (ffs[AMFROM].value) {
        if (strcmp(ffs[AMFROM].value, "ul") == 0) {
            char title[256];
            strcpy(title, "View Suspensions");
            if (ffs[AMSINCE].value) {
                if (strcmp(ffs[AMSINCE].value, "today") == 0) {
                    strcat(title, " For Today");
                }
                if (strcmp(ffs[AMSINCE].value, "yesterday") == 0) {
                    strcat(title, " Since Yesterday");
                }
            }

            AdminBaseHeader(
                t, 0, title,
                " | <a href='/usagelimits'>View usage limits and clear suspensions</a>");
            headerShown = 1;
        }
    }

    if (headerShown == 0) {
        char title[256];
        strcpy(title, "View " EZPROXYNAMEPROPER " Messages");
        if (last >= 0) {
            if (last > 0) {
                sprintf(AtEnd(title), " - Last %s", SeparateInt(last, lastBuffer));
            } else {
                strcat(title, " - All");
            }
        }
        AdminHeader(t, 0, title);
    }

    UUSendZ(s, "<pre>\n");
    if (ffs[AMFIND].value || since)
        SendFileMatches(t, msgFilename, SFREPORTIFMISSING | SFQUOTEDURL, ffs[AMFIND].value, since);
    else
        SendFileTail(t, msgFilename, SFREPORTIFMISSING | SFQUOTEDURL,
                     atoi(ffs[AMLAST].value ? ffs[AMLAST].value : "0"));
    UUSendZ(s, "</pre>\n");
    AdminFooter(t, 1);
}
