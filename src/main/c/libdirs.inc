#THIS IS NO LONGER USED...please look in src/main/resources/
GEOIPDIR=GeoIP-1.4.6
LIBXML2DIR=libxml2-2.9.2
OPENLDAPDIR=openldap-2.4.31
OPENSSLDIR=openssl-0.9.8x
PCREDIR=pcre-7.6
REGEXDIR=regex-0.12
XMLSEC1DIR=xmlsec1-1.2.11
ZLIBDIR=zlib-1.2.3

LOCALDIR=local
LIBDIR=${LOCALDIR}/lib
