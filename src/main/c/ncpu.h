/**
 * ncpu.h
 *
 *  Created on: May 25, 2010
 *      Author: Steven Eastman
 */

#ifndef __NCPU_H__
#define __NCPU_H__

intmax_t cpusAvailable(void);

#endif /* NCPU_H_ */
