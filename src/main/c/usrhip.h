#ifndef __USRHIP_H__
#define __USRHIP_H__

#include "common.h"

enum FINDUSERRESULT FindUserHIP(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                char *file,
                                int f,
                                struct FILEREADLINEBUFFER *frb,
                                struct sockaddr_storage *pInterface);

#endif  // __USRHIP_H__
