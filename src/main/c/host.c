#define __UUFILE__ "host.c"

#include "common.h"
#include "usr.h"
#include "identifier.h"

#include "adminebrary.h"

#ifdef WIN32
#include <share.h>
#include <sys/locking.h>
#else
/* Include SetUidGid header */
#include "unix.h"
#include <sys/uio.h>
#endif

#include <sys/stat.h>
#include <math.h>

char *IPInterfaceName(char *buffer, int size, struct sockaddr_storage *sa) {
    if (is_unspecified(sa)) {
        strcpy(buffer, "ANY");
    } else {
        ipToStr(sa, buffer, size);
        // inet_ntop(sa->ss_family, get_in_addr2(sa), buffer, sizeof buffer);
    }

    return buffer;
}

void NeedSave(void) {
    UUtime(&hostsNeedSave);
    hostsSaved = 0;
}

void NeedSaveUsage(void) {
    usageSaved = 0;
}

int UUAvlTreeCmpHost(const void *v1, const void *v2) {
    struct HOST *h1 = (struct HOST *)v1;
    struct HOST *h2 = (struct HOST *)v2;
    int i;

    if (i = strcmp(h1->hostname, h2->hostname))
        return i;

    if (i = ((int)h1->remport) - ((int)h2->remport))
        return i;

    if (i = ((int)h1->ftpgw) - ((int)h2->ftpgw))
        return i;

    return ((int)h1->useSsl) - ((int)h2->useSsl);
}

int UUAvlTreeCmpNeverProxyHosts(const void *v1, const void *v2) {
    struct NEVERPROXYHOST *nph1 = (struct NEVERPROXYHOST *)v1;
    struct NEVERPROXYHOST *nph2 = (struct NEVERPROXYHOST *)v2;
    int i;

    if (i = strcmp(nph1->host, nph2->host))
        return i;

    if (i = ((int)nph1->port) - ((int)nph2->port))
        return i;

    if (i = ((int)nph1->ftpgw) - ((int)nph2->ftpgw))
        return i;

    return ((int)nph1->useSsl) - ((int)nph2->useSsl);
}

/* VerifyHost2 can be used with bare host names or with URLs */
/* isMe comes back
   0 for a URL other than this server,
   1 for this server but not a known /login/ URL,
   2 for this server and a known /login/ URL
*/
int VerifyHost2(char *host, GROUPMASK gm, struct DATABASE **rb, BOOL *isMyLoggedIn, int *isMe) {
    PORT port, mport, defaultPort;
    char *p;
    char *q;
    int i;
    struct HOST *h;
    struct DATABASE *b;
    struct INDOMAIN *m;
    size_t len;
    size_t plen = 0;
    char *slashQuestion;
    BOOL me;
    BOOL myHost;
    char useSsl;
    char ftpgw;
    struct EBRARYSITE *ebs;
    struct OVERDRIVESITE *ods;
    char *login2Group = NULL;

    if (isMyLoggedIn)
        *isMyLoggedIn = 0;

    if (isMe)
        *isMe = 0;

    if (gm)
        GroupMaskDefault(gm);

    if (rb)
        *rb = NULL;

    if (host == NULL)
        return 1;

    host = SkipFTPHTTPslashesAt(host, &useSsl, &ftpgw);

    char *colon;
    ParseHost(host, &colon, &p, TRUE);

    defaultPort = (ftpgw ? 21 : (useSsl ? 443 : 80));

    if (colon) {
        len = colon - host;
        port = atoi(colon + 1);
    } else {
        len = p - host;
        port = defaultPort;
    }

    /* Recognize hostnames that have a final period added to them */
    if (len > 1 && host[len - 1] == '.')
        len--;

    /* the "login." variant is now checked in ValidRefererHostname */
    myHost = (len == strlen(myName) && strnicmp(host, myName, len) == 0) ||
             ValidRefererHostname(host, len, NULL);
    /* Always recognize our own administrative server name */
    me = myHost && IsALoginPort(port);

    if (me) {
        if (isMe)
            *isMe = 1;
        if (p != NULL) {
            struct DISPATCH *dp;
            dp = FindLoginDispatch(p);
            if (dp && (dp->flags & ADMIN_UNAUTH) == 0)
                if (isMyLoggedIn)
                    *isMyLoggedIn = 1;

            /*
            if (strnicmp(p, "/admin", 6) == 0) {
                if (isMyLoggedIn)
                    *isMyLoggedIn = 1;
            }

            if (strnicmp(p, "/ebrary", 7) == 0) {
                if (isMyLoggedIn)
                    *isMyLoggedIn = 1;
            }
            */

            if (q = StartsIWith(p, "/ebrary/")) {
                if (ebs = AdminEbrarySite(q, 0))
                    GroupMaskCopy(gm, ebs->gm);
            }

            if (q = StartsIWith(p, "/overdrive/")) {
                if (ods = AdminOverDriveSite(q, 0))
                    GroupMaskCopy(gm, ods->gm);
            }

            if ((q = StartsIWith(p, "/gartner")) && (*q == 0 || *q == '/' || *q == '?')) {
                GroupMaskCopy(gm, gartnerGm);
            }

            if ((q = StartsIWith(p, "/userObject")) && (*q == 0 || *q == '/' || *q == '?')) {
                if (isMyLoggedIn)
                    *isMyLoggedIn = 1;
            }

            if ((q = StartsIWith(p, "/cas/login")) && (*q == 0 || *q == '/' || *q == '?')) {
                /* Check to see if 'gateway' parameter is included;
                   if not, set isMyLoggedIn, which tells the
                   main login process that the user must be
                   authenticated to proceed
                */
                char *gateway = GetSnipField(q, "gateway", FALSE, TRUE);
                if (gateway == NULL && isMyLoggedIn != NULL) {
                    *isMyLoggedIn = 1;
                }
                FreeThenNull(gateway);
            }

            if (strnicmp(p, WSLOGGEDIN, strlen(WSLOGGEDIN)) == 0) {
                size_t len = strlen(WSLOGGEDIN);
                char *q, *r, *s;

                q = p + len;
                if (*q == '/') {
                    q++;
                    if (r = strchr(q, '/')) {
                        if (!(s = calloc(1, r - q + 1)))
                            PANIC;
                        /* We get the null terminator automatically by the previous calloc */
                        memcpy(s, q, r - q);
                        MakeGroupMask(gm, s, 0, '=');
                        FreeThenNull(s);
                    }
                }
            }
        }

        if (p == NULL || strnicmp(p, "/login/", 7) != 0) {
            return 0;
        }

        p += 7;
        /* Check for the /login/2/groupname/dbname format, and if so, skip the 2/groupname/ */
        if (strnicmp(p, "2/", 2) == 0) {
            p += 2;
            login2Group = p;
            p = strchr(p, '/');
            if (p == NULL)
                return 0;
            p++;
        }

        if (slashQuestion = FirstSlashQuestion(p))
            plen = slashQuestion - p;
        else
            plen = strlen(p);
    }

    if (!me)
        for (i = 0, h = hosts; i < cHosts; i++, h++)
            if ((port == h->remport && strnicmp(host, h->hostname, len) == 0 &&
                 len == strlen(h->hostname)) ||
                (myHost && port == h->myPort)) {
                if (h->ftpgw != ftpgw || h->useSsl != useSsl)
                    continue;
                b = h->myDb;
                goto TransferMask;
            }

    for (i = 0, b = databases; i < cDatabases; i++, b++) {
        /* Check to see if host is within the url provided for this database */
        if (me) {
            if (b->getName != NULL && strnicmp(b->getName, p, plen) == 0 &&
                strlen(b->getName) == plen) {
                if (gm) {
                    struct DATABASE *rb;
                    GroupMaskCopy(gm, b->gm);
                    for (rb = b->getRelated; rb; rb = rb->getRelated) {
                        GroupMaskOr(gm, rb->gm);
                    }
                    if (login2Group) {
                        size_t len;

                        len = p - login2Group - 1;
                        if (len == 0)
                            GroupMaskClear(gm);
                        else {
                            char *tg;
                            if (!(tg = malloc(len + 1)))
                                PANIC;
                            memcpy(tg, login2Group, len);
                            *(tg + len) = 0;
                            if (!SingletonGroupName(tg))
                                GroupMaskClear(gm);
                            else {
                                GROUPMASK limitMask = MakeGroupMask(NULL, tg, 0, '=');
                                GroupMaskAnd(gm, limitMask);
                                GroupMaskFree(&limitMask);
                            }
                            FreeThenNull(tg);
                        }
                    }
                }

                if (rb)
                    *rb = b;
                if (isMe)
                    *isMe = 2;
                return 0;
            }
            continue;
        }
        if (b->url && strnicmp(b->url, "http://", 7) == 0) {
            p = SkipHTTPslashesAt(b->url, NULL);
            if (strnicmp(p, host, len) == 0) {
                p += len;
                if (*p == 0 || *p == '/' || *p == ':') {
                    mport = (*p == ':' ? atoi(p + 1) : 80);
                    if (port == mport)
                        goto TransferMask;
                }
            }
        }

        for (m = b->dbdomains; m; m = m->next) {
            /* don't have to check wildcard domains here since hostOnly only allowed here */
            if (m->hostOnly == 0 || m->len != len || strnicmp(m->domain, host, len) != 0)
                continue;
            if (m->ftpgw != ftpgw || m->useSsl != useSsl)
                continue;
            if (m->port == 0) {
                if (port == defaultPort) {
                    goto TransferMask;
                }
            } else {
                if (port == m->port)
                    goto TransferMask;
            }
        }
    }
    return 2;

TransferMask:
    if (rb)
        *rb = b;

    if (gm && b) {
        GroupMaskCopy(gm, b->gm);
        if (b->fmgConfigs) {
            char *query;
            if (query = strchr(host, '?')) {
                char *inst;
                if (inst = GetSnipField(query, "inst", 0, 1)) {
                    struct FMGCONFIG *fmgc;
                    BOOL first = 1;
                    for (fmgc = b->fmgConfigs; fmgc; fmgc = fmgc->next) {
                        if (stricmp(fmgc->inst, inst) == 0) {
                            if (first) {
                                GroupMaskClear(gm);
                                first = 0;
                            }
                            GroupMaskOr(gm, fmgc->gm);
                        }
                    }
                    FreeThenNull(inst);
                }
            }
        }
    }

    return 0;
}

/* VerifyHost2 can be used with bare host names or with URLs */
int VerifyHost(char *host, GROUPMASK gm, struct DATABASE **rb) {
    return VerifyHost2(host, gm, rb, NULL, NULL);
}

/**
 * Creates the socket, binds, listens and sets to non-blocking
 */
SOCKET StartListener(PORT port, struct sockaddr_storage *mysl, int backlog) {
    SOCKET s;
    struct sockaddr_storage sl;
    int rc;
    int one = 1;

    int family = (mysl->ss_family == 0) ? UUDEFAULT_FAMILY : mysl->ss_family;

    s = MoveFileDescriptorHigh(socket(family, SOCK_STREAM, 0));
    if (debugLevel > 5)
        Log("%" SOCKET_FORMAT " Open socket, errno=%d", s, (s == INVALID_SOCKET) ? errno : 0);
    if (s == INVALID_SOCKET) {
        if (debugLevel > 5)
            Log("StartListener MFDH err=%d for port=%" PORT_FORMAT, socket_errno, port);
        return s;
    }

    rc = setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *)&one, sizeof(one));
    if (rc != 0) {
        if (debugLevel > 5)
            Log("StartListener SS err=%d for port=%" PORT_FORMAT, socket_errno, port);
        closesocket(s);
        return INVALID_SOCKET;
    }

    if (mysl == NULL) {
        init_storage(&sl, family, TRUE, port);
    } else {
        memcpy(&sl, mysl, sizeof(sl));
    }
    set_port(&sl, port);

    // check if the interface is not set, aka unspecified
    if (is_unspecified(&sl)) {
        struct addrinfo *ai, hints;
        int status;
        char buffer[6];

        memset(&hints, 0, sizeof hints);
        hints.ai_family = family;
        hints.ai_protocol = SOCK_STREAM;
        hints.ai_canonname = NULL;
        hints.ai_flags = AI_PASSIVE;
        hints.ai_protocol = IPPROTO_TCP;

        sprintf(buffer, "%" PORT_FORMAT, port);

        if ((status = getaddrinfo(NULL, buffer, &hints, &ai)) != 0) {
            Log("Error system call getaddrinfo() for default interface on port %s. %s\n", buffer,
                gai_strerror(status));
            return INVALID_SOCKET;
        }

        if (ai->ai_family != family) {
            Log("Failed to lookup localhost interface for selected family (%d).", family);
            return INVALID_SOCKET;
        }

        memcpy(&sl, ai->ai_addr, ai->ai_addrlen);
        set_port(&sl, port);

        if (debugLevel > 9000) {
            char ipbuffer[INET6_ADDRSTRLEN];
            Log("Found local default interface %s for port %d",
                ipToStr(&sl, ipbuffer, sizeof ipbuffer), get_port(&sl));
        }
    }

    rc = bind(s, (struct sockaddr *)&sl, get_size(&sl));

    if (debugLevel > 9000) {
        char buffer[INET6_ADDRSTRLEN];
        Log("Binding to interface %s on port %d", ipToStr(&sl, buffer, sizeof buffer),
            get_port(&sl));
    }

    if (rc == SOCKET_ERROR) {
        if (debugLevel > 5)
            Log("StartListener bind err=%d for port=%" PORT_FORMAT, socket_errno, port);
        closesocket(s);
        return INVALID_SOCKET;
    }

    rc = listen(s, backlog);
    if (rc != 0) {
        if (debugLevel > 5)
            Log("StartListener listen err=%d for port=%" PORT_FORMAT, socket_errno, port);
        closesocket(s);
        return INVALID_SOCKET;
    }

    /* Set socket not to block so a mid-stream canceled accept won't hang, plus can try to accept
       multiple times for high volume incoming traffic */
    /* Solaris 2.4 may not like this, check NU carefully */
    SocketNonBlocking(s);
    SocketSetBufferSize(s);

    return s;
}

BOOL IsALoginPort(PORT port) {
    struct LOGINPORT *lp;
    int i;

    for (i = 0, lp = loginPorts; i < cLoginPorts; i++, lp++)
        if (lp->port == port)
            return 1;
    return 0;
}

BOOL IsAHostPort(PORT port) {
    int i;
    struct HOST *h;

    for (i = 1, h = hosts + i; i < cHosts; i++, h++) {
        if (h->myPort && h->myPort == port)
            return 1;
    }
    return 0;
}

BOOL IsAnUnrecommendedPort(PORT port) {
    return port == 2049 || port == 2401 || port == 2064 || port == 2115 || port == 2140;
}

BOOL IsAReservedPort(PORT port) {
    int i;
    struct DATABASE *b;
    struct INDOMAIN *m;

    for (i = 0, b = databases; i < cDatabases; i++, b++) {
        for (m = b->dbdomains; m; m = m->next) {
            if (m->forceMyPort == port)
                return 1;
        }
    }

    return 0;
}

BOOL IsAnActivePort(PORT port) {
    return IsALoginPort(port) || IsAHostPort(port);
}

BOOL IsMyHost(char *host) {
    char useSsl;
    char *p;
    size_t len;
    PORT port;
    BOOL myHost;

    host = SkipHTTPslashesAt(host, &useSsl);

    char *colon;
    ParseHost(host, &colon, &p, TRUE);

    len = p - host;

    port = colon ? atoi(colon + 1) : (useSsl ? 443 : 80);

    /* "login." case now checked in ValidRefererName */
    myHost = (len == strlen(myName) && strnicmp(host, myName, len) == 0) ||
             ValidRefererHostname(host, len, NULL);

    /* Always recognize our own administrative server name */
    return myHost && IsALoginPort(port);
}

PORT NextPortFrom(PORT n) {
    if (n == 0)
        n = firstPort + 1;

    while (IsAnActivePort(n) || IsAReservedPort(n) || IsAnUnrecommendedPort(n))
        n++;

    return n;
}

PORT NextPort(int i) {
    PORT n;

    if (i == 0)
        return primaryLoginPort;

    n = firstPort + 1;

    while (IsAnActivePort(n) || IsAReservedPort(n))
        n++;

    return n;
}

struct REDIRECTSAFEORNEVERPROXYURLDATABASE *RedirectSafeOrNeverProxyHost(
    char *host,
    PORT port,
    BOOL includeRedirectSafe,
    struct REDIRECTSAFEORNEVERPROXYURLDATABASE *RedirectSafeOrNeverProxyURLDatabase) {
    struct REDIRECTSAFEORNEVERPROXY *np;
    char *p;

    if (stricmp(host, MVHOSTNAME) == 0 && (port == 80 || port == 443)) {
        RedirectSafeOrNeverProxyURLDatabase->result = 0;
        return RedirectSafeOrNeverProxyURLDatabase;
    }

    for (np = redirectSafeOrNeverProxy; np; np = np->next) {
        if (debugLevel > 100) {
            Log("RedirectSafeOrNeverProxyHost check: port %d, domain %s, redirectSafe %d, anyWild "
                "%d",
                np->port, np->domain, np->redirectSafe, np->anyWild);
        }
        if (np->port == 0 || np->port == port) {
            if (np->anyWild) {
                if (WildCompare(host, np->domain)) {
                    RedirectSafeOrNeverProxyURLDatabase->result = 1;
                    RedirectSafeOrNeverProxyURLDatabase->database = np->database;
                    return RedirectSafeOrNeverProxyURLDatabase;
                }
            } else {
                if (np->redirectSafe) {
                    if (includeRedirectSafe) {
                        // RedirectSafe matches in a domain manner
                        if ((p = EndsIWith(host, np->domain)) && (p == host || *(p - 1) == '.')) {
                            RedirectSafeOrNeverProxyURLDatabase->result = 1;
                            RedirectSafeOrNeverProxyURLDatabase->database = np->database;
                            return RedirectSafeOrNeverProxyURLDatabase;
                        }
                    }
                } else {
                    // NeverProxy matches only with host exact
                    if (stricmp(host, np->domain) == 0) {
                        RedirectSafeOrNeverProxyURLDatabase->result = 1;
                        RedirectSafeOrNeverProxyURLDatabase->database = np->database;
                        return RedirectSafeOrNeverProxyURLDatabase;
                    }
                }
            }
        }
    }
    RedirectSafeOrNeverProxyURLDatabase->result = 0;
    return RedirectSafeOrNeverProxyURLDatabase;
}

struct REDIRECTSAFEORNEVERPROXYURLDATABASE *RedirectSafeOrNeverProxyURL(
    char *url,
    BOOL includeRedirectSafe,
    struct REDIRECTSAFEORNEVERPROXYURLDATABASE *RedirectSafeOrNeverProxyURLDatabase) {
    char hostCopy[1024];
    char *host;
    char *endOfHost;
    PORT port;
    size_t len;

    ParseProtocolHostPort(url, NULL, NULL, &host, &endOfHost, NULL, &port, NULL, NULL);

    if (host == NULL) {
        RedirectSafeOrNeverProxyURLDatabase->result = 1;
        RedirectSafeOrNeverProxyURLDatabase->database = NULL;
        return RedirectSafeOrNeverProxyURLDatabase;
    }

    len = endOfHost ? endOfHost - host : strlen(host);

    if (len >= sizeof(hostCopy)) {
        RedirectSafeOrNeverProxyURLDatabase->result = 1;
        RedirectSafeOrNeverProxyURLDatabase->database = NULL;
        return RedirectSafeOrNeverProxyURLDatabase;
    }

    memcpy(hostCopy, host, len);
    hostCopy[len] = 0;

    RedirectSafeOrNeverProxyURLDatabase = RedirectSafeOrNeverProxyHost(
        hostCopy, port, includeRedirectSafe, RedirectSafeOrNeverProxyURLDatabase);
    return RedirectSafeOrNeverProxyURLDatabase;
}

/* In ClusterFindHost, the host is required to be all lower-case */
/* Locate the port number of a host, activating a listener if a new port is assigned */
struct HOST *ClusterFindHost(char *host, PORT remport, char useSsl, char ftpgw, BOOL fromCluster) {
    struct HOST *retHost = NULL;
    int i;
    int offset;
    struct DATABASE *b, *myDb;
    struct INDOMAIN *m;
    BOOL match, matched;
    struct VALIDATE *validate;
    int tries;
    BOOL javaScript;
    struct HOST *h;
    struct PEERMSG msg;
    int remain;
    size_t hostLen;
    BOOL mvLookup = 0;
    char ipBuffer[INET6_ADDRSTRLEN];
    struct HOST findHost;
    int cHostsStart;
    static struct NEVERPROXYHOST *neverProxyHostsNext = NULL;
    struct NEVERPROXYHOST nph;

    if (useSsl && UUSSLActive() == 0)
        goto Done;

    hostLen = strlen(host);

    if (cHosts > 0 && hostLen >= myNameLen && stricmp(host + hostLen - myNameLen, myName) == 0) {
        /* Check if they are trying to proxy a proxy, and if so, stop it */
        if (IsAnActivePort(remport)) {
            if (debugLevel > 100) {
                Log("trying to proxy a proxy, host %s, myName %s", host, myName);
            }
            goto Done;
        }
    }

    if (stricmp(host, MVHOSTNAME) == 0 && (remport == 80 || remport == 443) && ftpgw == 0)
        mvLookup = 1;

    nph.host[0] = 0;

    findHost.hostname = host;
    findHost.remport = remport;
    findHost.useSsl = useSsl;
    findHost.ftpgw = ftpgw;

    RWLAcquireRead(&rwlHosts);
    cHostsStart = cHosts;
    /* if (h->remport == remport && strcmp(host, h->hostname) == 0 && h->useSsl == useSsl &&
     * h->ftpgw == ftpgw) */
    if (retHost = UUAvlTreeFind(&avlHosts, &findHost)) {
        RWLReleaseRead(&rwlHosts);
        if (debugLevel > 100) {
            Log("ClusterFindHost, TreeFind TRUE, %s", host);
        }
        goto Done;
    }

    if (hostLen <= MAXNEVERPROXYHOST) {
        strcpy(nph.host, host);
        nph.port = remport;
        nph.useSsl = useSsl;
        nph.ftpgw = ftpgw;
        if (UUAvlTreeFind(&avlNeverProxyHosts, &nph)) {
            /* retHost is still NULL, so we pass this on as host not found */
            RWLReleaseRead(&rwlHosts);
            if (debugLevel > 100) {
                Log("ClusterFindHost, NeverProxyHost TRUE, %s", host);
            }
            goto Done;
        }
    }
    RWLReleaseRead(&rwlHosts);

    struct REDIRECTSAFEORNEVERPROXYURLDATABASE *RedirectSafeOrNeverProxyURLDatabase;
    if (!(RedirectSafeOrNeverProxyURLDatabase =
              calloc(1, sizeof(struct REDIRECTSAFEORNEVERPROXYURLDATABASE))))
        PANIC;
    RedirectSafeOrNeverProxyURLDatabase =
        RedirectSafeOrNeverProxyHost(host, remport, FALSE, RedirectSafeOrNeverProxyURLDatabase);

    if (cHosts > 0 && RedirectSafeOrNeverProxyURLDatabase->result) {
        /* cHosts > 0 because if cHosts == 0 it's the primary loginp ort */
        if (debugLevel > 100) {
            Log("ClusterFindHost, (cHosts > 0 && RedirectSafeOrNeverProxyURLDatabase->result, %s",
                host);
        }
        FreeThenNull(RedirectSafeOrNeverProxyURLDatabase);
        goto NeverProxy;
    }
    FreeThenNull(RedirectSafeOrNeverProxyURLDatabase);

    validate = NULL;
    javaScript = 0;
    myDb = NULL;
    match = matched = (cHosts == 0 || mvLookup);

    for (i = 0, b = databases; !match && i < cDatabases; i++, b++) {
        if (ftpgw && b->optionProxyFtp == 0)
            continue;
        for (m = b->dbdomains; !match && m; m = m->next) {
            if (m->wildcard) {
                match = WildCompare(host, m->domain);
            } else {
                offset = hostLen - m->len;
                if (offset < 0)
                    continue;
                if (m->hostOnly && offset != 0)
                    continue;
                if (match = strnicmp(m->domain, host + offset, m->len) == 0)
                    if (offset > 0 && *(host + offset - 1) != '.')
                        match = 0;
            }
            if (match && m->port != 0 && m->port != remport)
                match = 0;
            if (match) {
                matched = 1;
                if (m->validate != NULL && validate == NULL)
                    validate = m->validate;
                if (myDb == NULL)
                    myDb = b;
                javaScript = m->javaScript;
                if (javaScript == 0 && b->anyJavaScript)
                    match = 0;
            }
        }
    }

    if (!matched) {
        if (debugLevel > 100) {
            Log("ClusterFindHost, db matched FALSE, %s", host);
        }
        goto NeverProxy;
    }

    /* the AVL tree lookup that comes earlier handles this now
    for (i = 0, h = hosts; i < cHosts; i++, h++) {
        if (h->remport == remport && strcmp(host, h->hostname) == 0 && h->useSsl == useSsl &&
    h->ftpgw == ftpgw) { retHost = h; goto Done;
        }
    }
    */

    if (cHosts > 0 && cPeers > 0 && fromCluster == 0) {
        ClusterInitMsg(&msg, 'A', 'H', 0);
        remain = sizeof(msg.data);
        EncodeFields(msg.data, &remain, EDACHAR, host, EDSHORT, (int)remport, EDSTOP);
        msg.datalen = sizeof(msg.data) - remain;
        ClusterRequest(&msg);
    }

    RWLAcquireWrite(&rwlHosts);
    /* Now that we have mutex, check to see if any hosts were added concurrent to our previous check
     */
    for (i = cHostsStart, h = hosts + i; i < cHosts; i++, h++) {
        if (h->remport == remport && stricmp(host, h->hostname) == 0 && h->useSsl == useSsl &&
            h->ftpgw == ftpgw) {
            RWLReleaseWrite(&rwlHosts);
            retHost = h;
            if (debugLevel > 100) {
                Log("ClusterFindHost, host was added concurrent, %s", host);
            }
            goto Done;
        }
    }

    if (i == mHosts) {
        /* the hosts structure has extra space pre-allocated for the mvlookup hosts;
           if they weren't found, up mHosts to take advantage of this space just for them
        */
        if (mvLookup)
            mHosts++;
        else {
            RWLReleaseWrite(&rwlHosts);
            retHost = (useSsl ? mvHostSsl : mvHost);
            Log("%s MaxVirtualHosts at limit; unable to proxy %s:%d", EZPROXYCFG, host, remport);
            goto Done;
        }
    }

    h->hostname = host;
    h->remport = remport;
    RestoreValidations(h);

    if ((optionProxyType == PTHOST && h->forceMyPort == 0) || cHosts == 0) {
        /* This is a login port on this host (or cluster).  We don't test
         * its availability here because the whole exec inst dies if the port isn't available. */
        /* NOTE: rebuildMainMaster or StartLoginPorts() starts the listener. */
        h->myPort = 0;
        h->socket = INVALID_SOCKET;
        if (debugLevel > 100) {
            Log("ClusterFindHost, (optionProxyType == PTHOST && h->forceMyPort == 0) || cHosts == "
                "0, %s",
                host);
        }
    } else {
        /* Test the requested port for availability or find an available port. */
        h->myPort = (h->forceMyPort ? h->forceMyPort : NextPort(i));

        for (tries = 0;; h->myPort++) {
            if (IsAnActivePort(h->myPort))
                continue;
            if (h->forceMyPort) {
                if (tries > 0) {
                    Log("%s:%d cannot be assigned to %d (in use, will assign alternate)",
                        h->hostname, h->remport, h->forceMyPort);
                    h->forceMyPort = 0;
                    h->myPort = NextPort(i);
                    continue;
                }
            } else {
                if (IsAnUnrecommendedPort(h->myPort))
                    continue;
            }
            /* The following is just to prove that the port is available.  Listeners are really
             * started in rebuildMainMaster or StartLoginPorts(). */
            h->socket = StartListener(h->myPort, &defaultIpInterface, hostSocketBacklog);
            if (h->socket != INVALID_SOCKET)
                break;
            if (i == 0) {
                /* i == 0 means that this is the first attempt to start a listener which is the
                 * login port. */
                Log("Unable to listen on %s:%d (err=%d); another copy of EZproxy or another web "
                    "server may be "
                    "running",
                    IPInterfaceName(ipBuffer, sizeof ipBuffer, &loginPorts[0].ipInterface),
                    h->myPort, socket_errno);
                guardian->portOpenFailure |= GCPORTFAILED;
                RWLReleaseWrite(&rwlHosts);
                goto Done;
            }
#ifndef WIN32
            if (geteuid() != 0 && h->myPort < 1024) {
                h->myPort = 1024;
                continue;
            }
#endif
            Log("Unable to create virtual web server on %s:%" PORT_FORMAT " (err=%d)",
                IPInterfaceName(ipBuffer, sizeof ipBuffer, &defaultIpInterface), h->myPort,
                socket_errno);
            tries++;
            if (tries == 100) {
                RWLReleaseWrite(&rwlHosts);
                Log("Unable to allocate port for new virtual host, %s", host);
                goto Done;
            }
        }
    }

    if (!(h->hostname = strdup(host)))
        PANIC;
    h->validate = validate;
    h->javaScript = javaScript;
    h->ftpgw = ftpgw;
    if (i == 0) {
        /* i == 0 means that this is the first attempt to start a listener which is the login port.
         */
        h->remport = primaryLoginPort;
    } else {
        h->remport = remport;
    }
    h->useSsl = useSsl;
    /* This logic is now performed by RestoreValidations, so don't duplicate
       and cause a miscount of active hosts

    if (i != 0) {
        if (h->myDb = myDb)
            myDb->hostCount++;
    }
    */
    SetProxyHostname(h);
    UUtime(&h->created);
    /* h->refernced and h->references handled at the end of this function */
    /*  This can't be a NeverProxy host or we wouldn't be here
        since the NeverProxyHost test above would preclude this
    h->neverProxy = NeverProxyHost(host, remport);
    */
    if (debugLevel > 0) {
        Log("Listener created for %s:%d on %s:%" PORT_FORMAT ", socket %" SOCKET_FORMAT,
            h->hostname, h->remport, myName, (h->myPort ? h->myPort : primaryLoginPort), h->socket);
    }
    cHosts++;
    UUAvlTreeInsert(&avlHosts, h);
    h->active = 1;
    if (rebuildMainMaster == 0)
        rebuildMainMaster = 1;
    NeedSave();
    RWLReleaseWrite(&rwlHosts);
    retHost = h;

    if (mvLookup) {
        if (remport == 443)
            mvHostSsl = retHost;
        else
            mvHost = retHost;
    }

    goto Done;

NeverProxy:
    if (nph.host[0] == 0)
        goto Done;

    RWLAcquireWrite(&rwlHosts);

    if (UUAvlTreeFind(&avlNeverProxyHosts, &nph) == NULL) {
        if (neverProxyHostsNext == NULL)
            neverProxyHostsNext = neverProxyHosts;

        if (neverProxyHostsNext->host[0]) {
            UUAvlTreeDelete(&avlNeverProxyHosts, neverProxyHostsNext);
        }

        memcpy(neverProxyHostsNext, &nph, sizeof(*neverProxyHostsNext));
        UUAvlTreeInsert(&avlNeverProxyHosts, neverProxyHostsNext);
        if (neverProxyHostsNext == neverProxyHostsLast)
            neverProxyHostsNext = neverProxyHosts;
        else
            neverProxyHostsNext++;
    }
    RWLReleaseWrite(&rwlHosts);
    goto Done;

Done:
    if (retHost) {
        UUtime(&retHost->referenced);
        retHost->references++;

        if (retHost->neverProxy)
            retHost = NULL;
    }

    return retHost;
}

struct HOST *FindHost(char *ihost, char useSsl, char ftpgw) {
    char *colon;
    PORT remport;
    size_t len;
    struct HOST *retHost;
    char *p;
    char host[256];

    StrCpy3(host, ihost, sizeof(host));

    colon = NULL;
    if (debugLevel > 8)
        Log("Before: '%s'", host);

    BOOL bracket = FALSE;
    for (p = host; *p;) {
        if (*p == '[') {
            bracket = TRUE;
            StrCpyOverlap(p, p + 1);

        } else if (*p == ']' && bracket) {
            bracket = FALSE;
            StrCpyOverlap(p, p + 1);

        } else if (*p <= ' ') {
            StrCpyOverlap(p, p + 1);

        } else if (*p == ':' && !bracket) {
            *p++ = 0;
            colon = p;

        } else {
            if (isupper(*p))
                *p = tolower(*p);
            p++;
        }
    }
    /* Remove any trailing period from hostname so it can be recognized */
    len = strlen(host);
    if (len > 1 && host[len - 1] == '.') {
        len--;
        host[len] = 0;
    }
    if (debugLevel > 8)
        Log("After: '%s'", host);

    remport = 0;
    if (colon)
        remport = atoi(colon);
    if (remport == 0)
        remport = (ftpgw ? 21 : (useSsl ? 443 : 80));

    /* NOTE: host has been changed to lower case, as required by ClusterFindHost */
    retHost = ClusterFindHost(host, remport, useSsl, ftpgw, 0);
    return retHost;
}

/* This is used by the NetLibrary support to search for a database that matches the
   user's group
*/
struct DATABASE *NextMatchingDatabase(struct DATABASE *b, struct HOST *h) {
    int i;
    PORT remport;
    int offset;
    size_t len;
    struct INDOMAIN *m;
    BOOL match;
    BOOL anyMatch;
    char *host;

    host = h->hostname;
    len = strlen(host);
    remport = h->remport;
    match = 0;
    anyMatch = 0;

    if (b == NULL)
        b = databases;
    else
        b++;

    for (i = databases - b; !anyMatch && i < cDatabases; i++, b++) {
        for (m = b->dbdomains; m; m = m->next) {
            if (m->wildcard) {
                match = WildCompare(host, m->domain);
            } else {
                offset = len - m->len;
                if (offset < 0)
                    continue;
                if (m->hostOnly && offset != 0)
                    continue;
                if (match = strnicmp(m->domain, host + offset, m->len) == 0)
                    if (offset > 0 && *(host + offset - 1) != '.')
                        match = 0;
            }
            if (match && m->port != 0 && m->port != remport)
                match = 0;
            if (match)
                return b;
        }
    }

    return NULL;
}

struct DATABASE *MatchingDatabase(struct DATABASE *b,
                                  char *host,
                                  PORT remport,
                                  struct VALIDATE **validate,
                                  PORT *forceMyPort,
                                  BOOL *gateway,
                                  BOOL *javaScript) {
    int offset;
    size_t len;
    struct INDOMAIN *m;
    BOOL match;
    BOOL anyMatch;

    if (b == NULL)
        b = databases;

    if (validate)
        *validate = NULL;
    if (forceMyPort)
        *forceMyPort = 0;
    if (gateway)
        *gateway = 0;
    if (javaScript)
        *javaScript = 0;

    len = strlen(host);
    match = 0;
    anyMatch = 0;
    for (; b < databases + cDatabases; b++) {
        for (m = b->dbdomains; m; m = m->next) {
            if (m->wildcard) {
                if (WildCompare(host, m->domain) == 0)
                    continue;
            } else {
                offset = len - m->len;
                if (offset < 0)
                    continue;
                if (m->hostOnly && offset != 0)
                    continue;
                if (strnicmp(m->domain, host + offset, m->len) != 0)
                    continue;
                if (offset > 0 && *(host + offset - 1) != '.')
                    continue;
            }
            if (m->port != 0 && m->port != remport)
                continue;
            anyMatch = 1;
            if (m->validate != NULL && validate && *validate == NULL)
                *validate = m->validate;
            if (m->forceMyPort && forceMyPort)
                *forceMyPort = m->forceMyPort;
            if (m->gateway && gateway)
                *gateway = 1;
            if (javaScript)
                *javaScript |= m->javaScript;
        }
        if (anyMatch)
            return b;
    }

    return NULL;
}

void RestoreValidations(struct HOST *h) {
    int i;
    PORT remport;
    int offset;
    size_t len;
    struct DATABASE *b;
    struct INDOMAIN *m;
    BOOL match;
    BOOL anyMatch;
    char *host;

    host = h->hostname;
    len = strlen(host);
    remport = h->remport;
    match = 0;
    anyMatch = 0;
    if (strcmp(h->hostname, MVHOSTNAME) == 0)
        return;

    for (i = 0, b = databases; !anyMatch && i < cDatabases; i++, b++) {
        for (m = b->dbdomains; m; m = m->next) {
            if (m->wildcard) {
                match = WildCompare(host, m->domain);
            } else {
                offset = len - m->len;
                if (offset < 0)
                    continue;
                if (m->hostOnly && offset != 0)
                    continue;
                if (match = strnicmp(m->domain, host + offset, m->len) == 0)
                    if (offset > 0 && *(host + offset - 1) != '.')
                        match = 0;
            }
            if (match && m->port != 0 && m->port != remport)
                match = 0;
            if (match) {
                anyMatch = 1;
                if (m->validate != NULL && h->validate == NULL)
                    h->validate = m->validate;
                if (m->forceMyPort)
                    h->forceMyPort = m->forceMyPort;
                if (m->gateway)
                    h->gateway = 1;
                h->javaScript |= m->javaScript;
                if (h->javaScript == 0 && b->anyJavaScript)
                    match = 0;
                if (h->myDb == NULL) {
                    if (h->myDb = b) {
                        b->hostCount++;
                    }
                }
            }
        }
    }
}

BOOL ValidMenu(char *s) {
    if (s == NULL || *s == 0 || *s == '.' || strlen(s) >= MAXMENU - strlen(DOCSDIR))
        return 0;

    for (; *s; s++) {
        if (IsAlpha(*s) || IsDigit(*s) || *s == '_' || *s == '.')
            continue;
        return 0;
    }

    if (CheckIfDevice(s))
        return 0;

    return 1;
}

BOOL ValidDocsCustomDir(char *s) {
    if (*s == 0 || *s == '.' || strlen(s) >= MAXDOCSCUSTOMDIR)
        return 0;

    for (; *s; s++) {
        if (IsAlpha(*s) || IsDigit(*s) || *s == '_' || *s == '.')
            continue;
        return 0;
    }

    if (CheckIfDevice(s))
        return 0;

    return 1;
}

void LoadHosts(void) {
    FILE *f;
    struct HOST *h;
    char line[256];
    char *args;
    unsigned int sessionFlags;
    int v1, v2, v3, v4, v5, v6, v8, v9;
    unsigned int v7;
    char option;
    char *arg1;
    struct SESSION *e;
    char menu[MAXMENU];
    int v;
    /*  struct AUTHHOST *a; */
    BOOL firstGroup = 0;
    char *restoreUsageUser = NULL;
    time_t now;
    BOOL validSession = FALSE;

    guardianPid = 0;
    chargePid = 0;

    potentialListeners = cLoginPorts;

    hostsSaved = 1;
    /* Happens only at startup, so there will be a file descriptor available to fopen */
    TEMP_FAILURE_RETRY_FILE(f = interrupted_null_file(fopen(HOSTFILE, "r")));
    if (f == NULL) {
        return;
    }
    h = hosts + cHosts;
    /* the + cSessions skips over the anonymous session, if present */
    e = sessions + cSessions;

    UUtime(&now);
    lastModifiedMinimum = now;

    while (fgets(line, sizeof(line), f)) {
        Trim(line, TRIM_COMPRESS);
        if (line[0] == '#')
            continue;

        args = strchr(line, ' ');
        if (args == NULL)
            continue;
        *args++ = 0;
        if (strlen(line) > 1) {
            /* Old format so make a note to save right after this */
            NeedSave();
            arg1 = line;
            option = 'H';
        } else {
            option = line[0];
            arg1 = args;
            /* For g group code, allow spaces in the group name by not looking for space or
             * overwriting the space */
            if (option == 'g')
                args = NULL;
            else {
                args = strchr(arg1, ' ');
                if (args == NULL) {
                    if (option != 'U' && option != 'L' && option != 'P' && option != 'C' &&
                        option != 'c' && option != 'I' && option != 'T' && option != 's' &&
                        option != 'A') {
                        continue;
                    }
                } else
                    *args++ = 0;
            }
        }

        if (option == 'M') {
            /* If the saved EZPROXYCFG MD5 hash doesn't match, set the minimum last modified date
               to now.  This is used to help invalidate old files, particularly those with
               JavaScript, that might be parsed differently due to EZPROXYCFG changes */
            if (strcmp(arg1, ezproxyCfgDigest) == 0) {
                /* Since the hash is the same, use the original version of minimumLastModified */
                if (lastModifiedMinimum = atoi(args))
                    continue;
            }
            UUtime(&lastModifiedMinimum);
            continue;
        }

        if (option == 'P') {
            if (args == NULL)
                guardianPid = atoi(arg1);
            else {
                chargePid = atoi(arg1);
#ifdef WIN32
                if (guardianSharedMemoryID == NULL)
                    guardianSharedMemoryID = strdup(args);
#else
                if (guardianSharedMemoryID == 0)
                    guardianSharedMemoryID = atoi(args);
#endif
            }
        }

        if (option == 'H' && cHosts < mHosts) {
            /* v8 for new SSL option, will be missing in older files */
            v8 = 0;
            if (sscanf(args, "%d %d %d %d %d %d %d %d", &v1, &v2, &v3, &v4, &v5, &v6, &v9, &v8) < 7)
                continue;
            if (cHosts == mHosts) {
                Log("All virtual hosts in use, unable to restore host");
                continue;
            }
            if (debugLevel > 0)
                Log("Restore host %s %s", arg1, args);
#ifndef WIN32
            /* Don't restore privileged ports if we're going to run as non-root */
            if (runUid != 0 && v2 > 0 && v2 < 1024)
                continue;
#endif
            h->remport = v1;
            h->hostname =
                arg1; /* This is just temporary for AVL check and RestoreValidations use */
            h->useSsl = (v8 & 1 ? 1 : 0);
            h->ftpgw = (v8 & 2 ? 1 : 0);
            /* Check to see if this host has already been loaded, and if so, don't restore again */
            if (UUAvlTreeFind(&avlHosts, h)) {
                continue;
            }
            RestoreValidations(h);
            if (h->forceMyPort) {
                h->myPort = h->forceMyPort;
                if (IsAnActivePort(h->myPort)) {
                    Log("%s:%d cannot be assigned to %d (in use)", h->hostname, h->remport,
                        h->myPort);
                    guardian->portOpenFailure |= GCPORTFAILED;
                    continue;
                }
                h->socket = StartListener(h->myPort, &defaultIpInterface, hostSocketBacklog);
                potentialListeners++;
                if (h->socket == INVALID_SOCKET) {
                    Log("%s:%d cannot be assigned to %d (socket open failed)", h->hostname,
                        h->remport, h->myPort);
                    WarnIfLowPort(h->myPort, 0);
                    guardian->portOpenFailure |= GCPORTFAILED;
                    continue;
                }
            } else {
                if (h->myPort = v2) {
                    if (IsAnActivePort(h->myPort))
                        continue;
                    h->socket = StartListener(h->myPort, &defaultIpInterface, hostSocketBacklog);
                    if (h->socket == INVALID_SOCKET) {
                        continue;
                    }
                    potentialListeners++;
                } else if (optionProxyType == PTPORT)
                    continue;
                else
                    h->socket = INVALID_SOCKET;
            }

            h->created = v3;
            h->referenced = v4;
            h->accessed = v5;
            h->references = v6;
            h->accesses = v9;
            if (!(h->hostname = strdup(arg1)))
                PANIC;
            AllLower(h->hostname);
            SetProxyHostname(h);

            struct REDIRECTSAFEORNEVERPROXYURLDATABASE *RedirectSafeOrNeverProxyURLDatabase;
            if (!(RedirectSafeOrNeverProxyURLDatabase =
                      calloc(1, sizeof(struct REDIRECTSAFEORNEVERPROXYURLDATABASE))))
                PANIC;
            RedirectSafeOrNeverProxyURLDatabase = RedirectSafeOrNeverProxyHost(
                h->hostname, h->remport, 0, RedirectSafeOrNeverProxyURLDatabase);

            if (RedirectSafeOrNeverProxyURLDatabase->result)
                h->neverProxy = 1;
            else if (h->myDb == NULL) {
                if (strcmp(h->hostname, MVHOSTNAME) != 0 || h->ftpgw)
                    h->neverProxy = 1;
                else {
                    if (h->remport == 80)
                        mvHost = h;
                    else if (h->remport == 443)
                        mvHostSsl = h;
                    else
                        h->neverProxy = 1;
                }
            }
            FreeThenNull(RedirectSafeOrNeverProxyURLDatabase);

            UUAvlTreeInsert(&avlHosts, h);
            h->active = 1;
            h++;
            cHosts++;
        }

        /*
        if (option == 'G') {
            if (sscanf(arg1, "%u", &oldgm) != 1 || oldgm == 0 || oldgm == 1)
                continue;
            g = FindGroup(args, 0, 0);
            if (g == NULL)
                continue;
            g->oldgm = oldgm;
        }
        */

        if (optionSingleSession) {
            /* Do not restore sessions if running in single session mode */
            continue;
        }

        if (option == 'S' && cSessions < mSessions) {
            char *createdRelogin, *period;

            // If there was no adminKey restored from the file for the last session, generate one
            if (validSession) {
                AttachAdminKey(e - 1);
            }

            /* If the session we are being asked to restore already exists
               or has already expired, do not try to restore the duplicate
               The rest of the logic in the section was changed from
               checking cSessions > 0 to looking at validSession to
               determine if session-related information can be restored
               or not.
            */

            validSession = FALSE;

            /* Disallow duplicate session */
            if (FindSessionByKey(arg1)) {
                continue;
            }

            v3 = 1;
            v4 = -1;
            v7 = 0;
            menu[0] = 0;
            sessionFlags = 0;
            createdRelogin = args;
            args = NextWSArg(args);
            char *accessedConfirmed = args;
            args = NextWSArg(args);
            char address[INET6_ADDRSTRLEN];
            if (sscanf(args, "%d %d %s %u %u %d %s", &v3, &v4, &address, &sessionFlags, &v7, &v8,
                       menu) < 1)
                continue;
            if (debugLevel > 0)
                Log("Restore session %s %s %s", arg1, createdRelogin, args);
            StrCpy3(e->key, arg1, sizeof(e->key));

            if ((period = strchr(createdRelogin, '.'))) {
                *period++ = 0;
                e->relogin = atoi(period);
                e->reloginRequired = e->relogin < now;
            } else {
                e->relogin = 0;
                e->reloginRequired = 0;
            }
            e->created = e->lastAuthenticated = atoi(createdRelogin);
            if ((period = strchr(accessedConfirmed, '.'))) {
                *period++ = 0;
                e->confirmed = atoi(period);
            } else {
                e->confirmed = e->created;
            }
            e->accessed = atoi(accessedConfirmed);
            e->expirable = v3;
            if (v4 == -1)
                e->lifetime = (e->expirable ? mSessionLife : 0);
            else
                e->lifetime = v4;

            /* Don't restore expired session */
            if (SessionExpirationReached(e, now)) {
                continue;
            }

            inet_pton_st2(address, &e->sin_addr);
            e->gm = GroupMaskDefault(e->gm);
            firstGroup = 1;
            e->sessionFlags = sessionFlags;
            if (v7)
                e->priv = (char)(v7 & 0xFF);
            if (v8 >= autoLoginByNone && v8 < autoLoginByLast)
                e->autoLoginBy = v8;
            if (ValidMenu(menu))
                sprintf(e->menu, "%s%s", DOCSDIR, menu);

            e->sessionVariables = VariablesNew("session", TRUE);
            e->identifierVariables = VariablesNew("identifiers", TRUE);
            AttachConnectKey(e);
            UUAvlTreeInsert(&avlSessions, e);
            e->active = 1;
            e++;
            cSessions++;
            validSession = TRUE;
        }

        if (option == 'A' && validSession && *arg1 != 0) {
            StrCpy3((e - 1)->adminKey, arg1, sizeof((e - 1)->adminKey));
        }

        if (option == 's' && validSession && *arg1 != 0) {
            if (atoi(arg1))
                (e - 1)->lastAuthenticated = atoi(arg1);
        }

        if (option == 'U' && validSession && *arg1 != 0) {
            if (!((e - 1)->genericUser = strdup(arg1)))
                PANIC;
            /*
            if (sscanf(args, "%d", &v1) != 1)
                continue;
            a = FindAuthHostByName(arg1);
            if (a == NULL)
                continue;
            a->accessed = v1;
            (e-1)->genericAuthHost = a;
            */
        }

        if (option == 'T' && validSession && *arg1 != 0) {
            StrCpy3((e - 1)->userObjectTicket, arg1, sizeof((e - 1)->userObjectTicket));
        }

        if (option == 'I' && validSession) {
            UUGetIpInterfaceByName(arg1, &(e - 1)->ipInterface);
            if (e->ipInterface.ss_family == AF_INET || e->ipInterface.ss_family == AF_INET6)
                anySourceIP = 1;
            continue;
        }

        if (option == 'L' && validSession && *arg1 != 0) {
            StoreLogUser(e - 1, arg1);
        }

        if (option == 'g' && validSession) {
            if (firstGroup) {
                GroupMaskClear((e - 1)->gm);
                firstGroup = 0;
            }
            if (*arg1) {
                MakeGroupMask((e - 1)->gm, arg1, 0, '+');
            }
            continue;
        }

        if (option == 'C' && validSession && *arg1 != 0) {
            if (!((e - 1)->cpipSid = strdup(arg1)))
                PANIC;
        }

        if (option == 'c' && validSession && ValidDocsCustomDir(arg1)) {
            (e - 1)->docsCustomDir = StaticStringCopy(arg1);
        }

        if (option == 'V' && validSession && *arg1 != 0 && *args != 0) {
            v = atoi(arg1);
            if (v >= 0 && v < MAXSESSIONVARS) {
                if ((e - 1)->vars[v]) {
                    free((e - 1)->vars[v]);
                    (e - 1)->vars[v] = NULL;
                }
                if (strlen(args) >= MAXUSERINFOVARLEN)
                    args[MAXUSERINFOVARLEN - 1] = 0;
                if (!((e - 1)->vars[v] = strdup(args)))
                    PANIC;
            }
        }
    }
    TEMP_FAILURE_RETRY_INT(interrupted_neg_int(fclose(f)));

    // If there was no adminKey restored from the file for the last session, generate one
    if (validSession) {
        AttachAdminKey(e - 1);
    }

    if (optionProxyType == PTPORT)
        potentialListeners = cLoginPorts + mHosts;

    FreeThenNull(restoreUsageUser);

    RestoreCookies();
}

void SaveUsage(void) {
    FILE *f = NULL;
    int fi = -1;
    struct USAGELIMIT *ul;
    struct USAGE *u;
    struct USAGEDATEHOUR *ud;
    BOOL haveMutex = 0;
    time_t now;
    static time_t nextPruneUsageLimitExceeded = 0;
    struct INTRUDERIP *bg;

    unlink(ULSFILENEW);

    /* From a performance standpoint, this function deserves the acceleration that
       the buffering of fprintf can provide.  Thus, we use open to get a low
       number file descriptor, to allow fdopen to succeed on Solaris where such
       things are limited
    */

    fi = open(ULSFILENEW, O_CREAT | O_WRONLY | O_TRUNC, defaultFileMode);
    if (fi < 0) {
        Log("Unable to create temporary file %s to save usage statistics: %d", ULSFILENEW, errno);
        goto Leave;
    }

    f = fdopen(fi, "w");
    if (f == NULL) {
        Log("Unable to fdopen temporary file %s to save usage statistics: %d", ULSFILENEW, errno);
        TEMP_FAILURE_RETRY_INT(interrupted_neg_int(close(fi)));
        goto Leave;
    }

    /* Once an hour prune out usageLimitExceededs that are no longer active */
    UUtime(&now);
    if (nextPruneUsageLimitExceeded < now) {
        /* When nextPruneUsageLimitExceeded is 0, we are at startup
           and LoadUsage will already have run a PruneUsageLimitExceeded
           so only prune if we are beyond that point
        */
        if (nextPruneUsageLimitExceeded)
            PruneUsageLimitExceeded();
        nextPruneUsageLimitExceeded = now + 3600;
    }

    for (ul = usageLimits; ul; ul = ul->next) {
        UUAcquireMutex(&mUsage);
        haveMutex = 1;
        PruneUsage(&ul->usage, ul->oldestInterval);
        for (u = ul->usage; u; u = u->next) {
            if (fprintf(f, "=%s %s\n", u->user, ul->name) < 0)
                goto Failed;
            for (ud = u->usageDateHours; ud; ud = ud->next) {
                if (fprintf(f, "+%" PRIdMAX " %u %u %u\n", (intmax_t)(ud->dateHour), ud->transfers,
                            ud->recvKBytes, ud->intruderUserAttempts) < 0)
                    goto Failed;
            }
            if (u->when) {
                if (fprintf(f, "-%" PRIdMAX "\n", (intmax_t)(u->when)) < 0)
                    goto Failed;
            }
            if (u->lastAttempt) {
                if (fprintf(f, "*%" PRIdMAX "\n", (intmax_t)(u->lastAttempt)) < 0)
                    goto Failed;
            }
        }
        UUReleaseMutex(&mUsage);
        haveMutex = 0;
    }

    for (bg = intruderIPs; bg; bg = bg->next) {
        if (bg->lastAttempt) {
            char ipBuffer1[INET6_ADDRSTRLEN];
            char ipBuffer2[INET6_ADDRSTRLEN];
            if (ipToStr(&bg->ip, ipBuffer1, sizeof(ipBuffer1)) != NULL &&
                ipToStr(&bg->forwardedIp, ipBuffer2, sizeof(ipBuffer2)) != NULL) {
                fprintf(f, "%%%s %s %" PRIdMAX " %d\n", ipBuffer1, ipBuffer2,
                        (intmax_t)(bg->lastAttempt), bg->attempts);
            }
        }
    }

    TEMP_FAILURE_RETRY_INT(interrupted_neg_int(fclose(f)));
    f = NULL;

    unlink(ULSFILE);
    rename(ULSFILENEW, ULSFILE);

    goto Leave;

Failed:
    Log("Error appending to %s: %d", ULSFILENEW, errno);
    if (f != NULL) {
        TEMP_FAILURE_RETRY_INT(interrupted_neg_int(fclose(f)));
        f = NULL;
    }

Leave:
    /* Make sure new file got deleted if something errored along the way */
    unlink(ULSFILENEW);

    if (haveMutex) {
        UUReleaseMutex(&mUsage);
        haveMutex = 0;
    }
}

void DoSaveUsage(void *v) {
    time_t lastSaved = 0, now;

    usageSaved = 0;

    for (;;) {
        ChargeAlive(GCSAVEUSAGE, &now);
        if (usageSaved == 0 || lastSaved + 300 < now) {
            lastSaved = now;
            usageSaved = 1;
            SaveUsage();
        }
        SetErrno(0);
        SubSleep(5, 0);
#ifndef WIN32
        if ((errno == ECANCELED)) {
            break;
        }
#endif
    }
    ChargeStopped(GCSAVEUSAGE, NULL);
}

void LoadUsage(void) {
    char line[1024];
    FILE *f = NULL;
    struct USAGELIMIT *ul = NULL;
    struct USAGE *u;
    struct USAGEDATEHOUR *ud;
    struct USAGELIMITEXCEEDED *ule = NULL;
    struct INTRUDERIP *bg;
    time_t restoreUsageDateHour;
    char *restoreUsageUser = NULL;
    char *arg1;
    char *arg2;
    char *arg3;
    char *arg4;
    char mode;
    time_t when, now;

    UUtime(&now);

    /* Happens only at startup, so there will be a file descriptor available to fopen */
    f = fopen(ULSFILE, "r");
    if (f == NULL) {
        return;
    }

    while (fgets(line, sizeof(line), f)) {
        Trim(line, TRIM_TRAIL);
        if (line[0] == 0)
            continue;

        arg1 = line;
        mode = *arg1++;

        arg2 = NextWSArg(arg1);

        if (mode == '%') {
            arg3 = NextWSArg(arg2);
            arg4 = NextWSArg(arg3);

            struct sockaddr_storage t1, t2;
            if (inet_pton_st2(arg1, &t1) == 1 && inet_pton_st2(arg2, &t2) == 1) {
                if ((bg = FindIntruderIP(NULL, &t1, &t2, 1))) {
                    bg->lastAttempt = atoi(arg3);
                    bg->attempts = atoi(arg4);
                }
            }
            continue;
        }

        if (mode == '=') {
            ule = NULL;
            ul = FindUsageLimit(arg2, 0);

            FreeThenNull(restoreUsageUser);

            if (ul && *arg1) {
                if (!(restoreUsageUser = strdup(arg1)))
                    PANIC;
            }
            continue;
        }

        if (ul == NULL || restoreUsageUser == NULL)
            continue;

        switch (mode) {
        case '+':
            arg3 = NextWSArg(arg2);
            arg4 = NextWSArg(arg3);
            if (*arg1 && *arg2 && *arg3) {
                restoreUsageDateHour = ((time_t)(atoi(arg1) / ul->granularity)) * ul->granularity;
                if (restoreUsageDateHour >= (time_t)(now - ul->oldestInterval) &&
                    restoreUsageDateHour <= now) {
                    /* Lie about having mutex; it's not needed to protect anything since we're in
                     * startup */
                    if (ule == NULL)
                        ule = FindUsageLimitExceeded(restoreUsageUser, 1, 1);
                    if (ule) {
                        FindUsage(&ul->usage, restoreUsageUser, restoreUsageDateHour, 1, &u, &ud);
                        if (u && ud) {
                            unsigned int x;
                            x = atoi(arg2);
                            u->transfers += x;
                            ud->transfers += x;
                            x = atoi(arg3);
                            u->recvKBytes += x;
                            ud->recvKBytes += x;
                            x = atoi(arg4);
                            u->intruderUserAttempts += x;
                            ud->intruderUserAttempts += x;
                        }
                    }
                }
            }
            break;

        case '-':
            if (*arg2 == 0)
                arg2 = restoreUsageUser;
            if (*arg1 && *arg2 && (when = atoi(arg1)) &&
                (ul->expires == 0 || (time_t)(when + ul->expires) > now)) {
                /* Lie about having mutex... */
                if (ule == NULL)
                    ule = FindUsageLimitExceeded(restoreUsageUser, 1, 1);
                if (ule && (FindUsage(&ul->usage, arg2, 0, 1, &u, NULL), u)) {
                    /* Check if u->when already defined, in which case we don't want to double-count
                       a single entry
                    */
                    if (u->when == 0) {
                        if (ul == intruderUsageLimit) {
                            ule->intruderUserAttemptsCount++;
                        } else {
                            if (ul->enforce)
                                ule->enforcedCount++;
                            else
                                ule->monitoredCount++;
                        }
                    }
                    u->when = when;
                }
            }
            break;

        case '*':
            if (*arg2 == 0)
                arg2 = restoreUsageUser;
            if (*arg1 && *arg2 && (when = atoi(arg1))) {
                /* Lie about having mutex... */
                if (ule == NULL)
                    ule = FindUsageLimitExceeded(restoreUsageUser, 1, 1);
                if (ule && (FindUsage(&ul->usage, arg2, 0, 1, &u, NULL), u)) {
                    u->lastAttempt = when;
                }
            }
        }
    }

    FreeThenNull(restoreUsageUser);

    TEMP_FAILURE_RETRY_INT(interrupted_neg_int(fclose(f)));

    PruneUsageLimitExceeded();
    PruneIntruderIP();
}

void SaveHosts(void) {
    FILE *f = NULL;
    int fi = -1;
    int i;
    struct HOST *h;
    struct GROUP *g;
    int hostsToSave;
    struct SESSION *e;
    /*  struct AUTHHOST *a; */
    int v;
    BOOL noGroups;
    char ipBuffer[INET6_ADDRSTRLEN];

#ifndef WIN32
    SetUidGid();
#endif

    if (debugLevel > 0)
        Log("Saving hosts");

    RWLAcquireRead(&rwlHosts);
    hostsSaved = 1;

    hostsToSave = cHosts;

    unlink(HOSTFILENEW);

    /* From a performance standpoint, this function deserves the acceleration that
       the buffering of fprintf can provide.  Thus, we use open to get a low
       number file descriptor, to allow fdopen to succeed on Solaris where such
       things are limited
    */

    fi = open(HOSTFILENEW, O_CREAT | O_WRONLY | O_TRUNC, defaultFileMode);
    if (fi < 0) {
        Log("Unable to create temporary file %s to save hosts: %d", HOSTFILENEW, errno);
        goto Leave;
    }

    f = fdopen(fi, "w");
    if (f == NULL) {
        Log("Unable to fdopen temporary file %s to save hosts: %d", HOSTFILENEW, errno);
        TEMP_FAILURE_RETRY_INT(interrupted_neg_int(close(fi)));
        goto Leave;
    }

    if (guardian->gpid) {
        if (fprintf(f, "P %d\n", guardian->gpid) < 0)
            goto Failed;
    }

#ifdef WIN32
    if (fprintf(f, "P %u %s\n", guardian->cpid, guardianSharedMemoryID) < 0)
        goto Failed;
#else
    if (fprintf(f, "P %u %d\n", guardian->cpid, guardianSharedMemoryID) < 0)
        goto Failed;
#endif

    if (fprintf(f, "M %s %" PRIdMAX "\n", ezproxyCfgDigest, (intmax_t)(lastModifiedMinimum)) < 0)
        goto Failed;

    for (i = 1, h = hosts + i; i < hostsToSave; i++, h++) {
        if (h->dontSave)
            continue;
        if (h->useSsl < 2) {
            if (fprintf(f,
                        "H %s %" PORT_FORMAT " %" PORT_FORMAT " %" PRIdMAX " %" PRIdMAX " %" PRIdMAX
                        " %d %d %d\n",
                        h->hostname, h->remport, h->myPort, (intmax_t)(h->created),
                        (intmax_t)(h->referenced), (intmax_t)(h->accessed), h->references,
                        h->accesses, (int)(h->useSsl + 2 * h->ftpgw)) < 0)
                goto Failed;
        }
    }

    /*
    for (g = groups->next; g; g = g->next) {
        fprintf(f, "G %u %s\n", g->gm, g->name);
        if (write(f, line, strlen(line)) != (int) strlen(line)) {
            Log("Error while writing group to temporary host file");
            error = 1;
            break;
        }
    }
    */

    if (!optionSingleSession) {
        for (i = 0, e = sessions; i < cSessions; i++, e++) {
            char aRelogin[16];

            if (e->anonymous)
                continue;
            if (e->active != 1)
                continue;

            if (e->relogin)
                sprintf(aRelogin, ".%" PRIdMAX "", (intmax_t)(e->relogin));
            else
                aRelogin[0] = 0;

            char address[INET6_ADDRSTRLEN];
            ipToStr(&e->sin_addr, address, sizeof address);

            if (fprintf(f, "S %s %" PRIdMAX "%s %" PRIdMAX ".%" PRIdMAX " %d %d %s %u %u %d %s\n",
                        e->key, (intmax_t)(e->created), aRelogin, (intmax_t)(e->accessed),
                        (intmax_t)(e->confirmed), (int)(e->expirable), e->lifetime, address,
                        e->sessionFlags, (unsigned char)(e->priv), e->autoLoginBy,
                        (e->menu[0] ? e->menu + strlen(DOCSDIR) : "*")) < 0)
                goto Failed;

            if (e->adminKey[0]) {
                if (fprintf(f, "A %s\n", e->adminKey) < 0)
                    goto Failed;
            }

            if (e->created != e->lastAuthenticated) {
                if (fprintf(f, "s %" PRIdMAX "\n", (intmax_t)(e->lastAuthenticated)) < 0)
                    goto Failed;
            }

            if (e->genericUser) {
                if (fprintf(f, "U %s\n", e->genericUser) < 0)
                    goto Failed;
            }

            if (e->logUserFull) {
                if (fprintf(f, "L %s\n", e->logUserFull) < 0)
                    goto Failed;
            }

            if (e->cpipSid) {
                if (fprintf(f, "C %s\n", e->cpipSid) < 0)
                    goto Failed;
            }

            if (e->userObjectTicket[0]) {
                if (fprintf(f, "T %s\n", e->userObjectTicket) < 0)
                    goto Failed;
            }

            if (e->docsCustomDir) {
                if (fprintf(f, "c %s\n", e->docsCustomDir) < 0)
                    goto Failed;
            }

            if (e->ipInterface.ss_family == AF_INET || e->ipInterface.ss_family == AF_INET6) {
                if (fprintf(f, "I %s\n", ipToStr(&e->ipInterface, ipBuffer, sizeof ipBuffer)) < 0)
                    goto Failed;
            }

            /* Only save groups if we have a group membership other than just Default group */
            if (GroupMaskIsSet(e->gm, 0) == 0 || GroupMaskCardinality(e->gm) != 1) {
                noGroups = 1;
                for (g = groups; g; g = g->next) {
                    if (GroupMaskOverlap(e->gm, g->gm)) {
                        noGroups = 0;
                        if (fprintf(f, "g %s\n", g->name) < 0)
                            goto Failed;
                    }
                }

                if (noGroups) {
                    if (fprintf(f, "g NULL\n") < 0)
                        goto Failed;
                }
            }

            for (v = 0; v < MAXSESSIONVARS; v++) {
                if (e->vars[v]) {
                    if (fprintf(f, "V %d %s\n", v, e->vars[v]) < 0)
                        goto Failed;
                }
            }
        }
    }

    TEMP_FAILURE_RETRY_INT(interrupted_neg_int(fclose(f)));
    f = NULL;

    unlink(HOSTFILE);
    rename(HOSTFILENEW, HOSTFILE);

    goto Leave;

Failed:
    Log("Error appending to %s: %d", HOSTFILENEW, errno);
    if (f != NULL) {
        TEMP_FAILURE_RETRY_INT(interrupted_neg_int(fclose(f)));
        f = NULL;
    }

Leave:
    /* Make sure new file got deleted if something errored along the way */
    unlink(HOSTFILENEW);
    RWLReleaseRead(&rwlHosts);
}

void PrecreateHosts(void) {
    int i;
    struct DATABASE *b;
    struct INDOMAIN *m;

    for (i = 0, b = databases; i < cDatabases; i++, b++) {
        for (m = b->dbdomains; m; m = m->next) {
            if (m->hostOnly)
                ClusterFindHost(m->domain, m->port, m->useSsl, m->ftpgw, 0);
        }
    }
}

/**
 * Cycles through all the login ports, which is setup in the DoHandleRaw which
 * starts a thread which sets up all the translates.  The translate struct
 * has a member of LoginPorts.  So each of these get setup here and a bind
 *
 */
void StartLoginPorts(void) {
    int i;
    struct LOGINPORT *lp;
    char ipBuffer[INET6_ADDRSTRLEN];

    for (i = 0, lp = loginPorts + i; i < cLoginPorts; i++, lp++) {
        if (lp->useSsl == USESSLSKIPPORT || lp->virtualPort) {
            lp->socket = INVALID_SOCKET;
            continue;
        }
        lp->socket = StartListener(lp->port, &lp->ipInterface, loginSocketBacklog);
        if (lp->socket == INVALID_SOCKET) {
            Log("Unable to listen on %s:%" PORT_FORMAT
                " (err=%d), another copy of " EZPROXYNAMEPROPER
                " or another web server may be running",
                IPInterfaceName(ipBuffer, sizeof ipBuffer, &lp->ipInterface), lp->port,
                socket_errno);
            guardian->portOpenFailure |= GCPORTFAILED;
#ifndef WIN32
            if (geteuid() != 0 && lp->port < 1024)
                guardian->portOpenFailure |= GCPRIVPORTFAILED;
#endif
            return;
        }
    }
    // not sure what this is about
    hosts[0].socket = loginPorts[0].socket;
}

/* *** NOTE *** Some of this logic duplicated in translate for handling relative URLs in location
 * header */

void EditURL(struct TRANSLATE *t, char *url) {
    char *host;
    PORT myPort;
    char newHost[1024];
    int diff;
    size_t uriLen, oldLen, newLen;
    struct HOST *h;
    char useSsl;
    char specifiedHttp = 0;
    char *endOfHost;
    char endOfHostSave;
    char ftpgw = 0;

    if (t->rewritingDisabled)
        return;

    /* Added for metapress.com which puts spaces in front of URLs in their href tags on occassion */
    url = SkipWS(url);

    /*  printf("Before %s\n", url); */
    if (strnicmp(url, "ftp://", 6) == 0) {
        host = url + 6;
        useSsl = 0;
        ftpgw = 1;
    } else if (strnicmp(url, "http://", 7) == 0) {
        host = url + 7;
        useSsl = 0;
        specifiedHttp = 1;
    } else if (strnicmp(url, "https://", 8) == 0 && UUSslEnabled() &&
               (t->proxyType == PTPORT || primaryLoginPortSsl != 0)) {
        host = url + 8;
        useSsl = 1;
    } else if (strnicmp(url, "//", 2) == 0) {
        host = url + 2;
        useSsl = (t->host) != NULL && (t->host)->useSsl;
    } else
        return;

    host = SkipAt(host);

    /*  printf("Find %s\n", host); */

    ParseHost(host, NULL, &endOfHost, 0);
    if (endOfHostSave = *endOfHost) {
        *endOfHost = 0;
    }

    oldLen = strlen(host);

    if (oldLen == 0 || oldLen > 255) {
        if (endOfHostSave)
            *endOfHost = endOfHostSave;
        return;
    }

    h = FindHost(host, useSsl, ftpgw);
    if (endOfHostSave)
        *endOfHost = endOfHostSave;
    if (h == NULL)
        return;

    myPort = h->myPort;

    if (t->proxyType == PTHOST)
        strcpy(newHost, h->proxyHostname);
    else
        sprintf(newHost, "%s:%d", myName, myPort);

    newLen = strlen(newHost);
    diff = newLen - oldLen;

    if (*endOfHost == 0)
        StrCpyOverlap(host, newHost);
    else {
        uriLen = strlen(endOfHost) + 1;
        memmove(endOfHost + diff, endOfHost, uriLen);
        memmove(host, newHost, newLen);
    }

    if (ftpgw) {
        /* shift things over so we can put an h at the start and change f to t */
        memmove(url + 1, url, strlen(url) + 1);
        *url = 'h';       /* add new h at start */
        *(url + 1) = 't'; /* replace f from ftp */
        /* allow optionForceSSl to kick in if relevant */
        specifiedHttp = 1;
    }

    if (specifiedHttp && optionForceSsl) {
        /* strlen(url + 3) = (strlen(url) - 4) + 1 to ignore http but copy the final null */
        memmove(url + 5, url + 4, strlen(url + 3));
        *(url + 4) = 's';
    }

    /*  printf("After %s\n", url); */
}

void ReverseEditURL(struct TRANSLATE *t, char *url, BOOL *changed) {
    char *host;
    char *p;
    char *endOfHost;
    PORT myPort;
    char newHost[256];
    int diff;
    size_t uriLen, oldLen, newLen, proxyHostnameLen;
    int i;
    struct HOST *h;
    char useSsl;
    char httpsSpecified = 0;

    if (changed)
        *changed = 0;

    /*  printf("Before %s\n", url); */
    if (strnicmp(url, "http://", 7) == 0) {
        host = url + 7;
        useSsl = 0;
    } else if (UUSslEnabled() && strnicmp(url, "https://", 8) == 0) {
        host = url + 8;
        useSsl = 1;
        httpsSpecified = 1;
    } else if (strnicmp(url, "//", 2) == 0) {
        host = url + 2;
        useSsl = (t->host) != NULL && (t->host)->useSsl;
    } else
        return;

    host = SkipAt(host);

    if (t->proxyType == PTPORT) {
        if (strnicmp(host, myName, myNameLen) != 0)
            return;

        p = host + myNameLen;
        if (*p != ':')
            return;
        p++;

        myPort = atoi(p);

        for (i = 1, h = hosts + i;; i++, h++) {
            if (i >= cHosts)
                return;
            if (h->myPort == myPort)
                break;
        }
    } else {
        for (i = 1, h = hosts + i;; i++, h++) {
            if (i >= cHosts)
                return;
            proxyHostnameLen = strlen(h->proxyHostname);
            if (strnicmp(host, h->proxyHostname, proxyHostnameLen) == 0) {
                if (optionForceSsl == 0 && useSsl != h->useSsl)
                    continue;
                p = host + myNameLen;
                break;
            }
        }
    }

    ParseHost(host, NULL, &endOfHost, 0);
    oldLen = endOfHost - host;
    /*
    if ((slash = strchr(host, '/')))
        oldLen = slash - host;
    else
        oldLen = strlen(host);
    */

    strcpy(newHost, h->hostname);
    if (h->remport != (useSsl ? 443 : 80))
        if (h->ftpgw == 0 || h->remport != 21)
            sprintf(strchr(newHost, 0), ":%d", h->remport);

    newLen = strlen(newHost);
    diff = newLen - oldLen;

    if (*endOfHost == 0)
        StrCpyOverlap(host, newHost);
    else {
        uriLen = strlen(endOfHost) + 1;
        memmove(endOfHost + diff, endOfHost, uriLen);
        memmove(host, newHost, newLen);
    }

    if (h->useSsl == 0 && httpsSpecified) {
        StrCpyOverlap(url + 4, url + 5);
    }
    /* shift the tp:// back from offset 2 to 1, then make offset 0 f, so ftp:// */
    if (h->ftpgw) {
        StrCpyOverlap(url + 1, url + 2);
        *url = 'f';
    }
    if (changed)
        *changed = 1;
    /*  printf("After %s\n", url); */
}

void SavePidFile(void) {
    int saveGuardian = 0;
    int saveCharge = 0;
    static int savedGuardian = 0;
    static int savedCharge = 0;
    int f;
    char pids[80];

    if (pidFileName == NULL)
        return;

    if (guardian) {
        if (pidFileGuardian)
            saveGuardian = guardian->gpid;
        if (pidFileCharge)
            saveCharge = guardian->cpid;
    }

    if (saveGuardian == savedGuardian && saveCharge == savedCharge)
        return;

    f = UUopenCreateFD(pidFileName, O_CREAT | O_WRONLY | O_TRUNC, defaultFileMode);
    if (f < 0) {
        Log("Unable to create PidFile %s: %d", pidFileName, errno);
        return;
    }

    pids[0] = 0;

    if (saveGuardian)
        sprintf(strchr(pids, 0), "%d\n", saveGuardian);

    if (saveCharge)
        sprintf(strchr(pids, 0), "%d\n", saveCharge);

    write(f, pids, strlen(pids));

    TEMP_FAILURE_RETRY_INT(interrupted_neg_int(close(f)));

    savedGuardian = saveGuardian;
    savedCharge = saveCharge;
}

static int LockIPC(void) {
#ifdef WIN32
    return 0; /* _sopen with _SH_DENYWR did the lock. */
#else
#ifdef USEFLOCK
    return flock(ipcFile, LOCK_EX | LOCK_NB);
#elif defined(USEFCNTL)
    struct flock fl;
    fl.l_type = F_WRLCK;
    fl.l_whence = SEEK_SET;
    fl.l_start = 0;
    fl.l_len = 1;
    return fcntl(ipcFile, F_SETLK, &fl);
#elif defined(USERENAMELINK)
    /*
     * When using NFS, flock() and fcntl() don't work on all implementations. The solution for
     * performing atomic file locking using a lockfile is to create a unique file on a filesystem
     * (e.g., incorporating hostname and pid), use link(2) to make a link to the lockfile on the
     * same filesystem. Don't rely on link() returning 0, to tell if the lock is successful.
     * Instead, use stat(2) on the unique file to check if its link count has increased to 2, in
     * which case the "lock" is also successful. This should work on all UNIX/POSIX platforms: Linux
     * LSB 3.0, Solaris 8, Mac OS X Version 10.5 Leopard on Intel-based Macintosh computers, and
     * Solaris 10.
     */
    char ufn[64];
    struct stat s;
    long int rc;
    int fd;

    rc = pathconf(".", _PC_NAME_MAX);
    if (rc == -1) {
        rc = errno;
        Log("pathconf() failed for %s", ".");
        errno = rc;
    } else {
        snprintf(ufn, min(sizeof(ufn), rc) - 5, "%0X%0X%0X%0X%0X%0X%0X%0X%0X%0X%0X%0X%0X%0X%0X%0X",
                 RandNumber(15), RandNumber(15), RandNumber(15), RandNumber(15), RandNumber(15),
                 RandNumber(15), RandNumber(15), RandNumber(15), RandNumber(15), RandNumber(15),
                 RandNumber(15), RandNumber(15), RandNumber(15), RandNumber(15), RandNumber(15),
                 RandNumber(15));
        snprintf(strchr(ufn, 0), 4, ".lck");
        fd = UUopenCreateFD(ufn, O_WRONLY | O_CREAT, defaultFileMode);
        if (runUid && runGid) {
            chown(ufn, runUid, runGid);
        } else if (runUid) {
            chown(ufn, runUid, -1);
        } else if (runGid) {
            chown(ufn, -1, runGid);
        }
        if (fd < 0) {
            rc = errno;
            Log("open() failed for %s", ufn);
            errno = rc;
            rc = -1;
        } else {
            rc = link(ufn, IPCFILELOCK); /* Ignore the return code, it may be unreliable. */
            rc = stat(ufn, &s);
            if (rc == -1) {
                rc = errno;
                Log("stat() failed for %s", ufn);
                errno = rc;
                rc = -1;
            } else {
                if (s.st_nlink == 2) {
                    rc = UUopenCreateFD(IPCFILELOCK, O_WRONLY | O_CREAT, defaultFileMode);
                } else {
                    close(fd);
                    errno = EEXIST; /* What link() should've said. */
                    rc = -1;
                }
            }
            /* Doing this unlink() early seeks to guarantee that the file is removed no matter how
             * or when the program terminates.  It will be removed when or after a close(fd) is
             * performed. */
            unlink(ufn);
        }
    }
    return rc;
#else
#error LockIPC lacks definition to create file on this platform
#endif
#endif
}

void ExplainIPC() {
    Log("Another version of EZproxy may already be running in this directory.");
    Log("If this directory is shared, the other EZproxy may be running in this directory on "
        "another computer.");
    Log("If you're sure no other EZproxy is running, you may delete all files ending with '.ipc' "
        "and '.lck', and try running EZproxy again.");
}

void SaveIPC(void) {
    int tries;
    char *errorMsg = NULL;
    char newContents[4096];
#ifndef WIN32
    uid_t lastRunUid = 0;
    gid_t lastRunGid = 0;
#endif

    if (ipcFile == -1) {
        for (tries = 0;; tries++) {
#ifdef WIN32
            ipcFile = _sopen(IPCFILE, _O_CREAT | _O_WRONLY, _SH_DENYWR, _S_IWRITE | _S_IREAD);
#else
            ipcFile = UUopenCreateFD(IPCFILE, O_CREAT | O_WRONLY, defaultFileMode);
#endif
            if (ipcFile >= 0)
                break;
            errorMsg = ErrorMessage(errno);
            Log("Unable to create or open %s: %s", IPCFILE, errorMsg);
            FreeThenNull(errorMsg);
            if (tries == 3) {
                ExplainIPC();
                exit(2);
            }
            SubSleep(1, 0);
        }

        for (tries = 0;; tries++) {
            if (LockIPC() != -1)
                break;
            errorMsg = ErrorMessage(errno);
            Log("Unable to lock %s: %s", IPCFILELOCK, errorMsg);
            FreeThenNull(errorMsg);
            if (tries == 3) {
                close(ipcFile);
                ipcFile = -1;
                ExplainIPC();
                exit(2);
            }
            SubSleep(1, 0);
        }
    }

#ifndef WIN32
    if (runUid != lastRunUid || runGid != lastRunGid) {
        if (runUid && runGid)
            chown(IPCFILE, runUid, runGid);
        else if (runUid)
            chown(IPCFILE, runUid, -1);
        else if (runGid)
            chown(IPCFILE, -1, runGid);

        lastRunUid = runUid;
        lastRunGid = runGid;
    }
#endif

    newContents[0] = 0;

    if (myStartupPath)
        sprintf(AtEnd(newContents), "D %s\n", myStartupPath);

    if (guardian && guardian->gpid)
        sprintf(AtEnd(newContents), "G %d\n", guardian->gpid);

    if (guardian && guardian->cpid)
        sprintf(AtEnd(newContents), "C %d\n", guardian->cpid);

#ifdef WIN32
    if (guardianSharedMemoryID && strcmp(guardianSharedMemoryID, "none") != 0)
        sprintf(AtEnd(newContents), "M %s\n", guardianSharedMemoryID);
#else
    if (guardianSharedMemoryID)
        sprintf(AtEnd(newContents), "M %d\n", guardianSharedMemoryID);
#endif

#ifdef WIN32
    chsize(ipcFile, 0);
#else
    TEMP_FAILURE_RETRY_INT(interrupted_neg_int(ftruncate(ipcFile, 0)));
#endif

    lseek(ipcFile, 0, SEEK_SET);

    TEMP_FAILURE_RETRY_INT(interrupted_neg_int(write(ipcFile, newContents, strlen(newContents))));

    SavePidFile();
}

void UnlinkIPCFILEandPidFile(void) {
    if (ipcFile != -1) {
        TEMP_FAILURE_RETRY_INT(interrupted_neg_int(close(ipcFile)));
        ipcFile = -1;
        unlink(IPCFILE);
#ifndef WIN32
        unlink(IPCFILELOCK);
#endif
    }

    if (pidFileName) {
        unlink(pidFileName);
    }
}

#ifdef WIN32
void LoadIPC(int *gpid, int *cpid, char **smid, int *error)
#else
void LoadIPC(pid_t *gpid, pid_t *cpid, int *smid, int *error)
#endif
{
    FILE *f;
    char cmd, *arg;
    char line[256];
    BOOL sameDir = 0;

    if (gpid)
        *gpid = 0;

    if (cpid)
        *cpid = 0;

    if (error)
        *error = 0;

#ifdef WIN32
    if (smid)
        *smid = NULL;
#else
    if (smid)
        *smid = 0;
#endif

    errno = 0;
    TEMP_FAILURE_RETRY_FILE(f = interrupted_null_file(fopen(IPCFILE, "r")));
    if (f == NULL) {
        if (error)
            *error = errno;
        return;
    }

    sameDir = 1;

    while (fgets(line, sizeof(line), f)) {
        Trim(line, TRIM_COMPRESS);
        arg = line;
        cmd = *arg++;
        if (*arg++ != ' ')
            continue;

        /*
        if (cmd == 'D') {
            sameDir = myStartupPath && stricmp(arg, myStartupPath) == 0;
        }
        */

        if (!sameDir)
            continue;

        if (cmd == 'G') {
            if (gpid)
                *gpid = atoi(arg);
        }

        if (cmd == 'C') {
            if (cpid)
                *cpid = atoi(arg);
        }

        if (cmd == 'M') {
#ifdef WIN32
            if (smid) {
                if (*smid)
                    free(*smid);
                if (!(*smid = strdup(arg)))
                    PANIC;
            }
#else
            if (smid)
                *smid = atoi(arg);
#endif
        }
    }
    TEMP_FAILURE_RETRY_INT(interrupted_neg_int(fclose(f)));
}

void SetMyUrlHttp(void) {
    char *oldMyUrlHttp = myUrlHttp;
    char *newMyUrlHttp = NULL;

    if (primaryLoginPort == 0)
        return;

    if (!(newMyUrlHttp = malloc(strlen(myName) + 32)))
        PANIC;
    strcpy(newMyUrlHttp, "http://");
    /*
    if (optionWildcardHTTPS || (optionProxyType == PTHOST && optionSafariCookiePatch))
        strcat(newMyUrlHttp, "login.");
    */
    strcat(newMyUrlHttp, myName);
    if (primaryLoginPort != 80)
        sprintf(strchr(newMyUrlHttp, 0), ":%d", primaryLoginPort);

    myUrlHttp = newMyUrlHttp;
    FreeThenNull(oldMyUrlHttp);
}

void SetMyHttpName(void) {
    /*
    if (optionProxyType == PTHOST && optionSafariCookiePatch)
        sprintf(myHttpName, "login.%s", myName);
    else
    */
    strcpy(myHttpName, myName);
}

void SetMyUrlHttps(void) {
    char *oldMyUrlHttps = myUrlHttps;
    char *newMyUrlHttps = NULL;

    if (primaryLoginPortSsl == 0)
        return;

    if (!(newMyUrlHttps = malloc(strlen(myName) + 32)))
        PANIC;
    strcpy(newMyUrlHttps, "https://");
    /*
    if (optionWildcardHTTPS || (optionProxyType == PTHOST && optionSafariCookiePatch))
    */
    if (optionWildcardHTTPS)
        strcat(newMyUrlHttps, "login.");
    strcat(newMyUrlHttps, myName);
    if (primaryLoginPortSsl != 443)
        sprintf(strchr(newMyUrlHttps, 0), ":%d", primaryLoginPortSsl);

    myUrlHttps = newMyUrlHttps;
    FreeThenNull(oldMyUrlHttps);
}

void SetMyHttpsName(void) {
    if (optionWildcardHTTPS)
        sprintf(myHttpsName, "login.%s", myName);
    else
        strcpy(myHttpsName, myName);
}
