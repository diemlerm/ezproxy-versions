#define __UUFILE__ "adminintrusion.c"

#include "common.h"
#include "uustring.h"

#include "adminintrusion.h"
#include "intrusion.h"

static void AdminIntrusionHeader(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket;

    UUSendZ(s, "<p>\n<a href='/intrusion?clearall=on'>Clear all intrusion attempts");
    if (anyIntruderIPRejects) {
        UUSendZ(s, " except rejecting</a><br>\n");
        UUSendZ(s,
                "<a href='/intrusion?clearall=rejecting'>Clear all intrusion attempts including "
                "rejecting");
    }
    UUSendZ(s, "</a><br>\n</p>\n");

    UUSendZ(s,
            "<table class='bordered-table sortable' id='intrusion'>\n<tr align='center'><th "
            "scope='col'>Clear</th><th scope='col' class='sortip'>IP/User</th><th scope='col' "
            "class='sortnumeric'>Attempts</th><th scope='col'>Last Attempt</th><th scope='col'>* "
            "Evasion Expires</th></tr>\n");
}

void AdminIntrusion(struct TRANSLATE *t, char *query, char *post) {
    struct INTRUDERIP *bg, *next;
    time_t now;
    char ta[MAXASCIIDATE], te[MAXASCIIDATE];
    struct UUSOCKET *s = &t->ssocket;
    char *nextKev, *key, *arg1, *arg2, *arg3;
    int trs = 0;
    time_t expiresDate;
    struct USAGE *u;
    struct USAGELIMITEXCEEDED *ule;
    BOOL cleared;
    BOOL clearRejecting;
    BOOL any = 0;
    BOOL anyToClear = 0;
    int intruderIPLevel = 0;
    BOOL anyRejecting = 0;

    AdminHeader(t, AHSORTABLE, "View and Clear Intrusion Attempts");

    if (!IntrusionInvalidConfig()) {
        UUSendZ(s, "<p>IntrusionAPI configuration ");
        SendHTMLEncoded(s, intrusionVersion);
        UUSendZ(s, " active</p>\n");
    }

    if (intruderIPAttempts == NULL && intruderUsageLimit == NULL) {
        if (IntrusionInvalidConfig()) {
            UUSendZ(s,
                    "<p>Intrusion detection is disabled.  See <a "
                    "href='http://www.oclc.org/support/documentation/ezproxy/example/"
                    "securing.htm'>Securing Your " EZPROXYNAMEPROPER " Server</a>.</p>\n");
        }
        AdminFooter(t, 0);
        return;
    }

    UUSendZ(s, "<form action='/intrusion' method='post'>\n");

    UUtime(&now);

    for (nextKev = (post ? post : query); NextKEV(nextKev, &key, &arg1, &nextKev, 0);) {
        if (arg1 == NULL)
            continue;
        if (strcmp(key, "clearall") == 0) {
            clearRejecting = stricmp(arg1, "rejecting") == 0;
            UUAcquireMutex(&mIntruders);
            for (bg = intruderIPs; bg; bg = next) {
                next = bg->next;

                if (bg->lastAttempt && (clearRejecting || IntruderIPLevel(bg, now) != 3)) {
                    bg->lastAttempt = 0;
                    bg->attempts = 0;
                }
            }
            UUReleaseMutex(&mIntruders);

            if (intruderUsageLimit) {
                struct USAGE *uNext;

                UUAcquireMutex(&mUsage);
                for (u = intruderUsageLimit->usage; u; u = uNext) {
                    uNext = u->next;
                    if (u->lastAttempt > 0 && (ule = FindUsageLimitExceeded(u->user, 0, 1))) {
                        ClearUsageLimitExceeded(ule, intruderUsageLimit, u, "administrator (all)");
                    }
                }
                UUReleaseMutex(&mUsage);
            }
            UUSendZ(s, "<h2>All intrusions cleared</h2>\n");
        }

        if (strcmp(key, "clear") == 0) {
            time_t lastAttempt;
            arg2 = NextWSArg(arg1);
            arg3 = NextWSArg(arg2);
            lastAttempt = atoi(arg3);
            if (lastAttempt == 0)
                continue;
            cleared = 0;
            if (intruderUsageLimit && stricmp(arg1, "user") == 0) {
                UUAcquireMutex(&mUsage);
                if ((ule = FindUsageLimitExceeded(arg2, 0, 1)) &&
                    (FindUsage(&intruderUsageLimit->usage, arg2, 0, 1, &u, NULL), u) &&
                    u->lastAttempt == lastAttempt) {
                    ClearUsageLimitExceeded(ule, intruderUsageLimit, u, "administrator");
                    cleared = 1;
                }
                UUReleaseMutex(&mUsage);
            }
            if (intruderIPAttempts && stricmp(arg1, "ip") == 0) {
                UUAcquireMutex(&mIntruders);
                for (bg = intruderIPs; bg; bg = next) {
                    next = bg->next;
                    if (stricmp(arg2, bg->ipText) == 0 && bg->lastAttempt == lastAttempt) {
                        bg->lastAttempt = 0;
                        bg->attempts = 0;
                        cleared = 1;
                    }
                }
                UUReleaseMutex(&mIntruders);
            }

            if (cleared) {
                if (any == 0) {
                    UUSendZ(s, "<h2>Intrusions cleared</h2>\n<p>\n");
                    any = 1;
                }
                SendHTMLEncoded(s, arg1);
                UUSendZ(s, " ");
                SendHTMLEncoded(s, arg2);
                UUSendZ(s, "<br>\n");
            }
        }
    }

    if (any) {
        UUSendZ(s, "</p>\n");
    }

    any = 0;

    for (bg = intruderIPs; bg; bg = next) {
        UUAcquireMutex(&mIntruders);
        next = bg->next;
        if (bg->lastAttempt && (intruderIPLevel = IntruderIPLevel(bg, now)) == 0) {
            bg->lastAttempt = 0;
            bg->attempts = 0;
        }
        UUReleaseMutex(&mIntruders);

        if (bg->lastAttempt == 0) {
            continue;
        }

        if (any == 0) {
            AdminIntrusionHeader(t);
            any = 1;
        }

        expiresDate = (bg->attempts >= bg->maxAttempts ? bg->lastAttempt + bg->intruderExpires : 0);
        WriteLine(s, "<tr align='center' id='trs%d'><td>", trs++);
        WriteLine(s, "<input type='checkbox' name='clear' id='clear-%d' value='IP ", trs);
        SendQuotedURL(t, bg->ipText);
        WriteLine(s, " %d'>", bg->lastAttempt);
        anyToClear = 1;
        WriteLine(
            s,
            "</td><td align='left'><label for='clear-%d'>%s</label></td><td>%d</td><td>%s</td><td>",
            trs, bg->ipText, bg->attempts, ASCIIDate(&bg->lastAttempt, ta));
        if (intruderIPLevel == 3) {
            UUSendZ(s, "Rejecting");
            anyRejecting = 1;
        } else {
            if (expiresDate)
                UUSendZ(s, ASCIIDate(&expiresDate, te));
            else
                UUSendZ(s, "&nbsp");
        }
        UUSendZ(s, "</td></tr>\n");
    }

    if (intruderUsageLimit) {
        char *expiresOut;

        for (u = intruderUsageLimit->usage; u; u = u->next) {
            if (u->lastAttempt == 0)
                continue;

            /* If suspended, check suspension, and if it clears up, skip this to next user */
            if ((ule = FindUsageLimitExceeded(u->user, 0, 0)) &&
                (UsageLimitExceeded(NULL, ule, ULEINTRUDER), u->lastAttempt == 0))
                continue;

            if (any == 0) {
                AdminIntrusionHeader(t);
                any = 1;
            }

            WriteLine(s, "<tr align='center' id='trs%d'><td>", trs++);

            WriteLine(s, "<input type='checkbox' name='clear' id='clear-%d' value='User ", trs);
            SendQuotedURL(t, u->user);
            WriteLine(s, " %d'>", u->lastAttempt);
            anyToClear = 1;

            WriteLine(s, "</td><td align='left'><label for='clear-%d'>", trs);
            SendHTMLEncoded(s, u->user);

            if (u->when) {
                if (intruderUsageLimit->expires) {
                    expiresDate = u->when + intruderUsageLimit->expires;
                    expiresOut = ASCIIDate(&expiresDate, te);
                } else {
                    expiresOut = "(must be cleared manually)";
                }
            } else {
                expiresOut = "&nbsp;";
            }

            WriteLine(s, "</label></td><td>%d</td><td>%s</td><td>%s</td></tr>",
                      u->intruderUserAttempts,
                      (u->lastAttempt ? ASCIIDate(&u->lastAttempt, ta) : "&nbsp;"), expiresOut);
        }
    }

    if (any) {
        UUSendZ(s, "</table>\n");
        if (anyToClear) {
            UUSendZ(s, "<p><input type='submit' value='Clear checked intrusions'></p>");
        }
        UUSendZ(s,
                "<p>* If Evasion Expires is blank, there have not been enough invalid attempts to "
                "trigger evasion against the IP address or user.</p>\n");
        if (anyRejecting)
            UUSendZ(s,
                    "<p>IP addresses in the rejecting state never expires and must be manually "
                    "cleared.</p>\n");
    } else {
        UUSendZ(s, "<p>There are no intrusion attempts at this time.</p>\n");
    }

    /*
    WriteLine(s, "<p>EZproxy is set to perform intruder evasion if someone makes %d unsuccessful
    attempts from the same IP address within %d seconds.</p>", intruderAttempts, intruderTimeout);

    UUSendZ(s, "<p>To alter these thresholds, edit " EZPROXYCFG " to add or change lines similar
    to:</p>\n"); WriteLine(s, "<blockquote><tt><nobr>IntruderAttempts %d<br />IntruderTimeout %d<br
    /></nobr></tt></blockquote>\n", intruderAttempts, intruderTimeout); UUSendZ(s, "<p>and then
    restart EZproxy.</p>\n");
    */

    AdminFooter(t, 0);
}
