/**
 * environment.c
 *
 *  Created on: Feb 17, 2022
 *      Author: nebletta
 *      Currently sets up database for email
 *      distribution for security rules emails
 */

#define __UUFILE__ "environment.c"

#include <sys/stat.h>
#include "environment.h"
#include "sql.h"

// This separate connection can be shared by multiple threads.
sqlite3 *sharedConfigDb = NULL;
sqlite3 *configDb = NULL;

const char *startupEnvironmentCommands[] = {"PRAGMA foreign_keys=ON",

                                            "CREATE TABLE IF NOT EXISTS notificationtransactions("
                                            "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                                            "at INT NOT NULL,"
                                            "emailaddress TEXT NOT NULL,"
                                            "transactionid TEXT"
                                            ")",

                                            "CREATE TABLE IF NOT EXISTS notificationemailaddress("
                                            "emailaddress TEXT NOT NULL PRIMARY KEY"
                                            ")",

                                            "CREATE TABLE IF NOT EXISTS receivesdailydigest("
                                            "receivesdailydigest INTEGER PRIMARY KEY"
                                            ")",

                                            "CREATE TABLE IF NOT EXISTS queuedtrippedrules("
                                            "id INTEGER PRIMARY KEY"
                                            ")",

                                            "CREATE TABLE IF NOT EXISTS wskey("
                                            "id INTEGER PRIMARY KEY,"
                                            "key TEXT,"
                                            "secret TEXT"
                                            ")",

                                            NULL};

// This is a shared database connection. It must not be closed by its users.
sqlite3 *EnvironmentGetSharedDatabase(BOOL shared) {
    return sharedConfigDb;
}

sqlite3 *EnvironmentGetDatabase() {
    return configDb;
}

static void EnvironmentOpenDatabase(sqlite3 **db) {
    SQLOK(sqlite3_open_v2(ENVIRONMENTDB, db,
                          SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX,
                          NULL));
    SQLOK(sqlite3_busy_timeout(*db, 60000));
}

static void EnvironmentUpgradeDatabase() {}

static void EnvironmentInit() {
    const char **sql;

    EnvironmentOpenDatabase(&sharedConfigDb);
    EnvironmentOpenDatabase(&configDb);

    for (sql = startupEnvironmentCommands; *sql; sql++) {
        SQLEXEC(sharedConfigDb, *sql, NULL, NULL);
    }

    EnvironmentUpgradeDatabase();
}

void EnvironmentStart() {
    struct stat statBuf;
    if (stat(ENVIRONMENTDIR, &statBuf) < 0 && errno == ENOENT) {
        SetEUidGid();
        if (UUmkdir(ENVIRONMENTDIR) != 0)
            PANIC;
        ResetEUidGid();
    }
    EnvironmentInit();
}
