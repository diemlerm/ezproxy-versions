#define __UUFILE__ "unixrwl.c"

#include "common.h"

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int RWLInit(struct RWL *rwl, char *name) {
    if (pthread_mutex_init(&rwl->mutex, NULL) != 0)
        PANIC;
    if (pthread_cond_init(&rwl->condRead, NULL) != 0)
        PANIC;
    if (pthread_cond_init(&rwl->condWrite, NULL) != 0)
        PANIC;
    rwl->waitRead = rwl->waitWrite = rwl->activeRead = rwl->activeWrite = 0;
    if (!(rwl->name = strdup(name)))
        PANIC;
    return 0;
}

int RWLDestroy(struct RWL *rwl) {
    pthread_mutex_destroy(&rwl->mutex);
    pthread_cond_destroy(&rwl->condRead);
    pthread_cond_destroy(&rwl->condWrite);
    return 0;
}

int RWLSignal(struct RWL *rwl) {
    if (rwl->waitWrite != 0 && rwl->activeRead == 0) {
        if (pthread_cond_signal(&rwl->condWrite) != 0)
            PANIC;
    } else if (rwl->waitRead != 0 && rwl->activeWrite == 0) {
        if (pthread_cond_broadcast(&rwl->condRead) != 0)
            PANIC;
    }
    if (pthread_mutex_unlock(&rwl->mutex) != 0)
        PANIC;
    return 0;
}

int RWLAcquireRead(struct RWL *rwl) {
    if (pthread_mutex_lock(&rwl->mutex) != 0)
        PANIC;
    for (;;) {
        if (rwl->activeWrite == 0 && rwl->waitWrite == 0) {
            rwl->activeRead++;
            if (pthread_mutex_unlock(&rwl->mutex) != 0)
                PANIC;
            return 0;
        }
        rwl->waitRead++;
        if (pthread_cond_wait(&rwl->condRead, &rwl->mutex) != 0)
            PANIC;
        rwl->waitRead--;
    }
}

int RWLAcquireWrite(struct RWL *rwl) {
    pthread_t myThread = pthread_self();
    if (pthread_mutex_lock(&rwl->mutex) != 0)
        PANIC;
    for (;;) {
        if ((rwl->activeWrite == 0 && rwl->activeRead == 0) ||
            (rwl->activeWrite > 0 && pthread_equal(rwl->writeThread, myThread))) {
            rwl->activeWrite++;
            rwl->writeThread = myThread;
            if (pthread_mutex_unlock(&rwl->mutex) != 0)
                PANIC;
            return 0;
        }
        rwl->waitWrite++;
        if (pthread_cond_wait(&rwl->condWrite, &rwl->mutex) != 0)
            PANIC;
        rwl->waitWrite--;
    }
}

int RWLReleaseRead(struct RWL *rwl) {
    if (pthread_mutex_lock(&rwl->mutex) != 0)
        PANIC;
    rwl->activeRead--;
    return RWLSignal(rwl);
}

int RWLReleaseWrite(struct RWL *rwl) {
    if (pthread_mutex_lock(&rwl->mutex) != 0)
        PANIC;
    rwl->activeWrite--;
    return RWLSignal(rwl);
}
