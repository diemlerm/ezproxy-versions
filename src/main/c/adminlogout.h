#ifndef __ADMINLOGOUT_H__
#define __ADMINLOGOUT_H__

#include "common.h"

void AdminLogout(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINLOGOUT_H__
