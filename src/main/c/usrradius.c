/**
 * RADIUS user authentication
 *
 * This authentication method DOES NOT SUPPORT common conditions and actions.
 */

#define __UUFILE__ "usrradius.c"

#include "common.h"
#include "uustring.h"

#include "usr.h"
#include "usrradius.h"

#define RADIUS_ACCESS_REQUEST 1
#define RADIUS_ACCESS_ACCEPT 2
#define RADIUS_ACCESS_REJECT 3
#define RADIUS_USER_NAME 1
#define RADIUS_USER_PASSWORD 2
#define RADIUS_NAS_IP_ADDRESS 4
#define RADIUS_NAS_PORT 5
#define RADIUS_NAS_PORT_TYPE 61

void LogRADIUS(BOOL debug, char *s, int port, unsigned char *c, int len) {
    char prefix[512];

    if (debug) {
        sprintf(prefix, "%s %d", s, port);
        LogBinaryPacket(prefix, c, len);
    }
}

/* Returns 0 if valid, 1 if not, 3 if unable to connect to pop host */
int FindUserRadius(char *radiusHost,
                   char *user,
                   char *pass,
                   char *secret,
                   char *realm,
                   struct sockaddr_storage *pInterface,
                   BOOL debug) {
    SOCKET r = INVALID_SOCKET;
    struct sockaddr_storage sa;
    int result;
    char *colon;
    PORT port;
    MD5_CTX md5Context;
    struct radiusPacket {
        unsigned char code;
        unsigned char identifier;
        unsigned short packetLength;
        unsigned char authenticator[16];
        unsigned char attributes[1500];
    } sp, rp;
    static time_t seed = 0;
    int i;
    unsigned char *p;
    unsigned char *passStart;
    int slen, rlen;
    int rc;
    int group;
#ifdef USEPOLL
    struct pollfd pfds;
#else
    fd_set rfds, wfds;
    struct timeval pollInterval;
#endif
    BOOL needSend, canSend, canRecv;
    time_t started, now, last;
    unsigned char checkResponse[16];
    struct servent *servptr;
    struct sockaddr_storage sl;
    socklen_t_M slLen;
    char *semi, *semi2;
    unsigned int radiusPort = 0;
    BOOL radiusPortSpecified = 0;
    unsigned int radiusPortType = 0;
    BOOL radiusPortTypeSpecified = 0;
    result = 3;

    if (radiusHost == NULL || user == NULL || pass == NULL || secret == NULL)
        goto Finished;

    if (realm == NULL)
        realm = "";

    /* Make very conservative test and abort if too long */
    if (strlen(user) + strlen(pass) + strlen(realm) + 44 > sizeof(sp.attributes)) {
        result = 1;
        goto Finished;
    }

    if (semi = strchr(radiusHost, ';')) {
        *semi++ = 0;
        if (semi2 = strchr(semi, ';')) {
            *semi2++ = 0;
            if (*semi2) {
                if (IsAllDigits(semi2)) {
                    radiusPort = htonl(atoi(semi2));
                    radiusPortSpecified = 1;
                } else {
                    Log("RADIUS NAS port must be a number: %s", semi2);
                }
            }
        }

        if (*semi && stricmp(semi, "none") != 0) {
            radiusPortTypeSpecified = 1;
            if (stricmp(semi, "virtual") == 0)
                radiusPortType = htonl(5);
            else if (IsAllDigits(semi))
                radiusPortType = htonl(atoi(semi));
            else {
                Log("RADIUS unrecognized NAS port type: %s", semi);
                radiusPortTypeSpecified = 0;
            }
        }
    }

    ParseHost(radiusHost, &colon, NULL, TRUE);
    if (colon) {
        *colon = 0;
        port = atoi(colon + 1);
    } else {
        servptr = NULL;
        /* servptr = getservbyname("radius", "udp"); */
        port = (servptr ? ntohs(servptr->s_port) : 1645);
    }

    int family = (pInterface == NULL) ? AF_UNSPEC : pInterface->ss_family;

    // if we are going to bind to a local (ipInterface) interface,
    // then the radius host better line up
    if (UUGetHostByName(radiusHost, &sa, port, family)) {
        Log("Radius host not found: %s", radiusHost);
        goto Finished;
    }

    r = MoveFileDescriptorHigh(socket(sa.ss_family, SOCK_DGRAM, 0));
    if (debugLevel > 5)
        Log("%" SOCKET_FORMAT " Open socket, errno=%d", r, (r == INVALID_SOCKET) ? errno : 0);
    if (r == INVALID_SOCKET) {
        Log("socket() failed %d RADIUS", socket_errno);
        goto Finished;
    }

    if (pInterface != NULL) {
        if (bind(r, (struct sockaddr *)pInterface, get_size(pInterface)) == SOCKET_ERROR) {
            char ipBuffer[INET6_ADDRSTRLEN];
            ipToStr(pInterface, ipBuffer, sizeof ipBuffer);
            Log("UUconnect bind to local address %s failed", ipBuffer);
            goto Finished;
        }
    }

    if (optionRelaxedRADIUS == 0) {
        if (connect(r, (struct sockaddr *)&sa, sizeof(sa)) == SOCKET_ERROR) {
            Log("connect refused by %s on %" PORT_FORMAT " (%d)", radiusHost, get_port(&sa),
                socket_errno);
            goto Finished;
        }
    }

    slLen = sizeof(sl);
    if (getsockname(r, (struct sockaddr *)&sl, &slLen)) {
        if (UUGetHostByName(myName, &sl, 0, sl.ss_family)) {
            Log("Radius unable to determine my address");
            goto Finished;
        }
    }

    SocketNonBlocking(r);

    // pointer to the radius packet structure
    p = (unsigned char *)&sp;

    *p++ = RADIUS_ACCESS_REQUEST;  // contents of sp.code
    *p++ = RandNumber(256);
    p += 2; /* Skip length field */

    for (i = 0; i < sizeof(sp.authenticator); i++) {
        *p++ = RandNumber(256);
    }

    *p++ = RADIUS_USER_NAME;
    *p++ = strlen(user) + 2 + (*realm ? strlen(realm) + 1 : 0);
    strcpy((char *)p, user);
    p += strlen(user);
    if (*realm) {
        *p++ = '@';
        strcpy((char *)p, realm);
        p += strlen(realm);
    }

    passStart = p;
    *p++ = RADIUS_USER_PASSWORD;
    p++; /* Skip length for now */

    for (group = 0; group < 8; group++) {
        MD5_Init(&md5Context);
        MD5_Update(&md5Context, (unsigned char *)secret, (unsigned int)strlen(secret));
        if (group == 0)
            MD5_Update(&md5Context, (unsigned char *)&(sp.authenticator),
                       (unsigned int)sizeof(sp.authenticator));
        else
            MD5_Update(&md5Context, p - 16, 16);
        MD5_Final(p, &md5Context);
        for (i = 0; *pass != 0 && i < 16; i++, pass++)
            *(p + i) ^= (unsigned char)*pass;
        p += 16;
        if (*pass == 0)
            break;
    }

    *(passStart + 1) = p - passStart;

    *p++ = RADIUS_NAS_IP_ADDRESS;
    *p++ = 6;

    if (sl.ss_family == AF_INET) {
        memcpy(p, (char *)&((struct sockaddr_in *)&sl)->sin_addr, 4);
        p += 4;
    } else {
        memcpy(p, (char *)&((struct sockaddr_in6 *)&sl)->sin6_addr, 16);
        p += 16;
    }

    if (radiusPortSpecified) {
        *p++ = RADIUS_NAS_PORT;
        *p++ = 6;
        memcpy(p, (char *)&radiusPort, 4);
        p += 4;
    }

    if (radiusPortTypeSpecified) {
        *p++ = RADIUS_NAS_PORT_TYPE;
        *p++ = 6;
        memcpy(p, (char *)&radiusPortType, 4);
        p += 4;
    }

    slen = (int)(p - (unsigned char *)&sp);
    sp.packetLength = htons((unsigned short)slen);

    result = 1;

    needSend = 1;
    UUtime(&started);
    last = started;
    for (;;) {
        UUtime(&now);
        if (started + radiusTimeout < now) {
            Log("No response from Radius server %s", radiusHost);
            break;
        }
        if (now >= last + radiusRetry) {
            needSend = 1;
        }
#ifdef USEPOLL
        pfds.fd = r;
        pfds.events = POLLIN;
        if (needSend)
            pfds.events = POLLOUT;
        TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(poll(&pfds, 1, 1000)));
        if (rc < 0) {
            Log("Radius poll returned %d", socket_errno);
            break;
        }
        if (rc == 0)
            continue;
        canSend = needSend && pfds.revents & POLLOUT;
        canRecv = pfds.revents & POLLIN;
#else
        FD_ZERO(&rfds);
        FD_ZERO(&wfds);
        FD_SET(r, &rfds);
        if (needSend)
            FD_SET(r, &wfds);
        pollInterval.tv_sec = 1;
        pollInterval.tv_usec = 0;
        TEMP_FAILURE_RETRY_INT(
            rc = interrupted_neg_int(select(r, &rfds, &wfds, NULL, &pollInterval)));
        if (rc == 0)
            continue;
        if (rc == SOCKET_ERROR) {
            Log("Radius select returned %d", socket_errno);
            goto Finished;
        }
        canSend = needSend && FD_ISSET(r, &wfds);
        canRecv = FD_ISSET(r, &rfds);
#endif

        if (needSend && canSend) {
            if (optionRelaxedRADIUS == 0) {
                if (send(r, (char *)&sp, slen, 0) == slen) {
                    LogRADIUS(debug, "RADIUS sent from", get_port(&sl), (unsigned char *)&sp, slen);
                    needSend = 0;
                    UUtime(&last);
                }
            } else {
                if (sendto(r, (char *)&sp, slen, 0, (struct sockaddr *)&sa, sizeof(sa)) == slen) {
                    LogRADIUS(debug, "RADIUS sent from", get_port(&sl), (unsigned char *)&sp, slen);
                    needSend = 0;
                    UUtime(&last);
                }
            }
        }

        if (!canRecv)
            continue;

        /* UURecv wasn't interchangeable here, which is somewhat odd */
        rc = recv(r, (char *)&rp, sizeof(rp), 0);
        if (rc > 0) {
            LogRADIUS(debug, "RADIUS recv from %d", get_port(&sl), (unsigned char *)&rp, rc);
        }
        /* Eliminate the easy criteria for invalid response */
        if (rc < 20 || rp.identifier != sp.identifier || (rlen = ntohs(rp.packetLength)) > rc) {
            if (debug) {
                Log("RADIUS runt, wrong identifier or wrong length");
            }
            continue;
        }

        MD5_Init(&md5Context);
        MD5_Update(&md5Context, (unsigned char *)(&rp), 4);
        MD5_Update(&md5Context, (unsigned char *)&sp.authenticator,
                   (unsigned int)sizeof(sp.authenticator));
        if (rlen > 20)
            MD5_Update(&md5Context, (unsigned char *)&rp.attributes, rlen - 20);
        MD5_Update(&md5Context, (unsigned char *)secret, (unsigned int)strlen(secret));
        MD5_Final((unsigned char *)&checkResponse, &md5Context);

        if (memcmp(checkResponse, &rp.authenticator, sizeof(checkResponse)) != 0) {
            if (debug) {
                Log("RADIUS authenticator invalid, ignored");
            }
            continue;
        }

        if (rp.code == RADIUS_ACCESS_REJECT)
            break;
        if (rp.code == RADIUS_ACCESS_ACCEPT) {
            result = 0;
            break;
        }
        if (debug) {
            Log("RADIUS ignore unexpected code %d", rp.code);
        }
    }

Finished:
    if (r != INVALID_SOCKET)
        closesocket(r);
    return result;
}
