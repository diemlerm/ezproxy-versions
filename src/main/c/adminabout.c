#define __UUFILE__ "adminabout.c"

#include "ezproxyversion.h"
#include "common.h"
#include "uustring.h"

#include "adminabout.h"

#include "wskeylicense.h"

#ifndef WIN32
#ifndef DISABLEUTSNAME
#include <sys/utsname.h>
#endif
#endif

#include <libxml/xmlversion.h>
#include <maxminddb.h>
#include <openssl/opensslv.h>
#include <ldap_features.h>
#include <xmlsec/version.h>
#include <zlib.h>
#include "sqlite3.h"

void AdminAboutLicense(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket;

#if (EZPROXYRELEASELEVEL == 1) && EXPIREYEAR && EXPIREMONTH && EXPIREDAY
    WriteLine(s, "<p>BETA version expires %04d-%02d-%02d</p>\n", EXPIREYEAR, EXPIREMONTH,
              EXPIREDAY);
#endif

    struct WSKEYLICENSE *license = WskeyActiveLicense();
    if (license && license->key[0]) {
        char expires[20];
        struct tm tm;
        strftime(expires, sizeof(expires), "%Y-%m-%d %H:%M:%S",
                 UUlocaltime_r(&license->expiry, &tm));
        if (isLicenseExpired(license)) {
            WriteLine(s, "<p>EZproxy license key expired on %s.</p>", expires);
        } else {
            WriteLine(s, "<p>EZproxy license key requires revalidation with OCLC by %s</p>",
                      expires);
        }
    } else {
        UUSendZ(s, "<p>No valid license key installed</p>\n");
    }
}

void AdminAbout(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;

    AdminHeader(t, 0, "About EZproxy");
    WriteLine(s, "<p>%s</p>\n", EZPROXYVERSION);

    AdminAboutLicense(t);

#ifdef WIN32
    {
        DWORD dwVerInfoSize; /* Size of version information block */
        LPSTR lpProduct;
        LPSTR lpVersion;    /* String pointer to 'version' text */
        DWORD dwVerHnd = 0; /* An 'ignored' parameter, always '0' */
        UINT uVersionLen;
        WORD wRootLen;
        BOOL bRetCode;
        char szFullPath[256];
        char szGetName[256];
        DWORD dwVersion;
        char szVersion[40];

        /* get the .exe path */
        GetModuleFileName(NULL, szFullPath, sizeof(szFullPath));
        /* Now lets dive in and pull out the version information: */
        dwVerInfoSize = GetFileVersionInfoSize(szFullPath, &dwVerHnd);
        if (dwVerInfoSize) {
            LPSTR lpstrVffInfo;
            HANDLE hMem;
            hMem = GlobalAlloc(GMEM_MOVEABLE, dwVerInfoSize);
            lpstrVffInfo = GlobalLock(hMem);
            GetFileVersionInfo(szFullPath, dwVerHnd, dwVerInfoSize, lpstrVffInfo);
            /* The below 'hex' value looks a little confusing, but */
            /* essentially what it is, is the hexadecimal representation */
            /* of a couple different values that represent the language */
            /* and character set that we are wanting string values for. */
            /* 040904E4 is a very common one, because it means: */
            /*   US English, Windows MultiLingual characterset */
            /* Or to pull it all apart: */
            /* 04------        = SUBLANG_ENGLISH_USA */
            /* --09----        = LANG_ENGLISH */
            /* --11----        = LANG_JAPANESE */
            /* ----04E4 = 1252 = Codepage for Windows:Multilingual */
            lstrcpy(szGetName, "\\StringFileInfo\\040904b0\\");
            wRootLen = lstrlen(szGetName); /* Save this position */

            lstrcat(szGetName, "ProductName");
            uVersionLen = 0;
            lpVersion = NULL;
            bRetCode = VerQueryValue((LPVOID)lpstrVffInfo, (LPSTR)szGetName, (LPVOID)&lpProduct,
                                     (UINT *)&uVersionLen);
            if (bRetCode == 0)
                lpProduct = "EZproxy";

            szGetName[wRootLen] = 0;
            lstrcat(szGetName, "FileVersion");
            uVersionLen = 0;
            lpVersion = NULL;
            bRetCode = VerQueryValue((LPVOID)lpstrVffInfo, (LPSTR)szGetName, (LPVOID)&lpVersion,
                                     (UINT *)&uVersionLen);

            if (bRetCode == 0)
                lpVersion = "(unknown)";

            /*
            WriteLine(s, "%s version %s\n", lpProduct, lpVersion);
            WriteLine(s, "(%s)<br>\n", szFullPath);
            */
            GlobalUnlock(hMem);
            GlobalFree(hMem);
            dwVersion = GetVersion();
            if (dwVersion < 0x80000000) { /* Windows NT */
                wsprintf(szVersion, "Microsoft Windows NT %u.%u (Build: %u)",
                         (DWORD)(LOBYTE(LOWORD(dwVersion))), (DWORD)(HIBYTE(LOWORD(dwVersion))),
                         (DWORD)(HIWORD(dwVersion)));
            } else if (LOBYTE(LOWORD(dwVersion)) < 4) { /* Win32s */
                wsprintf(szVersion, "Microsoft Win32s %u.%u (Build: %u)",
                         (DWORD)(LOBYTE(LOWORD(dwVersion))), (DWORD)(HIBYTE(LOWORD(dwVersion))),
                         (DWORD)(HIWORD(dwVersion) & ~0x8000));
            } else {
                /* Windows 95 */
                wsprintf(szVersion, "Microsoft Windows 95 %u.%u",
                         (DWORD)(LOBYTE(LOWORD(dwVersion))), (DWORD)(HIBYTE(LOWORD(dwVersion))));
            }
            /* now display the finished product */
            UUSendZ(s, "<p>Operating system information: ");
            SendHTMLEncoded(s, szVersion);
            UUSendZ(s, "</p>\n");
        }
    }
#elif !defined(DISABLEUTSNAME)
    {
        struct utsname uts;
        memset(&uts, 0, sizeof(uts));
        if (uname(&uts) >= 0) {
            UUSendZ(s, "<p>Operating system information: ");
            SendHTMLEncoded(s, uts.sysname);
            UUSendZ(s, " ");
            SendHTMLEncoded(s, uts.release);
            UUSendZ(s, " ");
            SendHTMLEncoded(s, uts.version);
            UUSendZ(s, " ");
            SendHTMLEncoded(s, uts.machine);
            UUSendZ(s, "</p>\n");
        }
    }
#endif

    char versionString[128];

    UUSendZ(s, "<p>Library versions</p>\n");

    UUSendZ(s, "<ul>\n");

    UUSendZ(s, "<li>LibXML ");
    SendHTMLEncoded(s, LIBXML_DOTTED_VERSION);
    UUSendZ(s, "</li>\n");

    UUSendZ(s, "<li>MaxMindDB ");
    SendHTMLEncoded(s, "1.3.2");
    UUSendZ(s, "</li>\n");

    sprintf(versionString, "%d.%d.%d", LDAP_VENDOR_VERSION_MAJOR, LDAP_VENDOR_VERSION_MINOR,
            LDAP_VENDOR_VERSION_PATCH);
    UUSendZ(s, "<li>OpenLDAP ");
    SendHTMLEncoded(s, versionString);
    UUSendZ(s, "</li>\n");

    UUSendZ(s, "<li>");
    SendHTMLEncoded(s, OPENSSL_VERSION_TEXT);
    UUSendZ(s, "</li>\n");

    sprintf(versionString, "%d.%d", PCRE_MAJOR, PCRE_MINOR);
    UUSendZ(s, "<li>PCRE ");
    SendHTMLEncoded(s, versionString);
    UUSendZ(s, "</li>\n");

    UUSendZ(s, "<li>SQLite ");
    SendHTMLEncoded(s, sqlite3_libversion());
    UUSendZ(s, "</li>\n");

    UUSendZ(s, "<li>XMLSec ");
    SendHTMLEncoded(s, XMLSEC_VERSION);
    UUSendZ(s, "</li>\n");

    UUSendZ(s, "<li>zlib ");
    SendHTMLEncoded(s, ZLIB_VERSION);
    UUSendZ(s, "</li>\n");

    UUSendZ(s, "</ul>\n");

    AdminFooter(t, 0);
}
