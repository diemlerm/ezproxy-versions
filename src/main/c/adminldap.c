/**
 * This module implements LDAP user authentication using OpenLDAP.  This
 * version supercedes the original support which implemented a limited,
 * direct bind approach.  The original method still exists in usrldap.c.
 *
 * This authentication method supports common conditions and actions.
 *
 * https://github.com/rroemhild/docker-test-openldap works very well for testing.
 * docker run --rm -p 10389:10389 -p 10636:10636 rroemhild/test-openldap
 * See the github page for the various pre-existing accounts.
 */

#define __UUFILE__ "adminldap.c"

#include "common.h"
#include "obscure.h"
#include "usrldap2.h"

#include <ldap.h>

void AdminLDAP(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    const int ALHOST = 0;
    const int ALBINDUSER = 1;
    const int ALBINDPASS = 2;
    const int ALBASE = 3;
    const int ALATTR = 4;
    const int ALSCOPE = 5;
    const int ALFILTER = 6;
    const int ALVERSION = 7;
    const int ALSSL = 8;
    const int ALTESTUSER = 9;
    const int ALTESTPASS = 10;
    const int ALSUBMIT = 11;
    const int ALNEWBASE = 12;
    const int ALDISABLEREFERRALCHASING = 13;
    const int ALSSLCIPHERSUITE = 14;
    struct FORMFIELD ffs[] = {
        {"host",                   NULL, 0, 0, 128 },
        {"binduser",               NULL, 0, 0, 128 },
        {"bindpass",               NULL, 0, 0, 64  },
        {"base",                   NULL, 0, 0, 0   },
        {"attr",                   NULL, 0, 0, 0   },
        {"scope",                  NULL, 0, 0, 5   },
        {"filter",                 NULL, 0, 0, 128 },
        {"version",                NULL, 0, 0, 1   },
        {"ssl",                    NULL, 0, 0, 10  },
        {"testuser",               NULL, 0, 0, 64  },
        {"testpass",               NULL, 0, 0, 64  },
        {"submit",                 NULL, 0, 0, 0   },
        {"newbase",                NULL, 0, 0, 0   },
        {"disablereferralchasing", NULL, 0, 0, 5   },
        {"sslciphersuite",         NULL, 0, 0, 1024},
        {NULL,                     NULL, 0, 0, 0   }
    };
    char *host, *sslCipherSuite, *bindUser, *bindPass, *base, *attr, *scope, *filter, *version,
        *ssl, *testUser, *testPass, *disableReferralChasing;
    char *hostError = NULL, *sslCipherSuiteError = NULL, *bindUserError = NULL,
         *bindPassError = NULL, *baseError = NULL, *attrError = NULL, *filterError = NULL,
         *testUserError = NULL, *testPassError = NULL;
    char ipBuffer[INET6_ADDRSTRLEN + 2];  // allow room for IPv6 brackets
    int versionCode;
    LDAP *ld = NULL, *ld2 = NULL;
    int i;
    char *attrs[] = {"*", NULL};
    char *dn, *a;
    char **vals;
    BerElement *ber;
    int scopeCode;
    char *unobscurePassword = NULL;

    int tries = 0;
    LDAPMessage *res, *e;
    int rc;
    int never = LDAP_OPT_X_TLS_NEVER;
    char *bindUri = NULL;
    char *sport;
    PORT port, defaultPort;
    char *searchFilter = NULL;
    int count = 0;
    int errors = 0;
    BOOL findSearchBase = 0;
    BOOL foundSearchBase = 0;
    BOOL tableOpen = 0;
    BOOL formOpen;
    struct sockaddr_storage sa;
    enum STARTTLSTYPE startTLS;

    // OpenLDAP doesn't allow ready support of IPv6 with MinGW
    // due to lack of inet_ntop as a standard function.  On that
    // platform, force AF_INET (IPv4), but on others, allow
    // IPv4 or IPv6 (AF_UNSPEC).
#ifdef WIN32
    const int family = AF_INET;
#else
    const int family = AF_UNSPEC;
#endif

    FindFormFields(query, post, ffs);
    AdminHeader(t, 0, "Test LDAP");

    if (!(host = ffs[ALHOST].value))
        host = "";
    if (!(sslCipherSuite = ffs[ALSSLCIPHERSUITE].value))
        sslCipherSuite = "";
    if (!(bindUser = ffs[ALBINDUSER].value))
        bindUser = "";
    if (!(bindPass = ffs[ALBINDPASS].value))
        bindPass = "";
    unobscurePassword = UnobscureString(bindPass);
    if (!(base = ffs[ALBASE].value))
        base = "";
    if (!(attr = ffs[ALATTR].value))
        attr = "";
    if (!(scope = ffs[ALSCOPE].value))
        scope = "";
    if (!(filter = ffs[ALFILTER].value))
        filter = "";
    if (!(version = ffs[ALVERSION].value))
        version = "3";
    if (!(ssl = ffs[ALSSL].value)) {
        ssl = "";
    }
    // Default to enabled
    startTLS = STENABLED;
    if (stricmp(ssl, "disabled") == 0) {
        startTLS = STDISABLED;
    }
    if (stricmp(ssl, "enabled") == 0) {
        startTLS = STENABLED;
    }
    if (stricmp(ssl, "ldaps") == 0) {
        startTLS = STLDAPS;
    }
    if (!(testUser = ffs[ALTESTUSER].value))
        testUser = "";
    if (!(testPass = ffs[ALTESTPASS].value))
        testPass = "";
    if (!(disableReferralChasing = ffs[ALDISABLEREFERRALCHASING].value))
        disableReferralChasing = "";

    if (ffs[ALSUBMIT].value == NULL) {
        errors = 1;
        if (ffs[ALNEWBASE].value) {
            base = ffs[ALNEWBASE].value;
        } else {
            if (*filter == 0)
                filter = "(objectClass=person)";
            scope = "on";
        }
    } else {
        findSearchBase = ffs[ALSUBMIT].value[0] == 'f';
        if (*host == 0) {
            hostError = "Host is required";
            errors++;
        }
    }

    if (strcmp(version, "2") != 0)
        version = "3";

    scopeCode = scope && strcmp(scope, "on") == 0 ? LDAP_SCOPE_SUBTREE : LDAP_SCOPE_ONELEVEL;

    WriteLine(s, "<form action='%s/ldap' method='post'>\n", (myUrlHttps ? myUrlHttps : myUrlHttp));
    formOpen = 1;
    UUSendZ(s, "<table>\n");

    WriteField(t, "Bind User", "binduser", bindUser, bindUserError, 40, 128, NULL);
    WriteField(t, "Bind Password", "bindpass", bindPass, bindPassError, 40, 64, "password");

    UUSendZ(s, "<tr><td>LDAP Version: </td><td>");
    WriteLine(s,
              "<input name='version' id='version-2' type='radio' value='2'%s /> <label "
              "for='version-2'>2</label>&nbsp;&nbsp;&nbsp;",
              *version == '2' ? "checked='checked'" : "");
    WriteLine(s,
              "<input name='version' id='version-3' type='radio' value='3'%s /> <label "
              "for='version-3'>3</label></td></tr>\n",
              *version == '3' ? "checked='checked'" : "");

    UUSendZ(s, "<tr><td><label for='ssl'>SSL/TLS</label> </td><td><select name='ssl' id='ssl'>");
    WriteLine(s, "<option value='disabled' %s>StartTLS disabled</option>",
              startTLS == STDISABLED ? "selected" : "");
    WriteLine(s, "<option value='enabled' %s>StartTLS enabled</option>",
              startTLS == STENABLED ? "selected" : "");
    WriteLine(s, "<option value='ldaps' %s>ldaps</option>", startTLS == STLDAPS ? "selected" : "");
    UUSendZ(s, "</select></td></tr>\n");

    WriteField(t, "SSLCipherSuite", "sslciphersuite", sslCipherSuite, sslCipherSuiteError, 64,
               ffs[ALSSLCIPHERSUITE].maxLen, NULL);
    UUSendZ(s, "<tr><td>(normally left empty)&nbsp;</td><td></td></tr>\n");
    WriteField(t, "Host[:port]", "host", host, hostError, 64, 256, NULL);
    WriteField(t, "Search Base", "base", base, baseError, 64, 256, NULL);
    WriteLine(s,
              "<tr><td><label for='scope'>Include subcontainers in search:</label> </td><td><input "
              "type='checkbox' name='scope' id='scope'%s></td></tr>\n",
              scope && *scope ? " checked='checked'" : "");
    WriteLine(s,
              "<tr><td><label for='disablereferralchasing'>Disable referral chasing:</label> "
              "</td><td><input type='checkbox' name='disablereferralchasing' "
              "id='disablereferralchasing'%s></td></tr>\n",
              disableReferralChasing && *disableReferralChasing ? " checked='checked'" : "");

    WriteField(t, "Filter", "filter", filter, filterError, 64, 256, NULL);

    WriteField(t, "Search Attribute", "attr", attr, attrError, 16, 32, NULL);
    UUSendZ(s, "<tr><td>(e.g. cn, uid, sAMAccountName)&nbsp;&nbsp;</td><td></td></tr>\n");
    WriteField(t, "Test User", "testuser", testUser, testUserError, 40, 64, NULL);
    WriteField(t, "Test Password", "testpass", testPass, testPassError, 40, 64, "password");
    UUSendZ(
        s,
        "<tr><td><input type='submit' name='submit' value='Search' /></td><td>&nbsp;</td></tr>\n");
    UUSendZ(s, "</table>\n");
    UUSendZ(s,
            "<p>If you do not know the search base for this server, fill in the Host, check SSL if "
            "relevant, and then click\n");
    UUSendZ(s, "<input type='submit' name='submit' value='find search base' /></p>\n");

    if (errors)
        goto NoSearch;

    defaultPort = startTLS == STLDAPS ? 636 : 389;

    ParseHost(host, &sport, NULL, TRUE);
    if (sport) {
        *sport++ = 0;
        port = atoi(sport);
    } else {
        port = defaultPort;
    }

    TrimHost(host, "[]", host);

    (void)ldap_set_option(NULL, LDAP_OPT_X_TLS_REQUIRE_CERT, &never);
    versionCode = (*version == '3' ? LDAP_VERSION3 : LDAP_VERSION2);

    if (!(bindUri = malloc(128)))
        PANIC;

    UUSendHR(s);

    if (findSearchBase) {
        UUSendZ(s, "<p>Search base</p>\n");
        scopeCode = LDAP_SCOPE_BASE;
        version = "3";
        bindUser = NULL;
        bindPass = NULL;
        base = "";
        if (!(searchFilter = strdup("(objectclass=*)")))
            PANIC;
        attrs[0] = "namingContexts";
    } else {
        UUSendZ(s, "<p>Testing LDAP search</p>");
        searchFilter = LDAPFilter(filter, attr, testUser);
    }

    UUFlush(s);

Retry:
    if (UUGetHostByName(host, &sa, (PORT)port, family)) {
        UUSendZ(s, "LDAP host not found: ");
        SendHTMLEncoded(s, host);
        goto NoSearch;
    }

    // store the ip address into a buffer
    ipToStrV6Bracketed(&sa, ipBuffer, sizeof(ipBuffer));
    sprintf(bindUri, "ldap%s://%s:%d", (startTLS == STLDAPS ? "s" : ""), ipBuffer, port);

    if (optionDisableTestLDAPInternal &&
        // TODO: this is IPv4 specific
        (strnicmp(ipBuffer, "127.", 4) == 0 || strncmp(ipBuffer, "192.168.", 8) == 0)) {
        UUSendZ(s, "<p>LDAP host not found: ");
        SendHTMLEncoded(s, host);
        goto NoSearch;
    }

    ldap_initialize(&ld, bindUri);

    if (ld == NULL) {
        perror("ldap_init");
        exit(EXIT_FAILURE);
    }

    (void)ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &versionCode);

    (void)ldap_set_option(ld, LDAP_OPT_X_TLS_CONNECT_ARG, sslCipherSuite);
    (void)ldap_set_option(ld, LDAP_OPT_X_TLS_CONNECT_CB, FindUserLDAP2SetSSLCipherSuite);

    if (startTLS == STENABLED) {
        time_t start = UUtime(NULL);
        rc = ldap_start_tls_s(ld, NULL, NULL);
        time_t stop = UUtime(NULL);
        if (debugLevel > 10) {
            Log("AdminLDAP ldap_start_tls_s took %d seconds and returned %d", stop - start, rc);
        }
        if (rc == LDAP_PROTOCOL_ERROR) {
            UUSendZ(s, "<div>Server does not support to support StartTLS</div>\n");
        }
    } else {
        rc = LDAP_SUCCESS;
    }

    if (bindUser && *bindUser == 0)
        bindUser = NULL;

    if (bindPass && *bindPass == 0)
        bindPass = NULL;

    char *usePassword = bindPass;
    // We only substitute the unobscurePassword if the bindPass has
    // not been reset to NULL such as happens when using
    // Find Search Base.
    if (bindPass && unobscurePassword) {
        usePassword = unobscurePassword;
    }

    if (rc == LDAP_SUCCESS) {
        rc = ldap_bind_s(ld, bindUser, usePassword, LDAP_AUTH_SIMPLE);
    }
    if (rc != LDAP_SUCCESS) {
        if (rc != LDAP_SERVER_DOWN) {
            char *extendedError = NULL;
            ldap_get_option(ld, LDAP_OPT_ERROR_STRING, &extendedError);
            WriteLine(s, "<div>Attempt to connect to LDAP server failed: %d ", rc);
            SendHTMLEncoded(s, ldap_err2string(rc));
            UUSendZ(s, "</div>\n");
            if (extendedError) {
                UUSendZ(s, "<div>Extended error details: ");
                SendHTMLEncoded(s, extendedError);
                FreeThenNull(extendedError);
                UUSendZ(s, "</div>\n");
            }
        }
        ldap_unbind(ld);
        ld = NULL;
        if (rc == LDAP_SERVER_DOWN) {
            WriteLine(s, "<p>Unable to connect to %s (%s)</p>\n", host, ipBuffer);
            tries++;
            if (tries < UUGetHostByNameFailedAddress(host, &sa, family)) {
                goto Retry;
            }
            if (startTLS == STLDAPS && stricmp(sslCipherSuite, "TLSv1") != 0) {
                UUSendZ(s,
                        "<p>If you are trying to connect to a Windows 2003 Server, try specifying "
                        "<code>TLSv1</code> in SSLCipherSuite above.</p>");
            }
        }
        goto NoSearch;
    }

    (void)ldap_set_option(
        ld, LDAP_OPT_REFERRALS,
        (disableReferralChasing && *disableReferralChasing ? LDAP_OPT_OFF : LDAP_OPT_ON));

    count = 0;

    if ((rc = ldap_search_s(ld, base, scopeCode, searchFilter, attrs, 0, &res)) != LDAP_SUCCESS) {
        if (rc == LDAP_NO_SUCH_OBJECT) {
            goto NoResults;
        } else {
            WriteLine(s, "LDAP search failed: %d ", rc);
            SendHTMLEncoded(s, ldap_err2string(rc));
            WriteLine(s, " (%d)</p>\n", rc);
        }
        goto NoSearch;
    }

    if (*attr && ldap_count_entries(ld, res) > 1) {
        UUSendZ(s,
                "<p>This search will not pass LDAP authentication since it matched more than one "
                "object.</p>\n");
    }

    /* for each entry print out name + all attrs and values */
    for (e = ldap_first_entry(ld, res); e != NULL; e = ldap_next_entry(ld, e)) {
        count++;
        if ((dn = ldap_get_dn(ld, e)) != NULL) {
            if (!findSearchBase) {
                if (tableOpen == 0) {
                    UUSendZ(s, "<table class='th-row-nobold'>\n");
                    tableOpen = 1;
                } else {
                    UUSendZ(s, "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
                }

                UUSendZ(
                    s,
                    "<tr valign='top'><th scope='row'><strong>DN: </strong></th><td><div><strong>");
                SendHTMLEncoded(s, dn);
                UUSendZ(s, "</strong></div>");
                if (*testUser && *testPass) {
                    char *extendedError = NULL;

                    ldap_initialize(&ld2, bindUri);

                    if (ld2 == NULL) {
                        perror("ldap_init");
                        exit(EXIT_FAILURE);
                    }

                    (void)ldap_set_option(ld2, LDAP_OPT_PROTOCOL_VERSION, &versionCode);

                    (void)ldap_set_option(ld2, LDAP_OPT_X_TLS_CONNECT_ARG, sslCipherSuite);
                    (void)ldap_set_option(ld2, LDAP_OPT_X_TLS_CONNECT_CB,
                                          FindUserLDAP2SetSSLCipherSuite);

                    rc = ldap_bind_s(ld2, dn, testPass, LDAP_AUTH_SIMPLE);

                    if (ld2)
                        ldap_get_option(ld2, LDAP_OPT_ERROR_STRING, &extendedError);

                    ldap_unbind(ld2);
                    ld2 = NULL;

                    WriteLine(s, "<div>Password is%s correct", (rc == LDAP_SUCCESS ? "" : " not"));
                    if (rc != LDAP_SUCCESS) {
                        WriteLine(s, ": %d ", rc);
                        SendHTMLEncoded(s, ldap_err2string(rc));
                    }
                    UUSendZ(s, "</div>\n");

                    if (extendedError && *extendedError) {
                        UUSendZ(s, "<div>Extended information: ");
                        SendHTMLEncoded(s, extendedError);
                        UUSendZ(s, "</div>\n");
                    }

                    FreeThenNull(extendedError);
                }
                UUSendZ(s, "</td></tr>\n");
            }
            ldap_memfree(dn);
        }
        for (a = ldap_first_attribute(ld, e, &ber); a != NULL;
             a = ldap_next_attribute(ld, e, ber)) {
            if ((vals = (char **)ldap_get_values(ld, e, a)) != NULL) {
                for (i = 0; vals[i] != NULL; i++) {
                    if (findSearchBase) {
                        if (vals[i][0] == 0 ||
                            strnicmp(vals[i], "CN=Schema,CN=Configuration,", 27) == 0 ||
                            strnicmp(vals[i], "CN=Configuration,", 17) == 0)
                            continue;
                        foundSearchBase = 1;
                        UUSendZ(s, "<div><input type='submit' name='newbase' value='");
                        SendQuotedURL(t, vals[i]);
                        UUSendZ(s, "' /></div>\n");
                        continue;
                    }

                    UUSendZ(s, "<tr valign='top'><th scope='row'>");
                    SendHTMLEncoded(s, a);
                    UUSendZ(s, ": </th><td>");
                    SendHTMLEncoded(s, vals[i]);
                    UUSendZ(s, "</td></tr>\n");
                }
                ldap_value_free(vals);
            }
            ldap_memfree(a);
        }
    }

    ldap_msgfree(res);

    if (tableOpen)
        UUSendZ(s, "</table>\n");

NoResults:
    if (findSearchBase == 0) {
        if (count == 0) {
            UUSendZ(s, "<p>\nSearch returned no results</p>\n");
            if (bindPass == NULL) {
                UUSendZ(s,
                        "<p>Anonymous searching may be disabled; you may need to contact your LDAP "
                        "administrator to have an account created for Bind User and Bind "
                        "Password.</p>\n");
            }
        }
    } else {
        if (foundSearchBase == 0)
            UUSendZ(s, "<p>Attempt to find search base failed.</p>\n");
    }

    if (findSearchBase || *attr == 0)
        goto NoSearch;

    UUSendZ(s, "<hr />\n<p>To use this LDAP search with EZproxy, add the following to " EZPROXYUSR
               ".</p>\n");

    UUSendZ(s, "<tt><nobr>\n");
    UUSendZ(s, "<div>::LDAP</div>\n");
    if (bindUser) {
        UUSendZ(s, "<div>BindUser ");
        SendHTMLEncoded(s, bindUser);
        UUSendZ(s, "</div>\n");
        if (bindPass) {
            UUSendZ(s, "<div>BindPassword -Obscure ");
            if (unobscurePassword) {
                SendHTMLEncoded(s, bindPass);
            } else {
                char *obscure = ObscureString(bindPass);
                SendHTMLEncoded(s, obscure);
                FreeThenNull(obscure);
            }
            UUSendZ(s, "</div>\n");
        }
    }

    if (*version == '2') {
        UUSendZ(s, "<div>LDAPV2</div>\n");
    }

    if (*ssl && *sslCipherSuite) {
        UUSendZ(s, "<div>SSLCipherSuite ");
        SendHTMLEncoded(s, sslCipherSuite);
        UUSendZ(s, "</div>\n");
    }

    if (disableReferralChasing && *disableReferralChasing) {
        UUSendZ(s, "<div>DisableReferralChasing</div>\n");
    }

    if (startTLS == STENABLED) {
        WriteLine(s, "<div>StartTLS enabled</div>\n");
    }

    WriteLine(s, "<div>URL ldap%s://", startTLS == STLDAPS ? "s" : "");
    SendHTMLEncoded(s, host);
    if (port != defaultPort)
        WriteLine(s, ":%d", port);
    UUSendZ(s, "/");
    SendHTMLEncoded(s, base);
    UUSendZ(s, "?");
    SendHTMLEncoded(s, attr);
    WriteLine(s, "?%s", scopeCode == LDAP_SCOPE_SUBTREE ? "sub" : "one");
    if (*filter) {
        UUSendZ(s, "?");
        SendHTMLEncoded(s, filter);
    }
    UUSendZ(s, "</div>\n<div>IfUnauthenticated; Stop</div>\n<div>/LDAP</div>\n");

    UUSendZ(s, "</nobr></tt>\n");

NoSearch:
    if (formOpen)
        UUSendZ(s, "</form>\n");

    AdminFooter(t, 0);

    if (ld) {
        ldap_unbind(ld);
        ld = NULL;
    }

    FreeThenNull(searchFilter);
    FreeThenNull(bindUri);
    FreeThenNull(unobscurePassword);
}
