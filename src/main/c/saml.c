#define __UUFILE__ "saml.c"

#include "common.h"
#include "usr.h"
#include "saml.h"
#include "adminaudit.h"
#include <math.h>

#define SAMLDebugThreshold_Low 1
#define SAMLDebugThreshold_Medium 15
#define SAMLDebugThreshold_High 7999

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#ifdef DEBUG_COMPILATION_MODE
#define AT __UUFILE__ ":" TOSTRING(__LINE__) " "
#else
#define AT "SAML "
#endif

#define NULLValue ""
#define PNS(x) ((char *)(x) ? (char *)(x) : (char *)NULLValue)

enum SHIBBOLETHAVAILABLE shibbolethAvailable12 = SHIBBOLETHAVAILABLEOFF;
enum SHIBBOLETHAVAILABLE shibbolethAvailable13 = SHIBBOLETHAVAILABLEOFF;
enum SHIBBOLETHAVAILABLE shibbolethAvailable20 = SHIBBOLETHAVAILABLEOFF;
#define SAML_VERSION_TEXT(x)                   \
    ((x) == SHIBVERSION20)   ? "SHIBVERSION20" \
    : ((x) == SHIBVERSION13) ? "SHIBVERSION13" \
                             : "SHIBVERSION13OR20"

struct SAMLCONTEXTCOUNTERS {
    int totalEncryptedNodeCount;
    int totalDecryptedNodeCount;
    int totalSignatureNodeCount;
    int totalSignatureVerifiedNodeCount;

    int totalDocumentCount;
    int rejectedDocumentCount;

    int totalSubjectNodeCount;
    int rejectedSubjectNodeCount;

    int totalSubjectConfirmationNodeCount;
    int rejectedSubjectConfirmationNodeCount;

    int totalNameIDNodeCount;
    int rejectedNameIDNodeCount;

    int totalResponseCount;
    int signedResponseCount;
    int rejectedResponseCount;

    BOOL statusCodeSuccess;
    int totalStatusNodeCount;
    int rejectedStatusNodeCount;

    int totalAssertionCount;
    int signedAssertionCount;
    int encryptedAssertionCount;
    int rejectedAssertionCount;

    int totalAttributeStatementCount;
    int signedAttributeStatementCount;
    int rejectedAttributeStatementCount;

    int totalAttributeCount;
    int signedAttributeCount;
    int encryptedAttributeCount;
    int rejectedAttributeCount;

    int totalAuthnStatementCount;
    int encryptedAuthnStatementCount;
    int rejectedAuthnStatementCount;

    int totalAttributeValueCount;
    int signedAttributeValueCount;
    int encryptedAttributeValueCount;
    int rejectedAttributeValueCount;
    int outOfScopeAttributeValueCount;

    BOOL httpsGetMethod;
};

struct SAMLCONTEXT {
    enum SHIBBOLETHVERSION shibbolethVersion;
    struct SHIBBOLETHSITE *shibbolethSite;
    BOOL holdMutex;
    xmlDocPtr responseDoc;
    char *peerCommonName;
    xmlXPathContextPtr xPathCtx;
    char *responseIssuer;
    struct SAMLATTRIBUTE *saList;
    struct SAMLATTRIBUTE *saListLast;
    xmlXPathObjectPtr xPathObjScope;
    BOOL assertionMustBeSigned;
    BOOL includeInvalidScope;
    char *nameID;
    char *nameIDFormat;
    char *nameIDNameQualifier;
    char *nameIDSPNameQualifier;
    char *sessionIndex;
    char *SubjectConfirmationMethod;
    char *statusCode;
    struct SAMLCONTEXTCOUNTERS counters;
    char *attributeName;
    char *attributeFriendlyName;
};

struct SHIBBOLETHSITE *shibbolethSites = NULL;

char *shibbolethSitesCommonEntityID = NULL;

#ifdef WIN32
/* io.h, fcntl.h and share.h needed for _sopen in Win32 */
#include <io.h>
#include <fcntl.h>
#include <share.h>
#include <lm.h>
#endif

#include <sys/stat.h>

#include "uuxml.h"
#define LIBXML_XPTR_ENABLED
#define LIBXML_VALID_ENABLED
#include <xmlsec/xmlsec.h>
#include <xmlsec/xmltree.h>
#include <xmlsec/xmlenc.h>
#include <xmlsec/xmldsig.h>
#include <xmlsec/templates.h>
#include <xmlsec/crypto.h>
#include <xmlsec/keysmngr.h>
#include <xmlsec/errors.h>
#include <xmlsec/app.h>

#include "html.h"

#define EntityDescriptorSPrefixFormatStr "//md:EntityDescriptor[@entityID='%s']"
#define EntityOrganization "/md:Organization"
#define KeyDescriptorPrefixFormatStr \
    "//md:EntityDescriptor[@entityID='%s']/md:IDPSSODescriptor/md:KeyDescriptor[%s]"
#define KeyInfoPrefixFormatStr "/ds:KeyInfo"
#define KeyNamePrefixFormatStr "/ds:KeyName/text()"
#define MetadataSignaturePrefixFormatStr "//md:EntitiesdDescriptor/md:Signature"
#define SAMLMetadataXPathBufferSize 8192
#define X509CRLPrefixFormatStr "/ds:X509Data/ds:X509CRL"
#define X509CertificatePrefixFormatStr "/ds:X509Data/ds:X509Certificate"

#define SAMLMsgAuthFmtLog "%-32s %9d"
#define SAMLMsgAuthFmtHTML(title, value, success)                                                \
    "  <tr>  <td>%s</td>  <td align='right'><tt><span class='%s'>%d</span></tt></td>  </tr>\n ", \
        title, ((success) ? "success" : "failure"), (value)
#define SAMLMsgAuthFmtHTMLGREEN                                                              \
    "  <tr>  <td>%s</td>  <td align='right'><tt><span class='success'>%d</span></tt></td>  " \
    "</tr>\n "
#define SAMLMsgAuthFmtHTMLRED                                                                \
    "  <tr>  <td>%s</td>  <td align='right'><tt><span class='failure'>%d</span></tt></td>  " \
    "</tr>\n "

const xmlChar *NSSOAP = BAD_CAST "http://schemas.xmlsoap.org/soap/envelope/";
const xmlChar *NSMD = BAD_CAST "urn:oasis:names:tc:SAML:2.0:metadata";
const xmlChar *NSSHIBMD = BAD_CAST "urn:mace:shibboleth:metadata:1.0";
const xmlChar *NSSHIBMD1 = BAD_CAST "urn:mace:shibboleth:1.0";
const xmlChar *NSDS = BAD_CAST "http://www.w3.org/2000/09/xmldsig#";
const xmlChar *NSSAML1 = BAD_CAST "urn:oasis:names:tc:SAML:1.0:assertion";
const xmlChar *NSSAML2 = BAD_CAST "urn:oasis:names:tc:SAML:2.0:assertion";
const xmlChar *NSSAMLP1 = BAD_CAST "urn:oasis:names:tc:SAML:1.0:protocol";
const xmlChar *NSSAMLP2 = BAD_CAST "urn:oasis:names:tc:SAML:2.0:protocol";
const xmlChar *NSIDPDISC = BAD_CAST "urn:oasis:names:tc:SAML:profiles:SSO:idp-discovery-protocol";
const xmlChar *NSXENC = BAD_CAST "http://www.w3.org/2001/04/xmlenc#";

const xmlChar *GENERIC_KEY_NAME = BAD_CAST "key";

#define SAMLIDMAX 32

static int SAMLDecryptNodes(xmlNodePtr baseNodePtr, struct SAMLCONTEXT *samlCtx);

void SendShibFailure(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket;

    HTMLHeader(t, htmlHeaderOmitCache);
    if (SendEditedFile(t, NULL, SHIBFAILURE, 0, t->buffer, 1, NULL) != 0) {
        UUSendZ(s,
                "<p>Inter-institutional access failure.  Please contact your system administrator "
                "for assistance.</p>\n");
    }
}

void SAMLXMLValueReport(xmlNodePtr node, int nodeNumber, char *value) {
    xmlNsPtr *nsPtrPtr = NULL, *save;
    char *path = (char *)xmlGetNodePath(node);

    if (nodeNumber != 0) {
        Log(AT "node path \"%s[%d]\" has value \"%s\".", PNS(path), nodeNumber, PNS(value));
    } else {
        Log(AT "node path \"%s\" has value \"%s\".", PNS(path), PNS(value));
    }
    nsPtrPtr = save = xmlGetNsList(node->doc, node);
    if (nsPtrPtr != NULL) {
        while (*nsPtrPtr) {
            Log(AT "%s namespace xmlns:%s=\"%s\".",
                (((*nsPtrPtr)->type == XML_LOCAL_NAMESPACE) ? "local" : "global"),
                PNS((*nsPtrPtr)->prefix), PNS((*nsPtrPtr)->href));
            nsPtrPtr++;
        }
        xmlFree(save);
    }
    xmlFree(path);
}

void SAMLXMLNodeReport(xmlNodePtr node, int nodeNumber) {
    xmlNsPtr *nsPtrPtr = NULL, *save;
    char *path = (char *)xmlGetNodePath(node);

    if (nodeNumber != 0) {
        Log(AT "node path \"%s[%d]\".", PNS(path), nodeNumber);
    } else {
        Log(AT "node path \"%s\".", PNS(path), nodeNumber);
    }
    nsPtrPtr = save = xmlGetNsList(node->doc, node);
    if (nsPtrPtr != NULL) {
        while (*nsPtrPtr) {
            Log(AT "%s namespace xmlns:%s=\"%s\".",
                (((*nsPtrPtr)->type == XML_LOCAL_NAMESPACE) ? "local" : "global"),
                PNS((*nsPtrPtr)->prefix), PNS((*nsPtrPtr)->href));
            nsPtrPtr++;
        }
        xmlFree(save);
    }
    xmlFree(path);
}

static void SAMLID(char *id, time_t now) {
    static long next = 0;
    long got;

    if (now == 0)
        UUtime(&now);

    UUAcquireMutex(&mActive);
    got = next++;
    UUReleaseMutex(&mActive);
    sprintf(id, "_%" PRIdMAX "%ld", (intmax_t)now, (long int)got);
}

BOOL ShibbolethAvailable(void) {
    static char report = 1;

    if (someKey == 0) {
        if (report) {
            Log(AT "Due to export restrictions, Shibboleth requires an EZproxy license.");
            report = 0;
        }
        shibbolethAvailable12 = SHIBBOLETHAVAILABLEDISABLED;
        shibbolethAvailable13 = SHIBBOLETHAVAILABLEDISABLED;
        shibbolethAvailable20 = SHIBBOLETHAVAILABLEDISABLED;
        return 0;
    }
    return 1;
}

static int SAMLAddIDAttr(xmlNodePtr node,
                         const xmlChar *attrName,
                         const xmlChar *nodeName,
                         const xmlChar *nsHref) {
    xmlAttrPtr attr, tmpAttr;
    xmlNodePtr cur;
    xmlChar *id;

    if ((node == NULL) || (attrName == NULL) || (nodeName == NULL)) {
        return (-1);
    }

    /* process children first because it does not matter much but does simplify code */
    cur = xmlSecGetNextElementNode(node->children);
    while (cur != NULL) {
        if (SAMLAddIDAttr(cur, attrName, nodeName, nsHref) < 0) {
            /* return(-1); */
        }
        cur = xmlSecGetNextElementNode(cur->next);
    }

    /* node name must match */
    if (!xmlStrEqual(node->name, nodeName)) {
        return 0;
    }

    /* if nsHref is set then it also should match */
    if ((nsHref != NULL) && (node->ns != NULL) && (!xmlStrEqual(nsHref, node->ns->href))) {
        return 0;
    }

    /* the attribute with name equal to attrName should exist */
    for (attr = node->properties; attr != NULL; attr = attr->next) {
        if (xmlStrEqual(attr->name, attrName)) {
            break;
        }
    }
    if (attr == NULL) {
        return 0;
    }

    /* and this attr should have a value */
    id = xmlNodeListGetString(node->doc, attr->children, 1);
    if (id == NULL) {
        return 0;
    }

    /* check that we don't have same ID already */
    tmpAttr = xmlGetID(node->doc, id);
    if (tmpAttr == NULL) {
        xmlAddID(NULL, node->doc, id, attr);
    } else if (tmpAttr != attr) {
        Log(AT "duplicate ID attribute '%s', error", id);
        xmlFreeThenNull(id);
        return -1;
    }
    xmlFreeThenNull(id);
    return 0;
}

static BOOL VerifySignatureShibbolethMetadata(xmlDocPtr doc, struct SHIBBOLETHSITE *ss) {
    char *signCertList;
    xmlNodePtr node = NULL;
    xmlSecDSigCtxPtr dsigCtx = NULL;
    xmlSecDSigStatus status = xmlSecDSigStatusUnknown;
    int keyCount = 0;
    int sigCount = 0;
    int keyLoadedCount = 0;
    int sigVerifiedCount = 0;

    UUxmlSecInit();

    /* find start node */
    node = xmlSecFindNode(xmlDocGetRootElement(doc), xmlSecNodeSignature, xmlSecDSigNs);

    /* Loop through all Signature nodes. */
    while ((node != NULL)) {
        sigCount++;
        signCertList = ss->signCertList;
        keyCount = 0;

        while (signCertList[0] != 0) {
            keyCount++;

            do {
                /* create signature context, we don't need keys manager in this example */
                dsigCtx = xmlSecDSigCtxCreate(NULL);
                if (dsigCtx == NULL) {
                    break;
                }

                /* load public key */
                dsigCtx->signKey = xmlSecCryptoAppKeyLoad(signCertList, xmlSecKeyDataFormatCertPem,
                                                          NULL, NULL, NULL);
                if (dsigCtx->signKey == NULL) {
                    Log(AT "failed to load certificate key for %s", signCertList);
                    break;
                }
                keyLoadedCount++;

                if (xmlSecKeySetName(dsigCtx->signKey, BAD_CAST signCertList) < 0) {
                    Log(AT "failed to set key name for key from %s", signCertList);
                    break;
                }

                /* Verify signature */
                if (xmlSecDSigCtxVerify(dsigCtx, node) < 0) {
                    status = xmlSecDSigStatusUnknown;
                    Log(AT "call to xmlSecDSigCtxVerify failed for %s", signCertList);
                } else {
                    status = dsigCtx->status;
                }
                if (status != xmlSecDSigStatusSucceeded) {
                    Log(AT "signature verify failed for %s", signCertList);
                } else {
                    if (debugLevel > SAMLDebugThreshold_High) {
                        Log(AT "signature verify succeeded for %s", signCertList);
                    }
                    sigVerifiedCount++;
                }

            } while (FALSE);

            if (dsigCtx != NULL) {
                xmlSecDSigCtxDestroy(dsigCtx);
            }

            signCertList = signCertList + strlen(signCertList) + 1;
        }

        if (node->next == NULL) {
            break;
        } else {
            node = xmlSecFindNode(node->next, xmlSecNodeSignature, xmlSecDSigNs);
        }
    }

    return ((keyCount == 0) || (sigCount == 0) ||
            ((keyCount == keyLoadedCount) && (sigCount == 1) && (sigVerifiedCount == 1)));
}

/* returns: -1 error, 0 verified, 1 not verified by any key, 2 no signature.  All keys are checked
 * to see if any will verify all of the signatures. */
static int SAMLVerifySignature(xmlNodePtr baseNodePtr, struct SAMLCONTEXT *samlCtx) {
    int res;
    xmlNodePtr node = NULL;
    xmlSecDSigCtxPtr dsigCtx = NULL;
    int refCount;
    xmlChar *key = NULL;
    unsigned char *keyDer = NULL;
    size_t keyLen;
    int keyNum = 0;
    int keyCount = 0;
    int sigCount = 0;
    int sigVerfiedCount = 0;
    char keyNumStr[32];
    xmlSecDSigStatus status = xmlSecDSigStatusUnknown;
    xmlSecDSigReferenceCtxPtr dsigRefCtx = NULL;
    struct SHIBBOLETHSITE *ssMatchInner = NULL;
    xmlChar *id = NULL;
    xmlChar *refUri = NULL;

    if (samlCtx->responseIssuer == NULL) {
        Log(AT "issuer is NULL, abort");
        return -1;
    }
    UUxmlSecInit();

    /* find start node */
    node = xmlSecFindNode(baseNodePtr, xmlSecNodeSignature, xmlSecDSigNs);
    id = xmlGetProp(baseNodePtr, BAD_CAST "ID");

    while (node != NULL) {
        xmlFreeThenNull(refUri);
        xmlNodePtr ref = xmlSecFindNode(node, BAD_CAST "Reference", xmlSecDSigNs);
        if (ref) {
            refUri = xmlGetProp(ref, BAD_CAST "URI");
        }

        const xmlChar *cmpId = id != NULL ? id : BAD_CAST "";
        const xmlChar *cmpRefUri = refUri != NULL ? refUri : BAD_CAST "#";
        // Check if this signature points back to the node we are verifying
        if (*cmpRefUri == '#' && strcmp(cmpId, cmpRefUri + 1) == 0) {
            sigCount++;
            keyCount = 0;
            keyNum = 1;
            sprintf(keyNumStr, "%d", keyNum);

            /* Loop through all keys in the metadata to see if any will verify the signature. */
            key = SAMLShibbolethMetadataValue(
                KeyDescriptorPrefixFormatStr KeyInfoPrefixFormatStr X509CertificatePrefixFormatStr,
                (char *)(samlCtx->responseIssuer), keyNumStr, &ssMatchInner, samlCtx->holdMutex);
        } else {
            key = NULL;
        }

        while (key != NULL) {
            keyCount++;

            /* EXPROX-729 We want to quit if we find a single valid key */
            int verified = 0;

            do {
                keyDer = Decode64Binary(NULL, &keyLen, (char *)key);

                /* create signature context */
                dsigCtx = xmlSecDSigCtxCreate(NULL);
                if (dsigCtx == NULL)
                    PANIC;

                /* load public key */
                dsigCtx->signKey = xmlSecCryptoAppKeyLoadMemory(
                    keyDer, keyLen, xmlSecKeyDataFormatCertDer, NULL, NULL, NULL);
                if (dsigCtx->signKey == NULL) {
                    Log(AT "failed to load public pem key for " KeyDescriptorPrefixFormatStr,
                        (char *)(samlCtx->responseIssuer), keyNumStr);
                    break;
                }

                if (xmlSecKeySetName(dsigCtx->signKey, BAD_CAST samlCtx->responseIssuer) < 0) {
                    Log(AT "failed to set key name for " KeyDescriptorPrefixFormatStr,
                        (char *)(samlCtx->responseIssuer), keyNumStr);
                    break;
                }

                /* Additional restrictions required by SAML spec */

                /* limit allowed transforms for reference processing */
                dsigCtx->enabledReferenceUris =
                    xmlSecTransformUriTypeSameDocument | xmlSecTransformUriTypeEmpty;

                /* limit allowed transforms for signature */
                if ((xmlSecDSigCtxEnableSignatureTransform(dsigCtx, xmlSecTransformInclC14NId) <
                     0) ||
                    (xmlSecDSigCtxEnableSignatureTransform(dsigCtx, xmlSecTransformExclC14NId) <
                     0) ||
                    (xmlSecDSigCtxEnableSignatureTransform(dsigCtx, xmlSecTransformSha1Id) < 0) ||
                    (xmlSecDSigCtxEnableSignatureTransform(dsigCtx, xmlSecTransformRsaSha1Id) <
                     0) ||
                    (xmlSecDSigCtxEnableSignatureTransform(dsigCtx, xmlSecTransformRsaSha256Id) <
                     0)) {
                    Log(AT
                        "failed to limit allowed signature transforms "
                        "for " KeyDescriptorPrefixFormatStr,
                        (char *)(samlCtx->responseIssuer), keyNumStr);
                    break;
                }

                /* in addition, limit possible key data to valid X509 certificates only */
                if (xmlSecPtrListAdd(&(dsigCtx->keyInfoReadCtx.enabledKeyData),
                                     BAD_CAST xmlSecKeyDataX509Id) < 0) {
                    Log(AT "failed to limit allowed key data for " KeyDescriptorPrefixFormatStr,
                        (char *)(samlCtx->responseIssuer), keyNumStr);
                    break;
                }

                /* Verify signature */
                if (xmlSecDSigCtxVerify(dsigCtx, node) < 0) {
                    status = xmlSecDSigStatusUnknown;
                    if (debugLevel > SAMLDebugThreshold_Low) {
                        Log(AT
                            "call to xmlSecDSigCtxVerify failed for " KeyDescriptorPrefixFormatStr,
                            (char *)(samlCtx->responseIssuer), keyNumStr);
                    }
                } else {
                    status = dsigCtx->status;
                }

                /* check that the Signature Reference URI refers to exactly one element */
                refCount = xmlSecPtrListGetSize(&(dsigCtx->signedInfoReferences));
                if (refCount != 1) {
                    status = xmlSecDSigStatusUnknown;
                    Log(AT
                        "Signature reference count not exactly one "
                        "for " KeyDescriptorPrefixFormatStr,
                        (char *)(samlCtx->responseIssuer), keyNumStr);
                }

                if (status != xmlSecDSigStatusSucceeded) {
                    if (debugLevel > SAMLDebugThreshold_Medium) {
                        Log(AT "signature verify failed for " KeyDescriptorPrefixFormatStr,
                            (char *)(samlCtx->responseIssuer), keyNumStr);
                        Log(AT "at node:");
                        SAMLXMLNodeReport(baseNodePtr, 0);
                    }
                } else {
                    if (debugLevel > SAMLDebugThreshold_High) {
                        Log(AT "signature verify succeeded for " KeyDescriptorPrefixFormatStr,
                            (char *)(samlCtx->responseIssuer), keyNumStr);
                        Log(AT "at node:");
                        SAMLXMLNodeReport(baseNodePtr, 0);
                    }
                    sigVerfiedCount++;
                    verified = 1;
                    break;
                }
            } while (FALSE);

            /* try the next key if any. */
            if (dsigCtx != NULL) {
                xmlSecDSigCtxDestroy(dsigCtx);
                dsigCtx = NULL;
            }
            xmlFreeThenNull(keyDer);
            xmlFreeThenNull(key);

            if (verified)
                break;

            keyNum++;
            sprintf(keyNumStr, "%d", keyNum);
            key = SAMLShibbolethMetadataValue(
                KeyDescriptorPrefixFormatStr KeyInfoPrefixFormatStr X509CertificatePrefixFormatStr,
                (char *)(samlCtx->responseIssuer), keyNumStr, &ssMatchInner, samlCtx->holdMutex);
        }

        if (node->next == NULL) {
            break;
        } else {
            node = xmlSecFindNode(node->next, xmlSecNodeSignature, xmlSecDSigNs);
        }
    }

    if (dsigCtx != NULL) {
        xmlSecDSigCtxDestroy(dsigCtx);
    }
    xmlFreeThenNull(keyDer);
    xmlFreeThenNull(key);
    xmlFreeThenNull(id);
    xmlFreeThenNull(refUri);

    if ((sigCount == 1) && (sigVerfiedCount == 1)) {
        res = 0;
    } else if ((sigCount == 1) && (sigVerfiedCount == 0)) {
        res = 1;
    } else if (sigCount == 0) {
        res = 2;
    } else {
        res = -1;
    }

    samlCtx->counters.totalSignatureNodeCount += sigCount;
    samlCtx->counters.totalSignatureVerifiedNodeCount += sigVerfiedCount;

    return res;
}

void SAMLSendURLforDS(struct UUSOCKET *s, char *url, char *wayf, char *entityID, char *tag) {
    char *me = (myUrlHttps ? myUrlHttps : myUrlHttp);
    char *ur = NULL;
    char *p;

    UUSendZ(s, wayf);

    if (p = strrchr(wayf, '?')) {
        if (*(p + 1) != 0)
            UUSendZ(s, "&");
    } else
        UUSendZ(s, "?");

    UUSendZ(s, "entityID=");
    SendEscapedURL(s, entityID);
    UUSendZ(s, "&return=");
    SendEscapedURL(s, me);
    SendEscapedURL(s, "/Shibboleth.sso/DS?SAMLDS=1&target=");
    ur = URLReference(url, tag);
    SendEscapedURL(s, ur);
    FreeThenNull(ur);
}

/* returns FALSE if it can't fetch any metadata; TRUE if metadata fetched */
BOOL RetrieveShibbolethMetadata(struct SHIBBOLETHSITE *ss, BOOL holdMutex) {
    struct UUSOCKET rr, *r = &rr;
    BOOL needConnect = 0;
    BOOL throughProxy = 0;
    char line[4096];
    char *errorMessage = NULL;
    int rc = -1;
    int rc2 = -1;
    int result;
    char *arg;
    BOOL header;
    char *tempFilename = NULL;
    int f = -1;
    BOOL failed = 0;
    xmlDocPtr doc;
    BOOL retrieved = 0;
    char *psa = NULL;
    BOOL mutexAcquired = 0;
    time_t now;
    xmlValidCtxt vctx;

    UUInitSocket(r, INVALID_SOCKET);

    if (ss->url == NULL)
        goto Finished;

    UUtime(&now); /* We want ss->urlRetrieved == ss->urlAttempted if the retrieval attempt
                     succeeded. */

    ss->urlErrno = 0;
    ss->urlUUErrno = 0;
    ss->fileErrno = 0;
    ss->urlHTTPCode = 999;
    ss->docFetchValid = METADATADOC_FETCHING;
    ss->urlAttempted = now;

    if (ss->proxyPort != 0 && ss->urlSsl == 0) {
        rc = UUConnectWithSource2(r, ss->proxyHost, ss->proxyPort, "RetrieveShibbolethMetadata-p",
                                  &ss->ipInterface, 0);
        psa = ss->proxyAuth;
        throughProxy = 1;
    } else if (ss->proxySslPort != 0 && ss->urlSsl) {
        /* Connecting through a proxy for https still starts with normal socket */
        rc = UUConnectWithSource2(r, ss->proxySslHost, ss->proxySslPort,
                                  "RetrieveShibbolethMetadata-ps", &ss->ipInterface, 0);
        needConnect = 1;
    } else {
        rc = UUConnectWithSource2(r, ss->urlHost, ss->urlPort, "RetrieveShibbolethMetadata",
                                  &ss->ipInterface, ss->urlSsl);
    }
    ss->urlUUErrno = rc;

    if (rc != 0) {
        ss->urlErrno = errno;
        if (debugLevel > SAMLDebugThreshold_Low) {
            Log(AT "metadata URL '%s' unavailable", ss->url);
        }
        goto Finished;
    }

    size_t tempFilenameSize = strlen(ss->filename) + 10;
    tempFilename = UUmalloc(tempFilenameSize);
    snprintf(tempFilename, tempFilenameSize, "%s.new", ss->filename);
    f = UUopenCreateFD(tempFilename, O_CREAT | O_WRONLY | O_TRUNC, defaultFileMode);
    if (f < 0) {
        ss->fileErrno = errno;
        Log(AT "unable to create '%s' err %s", tempFilename, errorMessage = ErrorMessage(errno));
        FreeThenNull(errorMessage);
        goto Finished;
    }

    if (debugLevel > SAMLDebugThreshold_Medium) {
        Log(AT "logging XML to file '%s'.", tempFilename);
    }

    /* r->timeout = remoteTimeout; */

    if (needConnect) {
        UUSendZ(r, "CONNECT ");
        UUSendZ(r, ss->urlHost);
        WriteLine(r, ":%d HTTP/1.0\r\nHost: ", ss->urlPort);
        UUSendZ(r, ss->urlHost);
        WriteLine(r, ":%d\r\n", ss->urlPort);
        if (ss->proxySslAuth) {
            UUSendZ(r, "Proxy-Authorization: basic ");
            UUSendZCRLF(r, ss->proxySslAuth);
        }
        UUSendCRLF(r);
        result = 0;
        line[sizeof(line) - 1] = 1;
        while (ReadLine(r, line, sizeof(line))) {
            if (line[sizeof(line) - 1] != 1) {
                ss->docFetchValid = METADATADOC_INVALID_XML;
                Log(AT "ProxySSL connection sent a line longer than %d, abort", sizeof(line) - 1);
                goto Finished;
            }
            if (line[0] == 0)
                break;
            if (strnicmp(line, "HTTP/", 5) == 0) {
                arg = SkipNonWS(SkipWS(line));
                result = atoi(arg);
                ss->urlHTTPCode = result;
                if (result < 200 || result > 299) {
                    Log(AT "ProxySSL connection refused: %s", line);
                    goto Finished;
                }
            }
            line[sizeof(line) - 1] = 1;
        }
        UUInitSocketSsl(r, r->o, UUSSLCONNECT, 0, ss->urlHost);
        if (r->ssl == NULL) {
            Log(AT "unable to negotiate secure connection through proxy");
            goto Finished;
        }
    }

    UUSendZ(r, "GET ");
    if (throughProxy) {
        WriteLine(r, "http%s://", (ss->urlSsl ? "s" : ""));
        UUSendZ(r, ss->urlHost);
        if (ss->urlDefaultPort == 0) {
            WriteLine(r, ":%d", ss->urlPort);
        }
    }
    UUSendZ(r, ss->urlRequest);
    UUSendZ(r, " HTTP/1.0\r\nHost: ");
    UUSendZ(r, ss->urlHost);
    if (ss->urlDefaultPort == 0)
        WriteLine(r, ":%d", ss->urlPort);
    UUSendCRLF(r);
    if (psa && *psa) {
        UUSendZ(r, "Proxy-Authorization: basic ");
        UUSendZCRLF(r, psa);
    }
    UUSendCRLF(r);

    header = 1;
    result = 0;
    while (header && (ReadLine(r, line, sizeof(line)))) {
        if (line[0] == 0) {
            header = 0;
        } else if (result == 0) {
            if (strnicmp(line, "HTTP/", 5) == 0) {
                arg = SkipNonWS(SkipWS(line));
                result = atoi(arg);
                ss->urlHTTPCode = result;
            }
        }
    }
    if (result < 200 || result > 299) {
        Log(AT "HTTP GET of metadata for '%s' returned code %d", ss->url, result);
        goto Finished;
    }

    rc = rc2 = 0;
    while ((!failed) && (!header) && (!UUSocketEof(r))) {
        rc = UURecv(r, line, sizeof(line), 0);
        if (rc >= 0) {
            rc2 = write(f, line, rc); /* it's a regular file, so 0 is ok. */
            if (rc2 == rc) {
                /* all is well */
            } else {
                failed = 1;
                ss->fileErrno = GetLastError();
                Log(AT "while writing, rc = %d, err = %s", rc2,
                    errorMessage = ErrorMessage(ss->fileErrno));
                FreeThenNull(errorMessage);
            }
        } else if (!UUSocketEof(r)) {
            failed = 1;
            ss->urlErrno = GetLastError();
#ifdef WIN32
            Log(AT "while reading,  rc = %d, err = %s", rc,
                errorMessage = ErrorMessage(ss->urlErrno));
#else
            Log(AT "while reading,  rc = %d, sizeof ssize_t = %" PRIdMAX ", err = %s", rc,
                (intmax_t)sizeof(ssize_t), errorMessage = ErrorMessage(ss->urlErrno));
#endif
            FreeThenNull(errorMessage);
        }
    }

    UUStopSocket(r, 0);
    r = NULL;

    if (f >= 0) {
        close(f);
        f = -1;
    }

    if (failed) {
        goto Finished;
    }

    doc = xmlParseFile(tempFilename);

    if (doc == NULL) {
        ss->docFetchValid = METADATADOC_INVALID_XML;
        failed = 1;
        Log(AT "unable to parse metadata from '%s'", tempFilename);
        if (optionLogSAML || (debugLevel > SAMLDebugThreshold_High)) {
            Log(AT "retrieved metadata as fetched is in '%s'", tempFilename);
            FreeThenNull(tempFilename); /* so it won't be unlinked. */
        }
        goto Finished;
    }

    SAMLAddIDAttr(xmlDocGetRootElement(doc), BAD_CAST "ID", BAD_CAST "EntitiesDescriptor", NSMD);
    SAMLAddIDAttr(xmlDocGetRootElement(doc), BAD_CAST "entityID", BAD_CAST "EntityDescriptor",
                  NSMD);

    if (debugLevel > SAMLDebugThreshold_Medium) {
        memset(&vctx, 0, sizeof(vctx));
        vctx.userData = (void *)stderr;
        vctx.error = (xmlValidityErrorFunc)fprintf;
        vctx.warning = (xmlValidityWarningFunc)fprintf;
        rc = xmlValidateDocument(&vctx, doc);
        if (rc != 1) {
            Log(AT "DTD validation of metadata '%s' failed, warning", ss->filename);
        } else {
            Log(AT "DTD validation of metadata '%s' succeeded", ss->filename);
        }
    }

    if (!VerifySignatureShibbolethMetadata(doc, ss)) {
        ss->docFetchValid = METADATADOC_INVALID_SIGNATURE;
        failed = 1;
        Log(AT "metadata for '%s' has a signature related error, discarded", ss->filename);
        goto Finished;
    }
    ss->docFetchValid = METADATADOC_VALID;

    xmlFreeDoc(doc);
    doc = NULL;

    if (!holdMutex) {
        UUAcquireMutex(&ss->mutex);
        mutexAcquired = 1;
    }

    unlink(ss->filename);
    if (errno < 0 && errno != ENOENT) {
        ss->fileErrno = errno;
        failed = 1;
        Log(AT "Unable to remove '%s' errno %d", ss->filename);
        goto Finished;
    }

    if (rename(tempFilename, ss->filename) < 0) {
        ss->fileErrno = errno;
        failed = 1;
        Log(AT "Failed to rename '%s' to '%s' err %s", tempFilename, ss->filename,
            errorMessage = ErrorMessage(errno));
        FreeThenNull(errorMessage);
        goto Finished;
    }

    FreeThenNull(tempFilename);

    if (debugLevel > SAMLDebugThreshold_Low) {
        Log(AT "retrieved metadata into file '%s'", ss->filename);
    }

    retrieved = 1;
    ss->urlRetrieved = now;

Finished:
    if ((!holdMutex) && (!mutexAcquired)) {
        UUAcquireMutex(&ss->mutex);
        mutexAcquired = 1;
    }
    if (ss->docFetchValid == METADATADOC_FETCHING) {
        ss->docFetchValid = METADATADOC_INVALID_FETCH;
    }
    if (ss->docFetchValid == METADATADOC_VALID) {
        LoadUpdateShibbolethMetadata(ss, TRUE);
    }
    if (mutexAcquired) {
        UUReleaseMutex(&ss->mutex);
        mutexAcquired = 0;
    }

    if (f >= 0) {
        close(f);
        f = -1;
    }

    if (tempFilename && (debugLevel <= SAMLDebugThreshold_High)) {
        unlink(tempFilename);
        FreeThenNull(tempFilename);
    }

    if (r != NULL) {
        UUStopSocket(r, 0);
        r = NULL;
    }

    return retrieved;
}

/* returns FALSE if it can't get any metadata; TRUE if metadata is available */
BOOL LoadUpdateShibbolethMetadata(struct SHIBBOLETHSITE *ss, BOOL holdMutex) {
    xmlDocPtr doc = NULL;
    xmlXPathContextPtr xPathCtx = NULL;
    int rc;
    struct stat statBuf;
    xmlValidCtxt vctx;
    BOOL mutexAcquired = FALSE;
    BOOL result = FALSE;

    if (ss == NULL || ss->filename == NULL || ss->filename[0] == 0) {
        goto Finished;
    }

    if (!holdMutex) {
        UUAcquireMutex(&ss->mutex);
        mutexAcquired = TRUE;
    }

    rc = stat(ss->filename, &statBuf);
    if (rc < 0) {
        if (debugLevel > SAMLDebugThreshold_Low) {
            Log(AT "metadata file '%s' unavailable", ss->filename);
        }
        goto Finished;
    }

    if (ss->fileMTimeAsLoaded == statBuf.st_mtime) {
        /* The file hasn't changed since the metadata was last loaded from it.  Use the current
         * metadata. */
        result = TRUE;
        goto Finished;
    }

    doc = xmlParseFile(ss->filename);

    if (doc == NULL) {
        Log(AT "unable to parse metadata from '%s'", ss->filename);
        goto Finished;
    }

    SAMLAddIDAttr(xmlDocGetRootElement(doc), BAD_CAST "ID", BAD_CAST "EntitiesDescriptor", NSMD);
    SAMLAddIDAttr(xmlDocGetRootElement(doc), BAD_CAST "entityID", BAD_CAST "EntityDescriptor",
                  NSMD);

    if (optionLogSAML || (debugLevel > SAMLDebugThreshold_High)) {
        Log(AT "metadata as parsed is logged to a file");
        LogXMLDoc(doc, NULL);
    }

    if (debugLevel > SAMLDebugThreshold_High) {
        memset(&vctx, 0, sizeof(vctx));
        vctx.userData = (void *)stderr;
        vctx.error = (xmlValidityErrorFunc)fprintf;
        vctx.warning = (xmlValidityWarningFunc)fprintf;
        rc = xmlValidateDocument(&vctx, doc);
        if (rc != 1) {
            Log(AT "DTD validation of metadata '%s' failed, warning", ss->filename);
        } else {
            Log(AT "DTD validation of metadata '%s' succeeded", ss->filename);
        }
        if (!VerifySignatureShibbolethMetadata(doc, ss)) {
            Log(AT "metadata for '%s' has a signature related error, warning", ss->filename);
        }
    }

    xPathCtx = xmlXPathNewContext(doc);
    if (xPathCtx == NULL) {
        Log(AT "unable to create new XPath context for metadata '%s'", ss->filename);
        goto Finished;
    }

    xmlXPathRegisterNs(xPathCtx, BAD_CAST "default", NSSHIBMD1);
    xmlXPathRegisterNs(xPathCtx, BAD_CAST "md", NSMD);
    xmlXPathRegisterNs(xPathCtx, BAD_CAST "ds", NSDS);
    xmlXPathRegisterNs(xPathCtx, BAD_CAST "shibmd", NSSHIBMD);

    if (ss->xPathCtx) {
        xmlXPathFreeContext(ss->xPathCtx);
        ss->xPathCtx = NULL;
    }

    if (ss->doc) {
        xmlFreeDoc(ss->doc);
        ss->doc = NULL;
    }

    ss->doc = doc;
    doc = NULL;
    ss->xPathCtx = xPathCtx;
    xPathCtx = NULL;
    ss->fileMTimeAsLoaded = statBuf.st_mtime;

    if (debugLevel > SAMLDebugThreshold_Low) {
        Log(AT "loaded metadata from file '%s'", ss->filename);
    }

    result = TRUE;

Finished:
    if (xPathCtx) {
        xmlXPathFreeContext(xPathCtx);
        xPathCtx = NULL;
    }

    if (doc) {
        xmlFreeDoc(doc);
        doc = NULL;
    }

    if (mutexAcquired) {
        UUReleaseMutex(&ss->mutex);
    }

    return result;
}

void RetrieveLoadUpdateShibbolethMetadatas(void) {
    time_t now;
    struct SHIBBOLETHSITE *ss;
    int rc;
    struct stat statBuf;
    BOOL x;

    if (optionShibboleth == 0)
        return;

    UUtime(&now);

    for (ss = shibbolethSites; ss; ss = ss->next) {
        rc = stat(ss->filename, &statBuf);
        // If we have a URL for metadata and the file either does not exists
        // or is more than SHIBBOLETHSITEREFRESH old, try to get an update.
        if (ss->url != NULL && (rc < 0 || statBuf.st_mtime + SHIBBOLETHSITEREFRESH < now)) {
            RetrieveShibbolethMetadata(ss, 0);
            rc = stat(ss->filename, &statBuf);
        }
        if (rc < 0) {
            if (debugLevel > SAMLDebugThreshold_Low) {
                Log(AT "metadata file '%s' unavailable", ss->filename);
            }
            continue;
        }
        x = LoadUpdateShibbolethMetadata(ss, 0);
        if (debugLevel > SAMLDebugThreshold_Low) {
            Log(AT "metadata from '%s' is%s available.", ss->filename, x ? "" : " not");
        }
    }
}

xmlXPathContextPtr SAMLxmlXPathNewContext(xmlDocPtr doc, enum SHIBBOLETHVERSION shibVersion) {
    xmlXPathContextPtr xPathCtx = NULL;

    if (doc == NULL)
        return NULL;

    if (xPathCtx = xmlXPathNewContext(doc)) {
        xmlXPathRegisterNs(xPathCtx, BAD_CAST "soap", NSSOAP);
        xmlXPathRegisterNs(xPathCtx, BAD_CAST "md", NSMD);
        xmlXPathRegisterNs(xPathCtx, BAD_CAST "ds", NSDS);
        xmlXPathRegisterNs(xPathCtx, BAD_CAST "shibmd", NSSHIBMD);
        xmlXPathRegisterNs(xPathCtx, BAD_CAST "xenc", NSXENC);

        switch (shibVersion) {
        case SHIBVERSION13:
            xmlXPathRegisterNs(xPathCtx, BAD_CAST "samlp", NSSAMLP1);
            xmlXPathRegisterNs(xPathCtx, BAD_CAST "saml", NSSAML1);
            break;

        case SHIBVERSION20:
            xmlXPathRegisterNs(xPathCtx, BAD_CAST "samlp", NSSAMLP2);
            xmlXPathRegisterNs(xPathCtx, BAD_CAST "saml", NSSAML2);
            break;

        default:
            PANIC;
            break;
        }
    } else {
        Log(AT "unable to create new SAML2 XPath context");
    }

    return xPathCtx;
}

static BOOL SAMLAggregateTest(struct TRANSLATE *t,
                              struct USERINFO *uip,
                              void *context,
                              const char *var,
                              const char *testVal,
                              enum AGGREGATETESTOPTIONS ato,
                              struct VARIABLES *rev) {
    struct AGGREGATETESTCONTEXT atc;
    struct SAMLCONTEXT *sc = (struct SAMLCONTEXT *)context;
    struct SAMLATTRIBUTE *saList = (struct SAMLATTRIBUTE *)sc->saList, *p;
    char *scope = NULL;
    size_t varLen;

    if (!ExpressionAggregateTestInitialize(&atc, uip, testVal, ato))
        return FALSE;

    if (stricmp(var, "issuer") == 0) {
        if (sc->responseIssuer) {
            ExpressionAggregateTestValue(&atc, (char *)sc->responseIssuer);
        }
    } else if (stricmp(var, "nameid") == 0) {
        if (sc->nameID) {
            ExpressionAggregateTestValue(&atc, (char *)sc->nameID);
        }
    } else if (stricmp(var, "subjectconfirmationmethod") == 0) {
        if (sc->SubjectConfirmationMethod) {
            ExpressionAggregateTestValue(&atc, (char *)sc->SubjectConfirmationMethod);
        }
    } else if (stricmp(var, "shibversion") == 0) {
        ExpressionAggregateTestValue(&atc, sc->shibbolethVersion == SHIBVERSION20 ? "2.0" : "1.3");
    } else {
        if (scope = strchr(var, '@')) {
            varLen = scope - var;
            scope++;
        } else
            varLen = strlen(var);

        for (p = saList; p; p = p->next) {
            if (p->name == NULL || strncmp(var, p->name, varLen) != 0 || *(p->name + varLen) != 0)
                if (p->friendlyName == NULL || strncmp(var, p->friendlyName, varLen) != 0 ||
                    *(p->friendlyName + varLen) != 0)
                    continue;

            if (p->scope == NULL) {
                if (scope != NULL)
                    continue;
            } else {
                if (scope == NULL)
                    continue;
                if (strcmp(p->scope, scope) != 0)
                    continue;
            }

            if (!p->rejected) {
                if (!(ExpressionAggregateTestValue(&atc, p->value)))
                    break;
            }
        }
    }

    return ExpressionAggregateTestTerminate(&atc, rev);
}

static char *SAMLAuthGetValue(struct TRANSLATE *t,
                              struct USERINFO *uip,
                              void *context,
                              const char *var,
                              const char *index) {
    struct SAMLCONTEXT *sc = (struct SAMLCONTEXT *)context;
    struct SAMLATTRIBUTE *saList = (struct SAMLATTRIBUTE *)sc->saList, *p;
    char *val = NULL;
    char *scope = NULL;
    size_t varLen;
    int indexVal = index ? atoi(index) : 0;
    int indexCurrent = 0;

    if (stricmp(var, "issuer") == 0) {
        if (sc->responseIssuer) {
            if (!(val = strdup((char *)sc->responseIssuer)))
                PANIC;
        }
        return val;
    } else if (stricmp(var, "nameid") == 0) {
        if (sc->nameID) {
            if (!(val = strdup((char *)sc->nameID)))
                PANIC;
        }
        return val;
    } else if (stricmp(var, "subjectconfirmationmethod") == 0) {
        if (sc->SubjectConfirmationMethod) {
            if (!(val = strdup((char *)sc->SubjectConfirmationMethod)))
                PANIC;
        }
        return val;
    } else if (stricmp(var, "shibversion") == 0) {
        if (!(val = strdup(sc->shibbolethVersion == SHIBVERSION20 ? "2.0" : "1.3")))
            PANIC;
        return val;
    }

    if (scope = strchr(var, '@')) {
        varLen = scope - var;
        scope++;
    } else
        varLen = strlen(var);

    for (p = saList; p; p = p->next) {
        if (p->name == NULL || strncmp(var, p->name, varLen) != 0 || *(p->name + varLen) != 0)
            if (p->friendlyName == NULL || strncmp(var, p->friendlyName, varLen) != 0 ||
                *(p->friendlyName + varLen) != 0)
                continue;

        if (p->scope == NULL) {
            if (scope != NULL)
                continue;
        } else {
            if (scope == NULL)
                continue;
            if (strcmp(p->scope, scope) != 0)
                continue;
        }

        if (!p->rejected) {
            if (indexCurrent++ == indexVal) {
                if (p->value) {
                    if (!(val = strdup(p->value)))
                        PANIC;
                }
                break;
            }
        }
    }

    return val;
}

struct SHIBBOLETHSITE *SAMLShibbolethMetadataFindSite(const char *format, ...) {
    BOOL found = FALSE;
    struct SHIBBOLETHSITE *ss = NULL;
    struct SHIBBOLETHSITE *ssFound = NULL;
    xmlXPathObjectPtr xpathObj = NULL;
    va_list ap;
    char buffer[2048];

    _SNPRINTF_BEGIN(buffer, sizeof(buffer));
    if (format) {
        va_start(ap, format);
        _vsnprintf(buffer, sizeof(buffer), format, ap);
        va_end(ap);
    }
    _SNPRINTF_PANIC_OVERFLOW(buffer, sizeof(buffer));
    _SNPRINTF_END(buffer, sizeof(buffer));

    for (ss = shibbolethSites; ss && (!found); ss = ss->next) {
        /* Shibboleth 1.2 nodes lack an entityID; skip those for here */
        if (ss->entityID == NULL)
            continue;

        if (LoadUpdateShibbolethMetadata(ss, FALSE) == 0)
            continue;

        UUAcquireMutex(&ss->mutex);

        xpathObj = xmlXPathEvalExpression(BAD_CAST buffer, ss->xPathCtx);
        if (!(xpathObj == NULL || (xpathObj->nodesetval == NULL) ||
              (xpathObj->nodesetval->nodeNr == 0))) {
            found = TRUE;
            ssFound = ss;
        }
        if (xpathObj) {
            xmlXPathFreeObject(xpathObj);
            xpathObj = NULL;
        }

        UUReleaseMutex(&ss->mutex);

        if (debugLevel > SAMLDebugThreshold_Low) {
            Log(AT "ShibbolethMetadataFindSite: %d, \"%s\"", found, buffer);
        }
    }

    return ssFound;
}

xmlChar *SAMLShibbolethMetadataValue(char *xPathMain,
                                     char *subValue,
                                     char *subValue2,
                                     struct SHIBBOLETHSITE **ssMatch,
                                     BOOL holdMutex) {
    struct SHIBBOLETHSITE *ss;
    xmlChar *value = NULL;
    char *xPath = xPathMain;

    if (subValue || subValue2) {
        if (strchr(subValue, '\'') != NULL) {
            Log(AT "subvalue with ', ignoring");
            /*
                        if (ssMatch) {
                            *ssMatch = NULL;
                        }
            */
            return NULL;
        }
        if (!(xPath = malloc(strlen(xPathMain) + strlen(subValue) + strlen(subValue2) + 1)))
            PANIC;
        sprintf(xPath, xPathMain, PNS(subValue), PNS(subValue2));
    }

    for (ss = shibbolethSites; ss; ss = ss->next) {
        /* Shibboleth 1.2 nodes lack an entityID; skip those for here */
        if (ss->entityID == NULL)
            continue;

        if (LoadUpdateShibbolethMetadata(ss, holdMutex) == 0)
            continue;

        if ((!holdMutex)) {
            UUAcquireMutex(&ss->mutex);
        }

        value = UUxmlSimpleGetContent(NULL, ss->xPathCtx, xPath);

        if ((!holdMutex)) {
            UUReleaseMutex(&ss->mutex);
        }

        if (value)
            break;
    }

    if (ssMatch) {
        *ssMatch = value ? ss : NULL;
    }

    if (xPath != xPathMain)
        FreeThenNull(xPath);

    return value;
}

char *SAMLDecode(char *val, BOOL inflateContent) {
    size_t len;
    unsigned char *decode64 = NULL;
    char buffer[65536];
    int max = sizeof(buffer) - 1;  // reserve room for terminating nullu
    z_stream strm;
    int zResult;
    char *ret = NULL;

    decode64 = Decode64Binary(NULL, &len, val);

    if (decode64) {
        if (!inflateContent) {
            // Transfer decode64 to ret and add room to include a null terminator
            if (!(ret = realloc(decode64, len + 1)))
                PANIC;
            *(ret + len) = 0;
            decode64 = NULL;
        } else {
            strm.zalloc = Z_NULL;
            strm.zfree = Z_NULL;
            strm.opaque = Z_NULL;
            strm.avail_in = 0;
            strm.next_in = Z_NULL;

            zResult = inflateInit2(&strm, -MAX_WBITS);
            if (zResult != Z_OK)
                PANIC;

            strm.next_in = decode64;
            strm.avail_in = len;
            strm.next_out = (Bytef *)buffer;
            strm.avail_out = max;
            zResult = inflate(&strm, Z_FINISH);
            inflateEnd(&strm);
            if (zResult == Z_STREAM_END) {
                len = max - strm.avail_out;
                // We pre-reserved space for the null terminator when max was declared
                buffer[len] = 0;
                if (!(ret = strdup(buffer)))
                    PANIC;
            }
        }
    }

    FreeThenNull(decode64);
    return ret;
}

char *SAMLEncode(xmlChar *val) {
    int len;
    char buffer[32768];
    int max = sizeof(buffer) - 1;
    z_stream strm;
    int zResult;

    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;

    zResult =
        deflateInit2(&strm, Z_DEFAULT_COMPRESSION, Z_DEFLATED, -MAX_WBITS, 5, Z_DEFAULT_STRATEGY);
    if (zResult != Z_OK)
        PANIC;
    strm.next_in = (Bytef *)val;
    strm.avail_in = strlen((char *)val);
    strm.next_out = (Bytef *)buffer;
    strm.avail_out = max;
    zResult = deflate(&strm, Z_FINISH);
    deflateEnd(&strm);

    len = max - strm.avail_out;

    return Encode64Binary(NULL, (unsigned char *)buffer, len);
}

BOOL SAMLGetMetadataScopeNodes(struct SAMLCONTEXT *samlCtx) {
    BOOL rejectedNode = FALSE;

    if (samlCtx->responseIssuer && *(samlCtx->responseIssuer)) {
        if (samlCtx->shibbolethSite) {
            char *xPathScope = NULL;

            UUAcquireMutex(&(samlCtx->shibbolethSite->mutex));
            samlCtx->holdMutex = TRUE;

            if (!(xPathScope = malloc(256 + strlen(samlCtx->responseIssuer))))
                PANIC;
            sprintf(xPathScope,
                    "//md:EntityDescriptor[@entityID = "
                    "'%s']//md:IDPSSODescriptor/md:Extensions/shibmd:Scope",
                    samlCtx->responseIssuer);

            samlCtx->xPathObjScope =
                xmlXPathEvalExpression(BAD_CAST xPathScope, samlCtx->shibbolethSite->xPathCtx);
            if (!samlCtx->xPathObjScope) {
                Log(AT "unable to evaluate XPath expression '%s'.", xPathScope);
                rejectedNode = TRUE;
            }
            FreeThenNull(xPathScope);
        } else {
            rejectedNode = TRUE; /* todo: can this ever really happen? */
        }
    } else {
        if (debugLevel > SAMLDebugThreshold_Low) {
            Log(AT "no issuer given.");
        }
    }

    return rejectedNode;
}

void SAMLReleaseMetadataScopeNodes(struct SAMLCONTEXT *samlCtx) {
    if (samlCtx->xPathObjScope != NULL) {
        xmlXPathFreeObject(samlCtx->xPathObjScope);
        samlCtx->xPathObjScope = NULL;
    }

    if (samlCtx->holdMutex) {
        UUReleaseMutex(&(samlCtx->shibbolethSite->mutex));
        samlCtx->holdMutex = FALSE;
    }
}

/* Include passing samlCtx to this routine to allow debugging
   messages to report the issuer of the assertion being validated, especially
   if the assertion is invalid to diagnose if invalid instant time is a
   component of this issue.  If this routine needs to be used in the
   absence of a samlCtx, pass NULL and the routine will report the static
   string unknown.
*/
BOOL SAMLCheckInstantValue(xmlChar *ii, int maxDiff, struct SAMLCONTEXT *samlCtx) {
    char zulu[2];
    float f;
    struct tm tm, rtm;
    struct tm rtmNow;
    time_t newDtLocal, newDtGm, now;
    int diff;
    char nowIso8601[32];
    BOOL result = FALSE;

    zulu[0] = 0;
    zulu[1] = 0;

    if (ii == NULL) {
        Log(AT "Unable to locate SAML Instant");
        goto Finished;
    }

    if (debugLevel > SAMLDebugThreshold_High) {
        Log(AT "Instant '%s'", ii);
    }

    memset(&tm, 0, sizeof(tm));
    if (sscanf((char *)ii, "%d-%d-%dT%d:%d:%f%1s", &tm.tm_year, &tm.tm_mon, &tm.tm_mday,
               &tm.tm_hour, &tm.tm_min, &f, &(zulu[0])) != 7) {
        Log(AT "Unable to parse Instant");
        goto Finished;
    }
    /* amazingly, win32 and C89 have no native rounding function, so we have to define it ourself.
    The values here really are pegged positive, so the sign check and use of floor or ceil is
    overkill, but who knows when this might be lifted and placed somewhere else where that would
    matter
    */
    tm.tm_sec = (int)(f >= 0 ? floor(f + 0.5) : ceil(f - 0.5));
    if (zulu[0] != 'Z') {
        Log(AT "Unable to parse Instant (not UTC)");
        goto Finished;
    }

    tm.tm_year -= 1900;
    tm.tm_mon--;

    newDtLocal = mktime(&tm);
    if (newDtLocal == (time_t)-1 || UUgmtime_r(&newDtLocal, &rtm) == NULL ||
        (newDtGm = mktime(&rtm)) == (time_t)-1) {
        Log(AT "Invalid Instant");
        goto Finished;
    }

    /* Now convert to GMT by computing time zone difference and applying */
    diff = newDtGm - newDtLocal;

    newDtGm = newDtLocal - diff;
    UUtime(&now);

    diff = (now < newDtGm ? newDtGm - now : now - newDtGm);

    if ((maxDiff == 0) || (diff < maxDiff)) {
        result = TRUE;
    }

    if ((!result) || (debugLevel > SAMLDebugThreshold_Low)) {
        if (strlen((char *)ii) > 32) {
            /* Shorten a string that is obviously too long to avoid logging issue for overtly bad II
             */
            ii[32] = 0;
        }
        UUgmtime_r(&now, &rtmNow);
        strftime(nowIso8601, sizeof(nowIso8601), "%Y-%m-%dT%H:%M:%SZ", &rtmNow);
        if (!result) {
            Log(AT "Instant time disagreement, check system clock if recurrent.");
        }
        Log(AT "Instant %s, issuer %s, now %s, diff %d, max diff %d (zero means no maximum).", ii,
            (samlCtx != NULL && samlCtx->responseIssuer != NULL ? samlCtx->responseIssuer
                                                                : "unknown"),
            nowIso8601, diff, maxDiff);
    }

Finished:
    return result;
}

BOOL SAMLCheckIssueInstant(xmlXPathContextPtr xPathCtx, struct SAMLCONTEXT *samlCtx) {
    xmlChar *ii;
    BOOL result = FALSE;

    ii = UUxmlSimpleGetAttribute(NULL, xPathCtx, "//saml:Assertion", "IssueInstant");
    if (ii == NULL) {
        ii = UUxmlSimpleGetAttribute(NULL, xPathCtx, "//samlp:Response", "IssueInstant");
    }

    result = SAMLCheckInstantValue(ii, 300, samlCtx);

    xmlFreeThenNull(ii);

    return result;
}

void SAMLFreeAttribute(struct SAMLATTRIBUTE **sa) {
    FreeThenNull((*sa)->name);
    FreeThenNull((*sa)->friendlyName);
    FreeThenNull((*sa)->value);
    FreeThenNull((*sa)->scope);
    FreeThenNull((*sa)->path);
    FreeThenNull(*sa);
}

void SAMLFreeAttributes(struct SAMLATTRIBUTE **saList) {
    struct SAMLATTRIBUTE *sa, *saNext;

    if (*saList) {
        for (sa = *saList; sa; sa = saNext) {
            saNext = sa->next;
            SAMLFreeAttribute(&(sa));
        }
        *saList = NULL;
    }
}

void SAMLResetDocumentCtx(struct SAMLCONTEXT *samlCtx) {
    SAMLReleaseMetadataScopeNodes(samlCtx);
    if (samlCtx->xPathCtx) {
        xmlXPathFreeContext(samlCtx->xPathCtx);
        samlCtx->xPathCtx = NULL;
    }
    if (samlCtx->responseDoc) {
        xmlFreeDoc(samlCtx->responseDoc);
        samlCtx->responseDoc = NULL;
    }
    SAMLFreeAttributes(&(samlCtx->saList));
    xmlFreeThenNull(samlCtx->responseIssuer);
    xmlFreeThenNull(samlCtx->peerCommonName);
    xmlFreeThenNull(samlCtx->statusCode);
    xmlFreeThenNull(samlCtx->attributeFriendlyName);
    xmlFreeThenNull(samlCtx->attributeName);
    xmlFreeThenNull(samlCtx->nameID);
    xmlFreeThenNull(samlCtx->nameIDFormat);
    xmlFreeThenNull(samlCtx->nameIDNameQualifier);
    xmlFreeThenNull(samlCtx->nameIDSPNameQualifier);
    xmlFreeThenNull(samlCtx->sessionIndex);
    xmlFreeThenNull(samlCtx->SubjectConfirmationMethod);
    memset(&(samlCtx->counters), 0, sizeof(samlCtx->counters));
}

BOOL SAMLScopeValid(xmlChar **dv, xmlChar **rv, char *scope) {
    BOOL valid = 0;

    if (*dv == NULL)
        goto Finished;

    if (*rv == NULL || strcmp((char *)*rv, "false") == 0) {
        if (strcmp(scope, (char *)*dv) == 0)
            valid = 1;
    } else if (strcmp((char *)*rv, "true") == 0) {
        xmlRegexpPtr xrp = xmlRegexpCompile(*dv);
        if (xrp == NULL)
            Log(AT "scope check unable to parse regular expression: %s", *dv);
        else {
            if (xmlRegexpExec(xrp, *dv) == 1)
                valid = 1;
            xmlRegFreeRegexp(xrp);
            xrp = NULL;
        }
    }

Finished:
    xmlFreeThenNull(*rv);
    xmlFreeThenNull(*dv);

    return valid;
}

void SAMLExtractValue(xmlChar *val, char *sep, char **valuePtr) {
    int newLen;
    char *s = NULL;
    char *p = NULL;

    if (*valuePtr == NULL) {
        if (!(*valuePtr = strdup((char *)val)))
            PANIC;
    } else {
        newLen = strlen(*valuePtr) + strlen((char *)sep) + strlen((char *)val) + 1;
        if (newLen > sizeof(*valuePtr)) {
            if (!(s = malloc(newLen + 128)))
                PANIC;
            strcpy(s, *valuePtr);
            FreeThenNull(*valuePtr);
        } else {
            s = *valuePtr;
        }
        p = strchr(s, 0);
        if (s != p) {
            StrCpyOverlap(p, sep);
            p = strchr(s, 0);
        }
        StrCpyOverlap(p, (char *)val);
        *valuePtr = s;
    }
}

typedef BOOL (*SAMLExtractCtxFuncPtr)(struct SAMLCONTEXT *samlCtx,
                                      BOOL signedParent,
                                      BOOL encryptedParent,
                                      BOOL rejectedParent,
                                      xmlNodePtr nodePtr);

BOOL SAMLExtractCtx(struct SAMLCONTEXT *samlCtx,
                    BOOL signedParent,
                    BOOL encryptedParent,
                    BOOL rejectedParent,
                    xmlNodePtr parent,
                    char *childXPathStr,
                    char *childEncryptedXPathStr,
                    SAMLExtractCtxFuncPtr func) {
    BOOL rejectedNode = FALSE;
    BOOL encryptedNode = FALSE;
    xmlXPathObjectPtr xPathObj = NULL;
    xmlNodePtr savedNode = NULL;
    int i;
    int j = 0;
    int res;

    savedNode = samlCtx->xPathCtx->node;
    if (parent != NULL) {
        samlCtx->xPathCtx->node = parent;
    } else {
        /* A null parent means that the parent is the current xPathCtx. */
        parent = samlCtx->xPathCtx->node;
    }

    xPathObj = xmlXPathEvalExpression(BAD_CAST childXPathStr, samlCtx->xPathCtx);
    if ((xPathObj != NULL) && (xPathObj->nodesetval != NULL) &&
        (0 < xPathObj->nodesetval->nodeNr)) {
        for (i = 0; xPathObj->nodesetval && (i < xPathObj->nodesetval->nodeNr); i++) {
            if ((xPathObj->nodesetval->nodeTab[i]->type == XML_ATTRIBUTE_NODE) ||
                (xPathObj->nodesetval->nodeTab[i]->type == XML_ELEMENT_NODE)) {
                j++;
                if (debugLevel > SAMLDebugThreshold_High) {
                    SAMLXMLNodeReport(xPathObj->nodesetval->nodeTab[i], j);
                }
                rejectedNode |= (*func)(samlCtx, signedParent, encryptedParent, rejectedNode,
                                        xPathObj->nodesetval->nodeTab[i]);
            }
        }
    } else if (childEncryptedXPathStr != NULL) {
        xPathObj = xmlXPathEvalExpression(BAD_CAST childEncryptedXPathStr, samlCtx->xPathCtx);
        if ((xPathObj != NULL) && (xPathObj->nodesetval != NULL) &&
            (0 < xPathObj->nodesetval->nodeNr)) {
            for (i = 0; xPathObj->nodesetval && (i < xPathObj->nodesetval->nodeNr); i++) {
                if ((xPathObj->nodesetval->nodeTab[i]->type == XML_ELEMENT_NODE)) {
                    j++;
                    if (debugLevel > SAMLDebugThreshold_High) {
                        SAMLXMLNodeReport(xPathObj->nodesetval->nodeTab[i], j);
                    }
                    res = SAMLDecryptNodes(xPathObj->nodesetval->nodeTab[i], samlCtx);
                    encryptedNode = res == 0;
                    if (encryptedNode) {
                        rejectedNode |= SAMLExtractCtx(samlCtx, signedParent, TRUE, rejectedNode,
                                                       xPathObj->nodesetval->nodeTab[i],
                                                       childXPathStr, NULL, func);
                    } else {
                        if (debugLevel > SAMLDebugThreshold_High) {
                            Log(AT "not decrypted.");
                        }
                    }
                }
            }
        }
    }

    if (xPathObj) {
        xmlXPathFreeObject(xPathObj);
        xPathObj = NULL;
    }

    samlCtx->xPathCtx->node = savedNode;

    if (j == 0) {
        /* There must be at least one call to func() so that recursion in func() can continue. */
        rejectedNode |= (*func)(samlCtx, signedParent, encryptedParent, rejectedNode, NULL);
    }

    return rejectedNode || rejectedParent;
}

BOOL SAMLExtractNameID(struct SAMLCONTEXT *samlCtx,
                       BOOL signedParent,
                       BOOL encryptedParent,
                       BOOL rejectedParent,
                       xmlNodePtr nodePtr) {
    BOOL rejectedNode = FALSE;
    xmlChar *val;

    if (nodePtr != NULL) {
        val = xmlNodeGetContent(nodePtr);
        if (val != NULL) {
            SAMLExtractValue(val, " ", &(samlCtx->nameID));
            FreeThenNull(val);
            FreeThenNull(samlCtx->nameIDFormat);
            FreeThenNull(samlCtx->nameIDNameQualifier);
            FreeThenNull(samlCtx->nameIDSPNameQualifier);
            samlCtx->nameIDFormat = xmlGetProp(nodePtr, BAD_CAST "Format");
            samlCtx->nameIDNameQualifier = xmlGetProp(nodePtr, BAD_CAST "NameQualifier");
            samlCtx->nameIDSPNameQualifier = xmlGetProp(nodePtr, BAD_CAST "SPNameQualifier");
        } else {
            /* NameID (EncryptedID) is not required. */
        }

        samlCtx->counters.totalNameIDNodeCount++;
        if (rejectedNode) {
            samlCtx->counters.rejectedNameIDNodeCount++;
        }
    }

    return rejectedNode || rejectedParent;
}

BOOL SAMLExtractSubjectConfirmation(struct SAMLCONTEXT *samlCtx,
                                    BOOL signedParent,
                                    BOOL encryptedParent,
                                    BOOL rejectedParent,
                                    xmlNodePtr nodePtr) {
    BOOL rejectedNode = FALSE;
    xmlChar *method = NULL;

    if (nodePtr != NULL) {
        if (samlCtx->shibbolethVersion == SHIBVERSION20) {
            method = xmlGetProp(nodePtr, BAD_CAST "Method");
        } else {
            method = xmlNodeGetContent(nodePtr);
        }
        if (method != NULL) {
            SAMLExtractValue(method, " ", &(samlCtx->SubjectConfirmationMethod));
            xmlFreeThenNull(method);
        } else {
            rejectedNode = TRUE; /* Method is required. */
        }

        samlCtx->counters.totalSubjectConfirmationNodeCount++;
        if (rejectedNode) {
            samlCtx->counters.rejectedSubjectConfirmationNodeCount++;
        }
    }

    return rejectedNode || rejectedParent;
}

BOOL SAMLExtractSubject(struct SAMLCONTEXT *samlCtx,
                        BOOL signedParent,
                        BOOL encryptedParent,
                        BOOL rejectedParent,
                        xmlNodePtr nodePtr) {
    BOOL rejectedNode = FALSE;

    if (nodePtr != NULL) {
        samlCtx->counters.totalSubjectNodeCount++;
        if (rejectedNode) {
            samlCtx->counters.rejectedSubjectNodeCount++;
        }
    }

    rejectedNode |=
        SAMLExtractCtx(samlCtx, signedParent, encryptedParent, rejectedParent, nodePtr,
                       "saml:SubjectConfirmation", NULL, SAMLExtractSubjectConfirmation);

    if (samlCtx->shibbolethVersion == SHIBVERSION20) {
        rejectedNode |=
            SAMLExtractCtx(samlCtx, signedParent, encryptedParent, rejectedParent, nodePtr,
                           "saml:NameID", "saml:EncryptedID", SAMLExtractNameID);
    } else {
        rejectedNode |= SAMLExtractCtx(samlCtx, signedParent, encryptedParent, rejectedParent,
                                       nodePtr, "saml:NameIdentifier", NULL, SAMLExtractNameID);
    }

    return rejectedNode || rejectedParent;
}

BOOL SAMLExtractStatusCode(struct SAMLCONTEXT *samlCtx,
                           BOOL signedParent,
                           BOOL encryptedParent,
                           BOOL rejectedParent,
                           xmlNodePtr nodePtr) {
    BOOL rejectedNode = FALSE;
    xmlChar *code = NULL;

    if (nodePtr != NULL) {
        code = xmlGetProp(nodePtr, BAD_CAST "Value");
        if (code != NULL) {
            if (!(EndsIWith((char *)code, ":Success") != NULL)) {
                rejectedNode = TRUE;
            }
            SAMLExtractValue(code, " ", &(samlCtx->statusCode));
            xmlFreeThenNull(code);
        } else {
            rejectedNode = TRUE; /* it's required. */
        }
    }

    return rejectedNode || rejectedParent;
}

BOOL SAMLExtractStatus(struct SAMLCONTEXT *samlCtx,
                       BOOL signedParent,
                       BOOL encryptedParent,
                       BOOL rejectedParent,
                       xmlNodePtr nodePtr) {
    BOOL rejectedNode = FALSE;

    if (nodePtr != NULL) {
        samlCtx->counters.totalStatusNodeCount++;
        if (rejectedNode) {
            samlCtx->counters.rejectedStatusNodeCount++;
        }
    }

    rejectedNode |= SAMLExtractCtx(samlCtx, signedParent, encryptedParent, rejectedParent, nodePtr,
                                   "//samlp:StatusCode", NULL, SAMLExtractStatusCode);

    return rejectedNode || rejectedParent;
}

BOOL SAMLExtractAttributeValue(struct SAMLCONTEXT *samlCtx,
                               BOOL signedParent,
                               BOOL encryptedParent,
                               BOOL rejectedParent,
                               xmlNodePtr nodePtr) {
    BOOL valid = TRUE;
    BOOL rejectedNode = FALSE;
    BOOL writeLog = FALSE;
    char *scope = NULL;
    char *nv = NULL;
    char *name = NULL;
    char *friendlyName = NULL;
    xmlChar *dv, *rv;
    struct SAMLATTRIBUTE *sa;

    if (!(sa = calloc(1, sizeof(*sa))))
        PANIC;

    if (debugLevel > SAMLDebugThreshold_Low) {
        writeLog = TRUE;
    }

    if (nodePtr != NULL) {
        sa->path = (char *)xmlGetNodePath(nodePtr);

        if (encryptedParent) {
            samlCtx->counters.encryptedAttributeValueCount++;
        }
        if (signedParent) {
            samlCtx->counters.signedAttributeValueCount++;
        }

        if (samlCtx->shibbolethVersion == SHIBVERSION13) {
            name = (char *)xmlGetProp(nodePtr->parent, BAD_CAST "AttributeName");
            friendlyName = NULL;
        } else {
            name = (char *)xmlGetProp(nodePtr->parent, BAD_CAST "Name");
            friendlyName = (char *)xmlGetProp(nodePtr->parent, BAD_CAST "FriendlyName");
            if (friendlyName != NULL) {
                if (!(sa->friendlyName = strdup(friendlyName)))
                    PANIC;
                xmlFreeThenNull(friendlyName);
            }
        }
        if (name == NULL) {
            writeLog = TRUE;
            rejectedNode = TRUE;
            if (!(sa->name = strdup("")))
                PANIC;
        } else {
            if (!(sa->name = strdup(name)))
                PANIC;
            xmlFreeThenNull(name);
        }

        scope = (char *)xmlGetProp(nodePtr, BAD_CAST "Scope");
        if (scope && *scope == 0) {
            xmlFreeThenNull(scope);
        } else if (scope) {
            int j;
            valid = FALSE;
            dv = rv = NULL;
            if (samlCtx->xPathObjScope &&
                samlCtx->xPathObjScope->nodesetval) { /* todo: what's the correct thing to do if
                                                         samlCtx->xPathObjScope is NULL? */
                for (j = 0; j < samlCtx->xPathObjScope->nodesetval->nodeNr; j++) {
                    if (samlCtx->xPathObjScope->nodesetval->nodeTab[j]->type != XML_ELEMENT_NODE) {
                        continue;
                    }
                    if (dv = xmlNodeGetContent(samlCtx->xPathObjScope->nodesetval->nodeTab[j])) {
                        rv = xmlGetProp(samlCtx->xPathObjScope->nodesetval->nodeTab[j],
                                        BAD_CAST "regexp");
                        if (SAMLScopeValid(&dv, &rv, scope)) {
                            valid = TRUE;
                            break;
                        }
                    }
                }
            }
            if (!valid) {
                samlCtx->counters.outOfScopeAttributeValueCount++;
                rejectedNode |= !samlCtx->includeInvalidScope;
            }
            if (!(sa->scope = strdup(scope)))
                PANIC;
            xmlFreeThenNull(scope);
        }

        nv = (char *)xmlNodeGetContent(nodePtr);
        if (nv) {
            if (!(sa->value = strdup(nv)))
                PANIC;
            xmlFreeThenNull(nv);
        } else {
            writeLog = TRUE;
            rejectedNode = TRUE;
        }

        samlCtx->counters.totalAttributeValueCount++;
        if (rejectedNode) {
            samlCtx->counters.rejectedAttributeValueCount++;
        }

        sa->rejected = rejectedNode || rejectedParent;
    } else {
        sa->path = (char *)xmlGetNodePath(xmlDocGetRootElement(samlCtx->responseDoc));
        sa->rejected = TRUE;
    }

    sa->valid = valid;
    sa->encryptedValue = encryptedParent;
    sa->signedValue = signedParent;
    sa->httpsGetMethod = samlCtx->counters.httpsGetMethod;

    if (writeLog) {
        Log(AT
            "value '%s' at path '%s' name '%s', friendlyName '%s', scope '%s', valid in scope %d, "
            "rejected %d, signed %d, encrypted %d",
            PNS(sa->value), PNS(sa->path), PNS(sa->name), PNS(sa->friendlyName), PNS(sa->scope),
            sa->valid, sa->rejected, sa->signedValue, sa->encryptedValue);
    }

    if (nodePtr == NULL) {
        SAMLFreeAttribute(&(sa));
        sa = NULL;
    } else {
        if (samlCtx->saListLast) {
            samlCtx->saListLast->next = sa;
        } else {
            samlCtx->saList = sa;
        }
        samlCtx->saListLast = sa;
    }

    return rejectedNode || rejectedParent;
}

BOOL SAMLExtractAttribute(struct SAMLCONTEXT *samlCtx,
                          BOOL signedParent,
                          BOOL encryptedParent,
                          BOOL rejectedParent,
                          xmlNodePtr nodePtr) {
    BOOL rejectedNode = FALSE;

    if (nodePtr != NULL) {
        if (encryptedParent) {
            samlCtx->counters.encryptedAttributeCount++;
        }

        samlCtx->counters.totalAttributeCount++;
        if (rejectedNode) {
            samlCtx->counters.rejectedAttributeCount++;
        }
    }

    rejectedNode |= SAMLExtractCtx(samlCtx, signedParent, encryptedParent, rejectedNode, nodePtr,
                                   "saml:AttributeValue", NULL, SAMLExtractAttributeValue);

    return rejectedNode || rejectedParent;
}

BOOL SAMLExtractAttributeStatement(struct SAMLCONTEXT *samlCtx,
                                   BOOL signedParent,
                                   BOOL encryptedParent,
                                   BOOL rejectedParent,
                                   xmlNodePtr nodePtr) {
    BOOL rejectedNode = FALSE;

    if (nodePtr != NULL) {
        samlCtx->counters.totalAttributeStatementCount++;
        if (rejectedNode) {
            samlCtx->counters.rejectedAttributeStatementCount++;
        }
    }

    rejectedNode |=
        SAMLExtractCtx(samlCtx, signedParent, encryptedParent, rejectedNode, nodePtr,
                       "saml:Attribute", "saml:EncryptedAttribute", SAMLExtractAttribute);

    return rejectedNode || rejectedParent;
}

BOOL SAMLExtractAuthnStatement(struct SAMLCONTEXT *samlCtx,
                               BOOL signedParent,
                               BOOL encryptedParent,
                               BOOL rejectedParent,
                               xmlNodePtr nodePtr) {
    BOOL rejectedNode = FALSE;
    xmlChar *authnInstant = NULL;
    int res;

    if (nodePtr != NULL) {
        xmlFreeThenNull(samlCtx->sessionIndex);
        samlCtx->sessionIndex = (char *)xmlGetProp(nodePtr, BAD_CAST "SessionIndex");

        authnInstant = xmlGetProp(nodePtr, BAD_CAST "AuthnInstant");
        res = (authnInstant == NULL) ? FALSE : SAMLCheckInstantValue(authnInstant, 0, samlCtx);
        if (debugLevel > SAMLDebugThreshold_Low) {
            Log(AT "AuthenticationInstant = '%s', accepted = %d.", PNS(authnInstant), res);
        }
        xmlFreeThenNull(authnInstant);
        if (!res) {
            rejectedNode = TRUE; /* It's required. */
        }

        if (encryptedParent) {
            samlCtx->counters.encryptedAuthnStatementCount++;
        }

        samlCtx->counters.totalAuthnStatementCount++;
        if (rejectedNode) {
            samlCtx->counters.rejectedAuthnStatementCount++;
        }
    }

    return rejectedNode || rejectedParent;
}

BOOL SAMLExtractAuthnticationStatement(struct SAMLCONTEXT *samlCtx,
                                       BOOL signedParent,
                                       BOOL encryptedParent,
                                       BOOL rejectedParent,
                                       xmlNodePtr nodePtr) {
    BOOL rejectedNode = FALSE;
    xmlChar *authnInstant = NULL;
    int res;

    if (nodePtr != NULL) {
        authnInstant = xmlGetProp(nodePtr, BAD_CAST "AuthenticationInstant");
        res = (authnInstant == NULL) ? FALSE : SAMLCheckInstantValue(authnInstant, 0, samlCtx);
        if (debugLevel > SAMLDebugThreshold_Low) {
            Log(AT "AuthenticationInstant = '%s', accepted = %d.", PNS(authnInstant), res);
        }
        xmlFreeThenNull(authnInstant);
        if (!res) {
            rejectedNode = TRUE; /* It's required. */
        }

        if (encryptedParent) {
            samlCtx->counters.encryptedAuthnStatementCount++;
        }

        samlCtx->counters.totalAuthnStatementCount++;
        if (rejectedNode) {
            samlCtx->counters.rejectedAuthnStatementCount++;
        }
    }

    return rejectedNode || rejectedParent;
}

BOOL SAMLExtractAssertion(struct SAMLCONTEXT *samlCtx,
                          BOOL signedParent,
                          BOOL encryptedParent,
                          BOOL rejectedParent,
                          xmlNodePtr nodePtr) {
    xmlChar *issueInstant = NULL;
    int res;
    int sigCount = 0;
    int sigVerifiedCount = 0;
    BOOL signedNode = FALSE;
    BOOL rejectedNode = FALSE;

    if (nodePtr != NULL) {
        res = SAMLVerifySignature(nodePtr, samlCtx);
        signedNode = res == 0;
        samlCtx->counters.totalSignatureNodeCount += sigCount;
        samlCtx->counters.totalSignatureVerifiedNodeCount += sigVerifiedCount;
        switch (res) {
        case 0:
            /* Log(AT "Assertion is signed; signature is valid"); */
            break;
        case 1:
            Log(AT "Assertion is signed; signature is invalid");
            SAMLXMLNodeReport(nodePtr, 0);
            rejectedNode = TRUE;
            break;
        case 2:
            if (samlCtx->shibbolethSite->signAssertion) {
                rejectedNode = TRUE;
                Log(AT "Assertion is not signed; signature is required");
            }
            break;
        default:
            Log(AT "Assertion signature is malformed");
            rejectedNode = TRUE;
            break;
        }

        if (signedNode) {
            samlCtx->counters.signedAssertionCount++;
        }

        if (encryptedParent) {
            samlCtx->counters.encryptedAssertionCount++;
        } else if (samlCtx->shibbolethSite->encryptAssertion) {
            Log(AT "Assertion is not encrypted; encryption is required");
            rejectedNode = TRUE;
        }

        issueInstant = xmlGetProp(nodePtr, BAD_CAST "IssueInstant");
        res = (issueInstant == NULL) ? FALSE : SAMLCheckInstantValue(issueInstant, 300, samlCtx);
        if (debugLevel > SAMLDebugThreshold_Low) {
            Log(AT "IssueInstant = '%s', accepted = %d.", PNS(issueInstant), res);
        }
        xmlFreeThenNull(issueInstant);
        if (!res) {
            rejectedNode = TRUE; /* It's required. */
        }

        samlCtx->counters.totalAssertionCount++;
        if (rejectedNode) {
            samlCtx->counters.rejectedAssertionCount++;
        }
    }

    rejectedNode |= SAMLExtractCtx(samlCtx, signedNode || signedParent, encryptedParent,
                                   rejectedNode, nodePtr, "saml:Subject", NULL, SAMLExtractSubject);

    if (samlCtx->shibbolethVersion == SHIBVERSION20) {
        rejectedNode |=
            SAMLExtractCtx(samlCtx, signedNode || signedParent, encryptedParent, rejectedNode,
                           nodePtr, "saml:AuthnStatement", NULL, SAMLExtractAuthnStatement);
    } else {
        rejectedNode |= SAMLExtractCtx(samlCtx, signedNode || signedParent, encryptedParent,
                                       rejectedNode, nodePtr, "saml:AuthnticationStatement", NULL,
                                       SAMLExtractAuthnticationStatement);
    }

    rejectedNode |=
        SAMLExtractCtx(samlCtx, signedNode || signedParent, encryptedParent, rejectedNode, nodePtr,
                       "saml:AttributeStatement", NULL, SAMLExtractAttributeStatement);

    return rejectedNode || rejectedParent;
}

BOOL SAMLExtractResponse(struct SAMLCONTEXT *samlCtx,
                         BOOL signedParent,
                         BOOL encryptedParent,
                         BOOL rejectedParent,
                         xmlNodePtr nodePtr) {
    BOOL rejectedNode = FALSE;
    BOOL signedNode = FALSE;
    xmlChar *issueInstant = NULL;
    int res;
    int sigCount = 0;
    int sigVerifiedCount = 0;

    if (nodePtr != NULL) {
        res = SAMLVerifySignature(nodePtr, samlCtx);
        signedNode = res == 0;
        samlCtx->counters.totalSignatureNodeCount += sigCount;
        samlCtx->counters.totalSignatureVerifiedNodeCount += sigVerifiedCount;
        switch (res) {
        case 0:
            /* Log(AT "Response is signed; signature is valid"); */
            break;
        case 1:
            Log(AT "Response is signed; signature is invalid");
            SAMLXMLNodeReport(nodePtr, 0);
            rejectedNode = TRUE;
            break;
        case 2:
            if (samlCtx->shibbolethSite->signResponse) {
                rejectedNode = TRUE;
                Log(AT "Response is not signed; signature is required");
            }
            break;
        default:
            Log(AT "Response signature is malformed");
            rejectedNode = TRUE;
            break;
        }

        if (signedNode) {
            samlCtx->counters.signedResponseCount++;
        }

        issueInstant = xmlGetProp(nodePtr, BAD_CAST "IssueInstant");
        res = (issueInstant == NULL) ? FALSE : SAMLCheckInstantValue(issueInstant, 300, samlCtx);
        if (debugLevel > SAMLDebugThreshold_Low) {
            Log(AT "IssueInstant = '%s', accepted = %d.", PNS(issueInstant), res);
        }
        xmlFreeThenNull(issueInstant);
        if (!res) {
            rejectedNode = TRUE; /* It's required. */
        }

        samlCtx->counters.totalResponseCount++;
        if (rejectedNode) {
            samlCtx->counters.rejectedResponseCount++;
        }
    }

    rejectedNode |= SAMLExtractCtx(samlCtx, signedNode || signedParent, encryptedParent,
                                   rejectedNode, nodePtr, "samlp:Status", NULL, SAMLExtractStatus);

    rejectedNode |=
        SAMLExtractCtx(samlCtx, signedNode || signedParent, encryptedParent, rejectedNode, nodePtr,
                       "saml:Assertion", "saml:EncryptedAssertion", SAMLExtractAssertion);

    return rejectedNode || rejectedParent;
}

/* samlp:Response/saml:Assertion         /saml:AttributeStatement/saml:Attribute
 * /saml:AttributeValue */
/* samlp:Response/saml:EncryptedAssertion/saml:AttributeStatement/saml:EncryptedAttribute/saml:AttributeValue
 */
/* signed        /signed                 /non-signed             /non-signed             /non-signed
 */
BOOL SAMLExtractResponseDocument(struct SAMLCONTEXT *samlCtx) {
    BOOL rejectedNode = FALSE;
    xmlNodePtr rootElement = NULL;

    if ((samlCtx->xPathCtx != NULL) && (samlCtx->responseDoc != NULL)) {
        rootElement = xmlDocGetRootElement(samlCtx->responseDoc);
        if (rootElement != NULL) {
            if (debugLevel > SAMLDebugThreshold_Low) {
                SAMLXMLNodeReport(rootElement, 1);
            }

            if (samlCtx->shibbolethVersion == SHIBVERSION20) {
                SAMLAddIDAttr(rootElement, BAD_CAST "ID", BAD_CAST "Response", NSSAMLP2);
                SAMLAddIDAttr(rootElement, BAD_CAST "ID", BAD_CAST "Assertion", NSSAML2);
                SAMLAddIDAttr(rootElement, BAD_CAST "ID", BAD_CAST "Attribute", NSSAML2);
                SAMLAddIDAttr(rootElement, BAD_CAST "Id", BAD_CAST "EncryptedKey", NSXENC);
            } else {
                SAMLAddIDAttr(rootElement, BAD_CAST "ResponseID", BAD_CAST "Response", NSSAMLP1);
            }

            rejectedNode |= SAMLGetMetadataScopeNodes(samlCtx);

            rejectedNode |= SAMLExtractCtx(samlCtx, FALSE, FALSE, FALSE, NULL, "//samlp:Response",
                                           NULL, SAMLExtractResponse);

            SAMLReleaseMetadataScopeNodes(samlCtx);

            samlCtx->counters.totalDocumentCount++;
            if (rejectedNode) {
                samlCtx->counters.rejectedDocumentCount++;
            }
        }
    }

    if ((debugLevel > SAMLDebugThreshold_Low)) {
        Log(AT "SubjectConfirmationMethod is '%s'.", PNS(samlCtx->SubjectConfirmationMethod));
        Log(AT "statusCode is '%s'.", PNS(samlCtx->statusCode));
        Log(AT "responseIssuer is '%s'.", PNS(samlCtx->responseIssuer));
        Log(AT "nameID is '%s'.", PNS(samlCtx->nameID));
        Log(AT "peer certificate %s (%s) is '%s'.", LN_commonName, SN_commonName,
            PNS(samlCtx->peerCommonName));
    }

    samlCtx->counters.statusCodeSuccess = TRUE;

    /* It's only a success if nothing was rejected. */
    samlCtx->counters.statusCodeSuccess =
        samlCtx->counters.statusCodeSuccess &&
        ((samlCtx->counters.rejectedAssertionCount == 0) &&
         (samlCtx->counters.rejectedAttributeCount == 0) &&
         (samlCtx->counters.rejectedAttributeStatementCount == 0) &&
         (samlCtx->counters.rejectedAttributeValueCount == 0) &&
         (samlCtx->counters.rejectedDocumentCount == 0) &&
         (samlCtx->counters.rejectedNameIDNodeCount == 0) &&
         (samlCtx->counters.rejectedResponseCount == 0) &&
         (samlCtx->counters.rejectedStatusNodeCount == 0) &&
         (samlCtx->counters.rejectedSubjectConfirmationNodeCount == 0) &&
         (samlCtx->counters.rejectedSubjectNodeCount == 0));

    /* There must be a document, a response, a status node, and all signatures must be valid, and
     * all encrypted nodes must have been decrypted. */
    samlCtx->counters.statusCodeSuccess =
        samlCtx->counters.statusCodeSuccess &&
        ((samlCtx->counters.totalStatusNodeCount > 0) &&
         (samlCtx->counters.totalSignatureNodeCount ==
          samlCtx->counters.totalSignatureVerifiedNodeCount) &&
         (samlCtx->counters.totalEncryptedNodeCount == samlCtx->counters.totalDecryptedNodeCount) &&
         (samlCtx->counters.totalResponseCount == 1) &&
         (samlCtx->counters.totalDocumentCount == 1));

    /*
     * A SAML 2 <saml2p:Response> message issued by an Identity Provider MUST contain exactly one
     * assertion (either a <saml2:Assertion> or an <saml2:EncryptedAssertion> element). The
     * assertion MUST contain exactly one <saml2:AuthnStatement> element and MAY contain zero or one
     * <saml2:AttributeStatement> elements.
     *
     * Or, it's a version 1.3 response for which we must be more permissive.
     */
    samlCtx->counters.statusCodeSuccess =
        samlCtx->counters.statusCodeSuccess &&
        ((samlCtx->shibbolethVersion == SHIBVERSION13) ||
         ((samlCtx->counters.totalAssertionCount == 1) &&
          (samlCtx->counters.totalAuthnStatementCount == 1) &&
          (samlCtx->counters.totalSubjectConfirmationNodeCount == 1) /* &&
           (samlCtx->counters.totalNameIDNodeCount == 1)*/
          ));

    return samlCtx->counters.statusCodeSuccess;
}

void SAMLAddIssueInstant(xmlTextWriterPtr writer, time_t now) {
    char instant[64];
    struct tm tm;

    if (now == 0)
        UUtime(&now);

    UUgmtime_r(&now, &tm);

    sprintf(instant, "%04d-%02d-%02dT%02d:%02d:%02dZ", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
            tm.tm_hour, tm.tm_min, tm.tm_sec);

    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "IssueInstant", BAD_CAST instant) < 0)
        PANIC;
}

int SAMLAttributeQuery(struct TRANSLATE *t,
                       struct SHIBBOLETHSITE *ss,
                       xmlChar *entityID,
                       struct SAMLCONTEXT *samlCtx,
                       BOOL includeInvalidScope) {
    char *xPath = NULL;
    xmlChar *location = NULL;
    BOOL httpsGetMethod = FALSE;
    time_t now;
    char id[SAMLIDMAX];
    xmlTextWriterPtr writer = NULL;
    xmlBufferPtr buf = NULL;
    htmlParserCtxtPtr ctxt = NULL;
    int res = 2;
    xmlChar *val = NULL;

    if (!(xPath = malloc(256 + strlen((char *)entityID))))
        PANIC;

    sprintf(xPath,
            "//md:EntityDescriptor[@entityID = "
            "'%s']/md:AttributeAuthorityDescriptor/"
            "md:AttributeService[@Binding='urn:oasis:names:tc:SAML:1.0:bindings:SOAP-binding']/"
            "@Location",
            entityID);

    UUAcquireMutex(&ss->mutex);

    location = UUxmlSimpleGetContent(NULL, ss->xPathCtx, xPath);

    UUReleaseMutex(&ss->mutex);

    if (location == NULL) {
        Log(AT "AttributeQuery no location from metadata at %s", xPath);
        goto Finished;
    }

    res = 1;

    if (!(buf = xmlBufferCreate()))
        PANIC;
    if (!(writer = xmlNewTextWriterMemory(buf, 0)))
        PANIC;
    if (xmlTextWriterStartDocument(writer, NULL, "UTF-8", NULL) < 0)
        PANIC;

    /* Start root element */
    if (xmlTextWriterStartElementNS(writer, BAD_CAST "soap", BAD_CAST "Envelope", NSSOAP) < 0)
        PANIC;

    if (xmlTextWriterStartElement(writer, BAD_CAST "soap:Body") < 0)
        PANIC;

    if (xmlTextWriterStartElementNS(writer, BAD_CAST "samlp", BAD_CAST "Request", NSSAMLP1) < 0)
        PANIC;
    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "MajorVersion", BAD_CAST "1") < 0)
        PANIC;
    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "MinorVersion", BAD_CAST "1") < 0)
        PANIC;
    UUtime(&now);
    SAMLID(id, now);
    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "RequestID", BAD_CAST id) < 0)
        PANIC;

    SAMLAddIssueInstant(writer, now);

    if (xmlTextWriterStartElement(writer, BAD_CAST "samlp:AttributeQuery") < 0)
        PANIC;
    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Resource", BAD_CAST ss->entityID) < 0)
        PANIC;

    if (xmlTextWriterStartElementNS(writer, BAD_CAST "saml", BAD_CAST "Subject", NSSAML1) < 0)
        PANIC;

    if (xmlTextWriterStartElement(writer, BAD_CAST "saml:NameIdentifier") < 0)
        PANIC;

    if (val = UUxmlSimpleGetAttribute(NULL, samlCtx->xPathCtx, "//saml:NameIdentifier", "Format")) {
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Format", val) < 0)
            PANIC;
        xmlFreeThenNull(val);
    }

    if (val = UUxmlSimpleGetAttribute(NULL, samlCtx->xPathCtx, "//saml:NameIdentifier",
                                      "NameQualifier")) {
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "NameQualifier", val) < 0)
            PANIC;
        xmlFreeThenNull(val);
    }

    if (val = UUxmlSimpleGetContent(NULL, samlCtx->xPathCtx, "//saml:NameIdentifier")) {
        if (xmlTextWriterWriteString(writer, val) < 0)
            PANIC;
        xmlFreeThenNull(val);
    }

    /* Close NameIdentifier element */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    /* Close Subject element */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    /* Close AttributeQuery element */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    /* Close Request element */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    /* Close Body element */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    /* Close Envelope element */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    if (xmlTextWriterEndDocument(writer) < 0)
        PANIC;

    if (xmlTextWriterFlush(writer) < 0)
        PANIC;

    if (optionLogSAML) {
        Log(AT "AttributeQuery to %s:", location);
        LogXMLDoc(NULL, buf->content);
    }

    SAMLResetDocumentCtx(samlCtx);

    ctxt = GetHTML((char *)location, (char *)buf->content, NULL, NULL, NULL,
                   GETHTMLPOSTSOAPSECURITY | GETHTMLACTUALLYXML,
                   ss->certificates ? *ss->certificates : sslActiveIndex, &httpsGetMethod,
                   &(samlCtx->peerCommonName), optionLogSAML);

    /* It appears that ctxt is being returned as null, which was
       causing an abend. Modify code to log error and return on this condition.
    */
    if (ctxt == NULL) {
        Log(AT "AttributeQuery did not return a document");
        goto Finished;
    }

    samlCtx->responseDoc = ctxt->myDoc;
    ctxt->myDoc = NULL;
    samlCtx->shibbolethVersion = SHIBVERSION13;
    samlCtx->includeInvalidScope = includeInvalidScope;
    samlCtx->counters.httpsGetMethod = httpsGetMethod;
    if (debugLevel > SAMLDebugThreshold_Low) {
        Log(AT "Peer '%s=%s'", SN_commonName, PNS(samlCtx->peerCommonName));
    }
    if ((samlCtx->responseDoc == NULL) || (xmlDocGetRootElement(samlCtx->responseDoc) == NULL)) {
        Log(AT "Unable to parse SAML XML");
        goto Finished;
    }
    if (optionLogSAML) {
        Log(AT "Response document before decrypt %s:",
            SAML_VERSION_TEXT(samlCtx->shibbolethVersion));
        LogXMLDoc(samlCtx->responseDoc, NULL);
    }
    samlCtx->xPathCtx = SAMLxmlXPathNewContext(samlCtx->responseDoc, samlCtx->shibbolethVersion);
    if (samlCtx->xPathCtx == NULL) {
        Log(AT "Unable to SAMLxmlXPathNewContext");
        goto Finished;
    }
    samlCtx->responseIssuer =
        (char *)UUxmlSimpleGetContent(NULL, samlCtx->xPathCtx, "//saml:Assertion/@Issuer");
    res = SAMLExtractResponseDocument(samlCtx) ? 0 : 1;
    if (optionLogSAML) {
        Log(AT "Response document after decrypt %s:",
            SAML_VERSION_TEXT(samlCtx->shibbolethVersion));
        LogXMLDoc(samlCtx->responseDoc, NULL);
    }

Finished:
    UUxmlFreeParserCtxtAndDoc(&ctxt);

    if (writer) {
        xmlFreeTextWriter(writer);
        writer = NULL;
    }

    if (buf) {
        xmlBufferFree(buf);
        buf = NULL;
    }

    FreeThenNull(xPath);
    xmlFreeThenNull(location);

    return res;
}

static int SAMLMsgAuth(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct SAMLCONTEXT *samlCtx = (struct SAMLCONTEXT *)context;
    struct SAMLATTRIBUTE *sa;

    UsrLog(uip, "Response Document");
    LogXMLDocLogOnly(samlCtx->responseDoc, NULL);

    UsrLog(uip, "Shibboleth auth: variables");
    for (sa = samlCtx->saList; sa; sa = sa->next) {
        UsrLog(uip,
               "%s%s%s%s%s=%s (rejected=%d, validInScope=%d, signed=%d, encrypted=%d, "
               "httpsGetMethod=%d)",
               sa->name ? sa->name : "", sa->friendlyName ? "|" : "",
               sa->friendlyName ? sa->friendlyName : "", sa->scope ? "@" : "",
               sa->scope ? sa->scope : "", sa->value ? sa->value : "", sa->rejected, sa->valid,
               sa->signedValue, sa->encryptedValue, sa->httpsGetMethod);
    }

    return 0;
}

void SAMLShowAttributes(struct UUSOCKET *s, struct SAMLCONTEXT *samlCtx) {
    struct SAMLATTRIBUTE *sa;

    WriteLine(s, "<h2>Response from Issuer '");
    SendHTMLEncoded(s, PNS(samlCtx->responseIssuer));
    UUSendZ(s, "'\n</h2>\n");

    WriteLine(s, "<p>The response document status is '");
    SendHTMLEncoded(s, PNS(samlCtx->statusCode));
    UUSendZ(s, "' which is considered a ");
    WriteLine(
        s, "<span class='%s</span>\n",
        !samlCtx->counters.statusCodeSuccess ? "failure'>failure (see below)" : "success'>success");
    UUSendZ(s, "after analysis.\n");
    UUSendZ(s, "If this had been a real login, the " SHIBNEWUSR " file (if any) ");
    WriteLine(s, "<span class='%s</span>\n",
              !samlCtx->counters.statusCodeSuccess ? "failure'>would not" : "success'>would");
    UUSendZ(s, " have been run.  If there were no " SHIBNEWUSR " file, the user ");
    WriteLine(s, "<span class='%s</span>\n",
              !samlCtx->counters.statusCodeSuccess ? "failure'>would not" : "success'>would");
    UUSendZ(s, " have been logged in.\n");

    UUSendZ(s, "<p>The response Subject Confirmation Method is '");
    SendHTMLEncoded(s, PNS(samlCtx->SubjectConfirmationMethod));
    UUSendZ(s, "'");
    UUSendZ(s, ".</p>\n");

    UUSendZ(s, "<p>The response Subject NameID is '");
    SendHTMLEncoded(s, PNS(samlCtx->nameID));
    UUSendZ(s, "'");
    UUSendZ(s, ".</p>\n");

    if (samlCtx->peerCommonName && (samlCtx->peerCommonName[0] != 0)) {
        WriteLine(s, "<p>The peer certificate %s (%s) is '", LN_commonName, SN_commonName);
        SendHTMLEncoded(s, samlCtx->peerCommonName);
        UUSendZ(s, "'");
        UUSendZ(s, ".</p>\n");
    } else {
        WriteLine(s, "<p>The peer did not present a certificate with a %s (%s)", LN_commonName,
                  SN_commonName);
        UUSendZ(s, ".</p>\n");
    }

    if (samlCtx->saList == NULL) {
        UUSendZ(s, "<p>No attributes were extracted from the response document.</p>\n");
    } else {
        int row = 0;

        UUSendZ(
            s,
            "<p/><table class='bordered-table'>\n"
            "<caption>Response Attributes Extracted</caption>"
            "<thead>"
            "  <tr>  <th scope='col'>Row</th>  <th scope='col'>Name</th>  <th scope='col'>Friendly "
            "Name</th>  <th scope='col'>Value</th>  <th scope='col'>Path</th>  <th "
            "scope='col'>Scope</th>  <th scope='col'>Conditions</th></tr>\n"
            "</thead><tfoot><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</"
            "td><td>&nbsp;</td><td>&nbsp;</td><td>* Values that are not in scope are ignored and "
            "will not appear in the auth: namespace.<br/>&#x25A3; is TRUE, &#x25A2; is "
            "FALSE</td></tr></tfoot><tbody>");
        for (sa = samlCtx->saList; sa; sa = sa->next) {
            row++;

            UUSendZ(s, "  <tr>  <td align='right'><tt>");

            WriteLine(s, "%d", row);

            UUSendZ(s, "</tt></td>  <td>");

            if (sa->name && *sa->name) {
                SendHTMLEncoded(s, sa->name);
            } else {
                UUSendZ(s, "&nbsp;");
            }

            UUSendZ(s, "</td>  <td>");

            if (sa->friendlyName && *sa->friendlyName) {
                SendHTMLEncoded(s, sa->friendlyName);
            } else {
                UUSendZ(s, "&nbsp;");
            }

            UUSendZ(s, "</td>  <td>");

            if (sa->value && *sa->value) {
                SendHTMLEncoded(s, sa->value);
            } else {
                UUSendZ(s, "&nbsp;");
            }

            UUSendZ(s, "</td>  <td>");

            if (sa->path) {
                char q;
                char *p = sa->path;
                char *j;

                if (!(j = strchr(p, '/'))) {
                    j = strchr(p, 0) - 1;
                } else {
                    j++;
                    if (!(j = strchr(j, '/'))) {
                        j = strchr(p, 0) - 1;
                    }
                }
                while (*p) {
                    j++;
                    q = *j;
                    *j = 0;
                    SendHTMLEncoded(s, p);
                    UUSendZ(s, "&thinsp;");
                    *j = q;
                    p = j;
                    if (!(j = strchr(p, '/'))) {
                        j = strchr(p, 0) - 1;
                    }
                }
            } else {
                UUSendZ(s, "&nbsp;");
            }

            UUSendZ(s, "</td>  <td>");

            if (sa->scope) {
                SendHTMLEncoded(s, sa->scope);
            } else {
                UUSendZ(s, "&nbsp;");
            }

            UUSendZ(s, "</td>  <td>");

            WriteLine(s, "<div>&#x25A%s; Valid</div>", (!sa->rejected) ? "3" : "2");
            WriteLine(s, "<div>&#x25A%s; In Scope *</div>", (sa->valid) ? "3" : "2");
            WriteLine(s, "<div>&#x25A%s; Encrypted Parent</div>", (sa->encryptedValue) ? "3" : "2");
            WriteLine(s, "<div>&#x25A%s; Signed Parent</div>", (sa->signedValue) ? "3" : "2");
            WriteLine(s, "<div>&#x25A%s; HTTPS GET Method</div>", (sa->httpsGetMethod) ? "3" : "2");

            UUSendZ(s, "</td>  </tr>\n");
        }
        UUSendZ(s, "</tbody></table>\n");
    }

    UUSendZ(s,
            "<p/><table class='bordered-table'>\n"
            "<caption>Response Statistics</caption>\n"
            "<thead>\n"
            "  <tr>  <th scope='col' width='80%'>Item</th>  <th scope='col' width='20%' "
            "align='right'>Count</th>  </tr>\n"
            "</thead><tbody>\n");

    WriteLine(s, SAMLMsgAuthFmtHTMLGREEN, "encrypted Assertion",
              samlCtx->counters.encryptedAssertionCount);
    WriteLine(s, SAMLMsgAuthFmtHTML("rejected Assertion", samlCtx->counters.rejectedAssertionCount,
                                    samlCtx->counters.rejectedAssertionCount == 0));
    WriteLine(s, SAMLMsgAuthFmtHTMLGREEN, "signed Assertion",
              samlCtx->counters.signedAssertionCount);
    WriteLine(s, SAMLMsgAuthFmtHTML("total Assertion", samlCtx->counters.totalAssertionCount,
                                    (samlCtx->shibbolethVersion == SHIBVERSION13) ||
                                        (samlCtx->counters.totalAssertionCount == 1)));
    WriteLine(s, SAMLMsgAuthFmtHTMLGREEN, "encrypted Attribute",
              samlCtx->counters.encryptedAttributeCount);
    WriteLine(s, SAMLMsgAuthFmtHTML("rejected Attribute", samlCtx->counters.rejectedAttributeCount,
                                    samlCtx->counters.rejectedAttributeCount == 0));
    WriteLine(s, SAMLMsgAuthFmtHTMLGREEN, "signed Attribute",
              samlCtx->counters.signedAttributeCount);
    WriteLine(s, SAMLMsgAuthFmtHTMLGREEN, "total Attribute", samlCtx->counters.totalAttributeCount);
    WriteLine(s, SAMLMsgAuthFmtHTML("rejected Attribute Statement",
                                    samlCtx->counters.rejectedAttributeStatementCount,
                                    samlCtx->counters.rejectedAttributeStatementCount == 0));
    WriteLine(s, SAMLMsgAuthFmtHTMLGREEN, "signed Attribute Statement",
              samlCtx->counters.signedAttributeStatementCount);
    WriteLine(s, SAMLMsgAuthFmtHTMLGREEN, "total Attribute Statement",
              samlCtx->counters.totalAttributeStatementCount);
    WriteLine(s, SAMLMsgAuthFmtHTMLGREEN, "encrypted Attribute Value",
              samlCtx->counters.encryptedAttributeValueCount);
    WriteLine(s, SAMLMsgAuthFmtHTMLGREEN, "outOfScope Attribute Value",
              samlCtx->counters.outOfScopeAttributeValueCount);
    WriteLine(s, SAMLMsgAuthFmtHTML("rejected Attribute Value",
                                    samlCtx->counters.rejectedAttributeValueCount,
                                    samlCtx->counters.rejectedAttributeValueCount == 0));
    WriteLine(s, SAMLMsgAuthFmtHTMLGREEN, "signed Attribute Value",
              samlCtx->counters.signedAttributeValueCount);
    WriteLine(s, SAMLMsgAuthFmtHTMLGREEN, "total Attribute Value",
              samlCtx->counters.totalAttributeValueCount);
    WriteLine(s, SAMLMsgAuthFmtHTMLGREEN, "encrypted Authentication Statement Node",
              samlCtx->counters.encryptedAuthnStatementCount);
    WriteLine(s, SAMLMsgAuthFmtHTML("rejected Authentication Statement Node",
                                    samlCtx->counters.rejectedAuthnStatementCount,
                                    samlCtx->counters.rejectedAuthnStatementCount == 0));
    WriteLine(s, SAMLMsgAuthFmtHTML("total Authentication Statement Node",
                                    samlCtx->counters.totalAuthnStatementCount,
                                    (samlCtx->shibbolethVersion == SHIBVERSION13) ||
                                        (samlCtx->counters.totalAuthnStatementCount == 1)));
    WriteLine(s,
              SAMLMsgAuthFmtHTML("total Encrypted Node", samlCtx->counters.totalEncryptedNodeCount,
                                 samlCtx->counters.totalDecryptedNodeCount ==
                                     samlCtx->counters.totalEncryptedNodeCount));
    WriteLine(s,
              SAMLMsgAuthFmtHTML("total Decrypted Node", samlCtx->counters.totalDecryptedNodeCount,
                                 samlCtx->counters.totalDecryptedNodeCount ==
                                     samlCtx->counters.totalEncryptedNodeCount));
    WriteLine(s, SAMLMsgAuthFmtHTML("rejected Document", samlCtx->counters.rejectedDocumentCount,
                                    samlCtx->counters.rejectedDocumentCount == 0));
    WriteLine(s, SAMLMsgAuthFmtHTML("total Document", samlCtx->counters.totalDocumentCount,
                                    samlCtx->counters.totalDocumentCount == 1));
    WriteLine(s,
              SAMLMsgAuthFmtHTML("rejected NameID Node", samlCtx->counters.rejectedNameIDNodeCount,
                                 samlCtx->counters.rejectedAssertionCount == 0));
    WriteLine(s, SAMLMsgAuthFmtHTML("total NameID Node", samlCtx->counters.totalNameIDNodeCount,
                                    (samlCtx->shibbolethVersion == SHIBVERSION13) ||
                                        (samlCtx->counters.totalNameIDNodeCount == 1)));
    WriteLine(s, SAMLMsgAuthFmtHTML("rejected Response", samlCtx->counters.rejectedResponseCount,
                                    samlCtx->counters.rejectedResponseCount == 0));
    WriteLine(s, SAMLMsgAuthFmtHTMLGREEN, "signed Response", samlCtx->counters.signedResponseCount);
    WriteLine(s, SAMLMsgAuthFmtHTML("total Response", samlCtx->counters.totalResponseCount,
                                    samlCtx->counters.totalResponseCount == 1));
    WriteLine(s,
              SAMLMsgAuthFmtHTML("total Signature Node", samlCtx->counters.totalSignatureNodeCount,
                                 samlCtx->counters.totalSignatureVerifiedNodeCount ==
                                     samlCtx->counters.totalSignatureNodeCount));
    WriteLine(s, SAMLMsgAuthFmtHTML("total Signature Verified Node",
                                    samlCtx->counters.totalSignatureVerifiedNodeCount,
                                    samlCtx->counters.totalSignatureVerifiedNodeCount ==
                                        samlCtx->counters.totalSignatureNodeCount));
    WriteLine(s,
              SAMLMsgAuthFmtHTML("rejected Status Node", samlCtx->counters.rejectedStatusNodeCount,
                                 samlCtx->counters.rejectedStatusNodeCount == 0));
    WriteLine(s, SAMLMsgAuthFmtHTML("total Status Node", samlCtx->counters.totalStatusNodeCount,
                                    samlCtx->counters.totalStatusNodeCount > 0));
    WriteLine(s, SAMLMsgAuthFmtHTML("rejected Subject Confirmation Node",
                                    samlCtx->counters.rejectedSubjectConfirmationNodeCount,
                                    samlCtx->counters.rejectedSubjectConfirmationNodeCount == 0));
    WriteLine(s,
              SAMLMsgAuthFmtHTML("total Subject Confirmation Node",
                                 samlCtx->counters.totalSubjectConfirmationNodeCount,
                                 (samlCtx->shibbolethVersion == SHIBVERSION13) ||
                                     (samlCtx->counters.totalSubjectConfirmationNodeCount == 1)));
    WriteLine(
        s, SAMLMsgAuthFmtHTML("rejected Subject Node", samlCtx->counters.rejectedSubjectNodeCount,
                              samlCtx->counters.rejectedAssertionCount == 0));
    WriteLine(s, SAMLMsgAuthFmtHTML("total Subject Node", samlCtx->counters.totalSubjectNodeCount,
                                    samlCtx->counters.totalSubjectNodeCount > 0));

    UUSendZ(s, "</tbody></table>\n");

    UUSendZ(s, "<p>The response document follows.</p>");
    UUSendZ(s, "<p><tt><pre>\n");
    WriteXMLDocAsHTML(s, samlCtx->responseDoc);
    UUSendZ(s, "</pre></tt></p>\n");
}

xmlSecKeysMngrPtr SAMLGetKeysMngr(int certNum) {
    struct certsKeysMngr {
        struct certsKeysMngr *next;
        int certNum;
        xmlSecKeysMngrPtr mngr;
    };
    static BOOL initRwl = TRUE;
    static struct RWL rwl;
    static struct certsKeysMngr *cache = NULL;
    struct certsKeysMngr *p, *last;

    if (initRwl) {
        UUAcquireMutex(&mInitSsl);
        if (initRwl) {
            RWLInit(&rwl, "SAMLKeysMngr");
            initRwl = FALSE;
        }
        UUReleaseMutex(&mInitSsl);
    }

    RWLAcquireRead(&rwl);
    for (last = NULL, p = cache; p; last = p, p = p->next) {
        if (p->certNum == certNum) {
            break;
        }
    }
    RWLReleaseRead(&rwl);
    if (p) {
        return p->mngr;
    }

    RWLAcquireWrite(&rwl);
    // Check if another thread just added it on to the end
    if (last == NULL) {
        p = cache;
    } else {
        p = last->next;
    }
    for (; p; last = p, p = p->next) {
        if (p->certNum == certNum) {
            RWLReleaseWrite(&rwl);
            return p->mngr;
        }
    }

    xmlSecKeysMngrPtr mngr;
    char fnKey[MAX_PATH];
    char fnCrt[MAX_PATH];
    xmlSecKeyPtr key;

    mngr = xmlSecKeysMngrCreate();
    if (mngr == NULL) {
        Log(AT "xmlsec failed to create keysMngr manager");
        PANIC;
    }

    if (xmlSecCryptoAppDefaultKeysMngrInit(mngr) < 0) {
        Log(AT "xmlsec failed to initialize keysMngr manager");
        PANIC;
    }

    sprintf(fnKey, "%s%08d.key", SSLDIR, certNum);
    key = xmlSecCryptoAppKeyLoad(fnKey, xmlSecKeyDataFormatPem, NULL, NULL, NULL);
    if (key == NULL) {
        Log(AT "xmlsec failed to load key %s", fnKey);
        goto Failed;
    }

    sprintf(fnCrt, "%s%08d.crt", SSLDIR, certNum);
    if (xmlSecCryptoAppKeyCertLoad(key, fnCrt, xmlSecKeyDataFormatPem) < 0) {
        Log(AT "xmlsec failed to cert %s", fnCrt);
        goto Failed;
    }

    if (xmlSecKeySetName(key, GENERIC_KEY_NAME) < 0) {
        Log(AT "xmlsec failed to set key name for key from %s", fnKey);
        PANIC;
    }

    if (xmlSecCryptoAppDefaultKeysMngrAdoptKey(mngr, key) < 0) {
        Log(AT "xmlsec adoption failed for key %s", fnKey);
        PANIC;
    }

    if (!(p = calloc(1, sizeof(*p))))
        PANIC;
    p->certNum = certNum;
    p->mngr = mngr;
    if (last) {
        last->next = p;
    } else {
        cache = p;
    }

    goto Finished;

Failed:
    if (key) {
        xmlSecKeyDestroy(key);
        key = NULL;
    }

    if (mngr) {
        xmlSecKeysMngrDestroy(mngr);
        mngr = NULL;
    }

Finished:
    RWLReleaseWrite(&rwl);

    return mngr;
}

/* returns: -1 error, 0 exactly one encrypted node was decrypted, 1 some encrypted nodes are not
 * decrypted, 2 no encrypted nodes.*/
static int SAMLDecryptNodes(xmlNodePtr baseNodePtr, struct SAMLCONTEXT *samlCtx) {
    xmlNodePtr encNode = NULL;
    xmlNodePtr encNodeNext = NULL;
    xmlSecEncCtxPtr encCtx = NULL;
    int encryptedNodeCount = 0;
    int decryptedNodeCount = 0;
    int res = 0;
    int rc = 0;

    encNode = xmlSecFindNode(baseNodePtr, xmlSecNodeEncryptedData, xmlSecEncNs);
    while (encNode != NULL) {
        encryptedNodeCount++;
        encNodeNext = encNode->next;

        if (samlCtx->shibbolethSite->certificates == NULL && sslActiveIndex == 0) {
            /* Can't decode it. */
        } else {
            BOOL decrypted = FALSE;
            int *certificates = samlCtx->shibbolethSite->certificates;
            int certNum = certificates ? *certificates++ : sslActiveIndex;
            int nextCertNum;
            for (; certNum > 0 && !decrypted; certNum = nextCertNum) {
                nextCertNum = certificates ? *certificates++ : 0;
                xmlSecKeysMngrPtr mngr = SAMLGetKeysMngr(certNum);
                if (mngr == NULL) {
                    continue;
                }
                encCtx = xmlSecEncCtxCreate(mngr);
                if (encCtx == NULL) {
                    Log(AT "xmlsec failed to create encryption context");
                    res = -1;
                    break;
                }

                /* decrypt the data */
                if (((rc = xmlSecEncCtxDecrypt(encCtx, encNode)) < 0) || (encCtx->result == NULL)) {
                    if (nextCertNum == 0) {
                        Log(AT "decryption failed, rc = %d", rc);
                    }
                } else {
                    decryptedNodeCount++;
                    decrypted = TRUE;
                }

                xmlSecEncCtxDestroy(encCtx);
                encCtx = NULL;
            }
        }

        if (encNodeNext == NULL) {
            break;
        } else {
            encNode = xmlSecFindNode(encNodeNext, xmlSecNodeEncryptedData, xmlSecEncNs);
        }
    }
    if (res == -1) {
        /* return it */
    } else if ((decryptedNodeCount == 0) && (encryptedNodeCount == 0)) {
        res = 2;
    } else if ((decryptedNodeCount == 1) && (encryptedNodeCount == 1)) {
        res = 0;
    } else {
        res = 1;
    }

    samlCtx->counters.totalEncryptedNodeCount += encryptedNodeCount;
    samlCtx->counters.totalDecryptedNodeCount += decryptedNodeCount;

    if (decryptedNodeCount > 0) {
        if (samlCtx->shibbolethVersion == SHIBVERSION20) {
            SAMLAddIDAttr(baseNodePtr, BAD_CAST "ID", BAD_CAST "Response", NSSAMLP2);
            SAMLAddIDAttr(baseNodePtr, BAD_CAST "ID", BAD_CAST "Assertion", NSSAML2);
            SAMLAddIDAttr(baseNodePtr, BAD_CAST "ID", BAD_CAST "Attribute", NSSAML2);
        } else {
            SAMLAddIDAttr(baseNodePtr, BAD_CAST "ResponseID", BAD_CAST "Response", NSSAMLP1);
        }
    }

    return res;
}

int SAMLResponseDecode(struct TRANSLATE *t, struct SAMLCONTEXT *samlCtx) {
    int res = -1;
    int res1 = -1;

    if (samlCtx->shibbolethVersion == SHIBVERSION20) {
        /*
         * Whether encrypted or not, the <saml2:Assertion> element issued by the
         * Identity Provider MUST itself be signed directly using a <ds:Signature>
         * element within the <saml2:Assertion>.
         */
        samlCtx->assertionMustBeSigned = TRUE;
        samlCtx->responseIssuer =
            (char *)UUxmlSimpleGetContent(NULL, samlCtx->xPathCtx, "//saml:Issuer");
    } else if (samlCtx->shibbolethVersion == SHIBVERSION13) {
        samlCtx->responseIssuer =
            (char *)UUxmlSimpleGetContent(NULL, samlCtx->xPathCtx, "//saml:Assertion/@Issuer");
    } else {
        PANIC;
    }

    if (samlCtx->responseIssuer == NULL) {
        Log(AT "Unable to locate issuer, rejected.");
        goto Finished;
    }

    samlCtx->shibbolethSite = SAMLShibbolethMetadataFindSite(
        "//md:EntityDescriptor[@entityID = '%s']/md:IDPSSODescriptor", samlCtx->responseIssuer);
    if (samlCtx->shibbolethSite == NULL) {
        Log(AT
            "no \"//md:EntityDescriptor[@entityID = '%s']/md:IDPSSODescriptor\" in any "
            "ShibbolethMetadata, rejected.",
            samlCtx->responseIssuer);
        goto Finished;
    }

    if (samlCtx->shibbolethVersion == SHIBVERSION13) {
        /* Shib 1.3 Response must be signed, and must not be encrypted. */
        res1 = SAMLExtractResponseDocument(samlCtx) ? 0 : 1;

        if (res1 == 0) {
            /* Because there's no encryption of Assertions in Shibboleth 1.3, we do this
             * back-channel request for Assertions. */
            res = SAMLAttributeQuery(t, samlCtx->shibbolethSite, BAD_CAST samlCtx->responseIssuer,
                                     samlCtx, 1);
            if (res == 2) {
                /* There was no AttributeService Location, so we're stuck with the Assertions we've
                 * already received in the clear. */
                res = res1;
            }
        }
    } else {
        if (optionLogSAML) {
            Log(AT "Response document before decrypt %s:",
                SAML_VERSION_TEXT(samlCtx->shibbolethVersion));
            LogXMLDoc(samlCtx->responseDoc, NULL);
        }

        res = SAMLExtractResponseDocument(samlCtx) ? 0 : 1;

        if (optionLogSAML) {
            Log(AT "Response document after decrypt %s:",
                SAML_VERSION_TEXT(samlCtx->shibbolethVersion));
            LogXMLDoc(samlCtx->responseDoc, NULL);
        }
    }

Finished:
    return res;
}

int SAMLArtifactRequest(struct TRANSLATE *t,
                        struct FORMFIELD *samlArtifactField,
                        struct SHIBBOLETHSITE *ss,
                        xmlChar *entityID,
                        struct SAMLCONTEXT *samlCtx,
                        BOOL includeInvalidScope) {
    char *xPath = NULL;
    xmlChar *location = NULL;
    time_t now;
    BOOL httpsGetMethod = FALSE;
    char id[SAMLIDMAX];
    xmlTextWriterPtr writer = NULL;
    xmlBufferPtr buf = NULL;
    htmlParserCtxtPtr ctxt = NULL;
    struct FORMFIELDMULTIVALUE *ffnv;
    char *artifactElement;
    int res = FALSE;

    if (!(xPath = malloc(256 + strlen((char *)entityID))))
        PANIC;

    sprintf(xPath,
            "//md:EntityDescriptor[@entityID = "
            "'%s']/md:IDPSSODescriptor/md:ArtifactResolutionService[@Binding='%s']/@Location",
            entityID,
            samlCtx->shibbolethVersion == SHIBVERSION13
                ? "urn:oasis:names:tc:SAML:1.0:bindings:SOAP-binding"
                : "urn:oasis:names:tc:SAML:2.0:bindings:SOAP");

    UUAcquireMutex(&(samlCtx->shibbolethSite->mutex));

    location = UUxmlSimpleGetContent(NULL, samlCtx->shibbolethSite->xPathCtx, xPath);

    UUReleaseMutex(&(samlCtx->shibbolethSite->mutex));

    if (location == NULL)
        goto Finished;

    if (!(buf = xmlBufferCreate()))
        PANIC;
    if (!(writer = xmlNewTextWriterMemory(buf, 0)))
        PANIC;
    if (xmlTextWriterStartDocument(writer, NULL, "UTF-8", NULL) < 0)
        PANIC;

    /* Start root element */
    if (xmlTextWriterStartElementNS(writer, BAD_CAST "soap", BAD_CAST "Envelope", NSSOAP) < 0)
        PANIC;

    if (xmlTextWriterStartElement(writer, BAD_CAST "soap:Body") < 0)
        PANIC;

    if (samlCtx->shibbolethVersion == SHIBVERSION13) {
        if (xmlTextWriterStartElementNS(writer, BAD_CAST "samlp", BAD_CAST "Request", NSSAMLP1) < 0)
            PANIC;
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "MajorVersion", BAD_CAST "1") < 0)
            PANIC;
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "MinorVersion", BAD_CAST "1") < 0)
            PANIC;
        UUtime(&now);
        SAMLID(id, now);
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "RequestID", BAD_CAST id) < 0)
            PANIC;
        artifactElement = "samlp:AssertionArtifact";
    } else {
        if (xmlTextWriterStartElementNS(writer, BAD_CAST "samlp", BAD_CAST "ArtifactResolve",
                                        NSSAMLP2) < 0)
            PANIC;
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Version", BAD_CAST "2.0") < 0)
            PANIC;
        UUtime(&now);
        SAMLID(id, now);
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "ID", BAD_CAST id) < 0)
            PANIC;
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Destination", location) < 0)
            PANIC;
        artifactElement = "samlp:Artifact";
    }

    SAMLAddIssueInstant(writer, now);

    if (samlCtx->shibbolethVersion == SHIBVERSION20) {
        if (xmlTextWriterStartElementNS(writer, BAD_CAST "saml", BAD_CAST "Issuer", NSSAML2) < 0)
            PANIC;
        if (xmlTextWriterWriteString(writer, BAD_CAST ss->entityID) < 0)
            PANIC;
        if (xmlTextWriterEndElement(writer) < 0)
            PANIC;
    }

    if (xmlTextWriterStartElement(writer, BAD_CAST artifactElement) < 0)
        PANIC;
    if (xmlTextWriterWriteString(writer, BAD_CAST samlArtifactField->value) < 0)
        PANIC;
    /* Close AssertionArtifact/Artifact element */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    for (ffnv = samlArtifactField->nextMultiValue; ffnv; ffnv = ffnv->nextMultiValue) {
        if (xmlTextWriterStartElement(writer, BAD_CAST artifactElement) < 0)
            PANIC;
        if (xmlTextWriterWriteString(writer, BAD_CAST ffnv->value) < 0)
            PANIC;
        /* Close AssertionArtifact element */
        if (xmlTextWriterEndElement(writer) < 0)
            PANIC;
    }

    /* Close Request element */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    /* Close Body element */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    /* Close Envelope element */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    if (xmlTextWriterEndDocument(writer) < 0)
        PANIC;

    if (xmlTextWriterFlush(writer) < 0)
        PANIC;

    if (optionLogSAML) {
        Log(AT "ArtifactRequest to %s:", location);
        LogXMLDoc(NULL, buf->content);
    }

    SAMLResetDocumentCtx(samlCtx);

    ctxt =
        GetHTML((char *)location, (char *)buf->content, NULL, NULL, NULL,
                GETHTMLPOSTSOAPSECURITY | GETHTMLACTUALLYXML,
                samlCtx->shibbolethSite->certificates ? *samlCtx->shibbolethSite->certificates : 0,
                &httpsGetMethod, &(samlCtx->peerCommonName), optionLogSAML);
    samlCtx->responseDoc = ctxt->myDoc;
    ctxt->myDoc = NULL;
    samlCtx->includeInvalidScope = includeInvalidScope;
    samlCtx->counters.httpsGetMethod = httpsGetMethod;
    if ((samlCtx->responseDoc == NULL) || (xmlDocGetRootElement(samlCtx->responseDoc) == NULL)) {
        Log(AT "Unable to parse SAML XML");
        goto Finished;
    }
    if (optionLogSAML) {
        Log(AT "Response parsed document %s:", SAML_VERSION_TEXT(samlCtx->shibbolethVersion));
        LogXMLDoc(samlCtx->responseDoc, NULL);
    }
    samlCtx->xPathCtx = SAMLxmlXPathNewContext(samlCtx->responseDoc, samlCtx->shibbolethVersion);
    if (samlCtx->xPathCtx == NULL) {
        Log(AT "Unable to SAMLxmlXPathNewContext");
        goto Finished;
    }
    if (debugLevel > SAMLDebugThreshold_Low) {
        Log(AT "Peer '%s=%s'", SN_commonName, PNS(samlCtx->peerCommonName));
    }
    res = SAMLResponseDecode(t, samlCtx);

Finished:
    UUxmlFreeParserCtxtAndDoc(&ctxt);

    if (writer) {
        xmlFreeTextWriter(writer);
        writer = NULL;
    }

    if (buf) {
        xmlBufferFree(buf);
        buf = NULL;
    }

    FreeThenNull(xPath);
    xmlFreeThenNull(location);

    return res;
}

int SAMLSignDoc(struct SHIBBOLETHSITE *ss, xmlDocPtr doc, xmlChar *id) {
    int res = 1;
    char idref[strlen(id) + 2];
    xmlSecDSigCtxPtr dsigCtx = NULL;
    int certNum = ss->certificates ? *ss->certificates : sslActiveIndex;
    xmlNodePtr signNode = NULL;
    xmlNodePtr refNode = NULL;
    xmlNodePtr keyInfoNode = NULL;
    xmlNodePtr x509DataNode = NULL;
    xmlSecKeysMngrPtr mngr = NULL;

    UUxmlSecInit();

    sprintf(idref, "#%s", id);

    /* create signature template for RSA-SHA1 enveloped signature */
    signNode =
        xmlSecTmplSignatureCreate(doc, xmlSecTransformExclC14NId, xmlSecTransformRsaSha256Id, NULL);
    if (signNode == NULL)
        PANIC;

    /* add <Signature/> node to the doc */
    xmlAddChild(xmlDocGetRootElement(doc), signNode);

    /* add reference */
    refNode = xmlSecTmplSignatureAddReference(signNode, xmlSecTransformSha256Id, NULL, idref, NULL);
    if (refNode == NULL)
        PANIC;

    /* add enveloped and exclc14n transform */
    if (!xmlSecTmplReferenceAddTransform(refNode, xmlSecTransformEnvelopedId))
        PANIC;
    if (!xmlSecTmplReferenceAddTransform(refNode, xmlSecTransformExclC14NId))
        PANIC;

    if (!(keyInfoNode = xmlSecTmplSignatureEnsureKeyInfo(signNode, NULL)))
        PANIC;
    if (!(x509DataNode = xmlSecTmplKeyInfoAddX509Data(keyInfoNode)))
        PANIC;

    if (!(mngr = SAMLGetKeysMngr(certNum)))
        PANIC;

    dsigCtx = xmlSecDSigCtxCreate(mngr);

    /* sign the template */
    if (xmlSecDSigCtxSign(dsigCtx, signNode) < 0) {
        fprintf(stderr, "Error: signature failed\n");
    } else {
        res = 0;
    }

    if (dsigCtx != NULL) {
        xmlSecDSigCtxDestroy(dsigCtx);
        dsigCtx = NULL;
    }

    return res;
}

int SAMLSignQuery(struct SHIBBOLETHSITE *ss, char *query, char *stop) {
    int res = 1;
    int rc;
    xmlSecDSigCtxPtr dsigCtx = NULL;
    int certNum = ss->certificates ? *ss->certificates : sslActiveIndex;
    xmlNodePtr signNode = NULL;
    xmlNodePtr refNode = NULL;
    xmlNodePtr keyInfoNode = NULL;
    xmlNodePtr x509DataNode = NULL;
    xmlSecKeysMngrPtr mngr = NULL;

    UUxmlSecInit();

    strcat(query, "&SigAlg=");
    AddEncodedField(query, (char *)ss->redirectSignatureMethod->href, stop);

    if (!(mngr = SAMLGetKeysMngr(certNum)))
        PANIC;

    dsigCtx = xmlSecDSigCtxCreate(mngr);
    dsigCtx->operation = xmlSecTransformOperationSign;
    dsigCtx->signMethod =
        xmlSecTransformCtxCreateAndAppend(&(dsigCtx->transformCtx), ss->redirectSignatureMethod);
    if (dsigCtx->signMethod == NULL)
        PANIC;
    dsigCtx->signKey = xmlSecKeysMngrFindKey(mngr, GENERIC_KEY_NAME, &(dsigCtx->keyInfoReadCtx));
    dsigCtx->signMethod->operation = dsigCtx->operation;
    xmlSecTransformSetKeyReq(dsigCtx->signMethod, &(dsigCtx->keyInfoReadCtx.keyReq));

    rc = xmlSecKeyMatch(dsigCtx->signKey, NULL, &(dsigCtx->keyInfoReadCtx.keyReq));
    if (rc != 1) {
        Log("Invalid key type");
        goto Failed;
    }

    rc = xmlSecTransformSetKey(dsigCtx->signMethod, dsigCtx->signKey);
    if (rc < 0) {
        Log("Unable to set key");
        goto Failed;
    }

    dsigCtx->transformCtx.result = NULL;
    dsigCtx->transformCtx.status = xmlSecTransformStatusNone;

    rc = xmlSecTransformCtxBinaryExecute(&(dsigCtx->transformCtx), query, strlen(query));
    if (rc < 0) {
        Log("Transform failed");
        goto Failed;
    }

    dsigCtx->result = dsigCtx->transformCtx.result;
    strcat(query, "&Signature=");
    char *sigData = xmlSecBufferGetData(dsigCtx->result);
    size_t sigSize = xmlSecBufferGetSize(dsigCtx->result);
    char *sigBase64 = Encode64Binary(NULL, sigData, sigSize);
    AddEncodedField(query, sigBase64, stop);
    FreeThenNull(sigBase64);
    res = 0;

Failed:
    if (dsigCtx != NULL) {
        xmlSecDSigCtxDestroy(dsigCtx);
        dsigCtx = NULL;
    }

    return res;
}

void SAMLAuthnRequest(struct TRANSLATE *t,
                      xmlChar *location,
                      char *relayState,
                      struct SHIBBOLETHSITE *ss,
                      BOOL redirect) {
    const xmlChar *authnRequest = "AuthnRequest";
    struct UUSOCKET *s = &t->ssocket;
    time_t now;
    char id[SAMLIDMAX];
    xmlTextWriterPtr writer = NULL;
    char *encode64 = NULL;
    char *bufferEnd = t->buffer + sizeof(t->buffer) - 100;
    xmlDocPtr doc = NULL;
    int len;
    xmlChar *buffer = NULL;
    char *query;

    if (!(writer = xmlNewTextWriterDoc(&doc, 0)))
        PANIC;

    // if (xmlTextWriterStartDocument(writer, NULL, "UTF-8", NULL) < 0) PANIC;

    /* Start root element */
    if (xmlTextWriterStartElementNS(writer, BAD_CAST "samlp", authnRequest, NSSAMLP2) < 0)
        PANIC;
    // if (xmlTextWriterWriteAttribute(writer, BAD_CAST "AssertionConsumerServiceIndex", BAD_CAST
    // "1") < 0) PANIC;

    if (t->session && t->session->reloginRequired)
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "ForceAuthn", BAD_CAST "true") < 0)
            PANIC;

    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Destination", location) < 0)
        PANIC;
    UUtime(&now);
    SAMLID(id, now);

    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "ID", BAD_CAST id) < 0)
        PANIC;
    SAMLAddIssueInstant(writer, now);
    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Version", BAD_CAST "2.0") < 0)
        PANIC;

    if (xmlTextWriterStartElementNS(writer, BAD_CAST "saml", BAD_CAST "Issuer", NSSAML2) < 0)
        PANIC;
    if (xmlTextWriterWriteString(writer, BAD_CAST ss->entityID) < 0)
        PANIC;
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    if (xmlTextWriterStartElement(writer, BAD_CAST "samlp:NameIDPolicy") < 0)
        PANIC;
    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "AllowCreate", BAD_CAST "1") < 0)
        PANIC;
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    if (ss->authnContextClassRef) {
        if (xmlTextWriterStartElement(writer, BAD_CAST "samlp:RequestedAuthnContext") < 0)
            PANIC;
        if (xmlTextWriterStartElementNS(writer, BAD_CAST "saml", BAD_CAST "AuthnContextClassRef",
                                        NSSAML2) < 0)
            PANIC;
        if (xmlTextWriterWriteString(writer, BAD_CAST ss->authnContextClassRef) < 0)
            PANIC;
        // Close saml:AuthnContextClassRef element
        if (xmlTextWriterEndElement(writer) < 0)
            PANIC;
        // Close samlp:RequestedAuthnContext element
        if (xmlTextWriterEndElement(writer) < 0)
            PANIC;
    }

    /* Close root element */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    if (xmlTextWriterEndDocument(writer) < 0)
        PANIC;

    if (xmlTextWriterFlush(writer) < 0)
        PANIC;

    if (ss->signAuthnRequest && !redirect) {
        SAMLAddIDAttr(xmlDocGetRootElement(doc), BAD_CAST "ID", authnRequest, NSSAMLP2);
        SAMLSignDoc(ss, doc, id);
    }

    xmlDocDumpFormatMemory(doc, &buffer, &len, 0);
    if (buffer == NULL)
        PANIC;

    if (optionLogSAML || (debugLevel > SAMLDebugThreshold_High)) {
        Log(AT "SAMLAuthnRequest to '%s':", location);
        LogXMLDoc(doc, NULL);
    }

    if (redirect) {
        encode64 = SAMLEncode(buffer);
        sprintf(t->buffer, "%s?", location);
        // The fields must be appended in SAMLRequest then RelayState sequence.
        // See Bindings for the OASIS SAML V2.0 section 3.4.4.1 for details.
        query = AtEnd(t->buffer);
        strcat(t->buffer, "SAMLRequest=");
        AddEncodedField(t->buffer, encode64, bufferEnd);
        if (relayState && *relayState) {
            strcat(t->buffer, "&RelayState=");
            AddEncodedField(t->buffer, relayState, bufferEnd);
        }
        if (ss->signAuthnRequest) {
            SAMLSignQuery(ss, query, bufferEnd);
        }
        SimpleRedirect(t, t->buffer, 0);
    } else {
        encode64 = Encode64(NULL, (char *)buffer);

        HTMLHeader(t, htmlHeaderOmitCache);

        UUSendZ(s, TAG_DOCTYPE_HTML_HEAD
                "\
<title>Shibboleth Authentication Request</title></head>\
<body onload='EZproxyCheckBack()'>\
<form name='EZproxyForm' method='POST' action='");
        SendQuotedURL(t, (char *)location);
        UUSendZ(s, "'>");
        if (relayState && *relayState) {
            UUSendZ(s, "<input type='hidden' name='RelayState' value='");
            SendQuotedURL(t, relayState);
            UUSendZ(s, "'>\n");
        }

        UUSendZ(s, "<input type='hidden' name='SAMLRequest' value='");
        SendQuotedURL(t, encode64);
        UUSendZ(s, "'>\n");
        UUSendZ(s,
                "If your browser does not continue automatically, click <input type='submit' "
                "value='here'>");
        UUSendZ(s,
                "\r\n\
</form>\
<form name='EZproxyTrack'><input type='hidden' name='back' value='1'>\
<script language='JavaScript'>\r\n\
<!--\r\n\
function EZproxyCheckBack() {\
  var goForward = (document.EZproxyTrack.back.value==1);\
  document.EZproxyTrack.back.value=2;\
  document.EZproxyTrack.back.defaultValue=2;\
  if (goForward) { document.EZproxyForm.submit(); }\
}\r\n\
-->\r\n\
</script></body></html>\r\n");
    }

    FreeThenNull(encode64);
    xmlFreeThenNull(buffer);

    if (writer) {
        xmlFreeTextWriter(writer);
        writer = NULL;
    }

    if (doc) {
        xmlFreeDoc(doc);
        doc = NULL;
    }
}

xmlChar *SAMLSSOLocation(char *entityID,
                         struct SHIBBOLETHSITE **ss,
                         enum SHIBBOLETHVERSION shibVersion,
                         BOOL *redirect,
                         enum SHIBBOLETHVERSION *foundVersion) {
    xmlChar *location = NULL;

    if (entityID == NULL)
        return NULL;

    if (redirect)
        *redirect = TRUE;

    if (foundVersion)
        *foundVersion = 0;

    if (shibVersion == SHIBVERSION13OR20 || shibVersion == SHIBVERSION20) {
        location = SAMLShibbolethMetadataValue(
            "//md:EntityDescriptor[@entityID = "
            "'%s']/md:IDPSSODescriptor/md:SingleSignOnService[@Binding = "
            "'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST']/@Location",
            entityID, "", ss, FALSE);
        if (location) {
            if (redirect)
                *redirect = FALSE;
        } else {
            location = SAMLShibbolethMetadataValue(
                "//md:EntityDescriptor[@entityID = "
                "'%s']/md:IDPSSODescriptor/md:SingleSignOnService[@Binding = "
                "'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect']/@Location",
                entityID, "", ss, FALSE);
        }
        if (foundVersion && location)
            *foundVersion = SHIBVERSION20;
    }

    if (location == NULL && (shibVersion == SHIBVERSION13OR20 || shibVersion == SHIBVERSION13)) {
        location = SAMLShibbolethMetadataValue(
            "//md:EntityDescriptor[@entityID = "
            "'%s']/md:IDPSSODescriptor/md:SingleSignOnService[@Binding = "
            "'urn:mace:shibboleth:1.0:profiles:AuthnRequest']/@Location",
            entityID, "", ss, FALSE);
        if (redirect)
            *redirect = TRUE;
        if (foundVersion && location)
            *foundVersion = SHIBVERSION13;
    }

    if (location == NULL) {
        Log(AT "unable to locate SSO Location for '%s'", entityID);
    }

    return location;
}

xmlChar *SAMLSLOLocation(char *entityID,
                         struct SHIBBOLETHSITE **ss,
                         enum SHIBBOLETHVERSION shibVersion,
                         BOOL *redirect,
                         enum SHIBBOLETHVERSION *foundVersion) {
    xmlChar *location = NULL;

    if (entityID == NULL)
        return NULL;

    if (redirect)
        *redirect = TRUE;

    if (foundVersion)
        *foundVersion = 0;

    if (shibVersion == SHIBVERSION13OR20 || shibVersion == SHIBVERSION20) {
        location = SAMLShibbolethMetadataValue(
            "//md:EntityDescriptor[@entityID = "
            "'%s']/md:IDPSSODescriptor/md:SingleLogoutService[@Binding = "
            "'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect']/@Location",
            entityID, "", ss, FALSE);
        if (!location) {
            location = SAMLShibbolethMetadataValue(
                "//md:EntityDescriptor[@entityID = "
                "'%s']/md:IDPSSODescriptor/md:SingleLogoutService[@Binding = "
                "'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST']/@Location",
                entityID, "", ss, FALSE);
            if (location) {
                if (redirect)
                    *redirect = FALSE;
            }
        }
        if (foundVersion && location)
            *foundVersion = SHIBVERSION20;
    }

    if (location == NULL) {
        Log(AT "unable to locate SLO Location for '%s'", entityID);
    }

    return location;
}

BOOL SAMLLogoutRequest(struct TRANSLATE *t,
                       char *issuer,
                       char *nameID,
                       char *nameIDFormat,
                       char *nameIDNameQualifier,
                       char *nameIDSPNameQualifier,
                       char *sessionIndex) {
    const xmlChar *logoutRequest = "LogoutRequest";
    struct UUSOCKET *s = &t->ssocket;
    time_t now;
    char id[SAMLIDMAX];
    xmlTextWriterPtr writer = NULL;
    char *encode64 = NULL;
    char *bufferEnd = t->buffer + sizeof(t->buffer) - 100;
    xmlDocPtr doc = NULL;
    int len;
    xmlChar *buffer = NULL;
    char *location = NULL;
    struct SHIBBOLETHSITE *ss;
    BOOL redirect;
    BOOL handled = FALSE;
    char *query;

    if (issuer == NULL || nameID == NULL) {
        goto Finished;
    }

    if (!(location = SAMLSLOLocation(issuer, &ss, SHIBVERSION20, &redirect, NULL))) {
        goto Finished;
    }

    if (!(writer = xmlNewTextWriterDoc(&doc, 0)))
        PANIC;

    // if (xmlTextWriterStartDocument(writer, NULL, "UTF-8", NULL) < 0) PANIC;

    /* Start root element */
    if (xmlTextWriterStartElementNS(writer, BAD_CAST "samlp", logoutRequest, NSSAMLP2) < 0)
        PANIC;
    // if (xmlTextWriterWriteAttribute(writer, BAD_CAST "AssertionConsumerServiceIndex", BAD_CAST
    // "1") < 0) PANIC;

    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Destination", location) < 0)
        PANIC;
    UUtime(&now);
    SAMLID(id, now);

    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "ID", BAD_CAST id) < 0)
        PANIC;
    SAMLAddIssueInstant(writer, now);
    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Version", BAD_CAST "2.0") < 0)
        PANIC;

    if (xmlTextWriterStartElementNS(writer, BAD_CAST "saml", BAD_CAST "Issuer", NSSAML2) < 0)
        PANIC;
    if (xmlTextWriterWriteString(writer, BAD_CAST ss->entityID) < 0)
        PANIC;
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    if (xmlTextWriterStartElementNS(writer, BAD_CAST "saml", BAD_CAST "NameID", NSSAML2) < 0)
        PANIC;
    if (nameIDFormat) {
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Format", BAD_CAST nameIDFormat) < 0)
            PANIC;
    }
    if (nameIDNameQualifier) {
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "NameQualifier",
                                        BAD_CAST nameIDNameQualifier) < 0)
            PANIC;
    }
    if (nameIDSPNameQualifier) {
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "SPNameQualifier",
                                        BAD_CAST nameIDSPNameQualifier) < 0)
            PANIC;
    }
    if (xmlTextWriterWriteString(writer, BAD_CAST nameID) < 0)
        PANIC;
    /* Close NameID */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    if (sessionIndex) {
        if (xmlTextWriterStartElementNS(writer, BAD_CAST "samlp", BAD_CAST "SessionIndex",
                                        NSSAMLP2) < 0)
            PANIC;
        if (xmlTextWriterWriteString(writer, BAD_CAST sessionIndex) < 0)
            PANIC;
        /* Close SessionIndex */
        if (xmlTextWriterEndElement(writer) < 0)
            PANIC;
    }

    /* Close root element */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    if (xmlTextWriterEndDocument(writer) < 0)
        PANIC;

    if (xmlTextWriterFlush(writer) < 0)
        PANIC;

    SAMLAddIDAttr(xmlDocGetRootElement(doc), BAD_CAST "ID", logoutRequest, NSSAMLP2);
    if (!redirect) {
        SAMLSignDoc(ss, doc, id);
    }

    xmlDocDumpFormatMemory(doc, &buffer, &len, 0);
    if (buffer == NULL)
        PANIC;

    if (optionLogSAML || (debugLevel > SAMLDebugThreshold_High)) {
        Log(AT "SAMLLogoutRequest to '%s':", location);
        LogXMLDoc(doc, NULL);
    }

    if (redirect) {
        encode64 = SAMLEncode(buffer);
        sprintf(t->buffer, "%s?", location);
        query = AtEnd(t->buffer);
        strcat(query, "SAMLRequest=");
        AddEncodedField(t->buffer, encode64, bufferEnd);
        SAMLSignQuery(ss, query, bufferEnd);
        SimpleRedirect(t, t->buffer, 0);
    } else {
        encode64 = Encode64(NULL, (char *)buffer);

        HTMLHeader(t, htmlHeaderOmitCache);

        UUSendZ(s, TAG_DOCTYPE_HTML_HEAD
                "\
<title>Shibboleth Logout Request</title></head>\
<body onload='EZproxyCheckBack()'>\
<form name='EZproxyForm' method='POST' action='");
        SendQuotedURL(t, (char *)location);
        UUSendZ(s, "'>");
        UUSendZ(s, "<input type='hidden' name='SAMLRequest' value='");
        SendQuotedURL(t, encode64);
        UUSendZ(s, "'>\n");
        UUSendZ(s,
                "If your browser does not continue automatically, click <input type='submit' "
                "value='here'>");
        UUSendZ(s,
                "\r\n\
</form>\
<form name='EZproxyTrack'><input type='hidden' name='back' value='1'>\
<script language='JavaScript'>\r\n\
<!--\r\n\
function EZproxyCheckBack() {\
  var goForward = (document.EZproxyTrack.back.value==1);\
  document.EZproxyTrack.back.value=2;\
  document.EZproxyTrack.back.defaultValue=2;\
  if (goForward) { document.EZproxyForm.submit(); }\
}\r\n\
-->\r\n\
</script></body></html>\r\n");
    }

    handled = TRUE;

Finished:
    FreeThenNull(encode64);
    xmlFreeThenNull(buffer);

    if (writer) {
        xmlFreeTextWriter(writer);
        writer = NULL;
    }

    if (doc) {
        xmlFreeDoc(doc);
        doc = NULL;
    }

    return handled;
}

void SAMLLogoutResponse(struct TRANSLATE *t,
                        xmlChar *nameID,
                        xmlChar *sessionIndex,
                        xmlChar *location,
                        struct SHIBBOLETHSITE *ss,
                        BOOL redirect) {
    struct SESSION *e;
    int i;
    const xmlChar *logoutResponse = "LogoutResponse";
    struct UUSOCKET *s = &t->ssocket;
    time_t now;
    char id[SAMLIDMAX];
    xmlTextWriterPtr writer = NULL;
    char *encode64 = NULL;
    char *bufferEnd = t->buffer + sizeof(t->buffer) - 100;
    xmlDocPtr doc = NULL;
    int len;
    xmlChar *buffer = NULL;
    char *status = "urn:oasis:names:tc:SAML:2.0:status:UnknownPrincipal";
    char *compNameID = NULL;
    char *compSessionIndex = NULL;
    BOOL match = FALSE;

    if (nameID && sessionIndex) {
        for (i = 0, e = sessions; i < cSessions; i++, e++) {
            if (e->active != 1) {
                continue;
            }

            compNameID = VariablesGetValue(e->sessionVariables, SESSION_VARIABLE_SAML_NAMEID);
            compSessionIndex =
                VariablesGetValue(e->sessionVariables, SESSION_VARIABLE_SAML_SESSION_INDEX);
            match = compNameID && compSessionIndex && strcmp((char *)nameID, compNameID) == 0 &&
                    strcmp((char *)sessionIndex, compSessionIndex) == 0;
            FreeThenNull(compNameID);
            FreeThenNull(compSessionIndex);

            if (match) {
                StopSession(e, "Shibboleth SLO");
                status = "urn:oasis:names:tc:SAML:2.0:status:Success";
                break;
            }
        }
    }

    if (!(writer = xmlNewTextWriterDoc(&doc, 0)))
        PANIC;

    /* Start root element */
    if (xmlTextWriterStartElementNS(writer, BAD_CAST "samlp", logoutResponse, NSSAMLP2) < 0)
        PANIC;
    // if (xmlTextWriterWriteAttribute(writer, BAD_CAST "AssertionConsumerServiceIndex", BAD_CAST
    // "1") < 0) PANIC;

    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Destination", location) < 0)
        PANIC;
    UUtime(&now);
    SAMLID(id, now);

    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "ID", BAD_CAST id) < 0)
        PANIC;
    SAMLAddIssueInstant(writer, now);
    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Version", BAD_CAST "2.0") < 0)
        PANIC;

    if (xmlTextWriterStartElementNS(writer, BAD_CAST "saml", BAD_CAST "Issuer", NSSAML2) < 0)
        PANIC;
    if (xmlTextWriterWriteString(writer, BAD_CAST ss->entityID) < 0)
        PANIC;
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    if (xmlTextWriterStartElementNS(writer, BAD_CAST "samlp", BAD_CAST "Status", NSSAMLP2) < 0)
        PANIC;
    if (xmlTextWriterStartElementNS(writer, BAD_CAST "samlp", BAD_CAST "StatusCode", NSSAMLP2) < 0)
        PANIC;
    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Value", BAD_CAST status) < 0)
        PANIC;
    /* Close StatusCode */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;
    /* Close Status */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    /* Close root element */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    if (xmlTextWriterEndDocument(writer) < 0)
        PANIC;

    if (xmlTextWriterFlush(writer) < 0)
        PANIC;

    SAMLAddIDAttr(xmlDocGetRootElement(doc), BAD_CAST "ID", logoutResponse, NSSAMLP2);
    SAMLSignDoc(ss, doc, id);

    xmlDocDumpFormatMemory(doc, &buffer, &len, 0);
    if (buffer == NULL)
        PANIC;

    if (optionLogSAML || (debugLevel > SAMLDebugThreshold_High)) {
        Log(AT "SAMLLogoutRequest to '%s':", location);
        LogXMLDoc(doc, NULL);
    }

    if (redirect) {
        encode64 = SAMLEncode(buffer);
        sprintf(t->buffer, "%s?SAMLRequest=", location);
        AddEncodedField(t->buffer, encode64, bufferEnd);
        SimpleRedirect(t, t->buffer, 0);
    } else {
        encode64 = Encode64(NULL, (char *)buffer);

        HTMLHeader(t, htmlHeaderOmitCache);

        UUSendZ(s, TAG_DOCTYPE_HTML_HEAD
                "\
<title>Shibboleth Logout Request</title></head>\
<body onload='EZproxyCheckBack()'>\
<form name='EZproxyForm' method='POST' action='");
        SendQuotedURL(t, (char *)location);
        UUSendZ(s, "'>");
        UUSendZ(s, "<input type='hidden' name='SAMLRequest' value='");
        SendQuotedURL(t, encode64);
        UUSendZ(s, "'>\n");
        UUSendZ(s,
                "If your browser does not continue automatically, click <input type='submit' "
                "value='here'>");
        UUSendZ(s,
                "\r\n\
</form>\
<form name='EZproxyTrack'><input type='hidden' name='back' value='1'>\
<script language='JavaScript'>\r\n\
<!--\r\n\
function EZproxyCheckBack() {\
  var goForward = (document.EZproxyTrack.back.value==1);\
  document.EZproxyTrack.back.value=2;\
  document.EZproxyTrack.back.defaultValue=2;\
  if (goForward) { document.EZproxyForm.submit(); }\
}\r\n\
-->\r\n\
</script></body></html>\r\n");
    }

    FreeThenNull(encode64);
    xmlFreeThenNull(buffer);

    if (writer) {
        xmlFreeTextWriter(writer);
        writer = NULL;
    }

    if (doc) {
        xmlFreeDoc(doc);
        doc = NULL;
    }
}

static void SAMLLogout(struct TRANSLATE *t,
                       struct SAMLCONTEXT *samlCtx,
                       BOOL redirect,
                       char *request,
                       char *response) {
    BOOL failed = TRUE;
    char *rr = request ? request : response;
    char *saml = NULL;
    xmlDocPtr doc = NULL;
    xmlXPathContextPtr xPathCtx = NULL;
    char *issuer = NULL;
    struct SHIBBOLETHSITE *ss;
    BOOL sloRedirect;
    enum SHIBBOLETHVERSION foundVersion;
    xmlChar *slo;
    xmlChar *nameID = NULL;
    xmlChar *sessionIndex = NULL;
    char *statusCode = NULL;
    char *shortStatusCode;

    // For a redirect, the content will be deflated, but for a POST, it will not.
    saml = SAMLDecode(request ? request : response, redirect);
    if (saml) {
        doc = xmlParseMemory(saml, strlen(saml));
    }

    if ((doc == NULL) || (xmlDocGetRootElement(doc) == NULL)) {
        if (optionLogSAML) {
            Log(AT "Logout request failure");
            LogXMLDoc(NULL, BAD_CAST saml);
        }
        Log(AT "Unable to parse SAML Logout XML");
        goto Finished;
    }

    if (optionLogSAML) {
        Log(AT "Logout document");
        LogXMLDoc(doc, NULL);
    }

    if (!(xPathCtx = SAMLxmlXPathNewContext(doc, SHIBVERSION20))) {
        Log(AT "Unable to SAMLxmlXPathNewContext");
        goto Finished;
    }

    if (request) {
        issuer = (char *)UUxmlSimpleGetContent(NULL, xPathCtx, "/samlp:LogoutRequest/saml:Issuer");
    } else if (response) {
        issuer = (char *)UUxmlSimpleGetContent(NULL, xPathCtx, "/samlp:LogoutResponse/saml:Issuer");
    }

    if (issuer == NULL) {
        Log("Unable to locate issuer");
        goto Finished;
    }

    slo = SAMLSLOLocation(issuer, &ss, SHIBVERSION20, &sloRedirect, &foundVersion);

    if (slo == NULL) {
        Log("Unable to locate SingleLogout location for issuer");
        goto Finished;
    }

    if (!ss->slo) {
        Log("Shibboleth SLO not enabled");
        goto Finished;
    }

    samlCtx->shibbolethSite = ss;
    SAMLDecryptNodes(xmlDocGetRootElement(doc), samlCtx);

    if (optionLogSAML) {
        Log(AT "Logout decrypted document");
        LogXMLDoc(doc, NULL);
    }

    if (request) {
        nameID = UUxmlSimpleGetContent(NULL, xPathCtx, "/samlp:LogoutRequest//saml:NameID");
        sessionIndex =
            UUxmlSimpleGetContent(NULL, xPathCtx, "/samlp:LogoutRequest//samlp:SessionIndex");
        SAMLLogoutResponse(t, nameID, sessionIndex, slo, ss, sloRedirect);
        failed = FALSE;
    } else {
        failed = FALSE;
        statusCode = (char *)UUxmlSimpleGetContent(
            NULL, xPathCtx, "/samlp:LogoutResponse//samlp:Status/samlp:StatusCode/@Value");

        if ((shortStatusCode = strrchr(statusCode, ':'))) {
            shortStatusCode++;
        } else {
            shortStatusCode = statusCode;
        }

        HTMLHeader(t, htmlHeaderNoCache);
        VariablesSetOrDeleteValue(t->localVariables, "saml:statusCode", statusCode);
        VariablesSetOrDeleteValue(t->localVariables, "saml:shortStatusCode", shortStatusCode);

        if (strcmp(statusCode, "urn:oasis:names:tc:SAML:2.0:status:Success") == 0) {
            VariablesSetOrDeleteValue(t->localVariables, "saml:success", "1");
        } else {
            VariablesSetOrDeleteValue(t->localVariables, "saml:success", "0");
        }

        if (SendEditedFile(t, NULL, SHIBLOGOUTHTM, 0, "", 0, NULL) != 0) {
            SendFile(t, LOGOUTHTM, SFREPORTIFMISSING);
        }
    }

Finished:
    if (xPathCtx) {
        xmlXPathFreeContext(xPathCtx);
        xPathCtx = NULL;
    }

    if (doc) {
        xmlFreeDoc(doc);
        doc = NULL;
    }

    FreeThenNull(statusCode);
    FreeThenNull(nameID);
    FreeThenNull(sessionIndex);
    FreeThenNull(issuer);
    FreeThenNull(saml);
    xmlFreeThenNull(nameID);

    if (failed) {
        SendShibFailure(t);
    }
}

BOOL AdminShibbolethUnavailable(struct TRANSLATE *t,
                                enum SHIBBOLETHAVAILABLE shibbolethAvailableValue,
                                char *version) {
    if (shibbolethAvailableValue == SHIBBOLETHAVAILABLEON)
        return FALSE;

    HTMLHeader(t, htmlHeaderNoCache);

    WriteLine(&t->ssocket,
              TAG_DOCTYPE_HTML_HEAD
              "</head><body><p>Shibboleth %s is not enabled on this server.</p></body></html>\n",
              version ? version : "");

    return TRUE;
}

char *SAMLGetOrgnizationName(struct SHIBBOLETHSITE *ss, char *entityID) {
    xmlChar *name = NULL;
    char buffer[SAMLMetadataXPathBufferSize];

    name = UUxmlSimpleGetContentSnprintf(NULL, ss->xPathCtx, buffer, SAMLMetadataXPathBufferSize,
                                         EntityDescriptorSPrefixFormatStr EntityOrganization
                                         "/md:OrganizationDisplayName",
                                         entityID);
    if (name == NULL) {
        name = UUxmlSimpleGetContentSnprintf(
            NULL, ss->xPathCtx, buffer, SAMLMetadataXPathBufferSize,
            EntityDescriptorSPrefixFormatStr EntityOrganization "/md:OrganizationName", entityID);
        if (name == NULL) {
            if (!(name = (xmlChar *)strdup(entityID)))
                PANIC;
        }
    }

    return (char *)name;
}

void AdminShibbolethSSODS(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    const int ASSAMLDS = 0;
    const int ASTARGET = 1;
    const int ASENTITYID = 2;
    struct FORMFIELD ffs[] = {
        {"SAMLDS",   NULL, 0, 0, 0},
        {"target",   NULL, 0, 0, 0},
        {"entityID", NULL, 0, 0, 0},
        {NULL,       NULL, 0, 0, 0}
    };
    char *target;
    xmlChar *location = NULL;
    struct SHIBBOLETHSITE *ss;
    BOOL redirect;
    enum SHIBBOLETHVERSION shibVersion;
    UNUSED(ASSAMLDS);

    if (shibbolethAvailable13 != SHIBBOLETHAVAILABLEON &&
        shibbolethAvailable20 != SHIBBOLETHAVAILABLEON) {
        AdminShibbolethUnavailable(t, SHIBBOLETHAVAILABLEOFF, "1.3/2.0");
        return;
    }

    FindFormFields(query, post, ffs);

    target = ffs[ASTARGET].value;

    location =
        SAMLSSOLocation(ffs[ASENTITYID].value, &ss, SHIBVERSION13OR20, &redirect, &shibVersion);

    if (location) {
        if (shibVersion == SHIBVERSION13 && shibbolethAvailable13 == SHIBBOLETHAVAILABLEON) {
            HTTPCode(t, 302, 0);
            NoCacheHeaders(s);
            if (t->version) {
                UUSendZ(s, "Location: ");
                ShibbolethSendURLwithWAYF(s, target, (char *)location, ss->entityID, NULL);
                UUSendCRLF(s);
                HeaderToBody(t);
            }

            UUSendZ(s, TAG_DOCTYPE_HTML_HEAD);
            UUSendZ(s, "</head>\n<body>\n");
            UUSendZ(s, "To access this document, wait a moment or click <a href='");
            ShibbolethSendURLwithWAYF(s, "shibbolethattributes", (char *)location, ss->entityID,
                                      NULL);
            UUSendZ(s, "'>here</a> to continue\n");
            UUSendZ(s, "</body></html>\n");
            xmlFreeThenNull(location);
            return;
        }

        if (shibVersion == SHIBVERSION20 && shibbolethAvailable20 == SHIBBOLETHAVAILABLEON) {
            SAMLAuthnRequest(t, BAD_CAST location, target, ss, redirect);
            xmlFreeThenNull(location);
            return;
        }
    }

    HTMLErrorPageHeader(t, "Not found");
    if (ffs[ASENTITYID].value == NULL)
        UUSendZ(s, "<p>Required entityID missing</p>\n");
    else {
        UUSendZ(s, "<p>No metadata for  ");
        SendHTMLEncoded(s, ffs[ASENTITYID].value);
        UUSendZ(s, "</p>\n");
    }
    HTMLErrorPageFooter(t);

    xmlFreeThenNull(location);
}

static BOOL SAMLDecodeArtifact(struct FORMFIELD *samlArtifactField, struct SAMLARTIFACT *sa) {
    unsigned char *samlArtifactBinary = NULL;
    size_t remain;
    unsigned char *p;
    struct SHIBBOLETHSITE *ss;
    xmlXPathObjectPtr xPathObj = NULL;
    xmlNodeSetPtr nodes = NULL;
    EVP_MD_CTX *mdctx = NULL;

    memset(sa, 0, sizeof(*sa));

    if (samlArtifactField->value == NULL || *samlArtifactField->value == 0)
        goto Finished;

    p = samlArtifactBinary = Decode64Binary(NULL, &remain, samlArtifactField->value);
    if (samlArtifactBinary == NULL || remain < 2)
        goto Finished;

    sa->typeCode = ntohs(*((unsigned short *)p));

    p += 2;
    remain -= 2;

    switch (sa->typeCode) {
    case 4:
        if (remain < 2)
            goto Finished;
        sa->endPointIndex = ntohs(*((unsigned short *)p));
        p += 2;
        remain -= 2;
        /* and now flow into type 1 */

    case 1:
        sa->sourceIdLen = 20;
        sa->messageHandleLen = 20;
        if (remain != (int)(sa->sourceIdLen + sa->messageHandleLen))
            goto Finished;

        memcpy(sa->sourceId, p, sa->sourceIdLen);
        memcpy(sa->messageHandle, p + sa->sourceIdLen, sa->messageHandleLen);

        sa->valid = TRUE;
    }

    if (!sa->valid)
        goto Finished;

    for (ss = shibbolethSites; ss != NULL && sa->entityID == NULL; ss = ss->next) {
        if (LoadUpdateShibbolethMetadata(ss, FALSE) == 0)
            continue;

        UUAcquireMutex(&ss->mutex);

        if (xPathObj =
                xmlXPathEvalExpression(BAD_CAST "//md:EntityDescriptor/@entityID", ss->xPathCtx)) {
            if (nodes = xPathObj->nodesetval) {
                int i;
                for (i = 0; i < nodes->nodeNr; i++) {
                    xmlChar *value = xmlNodeGetContent(nodes->nodeTab[i]);
                    if (value) {
                        unsigned char md_value[EVP_MAX_MD_SIZE];
                        unsigned int md_len;

                        if (!(mdctx = EVP_MD_CTX_new()))
                            PANIC;
                        EVP_DigestInit(mdctx, EVP_sha1());
                        EVP_DigestUpdate(mdctx, value, strlen((char *)value));
                        EVP_DigestFinal(mdctx, md_value, &md_len);

                        if (md_len == sa->sourceIdLen &&
                            memcmp(sa->sourceId, md_value, md_len) == 0) {
                            sa->entityID = value;
                            value = NULL;
                            sa->shibbolethSite = ss;
                            Log(AT "Found match to entity '%s'", sa->entityID);
                            break;
                        }
                        xmlFreeThenNull(value);
                    }
                }
            }
            xmlXPathFreeObject(xPathObj);
            xPathObj = NULL;
        }

        UUReleaseMutex(&ss->mutex);
    }

Finished:
    if (mdctx) {
        EVP_MD_CTX_free(mdctx);
        mdctx = NULL;
    }
    FreeThenNull(samlArtifactBinary);

    return sa->valid;
}

static void AdminShibbolethSSOSAMLHandler(struct TRANSLATE *t,
                                          char *query,
                                          char *post,
                                          enum SHIBBOLETHVERSION shibVersion) {
    struct UUSOCKET *s = &t->ssocket;
    const int ASSAMLRESPONSE = 0;
    const int ASSAMLDS = 1;
    const int ASTARGET = 2;
    const int ASENTITYID = 3;
    const int ASRELAYSTATE = 4;
    const int ASSAMLART = 5;
    const int ASSAMLREQUEST = 6;
    struct FORMFIELD ffs[] = {
        {"SAMLResponse", NULL, 0, 0, 0, FORMFIELDNOTRIMCTRL},
        {"SAMLDS", NULL, 0, 0, 0},
        {"target", NULL, 0, 0, 0},
        {"entityID", NULL, 0, 0, 0},
        {"RelayState", NULL, 0, 0, 0},
        {"SAMLart", NULL, 0, 0, 0, FORMFIELDALLOWMULTIVALUES | FORMFIELDNOTRIMCTRL},
        {"SAMLRequest", NULL, 0, 0, 0, FORMFIELDNOTRIMCTRL},
        {NULL, NULL, 0, 0, 0}
    };
    struct USRDIRECTIVE udc[] = {
        {"MsgAuth", SAMLMsgAuth, 0},
        {NULL,      NULL,        0}
    };

    struct USERINFO ui;
    struct SESSION *e;
    struct FILEREADLINEBUFFER frb;
    struct FILEINCLUSION fileIncludeNodeNewTopOfStack;
    enum FINDUSERRESULT result;
    char *samlResponse = NULL;
    char *relayState = NULL;
    xmlChar *location = NULL;
    struct SAMLARTIFACT samlArtifact;
    char *handler;
    BOOL handlerPost = FALSE;
    BOOL handlerArtifact = FALSE;
    BOOL showAttributes;
    struct SAMLCONTEXT samlCtxBuffer, *samlCtx;
    int shibUsrFile = -1;
    GROUPMASK gmAuto = NULL;
    char *url = NULL;
    char phase;
    BOOL res;
    UNUSED(ASSAMLDS);
    UNUSED(ASENTITYID);

    FindFormFields(query, post, ffs);

    if ((relayState = ffs[ASRELAYSTATE].value) == NULL)
        relayState = ffs[ASTARGET].value;

    InitUserInfo(t, &ui, "shibboleth", NULL, NULL, NULL);
    samlCtx = &samlCtxBuffer;
    memset(samlCtx, 0, sizeof(*samlCtx));

    samlCtx->shibbolethVersion = shibVersion;

    /* Shibboleth availability should have been verified before we ever got here! */

    if (handler = StartsIWith(t->urlCopy, "/Shibboleth.sso/SAML")) {
        // SLO is only supported in Shibboleth 2.0 or later
        if (StartsIWith(handler, "2/SLO/POST")) {
            SAMLLogout(t, samlCtx, FALSE, ffs[ASSAMLREQUEST].value, ffs[ASSAMLRESPONSE].value);
            goto Finished;
        } else if (StartsIWith(handler, "2/SLO/Redirect")) {
            SAMLLogout(t, samlCtx, TRUE, ffs[ASSAMLREQUEST].value, ffs[ASSAMLRESPONSE].value);
            goto Finished;
        }

        if (*handler == '2')
            handler++;

        if (StartsIWith(handler, "/POST")) {
            handlerPost = 1;
        } else if (StartsIWith(handler, "/Artifact")) {
            handlerArtifact = 1;
        } else {
            HTTPCode(t, 404, 1);
            goto Finished;
        }
    } else {
        HTTPCode(t, 404, 1);
        goto Finished;
    }

    t->finalStatus = 997;

    shibUsrFile = -2;
    if (!PushIncludeFileStack(&ui, &frb, SHIBNEWUSR, NULL, ui.fileInclusion,
                              &fileIncludeNodeNewTopOfStack, &shibUsrFile)) {
        if ((shibUsrFile == -1) && (errno != ENOENT)) {
            Log(AT "Shibboleth disabled since " SHIBNEWUSR " exists but cannot be opened: %d",
                errno);
            HTTPCode(t, 500, 1);
            goto Finished;
        }
        if (shibUsrFile == -2) {
            /* failed to open the file for some other reason, see log */
            goto Finished;
        }
    } else {
        ui.fileInclusion = &fileIncludeNodeNewTopOfStack;
    }

    ui.urlReference = DecodeURLReference(relayState);

    showAttributes = relayState && strcmp(relayState, "shibbolethattributes") == 0;
    samlCtx->includeInvalidScope = showAttributes;

    SAMLDecodeArtifact(&ffs[ASSAMLART], &samlArtifact);

    if (samlArtifact.valid) {
        samlCtx->shibbolethSite = samlArtifact.shibbolethSite;
        samlCtx->responseIssuer = (char *)samlArtifact.entityID;
        res = (SAMLArtifactRequest(t, &ffs[ASSAMLART], samlArtifact.shibbolethSite,
                                   samlArtifact.entityID, samlCtx, showAttributes) == 0);
        if (res || showAttributes)
            goto Process;
        goto Finished;
    }

    if (ffs[ASSAMLRESPONSE].value) {
        samlResponse = Decode64(NULL, ffs[ASSAMLRESPONSE].value);
        samlCtx->responseDoc = xmlParseMemory(samlResponse, strlen(samlResponse));
        if ((samlCtx->responseDoc == NULL) ||
            (xmlDocGetRootElement(samlCtx->responseDoc) == NULL)) {
            if (optionLogSAML) {
                Log(AT "Response text %s:", SAML_VERSION_TEXT(samlCtx->shibbolethVersion));
                LogXMLDoc(NULL, BAD_CAST samlResponse);
            }
            Log(AT "Unable to parse SAML XML");
            goto Finished;
        }
        if (optionLogSAML) {
            Log(AT "Response document %s:", SAML_VERSION_TEXT(samlCtx->shibbolethVersion));
            LogXMLDoc(samlCtx->responseDoc, NULL);
        }
        samlCtx->xPathCtx =
            SAMLxmlXPathNewContext(samlCtx->responseDoc, samlCtx->shibbolethVersion);
        if (samlCtx->xPathCtx == NULL) {
            Log(AT "Unable to SAMLxmlXPathNewContext");
            goto Finished;
        }
        samlCtx->counters.httpsGetMethod = FALSE;
        res = (SAMLResponseDecode(t, samlCtx) == 0);
        if (res || showAttributes)
            goto Process;
        goto Finished;
    } else {
        if (debugLevel > SAMLDebugThreshold_Low) {
            Log(AT "No Response asserted");
        }
        goto Finished;
    }

Process:
    if (showAttributes) {
        t->finalStatus = 200;
        if (t->session && (t->session)->priv)
            AdminBaseHeader(t, 0, "Show Shibboleth Attributes",
                            " | <a href='/shibboleth'>Manage Shibboleth</a>");
        else
            AdminBaseHeader(t, AHNOTOPMENU, NULL, NULL);

        SAMLShowAttributes(&t->ssocket, samlCtx);
        if (t->session && (t->session)->priv && (samlCtx->responseIssuer)) {
            UUSendZ(s, "<h2>Configuration for " EZPROXYUSR "</h2>\n");
            UUSendZ(s,
                    "<p>To configure EZproxy to use this identity provider, add the following "
                    "lines to " EZPROXYUSR ":</p><p><pre>\n::Shibboleth\nIDP");
            WriteLine(s, "%d ", samlCtx->shibbolethVersion);
            SendHTMLEncoded(s, PNS((char *)samlCtx->responseIssuer));
            UUSendZ(s, "\n/Shibboleth\n</pre></p>\n");
            AdminFooter(t, 0);
        }
        goto Finished;
    }

    if (!samlCtx->counters.statusCodeSuccess) {
        Log(AT "received response without a status of success, denying access");
        goto Finished;
    }

    ui.url = relayState;
    StrCpy3(ui.logUser, ui.user, sizeof(ui.logUser));

    GroupMaskDefault(ui.gm);
    if (shibUsrFile >= 0) {
        ui.flags = 0;
        /* authGetValue and/or authAggregateTest should always be assigned
           just before a call to UsrHandler or before the macro USRCOMMON1
           since both of these reset the authGetValue and authAggregateTest
           to NULL when the test completes to avoid having one of these functions
           called later during user.txt processing when the function may no
           longer match the authentication in effect
        */
        ui.authGetValue = SAMLAuthGetValue;
        ui.authAggregateTest = SAMLAggregateTest;
        result = UsrHandler(t, "Shibboleth", udc, samlCtx, &ui, SHIBNEWUSR, shibUsrFile, &frb, NULL,
                            NULL);
        if (ui.denied)
            goto Finished;
    }

    /* ForceValid: */
    e = t->session;
    if (ui.logUser == NULL || *ui.logUser == 0) {
        strcpy(ui.logUser, "shibboleth");
    }
    if (e == NULL) {
        e = StartSession(ui.logUser, ui.relogin, NULL);
        if (e == NULL) {
            HTTPCode(t, 503, 0);
            HTTPContentType(t, NULL, 1);
            UUSendZ(
                s, TAG_DOCTYPE_HTML_HEAD
                "</head><body>Maximum sessions reached, please try again later</body></html>\n");
            goto Finished;
        }
        /* GroupMaskOr needs to be here for AuditEvent, and below if user is merging two session */
        if (optionLoginReplaceGroups)
            GroupMaskCopy(e->gm, ui.gm);
        else
            GroupMaskOr(e->gm, ui.gm);
        AuditEventLogin(t, AUDITLOGINSUCCESS, ui.logUser, e, NULL, "Shibboleth");
    } else {
        UUtime(&e->lastAuthenticated);
        UpdateUsername(t, e, ui.logUser, "Shibboleth");
        e->reloginRequired = 0;
    }
    e->autoLoginBy = autoLoginByNone;
    e->sin_addr = t->sin_addr;
    e->relogin = ui.relogin;

    /* Note that we've logged in through Shibboleth, so if there is a logup event, no reason to
     * retry */
    e->loggedinShibboleth = 1;

    if (samlCtx->shibbolethSite->slo && samlCtx->responseIssuer && samlCtx->nameID) {
        // Save these values as session variables that cannot be manipulated by user.txt
        VariablesSetOrDeleteValue(e->sessionVariables, SESSION_VARIABLE_SAML_ISSUER,
                                  samlCtx->responseIssuer);
        VariablesSetOrDeleteValue(e->sessionVariables, SESSION_VARIABLE_SAML_NAMEID,
                                  samlCtx->nameID);
        VariablesSetOrDeleteValue(e->sessionVariables, SESSION_VARIABLE_SAML_NAMEID_FORMAT,
                                  samlCtx->nameIDFormat);
        VariablesSetOrDeleteValue(e->sessionVariables, SESSION_VARIABLE_SAML_NAMEID_NAME_QUALIFIER,
                                  samlCtx->nameIDNameQualifier);
        VariablesSetOrDeleteValue(e->sessionVariables,
                                  SESSION_VARIABLE_SAML_NAMEID_SP_NAME_QUALIFIER,
                                  samlCtx->nameIDSPNameQualifier);
        VariablesSetOrDeleteValue(e->sessionVariables, SESSION_VARIABLE_SAML_SESSION_INDEX,
                                  samlCtx->sessionIndex);
    } else {
        VariablesDelete(e->sessionVariables, SESSION_VARIABLE_SAML_ISSUER);
        VariablesDelete(e->sessionVariables, SESSION_VARIABLE_SAML_NAMEID);
        VariablesDelete(e->sessionVariables, SESSION_VARIABLE_SAML_NAMEID_FORMAT);
        VariablesDelete(e->sessionVariables, SESSION_VARIABLE_SAML_NAMEID_NAME_QUALIFIER);
        VariablesDelete(e->sessionVariables, SESSION_VARIABLE_SAML_NAMEID_SP_NAME_QUALIFIER);
        VariablesDelete(e->sessionVariables, SESSION_VARIABLE_SAML_SESSION_INDEX);
    }

    MergeSessionVariables(t, e);
    // Force any variable changes to be saved
    e->cookiesChanged = 1;

    GroupMaskOr(e->gm, ui.gm);
    LoadDefaultCookies(e);
    e->priv = e->priv || ui.admin;
    e->docsCustomDir = t->docsCustomDir;
    e->sessionFlags |= ui.sessionFlags;
    AdminLoginVars(e, &ui, 1);
    memcpy(&e->ipInterface, &ui.sourceIpInterface, sizeof(e->ipInterface));
    if (ui.loginBanner[0])
        ReadConfigSetString(&e->loginBanner, ui.loginBanner);

    gmAuto = GroupMaskNew(0);
    URLIpAccess(t, relayState, NULL, gmAuto, NULL, NULL);
    GroupMaskOr(e->gm, gmAuto);
    LoadDefaultCookies(e);
    GroupMaskFree(&gmAuto);

    NeedSave();

    if (relayState) {
        if (!(url = calloc(1, strlen(relayState) * 3 + 1)))
            PANIC;
        AddEncodedField(url, relayState, NULL);
    } else {
        if (!(url = calloc(1, 1)))
            PANIC;
        if (debugLevel > SAMLDebugThreshold_Low) {
            Log(AT "no RelayState received, warning");
            Log(AT "no qurl to send, warning");
        }
    }

    phase = e->notify != 0 ? 't' : 's';

    HTTPCode(t, 303, 0);
    NoCacheHeaders(s);
    e->confirmed = 0;
    if (*t->version) {
        if (e->notify == 0)
            SendEZproxyCookie(t, e);
        WriteLine(s, "Location: %s/connect?session=%c%s&qurl=", t->myUrl, phase, e->connectKey);
        UUSendZCRLF(s, url);
        HeaderToBody(t);
    }

    UUSendZ(s, "To access this site, go <a href='");
    WriteLine(s, "%s/connect?session=%c%s&qurl=", t->myUrl, phase, e->connectKey);
    UUSendZ(s, url);
    UUSendZ(s, "'>here</a>\r\n");

Finished:
    if (t->finalStatus == 997) {
        SendShibFailure(t);
    }

    SAMLResetDocumentCtx(samlCtx);
    FreeThenNull(samlCtx->responseIssuer);

    FreeThenNull(samlResponse);
    xmlFreeThenNull(location);
    FindFormFieldsFreeMultiValues(ffs);
    GroupMaskFree(&ui.gm);
    FreeThenNull(url);

    if (shibUsrFile >= 0) {
        close(shibUsrFile);
        shibUsrFile = -1;
    }
    PopIncludeFileStack(&ui, ui.fileInclusion, NULL);
    ui.fileInclusion = NULL;
}

static BOOL SAMLAddKey(xmlTextWriterPtr writer, int certificate) {
    int f = -1;
    char fnCrt[MAX_PATH];
    char line[256];
    BOOL body = 0;

    sprintf(fnCrt, "%s%08d.crt", SSLDIR, certificate);

    f = SOPEN(fnCrt);

    if (f < 0) {
        return 0;
    }

    if (xmlTextWriterStartElement(writer, BAD_CAST "md:KeyDescriptor") < 0)
        PANIC;
    if (xmlTextWriterStartElementNS(writer, BAD_CAST "ds", BAD_CAST "KeyInfo", NSDS) < 0)
        PANIC;
    if (xmlTextWriterStartElement(writer, BAD_CAST "ds:X509Data") < 0)
        PANIC;
    if (xmlTextWriterStartElement(writer, BAD_CAST "ds:X509Certificate") < 0)
        PANIC;

    while (FileReadLine(f, line, sizeof(line), NULL)) {
        Trim(line, TRIM_TRAIL);
        if (strstr(line, "BEGIN CERTIFICATE")) {
            body = TRUE;
            continue;
        }
        if (!body)
            continue;
        if (strstr(line, "END CERTIFICATE")) {
            if (body) {
                if (xmlTextWriterWriteString(writer, BAD_CAST "\n          ") < 0)
                    PANIC;
            }
            break;
        }
        if (xmlTextWriterWriteString(writer, BAD_CAST "\n") < 0)
            PANIC;
        if (xmlTextWriterWriteString(writer, BAD_CAST line) < 0)
            PANIC;
    }

    close(f);
    f = -1;

    /* Close ds:X509Certificate */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    /* Close ds:X509Data */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    /* Close ds:KeyInfo */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    /* Close md:KeyDescriptor */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    return 1;
}

void SAMLSendShibbolethMetadata(struct TRANSLATE *t,
                                int certificate,
                                struct SHIBBOLETHSITE *ssSpecific,
                                BOOL forceSLO) {
    struct UUSOCKET *s = &t->ssocket;
    xmlTextWriterPtr writer = NULL;
    xmlBufferPtr buf = NULL;
    BOOL shib13 = shibbolethAvailable13 != SHIBBOLETHAVAILABLEDISABLED;
    BOOL shib20 = shibbolethAvailable20 != SHIBBOLETHAVAILABLEDISABLED;
    char *me = (myUrlHttps ? myUrlHttps : myUrlHttp);
    char *p;
    int index = 1;
    struct SHIBBOLETHSITE *ss = shibbolethSites;
    BOOL keySuccess = 0;

    if (sslActiveIndex <= 0 || primaryLoginPortSsl == 0) {
        HTMLErrorPage(t, "Shibboleth Metadata Not Available",
                      "<p>SSL configuration must be completed, including setting a certificate "
                      "active and adding a LoginPortSSL directive to " EZPROXYCFG
                      ", before Shibboleth metadata can be displayed.</p>");
        return;
    }

    if (!(buf = xmlBufferCreate()))
        PANIC;
    if (!(writer = xmlNewTextWriterMemory(buf, 0)))
        PANIC;
    xmlTextWriterSetIndent(writer, 1);
    xmlTextWriterSetIndentString(writer, BAD_CAST "  ");
    // if (xmlTextWriterStartDocument(writer, NULL, "UTF-8", NULL) < 0) PANIC;

    /* Attempt to find EntityID for this cert */
    if (ssSpecific == NULL) {
        if (xmlTextWriterWriteComment(
                writer, BAD_CAST
                " Please insure that EntityDescriptor has the correct entityID attribute. ") < 0)
            PANIC;
        if (ss != NULL && shibbolethSitesCommonEntityID == NULL) {
            if (xmlTextWriterWriteComment(
                    writer, BAD_CAST " Here are possible options according to config file: ") < 0)
                PANIC;
            for (; ss != NULL; ss = ss->next) {
                if (xmlTextWriterWriteComment(writer, BAD_CAST ss->entityID) < 0)
                    PANIC;
            }
        }
    }

    /* Start root element */
    if (xmlTextWriterStartElementNS(writer, BAD_CAST "md", BAD_CAST "EntityDescriptor", NSMD) < 0)
        PANIC;

    // add the EntityId if we have one for this ssl cert
    if (ssSpecific != NULL) {
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "entityID",
                                        BAD_CAST ssSpecific->entityID) < 0)
            PANIC;
    } else if (shibbolethSitesCommonEntityID) {
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "entityID",
                                        BAD_CAST shibbolethSitesCommonEntityID) < 0)
            PANIC;
    } else {
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "entityID",
                                        BAD_CAST "CHANGE-TO-APPROPRIATE-VALUE") < 0)
            PANIC;
    }

    if (xmlTextWriterStartElement(writer, BAD_CAST "md:SPSSODescriptor") < 0)
        PANIC;
    t->buffer[0] = 0;
    if (shib20)
        strcat(t->buffer, "urn:oasis:names:tc:SAML:2.0:protocol ");
    if (shib13)
        strcat(t->buffer, "urn:oasis:names:tc:SAML:1.1:protocol ");

    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "protocolSupportEnumeration",
                                    BAD_CAST t->buffer) < 0)
        PANIC;

    if (ssSpecific && ssSpecific->signAuthnRequest) {
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "AuthnRequestsSigned", BAD_CAST "true") <
            0)
            PANIC;
    }

    if (xmlTextWriterStartElement(writer, BAD_CAST "md:Extensions") < 0)
        PANIC;

    /* strcat(t->buffer, "http://schemas.xmlsoap.org/ws/2003/07/secext"); */
    strcpy(t->buffer, me);
    strcat(t->buffer, "/Shibboleth.sso/DS");

    if (xmlTextWriterStartElementNS(writer, BAD_CAST "idpdisc", BAD_CAST "DiscoveryResponse",
                                    NSIDPDISC) < 0)
        PANIC;
    if (xmlTextWriterWriteAttribute(
            writer, BAD_CAST "Binding",
            BAD_CAST "urn:oasis:names:tc:SAML:profiles:SSO:idp-discovery-protocol") < 0)
        PANIC;
    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Location", BAD_CAST t->buffer) < 0)
        PANIC;
    if (xmlTextWriterWriteAttribute(writer, BAD_CAST "index", BAD_CAST "1") < 0)
        PANIC;

    /* Close idpdisc:DiscoveryResponse */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    /* Close md:Extensions */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    if (certificate > 0) {
        keySuccess = SAMLAddKey(writer, certificate);
    } else {
        int *certificates = ssSpecific->certificates;
        if (certificates) {
            for (; *certificates; certificates++) {
                if (!SAMLAddKey(writer, *certificates)) {
                    keySuccess = 0;
                    break;
                }
                keySuccess = 1;
            }
        } else {
            keySuccess = SAMLAddKey(writer, sslActiveIndex);
        }
    }

    if (!keySuccess) {
        HTMLErrorPage(t, "Shibboleth Metadata", "Unable to access certificate");
        return;
    }

    strcpy(t->buffer, me);
    strcat(t->buffer, "/Shibboleth.sso/");
    p = AtEnd(t->buffer);

    if (shib20) {
        strcpy(p, "SAML2/POST");
        if (xmlTextWriterStartElement(writer, BAD_CAST "md:AssertionConsumerService") < 0)
            PANIC;
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Binding",
                                        BAD_CAST
                                        "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST") < 0)
            PANIC;
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Location", BAD_CAST t->buffer) < 0)
            PANIC;
        if (xmlTextWriterWriteFormatAttribute(writer, BAD_CAST "index", "%d", index++) < 0)
            PANIC;
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "isDefault", BAD_CAST "true") < 0)
            PANIC;
        /* Close md:AssertionConsumerService */
        if (xmlTextWriterEndElement(writer) < 0)
            PANIC;

        strcpy(p, "SAML2/Artifact");
        if (xmlTextWriterStartElement(writer, BAD_CAST "md:AssertionConsumerService") < 0)
            PANIC;
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Binding",
                                        BAD_CAST
                                        "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact") < 0)
            PANIC;
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Location", BAD_CAST t->buffer) < 0)
            PANIC;
        if (xmlTextWriterWriteFormatAttribute(writer, BAD_CAST "index", "%d", index++) < 0)
            PANIC;
        /* Close md:AssertionConsumerService */
        if (xmlTextWriterEndElement(writer) < 0)
            PANIC;
    }

    if (shib13) {
        strcpy(p, "SAML/POST");
        if (xmlTextWriterStartElement(writer, BAD_CAST "md:AssertionConsumerService") < 0)
            PANIC;
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Binding",
                                        BAD_CAST
                                        "urn:oasis:names:tc:SAML:1.0:profiles:browser-post") < 0)
            PANIC;
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Location", BAD_CAST t->buffer) < 0)
            PANIC;
        if (xmlTextWriterWriteFormatAttribute(writer, BAD_CAST "index", "%d", index++) < 0)
            PANIC;
        if (index == 2 &&
            xmlTextWriterWriteAttribute(writer, BAD_CAST "isDefault", BAD_CAST "true") < 0)
            PANIC;
        /* Close md:AssertionConsumerService */
        if (xmlTextWriterEndElement(writer) < 0)
            PANIC;

        strcpy(p, "SAML/Artifact");
        if (xmlTextWriterStartElement(writer, BAD_CAST "md:AssertionConsumerService") < 0)
            PANIC;
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Binding",
                                        BAD_CAST
                                        "urn:oasis:names:tc:SAML:1.0:profiles:artifact-01") < 0)
            PANIC;
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Location", BAD_CAST t->buffer) < 0)
            PANIC;
        if (xmlTextWriterWriteFormatAttribute(writer, BAD_CAST "index", "%d", index++) < 0)
            PANIC;
        /* Close md:AssertionConsumerService */
        if (xmlTextWriterEndElement(writer) < 0)
            PANIC;
    }

    if (shib20 && (forceSLO || (ssSpecific != NULL && ssSpecific->slo))) {
        if (xmlTextWriterStartElement(writer, BAD_CAST "md:SingleLogoutService") < 0)
            PANIC;
        strcpy(t->buffer, me);
        strcat(t->buffer, "/Shibboleth.sso/SAML2/SLO/POST");
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Binding",
                                        BAD_CAST
                                        "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST") < 0)
            PANIC;
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Location", BAD_CAST t->buffer) < 0)
            PANIC;
        // Close md:SingleLogoutService
        if (xmlTextWriterEndElement(writer) < 0)
            PANIC;

        if (xmlTextWriterStartElement(writer, BAD_CAST "md:SingleLogoutService") < 0)
            PANIC;
        strcpy(t->buffer, me);
        strcat(t->buffer, "/Shibboleth.sso/SAML2/SLO/Redirect");
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Binding",
                                        BAD_CAST
                                        "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect") < 0)
            PANIC;
        if (xmlTextWriterWriteAttribute(writer, BAD_CAST "Location", BAD_CAST t->buffer) < 0)
            PANIC;
        // Close md:SingleLogoutService
        if (xmlTextWriterEndElement(writer) < 0)
            PANIC;
    }

    /* Close md:SPSSODESCRIPTOR */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    /* Close root element */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    if (xmlTextWriterEndDocument(writer) < 0)
        PANIC;

    if (xmlTextWriterFlush(writer) < 0)
        PANIC;

    HTTPCode(t, 200, 0);
    NoCacheHeaders(s);
    HTTPContentType(t, "text/plain", 1);

    UUSendZCRLF(s, (char *)buf->content);

Finished:
    if (writer) {
        xmlFreeTextWriter(writer);
        writer = NULL;
    }

    if (buf) {
        xmlBufferFree(buf);
        buf = NULL;
    }
}

void AdminShibbolethSSOMetadata(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;

    if (shibbolethAvailable13 != SHIBBOLETHAVAILABLEON &&
        shibbolethAvailable20 != SHIBBOLETHAVAILABLEON) {
        AdminShibbolethUnavailable(t, SHIBBOLETHAVAILABLEOFF, "1.3/2.0");
        return;
    }

    HTMLHeader(t, htmlHeaderOmitCache);

    UUSendZ(s, TAG_DOCTYPE_HTML_HEAD
            "\
<title>Shibboleth Metadata</title></head>\n\
<body>\n\
<p>\n\
The Shibboleth Metadata for this EZproxy server is available in <a href='/shibboleth'>Manage Shibboleth</a>.\n\
</p>\n\
</body>\n\
</html>\n");
}

void AdminShibbolethSSOSAML(struct TRANSLATE *t, char *query, char *post) {
    if (AdminShibbolethUnavailable(t, shibbolethAvailable13, "1.3"))
        return;

    UUxmlSecInit();

    AdminShibbolethSSOSAMLHandler(t, query, post, SHIBVERSION13);
}

void AdminShibbolethSSOSAML2(struct TRANSLATE *t, char *query, char *post) {
    if (AdminShibbolethUnavailable(t, shibbolethAvailable20, "2.0"))
        return;

    UUxmlSecInit();

    AdminShibbolethSSOSAMLHandler(t, query, post, SHIBVERSION20);
}
