#ifndef __TELNET_H__
#define __TELNET_H__

#include "common.h"

enum TSSTATES { TSDATA, TSIAC, TSWOPT, TSDOPT, TSSUBNEG, TSSUBIAC };
#define TCSE 240
#define TCNOP 241
#define TCDM 242
#define TCBRK 243
#define TCIP 244
#define TCAO 245
#define TCAYT 246
#define TCEC 247
#define TCEL 248
#define TCGA 249
#define TCSB 250
#define TCWILL 251
#define TCWONT 252
#define TCDO 253
#define TCDONT 254
#define TCIAC 255

#define TOECHO 1

struct TELNETSTATE {
    enum TSSTATES tsState;
    unsigned char tsSubState;
    unsigned char buffer[1024];
    unsigned char bufferPtr;
    int bufferRemain;
    BOOL hitEof;
    BOOL lastCr;
};

int TelnetInit(struct TELNETSTATE *ts);
int TelnetRecv(struct TELNETSTATE *ts, struct UUSOCKET *s, char *msg, size_t len);
BOOL TelnetRecvLine(struct TELNETSTATE *ts, struct UUSOCKET *s, char *buffer, int max);
int TelnetSend(struct TELNETSTATE *ts, struct UUSOCKET *s, char *msg, size_t len);
int TelnetSendString(struct TELNETSTATE *ts, struct UUSOCKET *s, char *str);
void TelnetSetNoEcho(struct TELNETSTATE *ts, struct UUSOCKET *s);

#endif  // __TELNET_H__
