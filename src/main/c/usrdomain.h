#ifndef __USRDOMAIN_H__
#define __USRDOMAIN_H__

#include "common.h"

#ifdef WIN32

/* Returns 0 if valid, 1 if not */
int FindUserNtDomain(struct TRANSLATE *t,
                     struct USERINFO *uip,
                     char *domain,
                     char *newPass,
                     char *verifyPass,
                     char *formDomain);

#endif

#endif  // __USRDOMAIN_H__
