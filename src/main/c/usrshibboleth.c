/**
 * Shibboleth 1.3/2.0 user authentication (user.txt redirection
 * to an Identity Provider/Discovery Service)
 *
 * This module implements part of Shibboleth 1.3/2.0 user authentication.  This
 * module specifically implements the use of ::Shibboleth in user.txt
 * which can be used to redirect users to an Identity Provider or
 * Discovery Service.  The majority of Shibboleth 1.3/2.0 support is
 * actually located in saml.c.
 *
 * This authentication method supports common conditions and actions.
 */

#define __UUFILE__ "usrshibboleth.c"

#include "common.h"
#include "uustring.h"

#include "usr.h"
#include "usrshibboleth.h"
#include "saml.h"

int UsrShibbolethIDP13(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct SHIBBOLETHSITE *ss = NULL;

    if (*arg == 0)
        return 1;

    FreeThenNull(uip->wayf);
    if (uip->wayf = (char *)SAMLSSOLocation(arg, &ss, SHIBVERSION13, &uip->wayfRedirect, NULL)) {
        if (!(uip->wayfEntityID = strdup(ss->entityID)))
            PANIC;
        uip->skipping = 1;
        return 0;
    }

    Log("Shibboleth IDP13 entity not found: %s", arg);
    return 1;
}

int UsrShibbolethIDP20(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct SHIBBOLETHSITE *ss = NULL;

    if (*arg == 0)
        return 1;

    FreeThenNull(uip->wayf);
    if (uip->wayf = (char *)SAMLSSOLocation(arg, &ss, SHIBVERSION20, &uip->wayfRedirect, NULL)) {
        uip->wayfss = ss;
        uip->skipping = 1;
        return 0;
    }

    Log("Shibboleth IDP20 entity not found: %s", arg);
    return 1;
}

int UsrShibbolethDS20(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    char *arg2;
    char *p;
    char *entityID = NULL;

    if (*arg == 0)
        return 1;

    for (;;) {
        if (*arg != '-')
            break;

        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if ((p = StartsIWith(arg, "-EntityID=")) && *p) {
            entityID = p;
        } else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized Shibboleth DS20 option: %s", arg);
        }
        arg = arg2;
    }

    if (*arg == 0)
        return 1;

    if (entityID == NULL || *entityID == 0) {
        if (shibbolethSitesCommonEntityID)
            entityID = shibbolethSitesCommonEntityID;
        else {
            UsrLog(uip,
                   "Shibboleth DS20 requires explicit -EntityID due to ShibbolethMetadata "
                   "directives having different EntityIDs.");
            return 1;
        }
    }

    FreeThenNull(uip->wayf);
    if (!(uip->wayf = strdup(arg)))
        PANIC;
    if (!(uip->wayfEntityID = strdup(entityID)))
        PANIC;
    uip->wayfDS = 1;
    uip->skipping = 1;
    return 0;
}

int UsrShibbolethWAYF13(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    char *arg2;
    char *p;
    char *entityID = NULL;

    if (*arg == 0)
        return 1;

    for (;;) {
        if (*arg != '-')
            break;

        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if ((p = StartsIWith(arg, "-EntityID=")) && *p) {
            entityID = p;
        } else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized Shibboleth WAYF13 option: %s", arg);
        }
        arg = arg2;
    }

    if (*arg == 0)
        return 1;

    if (entityID == NULL || *entityID == 0) {
        if (shibbolethSitesCommonEntityID)
            entityID = shibbolethSitesCommonEntityID;
        else {
            UsrLog(uip,
                   "Shibboleth WAYF13 requires explicit -EntityID due to multiple "
                   "ShibbolethMetadata with different EntityIDs");
            return 1;
        }
    }

    FreeThenNull(uip->wayf);
    if (!(uip->wayf = strdup(arg)))
        PANIC;
    if (!(uip->wayfEntityID = strdup(entityID)))
        PANIC;
    uip->skipping = 1;
    return 0;
}

enum FINDUSERRESULT FindUserShibboleth(struct TRANSLATE *t,
                                       struct USERINFO *uip,
                                       char *file,
                                       int f,
                                       struct FILEREADLINEBUFFER *frb,
                                       struct sockaddr_storage *pInterface) {
    struct USRDIRECTIVE udc[] = {
        {"IDP13",  UsrShibbolethIDP13,  0},
        {"IDP20",  UsrShibbolethIDP20,  0},
        {"DS20",   UsrShibbolethDS20,   0},
        {"WAYF13", UsrShibbolethWAYF13, 0},
        {NULL,     NULL,                0}
    };

    if (shibbolethSites == NULL) {
        UsrLog(uip, "Shibboleth is not configured so ::Shibboleth is unavailable");
        return uip->result;
    }

    uip->result = resultInvalid;
    uip->flags = USRUSERMAYBENULL;
    return UsrHandler(t, "Shibboleth", udc, NULL, uip, file, f, frb, pInterface, NULL);
}
