/**
 * Sagebrush (small ILS) user authentication
 *
 * This authentication method supports common conditions and actions.
 */

#define __UUFILE__ "usrsagebrush.c"

#include "common.h"

#include "usr.h"
#include "usrsagebrush.h"

struct SAGEBRUSHCTX {
    char *mainForm;
    char *presentLoginForm;
    char *handleLoginForm;
};

int UsrSagebrushMainForm(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct SAGEBRUSHCTX *sagebrushCtx = (struct SAGEBRUSHCTX *)context;

    ReadConfigSetString(&sagebrushCtx->mainForm, arg);
    return 0;
}

int UsrSagebrushPresentLoginForm(struct TRANSLATE *t,
                                 struct USERINFO *uip,
                                 void *context,
                                 char *arg) {
    struct SAGEBRUSHCTX *sagebrushCtx = (struct SAGEBRUSHCTX *)context;

    ReadConfigSetString(&sagebrushCtx->presentLoginForm, arg);
    return 0;
}

int UsrSagebrushHandleLoginForm(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                void *context,
                                char *arg) {
    struct SAGEBRUSHCTX *sagebrushCtx = (struct SAGEBRUSHCTX *)context;

    ReadConfigSetString(&sagebrushCtx->handleLoginForm, arg);
    return 0;
}

int UsrSagebrushURL(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    int result = 1;
    char *user, *pass;
    struct UUSOCKET rr, *r = &rr;
    char line[512];
    BOOL header;
    char save;
    struct PROTOCOLDETAIL *protocolDetail;
    char *host;
    char *colon;
    char *endOfHost, *endOfHostPort;
    PORT port;
    char *p;
    char *session = NULL;
    char *redirect = NULL;
    int phase;
    struct SAGEBRUSHCTX *sagebrushCtx = (struct SAGEBRUSHCTX *)context;
    char *mainForm = sagebrushCtx->mainForm ? sagebrushCtx->mainForm : "/InfoCentre/Library.do";
    char *presentLoginForm = sagebrushCtx->presentLoginForm ? sagebrushCtx->presentLoginForm
                                                            : "/InfoCentre/MyLibraryInfo.do";
    char *handleLoginForm = sagebrushCtx->handleLoginForm ? sagebrushCtx->handleLoginForm
                                                          : "/InfoCentre/AuthenticateUser.do";
    char saveEndOfHostPort;

    UUInitSocket(r, INVALID_SOCKET);

    user = uip->user ? uip->user : "";
    pass = uip->pass ? uip->pass : "";

    if (*user == 0 || strlen(user) > 32 || strlen(pass) > 32)
        goto Finished;

    if (!(ParseProtocolHostPort(arg, "http://", &protocolDetail, &host, &endOfHost, &colon, &port,
                                NULL, &endOfHostPort)))
        goto Finished;

    if (protocolDetail->protocol > PROTOCOLHTTPS)
        goto Finished;

    if (endOfHostPort) {
        saveEndOfHostPort = *endOfHostPort;
        *endOfHostPort = 0;
    } else
        saveEndOfHostPort = 0;

    uip->result = resultRefused;

    for (phase = 0; phase < 4; phase++) {
        if (phase > 0 && session == NULL)
            break;

        if (save = *endOfHost)
            *endOfHost = 0;

        if (UUConnectWithSource2(r, host, port, "FindUserSagebrush", uip->pInterface,
                                 (char)protocolDetail->protocol))
            goto Finished;

        if (save)
            *endOfHost = save;

        if (phase == 0)
            uip->result = resultInvalid;

        if (phase == 3) {
            strcpy(line, "field%5B0%5D.value=");
            AddEncodedField(line, user, NULL);
            strcat(line, "&field%5B1%5D.value=");
            AddEncodedField(line, pass, NULL);
            strcat(line, "&login=Login");
        }

        WriteLine(r, "%s ", phase == 3 ? "POST" : "GET");

        if (phase < 2) {
            UUSendZ(r, mainForm);
        } else if (phase == 2) {
            UUSendZ(r, presentLoginForm);
        } else {
            UUSendZ(r, handleLoginForm);
        }
        /*
            if (saveEndOfHostPort == '/' && *(endOfHostPort + 1)) {
                UUSendZ(r, endOfHostPort + 1);
            }
        } else if (phase == 2) {
            UUSendZ(r, "/InfoCentre/MyLibraryInfo.do");
        } else {
            UUSendZ(r, "/InfoCentre/Library.do");
        }
        */

        UUSendZ(r, " HTTP/1.0\r\n");

        save = *endOfHostPort;
        *endOfHostPort = 0;
        WriteLine(r, "Host: %s\r\n", host);
        *endOfHostPort = save;

        if (phase > 0) {
            WriteLine(r, "Cookie: JSESSIONID=%s", session);
            if (phase > 1)
                UUSendZ(r, "; siteID=1");
            UUSendCRLF(r);
            if (phase == 3) {
                UUSendZ(r, "Content-Type: application/x-www-form-urlencoded\r\n");
                WriteLine(r, "Content-Length: %d\r\n", strlen(line));
            }
        }

        UUSendZ(r, "Connection: close\r\n\r\n");

        if (phase == 3) {
            UUSendZCRLF(r, line);
        }

        header = 1;
        while (ReadLine(r, line, sizeof(line))) {
            if (uip->debug)
                UsrLog(uip, "Sagebrush response %s", line);
            if (header) {
                if (line[0] == 0) {
                    header = 0;
                    continue;
                }
                if (phase == 0) {
                    if (p = StartsIWith(line, "Set-Cookie: ")) {
                        p = SkipWS(p);
                        if (p = StartsIWith(p, "JSESSIONID=")) {
                            FreeThenNull(session);
                            if (!(session = strdup(p)))
                                PANIC;
                            if (p = strchr(session, ';'))
                                *p = 0;
                            if (strlen(session) > 64)
                                *(session + 64) = 0;
                        }
                    }
                    if (p = StartsIWith(line, "Location: ")) {
                        char *redirectEndOfHostPort;
                        p = SkipWS(p);
                        ParseProtocolHostPort(p, "", NULL, NULL, NULL, NULL, NULL, NULL,
                                              &redirectEndOfHostPort);
                        if (redirectEndOfHostPort) {
                            FreeThenNull(redirect);
                            if (!(redirect = strdup(redirectEndOfHostPort)))
                                PANIC;
                        }
                    }
                }
                if (phase == 3) {
                    if (p = StartsIWith(line, "Location: ")) {
                        p = SkipWS(p);
                        if (StrIStr(p, presentLoginForm)) {
                            uip->result = resultValid;
                        }
                    }
                }
            }
        }

        UUStopSocket(r, 0);
    }

Finished:

    FreeThenNull(redirect);
    FreeThenNull(session);

    return result;
}

enum FINDUSERRESULT FindUserSagebrush(struct TRANSLATE *t,
                                      struct USERINFO *uip,
                                      char *file,
                                      int f,
                                      struct FILEREADLINEBUFFER *frb,
                                      struct sockaddr_storage *pInterface) {
    struct USRDIRECTIVE udc[] = {
        {"MainForm",         UsrSagebrushMainForm,         0},
        {"PresentLoginForm", UsrSagebrushPresentLoginForm, 0},
        {"HandleLoginForm",  UsrSagebrushHandleLoginForm,  0},
        {"URL",              UsrSagebrushURL,              0},
        {NULL,               NULL,                         0}
    };
    enum FINDUSERRESULT result;
    struct SAGEBRUSHCTX sagebrushCtx;

    memset(&sagebrushCtx, 0, sizeof(sagebrushCtx));

    uip->flags = 0;
    result = UsrHandler(t, "Sagebrush", udc, &sagebrushCtx, uip, file, f, frb, pInterface, NULL);

    FreeThenNull(sagebrushCtx.mainForm);
    FreeThenNull(sagebrushCtx.presentLoginForm);
    FreeThenNull(sagebrushCtx.handleLoginForm);

    return result;
}
