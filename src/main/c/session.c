#define __UUFILE__ "session.c"

#include "common.h"

#include "adminaudit.h"
#include "adminfiddler.h"
#include "intrusion.h"
#include "security.h"

int UUAvlTreeCmpSession(const void *v1, const void *v2) {
    struct SESSION *e1 = (struct SESSION *)v1;
    struct SESSION *e2 = (struct SESSION *)v2;

    return (strcmp(e1->key, e2->key));
}

void CheckGuardianKillSession(void) {
    struct SESSION *e;

    if (guardian->killSession != 1)
        return;

    e = FindSessionByKey(guardian->arg);

    if (e == NULL) {
        guardian->killSession = 3;
        return;
    }

    if (e->anonymous) {
        guardian->killSession = 4;
        return;
    }

    StopSession(e, "EZproxy kill");

    guardian->killSession = 0;
}

void EnforceUserLimit(char *user, int userLimit) {
    struct SESSION *e, *oldestSession;
    int i;
    int active;

    if (user == NULL || userLimit == 0)
        return;

    for (;;) {
        active = 0;
        oldestSession = NULL;
        for (e = sessions, i = 0; i < cSessions; e++, i++) {
            if (e->logUserFull == NULL || e->userLimitExceeded ||
                stricmp(e->logUserFull, user) != 0)
                continue;
            active++;
            if (oldestSession == NULL || oldestSession->accessed > e->accessed)
                oldestSession = e;
        }
        if (active < userLimit)
            return;
        oldestSession->userLimitExceeded = userLimit;
        /* In most cases, that should have dropped us down to the limit, so return if it did */
        active--;
        if (active < userLimit)
            return;
    }
}

struct SESSION *FindSessionByGenericUser(char *user) {
    struct SESSION *e;
    int i;

    for (i = 0, e = sessions; i < cSessions; i++, e++)
        if (e->active == 1 && e->genericUser != NULL && strcmp(e->genericUser, user) == 0 &&
            e->expirable == 0)
            return e;
    return NULL;
}

struct SESSION *FindSessionByUserObjectTicket(char *userObjectTicket) {
    int i;
    struct SESSION *e;

    for (i = 0, e = sessions; i < cSessions; i++, e++)
        if (e->active == 1 && strcmp(e->userObjectTicket, userObjectTicket) == 0) {
            return e;
        }

    return NULL;
}

struct SESSION *FindSessionByCpipSid(char *sid) {
    int i;
    struct SESSION *e;

    if (strnicmp(sid, "ezproxy:", 8) == 0)
        return FindSessionByKey(sid + 8);

    for (i = 0, e = sessions; i < cSessions; i++, e++)
        if (e->active == 1 && e->cpipSid && strcmp(e->cpipSid, sid) == 0) {
            return e;
        }

    return NULL;
}

BOOL FindSessionByAnonymousUrl(struct TRANSLATE *t, char *url, struct DATABASE *b) {
    int i;
    struct ANONYMOUSURL *au;
    char mode;
    char *urlCS = NULL;
    char *urlLower = NULL;
    PORT defaultPort;
    time_t ipLastActive = 0;
    BOOL ipLastActiveUninitialized = 1;
    char ipBasedDecision = 0;

    if (anonymousUrls == NULL)
        return 0;

    if (url && b) {
        urlCS = url;
    } else {
        if (t->host == NULL || (t->host)->myDb == NULL)
            return 0;

        b = (t->host)->myDb;

        /* The 20 account for up to 8 in https://, the : for the port, 5 for the port, the null, and
         * 5 to spare */
        if (!(urlCS = malloc(strlen((t->host)->hostname) + strlen(t->urlCopy) + 20)))
            PANIC;

        if (t->host->ftpgw) {
            strcpy(urlCS, "ftp");
            defaultPort = 21;
        } else if (t->host->useSsl) {
            strcpy(urlCS, "https");
            defaultPort = 443;
        } else {
            strcpy(urlCS, "http");
            defaultPort = 80;
        }

        sprintf(strchr(urlCS, 0), "://%s", t->host->hostname);
        if (t->host->remport != defaultPort)
            sprintf(strchr(urlCS, 0), ":%d", t->host->remport);
        strcat(urlCS, t->urlCopy);
    }

    mode = '-';
    for (i = 0, au = anonymousUrls; i < b->cAnonymousUrls; i++, au++) {
        if (au->options && stricmp(t->method, "OPTIONS") != 0) {
            continue;
        }
        if (au->regularExpression) {
            char *compUrl;
            if (au->caseSensitive) {
                compUrl = urlCS;
            } else {
                if (urlLower == NULL) {
                    if (!(urlLower = strdup(urlCS)))
                        PANIC;
                    AllLower(urlLower);
                }
                compUrl = urlLower;
            }
            if (xmlRegexpExec(au->regularExpression, BAD_CAST compUrl) != 1)
                continue;
        } else {
            if (!(au->caseSensitive ? WildCompareCaseSensitive(urlCS, au->anonymousUrl)
                                    : WildCompare(urlCS, au->anonymousUrl)))
                continue;
        }
        if (ipBasedDecision = au->activeIP) {
            if (ipLastActiveUninitialized) {
                ipLastActive = IPLastActive(&t->sin_addr, 0, NULL, NULL);
                ipLastActiveUninitialized = 0;
            }
            if (ipLastActive == 0)
                continue;
        }
        mode = au->mode;
    }

    FreeThenNull(urlLower);
    /* urlCS can point to url, which is static, or to a newly formed string.
       We only free if urlCS is not pointing to the passed in "url"
    */
    if (urlCS != url)
        FreeThenNull(urlCS);

    if (ipBasedDecision)
        UpdateAccessByIP(&t->sin_addr);

    if (mode == '+') {
        t->session = anonymousSession;
        return 1;
    }

    return 0;
}

static BOOL SameCountry(struct TRANSLATE *t, struct SESSION *e) {
    struct LOCATION location;
    char ipBuffer[INET6_ADDRSTRLEN];
    BOOL sameIP = is_ip_equal(&e->sin_addr, &t->sin_addr);
    BOOL sameCountry = TRUE;
    BOOL auditCountry = FALSE;

    if (sameIP) {
        goto Finished;
    }

    if (optionBlockCountryChange == 0 || AnyLocations() == 0 ||
        (e->sessionFlags & SESSION_FLAG_ALLOW_COUNTRY_CHANGE) != 0) {
        goto Finished;
    }

    if (e->lastCountryCode[0] == 0) {
        FindLocationByNetwork(&location, &e->sin_addr);
        StrCpy3(e->lastCountryCode, location.countryCode, sizeof(e->lastCountryCode));
        set_ip(&e->blockCountryIP, 0, 0, 0, 0);
    }

    if (is_ip_equal(&t->sin_addr, &e->blockCountryIP)) {
        sameCountry = FALSE;
        goto Finished;
    }

    FindLocationByNetwork(&location, &t->sin_addr);

    if (strnullcmp(location.countryCode, e->lastCountryCode) != 0) {
        memcpy(&e->blockCountryIP, &t->sin_addr, sizeof(t->sin_addr));
        sameCountry = FALSE;
        auditCountry = TRUE;
    }

Finished:
    if (!sameIP) {
        if (sameCountry) {
            ipToStr(&e->sin_addr, ipBuffer, sizeof ipBuffer);
            AuditEvent(t, AUDITSESSIONIPCHANGE, e->logUserBrief, e, "Previous IP %s", ipBuffer);
            memcpy(&e->sin_addr, &t->sin_addr, sizeof(e->sin_addr));
        } else if (auditCountry) {
            AuditEvent(t, AUDITBLOCKCOUNTRYCHANGE, e->logUserBrief, e, "Session country %s",
                       e->lastCountryCode);
        }
    }

    return sameCountry;
}

BOOL FindSessionByCookie(struct TRANSLATE *t) {
    char *cookie;
    char *ezproxy;
    char *p;
    struct SESSION *e;
    char save;
    char *comma;
    BOOL me;
    char *cookieStarts;

    t->session = NULL;

    if (optionSingleSession) {
        /* also overrides last location tracking */
        e = t->session = StartSession(NULL, 0, NULL);
        if (e->gm == NULL)
            e->gm = GroupMaskNew(0);
        GroupMaskClear(e->gm);
        GroupMaskNot(e->gm);
        LoadDefaultCookies(e);
        e->anonymous = 1;
        return 1;
    }

    for (cookie = t->requestHeaders; (cookie = FindField("cookie", cookie));) {
        LinkField(cookie);

        for (ezproxy = cookie; ezproxy && *ezproxy;) {
            while (*ezproxy == ';')
                ezproxy++;
            ezproxy = SkipWS(ezproxy);
            cookieStarts = StartsWith(ezproxy, loginCookieName);
            if (cookieStarts == NULL) {
                ezproxy = strchr(ezproxy, ';');
                continue;
            }
            if (*cookieStarts == 'n' || *cookieStarts == 'l') {
                cookieStarts++;
            }
            if (*cookieStarts == '=') {
                cookieStarts++;
            } else {
                ezproxy = strchr(ezproxy, ';');
                continue;
            }
            ezproxy = SkipWS(cookieStarts);
            comma = NULL;
            for (p = ezproxy; *p && *p != ';' && !IsWS(*p); p++) {
                if (*p == ',' && comma == NULL)
                    comma = p;
            }
            if (comma) {
                save = *comma;
                *comma = 0;
                me = stricmp(ezproxy, myUrlHttp) == 0;
                *comma = save;
                if (!me)
                    continue;
                ezproxy = comma + 1;
            }

            save = *p;
            *p = 0;
            if (*ezproxy == 'e' && strlen(ezproxy) == 11 && IsAllDigits(ezproxy + 1)) {
                time_t now, when = atoi(ezproxy + 1);
                UUtime(&now);
                if (now > when && now < when + mSessionLife)
                    t->excludeIPBannerSuppress = 1;
                e = NULL;
            } else
                e = FindSessionByKey(ezproxy);
            *p = save;
            if (e && SameCountry(t, e)) {
                t->session = e;
                UUtime(&e->accessed);
                if (e->confirmed == 0)
                    e->confirmed = e->accessed;
                if (e->lifetime == 0) {
                    e->lifetime = mSessionLife;
                    ClusterUpdateSession(e);
                }
                return 1;
            }
            ezproxy = strchr(ezproxy, ';');
        }
    }
    return 0;
}

struct SESSION *FindSessionByKey(char *key) {
    struct SESSION *e, findSession;

    RWLAcquireRead(&rwlSessions);
    StrCpy3(findSession.key, key, sizeof(findSession.key));
    e = UUAvlTreeFind(&avlSessions, &findSession);
    RWLReleaseRead(&rwlSessions);
    if (debugLevel > 8000) {
        if (e) {
            Log("session found by key '%s'", e->key);
        } else {
            Log("session not found for key '%s'", key);
        }
    }
    return e;
}

struct SESSION *FindSessionByConnectKey(const char *connectKey) {
    struct SESSION *e;
    int i;

    if (!*connectKey) {
        return NULL;
    }

    for (e = sessions, i = 0; i < cSessions; e++, i++) {
        if (e->active && strcmp(e->connectKey, connectKey) == 0) {
            return e;
        }
    }

    return NULL;
}

struct SESSION *FindSessionByAdminKey(const char *adminKey) {
    struct SESSION *e;
    int i;

    if (!*adminKey) {
        return NULL;
    }

    for (e = sessions, i = 0; i < cSessions; e++, i++) {
        if (e->active && strcmp(e->adminKey, adminKey) == 0) {
            return e;
        }
    }

    return NULL;
}

// buffer should be of size MAXKEY
static void GenerateKey(char *buffer) {
    int i;
    for (i = 0; i < sessionKeySize; i++) {
        *(buffer + i) = RandChar();
    }
    *(buffer + i) = 0;
}

// This function assumes the caller holds a write lock on rwlSessions
void AttachAdminKey(struct SESSION *e) {
    char adminKey[MAXKEY];
    while (!e->adminKey[0]) {
        GenerateKey(adminKey);
        if (!FindSessionByAdminKey(adminKey)) {
            strcpy(e->adminKey, adminKey);
        }
    }
}

// This function assumes the caller holds a write lock on rwlSessions
void AttachConnectKey(struct SESSION *e) {
    char connectKey[MAXKEY];
    while (!e->connectKey[0]) {
        GenerateKey(connectKey);
        if (!FindSessionByConnectKey(connectKey)) {
            strcpy(e->connectKey, connectKey);
        }
    }
}

struct SESSION *FindSessionByConnectKeyInSameCountryWithinConnectWindow(struct TRANSLATE *t,
                                                                        char *connectKey) {
    struct SESSION *e;

    e = FindSessionByConnectKey(connectKey);
    if (e) {
        if (!SameCountry(t, e)) {
            e = NULL;
        } else if (e->confirmed != 0) {
            time_t now;
            UUtime(&now);
            int diff = abs(now - e->confirmed);
            if (diff >= connectWindow) {
                AuditEvent(t, AUDITSESSIONRECONNECTBLOCKED, e->logUserFull, e, "%d", diff);
                e = NULL;
            }
        }
    }
    return e;
}

void SetLogUserBrief(struct SESSION *e) {
    char *hyphen;

    if (e) {
        VariablesReset(e->identifierVariables);
        if (e->logUserFull == NULL || *e->logUserFull == 0)
            e->logUserBrief = NULL;
        else {
            e->logUserBrief = e->logUserFull;

            if (e->autoLoginBy == autoLoginByReferer && StartsIWith(e->logUserFull, "referer-"))
                e->logUserBrief = "referer";
            else if (e->autoLoginBy == autoLoginByIP && (hyphen = strrchr(e->logUserFull, '-')) &&
                     NumericHost(hyphen + 1)) {
                size_t len = hyphen - e->logUserFull;
                int i;
                struct IPTYPE *l;

                if (len == 4 && strnicmp("auto", e->logUserFull, len) == 0) {
                    e->logUserBrief = "auto";
                } else {
                    for (i = 0, l = ipTypes; i < cIpTypes; i++, l++) {
                        if (l->autoUser && len == strlen(l->autoUser) &&
                            strnicmp(l->autoUser, e->logUserFull, len) == 0) {
                            e->logUserBrief = l->autoUser;
                            break;
                        }
                    }
                }
            }
        }
    }
}

BOOL StoreLogUser(struct SESSION *e, char *logUserFull) {
    char *p;
    char *newLogUserFull;
    char *oldLogUserFull;
    BOOL changed = 0;

    oldLogUserFull = e->logUserFull;

    if (logUserFull == NULL || *logUserFull == 0) {
        e->logUserFull = e->logUserBrief = NULL;
    } else {
        if (!(newLogUserFull = strdup(logUserFull)))
            PANIC;
        for (p = strchr(newLogUserFull, 0) - 1; p >= newLogUserFull; p--) {
            if (*p > 0 && *p <= ' ')
                StrCpyOverlap(p, p + 1);
        }
        if (*newLogUserFull == 0) {
            FreeThenNull(newLogUserFull);
        }
        e->logUserFull = newLogUserFull;
    }

    SetLogUserBrief(e);
    if (changed = strnullcmp(oldLogUserFull, e->logUserFull) != 0) {
        e->usageLimitExceeded = FindUsageLimitExceeded(e->logUserFull, 1, 0);
    }

    FreeThenNull(oldLogUserFull);

    return changed;
}

void UpdateUsername(struct TRANSLATE *t, struct SESSION *e, char *username, char *facility) {
    if (e && username && *username) {
        if (StoreLogUser(e, username)) {
            if (e->autoLoginBy != autoLoginByNone)
                e->autoLoginBy = autoLoginByNone;
        }
        AuditEventLogin(t, AUDITLOGINSUCCESSRELOGIN, username, e, NULL, facility);
    }
}

void LoadDefaultCookies(struct SESSION *e) {
    struct COOKIE *kd;
    time_t expires = 0;

    if (e) {
        for (kd = defaultCookies; kd; kd = kd->next) {
            if (GroupMaskOverlap(kd->gm, e->gm)) {
                if (expires == 0) {
                    UUtime(&expires);
                    expires += TWO_DAYS_IN_SECONDS;
                }
                ClusterAddCookie(e, kd->name, kd->domain, kd->path, expires);
            }
            /*
            if (!(kn = calloc(1, sizeof(*kn)))) PANIC;

            if (!(kn->name = strdup(kd->name))) PANIC;
            if (kd->val)
                if (!(kn->val = strdup(kd->val))) PANIC;
            if (!(kn->domain = strdup(kd->domain))) PANIC;
            if (!(kn->path = strdup(kd->path))) PANIC;
            kn->expires = now + 48 * 60 * 60;

            kn->next = e->cookies;
            e->cookies = kn;
            */
        }
    }
}

struct SESSION *ClusterStartSession(char *logUser,
                                    time_t relogin,
                                    char *clusterSession,
                                    BOOL fromCluster) {
    char key[MAXKEY];
    int open, i;
    struct SESSION *e, findSession;

    RWLAcquireWrite(&rwlSessions);

    if (optionSingleSession && sessions->active) {
        RWLReleaseWrite(&rwlSessions);
        return sessions;
    }

    for (;;) {
        if (clusterSession)
            strcpy(key, clusterSession);
        else {
            GenerateKey(key);
        }
        /* Check to see if this key exists, and if so, try again */
        StrCpy3(findSession.key, key, sizeof(findSession.key));
        if (e = UUAvlTreeFind(&avlSessions, &findSession)) {
            if (clusterSession) {
                RWLReleaseWrite(&rwlSessions);
                return e;
            }
            /* Not a cluster session, so if the key exists, try again for a new key */
            continue;
        }

        /* Now look to see if there is an open session to reuse */
        for (open = -1, i = 0, e = sessions; i < cSessions; i++, e++) {
            if (e->active == 0) {
                open = i;
                break;
            }
        }
        break;
    }

    if (open == -1) {
        if (cSessions == mSessions) {
            Log("Maximum sessions in use, increase with MS in %s", EZPROXYCFG);
            RWLReleaseWrite(&rwlSessions);
            return NULL;
        }
        open = cSessions++;
    }
    e = sessions + open;
    TossCookies(e);
    e->priv = 0;
    e->sessionFlags = 0;
    e->expirable = 1;
    e->accountChangePass = 0;
    e->lifetime = 0; /* If this isn't used within 2 minutes, waste it */
    memset(&e->sin_addr, 0, sizeof e->sin_addr);
    strcpy(e->key, key);
    AttachAdminKey(e);
    AttachConnectKey(e);
    UUtime(&e->created);
    e->lastAuthenticated = e->created;
    e->confirmed = optionSingleSession ? e->created : 0;
    e->accessed = e->created;
    e->relogin = relogin;
    e->reloginRequired = 0;
    e->userLimitExceeded = 0;
    e->notify = 0;  // IsLicenseExpired(&license) ? 2 : 0;
    e->proxyType = optionProxyType;
    e->csrfToken[0] = 0;
    StoreLogUser(e, logUser);

    if (e->gm == NULL)
        e->gm = GroupMaskNew(0);
    else
        GroupMaskClear(e->gm);

    memset(&e->ipInterface, 0, sizeof(e->ipInterface));

    if (e->sessionVariables == NULL)
        e->sessionVariables = VariablesNew("session", TRUE);

    if (e->identifierVariables == NULL)
        e->identifierVariables = VariablesNew("identifiers", TRUE);

    UUAvlTreeInsert(&avlSessions, e);
    e->active = 1;
    NeedSave();
    RWLReleaseWrite(&rwlSessions);
    if (cPeers > 0 && fromCluster == 0)
        ClusterUpdateSession(e);

    if (debugLevel > 8000)
        Log("new session created, key '%s'", e->key);

    return e;
}

struct SESSION *StartSession(char *logUser, time_t relogin, char *clusterSession) {
    return ClusterStartSession(logUser, relogin, clusterSession, 0);
}

/*
struct SESSION *StartOrResumeSession(struct TRANSLATE *t, time_t relogin, char *logUser, char
*clusterSession)
{
    struct SESSION *e;

    if (FindSessionByCookie(t)) {
        e = t->session;
        t->session = NULL;
        if (e && e->relogin) {
            if (e->userLimitExceeded)
                e->userLimitExceeded = 0;
            StoreLogUser(e, logUser);
            e->relogin = relogin;
            e->reloginRequired = 0;
            return e;
        }
    }

    if (e = StartSession(logUser, clusterSession)) {
        e->relogin = relogin;
        e->reloginRequired = 0;
    }

    return e;
}
*/

BOOL ReapSession(struct SESSION *e) {
    int i;
    struct TRANSLATE *t;
    char filename[MAXKEY + 32];
    struct PDFURL *h, *p, *n;
    int v;

    if (e->active != 2)
        return 0;

    RWLAcquireWrite(&rwlSessions);
    UUAvlTreeDelete(&avlSessions, e);
    RWLReleaseWrite(&rwlSessions);

    /* Remove saved cookies */
    sprintf(filename, "%s%s%s", COOKIESDIR, DIRSEP, e->key);
    unlink(filename);

    /* If any translate structure is using this session, return without reaping */
    for (i = 0, t = translates; i < cTranslates; i++, t++)
        if (t->active && (t->session == e))
            return 0;

    e->priv = 0;
    e->sessionFlags = 0;
    e->key[0] = 0;
    e->adminKey[0] = 0;
    e->connectKey[0] = 0;
    e->userObjectTicket[0] = 0;
    e->nextFiddlerSessionIndex = 0;
    e->fiddlerActive = 0;

    e->logupId = 0;
    e->logupDb = NULL;
    FreeThenNull(e->logupUrl);
    FreeThenNull(e->logupDenyFile);

    FreeThenNull(e->genericUser);
    FreeThenNull(e->cpipSid);
    FreeThenNull(e->accountUsername);

    FreeThenNull(e->logUserFull);
    e->logUserBrief = NULL;

    FreeThenNull(e->loginBanner);
    FreeThenNull(e->refererDestUrl);
    VariablesReset(e->sessionVariables);
    VariablesReset(e->identifierVariables);

    e->lastCountryCode[0] = 0;

    if (e->docsCustomDir) {
        /* This is from a StaticStringCopy, don't free, just NULL out pointer */
        e->docsCustomDir = NULL;
    }

    for (v = 0; v < MAXSESSIONVARS; v++) {
        FreeThenNull(e->vars[v]);
    }

    if (h = e->pdfUrls) {
        for (p = h->flink; p != h; p = n) {
            n = p->flink;
            FreeThenNull(p->url);
            FreeThenNull(p);
        }
        FreeThenNull(h);
        e->pdfUrls = NULL;
    }

    e->expirable = 1;
    memset(&e->sin_addr, 0, sizeof e->sin_addr);
    e->sciFinderAuthorized = 0;
    e->loggedinShibboleth = 0;
    e->active = 0;

    return 1;
}

void ClusterStopSession(struct SESSION *e, char *reason, BOOL fromCluster) {
    time_t now;
    int remain;
    struct PEERMSG msg;

    if (e == NULL)
        return;

    if (e->anonymous)
        return;

    if (e->active != 1)
        return;

    /* Create stop request to preserve key value, defer transmission until deleted */
    if (cPeers > 0 && fromCluster == 0) {
        ClusterInitMsg(&msg, 'A', 'S', 0);
        remain = sizeof(msg.data);
        EncodeFields(msg.data, &remain, EDCHAR, (int)'E', EDACHAR, e->key, EDSTOP);
        msg.datalen = sizeof(msg.data) - remain;
    }

    AuditEvent(NULL, AUDITLOGOUT, e->logUserFull, e, reason);
    e->active = 2;
    NeedSave();
    if (!ReapSession(e)) {
        /* If the user accessed logout, the reap won't have succeeded since a translate will be in
           use, so schedule an expire run in a few seconds */
        UUtime(&now);
        if (now + 10 < nextExpire)
            nextExpire = now + 10;
    }
    if (cPeers > 0 && fromCluster == 0)
        ClusterRequest(&msg);
}

void StopSession(struct SESSION *e, char *reason) {
    ClusterStopSession(e, reason, 0);
}

BOOL SessionExpirationReached(struct SESSION *e, time_t now) {
    return e->accessed + (e->lifetime ? e->lifetime : 120) < now && e->expirable != 0;
}

void ExpireSessions(void) {
    struct SESSION *e;
    int i;
    time_t expired;

    /*  printf("Expiring inactive sessions\n"); */
    UUtime(&expired);
    /*  expired -= mSessionLife; */

    for (i = 0, e = sessions; i < cSessions; i++, e++) {
        if (e->active == 2) {
            ReapSession(e);
            continue;
        }
        if (e->active != 1)
            continue;

        if (e->confirmed == 0) {
            if (e->created > expired)
                e->created = expired;
            if (expired - e->created > 60) {
                StopSession(e, "Login never completed");
            }
            continue;
        }

        if (SessionExpirationReached(e, expired))
            StopSession(e, "Expired");
    }

    FiddlerCleanup();
}

/* ClearUsageLimitExceeded assumes you hold the mUsage mutex */
void ClearUsageLimitExceeded(struct USAGELIMITEXCEEDED *ule,
                             struct USAGELIMIT *ul,
                             struct USAGE *u,
                             char *why) {
    if (u->when || u->lastAttempt) {
        PruneUsageDateHour(u, 0, 0);
        if (u->when) {
            if (ul == intruderUsageLimit) {
                ule->intruderUserAttemptsCount--;
            } else {
                if (ul->enforce)
                    ule->enforcedCount--;
                else
                    ule->monitoredCount--;
            }
            if (ul->name && u->user && why) {
                if (auditEventsMask &
                    (ul == intruderUsageLimit ? AUDITLOGININTRUDERUSERMASK : AUDITUSAGELIMITMASK))
                    AuditEvent(
                        NULL, (ul == intruderUsageLimit ? AUDITLOGININTRUDERUSER : AUDITUSAGELIMIT),
                        u->user, NULL, "%s suspension cleared by %s", ul->name, why);
                else
                    Log("%s usage limit suspension cleared for %s by %s", ul->name, u->user, why);
            }
            u->when = 0;
        }
        u->lastAttempt = 0;
    }
}

BOOL UsageLimitExceeded(struct TRANSLATE *t,
                        struct USAGELIMITEXCEEDED *ule,
                        enum USAGELIMITEXCEEDEDTYPE ulet) {
    struct USAGELIMIT *ul;
    struct USAGE *u;
    time_t now = 0;

    if (t && t->session && t->session->priv == 0 && SecurityUserBlocked(t->session->logUserBrief)) {
        return 1;
    }

    if (ule == NULL)
        return 0;

    if (ulet == ULEVOLUME && ule->enforcedCount + ule->monitoredCount == 0)
        return 0;

    if (ulet == ULEINTRUDER && ule->intruderUserAttemptsCount == 0)
        return 0;

    UUAcquireMutex(&mUsage);

    for (ul = usageLimits; ul; ul = ul->next) {
        if (ulet == ULEVOLUME) {
            if (ul == intruderUsageLimit)
                continue;
        } else {
            if (ul != intruderUsageLimit)
                continue;
        }
        if (ul->expires == 0)
            continue;
        FindUsage(&ul->usage, ule->user, 0, 1, &u, NULL);
        if (u == NULL || u->when == 0)
            continue;
        if (now == 0)
            UUtime(&now);
        if ((time_t)(u->when + ul->expires) <= now)
            ClearUsageLimitExceeded(ule, ul, u, "expiration");
    }

    UUReleaseMutex(&mUsage);

    if (t == NULL || t->session == NULL || (t->session)->priv == 0) {
        if (ulet == ULEVOLUME && ule->enforcedCount != 0)
            return 1;
        if (ulet == ULEINTRUDER && ule->intruderUserAttemptsCount != 0)
            return 1;
    }
    return 0;
}

/* PruneUsageDateHour assumes you hold the mUsage mutex */
void PruneUsageDateHour(struct USAGE *u, time_t oldest, unsigned int oldestInterval) {
    struct USAGEDATEHOUR *ud, *udLast, *udNext;

    if (oldest == 0) {
        UUtime(&oldest);
        oldest -= oldestInterval;
    }

    for (ud = u->usageDateHours, udLast = NULL; ud; udLast = ud, ud = ud->next) {
        if (ud->dateHour < oldest) {
            if (udLast)
                udLast->next = NULL;
            else
                u->usageDateHours = NULL;
            for (; ud; ud = udNext) {
                udNext = ud->next;
                u->transfers -= ud->transfers;
                u->recvKBytes -= ud->recvKBytes;
                u->intruderUserAttempts -= ud->intruderUserAttempts;
                FreeThenNull(ud);
            }
            break;
        }
    }
}

/* PruneUsage assumes you hold the mUsage mutex */
void PruneUsage(struct USAGE **head, unsigned int oldestInterval) {
    struct USAGE *u, *uLast, *uNext;
    time_t oldest;

    if (head == NULL)
        return;

    UUtime(&oldest);
    oldest -= oldestInterval;

    for (u = *head, uLast = NULL; u; u = uNext) {
        uNext = u->next;
        PruneUsageDateHour(u, oldest, 0);

        /* If there is a u->when, we have to keep the usage record to preserve it.
           Not doing this lead to mystery users who were locked out as
           enforcedCount would go out-of-sync
        */
        if (u->usageDateHours || u->when) {
            uLast = u;
        } else {
            FreeThenNull(u->user);
            if (uLast == NULL)
                *head = uNext;
            else
                uLast->next = uNext;
            FreeThenNull(u);
        }
    }
}

void PruneUsageLimitExceeded(void) {
    struct USAGELIMITEXCEEDED *ule, *uleNext, *uleLast;
    struct USAGELIMIT *ul;
    struct USAGE *u;
    struct SESSION *e;
    int i;
    time_t when;

    /* We won't prune any UsageLimitExceeded accessed in the past 15 minutes */
    UUtime(&when);
    when -= 900; /* 15 * 60 */

    UUAcquireMutex(&mUsage);

    for (ul = usageLimits; ul; ul = ul->next) {
        PruneUsage(&ul->usage, ul->oldestInterval);
    }

    for (ule = usageLimitExceededs; ule; ule = ule->next) {
        ule->links = (ule->enforcedCount || ule->monitoredCount || ule->intruderUserAttemptsCount ||
                      ule->accessed > when)
                         ? 1
                         : 0;
    }

    for (e = sessions, i = 0; i < cSessions; e++, i++) {
        if (e->active && (ule = e->usageLimitExceeded))
            ule->links++;
    }

    for (ule = usageLimitExceededs; ule; ule = ule->next) {
        if (ule->links)
            continue;

        for (ul = usageLimits; ul; ul = ul->next) {
            for (u = ul->usage; u; u = u->next) {
                if (stricmp(ule->user, u->user) == 0) {
                    ule->links++;
                    goto ContinueOuter;
                }
            }
        }
    ContinueOuter:;
    }

    for (ule = usageLimitExceededs, uleLast = NULL; ule; ule = uleNext) {
        uleNext = ule->next;
        if (ule->links) {
            uleLast = ule;
            continue;
        }

        if (uleLast == NULL)
            usageLimitExceededs = uleNext;
        else
            uleLast->next = uleNext;

        FreeThenNull(ule->user);
        FreeThenNull(ule);
    }

    UUReleaseMutex(&mUsage);
}

void FindUsage(struct USAGE **head,
               char *user,
               time_t dateHour,
               BOOL haveMutex,
               struct USAGE **uReturn,
               struct USAGEDATEHOUR **udReturn) {
    struct USAGE *u, *uLast, *uNew;
    struct USAGEDATEHOUR *ud, *udLast, *udNew;
    int c;
    BOOL notFound;

    if (uReturn)
        *uReturn = NULL;

    if (udReturn)
        *udReturn = NULL;

    if (user == NULL || (usageLogUser & ULUUSAGELIMIT) == 0)
        return;

    if (haveMutex == 0)
        UUAcquireMutex(&mUsage);

    for (notFound = 1, u = *head, uLast = NULL; u; uLast = u, u = u->next) {
        c = stricmp(user, u->user);
        if (c == 0) {
            notFound = 0;
            break;
        }
        if (c < 0)
            break;
    }

    if (notFound) {
        if (!(uNew = calloc(1, sizeof(*uNew))))
            PANIC;
        if (!(uNew->user = strdup(user)))
            PANIC;
        uNew->next = u;
        if (uLast)
            uLast->next = uNew;
        else
            *head = uNew;
        u = uNew;
    }

    if (uReturn)
        *uReturn = u;

    if (dateHour && udReturn) {
        for (notFound = 1, ud = u->usageDateHours, udLast = NULL; ud; udLast = ud, ud = ud->next) {
            if (dateHour == ud->dateHour) {
                notFound = 0;
                break;
            }
            if (dateHour > ud->dateHour)
                break;
        }
        if (notFound) {
            if (!(udNew = calloc(1, sizeof(*udNew))))
                PANIC;
            udNew->dateHour = dateHour;
            udNew->next = ud;
            if (udLast)
                udLast->next = udNew;
            else
                u->usageDateHours = udNew;
            ud = udNew;
        }
        *udReturn = ud;
    }

    if (haveMutex == 0)
        UUReleaseMutex(&mUsage);
}

struct USAGELIMIT *FindUsageLimit(char *name, BOOL add) {
    struct USAGELIMIT *ul, *last;

    for (ul = usageLimits, last = NULL; ul; last = ul, ul = ul->next) {
        if (stricmp(ul->name, name) == 0)
            break;
    }

    if (ul == NULL && add) {
        if (!(ul = calloc(1, sizeof(*ul))))
            PANIC;
        if (!(ul->name = strdup(name)))
            PANIC;
        ul->interval = ONE_DAY_IN_SECONDS; /* default interval one day */
        if (last)
            last->next = ul;
        else
            usageLimits = ul;
    }
    return ul;
}

struct USAGELIMITEXCEEDED *FindUsageLimitExceeded(char *user, BOOL create, BOOL haveMutex) {
    struct USAGELIMITEXCEEDED *ule, *last, *n;
    int c;

    if (user == NULL || *user == 0 || (usageLogUser & ULUUSAGELIMIT) == 0)
        return NULL;

    if (haveMutex == 0)
        UUAcquireMutex(&mUsage);

    for (ule = usageLimitExceededs, last = NULL; ule; last = ule, ule = ule->next) {
        c = stricmp(user, ule->user);
        if (c == 0)
            goto Finished;
        if (c < 0)
            break;
    }

    if (create == 0) {
        ule = NULL;
        goto Finished;
    }

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;
    if (!(n->user = strdup(user)))
        PANIC;
    AllLower(n->user);

    n->next = ule;
    if (last)
        last->next = n;
    else
        usageLimitExceededs = n;

    ule = n;

Finished:
    if (ule)
        UUtime(&ule->accessed);

    if (haveMutex == 0)
        UUReleaseMutex(&mUsage);

    return ule;
}

struct INTRUDERIP *FindIntruderIP(struct TRANSLATE *t,
                                  struct sockaddr_storage *inIp,
                                  struct sockaddr_storage *inForwardedIp,
                                  BOOL add) {
    struct sockaddr_storage ip, forwardedIp;
    struct INTRUDERIP *bg, *last, *n;
    char *p;
    struct INTRUDERIPATTEMPT *a;
    BOOL holdingMutex = 0;

    if (intruderIPAttempts == NULL) {
        return NULL;
    }

    if (inIp) {
        memcpy(&ip, inIp, get_size(inIp));
    } else {
        init_storage(&ip, AF_INET, 1, 0);
    }

    if (inForwardedIp) {
        memcpy(&forwardedIp, inForwardedIp, get_size(inForwardedIp));
    } else {
        init_storage(&forwardedIp, AF_INET, 1, 0);
    }

    if (t) {
        if (t->intruderIP)
            return t->intruderIP;

        memcpy(&ip, &t->sin_addr, get_size(&t->sin_addr));
        // Init to 0.0.0.0
        init_storage(&forwardedIp, AF_INET, 1, 0);
    }

    for (a = intruderIPAttempts;; a = a->next) {
        /* Haven't acquired mutex yet, so it's ok to return direct */
        if (a == NULL) {
            return NULL;
        }
        if (!a->haveIpRange) {
            break;
        }
        if (compare_ip(&ip, &a->start) >= 0 && compare_ip(&a->stop, &ip) >= 0)
            break;
    }

    if (!a->xForwardedFor) {
        init_storage(&forwardedIp, AF_INET, 1, 0);
    } else {
        if (t) {
            if (p = FindField("X-Forwarded-For", t->requestHeaders)) {
                LinkFields(p);

                inet_pton_st2(p, &forwardedIp);
                // forwardedIp = ntohl(inet_addr(p));

                if (is_addrnone(&forwardedIp)) {
                    init_storage(&forwardedIp, AF_INET, 1, 0);
                }
            }
        }
    }

    if (add) {
        UUAcquireMutex(&mIntruders);
        holdingMutex = 1;
    }

    for (bg = intruderIPs, last = NULL; bg; last = bg, bg = bg->next) {
        if (is_ip_equal(&ip, &bg->ip) && is_ip_equal(&forwardedIp, &bg->forwardedIp)) {
            goto Finished;
        }
        if (compare_ip(&ip, &bg->ip) > 0) {
            continue;
        }
        if (compare_ip(&bg->ip, &ip) > 0) {
            break;
        }
        /* On the ip == bg->ip case, consider the forwardedIp */
        if (compare_ip(&forwardedIp, &bg->forwardedIp) > 0) {
            continue;
        }
        break;
    }

    if (add) {
        if (!(n = calloc(1, sizeof(*n))))
            PANIC;
        memcpy(&n->ip, &ip, get_size(&ip));
        memcpy(&n->forwardedIp, &forwardedIp, get_size(&forwardedIp));
        n->maxAttempts = a->maxAttempts;
        n->rejectAttempts = a->rejectAttempts;
        n->intruderInterval = a->intruderInterval;
        n->intruderExpires = a->intruderExpires;
        n->next = bg;

        ipToStr(&n->ip, n->ipText, sizeof(n->ipText));

        if (!is_ip_zero(&forwardedIp)) {
            strcat(n->ipText, "/");
            char buffer[INET6_ADDRSTRLEN];
            ipToStr(&forwardedIp, buffer, sizeof(buffer));
            strcat(n->ipText, buffer);
        }

        if (last)
            last->next = n;
        else
            intruderIPs = n;
        bg = n;
    } else {
        bg = NULL;
    }

Finished:
    if (t && bg)
        t->intruderIP = bg;

    if (holdingMutex)
        UUReleaseMutex(&mIntruders);

    return bg;
}

/* PruneIntruderIP assumes you hold mIntruders;
   it is mainly designed to be called from LoadUsage()
   as it may be unsafe to call at other times (other
   threads may be using contents that are being freed)
*/
void PruneIntruderIP(void) {
    struct INTRUDERIP *bg, *last, *next;
    time_t now;

    UUtime(&now);

    for (bg = intruderIPs, last = NULL; bg; bg = next) {
        next = bg->next;
        if (IntruderIPLevel(bg, now) == 0) {
            if (last == NULL)
                intruderIPs = next;
            else
                last->next = next;
            FreeThenNull(bg);
            continue;
        }
        last = bg;
    }
}

int IntruderIPLevel(struct INTRUDERIP *bg, time_t now) {
    /* If no last attempt, then no intrusion being attempted */
    if (bg == NULL || bg->lastAttempt == 0)
        return 0;

    if (bg->rejectAttempts && bg->attempts >= bg->rejectAttempts) {
        return 3;
    }

    if (now == 0) {
        UUtime(&now);
    }

    /* If we are below the intrusion threshhold */

    if (bg->attempts < bg->maxAttempts) {
        if (bg->lastAttempt + bg->intruderInterval < now)
            return 0;
        return 1;
    }

    /* At or above the intrusion level */
    if (bg->lastAttempt + bg->intruderExpires < now)
        return 0;
    return 2;
}

void NoteIntruder(struct TRANSLATE *t, char *user) {
    struct INTRUDERIP *bg;
    time_t now;
    BOOL reportStoppedLogging = 0;
    struct USAGELIMITEXCEEDED *ule;
    struct USAGE *u;
    struct USAGEDATEHOUR *ud;
    time_t dateHour;
    struct USAGELIMIT *ul = intruderUsageLimit;

    UUtime(&now);

    if (ul && user && *user) {
        UUAcquireMutex(&mUsage);
        if (ule = FindUsageLimitExceeded(user, 1, 1)) {
            UUtime(&now);
            dateHour = ((time_t)(now / ul->granularity)) * ul->granularity;
            FindUsage(&(ul->usage), ule->user, dateHour, 1, &u, &ud);
            if (u && ud) {
                PruneUsageDateHour(u, 0, ul->oldestInterval);
                u->lastAttempt = now;
                u->intruderUserAttempts++;
                ud->intruderUserAttempts++;
                if (u->when == 0) {
                    if (u->intruderUserAttempts >= ul->maxIntruderUserAttempts) {
                        if (auditEventsMask & AUDITLOGININTRUDERUSERMASK) {
                            AuditEvent(t, AUDITLOGININTRUDERUSER, user, NULL,
                                       "%d failed login attempt%s; evading username",
                                       u->intruderUserAttempts, SIfNotOne(u->intruderUserAttempts));
                        } else {
                            Log("Login attempts for username %s exceed limit; evading username",
                                user);
                        }
                        ule->intruderUserAttemptsCount++;
                        u->when = now;
                    }
                } else {
                    u->when = now;
                }
            }
        }
        UUReleaseMutex(&mUsage);
    }

    if (intruderIPAttempts == NULL)
        return;

    bg = FindIntruderIP(t, 0, 0, 1);
    if (bg == NULL)
        return;

    UUAcquireMutex(&mIntruders);
    UUtime(&now);
    if (bg->lastAttempt && IntruderIPLevel(bg, now) == 0) {
        bg->attempts = 1;
    } else
        bg->attempts++;
    if (bg->attempts == bg->rejectAttempts)
        NeedSaveUsage();
    bg->lastAttempt = now;
    if (bg->stoppedLogging == 0 && intruderLog > 0 &&
        bg->attempts >= bg->maxAttempts + intruderLog) {
        bg->stoppedLogging = 1;
        reportStoppedLogging = 1;
    }
    UUReleaseMutex(&mIntruders);

    if (reportStoppedLogging) {
        Log("Login attempts for %s exceed IntruderLog value; further attempts will not be logged",
            bg->ipText);
    }
}

enum INTRUDERSTATUS IsIntruder(struct TRANSLATE *t, char *user, char *retIpBuffer) {
    struct INTRUDERIP *bg;
    enum INTRUDERSTATUS is = INTRUDERNOT;
    BOOL reportStoppedLogging = 0;
    time_t lastAttempt;
    char lastAttemptBuffer[MAXASCIIDATE];
    int attempts = 0;

    if (retIpBuffer)
        *retIpBuffer = 0;

    if (intruderIPAttempts == 0)
        goto SkipIP;

    bg = FindIntruderIP(t, 0, 0, 0);
    if (bg == NULL)
        goto SkipIP;

    UUAcquireMutex(&mIntruders);

    if (bg->lastAttempt && IntruderIPLevel(bg, 0) == 0) {
        if (bg->stoppedLogging) {
            bg->stoppedLogging = 0;
            reportStoppedLogging = 1;
            lastAttempt = bg->lastAttempt;
            attempts = bg->attempts;
        }
        bg->lastAttempt = 0;
        bg->attempts = 0;
    }
    if (bg->attempts >= bg->maxAttempts) {
        if (bg->attempts >= bg->maxAttempts + intruderReject)
            is = INTRUDERREJECT;
        else
            is = bg->stoppedLogging ? INTRUDERNOLOG : INTRUDEREVADE;
    }

    UUReleaseMutex(&mIntruders);

    if (retIpBuffer)
        strcpy(retIpBuffer, bg->ipText);

    if (reportStoppedLogging) {
        if (auditEventsMask & AUDITLOGININTRUDERUSERMASK) {
            AuditEvent(t, AUDITLOGININTRUDERUSER, user, NULL,
                       "attempts stopped after %d total attempts", attempts);
        } else {
            Log("Login attempts from %s stopped at %s after %d total attempts", bg->ipText,
                ASCIIDate(&lastAttempt, lastAttemptBuffer), attempts);
        }
    }

SkipIP:
    if (intrusionEvade) {
        enum IntrusionIPStatus iis = INTRUSION_IP_NONE;

        iis = IntrusionIPStatus(&t->sin_addr, 1);
        if (iis == INTRUSION_IP_BAD_IP && intrusionEnforce && is == INTRUDERNOT) {
            is = INTRUDEREVADE;
            if (retIpBuffer && *retIpBuffer == 0) {
                ipToStr(&t->sin_addr, retIpBuffer, INET6_ADDRSTRLEN);
            }
        }
    }

    if (is == INTRUDERNOT && intruderUsageLimit && user && *user) {
        /* For UsageLimitExceeded, you must NOT hold the mUsage mutex!!! */
        if (UsageLimitExceeded(NULL, FindUsageLimitExceeded(user, 0, 0), ULEINTRUDER))
            is = INTRUDERNOLOG;
    }

    return is;
}

/* This logic automatically screens out anonymous sessions */
struct SESSION *FindNextSessionByIP(struct SESSION *start, struct sockaddr_storage *sa) {
    struct SESSION *e;
    struct SESSION *lastSession = sessions + cSessions;

    // check if the address is 0
    if (is_ip_zero(sa))
        return NULL;

    e = (start == NULL ? sessions : start + 1);

    for (;; e++) {
        if (e >= lastSession)
            return NULL;
        if (e->active == 1 && is_ip_equal(&e->sin_addr, sa) && e->anonymous == 0)
            return e;
    }
}

time_t IPLastActive(struct sockaddr_storage *sa,
                    BOOL mostRecent,
                    struct SESSION **which,
                    GROUPMASK gm) {
    struct SESSION *e;
    time_t last = 0;

    if (which)
        *which = NULL;

    for (e = NULL; e = FindNextSessionByIP(e, sa);) {
        if (gm && GroupMaskOverlap(e->gm, gm) == 0)
            continue;
        if (e->accessed > last) {
            last = e->accessed;
            if (which)
                *which = e;
            if (mostRecent == 0)
                break;
        }
    }
    return last;
}

int UpdateAccessByIP(struct sockaddr_storage *sin_addr) {
    time_t now;
    int count = 0;
    struct SESSION *e;

    UUtime(&now);
    for (e = NULL; e = FindNextSessionByIP(e, sin_addr);) {
        e->accessed = now;
        count++;
    }
    return count;
}
