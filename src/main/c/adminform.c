#define __UUFILE__ "adminform.c"

#include "common.h"
#include "uustring.h"

#include "adminform.h"
#include "adminlogin.h"

void AdminForm(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    char destWithLogin[4096 + 32];
    char *dest;
    char *url = NULL;
    char *auth = NULL;
    char *tag = NULL;
    char cat[2];

    if (query == NULL)
        query = "";

    if (post == NULL)
        post = "";

    if (strlen(query) + strlen(post) > sizeof(destWithLogin) - 512) {
        HTTPCode(t, 413, 1);
        goto Finished;
    }

    strcpy(destWithLogin, "/login?");

    dest = strchr(destWithLogin, 0);

    if (auth = GetSnipField(query, "auth", 1, 1)) {
        if (strlen(auth) > 32)
            *(auth + 32) = 0;
        strcat(destWithLogin, "auth=");
        AddEncodedField(destWithLogin, auth, NULL);
        strcat(destWithLogin, "&");
        FreeThenNull(auth);
    }

    if (tag = GetSnipField(query, "tag", 1, 1)) {
        if (strlen(tag) > 32)
            *(tag + 32) = 0;
        strcat(destWithLogin, "tag=");
        AddEncodedField(destWithLogin, tag, NULL);
        strcat(destWithLogin, "&");
        FreeThenNull(tag);
    }

    strcat(destWithLogin, "url=");

    url = GetSnipField(query, "qurl", 1, 1);
    if (url == NULL)
        url = GetSnipField(post, "qurl", 1, 1);

    if (url == NULL) {
        HTMLHeader(t, htmlHeaderOmitCache);
        UUSendZ(s, "qurl missing\n");
        goto Finished;
    }

    strcat(dest, url);

    cat[0] = (strchr(dest, '?') ? '&' : '?');
    cat[1] = 0;

    if (*query) {
        strcat(dest, cat);
        cat[0] = '&';
        strcat(dest, query);
    }

    if (*post) {
        strcat(dest, cat);
        cat[0] = '&';
        strcat(dest, post);
    }

    if (HAReroute(t, 1, destWithLogin, sizeof(destWithLogin)))
        return;

    AdminLogin(t, dest, NULL);

Finished:
    FreeThenNull(url);

    return;
}
