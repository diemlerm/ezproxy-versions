#define __UUFILE__ "adminoverdrive.c"

#include "common.h"
#include "uustring.h"
#include "usertoken.h"

#include "adminoverdrive.h"
struct OVERDRIVESITE *AdminOverDriveSite(char *site, size_t siteLen) {
    struct OVERDRIVESITE *ods;

    if (siteLen == 0)
        siteLen = FirstSlashQuestion(site) - site;

    for (ods = overDriveSites; ods; ods = ods->next) {
        if (strnicmp(ods->site, site, siteLen) == 0 && strlen(ods->site) == siteLen)
            break;
    }

    return ods;
}

void AdminOverDrive(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    char *site;
    char *userToken = NULL;
    struct SESSION *e;
    struct OVERDRIVESITE *ods;
    const int AOURL = 0;
    struct FORMFIELD ffs[] = {
        {"URL", NULL, 0, 0, 4096},
        {NULL, NULL, 0, 0}
    };
    char *landingURL;
    char *p, *stop;
    time_t now;
    struct tm rtm;
    char timeStamp[32];
    EVP_MD_CTX *mdctx = NULL;
    unsigned char md_value[EVP_MAX_MD_SIZE];
    unsigned int md_len;
    char *hash = NULL;

    FindFormFields(query, post, ffs);

    landingURL = ffs[AOURL].value;

    if ((site = StartsIWith(t->urlCopy, "/overdrive/")) == NULL || *site == 0) {
        HTTPCode(t, 404, 1);
        goto Finished;
    }

    ods = AdminOverDriveSite(site, 0);

    if (ods == NULL) {
        HTMLHeader(t, htmlHeaderOmitCache);
        UUSendZ(s, "EZproxy is not configured for this OverDrive site");
        goto Finished;
    }

    if ((e = t->session) == NULL) {
        AdminRelogin(t);
        goto Finished;
    }

    if (!GroupMaskOverlap(e->gm, ods->gm)) {
        AdminLogup(t, TRUE, NULL, NULL, ods->gm, FALSE);
        goto Finished;
    }

    if (e->autoLoginBy != autoLoginByNone) {
        HTMLHeader(t, htmlHeaderOmitCache);
        UUSendZ(s, "OverDrive access does not support AutoLoginIP or referring URL authentication");
        goto Finished;
    }

    if (ods->noTokens) {
        if (t->session->logUserBrief) {
            if (!(userToken = strdup((t->session)->logUserBrief)))
                PANIC;
            AllLower(userToken);
            for (p = strchr(userToken, 0) - 1; p >= userToken; p--) {
                if (*p < ' ' || *p > 126)
                    StrCpyOverlap(p, p + 1);
            }
            if (*userToken == 0)
                FreeThenNull(userToken);
        }
    } else {
        userToken = UserToken2("ods", (t->session)->logUserBrief, 1);
    }

    if (userToken == NULL) {
        AdminNoToken(t);
        goto Finished;
    }

    UUtime(&now);
    UUgmtime_r(&now, &rtm);
    strftime(timeStamp, sizeof(timeStamp), "%Y-%m-%dT%H:%M:%SZ", &rtm);

    if (!(mdctx = EVP_MD_CTX_new()))
        PANIC;
    EVP_DigestInit(mdctx, EVP_sha1());
    EVP_DigestUpdate(mdctx, userToken, strlen(userToken));
    EVP_DigestUpdate(mdctx, "|", 1);
    EVP_DigestUpdate(mdctx, timeStamp, strlen(timeStamp));
    EVP_DigestUpdate(mdctx, "|", 1);
    EVP_DigestUpdate(mdctx, ods->secret, strlen(ods->secret));
    EVP_DigestFinal(mdctx, md_value, &md_len);

    hash = DigestToHex(NULL, md_value, md_len, 1);

    stop = t->buffer + sizeof(t->buffer) - 256;

    strcpy(t->buffer, ods->url);
    /* If the final character isn't a trailing slash, make it one */
    p = strchr(t->buffer, 0) - 1;
    if (*p++ != '/') {
        *p++ = '/';
        *p = 0;
    }

    strcat(p, "BANGAuthenticate.dll?Action=ExternalAuth&PatronID=");
    p = AddEncodedField(strchr(p, 0), userToken, stop);
    strcat(p, "&Timestamp=");
    p = AddEncodedField(strchr(p, 0), timeStamp, stop);
    strcat(p, "&Hash=");
    p = AddEncodedField(strchr(p, 0), hash, stop);
    if (landingURL) {
        strcat(p, "&URL=");
        p = AddEncodedField(strchr(p, 0), landingURL, stop);
    }
    if (ods->ilsName) {
        strcat(p, "&ILSName=");
        p = AddEncodedField(strchr(p, 0), ods->ilsName, stop);
    }
    if (ods->libraryID) {
        strcat(p, "&LibraryID=");
        p = AddEncodedField(strchr(p, 0), ods->libraryID, stop);
    }

    LocalRedirect(t, t->buffer, 0, 0, NULL);

Finished:
    if (mdctx) {
        EVP_MD_CTX_free(mdctx);
        mdctx = NULL;
    }
    FreeThenNull(userToken);
    FreeThenNull(hash);
}
