#define __UUFILE__ "translate.c"

#include "common.h"
#include "identifier.h"
#include "usr.h"

#include "adminaudit.h"
#include "adminsliptest.h"
#include "security.h"

/* Define the maximum size a pattern buffer might need to be for editing
   Must exceed the length of any match string or replace string
*/

#define MAXPATTERNBUFFER 80

/* There are stubs to allow ^^ and ^p to mean insert a ^ and the port number of current virtual
   server, but they aren't implemented yet
*/

size_t maxMatchLen = 0;
size_t maxReplaceLen = 0;

void ResetTranslate(struct TRANSLATE *t) {
    t->session = NULL;
    t->needPdfRefresh = 0;
    t->isMSIE = 0;
    memset(&t->requestHeadersBuffer, 0, sizeof(t->requestHeadersBuffer));
    t->requestHeaders = t->requestHeadersBuffer;
    t->requireAuthenticate = REQUIREAUTHENTICATENEVER;
    t->raw = 0;
    *t->url = 0;
    *t->urlCopy = 0;
    *t->contentType = 0;
    t->contentLength = -1;
    t->proxyType = optionProxyType;
    UUtime(&t->activated);
    t->finalStatus = 900;
    t->reject = 0;
    t->suppressPostContent = 0;
    t->isPDF = 0;
    t->rewritingDisabled = 0;
    t->eblSecretTimestamp = 0;
    t->safari = 0;
    t->ignoreIntrusion = 0;
    t->expect100Continue = 0;
    t->excludeIPBanner = 0;
    t->excludeIPBannerSuppress = 0;
    t->sendChunkingAllowed = 0;
    t->sendContentLengthSent = 0;
    t->sendZipAllowed = 0;
    t->clientVersionX1000 = 0;
    t->serverVersionX1000 = 0;
    t->maybeLogin = FALSE;
    /* Begin in the "start" state */
    t->findReplaceStates = 1;
    t->bodylessStatus = 0;
    t->sentKeepAlive = 0;
    t->alreadyEnded = 0;
    t->denyClientKeepAlive = 0;
    t->redirectSafeOrNeverProxyCache = redirectSafeOrNeverProxyCacheUninitialized;
    t->csrfToken[0] = 0;
    /* These are initialized before the translate is actually assigned
    t->intruderIP = NULL;
    t->intruderIPLevel = -1;
    */
}

/* If anything beside a single call from main every uses this,
   intruderIP and intruderIPLevel may need to be handled differently
*/
void InitTranslate(struct TRANSLATE *t, struct HOST *h) {
    static int sequence = 0;

    t->wsocketPtr = &t->wsocketTemp;
    UUInitSocket(t->wsocketPtr, INVALID_SOCKET);
    UUInitSocket(&t->ftpsocket, INVALID_SOCKET);
    t->ssocketInitialized = 0;
    t->host = h;
    t->acceptSslIdx = 0;
    t->keepAlivesRemaining = maxKeepAliveRequests - 1;
    InitLocation(&t->location);
    t->sequence = sequence++;
    if (t->localVariables == NULL)
        t->localVariables = VariablesNew("local", FALSE);
    ResetTranslate(t);
    UUAcquireMutex(&mActive);
    t->active = 1;
    t->urlRawRequested[0] = '\0';
    UUReleaseMutex(&mActive);
}

void EndTranslate(struct TRANSLATE *t, BOOL logRequest) {
    struct DATABASE *b;
    struct SESSION *e;
    struct DATABASEUSAGELIMIT *dbul;
    struct USAGELIMIT *ul;
    struct USAGELIMITEXCEEDED *ule;
    struct USAGE *u;
    struct USAGEDATEHOUR *ud;
    time_t now, dateHour;
    BOOL exceeded, anyExceeded = 0;

    if (t->alreadyEnded)
        return;

    t->alreadyEnded = 1;

    if (logRequest) {
        LogRequest(t, logSPUs, NULL, NULL, NULL);
        SecurityEnqueueRequest(t);

        if (usageLimits && t->host && (e = t->session) && e->logUserFull) {
            if (t->host == hosts)
                dbul = localDatabaseUsageLimits;
            else
                dbul = (b = (t->host)->myDb) ? b->databaseUsageLimits : NULL;
            if (dbul) {
                UUtime(&now);
                UUAcquireMutex(&mUsage);
                for (; dbul; dbul = dbul->next) {
                    ul = dbul->usageLimit;
                    if (ul->ignoreNormalLogin && e->autoLoginBy == autoLoginByNone)
                        continue;
                    if (ul->ignoreAutoLoginIP && e->autoLoginBy == autoLoginByIP)
                        continue;
                    if (ul->ignoreRefererLogin && e->autoLoginBy == autoLoginByReferer)
                        continue;
                    dateHour = ((time_t)(now / ul->granularity)) * ul->granularity;
                    FindUsage(&(ul->usage), e->logUserFull, dateHour, 1, &u, &ud);
                    if (u && ud) {
                        PruneUsageDateHour(u, 0, ul->oldestInterval);
                        u->transfers++;
                        ud->transfers++;
                        u->recvKBytes += (t->ssocket).sendCount / 1000;
                        ud->recvKBytes += (t->ssocket).sendCount / 1000;
                        if ((ule = e->usageLimitExceeded) && u->when == 0) {
                            char *priv = e->priv ? " (administrative user; not suspended)" : "";
                            exceeded = 0;
                            if (ul->maxTransfers && ul->maxTransfers < u->transfers) {
                                if (auditEventsMask & AUDITUSAGELIMITMASK)
                                    AuditEvent(t, AUDITUSAGELIMIT, e->logUserFull, e,
                                               "%s %d transfer exceeded%s", ul->name,
                                               ul->maxTransfers, priv);
                                else
                                    Log("%s %d transfer usage limit exceeded by %s%s", ul->name,
                                        ul->maxTransfers, e->logUserFull, priv);
                                exceeded = 1;
                            } else if (ul->maxRecvKBytes && ul->maxRecvKBytes < u->recvKBytes) {
                                char mb[MAXASCIIKMB];
                                if (auditEventsMask & AUDITUSAGELIMITMASK)
                                    AuditEvent(t, AUDITUSAGELIMIT, e->logUserFull, e,
                                               "%s %sMB transfer exceeded%s", ul->name,
                                               ASCIIKMB(ul->maxRecvKBytes, -1, mb), priv);
                                else
                                    Log("%s %sMB usage limit exceeded by %s%s", ul->name,
                                        ASCIIKMB(ul->maxRecvKBytes, -1, mb), e->logUserFull, priv);
                                exceeded = 1;
                            }
                            if (exceeded) {
                                if (ul->enforce) {
                                    ule->enforcedCount++;
                                    anyExceeded = 1;
                                } else {
                                    ule->monitoredCount++;
                                }
                                u->when = now;
                            }
                        }
                    }
                }
                UUReleaseMutex(&mUsage);
            }
        }
        if (anyExceeded)
            NeedSaveUsage();
    }

    if (t->requestHeaders != t->requestHeadersBuffer && t->requestHeaders != NULL) {
        free(t->requestHeaders);
        t->requestHeaders = t->requestHeadersBuffer;
    }

    FreeThenNull(t->museCookie);
    FreeThenNull(t->museReferer);
    FreeThenNull(t->logTag);
    FreeThenNull(t->fiddlerRequest);
    t->logTagApproved = 0;
    t->logTagSpur = 0;
    VariablesReset(t->localVariables);

    if (t->docsCustomDir) {
        /* This is from a StaticStringCopy, don't free, just NULL out pointer */
        t->docsCustomDir = NULL;
    }
}

void ReleaseTranslate(struct TRANSLATE *t) {
    UUAcquireMutex(&mActive);
    t->active = 0;
    UUReleaseMutex(&mActive);
}

void InitPatterns(struct PATTERN *pp, BOOL freeOldReplaceString) {
    char tempPattern[512];
    char *s, *t, *e;
    struct HOST *h;

    if (pp == NULL)
        return;

    for (; pp; pp = pp->next) {
        pp->replaceLen = 0;
        for (s = pp->replaceString, t = tempPattern; *s; s++) {
            if (*s == '^') {
                s++;
                if (*s == 0)
                    break;
                /* The first ^ is future expansion to distinguish for ^p */
                if (*s == '^') {
                    /* *t++ = '^'; */
                    *t++ = '^';
                    continue;
                }

                if (*s == 'C' || *s == 'c') {
                    s++;
                    if (*s == '^')
                        continue;
                    e = strchr(s, '^');
                    if (e == NULL) {
                        Log("Replace ignored ^c without closing ^");
                        continue;
                    }
                    if (optionProxyType == PTHOST)
                        strcpy(t, myDomain);
                    else {
                        strncpy(t, s, e - s);
                        *(t + (e - s)) = 0;
                        ApplyProxyHostnameEdits(t);
                        t = strchr(t, 0);
                        *t++ = '.';
                        strcpy(t, myName);
                    }
                    t = strchr(t, 0);
                    s = e - 1;
                    continue;
                }

                if (*s == 'P' || *s == 'p') {
                    /* Allow ^phostname^ to be written to port number for the insecure version of
                     * the host */
                    /* (see also ^S) */
                    s++;
                    /* check for ^p^ which becomes my main URL */
                    if (*s == '^') {
                        if (myUrlHttp) {
                            strcpy(t, myUrlHttp);
                            t = strchr(t, 0);
                        }
                        continue;
                    }
                    e = strchr(s, '^');
                    if (e == NULL) {
                        Log("Replace ignored ^p without closing ^: %s", pp->replaceString);
                        continue;
                    }
                    strncpy(t, s, e - s);
                    *(t + (e - s)) = 0;
                    if (!UUIsRFC952Hostname(t, 1, 1)) {
                        Log("Replace ignored non-RFC952 hostname in ^p: %s", t);
                    } else {
                        h = FindHost(t, 0, 0);
                        if (h != NULL) {
                            if (optionProxyType == PTHOST)
                                strcpy(t, h->proxyHostname);
                            else
                                sprintf(t, "%s:%d", myName, h->myPort);
                        }
                    }
                    t = strchr(t, 0);
                    s = e - 1;
                    continue;
                }

                if (*s == 'S' || *s == 's') {
                    /* Allow ^shostname^ to be written to port number for the secure version of the
                     * host */
                    /* (see also ^P) */
                    s++;
                    /* check for ^s^ which becomes my main URL in https */
                    if (*s == '^') {
                        if (myUrlHttps) {
                            strcpy(t, myUrlHttp);
                            t = strchr(t, 0);
                        }
                        continue;
                    }
                    e = strchr(s, '^');
                    if (e == NULL) {
                        Log("Replace ignored ^s without closing ^: %s", pp->replaceString);
                        continue;
                    }
                    strncpy(t, s, e - s);
                    *(t + (e - s)) = 0;
                    if (!UUIsRFC952Hostname(t, 1, 1)) {
                        Log("Replace ignored non-RFC952 hostname in ^s: %s", t);
                    } else {
                        h = FindHost(t, 1, 0);
                        if (h != NULL) {
                            if (optionProxyType == PTHOST)
                                strcpy(t, h->proxyHostname);
                            else
                                sprintf(t, "%s:%d", myName, h->myPort);
                        }
                    }
                    t = strchr(t, 0);
                    s = e - 1;
                    continue;
                }

                if (*s == 'H' || *s == 'h') {
                    strcpy(t, myName);
                    t = strchr(t, 0);
                    continue;
                }
                if (*s == 'D' || *s == 'd') {
                    strcpy(t, myDomain + (*myDomain == '.' ? 1 : 0));
                    t = strchr(t, 0);
                    continue;
                }
                if (*s == 'L' || *s == 'l') {
                    strcpy(t, myDomain);
                    t = strchr(t, 0);
                    continue;
                }
                if (*s == 'a' || *s == 'A') {
                    *t++ = 1;
                    continue;
                }
            }
            *t++ = *s;
        }
        *t = 0;
        pp->replaceLen += strlen(tempPattern);
        pp->originalReplaceString = pp->replaceString;
        if (strcmp(pp->replaceString, tempPattern) != 0) {
            if (!(pp->replaceString = strdup(tempPattern)))
                PANIC;
        }

        pp->matchLen = strlen(pp->matchString);
        if (pp->matchLen > maxMatchLen)
            maxMatchLen = pp->matchLen;
        if (pp->replaceLen > maxReplaceLen)
            maxReplaceLen = pp->replaceLen;
    }
}

void InitAllPatterns(void) {
    struct DATABASE *b;
    int i;

    /* InitPatterns(globalPatterns, 0); */
    for (i = 0, b = databases; i < cDatabases; i++, b++)
        InitPatterns(b->patterns.pattern, 1);
}

char *NextEOL(char *s, int len) {
    for (; len > 0; s++, len--) {
        if (*s == 13 || *s == 10)
            return s;
        /*
        if (*s > 0 && *s < ' ')
            return s;
        */
    }

    return NULL;
}

void CheckPatterns(struct TRANSLATE *t, char **inbuffMin, char **inbuffEnd, BOOL finalText) {
    struct PATTERN *pp;
    size_t curLen, lineLen;
    char *start;
    struct PATTERN *dbPatterns;
    char *nextEOL = NULL;
    /* int i; */

    dbPatterns = (t && t->host && (t->host)->myDb ? ((t->host)->myDb)->patterns.pattern : NULL);

    if (*inbuffMin >= *inbuffEnd)
        return;

    start = *inbuffMin;
    curLen = *inbuffEnd - start;

    nextEOL = NextEOL(start, curLen);

    for (;; start++, curLen--) {
        /* OK, here's what this if is considering:
           if curLen == 0, then there is nothing left to examine and we stop
           if there is something left, then we only want to consider it under one these
           circumstances:
           1. if there is more left than our maximum match pattern (don't want a shorter
              but lower priority pattern to get to match just because we don't have enough
              data in the buffer)
           2. if there is an end-of-line character remaining, since we don't test beyond those
              boundaries, making the length argument irrelevant (lineLen below let's us avoid
              that lower-priority issue since we don't peek beyond the current line unless
              we are in a partial, unterminated line that meet criterion 1)
           3. if we are at end-of-stream, so the short-string argument is irrelevant
        */
        if (curLen == 0 || (curLen < maxMatchLen && nextEOL == NULL && finalText == 0)) {
            *inbuffMin = start;
            return;
        }

        if (start == nextEOL) {
            nextEOL = NextEOL(nextEOL + 1, curLen - 1);
            continue;
        }

        lineLen = (nextEOL ? nextEOL - start : curLen);

        /*
            for (i = 0; i < 2; i++) {
                pp = (i == 0 ? dbPatterns : globalPatterns);
                if (pp == NULL)
                    continue;
        */

        for (pp = dbPatterns; pp; pp = pp->next) {
            if (pp->matchString == NULL) {
                break;
            }
            if (pp->matchLen > lineLen)
                continue;
            if (strncmp(pp->matchString, start, pp->matchLen) != 0)
                continue;
            if (pp->currentStates != (pp->currentStates & t->findReplaceStates))
                continue;
            t->findReplaceStates = (t->findReplaceStates | pp->addStates) & pp->removeStates;
            memmove(start + pp->replaceLen, start + pp->matchLen, curLen - pp->matchLen + 1);
            memmove(start, pp->replaceString, pp->replaceLen);
            *inbuffMin = start + pp->replaceLen;
            *inbuffEnd = *inbuffEnd + (pp->replaceLen - pp->matchLen);
            /* We only perform a maximum of one replacement per call to CheckPatterns
               since doing more could cause a buffer overflow */
            return;
        }
        /*
        }
        */
    }
}

void ProcessAttribute(struct TRANSLATE *t,
                      char *tag,
                      char *attr,
                      char *attrVal,
                      int *refreshTag,
                      int *ezproxyRewritingTag,
                      int currentTag,
                      BOOL mondoValue) {
    char *edits[] = {"href", "background", "action", "src", "usemap", "codebase", NULL};
    char **editp;
    char *refreshURL;

    /* Log("%s process *%s*%s*%s*", t->url, tag, attr, attrVal); */

    if (mondoValue)
        return;

    for (editp = edits; *editp != NULL; editp++) {
        if (stricmp(attr, *editp) == 0)
            break;
    }

    if (*editp != NULL)
        EditURL(t, attrVal);

    if (stricmp(tag, "meta") == 0 && stricmp(attr, "http-equiv") == 0 &&
        stricmp(attrVal, "refresh") == 0)
        *refreshTag = currentTag;

    if (stricmp(tag, "meta") == 0 && stricmp(attr, "name") == 0 &&
        stricmp(attrVal, "ezproxyrewriting") == 0)
        *ezproxyRewritingTag = currentTag;

    if (*ezproxyRewritingTag == currentTag && stricmp(tag, "meta") == 0 &&
        stricmp(attr, "content") == 0 && t->host && (t->host)->myDb &&
        ((t->host)->myDb)->optionMetaEZproxyRewriting) {
        if (stricmp(attrVal, "enable") == 0) {
            t->rewritingDisabled = 0;
        }
        if (stricmp(attrVal, "disable") == 0) {
            t->rewritingDisabled = 1;
        }
    }

    /* I've seen the content tag show up before the http-equiv tag, so now we just rewrite if the
       content takes the form of a refresh URL, namely including some digits for time, a semi-colon,
       and url=
    */
    /*  if (*refreshTag == currentTag && stricmp(tag, "meta") == 0 && stricmp(attr, "content") == 0)
     * { */
    if (stricmp(tag, "meta") == 0 && stricmp(attr, "content") == 0) {
        refreshURL = attrVal;
        for (;; refreshURL++) {
            if (IsDigit(*refreshURL) || IsWS(*refreshURL) || *refreshURL == '\'' ||
                *refreshURL == '"')
                continue;
            if (*refreshURL == ';')
                break;
            goto NotRefresh;
        }
        refreshURL = SkipWS(refreshURL + 1);
        if (strnicmp(refreshURL, "url=", 4) != 0)
            goto NotRefresh;
        refreshURL += 4;
        /* Putting single quotes around the URL is acceptable, if not odd */
        /* The double-quote has also been seen around URLs at dc2.chass.utoronto.ca */
        if (*refreshURL == '\'' || *refreshURL == '"')
            refreshURL++;
        EditURL(t, refreshURL);
    }

/*  Log("%s process %s %s became %s", t->url, tag, attr, attrVal); */
NotRefresh:
    UUSendZ(&t->ssocket, attrVal);

    *attr = *attrVal = 0;
}

enum html_states { S_INITIAL, S_TAGO, S_TAG, S_ATTR1, S_ATTR2, S_ATTRVAL, S_JUNK };

/* These values should be at least two larger than any valid tag or attribute */
/* (one extra for null, other extra to allow HREFA to look invalid from HREF) */
#define MAXTAG 16
#define MAXATTR 16

/* Returns 0 if this is an HTML header, 1 if still could be, 2 if there is no chance */
int DetectHTML(char *s) {
    /* I think it's odd to skip comments, but scitation.aip.org had a weird case of no content-type,
    a lot of headers, then finally the opening <html> tag */
    for (;;) {
        s = SkipWS(s);
        if (strlen(s) < 5)
            return 1;
        if (strnicmp(s, "<!--", 4) != 0)
            break;
        s += 4;
        if (!IsWS(*s))
            break;
        /* Don't skip the space as it could be the very space that closes the comment e.g. <!-- -->
         * is valid, if not odd */
        for (;;) {
            s = strstr(s, "-->");
            if (s == NULL)
                return 1;
            if (IsWS(*(s - 1))) {
                s += 3;
                break;
            }
            /* That wasn't the right closing, but maybe there is another one further along */
            s += 3;
        }
    }

    /* This change is for sentinel.janes.com where a Flash file retrieves
       a series of URLs that are encoded in a text/plain file which have the
       format
       &balkans=http://sentinel.janes.com/docs/sentinel/balkans.jsp?refreshSession=true%26SORT=PostDate+desc%26ResultCount=20%26CONTENT=CURRENT%26SubscribedSelection=BALK%26
    */
    if (StrIStr(s, "=http://sentinel.janes.com/docs/sentinel/")) {
        return 0;
    }

    if (strnicmp(s, "<HTML", 5) == 0)
        return 0;

    if (strnicmp(s, "<!DOCTYPE ", 10) == 0) {
        s = SkipWS(s + 10);
        if (strnicmp(s, "HTML", 4) == 0)
            return 0;
        if (strlen(s) > 4)
            return 2;
        return 1;
    }

    if (strlen(s) > 50)
        return 2;

    return 1;
}

void ProcessHTML(struct TRANSLATE *t) {
    enum html_states state;
    char quote;
    char tag[MAXTAG];
    char attr[MAXATTR];
    int refreshTag, ezproxyRewritingTag, currentTag;
    char c;
    int breather;
    char *h = "http://";
    char *ph;
    BOOL checkJavascriptHttps;
    char *pu;
    char *p;
    char *pi;
    int len;
    enum { jsStart, jsPreamble, jsUrl, jsUrlStart } jsState;
    int remain = 0;
    BOOL javaScript;
    BOOL blocking;
    char *inbuffMin, *inbuffEnd;
    int inbuffRemain;
    /* mondoValue tracks when a parameter value in a tag is really long, in which case we rewrite
       (if needed) just after 2048 of the value has been received, instead of the whole thing, then
       we just echo everything else in the paramter
    */
    BOOL mondoValue = 0;

    /* printf("start processhtml %s %d %d %d\n", t->urlCopy, contentTypeSpecified, t->html,
     * t->javaScript); */

    javaScript = (t->host)->javaScript || t->javaScript;

    state = S_INITIAL;
    quote = 0;

    refreshTag = ezproxyRewritingTag = 0;
    currentTag = 1;

    tag[0] = attr[0] = *t->buffer = 0;

    pu = t->url;
    jsState = jsPreamble;
    ph = h;
    checkJavascriptHttps = 1;

    inbuffMin = inbuffEnd = t->inbuffer;

    blocking = 1;

    for (breather = 0;; breather++) {
        if (!UUValidSocketSend(&t->ssocket))
            break;

        if (optionTicks)
            ticksTranslate++;
        if (breather == 512) {
#ifdef WIN32
            Sleep(0); /* relinquish thread every now and again to help performance */
#endif
            breather = 0;
        }

        /* Leave room to throw in a null as well, along with any replacement text size */
        inbuffRemain = sizeof(t->inbuffer) - (inbuffEnd - t->inbuffer) - 1 - maxReplaceLen;

        len = UURecv(t->wsocketPtr, inbuffEnd, inbuffRemain, blocking ? 0 : UUSNOWAIT);
        /* Log("%s %d %d %d %" PRIuMAX " %d\n", t->urlCopy, len, blocking, socket_errno,
         * t->rsocket.recvCount, t->rsocket.recvLimit); */
        if (len < 0) {
            if (socket_errno == WSAEWOULDBLOCK) {
                UUFlush(&t->ssocket);
                if (debugLevel > 5) {
                    Log("ProcessHTML flush");
                }
                blocking = 1;
                continue;
            }
            if (debugLevel > 5) {
                Log("PHR %d", socket_errno);
            }
            /* For any other error, signal as EOF since we want to flush out whatever remains */
            len = 0;
        }

        if (len > 0) {
            *(inbuffEnd + len) = 0;
            /* printf("*%s*\n", inbuffEnd); */
        }

        blocking = 0;

        inbuffEnd += len;
        *inbuffEnd = 0;

        for (;;) {
            CheckPatterns(t, &inbuffMin, &inbuffEnd, len == 0);

            if (t->inbuffer == inbuffMin)
                break;

            for (pi = t->inbuffer; pi < inbuffMin; pi++) {
                c = *pu++ = *pi;

                if (javaScript) {
                    if (jsState == jsPreamble) {
                        /* This allows ftp:// URLs to be recognized for consideration */
                        /* It's a little odd.  Consider that ph is usually pointing at http:// */
                        /* If we get an F, it could start ftp://, so we skip the ph forward 2 */
                        /* characters, and now it points at tp://, so it will match the rest and
                         * voila! */
                        if ((c == 'f' || c == 'F') && ph == h) {
                            ph += 2;
                            continue;
                        }
                        if (c == *ph || tolower(c) == *ph) {
                            ph++;
                            if (*ph == 0) {
                                jsState = jsUrlStart;
                                remain = sizeof(t->url) - 32;
                            }
                            continue;
                        }
                        /* This extra check let's https get through as well as http */
                        if (checkJavascriptHttps && *ph == ':' && (c == 'S' || c == 's')) {
                            checkJavascriptHttps = 0;
                            continue;
                        }
                        jsState = jsStart;
                    }

                    if (jsState == jsStart) {
                        if (!IsAlpha(c)) {
                            jsState = jsPreamble;
                            ph = h;
                            checkJavascriptHttps = 1;
                        }
                    }

                    if (jsState == jsUrlStart) {
                        if (c == 1) {
                            pu--;
                            jsState = jsStart;
                        } else {
                            jsState = jsUrl;
                        }
                    }

                    if (jsState == jsUrl) {
                        /* Stop on \ as well since since some browsers seem to allow that kinda
                         * thing */
                        if (c == '/' || c == '\\' || c == '?' || c == ' ' || c == '&' ||
                            c == '\'' || c == '"' || c == '<' || c == '>') {
                            /* Blank out current char since it might be non-URL in appearance */
                            *--pu = 0;
                            EditURL(t, t->url);
                            pu = strchr(t->url, 0);
                            /* Restore current char */
                            *pu++ = c;
                            jsState = jsStart;
                        } else {
                            if (remain-- > 0)
                                continue;
                            /* Too many characters for a real URL, give up and allow contents
                               to be sent */
                        }
                    }
                }

                /* If this is a JavaScript file we're editing, don't perform any HTML processing */
                if (t->javaScript) {
                    if (t->url != pu) {
                        UUSend(&t->ssocket, t->url, pu - t->url, 0);
                        pu = t->url;
                    }
                    continue;
                }

                for (p = t->url; p < pu; p++) {
                    c = *p;
                    if (state != S_ATTRVAL)
                        UUSend(&t->ssocket, &c, 1, 0);

                    if (state == S_INITIAL) {
                        if (c == '<') {
                            tag[0] = 0;
                            state = S_TAGO;
                            currentTag++;
                        }
                        continue;
                    }

                    if (c == '>' && quote == 0) {
                        if (state == S_ATTRVAL) {
                            ProcessAttribute(t, tag, attr, t->buffer, &refreshTag,
                                             &ezproxyRewritingTag, currentTag, mondoValue);
                            UUSend(&t->ssocket, &c, 1, 0);
                        }
                        state = S_INITIAL;
                        continue;
                    }

                    if (state == S_TAGO) {
                        if (IsAlpha(c) || (tag[0] == 0 && c == '/') ||
                            (tag[0] != 0 && (IsDigit(c) || c == '.' || c == '-'))) {
                            StrCCat3(tag, c, sizeof(tag));
                        } else if (IsWS(c)) {
                            if (tag[0] == 0) {
                                state = S_INITIAL;
                                continue;
                            }
                            attr[0] = *t->buffer = 0;
                            state = S_ATTR1;
                        } else {
                            state = S_JUNK;
                        }
                        continue;
                    }

                    if (state == S_ATTR1) {
                        if (IsAlpha(c) || (attr[0] != 0 && (IsDigit(c) || c == '.' || c == '-'))) {
                            StrCCat3(attr, c, sizeof(attr));
                        } else if (IsWS(c)) {
                            if (attr[0] != 0) {
                                *t->buffer = 0;
                                state = S_ATTR2;
                            }
                        } else if (c == '=' && attr[0] != 0) {
                            *t->buffer = 0;
                            state = S_ATTRVAL;
                            mondoValue = 0;
                        }
                        continue;
                    }

                    if (state == S_ATTR2) {
                        if (c == '=') {
                            state = S_ATTRVAL;
                            mondoValue = 0;
                        } else if (IsWS(c)) {
                        } else if (IsAlpha(c)) {
                            attr[0] = c;
                            attr[1] = 0;
                            state = S_ATTR1;
                        } else {
                            state = S_JUNK;
                        }
                        continue;
                    }

                    if (state == S_ATTRVAL) {
                        if (quote != 0) {
                            if (c == quote) {
                                ProcessAttribute(t, tag, attr, t->buffer, &refreshTag,
                                                 &ezproxyRewritingTag, currentTag, mondoValue);
                                UUSend(&t->ssocket, &c, 1, 0);
                                quote = 0;
                                state = S_ATTR1;
                                continue;
                            }
                            /* duplicated below in non-quote version, too */
                            if (mondoValue)
                                UUSend(&t->ssocket, &c, 1, 0);
                            else {
                                StrCCat3(t->buffer, c, sizeof(t->buffer));
                                if (strlen(t->buffer) > 2048) {
                                    ProcessAttribute(t, tag, attr, t->buffer, &refreshTag,
                                                     &ezproxyRewritingTag, currentTag, mondoValue);
                                    mondoValue = 1;
                                }
                            }
                            continue;
                        }
                        if (*t->buffer == 0 && (c == '"' || c == '\'')) {
                            UUSend(&t->ssocket, &c, 1, 0);
                            quote = c;
                            continue;
                        }
                        if (IsWS(c)) {
                            if (*t->buffer == 0) {
                                UUSend(&t->ssocket, &c, 1, 0);
                            } else {
                                ProcessAttribute(t, tag, attr, t->buffer, &refreshTag,
                                                 &ezproxyRewritingTag, currentTag, mondoValue);
                                UUSend(&t->ssocket, &c, 1, 0);
                                state = S_ATTR1;
                            }
                        } else {
                            /* duplicated above in quote version, too */
                            if (mondoValue)
                                UUSend(&t->ssocket, &c, 1, 0);
                            else {
                                StrCCat3(t->buffer, c, sizeof(t->buffer));
                                if (strlen(t->buffer) > 2048) {
                                    ProcessAttribute(t, tag, attr, t->buffer, &refreshTag,
                                                     &ezproxyRewritingTag, currentTag, mondoValue);
                                    mondoValue = 1;
                                }
                            }
                        }
                        continue;
                    }
                }
                pu = t->url;
            }
            if (inbuffMin >= inbuffEnd) {
                inbuffMin = inbuffEnd = t->inbuffer;
            } else {
                /*      printf("Move %d ", inbuffEnd - inbuffMin); */
                memmove(t->inbuffer, inbuffMin, inbuffEnd - inbuffMin);
                inbuffEnd -= (inbuffMin - t->inbuffer);
                inbuffMin = t->inbuffer;
            }
        }

        if (len == 0) {
            break;
        }
    }
}

char *CompareJustHostNext(char *s, char *l, BOOL hyphenLikePeriod, BOOL ignoreColon) {
    s = SkipWS(s);
    if ((!ignoreColon && *s == ':') || *s == '/' || *s == '?' || *s == '\\')
        s = strchr(s, 0);
    *l = isupper(*s) ? tolower(*s) : *s;
    if (hyphenLikePeriod && *l == '-')
        *l = '.';
    return s;
}

BOOL CompareJustHost(char *n1, char *n2, BOOL hyphenLikePeriod) {
    char l1, l2;
    BOOL bracket = FALSE;

    if (n1 == NULL)
        n1 = "";
    if (n2 == NULL)
        n2 = "";
    n1 = SkipFTPHTTPslashesAt(n1, NULL, NULL);
    n2 = SkipFTPHTTPslashesAt(n2, NULL, NULL);
    for (;; n1++, n2++) {
        // if this is ipv6 bracket notation
        if (*n1 == '[')
            bracket = TRUE;
        if (*n1 == ']' && bracket)
            bracket = FALSE;

        n1 = CompareJustHostNext(n1, &l1, hyphenLikePeriod, bracket);
        n2 = CompareJustHostNext(n2, &l2, hyphenLikePeriod, bracket);
        if (l1 != l2)
            return 0;
        if (l1 == 0)
            return 1;
    }
}

BOOL FindProxyHost(struct TRANSLATE *t, BOOL *me) {
    char *hostname;
    struct HOST *h;
    int i;
    char *colon;
    size_t len;
    struct DATABASE *b;
    struct INDOMAIN *m;
    char tempName[512];
    BOOL portless = FALSE;
    BOOL exact;

    if (me)
        *me = 0;

    if (t->useSsl == USESSLMINPROXY)
        return 0;

    if (t->host != hosts)
        return 0;

    hostname = FindField("host", t->requestHeaders);
    if (hostname == NULL)
        return 0;

    LinkFields(hostname);
    hostname = SkipWS(hostname);

    ParseHost(hostname, &colon, NULL, TRUE);
    if (colon)
        len = colon - hostname;
    else
        len = strlen(hostname);

    if (me) {
        if (CompareJustHost(hostname, myName, 0) || CompareJustHost(hostname, myHttpName, 0) ||
            CompareJustHost(hostname, myHttpsName, 0) || ValidRefererHostname(hostname, len, NULL))
            *me = 1;
    }

    for (i = 0, h = hosts; i < cHosts; i++, h++) {
        if (strncmp(hostname, h->proxyHostname, len) == 0 &&
            (*(h->proxyHostname + len) == ':' || *(h->proxyHostname + len) == 0)) {
            /* When local port number is forced, this isn't really a match as
               t->hosts should already have pointed to the real host entry when we got
               here, and been rejected out at that time
            */
            if (h->forceMyPort)
                continue;
            if (optionForceSsl == 0 && t->useSsl != h->useSsl)
                continue;
            t->host = h;
            return 0;
        }
    }

    /* Now check for the possibility that a proxy by hostname was used for which no virtual
       web server has been started.  Also allows the bare-host (non 80-) form to be recognized.
    */
    for (i = 0, b = databases; i < cDatabases; i++, b++) {
        for (m = b->dbdomains; m; m = m->next) {
            if (m->hostOnly == 0 || m->proxyHostname == NULL)
                continue;
            exact = (strncmp(hostname, m->proxyHostname, len) == 0 &&
                     (*(m->proxyHostname + len) == ':' || *(m->proxyHostname + len) == 0));
            if (exact == 0) {
                /*
                portless = strncmp(m->proxyHostname, "80-", 3) == 0 && m->useSsl == 0 &&
                           strncmp(hostname, m->proxyHostname + 3, len) == 0 &&
                           (*(m->proxyHostname + len + 3) == ':' || *(m->proxyHostname + len + 3) ==
                0);
                */
                /* Match the old 80- leading version */
                portless = strncmp(hostname, "80-", 3) == 0 && m->useSsl == 0 &&
                           CompareJustHost(hostname + 3, m->proxyHostname, 0);
                /* and the old s443- leading version */
                if (portless == 0)
                    portless = strnicmp(hostname, "s443-", 5) == 0 && m->useSsl == 1 &&
                               CompareJustHost(hostname + 5, m->proxyHostname, 1) == 0;
                /* plus versions where the dots and hyphens in ssl version don't match */
                if (portless == 0)
                    portless = optionForceSsl == 0 && t->useSsl && m->useSsl &&
                               CompareJustHost(hostname, m->proxyHostname, 1);
            }
            if (exact || portless) {
                /* When local port number is forced, this isn't really a match as
                   t->hosts should already have pointed to the real host entry when we got
                   here, and been rejected out at that time
                */
                if (m->forceMyPort)
                    continue;
                if (optionForceSsl == 0 && t->useSsl != m->useSsl)
                    continue;
                sprintf(tempName, "%s:%d", m->domain, m->port);
                if (h = FindHost(tempName, m->useSsl, 0)) {
                    t->host = h;
                    return h && portless;
                }
            }
        }
    }
    return 0;
}

int ParseRequest(struct TRANSLATE *t) {
    struct UUSOCKET *s;
    int remain;
    char *bol, *eol;
    char *space1, *space2;
    int blanks;
    char holder;
    BOOL validMethod = 0;
    BOOL adminMethod = FALSE;

    /*
    char ipBuffer[16];
    */

    s = &t->ssocket;

    *t->method = *t->url = *t->version = 0;

    for (blanks = 0;; blanks++) {
        if (!ReadLine(s, t->buffer, sizeof(t->buffer))) {
            /* HTTPCode(t, 400, 1); */
            if (debugLevel > 0)
                Log("400 at ParseRequest 1");
            return 400;
        }
        UURandomForSsl(t->buffer, -1);
        /* Skip blank lines (not RFC, per se, but recommend by RFC for old buggy client */
        if (*t->buffer)
            break;
        if (blanks > 20) {
            HTTPCode(t, 400, 1);
            if (debugLevel > 0)
                Log("400 at ParseRequest 2");
            return 400;
        }
    }

    /* Restore full client timeout, which may have been reduced if we are reusing */
    (t->ssocket).timeout = clientTimeout;

    /* This is how we use to parse out the request, but things had to change as mentioned below */
    /*
    for (str = NULL, token = StrTok_R(t->buffer, WS, &str); token; token = StrTok_R(NULL, WS, &str))
    { if (*t->method == 0) StrCpy3(t->method, token, sizeof(t->method)); else if (*t->url == 0) { if
    (strlen(token) > sizeof(t->url) - 1) { HTTPCode(t, 414, 1); return 414;
            }
    */
    /* Make room to put the METHOD at the start and the HTTPVERSION at the end, with 2 spaces */
    /*
            StrCpy3(t->url, token, sizeof(t->url) - MAXMETHOD - strlen(HTTPVERSION) - 2);
        }
        else if (*t->version == 0)
            StrCpy3(t->version, token, sizeof(t->version));
    }
    */

    if (debugLevel > 19) {
        holder = t->buffer[400];
        t->buffer[400] = 0;
        Log("%d received %s", (t->ssocket).o, t->buffer);
        t->buffer[400] = holder;
    }

    if (optionFiddler) {
        if (!(t->fiddlerRequest = strdup(t->buffer)))
            PANIC;
    }

    space1 = strchr(t->buffer, ' ');

    validMethod = 0;
    if (space1) {
        struct HTTPMETHOD *hm;
        size_t len = space1 - t->buffer;

        for (hm = httpMethods; hm; hm = hm->next) {
            if (hm->len == len && strnicmp(t->buffer, hm->method, len) == 0) {
                validMethod = 1;
                adminMethod = hm->admin;
                break;
            }
        }
    }

    /* OWA was using SUBSCRIBE and SEARCH during beta */
    if (validMethod == 0 && t->host != hosts && (t->host)->gateway) {
        ProxyGateway(t);
        return 1234;
    }

    if (space1 == NULL) {
        HTTPCode(t, 400, 1);
        if (debugLevel > 0)
            Log("HTTP code 400 at ParseRequest 3");

        return 400;
    }

    /* This strategy allows the URI to contains spaces; this violates RFC 2396,
       but alas, NYU and another database needed this */

    *space1 = 0;
    StrCpy3(t->method, t->buffer, sizeof(t->method));
    space1 = SkipWS(space1 + 1);
    space2 = strrchr(space1, ' ');

    t->clientVersionX1000 = 0;
    if (space2) {
        StrCpy3(t->version, space2 + 1, sizeof(t->version));
        for (; space2 >= space1 && *space2 == ' '; space2--)
            ;
        *(space2 + 1) = 0;
        if (strnicmp(t->version, "HTTP/", 5) == 0)
            t->clientVersionX1000 = (int)(atof(t->version + 5) * 1000.0);
    }

    /* Make room to put the METHOD at the start and the HTTPVERSION at the end, with 2 spaces */
    StrCpy3(t->url, space1, sizeof(t->url) - MAXMETHOD - strlen(HTTPVERSION11) - 2);
    strcpy(t->urlCopy, t->url);

    if (*t->method == 0 || *t->url == 0) {
        HTTPCode(t, 400, 1);
        if (debugLevel > 0)
            Log("HTTP code 400 at ParseRequest 4");

        return 400;
    }

    if (validMethod == 0) {
        // When MinProxy is active, CONNECT is a valid admin method if the request arrived on the
        // MinProxy port
        adminMethod = strcmp(t->method, "CONNECT") == 0 && t->useSsl == USESSLMINPROXY;
        if (!adminMethod) {
            HTTPCode(t, 405, 1);
            return 405;
        }
    }

    *t->requestHeaders = 0;
    /* Use sizeof(t->requestHeadersBuffer) - 2 to leave room for final newline */
    remain = sizeof(t->requestHeadersBuffer) - 2;

    if (t->clientVersionX1000 >= 1100) {
        /* This is 1.1, so we assume client reuse, although it is only set to true
           if Option Reuse is also in effect
        */
        t->clientKeepAlive = optionKeepAlive;
        /* Since this is HTTP 1.1, chunking is always allowed.  This stays set
           unless we send a Content-Length header, in which case chunking would
           be a problem since it would interfere with the true content length
           by adding extra characters in the data stream
        */
        t->sendChunkingAllowed = optionAllowSendChunking;
    }

    if (*t->version) {
        for (bol = t->requestHeaders; remain > 1;) {
            // ReadLine will read from socket, since bol points to t->requestHeaders
            // requestHeaders will be populated
            if (!ReadLine(s, bol, remain)) {
                HTTPCode(t, 400, 1);
                if (debugLevel > 0)
                    Log("HTTP code 400 at ParseRequest 5");
                return 400;
            }

            UURandomForSsl(bol, -1);
            if (*bol == 0)
                break;

            eol = strchr(bol, 0);

            // lets get the host request, looking for "host:" and build the request url
            if (strnicmp(bol, "Host:", 5) == 0) {
                char *colon = strchr(bol, ':');
                colon++;
                colon = SkipWS(colon);
                (t->useSsl) ? strncpy(t->urlRawRequested, "https://\0", 9)
                            : strncpy(t->urlRawRequested, "http://\0", 8);
                strncat(t->urlRawRequested, colon, strlen(colon));
                strcat(t->urlRawRequested, t->urlCopy);
            }

            *++eol = 0;
            remain -= (eol - bol);
            bol = eol;
            /*
            if (remain <= 0) {
                if (debugLevel)
                    Log("Discard oversized request from %s", UUinet_ntoa(ipBuffer, t->sin_addr));
                HTTPCode(t, 413, 1);
                break;
            }
            */
        }
    }

    // If method is not authorized for admin requests, check if the target is the admin server, and
    // if so, deny the request. This check cannot happen during the earlier method check as the
    // target host is not known at that point.
    if (!adminMethod) {
        BOOL me;
        FindProxyHost(t, &me);
        if (me) {
            HTTPCode(t, 405, 1);
            return 405;
        }
    }

    return 0;
}

int HAPeerUp(struct TRANSLATE *t, struct HAPEER *hap, char *key) {
    struct UUSOCKET rr, *r = &rr;
    char line[256];
    int result;
    time_t now;
    BOOL any = 0;

    UUInitSocket(r, INVALID_SOCKET);

    if (hap == NULL)
        return 1;

    if (*key == 0 || strlen(key) > MAXKEY || !IsAllAlphameric(key))
        return 2;

    for (;;) {
        UUAcquireMutex(&hap->mutex);
        if (hap->failures >= 3) {
            UUtime(&now);
            if (now >= hap->lastFailure && (now - hap->lastFailure) < 30) {
                UUReleaseMutex(&hap->mutex);
                return 5;
            }
        }

        if (hap->trying > 3) {
            UUReleaseMutex(&hap->mutex);
            SubSleep(1, 0);
            continue;
        }

        hap->trying++;
        UUReleaseMutex(&hap->mutex);
        break;
    }

    if (UUConnectWithSource4(r, hap->host, hap->port, "HAPeerUp", NULL, hap->useSsl, 0, 2)) {
        UUAcquireMutex(&hap->mutex);
        hap->trying--;
        UUtime(&now);
        hap->failures++;
        UUtime(&hap->lastFailure);
        UUReleaseMutex(&hap->mutex);
        return 3;
    }

    char body[32 + MAXKEY];
    snprintf(body, sizeof(body), "session=%s", key);

    WriteLine(r,
              "POST /ha HTTP/1.1\r\n"
              "Host: %s:%d\r\n"
              "Accept: text/plain\r\n"
              "Connection: close\r\n"
              "Content-Length: %d\r\n"
              "Content-Type: application/x-www-form-urlencoded\r\n"
              "\r\n"
              "%s",
              hap->host, hap->port, strlen(body), body);

    any = 0;
    result = 4;
    while (ReadLine(r, line, sizeof(line))) {
        any = 1;
        if (strnicmp(line, "SESSION=1", 9) == 0)
            result = 0;
    }

    UUAcquireMutex(&hap->mutex);
    hap->trying--;

    if (any) {
        hap->lastFailure = 0;
        UUtime(&hap->lastSentTo);
        hap->failures = 0;
    }

    UUReleaseMutex(&hap->mutex);

    UUStopSocket(r, 0);

    return result;
}

int HAReroute(struct TRANSLATE *t, BOOL fromAdminForm, char *url, size_t urlLen) {
    char *host;
    struct HAPEER *hap = NULL;
    char *cookie;
    char *ezproxy;
    char *comma;
    char *p;
    char save;
    BOOL me;
    struct SESSION *e;
    char *cookieStarts;
    /*
    struct USERINFO ui;
    GROUPMASK gmAuto;
    int userLevel;
    int userLimit;
    */

    if (haName == NULL)
        return 0;

    if (fromAdminForm == 0 && strnicmp(url, "/form", 5) == 0)
        return 0;

    host = FindField("Host", t->requestHeaders);
    if (host == NULL)
        return 0;

    if (CompareJustHost(host, haName, 0) == 0)
        return 0;

    EncodeAngleBrackets(url, urlLen);

    for (cookie = t->requestHeaders; (cookie = FindField("cookie", cookie));) {
        LinkField(cookie);

        for (ezproxy = cookie; ezproxy && *ezproxy;) {
            while (*ezproxy == ';')
                ezproxy++;
            ezproxy = SkipWS(ezproxy);

            cookieStarts = StartsWith(ezproxy, loginCookieName);
            if (cookieStarts == NULL) {
                ezproxy = strchr(ezproxy, ';');
                continue;
            }
            if (*cookieStarts == 'n' || *cookieStarts == 'l') {
                cookieStarts++;
            }
            if (*cookieStarts == '=') {
                cookieStarts++;
            } else {
                ezproxy = strchr(ezproxy, ';');
                continue;
            }

            ezproxy = SkipWS(cookieStarts);
            comma = NULL;
            for (p = ezproxy; *p && *p != ';' && !IsWS(*p); p++) {
                if (*p == ',' && comma == NULL)
                    comma = p;
            }
            if (comma == NULL)
                continue;

            save = *comma;
            *comma = 0;
            me = stricmp(ezproxy, myUrlHttp) == 0;
            if (!me)
                hap = FindHAPeer(ezproxy, 0);
            *comma = save;
            ezproxy = comma + 1;
            if (!me) {
                if (HAPeerUp(t, hap, ezproxy))
                    continue;
                GenericRedirect(t, hap->host, hap->port, url, hap->useSsl, 0);
                return 302;
            }

            save = *p;
            *p = 0;
            e = FindSessionByKey(ezproxy);
            *p = save;
            if (e)
                goto Found;
            ezproxy = strchr(ezproxy, ';');
        }
    }

Found:
    /* The following logic tried to short-cut getting people in WAYF, CGI, etc. over
       to the right authenticator, but it short-circuited ExcludeIP processing, so
       that didn't really make sense and has been stopped to avoid conflicts
    */
    /*
    InitUserInfo(t, &ui, NULL, NULL, url, NULL);
    gmAuto = GroupMaskNot(GroupMaskNew(0));
    userLevel = FindUser(t, &ui, NULL, NULL, gmAuto, &userLimit, 0);
    GroupMaskFree(&gmAuto);
    GroupMaskFree(&ui.gm);

    if (userLevel > 0)
        return 0;
    */

    MyNameRedirect(t, url, 0);
    return 302;
}

BOOL DenyIfGoogleWebAccelerator(struct TRANSLATE *t) {
    char *header;

    for (header = t->requestHeaders; header = FindField("X-moz", header);) {
        LinkField(header);
        if (StrIStr(header, "prefetch")) {
            HTTPCode(t, 403, 0);
            HTTPContentType(t, NULL, 1);
            UUSendZ(&t->ssocket,
                    "Prefetch forbidden to prevent Google Web Accelerator conflicts\n");
            t->finalStatus = 995;
            return 1;
        }
    }

    return 0;
}

BOOL DenyIfRequestHeaders(struct TRANSLATE *t) {
    char *p;
    struct DENYIFREQUESTHEADER *dirh;

    if (denyIfRequestHeaders == NULL)
        return 0;

    for (p = t->requestHeaders; *p; p = NextField(p)) {
        for (dirh = denyIfRequestHeaders; dirh; dirh = dirh->next) {
            if (WildCompare(p, dirh->header)) {
                if (strcmp(dirh->file, "allow") == 0)
                    break;
                HTMLHeader(t, htmlHeaderNoCache);
                SendFile(t, dirh->file, 1);
                t->finalStatus = 998;
                return 1;
            }
        }
    }

    return 0;
}

static void ProcessXForwardedFor(struct TRANSLATE *t) {
    char *p;
    char *headers;
    struct XFFREMOTEIPRANGE *r;

    if (!optionAcceptXForwardedFor) {
        return;
    }

    if (xffRemoteIPRanges) {
        for (r = xffRemoteIPRanges;; r = r->next) {
            if (r == NULL) {
                return;
            }
            if (compare_ip(&t->sin_addr, &r->start) >= 0 &&
                compare_ip(&t->sin_addr, &r->stop) <= 0) {
                break;
            }
        }
    }

    // Some load balancers simply inject the X-Forwarded-For header as a new header at the bottom,
    // so process all such headers
    for (headers = t->requestHeaders; (p = FindField(xffRemoteIPHeader, headers));
         headers = NextField(p)) {
        LinkFields(p);
        char *xff;
        if (!(xff = strdup(p)))
            PANIC;
        // The X-Forwarded-For header is in the form
        //     X-Forwarded-For: client, proxy1, proxy2
        // where each subsequent proxy adds another address to the end.  As such, the
        // terminal IP address is the most meaningful.
        BOOL more;

        for (more = TRUE; more;) {
            char *comma;
            char *next;
            struct sockaddr_storage forwardedIp;
            BOOL filterProxy;

            if ((comma = strrchr(xff, ','))) {
                next = comma + 1;
                *comma = 0;
            } else {
                next = xff;
                more = FALSE;
            }

            Trim(next, TRIM_LEAD | TRIM_TRAIL);

            // If we hit something that is not a valid IP address, give up
            if (inet_pton_st2(next, &forwardedIp) == 0) {
                break;
            }

            // Filter out authorized proxies
            for (r = xffRemoteIPRanges, filterProxy = FALSE; r && !filterProxy; r = r->next) {
                if (compare_ip(&forwardedIp, &r->start) >= 0 &&
                    compare_ip(&forwardedIp, &r->stop) <= 0) {
                    if (r->trustInternal || PublicAddress(&forwardedIp)) {
                        filterProxy = TRUE;
                        break;
                    }
                }
            }

            if (!filterProxy) {
                memcpy(&t->sin_addr, &forwardedIp, sizeof forwardedIp);
                /* If a country code was located for original address, go ahead and reset it
                   based on new source IP
                */
                if (t->location.countryCode[0]) {
                    FindLocationByTranslateNoCache(t);
                }
                break;
            }
        }

        FreeThenNull(xff);
    }
    /* When supporting X-Forwarded-For, IP rejection checking is delayed
       to this point
    */
    t->reject = CheckForIPReject(t);
}

int ValidateSession(struct TRANSLATE *t) {
    char *host;
    BOOL isProxyByPort;
    BOOL me = 0;
    int result;

    if (t->useSsl == USESSLMINPROXY)
        return 0;

    ProcessXForwardedFor(t);

    {
        char *p;
        if (p = FindField("Accept-Encoding", t->requestHeaders)) {
            if (StrIStr(p, "gzip"))
                t->sendZipAllowed = optionAllowSendGzip;
        }
    }

    if (t->reject) {
        /* With a value of 2, REJECTHTM exists and should be sent */
        if (t->reject == 2) {
            HTMLHeader(t, htmlHeaderNoCache);
            SendEditedFile(t, NULL, REJECTHTM, 1, t->url, 1, NULL);
        }
        t->finalStatus = 999;
        t->reject = 0;
        return 999;
    }

    if (DenyIfRequestHeaders(t))
        return t->finalStatus;

    FindSessionByCookie(t);

    if (t->session)
        t->docsCustomDir = t->session->docsCustomDir;

    CheckRequireAuthenticate(t);
    if (t->host != hosts)
        isProxyByPort = 1;
    else {
        if (FindProxyHost(t, &me)) {
            GenericRedirect(t, (t->host)->proxyHostname, 0, t->url, (t->host)->useSsl, 0);
            return 302;
        }
        isProxyByPort = t->host == hosts;

        if (isProxyByPort && me == 0) {
            if (result = HAReroute(t, 0, t->url, sizeof(t->url)))
                return result;
            if (AdminSlipTestReport(t))
                return t->finalStatus;
            if (FileExists(BADHOSTHTM)) {
                HTMLHeader(t, htmlHeaderOmitCache);
                SendEditedFile(t, NULL, BADHOSTHTM, 1, t->url, 1, NULL);
                return 200;
            }
        }
    }

    if (t->session == NULL) {
        FindSessionByAnonymousUrl(t, NULL, NULL);
    }

    /* Logging of request used to occur here */

    /* If we didn't find a session, perhaps the user omitted the domain which prevented our domain
     */
    /* based cookie from coming back, so try a redirect to correct */
    if (t->session == NULL && isProxyByPort) {
        host = FindField("Host", t->requestHeaders);
        if (host) {
            char *colon;
            size_t hostLen;
            LinkFields(host);
            host = SkipWS(host);

            ParseHost(host, &colon, NULL, TRUE);

            if (colon)
                hostLen = colon - host;
            else
                hostLen = strlen(host);
            if (CompareJustHost(host, myName, 0) == 0 &&
                CompareJustHost(host, myHttpName, 0) == 0 &&
                CompareJustHost(host, myHttpsName, 0) == 0 &&
                ValidRefererHostname(host, hostLen, 0) == 0 &&
                (haName == NULL || CompareJustHost(host, haName, 0) == 0)) {
                /*  StripScripts(t->url); */
                EncodeAngleBrackets(t->url, sizeof(t->url));
                /* This is a 301 redirect since the URL specified really was wrong and should be
                   corrected to what we specify below
                */
                if ((t->host)->myPort) {
                    GenericRedirect(t, myName, (t->host)->myPort, t->url, (t->host)->useSsl, 301);
                } else {
                    MyNameRedirect(t, t->url, 0);
                }
                return 302;
            }
        }
    }

    CheckIsPdfUrl(t);

    return 0;
}

void SendMuseCookies(struct TRANSLATE *t, BOOL *cookiesSent) {
    struct UUSOCKET *s = &t->ssocket;
    char *p, *next;

    if (*cookiesSent)
        return;

    *cookiesSent = 1;

    for (p = t->museCookie; p && *p; p = next) {
        p = SkipWS(p);
        next = strchr(p, 0) + 1;
        if (*p) {
            if (!SetDomainCookie(t, p)) {
                UUSendZ(s, "Set-Cookie: ");
                UUSendZCRLF(s, p);
            }
        }
    }
}

/* The 1XX, 204, and 304 do not return bodies by RFC2616, so send back their code to indicate
   that no further processing is required
*/
BOOL BodylessStatus(struct TRANSLATE *t, int code) {
    return (code >= 100 && code < 200) || code == 204 || code == 304 ||
           (t != NULL && strcmp(t->method, "HEAD") == 0);
}

/**
 * Detect the mime type using the html header Content-Type value held in field.
 *
 * @param struct MIMEFILTER *mPtr The mime type filter pointer to check field against
 * @param struct TRANSLATE *t This is the current translate being evaluated.  Will
 *      set the appropriate t->javascript, t->html, etc...
 * @param char *field The html header value in Content-Type line.
 * @param int *detectContentType Some flag used by rest of code to auto detect type.
 *
 * @note This uses pcre regular expressions.
 * @see http://www.pcre.org
 */
void DetectMimeType(struct MIMEFILTER *mPtr,
                    struct TRANSLATE *t,
                    char *field,
                    int *detectContentType) {
    int ovecsize = 30;
    int ovector[ovecsize];
    int fieldLen = strlen(field);
    int urlLen;
    int rcMime, rcUri;

    if (t->isPDF) {
        *detectContentType = 0;
        return;
    }

    char url[strlen(t->host->hostUrl) + strlen(t->urlCopy) + 1];
    strcpy(url, t->host->hostUrl);
    strcat(url, t->urlCopy);
    urlLen = strlen(url);

    // find match in the Content-Type or un-proxied url
    for (; mPtr != NULL; mPtr = mPtr->next) {
        if (((rcMime = pcre_exec(mPtr->regexMime, mPtr->regexMimeExtra, field, fieldLen, 0,
                                 PCRE_ANCHORED, ovector, ovecsize)) >= 0) &&
            ((rcUri = pcre_exec(mPtr->regexUri, mPtr->regexUriExtra, url, urlLen, 0, PCRE_ANCHORED,
                                ovector, ovecsize)) >= 0)) {
            switch (mPtr->action) {
            case javascript:
                t->javaScript = 1;
                t->html = 0;
                *detectContentType = 0;
                break;
            case html:
                t->html = 1;
                t->javaScript = 0;
                *detectContentType = 0;
                break;
            case pdf:
                t->html = 0;
                t->javaScript = 0;
                *detectContentType = 0;
                FindPdfUrl(t, 1);
                break;
            case text:
                t->html = 0;
                t->javaScript = 0;
                *detectContentType = 1;
                break;
            case image:
            case none:
                t->html = 0;
                t->javaScript = 0;
                *detectContentType = 0;
                break;
            }
            break;  // exit for
        }
    }
}

static BOOL ProcessHttpHeadersExpr(struct TRANSLATE *t,
                                   struct HTTPHEADER *hh,
                                   struct UUSOCKET *w,
                                   const char *field,
                                   const char *value) {
    const char *varHeader = "hh:header";
    const char *varValue = "hh:value";
    VariablesSetValue(t->localVariables, varHeader, field);
    if (value) {
        VariablesSetValue(t->localVariables, varValue, value);
    }
    char *newValue = ExpressionValue(t, NULL, NULL, hh->expr);
    VariablesDelete(t->localVariables, varHeader);
    VariablesDelete(t->localVariables, varValue);
    if (newValue && *newValue) {
        UUSendZ(w, field);
        UUSendZ(w, ": ");
        UUSendZCRLF(w, newValue);
    }
    FreeThenNull(newValue);
}

BOOL ProcessHttpHeadersList(struct TRANSLATE *t,
                            struct HTTPHEADER *hh,
                            struct UUSOCKET *w,
                            enum HTTPHEADER_DIRECTION direction,
                            char *field,
                            BOOL *lastOK) {
    // Short-circuit handling of what will be the most common case
    if (hh == NULL) {
        return 0;
    }

    if (field == NULL) {
        for (; hh; hh = hh->next) {
            if (hh->direction == direction && hh->processing == HHINJECT) {
                ProcessHttpHeadersExpr(t, hh, w, hh->header, NULL);
            }
        }
        return 0;
    }

    BOOL result = 0;
    char *colon = strchr(field, ':');
    if (colon == NULL) {
        goto Finished;
    }
    *colon = 0;

    for (; hh; hh = hh->next) {
        if (hh->direction != direction) {
            continue;
        }
        if (hh->wild) {
            if (!WildCompare(field, hh->header)) {
                continue;
            }
        } else {
            if (stricmp(field, hh->header) != 0) {
                continue;
            }
        }
        break;
    }

    if (hh == NULL) {
        goto Finished;
    }

    if (hh->processing == HHPPROCESS) {
        if (lastOK) {
            *lastOK = 1;
        }
        goto Finished;
    }

    // All processing after this point requires that the caller
    // not do anything else with this header.

    result = 1;

    char *value = SkipWS(colon + 1);

    if (hh->processing == HHPBLOCK || hh->processing == HHINJECT) {
        goto Finished;
    }

    if (hh->processing == HHPREWRITE) {
        char *rewritten;
        if (!(rewritten = malloc(strlen(value) + 256)))
            PANIC;
        strcpy(rewritten, value);
        EditURL(t, rewritten);
        UUSendZ(w, field);
        UUSendZ(w, ": ");
        UUSendZCRLF(w, rewritten);
        FreeThenNull(rewritten);
        goto Finished;
    }

    if (hh->processing == HHPUNREWRITE) {
        char *unrewritten;
        if (!(unrewritten = malloc(strlen(value) + 256)))
            PANIC;
        strcpy(unrewritten, value);
        ReverseEditURL(t, unrewritten, NULL);
        UUSendZ(w, field);
        UUSendZ(w, ": ");
        UUSendZCRLF(w, unrewritten);
        FreeThenNull(unrewritten);
        goto Finished;
    }

    if (hh->processing == HHEDIT) {
        ProcessHttpHeadersExpr(t, hh, w, field, value);
        goto Finished;
    }

Finished:
    if (colon) {
        *colon = ':';
    }

    return result;
}

static BOOL ProcessHttpHeaders(struct TRANSLATE *t,
                               struct UUSOCKET *w,
                               enum HTTPHEADER_DIRECTION direction,
                               char *field,
                               BOOL *lastOK) {
    if (t->host && t->host->myDb) {
        BOOL result =
            ProcessHttpHeadersList(t, t->host->myDb->httpHeaders, w, direction, field, lastOK);
        if (result || t->host->myDb->ignoreGlobalHttpHeaders) {
            return result;
        }
    }

    return ProcessHttpHeadersList(t, globalHttpHeaders, w, direction, field, lastOK);
}

static void SendIdentifier(struct TRANSLATE *t) {
    char *text = NULL;
    struct SESSION *e = t->session;
    struct UUSOCKET *w = t->wsocketPtr;
    struct IDENTIFIERSCOPE *scope;
    int scopeId;
    char sScopeId[16];

    if (e == NULL || e->logUserBrief == NULL || *e->logUserBrief == 0) {
        return;
    }

    scope = IdentifierScopeByHost(t->host);
    if (!scope->enabled) {
        return;
    }

    snprintf(sScopeId, sizeof(sScopeId), "%d", scope->id);

    text = VariablesGetValue(e->identifierVariables, sScopeId);
    if (text == NULL) {
        text = IdentifierTextByScopeId(e->logUserBrief, scope->id, e->created);
        if (text) {
            VariablesSetValue(e->identifierVariables, sScopeId, text);
        }
    }

    if (text && *text) {
        WriteLine(w, "%s: %s\r\n", IDENTIFIERHEADER, text);
        if (scope->header) {
            WriteLine(w, "%s: %s\r\n", scope->header, text);
        }
    }

    FreeThenNull(text);
}

int ProcessResponse(struct TRANSLATE *t) {
    char *question;
    char *period;
    char *colon;
    char *semi;
    char *field;
    char *buffer;
    char *p;
    int status;
    int remain;
    int len;
    BOOL suppress;
    BOOL acceptRanges;
    int halfBufferSize;
    char *halfBufferPtr;
    char *refreshUrl;
    BOOL anyCookieTried, anyCookieSent;
    struct SESSION *e = t->session;
    time_t now, headerDate = 0, headerLastModified = 0;
    char httpDate[HTTPDATELENGTH];
    BOOL anyHeader = 1;
    BOOL museCookiesSent = 0;
    int tries = 0;
    BOOL chunked = 0;
    BOOL skipping100;
    BOOL detectUTF16;
    char deferredContentType[32] = {0};
    char *httpVersion;
    char *ws;
    BOOL detectContentType = 1;
    BOOL contentTypeMissing = 1;

    anyCookieTried = anyCookieSent = 0;
    acceptRanges = 0;

    halfBufferSize = MAXBUFFER / 2;
    halfBufferPtr = t->buffer + halfBufferSize;

    t->html = 0;
    t->javaScript = 0;
    t->sentPostContent = 0;
    t->zip = UURECVZIPNONE;
    detectUTF16 = t->host && t->host->myDb && t->host->myDb->optionUTF16;

    for (tries = 0, skipping100 = 0;; tries++) {
        if (tries > 10 && skipping100 == 0)
            return 1;
        if (ReadLine(t->wsocketPtr, t->buffer, sizeof(t->buffer))) {
            if (t->buffer[0]) {
                if (StartsWith(t->buffer, "HTTP/") && atoi(SkipWS(SkipNonWS(t->buffer))) == 100) {
                    if (debugLevel > 3)
                        Log("Skipping 100 continue");
                    skipping100 = 1;
                }
                if (skipping100) {
                    if (t->expect100Continue)
                        UUSendZCRLF(&t->ssocket, t->buffer);
                    continue;
                }
                break;
            } else {
                if (skipping100) {
                    if (t->expect100Continue)
                        UUSendCRLF(&t->ssocket);
                    skipping100 = 0;
                    tries = 0;
                }
            }
        }
    }

    t->finalStatus = 200;

    strcat(t->buffer, "\r\n");

    // if the buffer does not start with HTTP/ then this is invalid syntax and we will assume no
    // header was sent
    if ((httpVersion = StartsWith(t->buffer, "HTTP/")) == NULL) {
        t->sendChunkingAllowed = 0;
        t->sendZipAllowed = 0;

        // real simple detection for HTML or html content
        if (StrIStr(t->buffer, "<HTML>") != NULL || StrIStr(t->buffer, "<HEAD>") != NULL)
            t->html = 1;
        anyHeader = 0;
        UUSendZ(&t->ssocket, t->buffer);

        goto SkipHeader;
    }

    t->serverVersionX1000 = (int)(atof(httpVersion) * 1000.0);
    /* In HTTP 1.1, keep-alive is assumed */
    if (t->serverVersionX1000 >= 1100)
        t->serverKeepAlive = optionKeepAlive;

    /* we now respond with HTTP/1.1, so chunking is allowed regardless of server version
    if (t->serverVersionX1000 < 1100)
        t->sendChunkingAllowed = 0;
    */

    /* If there is no actual space, ws will end up point at the CRLF we added above, so
       this still works correctly
    */
    ws = SkipNonWS(httpVersion);
    UUSend2(&t->ssocket, HTTPVERSION11, strlen(HTTPVERSION11), ws, strlen(ws), 0);

    if (status = atoi(SkipWS(ws)))
        t->finalStatus = status;

    *(t->buffer) = 0;
    buffer = halfBufferPtr;
    *(buffer) = 0;
    remain = halfBufferSize - 1;
    suppress = 0;
    for (;;) {
        /* If we get an EOF early, pretend it was the blank line that ends the header */
        /* This was necessary to fix a problem with a UCLA redirect script */
        if (!ReadLine(t->wsocketPtr, halfBufferPtr, remain))
            *halfBufferPtr = 0;

        /*  printf("Received %s\n", halfBufferPtr); */
        /*      if (buffer == t->buffer || IsWS(*buffer)) { */
        if (*(t->buffer) == 0 || IsWS(*halfBufferPtr)) {
            len = strlen(halfBufferPtr);
            if (len == 0)
                break;
            strcat(t->buffer, halfBufferPtr);
            remain -= len;
            continue;
        }

        /* This was to handle Canadian Labour Law Library, which used a content-type of
           application/octet-stream but indicated HTML by including the DOCTYPE within the header
           space?
        */
        if (DetectHTML(t->buffer) == 0) {
            t->html = 1;
        }

        if (!ProcessHttpHeaders(t, &t->ssocket, HHDRESPONSE, t->buffer, NULL)) {
            if ((colon = strchr(t->buffer, ':')) != NULL) {
                *colon = 0;
                field = SkipWS(colon + 1);

                if (stricmp(t->buffer, "Date") == 0) {
                    UUtime(&now);
                    /* Check to see if someone has been messing with clock since EZproxy started,
                       and if so, adjust for it as best possible
                    */
                    if (now < lastModifiedMinimum)
                        lastModifiedMinimum = now;
                    headerDate = ParseHttpDate(field, now);
                    WriteLine(&t->ssocket, "Date: %s\r\n",
                              FormatHttpDate(httpDate, sizeof(httpDate), headerDate));
                    suppress = 1;
                }

                /* If last modified date is before EZproxy was last started, alter it to be the
                   date/time EZproxy was started so if people are making config file changes, they
                   are more likely to be picked up by testers and remote users alike.
                */

                if (stricmp(t->buffer, "Last-Modified") == 0) {
                    headerLastModified = ParseHttpDate(field, lastModifiedMinimum);
                    if (headerLastModified <= lastModifiedMinimum) {
                        headerLastModified = lastModifiedMinimum;
                        /* The last modified date is not supposed to be beyond the date header, per
                         * RFC */
                        if (headerDate > 0 && headerLastModified > headerDate)
                            headerLastModified = headerDate;
                        WriteLine(&t->ssocket, "Last-Modified: %s\r\n",
                                  FormatHttpDate(httpDate, sizeof(httpDate), lastModifiedMinimum));
                        suppress = 1;
                    }
                }

                if (stricmp(t->buffer, "Content-Encoding") == 0) {
                    if (stricmp(field, "gzip") == 0 || stricmp(field, "x-gzip") == 0) {
                        t->zip = UURECVZIPGZIP;
                    }
                    if (stricmp(field, "deflate") == 0 || stricmp(field, "x-deflate") == 0) {
                        t->zip = UURECVZIPDEFLATE;
                    }
                    suppress = 1;
                }

                if (stricmp(t->buffer, "Transfer-Encoding") == 0) {
                    chunked = StrIStr(field, "chunked") != NULL;
                    suppress = 1;
                }

                if (stricmp(t->buffer, "Content-Type") == 0) {
                    contentTypeMissing = 0;
                    detectContentType = 0;
                    if ((semi = strchr(field, ';')))
                        *semi = 0;

                    if (*field) {
                        StrCpy3(t->contentType, field, sizeof(t->contentType));
                    }

                    // detect the default mime types
                    DetectMimeType(t->host->myDb->mimeFiltersDefault, t, field, &detectContentType);

                    // detect and overwrite the default mime types potentially
                    DetectMimeType(t->host->myDb->mimeFiltersUser, t, field, &detectContentType);

                    if (semi)
                        *semi = ';';
                    /* if (detectUTF16 && contentTypeSpecified && (t->html || t->javaScript) && semi
                     * == NULL) { */
                    if (detectUTF16 && detectContentType == 0 && (t->html || t->javaScript) &&
                        semi == NULL) {
                        StrCpy3(deferredContentType, field, sizeof(deferredContentType));
                        suppress = 1;
                    }
                }

                if (stricmp(t->buffer, "Content-Length") == 0) {
                    int cl = atoi(field);
                    if (cl > 0 || (cl == 0 && *field == '0')) {
                        t->contentLength = cl;
                    }
                    suppress = 1;
                }

                /* The Vary: Accept-Encoding from pubmed was causing the POST form results to expire
                for a hit list, since the client was willing to accept gzip format but wasn't
                getting that back. Suppressing Accept-Encoding resolved the issue.  If EZproxy sends
                in gzip some day, this can likely be removed
                */
                if (stricmp(t->buffer, "Vary") == 0) {
                    if (StrIStr(field, "accept-encoding")) {
                        char *str, *token, *oldField;

                        if (!(oldField = strdup(field)))
                            PANIC;
                        *field = 0;
                        for (str = NULL, token = StrTok_R(oldField, WS ",", &str); token;
                             token = StrTok_R(NULL, WS ",", &str)) {
                            if (*token == 0 || stricmp(token, "accept-encoding") == 0)
                                continue;
                            if (*field)
                                strcat(field, ",");
                            strcat(field, token);
                        }
                        if (*field == 0)
                            suppress = 1;
                        FreeThenNull(oldField);
                    }
                }

                if (stricmp(t->buffer, "Connection") == 0) {
                    /* If this was an HTTP/1.1 connection, we already marked it for keep-alive
                       If this was an HTTP/1.0 connection, we didn't, and we don't want to since
                       the dynamics weren't as well defined
                     */
                    if (StrIStr(field, "close"))
                        t->serverKeepAlive = 0;
                    suppress = 1;
                }

                if (stricmp(t->buffer, "Keep-Alive") == 0) {
                    /* Suppress the old HTTP/1.1 Keep-Alive header */
                    suppress = 1;
                }

                if (stricmp(t->buffer, "WWW-Authenticate") == 0) {
                    suppress = strnicmp(field, "Basic", 5) != 0;
                }

                if (stricmp(t->buffer, "Accept-Ranges") == 0) {
                    /* note that this header was used, but defer sending response until
                       we know for certain whether or not this is PDF content type
                    */
                    suppress = 1;
                    acceptRanges = 1;
                }

                /* Suppress ETag since changes in databases definitions may invalidate content, but
                   the ETag could cause copies to be cached.  There are also various known issues
                   with ETag, see _High Performance Web Sites_ by Steve Souders
                */
                if (stricmp(t->buffer, "ETag") == 0) {
                    suppress = 1;
                }

                if (stricmp(t->buffer, "Access-Control-Allow-Origin") == 0) {
                    EditURL(t, field);
                }

                if (stricmp(t->buffer, "Location") == 0 ||
                    stricmp(t->buffer, "Content-Location") == 0) {
                    /*              printf("requested %s\n", t->url); */
                    /*              printf("Location is %s\n", field); */
                    /* Change relative URLs to absolute */
                    if (strnicmp(field, "//", 2) != 0 && strstr(field, "://") == NULL &&
                        t->urlCopy && (*(t->urlCopy) == '/')) {
                        char *slash;
                        struct HOST *h;
                        char *sfield;
                        size_t len;

                        sfield = field;
                        /* printf("location: %s", field); */
                        h = t->host;
                        if (h) {
                            WriteLine(&t->ssocket, "%s: http%s://", t->buffer,
                                      (h->useSsl || optionForceSsl ? "s" : ""));
                            if (t->proxyType == PTHOST)
                                UUSendZ(&t->ssocket, h->proxyHostname);
                            else
                                WriteLine(&t->ssocket, "%s:%d", myName, h->myPort);
                            if (*sfield != '/') {
                                /* slash must get a value since t->urlCopy == '/' from above */
                                for (p = t->urlCopy, slash = NULL; *p && *p != '?' && *p != '#';
                                     p++) {
                                    if (*p == '/')
                                        slash = p;
                                }
                                /* If the new URL starts with ?, then the browser behavior is to
                                   replace an existing query string or append if there is no query
                                   string.  At this point, p points at ?, the null, or the #, so
                                   regardless, it is in the right place to handle this scenario.  If
                                   we don't have ?, then we move back to the last observed slash
                                */
                                if (*field == '?')
                                    len = p - t->urlCopy;
                                else
                                    len = slash - t->urlCopy + 1;
                                UUSend(&t->ssocket, t->urlCopy, len, 0);
                                /* If the relative url starts ../ or ./ and our absolute URL is at
                                /, then strip this away to avoid browser issues as seen by
                                http://www.libweb.co.uk/ */
                                if (len == 1) {
                                    for (;;) {
                                        if (strncmp(sfield, "../", 3) == 0)
                                            sfield += 3;
                                        else if (strncmp(sfield, "./", 2) == 0)
                                            sfield += 2;
                                        else
                                            break;
                                    }
                                }
                            }
                            if (*sfield)
                                UUSendZ(&t->ssocket, sfield);
                            UUSendCRLF(&t->ssocket);
                            suppress = 1;
                        }
                    }
                    if (!suppress)
                        EditURL(t, field);
                    /*              printf("Changed to %s\n", field); */
                }

                if (stricmp(t->buffer, "Set-Cookie") == 0) {
                    SendMuseCookies(t, &museCookiesSent);
                    /* WARNING: SetDomainCookie will destroy this buffer if this is a domain cookie
                     */
                    /* Since suppress will be true, that's OK, but if there is anything else added
                     */
                    /* between here and sending the data could be a problem. */
                    anyCookieTried = 1;
                    suppress = SetDomainCookie(t, field);
                    if (!suppress)
                        anyCookieSent = 1;
                }

                if (stricmp(t->buffer, "Refresh") == 0) {
                    if (refreshUrl = StrIStr(field, "url=")) {
                        refreshUrl += 4;
                        /* Putting single quotes around the URL is acceptable, if not odd */
                        if (*refreshUrl == '\'')
                            refreshUrl++;
                        EditURL(t, refreshUrl);
                    }
                }

                if (stricmp(t->buffer, "Strict-Transport-Security") == 0) {
                    if (optionProxyType == PTPORT) {
                        suppress = 1;
                    } else if (StrIStr(field, "includeSubdomains") != NULL) {
                        // If includeSubdomains is present, snip it out
                        char *sts = NULL;
                        char *str;
                        char *token;
                        if (!(sts = strdup(field)))
                            PANIC;
                        *field = 0;
                        for (token = StrTok_R(sts, ";", &str); token;
                             token = StrTok_R(NULL, ";", &str)) {
                            if (StrIStr(token, "includeSubdomains") == NULL) {
                                if (*field) {
                                    strcat(field, ";");
                                }
                                strcat(field, token);
                            }
                        }
                        Trim(field, TRIM_LEAD | TRIM_TRAIL);
                        if (*field == 0) {
                            suppress = 1;
                        }
                        FreeThenNull(sts);
                    }
                }

                *colon = ':';
            }

            /*      printf("%s\n", t->buffer); */
            if (suppress)
                suppress = 0;
            else {
                UUSendZCRLF(&t->ssocket, t->buffer);
                /*          printf("Sent %s\n", t->buffer); */
            }
        }
        if (*halfBufferPtr == 0)
            break;
        StrCpyOverlap(t->buffer, halfBufferPtr);
        len = strlen(t->buffer);
        remain = halfBufferSize - len - 1;
    }

    if (acceptRanges)
        WriteLine(&t->ssocket, "Accept-Ranges: %s\r\n", (t->isPDF ? "bytes" : "none"));
    ProcessHttpHeaders(t, &t->ssocket, HHDRESPONSE, NULL, NULL);
    SendMuseCookies(t, &museCookiesSent);
    /* By the least-recently-used policy for cookies in a domain, the EZproxy cookie was getting
       expired off by Grolier.  So, we reset every time. The user's browser doesn't bug him/her,
       so it seems painless otherwise
    */
    if (optionPassiveCookie == 0)
        SendEZproxyCookie(t, e);

    if (anyCookieTried && anyCookieSent == 0 && e != NULL) {
        e->domainCookieSuffix = (e->domainCookieSuffix & 15) + '1';
        if (e->domainCookieSuffix > '9')
            e->domainCookieSuffix = '0';
        /* It was hoped that this would fix a situation where a web page was considered unchanged
           when a domain cookie was set as part of a redirect, but it didn't matter; the code
           remains in case it is ever useful, but is disabled for now
        */
        /*
        UUSendZ(&t->ssocket, "Set-Cookie: %s=%s%c; Path=/; Domain=%s\r\n", loginCookieName, e->key,
        e->domainCookieSuffix, myDomain);
        */
    }

SkipHeader:

    if (contentTypeMissing) {
        // detect the default mime types
        DetectMimeType(t->host->myDb->mimeFiltersDefault, t, "", &detectContentType);

        // detect and overwrite the default mime types potentially
        DetectMimeType(t->host->myDb->mimeFiltersUser, t, "", &detectContentType);
    }

    /* If it's a JavaScript file but we don't do JavaScript translation on this host,
       pretend like it isn't JavaScript */
    if ((t->host)->javaScript == 0)
        t->javaScript = 0;

    if (t->contentLength == 0) {
        UUSendZ(&t->ssocket, "Content-Length: 0\r\n");
        /* If there is no body, there is no content type to detect in it */
        detectContentType = 0;
        t->sendChunkingAllowed = 0;
        t->sendContentLengthSent = 1;
    }

    if (BodylessStatus(t, t->finalStatus)) {
        t->bodylessStatus = 1;
        /* If they had a content length, it is to be ignored, so set the remote socket accordingly
         */
        if (deferredContentType[0])
            WriteLine(&t->ssocket, "Content-Type: %s\r\n", deferredContentType);
        UUInitSocketRecvChunking(t->wsocketPtr, 0);
        detectContentType = 0;
        t->zip = UURECVZIPNONE;
        HeaderToBody(t);
        return t->finalStatus;
    }

    if (chunked)
        UUInitSocketRecvChunking(t->wsocketPtr, -1);
    else if (t->contentLength >= 0)
        UUInitSocketRecvChunking(t->wsocketPtr, t->contentLength);

    if (t->zip) {
        t->wsocketPtr->recvZip = t->zip;
    }

    if (detectUTF16)
        t->wsocketPtr->utf16 = 1;

    if (t->html || t->javaScript)
        detectContentType = 0;

    if (detectContentType) {
        int result;
        size_t len = 0;
        char *p;

        UURecvMarkSet(t->wsocketPtr);
        if (t->zip) {
            t->wsocketPtr->recvZip = t->zip;
        }
        p = t->buffer;
        for (;;) {
            result = UURecv(t->wsocketPtr, p, sizeof(t->buffer) - len - 1, 0);
            if (result <= 0) {
                break;
            }
            p += result;
            *p = 0;
            result = DetectHTML(t->buffer);
            if (result == 1)
                continue;
            if (result == 0)
                t->html = 1;
            /* Otherwise, result is 2 and we don't have HTML and we still need to stop */
            break;
        }
        if (t->html && t->wsocketPtr->utf16 >= 10)
            WriteLine(&t->ssocket, "Content-Type: text/html; charset=UTF-8\r\n");
        UURecvMarkRewind(t->wsocketPtr);
        detectContentType = 0;
    }

    if (deferredContentType[0]) {
        /* Detect if content is text/html and optionUTF16 */
        while ((!UUSocketEof(t->wsocketPtr)) && t->wsocketPtr->utf16 >= 1 &&
               t->wsocketPtr->utf16 <= 2) {
            UURecv(t->wsocketPtr, 0, 0, 0);
        }
        WriteLine(&t->ssocket, "Content-Type: %s%s\r\n", deferredContentType,
                  (t->wsocketPtr->utf16 >= 10 ? "; charset=UTF-8" : ""));
    }

    if (t->html || t->javaScript) {
        if (t->zip) {
            /* Removing the Content-Encoding: gzip header wasn't enough for
               EBSCO Visual Search to accept the non-gzip response.  It
               actually required the Content-Encoding: none header, so
               that's what we give it.
               Of course, this only matter if we aren't going to be zipping up
               the response
            */
            if (t->sendChunkingAllowed == 0 || t->sendZipAllowed == 0 ||
                optionAllowSendChunking == 0) {
                WriteLine(&t->ssocket, "Content-Encoding: none\r\n");
                t->sendZipAllowed = 0;
            }
        }
    } else {
        /* For non-processed content, pass through the content-encoding header if one was provided,
           but then erase the flag so EZproxy doesn't try to act on it
        */
        if (t->zip) {
            if (t->zip == UURECVZIPGZIP) {
                WriteLine(&t->ssocket, "Content-Encoding: gzip\r\n");
            } else {
                WriteLine(&t->ssocket, "Content-Encoding: deflate\r\n");
            }
            t->zip = UURECVZIPNONE;
        }
        t->wsocketPtr->recvZip = UURECVZIPNONE;
        if (t->contentLength > 0) {
            WriteLine(&t->ssocket, "Content-Length: %" PRIdMAX "\r\n", t->contentLength);
            t->sendChunkingAllowed = 0;
            t->sendContentLengthSent = 1;
            t->sendZipAllowed = 0;
        }
    }

    // close out the header
    HeaderToBody(t);

    // still no body written to socket yet

    return 0;
}

BOOL ReloginRequired(struct SESSION *e) {
    time_t now;

    if (e == NULL || e->relogin == 0)
        return 0;

    if (e->reloginRequired == 0 && e->relogin < UUtime(&now))
        e->reloginRequired = 1;

    return e->reloginRequired;
}

int ValidateRequest(struct TRANSLATE *t) {
    struct UUSOCKET *s;
    struct UUSOCKET *w;
    GROUPMASK dbGm;
    char *up;
    struct SESSION *e;
    struct DATABASE *b;

    s = &t->ssocket;
    w = t->wsocketPtr;

    /* If user lacks a valid cookie, try to reroute them through login since they may be using */
    /* a bookmark or may have been idle beyond cookie expiration */
    /* Also have to check to see if user is trying for something outside his/her group mask */

    b = (t->host ? (t->host)->myDb : NULL);
    dbGm = (b ? b->gm : NULL);
    e = t->session;
    up = "";
    if (e == NULL || ReloginRequired(e) || e->accountChangePass || (b && b->referer))
        goto MustLogin;

    up = "/up";

    if (GroupMaskOverlap((t->session)->gm, dbGm) == 0)
        goto MustLogin;

    return 0;

MustLogin:
    AdminLogup2(t, up);

    return 1;
}

BOOL ConnectThroughProxy(struct UUSOCKET *r,
                         char *hostname,
                         PORT port,
                         char *facility,
                         struct sockaddr_storage *pInterface,
                         char useSsl,
                         char *proxyHost,
                         PORT proxyPort,
                         char *proxySslAuth) {
    int rc;
    char line[256];
    int result;
    char *arg;

    if (proxyPort != 0) {
        rc = UUConnectWithSource2(r, proxyHost, proxyPort, facility, pInterface, 0);
    } else {
        rc = UUConnectWithSource2(r, hostname, port, facility, pInterface, useSsl);
    }

    if (rc != 0)
        return rc;

    if (useSsl) {
        UUSendZ(r, "CONNECT ");
        UUSendZ(r, hostname);
        WriteLine(r, ":%d HTTP/1.0\r\nHost: ", port);
        UUSendZ(r, hostname);
        WriteLine(r, ":%d\r\n", port);
        if (proxySslAuth) {
            UUSendZ(r, "Proxy-Authorization: basic ");
            UUSendZCRLF(r, proxySslAuth);
        }
        UUSendCRLF(r);
        result = 0;
        while (ReadLine(r, line, sizeof(line))) {
            if (line[0] == 0)
                break;
            if (strnicmp(line, "HTTP/", 5) == 0) {
                arg = SkipNonWS(SkipWS(line));
                result = atoi(arg);
                if (result < 200 || result > 299) {
                    Log("ProxySSL connection refused: %s", line);
                }
            }
        }
        UUInitSocketSsl(r, r->o, UUSSLCONNECT, 0, hostname);
        if (r->ssl == NULL) {
        }
        /* Actually, since this form of proxy connection is transparent, we lie back and
           say we're not going through a proxy as that is what it looks like */
        /* t->connectedByProxy = 1; */
    }

    return 0;
}

BOOL ConnectToServer(struct TRANSLATE *t,
                     BOOL skipProxy,
                     char *hostname,
                     PORT port,
                     struct sockaddr_storage *pInterface,
                     char useSsl,
                     struct DATABASE *b) {
    int rc;
    struct UUSOCKET *s;
    struct UUSOCKET *w;
    BOOL needConnect = 0;
    char line[256];
    int result;
    char *arg;
    char *psa;
    int sslIdx = b ? b->sslIdx : 0;

#ifdef WIN32
    DWORD dwVersion;
#endif

    s = &t->ssocket;
    w = t->wsocketPtr;

    if (t->wsocketPtr->o != INVALID_SOCKET) {
        if (debugLevel > 5) {
            Log("Reusing remote %d", t->wsocketPtr->o);
        }
        return 0;
    }

    t->connectedByProxy = 0;

    if (b && b->proxyPort != 0 && skipProxy == 0 && useSsl == 0) {
        rc = UUConnectWithSource2(t->wsocketPtr, b->proxyHost, b->proxyPort, "DoTranslate-p",
                                  pInterface, 0);
        if (rc == UUSEHOSTNOTFOUND) {
            HTTPCode(t, 503, 0);
            if (*t->version) {
                UUSendZ(s, "Content-type: text/html\r\n\r\n");
            }
            UUSendZ(s, "Unable to locate address for ");
            UUSendZ(s, b->proxyHost);
            UUSendZ(s, ", please try again later.\n");
            return 1;
        }
        t->connectedByProxy = 1;
    } else if (b && b->proxySslPort != 0 && skipProxy == 0 && useSsl) {
        /* Connecting through a proxy for https still starts with normal socket */
        rc = UUConnectWithSource2(t->wsocketPtr, b->proxySslHost, b->proxySslPort, "DoTranslate-ps",
                                  pInterface, 0);
        if (rc == UUSEHOSTNOTFOUND) {
            HTTPCode(t, 503, 0);
            if (*t->version) {
                UUSendZ(s, "Content-type: text/html\r\n\r\n");
            }
            UUSendZ(s, "Unable to locate address for ");
            UUSend(s, b->proxySslHost, strlen(b->proxySslHost), 0);
            UUSendZ(s, ", please try again later.\n");
            return 1;
        }
        needConnect = 1;
    } else {
        rc = UUConnectWithSource3(t->wsocketPtr, hostname, port, "DoTranslate", pInterface, useSsl,
                                  sslIdx);
    }

    if (rc != 0) {
        if (rc == UUSEHOSTNOTFOUND) {
            Log("Host not found: %s", hostname);
            HTTPCode(t, 503, 0);
            if (*t->version) {
                UUSendZ(s, "Content-type: text/html\r\n\r\n");
            }
            UUSendZ(s, "Unable to locate address for ");
            UUSendZ(s, hostname);
            UUSendZ(s, ", please try again later.\n");
            return 2;
        }
        if (rc == UUSESOCKETFAIL) {
            HTTPCode(t, 503, 0);
            if (*t->version) {
                UUSendZ(s, "Content-type: text/html\r\n\r\n");
            }
#ifdef WIN32
            dwVersion = GetVersion();
            if (dwVersion >= 0x80000000) { /* not Windows NT */
                UUSendZ(s, "Unable to create new socket.<p>\n");
                UUSendZ(s,
                        "This is a known restriction within Windows 95/98.  If you are testing "
                        "under one of these ");
                UUSendZ(s,
                        "operating systems, please stop EZproxy, delete the ezproxy.hst file, and "
                        "restart.  Due to ");
                UUSendZ(s, "these restrictions, though, you cannot fully deploy " EZPROXYNAMEPROPER
                           " under these operating systems.\n");
                return 3;
            }
#endif
            UUSendZ(s, "Unable to create new socket, please try again later.\n");
            return 4;
        }
        HTTPCode(t, 503, 0);
        if (*t->version) {
            UUSendZ(s, "Content-type: text/html\r\n\r\n");
        }
        UUSendZ(s, "Connection refused by ");
        UUSendZ(s, hostname);
        UUSendZ(s, ", please try again later.\n");
        return 5;
    }

    w->timeout = remoteTimeout;

    if (needConnect) {
        UUSendZ(w, "CONNECT ");
        UUSendZ(w, hostname);
        WriteLine(w, ":%d HTTP/1.0\r\nHost: ", port);
        UUSendZ(w, hostname);
        WriteLine(w, ":%d\r\n", port);
        psa = (b ? b->proxySslAuth : NULL);
        if (t->session && (t->session)->sessionProxyAuth)
            psa = (t->session)->sessionProxyAuth;
        if (psa) {
            UUSendZ(w, "Proxy-Authorization: basic ");
            UUSendZCRLF(w, psa);
        }
        UUSendCRLF(w);
        result = 0;
        while (ReadLine(w, line, sizeof(line))) {
            if (line[0] == 0)
                break;
            if (strnicmp(line, "HTTP/", 5) == 0) {
                arg = SkipNonWS(SkipWS(line));
                result = atoi(arg);
                if (result < 200 || result > 299) {
                    Log("ProxySSL connection refused: %s", line);
                    HTTPCode(t, 503, 0);
                    if (*t->version) {
                        HTTPContentType(t, NULL, 1);
                    }
                    UUSendZ(s, "Unable to start secure connection through proxy to ");
                    UUSendZ(s, hostname);
                    UUSendZ(s, ", please try again later.\n");
                    UUStopSocket(w, 0);
                    return 6;
                }
            }
        }
        UUInitSocketSsl(w, w->o, UUSSLCONNECT, sslIdx, hostname);
        if (w->ssl == NULL) {
            HTTPCode(t, 503, 0);
            if (*t->version) {
                UUSendZ(s, "Content-type: text/html\r\n\r\n");
            }
            UUSendZ(s, "Unable to negotiate secure connection through proxy to ");
            UUSendZ(s, hostname);
            UUSendZ(s, ", please try again later.\n");
            UUStopSocket(w, 0);
            return 7;
        }
        /* Actually, since this form of proxy connection is transparent, we lie back and
           say we're not going through a proxy as that is what it looks like */
        /* t->connectedByProxy = 1; */
    }

    return 0;
}

int ForwardRequestFTPPasv(struct TRANSLATE *t, char *line, char *ipName, PORT *ipPort) {
    char *str, *token, *firstDigit, *lastDigit;
    int idx;
    unsigned int val;
    int funcStatus;

    *ipName = 0;
    *ipPort = 0;

    for (idx = 0, str = NULL, token = StrTok_R(line, ",", &str); idx < 6 && token;
         token = StrTok_R(NULL, ",", &str), idx++) {
        if (idx == 0) {
            for (firstDigit = strchr(token, 0) - 1;; firstDigit--) {
                if (firstDigit < token || !IsDigit(*firstDigit)) {
                    firstDigit++;
                    break;
                }
            }
        } else {
            if (!IsDigit(*token))
                return 1;

            firstDigit = token;
            for (lastDigit = token; *lastDigit && IsDigit(*lastDigit); lastDigit++) {
            }
            *lastDigit = 0;
        }

        if (*firstDigit == 0)
            return 2;

        if (strlen(firstDigit) > 3)
            return 3;

        val = atoi(firstDigit);
        if (val > 255)
            return 4;

        if (idx < 4) {
            if (*ipName)
                strcat(ipName, ".");
            strcat(ipName, firstDigit);
        }
        if (idx == 4)
            *ipPort = val << 8;
        if (idx == 5)
            *ipPort |= val;
    }

    if (idx != 6)
        return 4;

    if (funcStatus = UUConnectWithSource(t->wsocketPtr, ipName, *ipPort, "ForwardRequestFTPPasv",
                                         DatabaseOrSessionIpInterface(t, NULL)))
        return funcStatus;

    return 0;
}

BOOL ForwardRequestFTP(struct TRANSLATE *t) {
    struct UUSOCKET *f;
    int funcStatus = 0;
    char *user = "anonymous";
    char *pass = "ezftp@ezproxy.com";
    int result;
    int state;
    char line[256];
    char multi[4];
    char ipName[16];
    PORT ipPort;
    char mimeType[128];
    int error = 503;

    /* Assumes UUInitSocket was called on both of these already */
    f = &t->ftpsocket;

    result = 3;

    multi[0] = 0;

    if (funcStatus =
            UUConnectWithSource(&t->ftpsocket, (t->host)->hostname, (t->host)->remport,
                                "ForwardRequestFTP", DatabaseOrSessionIpInterface(t, NULL)))
        return funcStatus;

    result = 1;
    for (state = 0;;) {
        if (ReadLine(f, line, sizeof(line)) == 0) {
            result = 3;
            break;
        }
        if (debugLevel > 5) {
            Log("FTP proxy: %s", line);
        }
        /* If we are receiving a multi-line response, skip all but final line */
        if (strlen(line) >= 4) {
            /* Multi-line responses end when the original code is received, followed by a space */
            if (multi[0]) {
                if (strncmp(line, multi, 3) == 0 && line[3] == ' ')
                    multi[0] = 0;
                /* Multi-line responses start when there is a - after the 3 digit code */
            } else if (line[3] == '-') {
                StrCpy3(multi, line, 4);
            }
        }
        if (multi[0])
            continue;

        if (state == 0) {
            if (strncmp(line, "120", 3) == 0)
                continue;
            if (strncmp(line, "220", 3) != 0)
                break;
            UUSendZ(f, "USER ");
            UUSendZCRLF(f, user);
            if (debugLevel > 10)
                Log("Sent FTP USER %s", user);
        } else if (state == 1) {
            if (strncmp(line, "331", 3) != 0)
                break;
            UUSendZ(f, "PASS ");
            UUSendZCRLF(f, pass);
        } else if (state == 2) {
            if (strncmp(line, "230", 3) != 0)
                break;
            UUSendZ(f, "TYPE I\r\n");
        } else if (state == 3) {
            if (strncmp(line, "200", 3) != 0)
                break;
            UUSendZ(f, "PASV\r\n");
        } else if (state == 4) {
            if (strncmp(line, "227", 3) != 0)
                break;
            if (ForwardRequestFTPPasv(t, line, ipName, &ipPort))
                break;
            UUSendZ(f, "RETR ");
            UUSendZCRLF(f, t->urlCopy);
        } else if (state == 5) {
            if (strncmp(line, "150", 3) != 0 && strncmp(line, "125", 3) != 0) {
                error = 404;
                break;
            }
            FindMIMEType(t->url, mimeType, sizeof(mimeType), "application/octet-stream");
            if (stricmp(mimeType, "text/html") == 0) {
                t->html = 1;
            }
            HTTPCode(t, 200, 0);
            HTTPContentType(t, mimeType, 1);
            return 0;
        }
        state++;
    }

    HTTPCode(t, error, 1);
    /* If socket not started by us, no harm will come of this */
    UUStopSocket(t->wsocketPtr, 0);
    UUStopSocket(&t->ftpsocket, 0);
    return 1;
}

void ForwardRequestSendHost(struct TRANSLATE *t, struct UUSOCKET *r) {
    UUSendZ(r, "Host: ");
    UUSendZ(r, (t->host)->hostname);
    SendIfNonDefaultPort(r, (t->host)->remport, (t->host)->useSsl);
    UUSendCRLF(r);
}

void CheckRemoveRedirectPatchString(struct TRANSLATE *t) {
    char *pr, *ps;

    pr = t->url + strlen(t->url) - strlen(REDIRECTPATCHSTRING);
    if (pr < t->url)
        return;

    if (*pr == '&')
        ps = pr + 1;
    else {
        pr -= 2;
        if (pr < t->url)
            return;

        if (*pr == '%' && *(pr + 1) == '2' && *(pr + 2) == '6')
            ps = pr + 3;
        else
            return;
    }

    if (strcmp(ps, REDIRECTPATCHSTRING + 1) == 0)
        *pr = 0;
}

/* This routine extracts a MuseMarker value from a URL and returns a dynamic string copy of the
 * value if found */
/* Extracted string has two NULLs at the end to allow special processing when MuseCookie processed
 */

/*
There are four variations that must be addressed

1 ?marker=value  ===
2 ?marker=value&nonmarker=value === ?nonmarker=value
3 ?nonmarker=value&marker=value === ?nonmarker=value
4 ?nonmarker=value&maker=value&nonmarker=value === ?nonmarker=value&nonmarker=value
*/

char *MuseMarkerValue(char *url, char *marker, BOOL extractAndReturn) {
    char *markerStart, *valueStart, *valueStop, *markerStop;
    char *qa, *m, *v, *a, *e;
    size_t markerLen = strlen(marker), valueLen;
    char *valueCopy = NULL;

    markerStop = valueStart = valueStop = markerStop = NULL;

    for (qa = strchr(url, '?'); qa && *qa; qa = strchr(qa + 1, '&')) {
        m = qa + 1;
        if (strncmp(m, marker, markerLen) != 0)
            continue;
        v = m + markerLen;
        if (*v != 0) {
            if (*v != '=')
                continue;
            v++;
        }

        a = strchr(v, '&');
        e = strchr(v, 0);

        if (a == NULL)
            markerStart = qa;
        else
            markerStart = (*qa == '?' ? m : qa);

        valueStart = v;
        valueStop = (a ? a - 1 : e - 1);

        if (a == NULL)
            markerStop = e - 1;
        else
            markerStop = (*qa == '?' ? a : a - 1);

        valueStop = valueStop + 1;
        markerStop = markerStop + 1;

        if (extractAndReturn) {
            valueLen = valueStop - valueStart;
            /* Leave room to add an extra null at the end when performing cookie processing */
            if (!(valueCopy = malloc(valueLen + 2)))
                PANIC;
            if (valueLen)
                memmove(valueCopy, valueStart, valueStop - valueStart);
            *(valueCopy + valueLen) = 0;
            *(valueCopy + valueLen + 1) = 0;
            UnescapeString(valueCopy);
        }

        StrCpyOverlap(markerStart, markerStop);

        /* was initialized to NULL, so if not extracted, will still be NULL */
        return valueCopy;
    }

    return NULL;
}

BOOL SendMuseReferer(struct TRANSLATE *t) {
    struct UUSOCKET *w = t->wsocketPtr;
    char *referer = NULL;

    if (t->museReferer && *t->museReferer) {
        if (!(referer = malloc(strlen(t->museReferer) + 256)))
            PANIC;
        strcpy(referer, SkipWS(t->museReferer));
        ReverseEditURL(t, referer, NULL);
        UUSendZ(w, "Referer: ");
        UUSendZCRLF(w, referer);
        FreeThenNull(referer);
        return 1;
    }

    return 0;
}

static size_t StrLenMinusCrLf(char *s) {
    size_t len = strlen(s);
    char *p = s + len - 1;

    for (; p >= s && (*p == 13 || *p == 10); len--, p--)
        ;

    return len;
}

static void OpenFiddlerFiles(struct TRANSLATE *t) {
    struct SESSION *e = t->session;
    struct UUSOCKET *s = &t->ssocket;
    struct UUSOCKET *w = t->wsocketPtr;
    char *field;
    char destDir[MAX_PATH];
    char destName[MAX_PATH];
    int fiddlerSessionIndex;
    size_t len;

    if (!(optionFiddler && e && e->fiddlerActive)) {
        return;
    }

    strcpy(destDir, FIDDLERDIR);
    if (e->nextFiddlerSessionIndex == 0) {
        UUmkdir(destDir);
    }
    strcat(destDir, DIRSEP);
    strcat(destDir, e->key);
    if (e->nextFiddlerSessionIndex == 0) {
        UUmkdir(destDir);
    }

    strcat(destDir, DIRSEP);

    // Let's move the file right here, right now before a lot of data has been tied up in the copy
    UUAcquireMutex(&mFiddler);
    for (;;) {
        fiddlerSessionIndex = e->nextFiddlerSessionIndex * 2 + 1;
        e->nextFiddlerSessionIndex++;

        sprintf(destName, "%s%06d_c.txt", destDir, fiddlerSessionIndex);
        s->fiddlerRecvFile =
            UUopenCreateFD(destName, O_CREAT | O_EXCL | O_WRONLY | O_BINARY, defaultFileMode);
        if (s->fiddlerRecvFile >= 0) {
            break;
        }
        if (errno == EEXIST)
            continue;
        Log("Unable to create %s: %d", destName, errno);
        break;
    }
    UUReleaseMutex(&mFiddler);
    if (s->fiddlerRecvFile < 0) {
        return;
    }

    sprintf(destName, "%s%06d_s.txt", destDir, fiddlerSessionIndex);
    s->fiddlerSendFile =
        UUopenCreateFD(destName, O_CREAT | O_WRONLY | O_TRUNC | O_BINARY, defaultFileMode);

    fiddlerSessionIndex++;
    sprintf(destName, "%s%06d_c.txt", destDir, fiddlerSessionIndex);
    w->fiddlerSendFile =
        UUopenCreateFD(destName, O_CREAT | O_WRONLY | O_TRUNC | O_BINARY, defaultFileMode);
    sprintf(destName, "%s%06d_s.txt", destDir, fiddlerSessionIndex);
    w->fiddlerRecvFile =
        UUopenCreateFD(destName, O_CREAT | O_WRONLY | O_TRUNC | O_BINARY, defaultFileMode);

    len = StrLenMinusCrLf(t->fiddlerRequest);
    write(s->fiddlerRecvFile, t->fiddlerRequest, len);
    write(s->fiddlerRecvFile, "\r\n", 2);
    for (field = t->requestHeaders; *field; field = NextField(field)) {
        len = StrLenMinusCrLf(field);
        if (len == 0) {
            Log("yep it ends 0");
        }
        write(s->fiddlerRecvFile, field, len);
        write(s->fiddlerRecvFile, "\r\n", 2);
    }
    write(s->fiddlerRecvFile, "\r\n", 2);
}

BOOL MatchFieldName(char *browserHeader, char *testHeader) {
    char *endBrowserHeader;

    if (browserHeader == NULL || testHeader == NULL) {
        return FALSE;
    }

    endBrowserHeader = StartsIWith(browserHeader, testHeader);
    if (endBrowserHeader == NULL) {
        return FALSE;
    }

    endBrowserHeader = SkipWS(endBrowserHeader);
    return *endBrowserHeader == ':';
}

static BOOL BlockCookie(struct DATABASE *b, char *cookie) {
    enum COOKIEFILTER_PROCESSING processing = CFPPROCESS;
    char *cookieName = NULL;  // use lazy assignment
    struct COOKIEFILTER *cf;
    int pass;

    for (pass = 0; pass < 2; pass++) {
        if (pass == 0) {
            cf = globalCookieFilters;
        } else {
            cf = b ? b->cookieFilters : NULL;
        }
        for (; cf && processing != CFPBLOCKMANDATORY; cf = cf->next) {
            if (cookieName == NULL) {
                char *equals = strchr(cookie, '=');
                if (equals == NULL) {
                    break;
                }
                size_t cookieLen = equals - cookie;
                if (!(cookieName = malloc(cookieLen + 1)))
                    PANIC;
                memcpy(cookieName, cookie, cookieLen);
                cookieName[cookieLen] = 0;
            }
            if (WildCompare(cookieName, cf->cookieName)) {
                processing = cf->processing;
            }
        }
    }
    FreeThenNull(cookieName);
    return processing == CFPBLOCK || processing == CFPBLOCKMANDATORY;
}

BOOL ForwardRequest(struct TRANSLATE *t) {
    struct UUSOCKET *s;
    char dummy[3];
    char *field;
    struct UUSOCKET *w;
    BOOL lastOK;
    BOOL authPresent;
    char *validFields[] = {"Cookie:",
                           "User-Agent:",
                           "Referer:",
                           "If-Modified-Since:",
                           "Date:",
                           "From:",
                           "Content-Length:",
                           "Content-Type:",
                           "Authorization:",
                           "Unless-Modified-Since:",
                           "Pragma:",
                           "Cache-Control:",
                           "Accept:",
                           "Accept-Language:",
                           "Accept-Charset:",
                           "Range:",
                           "Request-Range:",
                           "If-Range:",
                           "destination:",
                           "allow-rename:",
                           "overwrite:",
                           "translate:",
                           "brief:",
                           "depth:",
                           "notification-type:",
                           "subscription-id:",
                           "Ajax-method:",
                           "AjaxPro-method:",
                           "X-AjaxPro-method:",
                           "SOAPAction:",
                           "Expect:",
                           "x-microsoftajax:",
                           "X-JSON:",
                           "Origin:",
                           "Access-Control-Request-Headers:",
                           "Access-Control-Request-Method:",
                           NULL};
    char **vfp;
    intmax_t contentLength;
    size_t mlen, ulen, hlen, plen, blen, slen;
    char *p;
    char *nf;
    char *semi;
    char *referer;
    char *cookieValue;
    char sport[16];
    BOOL sendCookies;
    BOOL redirectPatch;
    enum OPTIONCOOKIE optionCookie;
    time_t headerIfModifiedSince = 0;
    struct COOKIEFILTER *cf;
    int cfi;
    char *pa;
    char *crcPrint = "/tables/print-feed.asp?source=http://";
    char *crcOfs;
    int funcStatus = 0;
    struct DATABASE *b;
    BOOL noSlash;

    BOOL sentDomainCookies;
    BOOL sentHost = 0;
    BOOL sentReferer = 0;
    BOOL sentPragma = 0;
    BOOL sentAcceptEncoding = 0;
    BOOL sentXForwardedFor = 0;
    char *cookieStarts;
    struct IDENTIFIERSCOPE *scope;

    b = (t->host ? (t->host)->myDb : NULL);

    /*  sendCookies = t->host == NULL || (t->host)->myDb == NULL || ((t->host)->myDb)->optionCookie
     * == OCALL; */
    optionCookie = (b != NULL ? b->optionCookie : OCALL);

    sendCookies =
        (optionCookie == OCALL || optionCookie == OCPASSTHROUGH) && ((t->session)->anonymous == 0);

    redirectPatch = b && b->redirectPatch;

    s = &t->ssocket;
    w = t->wsocketPtr;

    OpenFiddlerFiles(t);

    if (b && b->optionLawyeePatch && strcmp(t->method, "POST") == 0) {
        char line[1024];
        UUSendZ(w, "OPTIONS * HTTP/1.0\r\n");
        ForwardRequestSendHost(t, w);
        UUSendZ(w, "Connection: Keep-Alive\r\n\r\n");
        while (ReadLine(w, line, sizeof(line) - 1)) {
            Trim(line, TRIM_TRAIL);
            if (line[0] == 0)
                break;
        }
    }

    /*  printf("method %s for %s\n", t->method, t->url); */
    /*  WriteLine(r, "%s ", t->method); */
    if (redirectPatch) {
        CheckRemoveRedirectPatchString(t);
    }

    mlen = strlen(t->method);
    ulen = strlen(t->url) + 1;

    if (t->connectedByProxy) {
        slen = (t->host)->useSsl ? 1 : 0;
        hlen = strlen((t->host)->hostname);
        /* When the :80 was included, the NetCache at nus.edu.sg would add the :80 to https
           URLs, which was a very bad thing, so now it is suppressed when possible
        */
        if ((t->host)->useSsl == 0 && (t->host)->remport == 80)
            sport[0] = 0;
        else
            sprintf(sport, ":%d", (t->host)->remport);
        plen = strlen(sport);
    } else {
        hlen = plen = slen = 0;
        sport[0] = 0;
    }

    blen = mlen + 1;
    if (hlen)
        blen = blen + slen + hlen + plen +
               7; /* The 7 is http://, the : for port used to be here but isn't any more */

    if (noSlash = *t->url != '/')
        blen++;

    memmove(t->url + blen, t->url, ulen);
    p = t->url;
    memmove(p, t->method, mlen);
    p += mlen;
    *p++ = ' ';
    if (hlen) {
        memmove(p, "http", 4);
        p += 4;
        if ((t->host)->useSsl)
            *p++ = 's';
        memmove(p, "://", 3);
        p += 3;
        memmove(p, (t->host)->hostname, hlen);
        p += hlen;
        if (plen) {
            memmove(p, sport, plen);
            p += plen;
        }
    }
    if (noSlash)
        *p++ = '/';

    if (redirectPatch) {
        CheckRemoveRedirectPatchString(t);
    }

    if (t->host && (t->host)->hostname &&
        stricmp((t->host)->hostname, "www.hbcpnetbase.com") == 0) {
        crcOfs = strstr(t->url, crcPrint);
        if (crcOfs) {
            ReverseEditURL(t, crcOfs + strlen(crcPrint) - 7, NULL);
        }
    }

    if (b && b->metaFindMuseCookie) {
        t->museCookie = MuseMarkerValue(t->url, "StartMuseCookie", 1);
        t->museReferer = MuseMarkerValue(t->url, "StartMuseReferer", 1);
        (void)MuseMarkerValue(t->url, "StartMuseAuthorization", 0);
        (void)MuseMarkerValue(t->url, "StartMuseProxyAuthorization", 0);
        (void)MuseMarkerValue(t->url, "StartMuseCharset", 0);
    }

    strcat(t->url, " ");
    if (optionDisableHTTP11)
        strcat(t->url, "HTTP/1.0");
    else
        strcat(t->url, "HTTP/1.1");
    strcat(t->url, "\r\n");

    UUSendZ(w, t->url);

    memmove(t->url, t->url + mlen + 1, ulen);
    *(t->url + ulen - 1) = 0;

    /*  WriteLine(r, " %s\r\n", HTTPVERSION); */
    /* For SSL, this was done in ConnectToServer as part of the CONNECT sequence */
    pa = (b ? b->proxyAuth : NULL);
    if (t->session && (t->session)->sessionProxyAuth)
        pa = (t->session)->sessionProxyAuth;
    if (t->connectedByProxy && (t->host)->useSsl == 0 && pa != NULL) {
        UUSendZ(w, "Proxy-authorization: Basic ");
        UUSendZCRLF(w, pa);
    }

    sentDomainCookies = !sendCookies;
    lastOK = 0;
    authPresent = 0;
    t->ifModifiedSince = 0;

    /*
    ForwardRequestSendHost(t, r);
    sentHost = 1;
    */

    // We want to add user headers first
    struct ADDHEADER *addheader;
    for (addheader = b->addHeader; addheader; addheader = addheader->next) {
        // have to track these to escape them outside the upcoming for loop.
        if (stricmp(addheader->name, "host") == 0) {
            sentHost = 1;
        } else if (stricmp(addheader->name, "accept-encoding") == 0) {
            sentAcceptEncoding = 1;
        } else if (stricmp(addheader->name, "pragma") == 0) {
            sentPragma = 1;
        } else if (stricmp(addheader->name, "x-forwarded-for") == 0) {
            sentXForwardedFor = 1;
        }

        char *exprVal = ExpressionValue(t, NULL, NULL, addheader->expression);
        if (!exprVal)
            Log("Failed to evaluate AddHeader(%s) expression.", addheader->name);

        WriteLine(w, "%s: %s\r\n", addheader->name, exprVal);
        FreeThenNull(exprVal);
    }

    if (byteServes && t->host && t->host->hostUrl) {
        struct BYTESERVE *bs;
        int ovecsize = 30;
        int ovector[ovecsize];
        char url[strlen(t->host->hostUrl) + strlen(t->urlCopy) + 1];
        strcpy(url, t->host->hostUrl);
        strcat(url, t->urlCopy);
        int urlLen = strlen(url);
        for (bs = byteServes; bs; bs = bs->next) {
            if (pcre_exec(bs->regexUri, bs->regexUriExtra, url, urlLen, 0, PCRE_ANCHORED, ovector,
                          ovecsize) >= 0) {
                t->isPDF = 1;
                t->html = 0;
                t->javaScript = 0;
                break;
            }
        }
    }

    SendIdentifier(t);
    scope = IdentifierScopeByHost(t->host);

    for (field = t->requestHeaders; *field; field = nf) {
        LinkField(field);
        nf = NextField(field);
        lastOK = 0;
        /* If we are adding a user header, then suppress any corresponding header that tries to come
         * through */
        if (b->addUserHeader && strnicmp(field, b->addUserHeader, b->addUserHeaderColon) == 0)
            continue;

        if (MatchFieldName(field, IDENTIFIERHEADER)) {
            continue;
        }

        // If identifier is using an alternate header, suppress attempts to send the alternate
        // header as well.
        if (scope && scope->header && MatchFieldName(field, scope->header)) {
            continue;
        }

        // Skip any AddHeader headers
        BOOL match = FALSE;
        for (addheader = b->addHeader; addheader; addheader = addheader->next) {
            if (stricmp(field, addheader->name) == 0) {
                match = TRUE;
                break;
            }
        }
        if (match)
            continue;

        for (vfp = validFields; *vfp != NULL; vfp++) {
            if (strnicmp(field, *vfp, strlen(*vfp)) == 0) {
                lastOK = 1;
                break;
            }
        }

        if (ProcessHttpHeaders(t, w, HHDREQUEST, field, &lastOK)) {
            continue;
        }
        /* If the modified since EZPROXYCFG changed, force update
         */
        if (strnicmp(field, "If-Modified-Since:", 18) == 0) {
            headerIfModifiedSince = ParseHttpDate(SkipWS(field + 18), lastModifiedMinimum);
            if (headerIfModifiedSince < lastModifiedMinimum)
                lastOK = 0;
        }

        /* If we detect the Referer field, make a note of where it is and suppress sending it */
        /* for now.  After this loop, we can reverse edit the URL before the buffer is destroyed */
        if (strnicmp(field, "Referer:", 8) == 0) {
            sentReferer = 1;
            if (!SendMuseReferer(t)) {
                if (b && b->optionHideEZproxy && IsMyHost(SkipWS(field + 8))) {
                    /* skip it for those conditions */
                } else {
                    if (!(referer = malloc(strlen(field) + 256)))
                        PANIC;
                    strcpy(referer, field);
                    p = SkipWS(referer + 8);
                    ReverseEditURL(t, p, NULL);
                    UUSendZCRLF(w, referer);
                    FreeThenNull(referer);
                }
            }
            lastOK = 0;
        }

        if (strnicmp(field, "Origin:", 7) == 0) {
            char *origin;
            if (!(origin = malloc(strlen(field) + 256)))
                PANIC;
            strcpy(origin, field);
            p = SkipWS(origin + 7);
            ReverseEditURL(t, p, NULL);
            UUSendZCRLF(w, origin);
            FreeThenNull(origin);
            lastOK = 0;
        }

        if (strnicmp(field, "SOAPAction:", 11) == 0) {
            char *SOAPaction;
            if (!(SOAPaction = malloc(strlen(field) + 256)))
                PANIC;
            strcpy(SOAPaction, field);
            p = SkipWS(SOAPaction + 11);
            ReverseEditURL(t, p, NULL);
            UUSendZCRLF(w, SOAPaction);
            FreeThenNull(SOAPaction);
            lastOK = 0;
        }

        if (p = StartsIWith(field, "Expect:")) {
            /* At some point, we may allow the 100-continue response to come
            back through, but for now, we suppress it and send the
            417 Expectation failed response, then suppress it
            */
            /*
            t->expect100Continue |= StrIStr(p, "100-continue") != NULL;
            */
            char httpDate[HTTPDATELENGTH];
            time_t now;
            if (StrIStr(p, "100-continue")) {
                WriteLine(s, "HTTP/1.1 417 Expectation failed\r\nDate: %s\r\n\r\n",
                          FormatHttpDate(httpDate, sizeof(httpDate), UUtime(&now)));
                UUFlush(s);
            }

            lastOK = 0;
        }

        if (p = StartsIWith(field, "Connection:")) {
            if (StrIStr(p, "close"))
                t->clientKeepAlive = 0;
            /* This header is hop-to-hop and should never be forwarded */
            lastOK = 0;
        }

        if (strnicmp(field, "Host:", 5) == 0) {
            if (!sentHost) {
                ForwardRequestSendHost(t, w);
                sentHost = 1;
            }
            lastOK = 0;
        }

        if (strnicmp(field, "destination:", 12) == 0) {
            char *destination;

            if (!(destination = malloc(strlen(field) + 256)))
                PANIC;
            strcpy(destination, field);
            p = SkipWS(destination + 12);
            ReverseEditURL(t, p, NULL);
            UUSendZCRLF(w, destination);
            FreeThenNull(destination);
            lastOK = 0;
        }

        /* Allow the Range header through only for PDF images */
        if (strnicmp(field, "Range:", 6) == 0 || strnicmp(field, "If-Range:", 9) == 0 ||
            strnicmp(field, "Request-Range:", 14) == 0) {
            lastOK = t->isPDF;
        }

        /* If we see an Authorization header, make a note of it */
        if (strnicmp(field, "Authorization:", 14) == 0) {
            authPresent = 1;
        }

        /* Let's suppress the ezproxy= cookie, which we do with whitespace since there might be
         * multiple */
        /* cookies being sent here */
        /* Likewise, suppress any cookies from the "CookieFilter" structure */
        if (strnicmp(field, "Cookie:", 7) == 0) {
            lastOK = 0;
            if (sendCookies) {
                BOOL skipCookie;
                cookieValue = NULL;
                for (p = field + 7; p && *p;) {
                    while (*p && (IsWS(*p) || *p == ';'))
                        p++;
                    if (*p == 0)
                        break;

                    skipCookie = FALSE;

                    cookieStarts = StartsWith(p, loginCookieName);
                    if (cookieStarts != NULL) {
                        if (*cookieStarts == 'n' || *cookieStarts == 'l') {
                            cookieStarts++;
                        }
                        if (*cookieStarts == '=') {
                            cookieStarts++;
                        } else {
                            cookieStarts = NULL;
                        }
                    }

                    if (cookieStarts != NULL) {
                        skipCookie = TRUE;
                    } else {
                        skipCookie = BlockCookie(b, p);
                    }

                    if (!skipCookie) {
                        p = strchr(p, ';');
                        continue;
                    }

                    semi = strchr(p, ';');
                    if (semi == NULL) {
                        for (p--; p >= field && (IsWS(*p) || *p == ';'); p--)
                            ;
                        *(p + 1) = 0;
                        break;
                    }
                    semi = SkipWS(semi + 1);
                    StrCpyOverlap(p, semi);
                }

                Trim(field, TRIM_TRAIL);
                lastOK = 0;
                if (strlen(field) > 7) {
                    if (sentDomainCookies)
                        lastOK = 1;
                    else {
                        SendDomainCookies(t, field);
                        sentDomainCookies = 1;
                    }
                }
            }
        }

        /*      if (!lastOK) */
        /*          printf("Not-"); */

        /*      printf("Sending: %s\n", field); */
        if (lastOK) {
            UUSendZCRLF(w, field);
        }
    }

    if (t->sendZipAllowed && !sentAcceptEncoding)
        UUSendZ(w, "Accept-Encoding: gzip, deflate\r\n");

    if (sentReferer == 0)
        SendMuseReferer(t);

    if (b && b->sendNoCache && !sentPragma) {
        UUSendZ(w, "Pragma: no-cache\r\nCache-Control: no-cache\r\n");
    }

    if (sentDomainCookies == 0 || optionCookie == OCDOMAIN)
        SendDomainCookies(t, NULL);

    ProcessHttpHeaders(t, w, HHDREQUEST, NULL, NULL);

    contentLength =
        (field = FindField("Content-Length", t->requestHeaders)) != NULL ? atoll(field) : 0;

    /* If the user didn't send an authorization, and we have something we are suppose to send, send
     * it */
    if (authPresent == 0 && (t->host)->validate != NULL) {
        struct VALIDATE *v;
        for (v = (t->host)->validate; v; v = v->next) {
            if (v->path == NULL || WildCompare(t->urlCopy, v->path)) {
                if (v->auth && *(v->auth)) {
                    UUSendZ(w, "Authorization: Basic ");
                    UUSendZCRLF(w, v->auth);
                }
                break;
            }
        }
    }

    if (!sentHost) {
        ForwardRequestSendHost(t, w);
    }

    if (b && b->optionXForwardedFor && !sentXForwardedFor) {
        char ipBuffer[INET6_ADDRSTRLEN];
        WriteLine(w, "X-Forwarded-For: %s\r\n", ipToStr(&t->sin_addr, ipBuffer, sizeof ipBuffer));
    }

    if (b->addUserHeader && t->session && (t->session)->logUserBrief) {
        char *lub = (t->session)->logUserBrief;

        UUSendZ(w, b->addUserHeader);
        if (b->addUserHeaderBase64) {
            char *b64;
            b64 = Encode64(NULL, lub);
            UUSendZ(w, b64);
            FreeThenNull(b64);
        } else {
            UUSendZ(w, lub);
        }
        UUSendCRLF(w);
    }

    UUSendZ(w, "TE: chunked\r\nConnection: keep-alive\r\n\r\n");

    /* pre-3.6j
    UUSendZ(r, "Connection: close\r\n\r\n");
    */

    UUFlush(w);

    t->finalStatus = 903;

    /* If there is content to upload, send it */
    if (contentLength > 0 && strcmp(t->method, "HEAD") != 0 && t->suppressPostContent == 0) {
        /*      printf("Send post content\n"); */
        if (debugLevel > 0)
            Log("POST phase 1 %" PRIdMAX " bytes", contentLength);
        t->finalStatus = 904;
        if (!RawTransfer(t, w, s, contentLength, optionPostCRLF)) {
            funcStatus = 1;
            goto ReturnStatus;
        }
        t->finalStatus = 905;
        if (!UUValidSocketSend(w)) {
            funcStatus = 2;
            goto ReturnStatus;
        }
        t->finalStatus = 907;

        /* Some web browsers add a CR/LF after the content.  The RFC doesn't say they should,
        but they do.  We used to reap this at the end of processing, but MBC had a problem with
        their AS/400 proxying, so now we try to consume this in non-blocking mode
        */
        if (debugLevel > 0)
            Log("POST phase 2");
        UURecv(s, dummy, sizeof(dummy), UUSNOWAIT);
        if (debugLevel > 0)
            Log("POST phase 3");
    }

    t->finalStatus = 906;
    funcStatus = 0;

ReturnStatus:
    return funcStatus;
}
