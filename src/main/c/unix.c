#define __UUFILE__ "unix.c"

#include "ezproxyversion.h"
#include "common.h"
#include "unix.h"
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/uio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
#include <signal.h>
#include <ucontext.h>
#if defined(GNUBACKTRACE)
#include <execinfo.h>
#endif

BOOL inCharge = 0;
BOOL cancleAck = 0;

long int interrupted_neg_int(long int rc) {
    volatile long int x;
    volatile long int y;
    volatile long int z;

    if (rc == -1 && errno == EINTR) {
        x = guardian->chargeStop;
        y = inCharge;
        z = cancleAck;
        if (x == 1 && y == 1 && z == 0) {
            SetErrno(ECANCELED);
            return -1;
        }
    }
    return rc;
}

FILE *interrupted_null_file(FILE *f) {
    volatile long int x;
    volatile long int y;
    volatile long int z;

    if (f == NULL && errno == EINTR) {
        x = guardian->chargeStop;
        y = inCharge;
        z = cancleAck;
        if (x == 1 && y == 1 && z == 0) {
            SetErrno(ECANCELED);
            return NULL;
        }
    }
    return f;
}

void ackCancel() {
    cancleAck = 1;
}

void EnablePrivilege(void) {
    if (runUid)
        seteuid(0);
    return;
}

void SuspendPrivilege(void) {
    if (runUid)
        seteuid(runUid);
}

void AbdicatePrivilege(void) {
    if (runUid)
        setuid(runUid);
}

void GuardianAtExit(void) {
    struct shmid_ds shmid_ds;

    /*
     * Delete shared memory segment when Guardian terminates, no matter how it terminates.
     * If there's no guardianSharedMemoryID, there's nothing to do.
     */
    if (mySharedMemoryID) {
        shmctl(mySharedMemoryID, IPC_RMID, &shmid_ds);
        if (debugLevel > 0) {
            Log("Shared memory segment %d removed by process %d.", mySharedMemoryID, getpid());
        }
        mySharedMemoryID = 0;
        guardianSharedMemoryID = 0;
        UnlinkIPCFILEandPidFile();
    }
}

void stackTrace(int f, char *sep) {
    /* To cross-reference a backtrace, run "objdump -d" on the unstripped program file.
       objdump is standard on Linux; for Solaris, download from
       http://ftp.gnu.org/gnu/binutils/
    */
#ifdef GNUBACKTRACE
#define GNUBACKTRACEFRAMES 200
    {
        void *array[GNUBACKTRACEFRAMES];
        size_t size;
        size_t i;
        char **listOfStrings;

        size = backtrace(array, GNUBACKTRACEFRAMES);
        listOfStrings = backtrace_symbols(array, size);
        for (i = 1; i < size;) {
            UUwriteFD(f, listOfStrings[i], strlen(listOfStrings[i]));
            i++;
            if (i < size) {
                UUwriteFD(f, sep, strlen(sep));
            }
        }
        free(listOfStrings);
    }
#elif defined(SOLBACKTRACE)
    {
        int printstack(int dofd);

        printstack(f);
    }
#else
    {
        char buffer[128];

        sprintf(buffer, "Stack trace is not available.\n");
        UUwriteFD(f, buffer, strlen(buffer));
    }
#endif
    UUwriteFD(f, "\n", 1);
}

void WriteTrace(int f, int sig, void *faultAddr, char *message) {
    char buffer[128];
    time_t now;

    UUtime(&now);
    ASCIIDate(&now, buffer);
    UUwriteFD(f, buffer, strlen(buffer));

    sprintf(buffer, " PID %d", getpid());
    UUwriteFD(f, buffer, strlen(buffer));

    if (sig != 0) {
        sprintf(buffer, " caught signal %d", sig);
        UUwriteFD(f, buffer, strlen(buffer));
    }

    if (faultAddr != NULL) {
        sprintf(buffer, " at address %p", faultAddr);
        UUwriteFD(f, buffer, strlen(buffer));
    }

    if ((message != NULL) && (*message != 0)) {
        UUwriteFD(f, ", ", 2);
        UUwriteFD(f, message, strlen(message));
    }

    UUwriteFD(f, "\n", 1);

    stackTrace(f, "\n");
}

void WriteTraceFile(int sig, void *faultAddr, char *message) {
    int f;

    f = UUopenSimpleFD(FAILFILE, O_APPEND | O_CREAT | O_WRONLY, defaultFileMode);

    UUwriteFD(f, EZPROXYVERSION, strlen(EZPROXYVERSION));
    UUwriteFD(f, "\n", 1);

    WriteTrace(f, sig, faultAddr, message);

    UUcloseFD(f);
}

void handle_fatal(int sig, siginfo_t *info, void *context) {
    ucontext_t *uc = (ucontext_t *)context;
    struct sigaction act, prevAct;
    void *faultAddr = NULL;

    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;

    act.sa_handler = SIG_IGN;
    sigaction(SIGHUP, &act, &prevAct);
    sigaction(SIGTERM, &act, &prevAct);
    sigaction(SIGINT, &act, &prevAct);
    sigaction(SIGBUS, &act, &prevAct);
    sigaction(SIGSEGV, &act, &prevAct);
    sigaction(SIGILL, &act, &prevAct);
    sigaction(SIGABRT, &act, &prevAct);

/* The instruction pointer register offsets (32-bit REG_EIP, 64-bit REG_RIP)
   are not defined in LSB.  Offset values taken from /usr/include/sys/ucontext.h
*/
#if defined(__i386) || defined(__i386__)
#ifndef REG_EIP
#define REG_EIP 14
#endif
    faultAddr = (void *)uc->uc_mcontext.gregs[REG_EIP];
#elif defined(__amd64) || defined(__x86_64__)
#ifndef REG_RIP
#define REG_RIP 16
#endif
    faultAddr = (void *)uc->uc_mcontext.gregs[REG_RIP];
#elif defined(__sparc)
    faultAddr = (void *)uc->uc_mcontext.gregs[REG_PC];
#endif

    WriteTraceFile(sig, faultAddr, "fatal");

    /*
     * If this is called by the Charge, exit(!=0) tells the
     * Guardian (parent) process not to start a new Charge process.
     *
     * If this is called by the Guardian, exit(!=0) just stops the Guardian.
     */
    exit(16);
}

void handle_sigabrt(int sig) {
    struct sigaction act, prevAct;

    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;

    act.sa_handler = SIG_IGN;
    sigaction(SIGHUP, &act, &prevAct);
    sigaction(SIGTERM, &act, &prevAct);
    sigaction(SIGINT, &act, &prevAct);
    sigaction(SIGBUS, &act, &prevAct);
    sigaction(SIGSEGV, &act, &prevAct);
    sigaction(SIGILL, &act, &prevAct);
    sigaction(SIGABRT, &act, &prevAct);

    WriteTraceFile(sig, NULL, "fatal");

    /*
     * SIGABRT never returns to the program.
     */
}

/*
 * SIGUSR1 and SIGUSR2 are signals sent to a process to indicate user-defined conditions.
 *
 * We use SIGUSR1 to signal that the processes' global volatile variable named sigusr
 * should be incremented by one.
 */
void handle_sigusr1(int sig) {
    char buffer[128];

    sigusr--;

    safe_portable_snprintf(buffer, 128, "sigusr = %d\n", sigusr);
    UUwriteFD(2, buffer, strlen(buffer));
}

/*
 * SIGUSR1 and SIGUSR2 are signals sent to a process to indicate user-defined conditions.
 *
 * We use SIGUSR2 to signal that the processes' global volatile variable named sigusr
 * should be decremented by one.
 */
void handle_sigusr2(int sig) {
    char buffer[128];

    sigusr++;

    safe_portable_snprintf(buffer, 128, "sigusr = %d\n", sigusr);
    UUwriteFD(2, buffer, strlen(buffer));
}

/*
 * SIGCHLD is the signal sent to a process when a child process terminates.
 * In Unix, a process can have children created by fork/vfork or similar system
 * calls . When the child terminates a SIGCHLD signal is sent to the parent. By
 * default the signal is simply ignored.[1] However commonly wait system call
 * implemented in a handler for the SIGCHLD, so that the parent may act upon the
 * exit status of the child.
 *
 * When a child process terminates before the parent has called wait, the kernel
 * retains some information about the process to enable its parent to call wait
 * later [2]. Because the child is still consuming system resources but not
 * executing it is known as a zombie process.
 *
 * POSIX.1-2001 allows a parent process to elect for the kernel to automatically
 * reap child processes that terminate by setting the disposition of SIGCHLD to
 * SIG_IGN or by setting the SA_NOCLDWAIT flag for the SIGCHLD signal; Linux 2.6
 * kernels adhere to this behavior, and FreeBSD supports both of these methods
 * since version 5.0 [3]. However, because of historical differences between
 * System V and BSD behaviors with regard to ignoring SIGCHLD, calling wait
 * remains the most portable paradigm for cleaning up after forked child
 * processes.
 */
void handle_sigchld(int arg) {
    /*
     * Can't use SIG_IGN or we calls to waitpid() will fail, so use this null
     * procedure.
     */
    return;
}

/*
 * SIGINT is the signal sent to a process by its controlling terminal when a user
 * wishes to interrupt the process.
 *
 * We use SIGINT to signal that the process should terminate.
 */
void handle_sigint(int sig) {
    exit(0);
}

/*
 * SIGHUP is a signal sent to a process when its controlling terminal is closed.
 * (It was originally designed to notify the process of a serial line drop).
 * Signals have always been a convenient method of inter-process communication
 * (IPC), but in early implementations there were no user-definable signals (such
 * as the later additions of SIGUSR1 and SIGUSR2) that programs could intercept
 * and interpret for their own purposes. For this reason, applications that did
 * not require a controlling terminal, such as daemons, would re-purpose SIGHUP
 * as a signal to re-read configuration files, or reinitialize.
 *
 * We use SIGHUP to signal that the access file should be closed.
 */
void handle_sighup(int arg) {
    if (guardian) {
        if (inCharge == FALSE) {
            guardian->renameLog[0] = 0;         /* says to not rename the access file */
            guardian->dontReopenAccessFile = 0; /* says to reopen the access file */
        }
        UUAcquireMutex(&mMsgFile);
        CloseMsgFile();
        OpenMsgFile();
        UUReleaseMutex(&mMsgFile);
    }
}

/*
 * SIGTERM is the signal sent to a process to request its termination.  SIGTERM
 * is the default signal sent to a process by the kill or killall commands. It
 * causes the termination of a process, but unlike the SIGKILL signal, it can
 * be caught and interpreted (or ignored) by the process. Therefore, SIGTERM is
 * akin to asking a process to terminate nicely, allowing cleanup and closure
 * of files. For this reason, on many Unix systems during shutdown, init issues
 * SIGTERM to all processes that are not essential to powering off, waits a few
 * seconds, and then issues SIGKILL to forcibly terminate any such processes
 * that remain.
 *
 * We use SIGTERM to signal that the program should stop in a normal way.
 */
void handle_sigterm(int arg) {
    if (guardian) {
        guardian->chargeStatus = chargeStopping;
        guardian->chargeStop = TRUE;
    }
}

/*
 * SIGXFSZ is the signal sent to a process when it grows a file larger than the
 * maximum allowed size.  SIGXFSZ is sent to a process when it causes a file to grow
 * larger than the maximum allowed size, as determined by the ulimit system call and
 * shell builtin.
 *
 * We use SIGXFSZ to signal that the program should "roll" its files.
 */
void handle_sigxfsz(int arg) {
    if (guardian) {
        if (inCharge == FALSE) {
            guardian->renameLog[0] = 0;         /* says to not rename the access file */
            guardian->dontReopenAccessFile = 0; /* says to reopen the access file */
        }
        UUAcquireMutex(&mMsgFile);
        CloseMsgFile();
        OpenMsgFile();
        UUReleaseMutex(&mMsgFile);
    }
}

/*
 * Only the Charge calls this.  These are the ones that interfere with the debugger's
 * ability to observe the program.
 */
void DisableDebugger(void) {
    struct sigaction act, prevAct, fatalAct;

    memset(&act, 0, sizeof(act));
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;

    memset(&fatalAct, 0, sizeof(fatalAct));
    sigemptyset(&fatalAct.sa_mask);
    fatalAct.sa_flags = SA_SIGINFO;
    fatalAct.sa_sigaction = handle_fatal;

#ifdef SIGBUS
    /*
     * SIGBUS is the signal sent to a process when it causes a bus error. The default
     * action for a program upon receiving SIGBUS is abnormal termination. This will
     * end the process, but may generate a core file to aid debugging, or other
     * platform dependent action.  SIGBUS can be handled (caught). That is,
     * applications can request what action they want to occur. Examples of such
     * action might be ignoring it, calling a function, or restoring the default
     * action. In some circumstances, ignoring SIGBUS results in undefined behavior.
     * Computer programs may throw SIGBUS for improper memory handling:
     *      Invalid address alignment
     *          The program has attempted to read or write data that does not fit
     *          the CPU's memory-alignment rules.
     *      Non-existent physical address
     *          This is equivalent to a segmentation fault, but for a physical
     *          address rather than a virtual address.
     */
    sigaction(SIGBUS, &fatalAct, &prevAct);
#endif

#ifdef SIGSEGV
    /*
     * SIGSEGV is the signal sent to a process when it makes an invalid memory reference,
     * or segmentation fault.  SIGSEGV can be caught; that is, applications can request
     * what action they want to occur in place of the default. Examples of such action
     * might be ignoring it, calling a function, or restoring the default action. In
     * some circumstances, ignoring SIGSEGV results in undefined behavior.
     */
    sigaction(SIGSEGV, &fatalAct, &prevAct);
#endif

#ifdef SIGILL
    /*
     *  SIGILL is the signal sent to a process when it attempts to execute a malformed,
     *  unknown, or privileged instruction.  SIGILL can be handled. That is, programmers
     *  can specify the action they would like to occur upon receiving a SIGILL, such as
     *  execute a subroutine, ignore the event, or restore the default behavior. Some
     *  programs use this mechanism to write diagnostics to an external file.
     */
    sigaction(SIGILL, &fatalAct, &prevAct);
#endif

#ifdef SIGABRT
    /*
     * SIGABRT is sent by the process to itself when it calls the abort() libc function,
     * defined in stdlib.h. The SIGABRT signal can be caught, but it cannot be blocked;
     * if the signal handler returns then all open streams are closed and flushed and
     * the program terminates (dumping core if appropriate) This means that the
     * abort call never returns. Because of this characteristic, it is often used to
     * signal fatal conditions in support libraries, situations where the current
     * operation cannot be completed but the main program can perform cleanup before
     * exiting. It is used when an assertion fails.
     */
    act.sa_handler = handle_sigabrt;
    sigaction(SIGABRT, &act, &prevAct);
#endif

#ifdef SIGFPE
    /*
     * A common oversight is to consider division by zero the only source of SIGFPE
     * conditions. On some architectures (IA-32 included[citation needed]), integer
     * division of INT_MIN (the most negative representable integer value) by -1
     * triggers the signal because the quotient, a positive number, is not
     * representable.
     */
    sigaction(SIGFPE, &fatalAct, &prevAct);
#endif

#ifdef SIGSTKFLT
    /*
     * On some Unix-like platforms, SIGSTKFLT is the signal sent to computer programs
     * when the processor experiences a stack fault (i.e. when the stack is empty
     * or full).  SIGSTKFLT does not appear to be used in any modern Unix-like systems.
     */
    sigaction(SIGSTKFLT, &fatalAct, &prevAct);
#endif

#ifdef SIGXFSZ
    /*
     * SIGXFSZ is the signal sent to a process when it grows a file larger than the
     * maximum allowed size.  SIGXFSZ is sent to a process when it causes a file to grow
     * larger than the maximum allowed size, as determined by the ulimit system call and
     * shell builtin.
     */
    act.sa_handler = handle_sigxfsz;
    sigaction(SIGXFSZ, &act, &prevAct);
#endif
}

void EstablishGuardianSignalHandlers(void) {
    struct sigaction act, prevAct;

    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;

#ifdef SIGINT
    act.sa_handler = handle_sigint;
    sigaction(SIGINT, &act, &prevAct);
#endif

#ifdef SIGPIPE
    act.sa_handler = SIG_IGN;
    sigaction(SIGPIPE, &act, &prevAct);
#endif

#ifdef SIGHUP
    act.sa_handler = handle_sighup;
    sigaction(SIGHUP, &act, &prevAct);
#endif

#ifdef SIGTERM
    act.sa_handler = handle_sigterm;
    sigaction(SIGTERM, &act, &prevAct);
#endif

#ifdef SIGUSR1
    act.sa_handler = handle_sigusr1;
    sigaction(SIGUSR1, &act, &prevAct);
#endif

#ifdef SIGUSR2
    act.sa_handler = handle_sigusr2;
    sigaction(SIGUSR2, &act, &prevAct);
#endif

#ifdef SIGCHLD
#ifdef SA_NOCLDSTOP
    act.sa_flags = SA_NOCLDSTOP;
#else
    act.sa_flags = 0;
#endif
    if (optionIgnoreSIGCHLD) {
        act.sa_handler = SIG_IGN;
    } else {
        act.sa_handler = handle_sigchld;
    }
    sigaction(SIGCHLD, &act, &prevAct);
#endif
}

void EstablishChargeSignalHandlers(void) {
    struct sigaction act, prevAct;

    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;

#ifdef SIGINT
    act.sa_handler = handle_sigint;
    sigaction(SIGINT, &act, &prevAct);
#endif

#ifdef SIGPIPE
    act.sa_handler = SIG_IGN;
    sigaction(SIGPIPE, &act, &prevAct);
#endif

#ifdef SIGHUP
    act.sa_handler = handle_sighup;
    sigaction(SIGHUP, &act, &prevAct);
#endif

#ifdef SIGTERM
    act.sa_handler = handle_sigterm;
    sigaction(SIGTERM, &act, &prevAct);
#endif

#ifdef SIGUSR1
    act.sa_handler = handle_sigusr1;
    sigaction(SIGUSR1, &act, &prevAct);
#endif

#ifdef SIGUSR2
    act.sa_handler = handle_sigusr2;
    sigaction(SIGUSR2, &act, &prevAct);
#endif

#ifdef SIGCHLD
    /* The Charge doesn't create any children. */
#endif
}

int UUInitMutexOS(UUMUTEX *uumutex) {
    if (pthread_mutex_init(&uumutex->mutex, NULL) != 0)
        PANIC;
    return 0;
}

int UUAcquireMutexOS(UUMUTEX *uumutex, const char *file, int line) {
    if (pthread_mutex_lock(&uumutex->mutex) != 0)
        PANIC;
    return 0;
}

int UUTryAcquireMutexOS(UUMUTEX *uumutex) {
    return pthread_mutex_trylock(&uumutex->mutex) == 0;
}

int UUReleaseMutexOS(UUMUTEX *uumutex) {
    if (pthread_mutex_unlock(&uumutex->mutex) != 0)
        PANIC;
    return 0;
}

void MapGuardian(BOOL allowFailure) {
    char *errorMsg;

    guardian = shmat(guardianSharedMemoryID, NULL, 0);
    if (guardian == (void *)-1) {
        if ((debugLevel > 0) || (!allowFailure)) {
            Log("Unable to attach shared memory: shmID=%d, errno=%d, err=%s",
                guardianSharedMemoryID, errno, errorMsg = ErrorMessage(errno));
            FreeThenNull(errorMsg);
        }
        if (allowFailure) {
            guardian = NULL;
            return;
        }
        exit(1);
    }
}

int Guardian(char *guardianMap, int argc, char **argv) {
    pid_t child;
    pid_t someChild;
    BOOL childStopped = FALSE;
    BOOL childStoppedEver = FALSE;
    BOOL inGuardian = 0;
    BOOL seized = FALSE;
    int killSignal = SIGINT;
    int seizedCount = 0;
    int i;
    int result;
    int status, wstatus;
    int tmpInt, seizedCharge, seizedMainLocation, seizedRawLocation, seizedStopSocketsLocation;
    time_t sentTerm, sentKill, now, lastStarted, seizureTime = 0, firstNoticedChargeStopping = 0;
    DWORD endTicks;
    struct shmid_ds shmid_ds, shmid_ds_orig;
    int gotShmid0;
    int grantPid = 0;
    int startUid, startGid;
    int loginPortsFailed;
    int chargeStartsFailed;
    char sGuardianSharedMemoryID[64];
    char *newArgvs[10];
    char **newArgv;
    char *errorMsg;

    if (guardianMap == NULL) {
        guardianMap = getenv(GUARDIANMAPENVVAR);
    }

    if (guardianMap != NULL && (stricmp(guardianMap, "none") == 0)) {
        /* Explicitly no shared memory ID, run without a guardian */
        ResetGuardian(0);
        SaveIPC();
        if (optionLogThreadStartup) {
            Log("Thread starting: %s (without guardian).", guardianChargeName[GCMAIN]);
        }
        guardian->chargeStatus = chargeStarting;
        return IAmTheCharge;
    }

    if (guardianMap != NULL && (guardianSharedMemoryID = atoi(guardianMap))) {
        /* Guardian has started this child, "charge", process. */
        inCharge = TRUE;
        MapGuardian(0);
        if (optionLogThreadStartup) {
            Log("Thread starting: %s.", guardianChargeName[GCMAIN]);
        }
        guardian->chargeStatus = chargeStarting;
        return IAmTheCharge;
    }

    if (optionLogThreadStartup) {
        Log("Thread starting: guardian.");
    }

    result = atexit(GuardianAtExit);
    if (result < 0) {
        Log("atexit returned %d.", result);
    }

    /* Get any already existing shared memory ID */
    guardianSharedMemoryID = 0;
    LoadIPC(NULL, NULL, &guardianSharedMemoryID, &result);
    if (result != 0) {
        if (debugLevel > 0) {
            Log("LoadIPC returned %s.", errorMsg = ErrorMessage(result));
            FreeThenNull(errorMsg);
        }
    }
    if (guardianSharedMemoryID) {
        MapGuardian(1);
        /* Note: don't do anything with the guardian shared memory until we know we have the ipcFile
         * lock. It's contents may belong to another running process.*/
    }

    /* MapGuardian might have mapped-in a shared memory segment that is incompatible with this
     * version of EZproxy, or it may have attempted to map a shared memory segment ID that isn't
     * valid. If so, ignore that shared memory ID.
     */
    if (guardian && strncmp(guardian->version, GUARDIANVERSION, strlen(guardian->version)) != 0) {
        /* Detach the shared memory segment from the address space of this process */
        result = shmdt((void *)guardian);
        if (result < 0) {
            Log("shmdt returned %s.", errorMsg = ErrorMessage(GetLastError()));
            FreeThenNull(errorMsg);
        }
        guardianSharedMemoryID = 0;
        guardian = NULL;
    }

    if (guardian == NULL) {
        /* We need a new guardianSharedMemoryID, which is why we retry if we actually get 0 (seems
         * unlikely, but...) */
        gotShmid0 = 0;
        for (;;) {
            guardianSharedMemoryID = shmget(IPC_PRIVATE, sizeof(struct GUARDIAN), IPC_CREAT | 0600);
            if (guardianSharedMemoryID < 0) {
                Log("Unable to allocate shared memory: errno=%d, err=%s", errno,
                    errorMsg = ErrorMessage(errno));
                FreeThenNull(errorMsg);
                exit(1);
            }
            if (guardianSharedMemoryID)
                break;
            gotShmid0 = 1;
        }
        if (debugLevel > 0) {
            Log("New shared memory segment %d belongs to process %d.", guardianSharedMemoryID,
                getpid());
        }
        mySharedMemoryID = guardianSharedMemoryID;

        /* If we actually got something with ID 0, go ahead and IPC_RMID it now, but keep our new
         * guardianSharedMemoryID. */
        if (gotShmid0) {
            result = shmctl(0, IPC_RMID, &shmid_ds);
        }

        MapGuardian(0);
    } else {
        if (debugLevel > 0) {
            Log("Existing shared memory segment %d has been mapped into process %d.",
                guardianSharedMemoryID, getpid());
        }
    }

    if (optionAllowDebugger == 0) {
        DisableDebugger();
    }
    EstablishGuardianSignalHandlers();

    /* Get original shared memory permissions.  Guardian runs as the original user, but the charge
     * runs as a possibly other user. */
    result = shmctl(guardianSharedMemoryID, IPC_STAT, &shmid_ds_orig);
    if (result < 0) {
        Log("shmctl returned %s.", errorMsg = ErrorMessage(GetLastError()));
        FreeThenNull(errorMsg);
    }

    startUid = geteuid();
    startGid = getegid();

    SaveIPC();
    ResetGuardian(1);
    if ((debugLevel > 0) && (mySharedMemoryID != guardianSharedMemoryID)) {
        Log("Existing shared memory segment %d now belongs to process %d.", guardianSharedMemoryID,
            guardian->gpid);
    }
    mySharedMemoryID = guardianSharedMemoryID;

    chargeStartsFailed = 0;
    for (; !guardian->guardianStop;) {
        if (!inGuardian) {
            inGuardian = 1;
            /* At this point, this process is the only one running guardian. */
            if (optionLogThreadStartup) {
                Log("Guardian running in process %d.", guardian->gpid);
            }
        }
        CloseMsgFile();
        OpenMsgFile(); /* no need for a lock because the Guardian is only one thread. */

        UUtime(&lastStarted);

        memcpy(&shmid_ds, &shmid_ds_orig, sizeof(shmid_ds));
        /* If the charge is going to run under a another account, change permissions here */
        if (runGid)
            shmid_ds.shm_perm.gid = runGid;
        if (runUid)
            shmid_ds.shm_perm.uid = runUid;
        result = shmctl(guardianSharedMemoryID, IPC_SET, &shmid_ds);
        if (result < 0) {
            Log("shmctl returned %s.", errorMsg = ErrorMessage(GetLastError()));
            FreeThenNull(errorMsg);
        }

        ResetGuardian(1);

        child = fork();
        if (child < 0) {
            Log("fork returned %s.", errorMsg = ErrorMessage(GetLastError()));
            FreeThenNull(errorMsg);
        }

        if (child == 0) {
            sprintf(sGuardianSharedMemoryID, "%s=%d", GUARDIANMAPENVVAR, guardianSharedMemoryID);
            result = putenv(sGuardianSharedMemoryID);
            if (result != 0) {
                Log("putenv with '%s' returned %s.", sGuardianSharedMemoryID,
                    errorMsg = ErrorMessage(GetLastError()));
                FreeThenNull(errorMsg);
            }
            if (mallocCheck) {
                result = putenv(mallocCheck);
                if (result != 0) {
                    Log("putenv with '%s' returned %s.", mallocCheck,
                        errorMsg = ErrorMessage(GetLastError()));
                    FreeThenNull(errorMsg);
                }
            }
            newArgv = &newArgvs[0];
            *newArgv++ = argv[0];
            if (myChangedPath) {
                *newArgv++ = "-d";
                *newArgv++ = myChangedPath;
                result = chdir(myStartupPath);
                if (result != 0) {
                    Log("chdir with '%s' returned %s.", myStartupPath,
                        errorMsg = ErrorMessage(GetLastError()));
                    FreeThenNull(errorMsg);
                }
            }
            if (debugLevel) {
                if (!(*newArgv = malloc(64)))
                    PANIC;
                sprintf(*newArgv, "-D%d", debugLevel);
                newArgv++;
            }

            if (ttyDetach) {
                if (!(*newArgv = strdup("-ttyDetach")))
                    PANIC;
                newArgv++;
            }
            for (i = 0; i < argc; i++) {
                if (strncmp(argv[i], "-T", 2) == 0) {
                    /* Here we explicitly copy the "T" parameter. It's used as a tag on the command
                     * line so something like "ps -ef | grep TABC" can find the commands so tagged.
                     */
                    if (strlen(argv[i]) > 2) {
                        if (!(*newArgv = strdup(argv[i])))
                            PANIC;
                        newArgv++;
                    } else if (i + 1 < argc) {
                        if (!(*newArgv = strdup(argv[i])))
                            PANIC;
                        newArgv++;
                        i++;
                        if (!(*newArgv = strdup(argv[i])))
                            PANIC;
                        newArgv++;
                    }
                    // handle the environment
                } else if (strncmp(argv[i], "-E", 2) == 0) {
                    if (strlen(argv[i]) > 2) {
                        if (!(*newArgv = strdup(argv[i])))
                            PANIC;
                        newArgv++;
                    } else if (i + 1 < argc) {
                        if (!(*newArgv = strdup(argv[i])))
                            PANIC;
                        newArgv++;
                        i++;
                        if (!(*newArgv = strdup(argv[i])))
                            PANIC;
                        newArgv++;
                    }
                }
            }
            *newArgv++ = NULL;
            execv(myBinary, newArgvs);
            OpenMsgFile();
            Log("execv with '%s' returned %s.", argv[0], errorMsg = ErrorMessage(GetLastError()));
            FreeThenNull(errorMsg);
            PANIC;

            /* Keep the ReadConfig() runas happy that there are no dups */
            /* This is the old way before execl
            changedUid = changedGid = runUid = runGid = 0;
            inCharge = 1;
            return 0;
            */
        } else if (child > 0) {
            if (optionLogThreadStartup) {
                if (chargeStartsFailed > 0) {
                    Log(EZPROXYNAMEPROPER " launched in process %d, attempt %d.", child,
                        chargeStartsFailed + 1);
                } else {
                    Log(EZPROXYNAMEPROPER " launched in process %d.", child);
                }
            }
        } else {
            /* Log(EZPROXYNAMEPROPER " process has failed to start."); */
        }

        guardian->cpid = child;
        SaveIPC();
        sentTerm = sentKill = 0;

        /* Loop monitoring children. */
        tmpInt = seizedCharge = seizedMainLocation = seizedRawLocation = seizedStopSocketsLocation =
            -1;
        loginPortsFailed = 0;
        for (; child > 0;) {
            /* The following is a locking mechanism so that only one client has access to the
             * "guardian" shared memory object at once. */
            if (grantPid) {
                if (!ProcessAlive(grantPid) || guardian->serverGrantPid != grantPid) {
                    grantPid = guardian->serverGrantPid = 0;
                }
            }

            if (grantPid == 0 && (grantPid = guardian->clientRequestPid)) {
                if (ProcessAlive(grantPid)) {
                    guardian->serverGrantPid = grantPid;
                } else {
                    grantPid = 0;
                }
            }

            /* This will also reap any zombies we might have created along the way */
            wstatus = waitpid(-1, &status,
                              WNOHANG |
                                  WUNTRACED
#ifdef WCONTINUED
                                  /* Not all implementations support this */
                                  | WCONTINUED
#endif
            );
            if (restarting) { /* not used */
                restarting = FALSE;
                result = kill(child, killSignal);
                if (result < 0) {
                    Log("kill() returned %s", errorMsg = ErrorMessage(GetLastError()));
                    FreeThenNull(errorMsg);
                }
                UUtime(&sentTerm);
            }
            if (guardian->guardianStop) {
                guardian->chargeStatus = chargeStopping;
                guardian->chargeStop = 1;
            }
            if ((guardian->chargeStop) && (firstNoticedChargeStopping == 0)) {
                UUtime(&firstNoticedChargeStopping);
            }
            if (wstatus < 0) {
                someChild = 0;
                /* The value of the location pointed to by &status is undefined. */
                if (errno == ECHILD) {
                    if (child > 0) {
                        Log("This process has no children, but one was started with pid %d", child);
                    } else {
                        /* a child could not be created. */
                    }
                    guardian->cpid = 0;
                    break;
                }
                if (errno != EINTR) {
                    Log("The waitpid() failed with %s", errorMsg = ErrorMessage(GetLastError()));
                    FreeThenNull(errorMsg);
                    PANIC;
                }
                /* The function, waitpid(), was interrupted by a signal. */
            } else if (wstatus == 0) {
                someChild = 0;
                /* There is at least one child, but no child has a status to report at this time. */
                /* The value of the location pointed to by &status is undefined. */
            } else {
                someChild = wstatus;
                if (WIFEXITED(status)) {
                    if (guardian->chargeStop && (WEXITSTATUS(status) == 0) &&
                        (someChild == child)) {
                        if (optionLogThreadStartup) {
                            Log(EZPROXYNAMEPROPER " process %d exited.", child);
                        }
                    } else if ((WEXITSTATUS(status) != 0) && (someChild == child)) {
                        Log(EZPROXYNAMEPROPER " process %d exited abnormally, status=%d", child,
                            WEXITSTATUS(status));
                    } else {
                        Log("child %d exited, status=%d", someChild, WEXITSTATUS(status));
                    }
                    if (someChild == child) {
                        guardian->cpid = 0;
                        break;
                    }
                } else if (WIFSIGNALED(status)) {
                    if (someChild == child) {
                        Log(EZPROXYNAMEPROPER " process %d killed by signal %d", child,
                            WTERMSIG(status));
                        guardian->cpid = 0;
                        break;
                    } else {
                        Log("child %d killed by signal %d", someChild, WTERMSIG(status));
                    }
                } else if (WIFSTOPPED(status)) {
                    if (someChild == child) {
                        Log(EZPROXYNAMEPROPER " process %d stopped by signal %d", child,
                            WSTOPSIG(status));
                        childStopped = TRUE;
                        childStoppedEver = TRUE;
                    } else {
                        Log("child %d stopped by signal %d", someChild, WSTOPSIG(status));
                    }
#ifdef WIFCONTINUED /* Not all implementations support this */
                } else if (WIFCONTINUED(status)) {
                    if (someChild == child) {
                        Log(EZPROXYNAMEPROPER " process %d continued after having been stopped",
                            child);
                        childStopped = FALSE;
                    } else {
                        Log("child %d continued after having been stopped", someChild);
                    }
#endif
                } else {
                    /* Non-standard case -- may never happen */
                    if (someChild == child) {
                        Log(EZPROXYNAMEPROPER " process %d unexpected status 0x%x", child, status);
                    } else {
                        Log("child %d unexpected status 0x%x", someChild, status);
                    }
                }
            }

            UUtime(&now);

            if (sentTerm == 0) {
                seizureTime = ADJUST_TIME(seizureTime, now);
                tmpInt = CheckCharge();
                if (tmpInt >= 0) {
                    seizedCharge = tmpInt;
                } else {
                    SubSleep(1, 0);
                    continue; /* not seized, normal case */
                }
            }

            /* At least one of the child's threads has seized.  seizedCharge indicates
             * which of several threads has seized.*/

            /* Deal with possible clock changes mid-stream */
            sentTerm = ADJUST_TIME(sentTerm, now);
            sentKill = ADJUST_TIME(sentKill, now);

            if (sentTerm == 0) {
                seizedMainLocation = guardian->mainLocation;
                seizedRawLocation = guardian->rawLocation;
                seizedStopSocketsLocation = guardian->stopSocketsLocation;
                result = kill(child, killSignal);
                if (result < 0) {
                    Log("kill() returned %s", errorMsg = ErrorMessage(GetLastError()));
                    FreeThenNull(errorMsg);
                }
                sentTerm = now;
                SubSleep(1, 0);
                continue;
            }

            if (sentKill == 0) {
                if (difftime(now, sentTerm) < 5.0) {
                    SubSleep(1, 0);
                    continue;
                }
                sentKill = now;
            }

            if (difftime(now, sentKill) > 15.0) {
                break; /* We waited long enough for the process to die.  Exit the child monitor
                          loop. */
            }

            kill(child, SIGKILL);
            SubSleep(1, 0);
        }
        killSignal = SIGINT;

        UUtime(&now);
        endTicks = GetTickCount();

        if (guardian->cpid != 0) {
            Log("The " EZPROXYNAMEPROPER " process %d could not be stopped.  It will be abandoned.",
                child);
            child = 0;
            guardian->cpid = 0;
        }

        UUtime(&now);

        if (seizedCharge >= 0) {
            Log("Guardian terminated " EZPROXYNAMEPROPER
                " process because its %s thread's latency is greater than %f seconds (main "
                "location %d, raw location %d, stopSockets location %d).",
                guardianChargeName[seizedCharge], guardianChargeMaximumLatency[seizedCharge],
                seizedMainLocation, seizedRawLocation, seizedStopSocketsLocation);
            if (childStoppedEver) {
                Log("The " EZPROXYNAMEPROPER
                    " process had been stopped by a signal at least once.");
                if (!childStopped) {
                    Log("The " EZPROXYNAMEPROPER
                        " process was runnable at the time the charge seized.");
                }
#ifndef WIFCONTINUED
                Log("There is no way on this platform to detect that a stopped process has been "
                    "restarted except for inferring this from the fact that the process has "
                    "completed some work after it was stopped.");
#endif
            }
        }
        if ((seizedCharge >= 0) || optionLogThreadStartup) {
            seizedCount = 0;
            for (i = 0; i <= GCLAST; i++) {
                Log("The %s thread had%s seized.", guardianChargeName[i],
                    (seized = ChargeHasSeized(i, endTicks)) ? "" : " not");
                if (seized) {
                    seizedCount++;
                }
            }
            if (seizedCount > GCLAST) {
                Log("Because all " EZPROXYNAMEPROPER
                    " threads had seized, it's probable that the process in which they ran had "
                    "seized.");
            }
        }

        if (guardian->chargeStatus == chargeRunning) {
            /* Abnormal termination of the child AKA charge or it's being restarted. Try to
             * restart.*/
            chargeStartsFailed = 0;
        } else if (guardian->chargeStatus == chargeStopping) {
            /* Normal termination of the child AKA charge. */
            guardian->guardianStop = TRUE;
            chargeStartsFailed = 0;
        } else {
            /* Abnormal termination try to restart. */
            if (guardian->chargeStatus == chargeBeingStarted) {
                Log(EZPROXYNAMEPROPER " process has failed to start.");
            } else if (seizedCharge < 0) {
                Log(EZPROXYNAMEPROPER " %s thread has failed.", guardianChargeName[GCMAIN]);
            }
            chargeStartsFailed++;
        }

        if (guardian->portOpenFailure) {
            loginPortsFailed++;
            SubSleep(5, 0);
        } else {
            loginPortsFailed = 0;
        }

        if (guardian->portOpenFailure) {
            Log(EZPROXYNAMEPROPER " can't listen on the configured port.");
        }
        if ((guardian->portOpenFailure & GCPRIVPORTFAILED) || (chargeStartsFailed > 6) ||
            (loginPortsFailed > 6)) {
            guardian->guardianStop = TRUE;
            Log("Guardian is unable to start " EZPROXYNAMEPROPER ".");
            break;
        }

        if (((seizedCharge >= 0) || (chargeStartsFailed > 5)) && (loginPortsFailed == 0) &&
            (debugLevel > 2)) {
            killSignal =
                SIGABRT; /* Maybe get a core dump the next time the Charge needs to be killed. */
        }

        SaveIPC();
    }

    if (optionLogThreadStartup) {
        Log("Thread exiting: guardian.");
    }

    return IAmTheGuardian;
}

/* The thread safe Facade for getgrgid */
int getgrgidFacade(gid_t gid,
                   struct group *grp,
                   char *buffer,
                   size_t bufferMax,
                   struct group **grpp) {
#if defined(HAVE_GETGRGID_R) || defined(HAVE_GETGRGID)
    int rc;
#endif

#ifdef HAVE_GETGRGID_R
    SetErrno(0);
    rc = getgrgid_r(gid, grp, buffer, bufferMax, grpp);
    return rc;
#elif defined(HAVE_GETGRGID)
    SetErrno(0);
    TEMP_FAILURE_RETRY_FILE(*grpp = getgrgid(gid));
    rc = errno;
    return rc;
#else
    return EPERM;
#endif
}

/* The thread safe Facade for getpwuid */
int getpwuidFacade(uid_t uid,
                   struct passwd *pwd,
                   char *buffer,
                   size_t bufferMax,
                   struct passwd **pwdp) {
#if defined(HAVE_GETPWUID_R) || defined(HAVE_GETPWUID)
    int rc;
#endif

#ifdef HAVE_GETPWUID_R
    SetErrno(0);
    rc = getpwuid_r(uid, pwd, buffer, bufferMax, pwdp);
    return rc;
#elif defined(HAVE_GETPWUID)
    SetErrno(0);
    TEMP_FAILURE_RETRY_FILE(*pwdp = getpwuid(uid));
    rc = errno;
    return rc;
#else
    return EPERM;
#endif
}

static uid_t saveUid = -1;
static gid_t saveGid = -1;
static eUidGidActive = 0;
static BOOL haveChanged = 0;

void SetEUidGid(void) {
    if (haveChanged) {
        return;
    }

    if (!eUidGidActive) {
        saveGid = getegid();
        saveUid = geteuid();
        if (changedGid) {
            if (setegid(runGid))
                PANIC;
        }
        if (changedUid) {
            if (seteuid(runUid))
                PANIC;
        }
        eUidGidActive = 1;
    }
}

void ResetEUidGid(void) {
    if (haveChanged) {
        return;
    }

    if (eUidGidActive) {
        if (changedGid) {
            if (setegid(saveGid))
                PANIC;
        }
        if (changedUid) {
            if (seteuid(saveUid))
                PANIC;
        }
        eUidGidActive = 0;
    }
}

void SetUidGid(void) {
    static BOOL haveChanged = 0;
    int rc;
    struct group grp;
    struct group *grpp;
    struct passwd pwd;
    struct passwd *pwdp;
    char *buffer;
    char *errorMsg;
    int bufferMax;
    int bufferMaxGR;
    int bufferMaxPW;
    uid_t actualUID;
#if defined(HAVE_GETGROUPS)
    int nGroups, g;
    long nGroupsMax;
    GETGROUPS_T *groups;
#endif
    GETGROUPS_T actualGID;

    if (haveChanged) {
        return;
    }

    haveChanged = 1;

    if (changedGid || changedUid) {
        bufferMaxGR = sysconf(_SC_GETGR_R_SIZE_MAX) + 1;
        if (bufferMaxGR <= 0) {
            Log("sysconf(_SC_GETGR_R_SIZE_MAX returned %s.",
                errorMsg = ErrorMessage(GetLastError()));
            FreeThenNull(errorMsg);
        }
        bufferMaxPW = sysconf(_SC_GETPW_R_SIZE_MAX) + 1;
        if (bufferMaxPW <= 0) {
            Log("sysconf(_SC_GETPW_R_SIZE_MAX) returned %s.",
                errorMsg = ErrorMessage(GetLastError()));
            FreeThenNull(errorMsg);
        }
        bufferMax = bufferMaxGR > bufferMaxPW ? bufferMaxGR : bufferMaxPW;
        if (!(buffer = (char *)malloc(bufferMax)))
            PANIC;

        if (changedGid) {
            if (setgid(runGid)) {
                Log("Unable to change group per RunAs in " EZPROXYCFG ", aborting");
                exit(1);
            }
            /* Changed 9/7/05 to remove supplementary groups when group specified in RunAs */
            /* setgroups(0, NULL); NOT POSIX */
            actualGID = getegid();

            rc = getgrgidFacade(actualGID, &grp, buffer, bufferMax, &grpp);
            if (rc || grpp == NULL || grpp->gr_name == NULL) {
                Log("Running in group: %" PRIuMAX " (%s)", (intmax_t)actualGID,
                    errorMsg = ErrorMessage(rc));
                FreeThenNull(errorMsg);
            } else {
                Log("Running in group: %s", grpp->gr_name);
            }

#if defined(HAVE_GETGROUPS)
            nGroupsMax = sysconf(_SC_NGROUPS_MAX) + 1;
            if (nGroupsMax <= 0) {
                Log("sysconf(_SC_NGROUPS_MAX) returned %s.",
                    errorMsg = ErrorMessage(GetLastError()));
                FreeThenNull(errorMsg);
                nGroupsMax = 0;
            }
            if (!(groups = (gid_t *)malloc(nGroupsMax * sizeof(gid_t))))
                PANIC;

            nGroups = getgroups(nGroupsMax, groups);
            if (nGroupsMax < 0) {
                Log("getgroups returned %s.", errorMsg = ErrorMessage(GetLastError()));
                FreeThenNull(errorMsg);
                nGroupsMax = 0;
            }
            for (g = 0; g < nGroups; g++) {
                rc = getgrgidFacade(groups[g], &grp, buffer, bufferMax, &grpp);
                if (rc || grpp == NULL || grpp->gr_name == NULL) {
                    Log("Running in group: %" PRIuMAX " (%s)", (intmax_t)(groups[g]),
                        errorMsg = ErrorMessage(rc));
                    FreeThenNull(errorMsg);
                } else {
                    Log("Running in group: %s", grpp->gr_name);
                }
            }
            FreeThenNull(groups);
#endif
        }

        if (changedUid) {
            if (setuid(runUid)) {
                Log("Unable to change user per RunAs in " EZPROXYCFG ", aborting");
                exit(1);
            }
            actualUID = geteuid();

            rc = getpwuidFacade(actualUID, &pwd, buffer, bufferMax, &pwdp);
            if (rc) {
                Log("Running as user: %" PRIuMAX " (%s)", (intmax_t)actualUID,
                    errorMsg = ErrorMessage(rc));
                FreeThenNull(errorMsg);
            } else {
                Log("Running as user: %s", pwdp->pw_name);
            }
        }

        FreeThenNull(buffer);
    }
}

void RequireRoot(void) {
    if (geteuid() != 0) {
        fprintf(stderr, "You must be logged in as root to use this option.\n");
        exit(1);
    }
}

char *startupFile = "r|init.d/ezproxy";
char *tmpStartupFile = NULL;
char *startupLinks[] = {"r|rc0.d/K00ezproxy",
                        "o|rc2.d/S99ezproxy",
                        "r|rc3.d/S99ezproxy",
                        "o|rc4.d/S99ezproxy",
                        "o|rc5.d/S99ezproxy",
                        "o|rc6.d/K00ezproxy",
                        NULL};

char *RelativeName(char *fileName, char *relativeTo) {
    static char rn[MAX_PATH];
    char *r1, *r2;
    char *s1, *s2;

    for (r1 = fileName, r2 = relativeTo; *r1 && *r2 && *r1 == *r2; r1++, r2++)
        ;
    s1 = strchr(r1, '/');
    s2 = strchr(r2, '/');
    if (s1 && s2 && strchr(s1 + 1, '/') == NULL && strchr(s2 + 1, '/') == NULL) {
        sprintf(rn, "../%s", r1);
    } else {
        strcpy(rn, fileName);
    }
    return rn;
}

void FixupStartupPath(char **file) {
    char *partDir = NULL;
    char *partFile = NULL; /* not made by malloc, do not free! */
    char dirName[MAX_PATH];
    char **prefix;
    char *prefixes[] = {"/etc/rc.d", "/etc", "/sbin", NULL};
    struct stat statBuf;
    BOOL optional = 1;

    if (file == NULL || *file == NULL)
        goto Done;

    if (*(*file + 1) == '|') {
        optional = **file != 'r';
        if (!(partDir = strdup(*file + 2)))
            PANIC;
    } else {
        if (!(partDir = strdup(*file)))
            PANIC;
    }

    partFile = strrchr(partDir, '/');
    if (partFile == NULL)
        goto Done;

    *partFile++ = 0;

    for (prefix = prefixes; *prefix; prefix++) {
        sprintf(dirName, "%s/%s", *prefix, partDir);
        if (stat(dirName, &statBuf) != 0)
            continue;
        if (S_ISLNK(statBuf.st_mode) || !S_ISDIR(statBuf.st_mode))
            continue;
        break;
    }

    if (*prefix == NULL) {
        if (optional) {
            if (!(*file = strdup("")))
                PANIC;
            goto Done;
        }
        fprintf(stderr, "Unable to find correct directory for %s.\n", *file);
        exit(1);
    }

    /* This +2 is for the slash and final null */

    if (!(*file = malloc(strlen(dirName) + strlen(partFile) + 2)))
        PANIC;
    sprintf(*file, "%s/%s", dirName, partFile);

Done:
    FreeThenNull(partDir);
    /* do not free partFile! */
}

void FixupStartupPaths(void) {
    BOOL fixed = 0;

    char **link;

    if (fixed)
        return;

    FixupStartupPath(&startupFile);
    for (link = startupLinks; *link; link++)
        FixupStartupPath(link);

    if (!(tmpStartupFile = malloc(strlen(startupFile) + 10)))
        PANIC;
    sprintf(tmpStartupFile, "%s.new", startupFile);

    fixed = 1;
}

void StartupInstall(void) {
    FILE *oldFile = NULL, *newFile = NULL;
    char line[256];
    char **link;
    int status;
    int errors = 0;
    char oldLink[MAX_PATH];
    char *relativeName;
    char *myFilename;
    BOOL alreadyInstalled = 0;
    char *str;

    RequireRoot();
    FixupStartupPaths();

    myFilename = strrchr(myBinary, '/');
    if (myFilename == NULL)
        myFilename = myBinary;
    else
        myFilename++;

    oldFile = fopen(startupFile, "r");
    unlink(tmpStartupFile);
    umask(0077);
    newFile = fopen(tmpStartupFile, "w");
    if (newFile == NULL) {
        fprintf(stderr, "Unable to create %s (errno=%d).\n", tmpStartupFile, errno);
        exit(1);
    }

    if (oldFile == NULL) {
        fprintf(newFile,
                "\
#!/bin/sh\n\
\n\
#\n\
# chkconfig: 345 99 00\n\
# description: Starts and stop the EZproxy daemon\n\
#\n\
# To run EZproxy as non-root, change the line(s) that end $* to be like:\n\
#    su - someuser -c \"%s/%s $*\"\n\
#\n\
\n\
if [ -z \"$1\" ]\n\
then\n\
  echo \"Usage: $0 {start|stop|restart|bounce|status}\"\n\
  exit 1\n\
fi\n\
\n\
%s/%s $*\n",
                myStartupPath, myFilename, myStartupPath, myFilename);
        printf("Created startup file %s\n", startupFile);
    } else {
        while (fgets(line, sizeof(line), oldFile)) {
            Trim(line, TRIM_LEAD | TRIM_TRAIL);
            fprintf(newFile, "%s\n", line);
            if (line[0] == 0 || line[0] == '#')
                continue;
            /* check to see if this path part of a command to start EZproxy */
            for (str = strtok(line, " \t\""); alreadyInstalled == 0 && str;
                 str = strtok(NULL, " \t\"")) {
                if (strncmp(str, myStartupPath, strlen(myStartupPath)) != 0)
                    continue;
                str += strlen(myStartupPath);
                if (*str != '/')
                    continue;
                if (strchr(str + 1, '/') == NULL)
                    alreadyInstalled = 1;
            }
        }
        fclose(oldFile);
        oldFile = NULL;
        if (alreadyInstalled == 0) {
            fprintf(newFile, "%s/%s $*\n", myStartupPath, myFilename);
            printf("Updated startup file %s\n", startupFile);
        } else {
            printf("Existing startup file %s OK\n", startupFile);
        }
    }

    fclose(newFile);
    unlink(startupFile);
    rename(tmpStartupFile, startupFile);
    chown(startupFile, 0, -1);
    /* umask should have insured this, but we'll just be a little paranoid */
    chmod(startupFile, 0700);

    for (link = startupLinks; *link; link++) {
        /* skip "optional" directories that weren't found */
        if (**link == 0)
            continue;
        relativeName = RelativeName(startupFile, *link);
        memset(oldLink, 0, sizeof(oldLink));
        readlink(*link, oldLink, sizeof(oldLink) - 1);
        if (strcmp(oldLink, relativeName) == 0) {
            printf("Existing symbolic link %s OK\n", *link);
            continue;
        }
        unlink(*link);
        status = symlink(relativeName, *link);
        if (status) {
            fprintf(stderr, "Unable to create symbolic link %s: %d.\n", *link, errno);
            errors = 1;
        } else {
            printf("Created symbolic link %s\n", *link);
        }
    }
    if (errors) {
        fprintf(stderr,
                "Error(s) occurred while installing startup script; startup may not occur.\n");
    }
}

void StartupRemove(void) {
    char **link;
    char yes[32];
    char line[256];
    int lines = 0;
    FILE *oldFile;
    BOOL anyToDelete = 0;

    RequireRoot();
    FixupStartupPaths();

    oldFile = fopen(startupFile, "r");
    if (oldFile) {
        anyToDelete = 1;
        while (fgets(line, sizeof(line), oldFile)) {
            Trim(line, TRIM_LEAD | TRIM_TRAIL);
            if (line[0] == 0 || line[0] == '#' || strchr(line, '/') == NULL)
                continue;
            lines++;
        }
        fclose(oldFile);
    }

    for (link = startupLinks; *link; link++) {
        /* skip "optional" directories that weren't found */
        if (**link == 0)
            continue;

        if (FileExists(*link)) {
            anyToDelete = 1;
            break;
        }
    }

    if (anyToDelete == 0) {
        printf("There are no " EZPROXYNAMEPROPER " startup files to delete.\n");
        exit(0);
    }

    if (lines > 1) {
        printf(
            "\n\
If you are running multiple instances of " EZPROXYNAMEPROPER
            " and want to remove just this\n\
instance, you should edit:\n\
     %s\n\
by hand and remove the line that refers to just this instance.\n\n",
            startupFile);
    }

    printf("Please confirm that you want to delete all " EZPROXYNAMEPROPER
           " startup files\nby typing yes: ");
    if (fgets(yes, sizeof(yes), stdin) == NULL)
        exit(0);
    Trim(yes, TRIM_LEAD | TRIM_TRAIL);
    if (stricmp(yes, "yes") != 0)
        exit(0);

    unlink(startupFile);
    for (link = startupLinks; *link; link++) {
        /* skip "optional" directories that weren't found */
        if (**link == 0)
            continue;

        unlink(*link);
    }

    printf("All " EZPROXYNAMEPROPER " startup files have been removed.\n");

    exit(0);
}

UUTHREADID UUThreadId(void) {
    return pthread_self();
}

// Provide a functionally equivalent version of the Windows GetTickCount() function which returns
// a monotonic time in milliseconds.  This is set up intentionally to wrap to mimic the behavior of
// its windows counterpart to ensure that (end - start) will wrap appropriately for checking
// intervals. DWORD is typedef around uint32_t in common.h.
DWORD GetTickCount(void) {
    struct timespec monotime;
    DWORD ticks;
    static const DWORD u1000 = 1000;

    if (clock_gettime(CLOCK_MONOTONIC, &monotime))
        PANIC;
    // tv_sec is a time_t which may be a uint64_t
    // Use assignment separate from multiplication to ensure we truncate value into a uint32_t
    // first.
    ticks = monotime.tv_sec;
    ticks *= u1000;
    return ticks;
}
