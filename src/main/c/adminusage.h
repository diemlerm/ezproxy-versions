#ifndef __ADMINUSAGE_H__
#define __ADMINUSAGE_H__

#include "common.h"

void AdminUsage(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINUSAGE_H__
