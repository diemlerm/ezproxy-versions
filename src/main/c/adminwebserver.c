/**
 * /public, /limited, and /loggedin URL to serve arbitary files to remote
 * users.
 */

#define __UUFILE__ "adminwebserver.c"

#include "common.h"
#include "uustring.h"
#include <sys/stat.h>

#include "adminwebserver.h"

BOOL MagicDirectory(char *s) {
    /* In Windows, multiple periods go back multiple directory levels,
       so disallow anything with periods then a slash or null
    */
    if (s == NULL || *s != '.')
        return 0;

    for (;; s++) {
        if (*s == 0 || *s == '/')
            return 1;
        if (*s != '.')
            return 0;
    }
}

void AdminWebServer(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    char fileName[MAX_PATH];
    char *p, *q, *e, *g;
    char mimeType[128];
    struct stat fileStat;
    char httpDate[HTTPDATELENGTH];
    time_t lastModified;
    BOOL loggedIn;
    int slashAllowed = 0;
    GROUPMASK gm = NULL, requiredGm = NULL;
    BOOL limited;
    BOOL wellKnown;
    BOOL shtml;

    limited = strnicmp(t->url, WSLIMITED, strlen(WSLIMITED)) == 0;
    loggedIn = strnicmp(t->url, WSLOGGEDIN, strlen(WSLOGGEDIN)) == 0;
    wellKnown = strnicmp(t->url, WSWELLKNOWN, strlen(WSWELLKNOWN)) == 0;

    /* WSPUBLIC doesn't have to authenticate
       WSLOGGEDIN will have been forced to login and reauthenticate to get here
       WSLIMITED requires a valid session if the user is remote
       WSWELLKNOWN doesn't have to authenticate
    */
    if (limited && URLIpAccess(t, NULL, NULL, NULL, NULL, NULL) == IPAREMOTE) {
        if (t->session) {
            if (AdminReloginIfRequired(t))
                goto Finished;
        } else {
            AdminRelogin(t);
            goto Finished;
        }
    }

    if ((p = strchr(t->url, '?'))) {
        *p = 0;
    }

    p = strchr(t->url + 1, '/');
    if (p == NULL) {
        goto NoWay;
    }

    *p = 0;
    slashAllowed = optionAllowWebSubdirectories || loggedIn || wellKnown;
    g = (loggedIn ? p + 1 : NULL);
    snprintf(fileName, sizeof(fileName), "%s%s%s", DOCSDIR, t->url + 1, DIRSEP);
    *p = '/';

    p++;

    if (MagicDirectory(p))
        goto NoWay;

    q = strchr(fileName, 0);
    e = fileName + sizeof(fileName) - 1;

    for (; *p;) {
        if (!(IsAlpha(*p) || IsDigit(*p) || *p == '.' || *p == '_' || *p == '-')) {
            BOOL denied;
            if (slashAllowed == 0 || *p != '/')
                goto NoWay;
            if (MagicDirectory(p + 1))
                goto NoWay;
            slashAllowed = optionAllowWebSubdirectories || wellKnown;
            if (loggedIn && requiredGm == NULL) {
                *p = 0;
                requiredGm = MakeGroupMask(NULL, g, 0, '=');
                *p = '/';
                denied = GroupMaskOverlap((t->session)->gm, requiredGm) == 0;
                if (denied) {
                    AdminLogup(t, TRUE, NULL, NULL, requiredGm, FALSE);
                    goto Finished;
                }
            }
        }
        if (q == e)
            goto NoWay;
        if (*p == '/') {
            strcpy(q, DIRSEP);
            q = strchr(q, 0);
            p++;
        } else {
            *q++ = *p++;
        }
    }

    *q++ = 0;

    if (CheckIfDevice(fileName)) {
        goto NoWay;
    }

    if (stat(fileName, &fileStat) < 0)
        goto NoWay;

    if ((fileStat.st_mode & S_IFREG) == 0)
        goto NoWay;

    if (!FileExists(fileName)) {
        goto NoWay;
    }

    // The EZproxy webserver historically defaults unknown MIME type to text/html.
    // For .well-known, do not provide a default as one is not expected or correct.
    if (wellKnown) {
        FindMIMEType(fileName, mimeType, sizeof(mimeType), "");
    } else {
        FindMIMEType(fileName, mimeType, sizeof(mimeType), "text/html");
    }

    // Although unintended, EZproxy has always triggered server-side processing for files
    // that end in .shtml (intended) and those that have MIME type text/html (unintended).
    // In case there are dependencies on this behavior, it is allowed to continue this way
    // except /.well-known which would never have a reasonable expectation of
    // EZproxy style server-side processing.
    shtml =
        (EndsIWith(fileName, ".shtml") != 0 || stricmp(mimeType, "text/html") == 0) && !wellKnown;

    lastModified = UUMAX(fileStat.st_mtime, lastModifiedMinimum);

    if (lastModified <= t->ifModifiedSince) {
        HTTPCode(t, 304, 1);
        goto Finished;
    }

    HTTPCode(t, 200, 0);
    StrCpy3(t->contentType, mimeType, sizeof(t->contentType));
    if (t->version) {
        // Suppress Last-Modified for server processed files since their content can change every
        // time.
        if (!shtml) {
            WriteLine(s, "Last-Modified: %s\r\n",
                      FormatHttpDate(httpDate, sizeof(httpDate),
                                     UUMAX(fileStat.st_mtime, lastModifiedMinimum)));
        }
        if (mimeType[0]) {
            WriteLine(s, "Content-Type: %s\r\n", mimeType);
        }
    }

    if (shtml) {
        if (t->version) {
            HeaderToBody(t);
        }
        SendEditedFile(t, NULL, fileName, 1, NULL, 0, NULL);
    } else {
        t->sendChunkingAllowed = 0;
        t->sendContentLengthSent = 1;
        t->sendZipAllowed = 0;
        if (t->version) {
            WriteLine(s, "Content-Length: %d\r\n", fileStat.st_size);
            HeaderToBody(t);
        }
        SendFile(t, fileName, SFREPORTIFMISSING);
    }

    goto Finished;

NoWay:
    HTTPCode(t, 404, 1);

Finished:
    GroupMaskFree(&gm);
    GroupMaskFree(&requiredGm);
}
