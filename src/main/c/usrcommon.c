/**
 * Common "user authentication"
 *
 * This module allows common conditions and actions to be used separate from
 * a specific user authentication method.
 *
 * This authentication method supports common conditions and actions.
 */

#define __UUFILE__ "usrcommon.c"

#include "common.h"
#include "uustring.h"

#include "usr.h"
#include "usrcommon.h"

enum FINDUSERRESULT FindUserCommon(struct TRANSLATE *t,
                                   struct USERINFO *uip,
                                   char *file,
                                   int f,
                                   struct FILEREADLINEBUFFER *frb,
                                   struct sockaddr_storage *pInterface) {
    uip->result = resultInvalid;
    uip->flags = USRUSERMAYBENULL;
    return UsrHandler(t, "Common", NULL, NULL, uip, file, f, frb, pInterface, NULL);
}
