#define __UUFILE__ "intrusion.c"

#include "common.h"
#include "wskeylicense.h"
#include "wskey.h"
#include "adminaudit.h"
#include "intrusion.h"
#include "intrusiontxt.h"
#include "obscure.h"
#include <jansson.h>
#include <jwt.h>
#include <curl/curl.h>

char *intrusionVersion = NULL;
BOOL intrusionEnforce = 0;
BOOL intrusionEvade = 0;
BOOL intrusionReject = 0;
char *intrusionProxySslHost = NULL;
PORT intrusionProxySslPort = 0;
char *intrusionProxySslAuth = NULL;
char *intrusionPem = NULL;

struct IntrusionIP {
    char *ip;
    time_t expires;
    time_t accessed;
    UUMUTEX mutex;
    enum IntrusionIPStatus status;
};

char *baseUrl = NULL;  // "https://gw.usefulutilities.com/psi.php";
char *apiKey = NULL;   // "B7A0629516F0430287A2DD734D51E044";
BOOL warnInvalidConfig = 0;

int UUAvlTreeCmpIntrusionIPs(const void *v1, const void *v2) {
    struct IntrusionIP *i1 = (struct IntrusionIP *)v1;
    struct IntrusionIP *i2 = (struct IntrusionIP *)v2;

    return (strcmp(i1->ip, i2->ip));
}

char *getIntrusionPem() {
    return intrusionPem;
}

static CURLcode IntrusionSslCtx(CURL *curl, void *sslCtx, void *parm) {
    CURLcode rv = CURLE_ABORTED_BY_CALLBACK;
    BIO *cbio = NULL;
    X509_STORE *cts = NULL;
    int i;
    STACK_OF(X509_INFO) *inf = NULL;

    if (intrusionPem == NULL) {
        return rv;
    }

    if (!(cbio = BIO_new_mem_buf(intrusionPem, -1)))
        PANIC;
    if (!(cts = SSL_CTX_get_cert_store((SSL_CTX *)sslCtx)))
        PANIC;

    if (!(inf = PEM_X509_INFO_read_bio(cbio, NULL, NULL, NULL)))
        PANIC;

    for (i = 0; i < sk_X509_INFO_num(inf); i++) {
        X509_INFO *itmp = sk_X509_INFO_value(inf, i);
        if (itmp->x509) {
            X509_STORE_add_cert(cts, itmp->x509);
        }
        if (itmp->crl) {
            X509_STORE_add_crl(cts, itmp->crl);
        }
    }

    sk_X509_INFO_pop_free(inf, X509_INFO_free);
    BIO_free(cbio);

    rv = CURLE_OK;
    return rv;
}

BOOL IntrusionInvalidConfig() {
    return baseUrl == NULL || apiKey == NULL;
}

BOOL IntrusionReadConfig() {
    FILE *ca = NULL;
    char line[256];
    int lineNo = 0;
    char *content;
    size_t len;
    char *str = NULL;
    char *token = NULL;
    char *defaultContent = NULL;
    char *fileContent = NULL;

    warnInvalidConfig = 1;

    defaultContent = (char *)UnobscureBinary(DefaultIntrusionTxt(), &len);
    if (defaultContent == NULL) {
        Log("Intrusion default content failed?");
        goto Cleanup;
    }

    if (!(defaultContent = realloc(defaultContent, len + 1)))
        PANIC;
    *(defaultContent + len) = 0;
    Trim(defaultContent, TRIM_CRTOLF);

    content = defaultContent;

    if (FileExists(INTRUSIONTXT)) {
        fileContent = (char *)UnobscureFile(INTRUSIONTXT, &len);

        if (fileContent) {
            if (!(fileContent = realloc(fileContent, len + 1)))
                PANIC;
            *(fileContent + len) = 0;
            Trim(fileContent, TRIM_CRTOLF);

            if (strcmp(defaultContent, fileContent) < 0) {
                content = fileContent;
            }
        }
    }

    FreeThenNull(baseUrl);
    FreeThenNull(apiKey);

    for (lineNo = 0, str = NULL, token = StrTok_R(content, "\n", &str); token;
         lineNo++, token = StrTok_R(NULL, "\n", &str)) {
        if (lineNo == 0) {
            intrusionVersion = strdup(token);
        } else if (lineNo == 1) {
            if (!(baseUrl = strdup(token)))
                PANIC;
        } else if (lineNo == 2) {
            if (!(apiKey = strdup(token)))
                PANIC;
        } else {
            *AtEnd(token) = '\n';
            if (!(intrusionPem = strdup(token)))
                PANIC;
            break;
        }
    }

Cleanup:
    if (intrusionPem != NULL) {
        Log("Intrusion API configuration %s loaded", intrusionVersion);
    } else {
        Log("Intrusion disabled, " INTRUSIONTXT " invalid or missing");
        FreeThenNull(baseUrl);
        FreeThenNull(apiKey);
    }

    FreeThenNull(defaultContent);
    FreeThenNull(fileContent);

    return intrusionPem != NULL;
}

enum IntrusionIPStatus IntrusionIPStatus(struct sockaddr_storage *inIp, BOOL audit) {
    enum IntrusionIPStatus status = INTRUSION_IP_NET_FAILURE;
    char *url = NULL;
    char *query = "?IPAddress=";
    CURL *curl = NULL;
    CURLcode res;
    struct CURLMEMORYBUFFER cmb;
    struct curl_slist *headers = NULL;
    char ip[INET6_ADDRSTRLEN];
    char *escapedIp = NULL;
    char header[256];
    int result;
    long httpStatus;
    const struct WSKEYLICENSE *license = WskeyActiveLicense();
    static BOOL warnUnlicensed = 1;
    struct IntrusionIP ii, *p;
    time_t now;
    BOOL acquired = 0;
    jwt_t *jwt = NULL;
    time_t expires = 0;
    char *events = NULL;
    json_t *json = NULL;
    json_error_t json_error;
    struct TRANSLATE t;
    char error[256];
    struct tm tm;
    int i;

    memset(&t, 0, sizeof(t));
    memcpy(&t.sin_addr, inIp, sizeof(t.sin_addr));

    cmb.memory = NULL;
    cmb.size = 0;

    if (IntrusionInvalidConfig()) {
        if (warnInvalidConfig) {
            Log("Invalid " INTRUSIONTXT);
            warnInvalidConfig = 0;
        }
        goto Finished;
    }

    if (license == NULL) {
        if (warnUnlicensed) {
            Log("Intrusion unavailable in unlicensed beta copies");
            warnUnlicensed = 0;
        }
        goto Finished;
    }

    if (!licenseValid) {
        goto Finished;
    }

    if (!PublicAddress(inIp)) {
        status = INTRUSION_IP_PRIVATE;
        goto Finished;
    }

    for (i = 0; i < cWhitelistIPs; i++) {
        if (compare_ip(inIp, &whitelistIPs[i].start) >= 0 &&
            compare_ip(&whitelistIPs[i].stop, inIp) >= 0) {
            status = INTRUSION_IP_WHITELISTED;
            goto Finished;
        }
    }

    ipToStr(inIp, ip, sizeof(ip));
    UUtime(&now);

    RWLAcquireRead(&rwlIntrusionIPs);
    ii.ip = ip;
    p = UUAvlTreeFind(&avlIntrusionIPs, &ii);
    RWLReleaseRead(&rwlIntrusionIPs);
    if (p == NULL) {
        RWLAcquireWrite(&rwlIntrusionIPs);
        p = UUAvlTreeFind(&avlIntrusionIPs, &ii);
        if (p == NULL) {
            if (!(p = calloc(1, sizeof(*p))))
                PANIC;
            if (!(p->ip = strdup(ip)))
                PANIC;
            p->expires = 0;
            UUInitMutex(&p->mutex, p->ip);
            UUAvlTreeInsert(&avlIntrusionIPs, p);
        }
        RWLReleaseWrite(&rwlIntrusionIPs);
    }

    UUAcquireMutex(&p->mutex);
    p->accessed = now;
    acquired = 1;
    if (p->expires > now) {
        status = p->status;
        goto Finished;
    }

    if (!(curl = curl_easy_init()))
        PANIC;
    escapedIp = curl_easy_escape(curl, ip, 0);
    if (!(url = malloc(strlen(baseUrl) + strlen(query) + strlen(escapedIp) + 1)))
        PANIC;
    sprintf(url, "%s%s%s", baseUrl, query, escapedIp);
    curl_free(escapedIp);

    //  if ((res = curl_easy_setopt(curl, CURLOPT_CAINFO, "intrusion.pem")) != CURLE_OK) PANIC;
    curl_easy_setopt(curl, CURLOPT_CAINFO, NULL);
    curl_easy_setopt(curl, CURLOPT_CAPATH, NULL);
    curl_easy_setopt(curl, CURLOPT_SSL_CTX_FUNCTION, IntrusionSslCtx);
    curl_easy_setopt(curl, CURLOPT_URL, url);
    if (intrusionProxySslHost != NULL && intrusionProxySslPort != 0) {
        curl_easy_setopt(curl, CURLOPT_PROXY, intrusionProxySslHost);
        curl_easy_setopt(curl, CURLOPT_PROXYPORT, intrusionProxySslPort);
        if (intrusionProxySslAuth) {
            char *userpwd = calloc(1, strlen(intrusionProxySslAuth) + 1);
            Decode64(userpwd, intrusionProxySslAuth);
            curl_easy_setopt(curl, CURLOPT_PROXYAUTH, CURLAUTH_ANY);
            curl_easy_setopt(curl, CURLOPT_PROXYUSERPWD, userpwd);
            memset(userpwd, 0, strlen(userpwd));
        }
    }
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CurlMemoryBufferCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&cmb);
    sprintf(header, "X-Api-Key: %s", apiKey);
    if (!(headers = curl_slist_append(headers, header)))
        PANIC;
    sprintf(header, "X-WSKey: %s", license->key);
    if (!(headers = curl_slist_append(headers, header)))
        PANIC;
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    res = curl_easy_perform(curl);

    if (res != CURLE_OK) {
        sprintf(error, "curl failure %d", res);
        p->expires = now + 60;
        status = INTRUSION_IP_NET_FAILURE;
        if (audit) {
            AuditEvent(&t, AUDITINTRUSIONAPIERROR, NULL, NULL, error, NULL);
        }
    } else {
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpStatus);
        if (httpStatus == 200) {
            // Quotes were not in the original API, but may appear now.  If so, remove them.
            if (cmb.memory[0] == '"') {
                StrCpyOverlap(cmb.memory, cmb.memory + 1);
                if (strlen(cmb.memory) > 0 && cmb.memory[strlen(cmb.memory) - 1] == '"') {
                    cmb.memory[strlen(cmb.memory) - 1] = 0;
                }
            }
            result = jwt_decode(&jwt, cmb.memory, NULL, 0);

            if (result == 0) {
                long expires = jwt_get_grant_int(jwt, "exp");
                if (expires <= 0) {
                    expires = jwt_get_grant_int(jwt, "Exp");
                }
                if (expires <= 0) {
                    expires = now + 3600;
                }
                p->expires = expires;
                UUlocaltime_r(&p->expires, &tm);
                strftime(error, sizeof(error), "expires %Y-%m-%d %H:%M:%S", &tm);

                events = jwt_get_grants_json(jwt, "events");
                if (events == NULL) {
                    events = jwt_get_grants_json(jwt, "Events");
                }

                json = json_loads(events, 0, &json_error);
                if (!json) {
                    status = INTRUSION_IP_NET_FAILURE;
                    if (audit) {
                        char *badResponse = jwt_dump_str(jwt, 0);
                        Trim(badResponse, TRIM_CTRL);
                        snprintf(AtEnd(error), sizeof(error) - strlen(error) - 1,
                                 " bad response: %s", badResponse ? badResponse : "null");
                        AuditEvent(&t, AUDITINTRUSIONAPIERROR, NULL, NULL, error, NULL);
                        FreeThenNull(badResponse);
                    }
                } else {
                    const char *state = NULL;
                    result = json_unpack(json, "{s:{s: s}}",
                                         "https://api.ip-intrusion.org/SETschema", "state", &state);
                    if (state == NULL) {
                        result =
                            json_unpack(json, "{s:{s: s}}",
                                        "https://api.ip-intrusion.org/SETschema", "State", &state);
                    }
                    if (state != NULL && stricmp(state, "Whitelisted") == 0) {
                        status = INTRUSION_IP_WHITELISTED;
                        if (audit) {
                            AuditEvent(&t, AUDITINTRUSIONAPIWHITELISTED, NULL, NULL, error, NULL);
                        }
                    } else if (state != NULL && stricmp(state, "BadIP") == 0) {
                        status = INTRUSION_IP_BAD_IP;
                        if (audit) {
                            AuditEvent(&t, AUDITINTRUSIONAPIBADIP, NULL, NULL, error, NULL);
                        }
                    } else {
                        status = INTRUSION_IP_INVALID_STATE;
                        if (audit) {
                            char *badResponse = jwt_dump_str(jwt, 0);
                            Trim(badResponse, TRIM_CTRL);
                            snprintf(AtEnd(error), sizeof(error) - strlen(error) - 1,
                                     " bad state: %s", badResponse ? badResponse : "null");
                            AuditEvent(&t, AUDITINTRUSIONAPIERROR, NULL, NULL, error, NULL);
                            FreeThenNull(badResponse);
                        }
                    }
                }
            }
        } else if (httpStatus == 204) {
            p->expires = now + 3600;
            status = INTRUSION_IP_NONE;
            if (audit) {
                UUlocaltime_r(&p->expires, &tm);
                strftime(error, sizeof(error), "expires %Y-%m-%d %H:%M:%S", &tm);
                AuditEvent(&t, AUDITINTRUSIONAPINONE, NULL, NULL, error, NULL);
            }
        } else {
            status = INTRUSION_IP_NET_FAILURE;
            p->expires = now + 60;
            if (audit) {
                UUlocaltime_r(&p->expires, &tm);
                strftime(error, sizeof(error), "expires %Y-%m-%d %H:%M:%S", &tm);
                sprintf(AtEnd(error), " bad HTTP status %d", httpStatus);
                AuditEvent(&t, AUDITINTRUSIONAPIERROR, NULL, NULL, error, NULL);
            }
        }
    }

Finished:
    if (acquired) {
        if (status != p->status) {
            p->status = status;
        }
        UUReleaseMutex(&p->mutex);
        acquired = 0;
    }

    if (jwt) {
        jwt_free(jwt);
        jwt = NULL;
    }

    if (headers) {
        curl_slist_free_all(headers);
        headers = NULL;
    }

    if (curl) {
        curl_easy_cleanup(curl);
        curl = NULL;
    }

    FreeThenNull(events);
    FreeThenNull(cmb.memory);

    return status;
}
