/**
 * Microsoft Windows NT Domain user authentication (win32 only)
 *
 * This module implements Microsoft Windows NT Domain user authentication.
 * This feature is limited to win32 builds of EZproxy since it is a Microsoft-Windows
 * specific functionality.  This support is extremely limited.  For sites using
 * Windows 2000 or later, access to Active Directory using LDAP is the recommended
 * method for user authentication.
 *
 * This authentication method DOES NOT SUPPORT common conditions and actions.
 */

#define __UUFILE__ "usrdomain.c"

#include "common.h"

#ifdef WIN32
#include "uustring.h"
#include <lm.h>

#include "usr.h"
#include "usrdomain.h"

BOOL IsDomainGuest(HANDLE logonHandle) {
    PTOKEN_GROUPS pTokenGroups;
    DWORD tokenGroupsLength;
    SID_IDENTIFIER_AUTHORITY sia = SECURITY_NT_AUTHORITY;
    PSID pSid;
    BOOL result;
    unsigned int i;

    result = 0;

    pSid = NULL;
    pTokenGroups = NULL;

    if (GetTokenInformation(logonHandle, TokenGroups, NULL, 0, &tokenGroupsLength) == 0 &&
        GetLastError() != ERROR_INSUFFICIENT_BUFFER) {
        Log("GetTokenInformation 1 error %d", GetLastError());
        goto Failed;
    }

    if (!(pTokenGroups = (PTOKEN_GROUPS)malloc(tokenGroupsLength)))
        PANIC;

    if (GetTokenInformation(logonHandle, TokenGroups, pTokenGroups, tokenGroupsLength,
                            &tokenGroupsLength) == 0) {
        Log("GetTokenInformation 2 error %d", GetLastError());
        goto Failed;
    }

    if (AllocateAndInitializeSid(&sia, 2, SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_GUESTS, 0,
                                 0, 0, 0, 0, 0, &pSid) == 0) {
        Log("AllocateAndInitializeSid error %d", GetLastError());
        goto Failed;
    }

    for (i = 0; i < pTokenGroups->GroupCount; i++) {
        if (EqualSid(pSid, pTokenGroups->Groups[i].Sid)) {
            result = 1;
            break;
        }
    }

Failed:
    if (pSid) {
        FreeSid(pSid);
        pSid = NULL;
    }
    FreeThenNull(pTokenGroups);

    return result;
}

/* Returns 0 if valid, 1 if not */
int FindUserNtDomain(struct TRANSLATE *t,
                     struct USERINFO *uip,
                     char *domain,
                     char *newPass,
                     char *verifyPass,
                     char *formDomain) {
    int result;
    HANDLE logonHandle = NULL;
    int error;
    int rc;
    char *reason;
    WCHAR uDomain[DNLEN + 1];
    WCHAR uUser[UNLEN + 1];
    WCHAR uPass[PWLEN + 1];
    WCHAR uNewPass[PWLEN + 1];
    char errorUser[33];
    char *auth, *user, *pass;

    auth = uip->auth ? uip->auth : "";
    user = uip->user ? uip->user : "";
    pass = uip->pass ? uip->pass : "";

    if (formDomain != NULL && stricmp(domain, formDomain) != 0)
        return 1;

    if (verifyPass == NULL)
        verifyPass = "";

    result = (LogonUser(user, domain, pass, LOGON32_LOGON_NETWORK, LOGON32_PROVIDER_DEFAULT,
                        &logonHandle)
                  ? 0
                  : 1);
    if (result == 1) {
        error = GetLastError();
        StrCpy3(errorUser, user, sizeof(errorUser));
        Log("LogonUser for %s returned %d", errorUser, error);
        if ((!FileExists(WEXPIREDHTM)) || sNetUserChangePassword == NULL)
            return 1;
        reason = NULL;
        if (error != ERROR_PASSWORD_MUST_CHANGE && error != ERROR_PASSWORD_EXPIRED) {
            if (error != ERROR_LOGON_FAILURE || newPass == NULL)
                return 1;
            reason = "Your old password was incorrect.";
        } else {
            if (newPass == NULL) {
                reason = "You must change your password before you can proceed.";
            } else {
                if (strcmp(pass, newPass) == 0) {
                    reason = "You must select a new password.";
                } else if (strcmp(newPass, verifyPass) != 0) {
                    reason = "The two copies of your new password did not match.";
                } else {
                    rc = NERR_Success;
                    if (MultiByteToWideChar(CP_ACP, 0, domain, strlen(domain) + 1, (LPWSTR)&uDomain,
                                            sizeof(uDomain)) != 0 &&
                        MultiByteToWideChar(CP_ACP, 0, user, strlen(user) + 1, (LPWSTR)&uUser,
                                            sizeof(uUser)) != 0 &&
                        MultiByteToWideChar(CP_ACP, 0, pass, strlen(pass) + 1, (LPWSTR)&uPass,
                                            sizeof(uPass)) != 0 &&
                        MultiByteToWideChar(CP_ACP, 0, newPass, strlen(newPass) + 1,
                                            (LPWSTR)&uNewPass, sizeof(uNewPass)) != 0 &&
                        (rc = sNetUserChangePassword(uDomain, uUser, uPass, uNewPass)) ==
                            NERR_Success)
                        return 0;
                    else {
                        if (rc != NERR_Success)
                            Log("NetUserChangePassword returned %d", rc);
                        reason = "Your new password was invalid, please try again.";
                    }
                }
            }
        }
        HTMLHeader(t, htmlHeaderNoCache);
        SendEditedFile(t, uip, WEXPIREDHTM, 1, uip->url, 1, reason, user, domain, auth, NULL);
        return -1;
    } else {
        /* Check to see if this is the Guest account, and if so, deny the login */
        if (IsDomainGuest(logonHandle))
            result = 1;
        CloseHandle(logonHandle);
    }
    return result;
}
#endif
