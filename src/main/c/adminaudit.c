#define __UUFILE__ "adminaudit.c"

#include "common.h"
#include "uustring.h"

#ifndef WIN32
#include <dirent.h>
#endif

#include "adminaudit.h"
#include "security.h"

#define MAXAUDITFIELDS 20

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

struct AUDITFILES {
    struct AUDITFILES *next;
    char *name;
};

int auditFile = -1;

uint64_t auditEventsMask = 0;
int auditPurge = 0;

static BOOL AdminAuditValidFilename(char *fn, BOOL checkExists) {
    char testName[32];
    int i;

    if (strlen(fn) != 12 || stricmp(fn + 8, ".txt") != 0)
        return 0;

    for (i = 0; i < 8; i++)
        if (!IsDigit(*(fn + i)))
            return 0;

    if (!checkExists)
        return 1;

    sprintf(testName, "%s%s", AUDITDIR, fn);

    return FileExists(testName);
}

static struct AUDITFILES *AdminAuditGetAuditFiles(BOOL ascending) {
    struct AUDITFILES *auditFiles = NULL, *p, *n, *last;
    int match;
    char *fn;

#ifdef WIN32
    char searchName[64];
    WIN32_FIND_DATA fileData;
    HANDLE hSearch;
#else
    DIR *dir;
    struct dirent *dirent;
#endif

#ifdef WIN32
    sprintf(searchName, "%s*.*", AUDITDIR);
    hSearch = FindFirstFile(searchName, &fileData);
    if (hSearch == INVALID_HANDLE_VALUE)
        return NULL;
    do {
        fn = fileData.cFileName;
#else
    dir = opendir(AUDITDIR);
    if (dir == NULL) {
        return NULL;
    }

    while (dirent = readdir(dir)) {
        fn = dirent->d_name;
#endif
        if (strcmp(fn, ".") == 0 || strcmp(fn, "..") == 0)
            continue;
        if (!AdminAuditValidFilename(fn, 0))
            continue;

        for (match = -1, last = NULL, p = auditFiles; p; last = p, p = p->next) {
            match = stricmp(p->name, fn);
            if (ascending) {
                if (match >= 0)
                    break;
            } else {
                if (match <= 0)
                    break;
            }
        }
        if (match != 0) {
            if (!(n = calloc(1, sizeof(*n))))
                PANIC;
            if (!(n->name = strdup(fn)))
                PANIC;
            n->next = p;
            if (last)
                last->next = n;
            else
                auditFiles = n;
        }
#ifdef WIN32
    } while (FindNextFile(hSearch, &fileData));
    FindClose(hSearch);
#else
    }
    closedir(dir);
#endif

    return auditFiles;
}

static void AdminAuditFreeAuditFiles(struct AUDITFILES *auditFiles) {
    struct AUDITFILES *p, *n;

    for (p = auditFiles; p; p = n) {
        n = p->next;
        FreeThenNull(p->name);
        FreeThenNull(p);
    }
}

struct AUDITFILTER {
    char *field;
    char *cond;
    char *value;
};

static void AdminAuditShowTokens(struct UUSOCKET *s,
                                 struct ARRAYOFSTRINGS *tokens,
                                 int maxFields,
                                 int eventCol,
                                 int ipCol,
                                 int locationCol,
                                 int *trs,
                                 BOOL inHeader) {
    char *value;
    char *ipValue = "";
    char *eventValue = "";
    int i;
    char *bar;
    const char *td = inHeader ? "th" : "td";
    const char *tda = inHeader ? " scope='col'" : "";

    WriteLine(s, "<tr valign='top' id='trs%d'>", (*trs)++);

    for (i = 0; i < maxFields; i++) {
        value = ArrayOfStringsGetValue(tokens, i);
        if (value == NULL) {
            value = "";
        }
        if (i == eventCol) {
            eventValue = value;
        }
        if (i == ipCol) {
            ipValue = value;
        }
        if (inHeader && stricmp(value, "IP") == 0) {
            WriteLine(s, "<%s%s class='sortip'>", td, tda);
        } else {
            WriteLine(s, "<%s%s>", td, tda);
        }
        Trim(value, TRIM_TRAIL);
        if (*value == 0) {
            UUSendZ(s, "&nbsp;");
        } else {
            if (inHeader) {
                SendHTMLEncoded(s, value);
            } else if (i == locationCol && ipValue && *ipValue) {
                UUSendZ(s, "<a href='/ip?details=");
                SendUrlEncoded(s, ipValue);
                UUSendZ(s, "'>");
                SendHTMLEncoded(s, value);
                WriteLine(s, "</a>");
            } else if (i + 1 == maxFields && *value == '|') {
                struct ARRAYOFSTRINGS otherFields;
                ArrayOfStringsTokenizeString(&otherFields, value + 1, '|');
                BOOL security =
                    StartsWith(eventValue, "Security") && ArrayOfStringsCount(&otherFields) >= 3;
                if (security) {
                    WriteLine(s, "<a href='/security/tripped?trippedid=");
                    SendUrlEncoded(s, ArrayOfStringsGetValue(&otherFields, 0));
                    WriteLine(s, "'>");
                    SendHTMLEncoded(s, ArrayOfStringsGetValue(&otherFields, 1));
                    WriteLine(s, " ");
                    SendHTMLEncoded(s, ArrayOfStringsGetValue(&otherFields, 2));
                    WriteLine(s, "</a>\n");
                } else {
                    int i;
                    for (i = 0; i < ArrayOfStringsCount(&otherFields); i++) {
                        if (i > 0) {
                            UUSendZ(s, "<br />");
                        }
                        SendHTMLEncoded(s, ArrayOfStringsGetValue(&otherFields, i));
                    }
                }
            } else {
                SendHTMLEncoded(s, value);
            }
        }
        WriteLine(s, "</%s>", td);
    }
    UUSendZ(s, "</tr>\n");
}

static char *AdminAuditRefreshHeader(struct TRANSLATE *t, char *event, int refresh) {
    time_t now;
    char *refreshHeader = NULL;
    size_t len = (event ? strlen(event) * 3 : 0) + strlen(t->myUrl) + 256;
    char *stop;

    if (refresh <= 0)
        return NULL;

    if (!(refreshHeader = malloc(len)))
        PANIC;
    stop = refreshHeader + len - 20;

    UUtime(&now);
    sprintf(
        refreshHeader,
        "<meta http-equiv='Refresh' content='%d; URL=%s/audit?date=today&refresh=%d&now=%" PRIdMAX
        "",
        refresh * 60, t->myUrl, refresh, (intmax_t)now);

    if (event && *event) {
        strcat(refreshHeader, "&event=");
        AddEncodedField(refreshHeader, event, stop);
    }
    strcat(refreshHeader, "#bottom'>\n");

    return refreshHeader;
}

static void AdminAuditShowFile(struct TRANSLATE *t,
                               BOOL header,
                               char *dateField,
                               char *event,
                               int *trs,
                               int refresh,
                               struct AUDITFILTER *auditFilters,
                               BOOL auditFiltersMatchAll,
                               char *titlePrefix) {
    struct UUSOCKET *s = &t->ssocket;
    int f;
    char line[512], headerLine[512];
    char locationDetails[512];
    struct ARRAYOFSTRINGS lineFields, headerFields;
    char fn[MAX_PATH];
    char headerDate[16];
    char title[64];
    int maxFields = 0;
    size_t eventLen = event ? strlen(event) : 0;
    BOOL any = 0;
    struct FILEREADLINEBUFFER frb;
    char *refreshHeader = NULL;
    int eventCol = -1;
    int ipCol = -1;
    int locationCol = -1;

    if (header) {
        refreshHeader = AdminAuditRefreshHeader(t, event, refresh);
    }

    sprintf(fn, "%s.txt", dateField);
    memcpy(headerDate, dateField, 4);
    headerDate[4] = headerDate[7] = '-';
    memcpy(headerDate + 5, dateField + 4, 2);
    memcpy(headerDate + 8, dateField + 6, 2);
    headerDate[10] = 0;
    snprintf(title, sizeof(title), "%s%sAudit Events for %s", titlePrefix ? titlePrefix : "",
             titlePrefix ? " " : "", headerDate);
    if (!AdminAuditValidFilename(fn, 1)) {
        if (header) {
            AdminBaseHeader2(t, AHSORTABLE, title, " | <a href='/audit'>Audit Events</a>",
                             refreshHeader);
            WriteLine(s, "<p>No audit events recorded %s.</p>\n", dateField);
            return;
        }
    } else {
        sprintf(fn, "%s%s.txt", AUDITDIR, dateField);
        f = SOPEN(fn);
        if (f >= 0) {
            if (header) {
                AdminBaseHeader2(t, AHSORTABLE, title, " | <a href='/audit'>Audit Events</a>",
                                 refreshHeader);
            } else {
                UUSendZ(s, "<h2>");
                SendHTMLEncoded(s, title);
                UUSendZ(s, "</h2>\n");
            }

            InitializeFileReadLineBuffer(&frb);

            while (FileReadLine(f, line, sizeof(line), &frb)) {
                if (maxFields == 0) {
                    strcpy(headerLine, line);
                    ArrayOfStringsTokenizeString(&headerFields, headerLine, '\t');
                    if (maxFields = ArrayOfStringsCount(&headerFields)) {
                        eventCol = ArrayOfStringsGetIIndex(&headerFields, "Event");
                        ipCol = ArrayOfStringsGetIIndex(&headerFields, "IP");
                        if (ipCol >= 0 && AnyLocations()) {
                            locationCol = ipCol + 1;
                            ArrayOfStringsInsertValue(&headerFields, "Location", ipCol + 1);
                            maxFields++;
                        }
                    }
                    continue;
                }

                /* Remove current date from start of line */
                if (strncmp(line, headerDate, 10) == 0 && line[10] == ' ')
                    StrCpyOverlap(line, line + 11);

                ArrayOfStringsTokenizeString(&lineFields, line, '\t');

                if (locationCol >= 0) {
                    struct LOCATION location;
                    char *ipValue = ArrayOfStringsGetValue(&lineFields, ipCol);
                    if (ipValue == NULL || *ipValue == 0) {
                        locationDetails[0] = 0;
                    } else {
                        FindLocationByASCIIIP(&location,
                                              ArrayOfStringsGetValue(&lineFields, ipCol));
                        strcpy(locationDetails, location.countryCode);
                        if (location.region[0]) {
                            strcat(locationDetails, " ");
                            strcat(locationDetails, location.region);
                        }
                        if (location.city[0]) {
                            strcat(locationDetails, " ");
                            strcat(locationDetails, location.city);
                        }
                    }
                    ArrayOfStringsInsertValue(&lineFields, locationDetails, locationCol);
                }

                if (event && maxFields > 0) {
                    char *eventValue;
                    if (eventCol == -1)
                        continue;

                    eventValue = ArrayOfStringsGetValue(&lineFields, eventCol);

                    if (eventValue == NULL || *eventValue == 0)
                        continue;

                    if (strnicmp(event, eventValue, eventLen) != 0)
                        continue;
                    eventValue += eventLen;
                    if (*eventValue != 0 && *eventValue != '.')
                        continue;
                }

                if (auditFilters && auditFilters->field) {
                    struct AUDITFILTER *af;
                    BOOL afMatch = 0;

                    for (af = auditFilters; af->field; af++) {
                        int afIdx;
                        char *afValue;
                        char *cond;

                        afMatch = 0;
                        afIdx = ArrayOfStringsGetIIndex(&headerFields, af->field);
                        if (afIdx < 0)
                            break;
                        afValue = ArrayOfStringsGetValue(&lineFields, afIdx);
                        if (afValue == NULL)
                            afValue = "";

                        cond = af->cond;
                        if (*cond == 'n')
                            cond++;

                        if (strcmp(cond, "c") == 0)
                            afMatch = StrIStr(afValue, af->value) != NULL;
                        else if (strcmp(cond, "i") == 0)
                            afMatch = stricmp(afValue, af->value) == 0;
                        else if (strcmp(cond, "s") == 0)
                            afMatch = strnicmp(afValue, af->value, strlen(af->value)) == 0;
                        else if (strcmp(cond, "e") == 0)
                            afMatch = afValue[0] == 0;
                        else
                            afMatch = 0;

                        if (af->cond[0] == 'n')
                            afMatch = !afMatch;

                        if (!afMatch) {
                            if (auditFiltersMatchAll)
                                break;
                        } else {
                            if (!auditFiltersMatchAll)
                                break;
                        }
                    }
                    if (!afMatch)
                        continue;
                }

                if (any == 0) {
                    UUSendZ(s, "<table class='bordered-table sortable' id='audit'>\n");
                    AdminAuditShowTokens(s, &headerFields, maxFields, eventCol, ipCol, locationCol,
                                         trs, 1);
                    any = 1;
                }
                AdminAuditShowTokens(s, &lineFields, maxFields, eventCol, ipCol, locationCol, trs,
                                     0);
            }
            if (any) {
                UUSendZ(s, "</table>\n<br>\n");
            } else {
                UUSendZ(s, "<p>No events matched.</p>\n");
            }
            if (header && refresh >= 0) {
                UUSendZ(s, "<a name='bottom'></a>\n");
                UUSendZ(s, "<form action='/audit' method='get'>\n");
                UUSendZ(s, "<input type='hidden' name='date' value='today'>\n");
                UUSendZ(s,
                        "<label for='refresh'>Refresh every </label><input type='text' "
                        "name='refresh' id='refresh' size='3'");
                if (refresh)
                    WriteLine(s, " value='%d'", refresh);
                UUSendZ(s, "> <input type='submit' value='minute(s)'>\n");
                if (event && *event) {
                    UUSendZ(s, "<input type='hidden' name='event' value='");
                    SendHTMLEncoded(s, event);
                    UUSendZ(s, "'>\n");
                }
                UUSendZ(s, "</form>\n");
            }
        }
        close(f);
    }

    FreeThenNull(refreshHeader);
}

static void AdminAuditSearchForm(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket;
    int i;
    const char *selected = " selected='selected'";

    UUSendZ(s, "<h2>Search Audit Events</h2>\n");
    UUSendZ(s,
            "<form action='/audit' method='get' name='auditsearch'>\n<table id='audittable' "
            "class='loose-table'>");
    UUSendZ(s, "<caption>Search Settings</caption>\n");

    UUSendZ(s,
            "<tr><th scope='row'><label for='date'>Number of previous days to "
            "search:</label>&nbsp;</th><td><input type='text' name='date' id='date' "
            "value='0'></td></tr>\n");

    UUSendZ(s,
            "<tr><th scope='row'><label for='anyall'>Show events that "
            "match</label></th><td><select name='anyall'><option value='all'>all "
            "criteria</option><option value='any'>any criterion</option></select></td></tr>\n");
    UUSendZ(s, "</table>\n");
    UUSendZ(s, "<table id='criteriatable' class='loose-table'>\n");
    UUSendZ(s, "<caption>Criteria</caption>\n");
    UUSendZ(s,
            "<tr><th scope='col'>Field</th><th scope='col'>Condition</th><th "
            "scope='col'>Value</th></tr>\n");
    for (i = 1; i <= MAXAUDITFIELDS; i++) {
        WriteLine(s, "<tr id='row%d'%s><td><select name='field%d' id='field%d'>", i,
                  (i > 4 ? " style='display: none;'" : ""), i, i);
        WriteLine(s,
                  "<option value='event'%s>Event</option>\n\
            <option value='ip'%s>IP</option>\n",
                  (i == 1 ? selected : ""), (i == 3 ? selected : ""));
        if (AnyLocations())
            WriteLine(s, "<option value='location'%s>Location</option>\n",
                      (i == 4 ? selected : ""));
        WriteLine(s,
                  "<option value='username'%s>Username</option>\n\
<option value='session'>Session</option>\n\
<option value='other'>Other</option>\n\
</select>\n</td><td>",
                  i == 2 ? selected : "");
        WriteLine(s, "<select name='cond%d' id='cond%d' onchange='auditempty(%d)'>\n", i, i, i);
        UUSendZ(s,
                "<option value='c'>contains</option>\n\
<option value='nc'>does not contain</option>\n\
<option value='s'>starts with</option>\n\
<option value='ns'>does not start with</option>\n\
<option value='i'>is exactly</option>\n\
<option value='ni'>is not exactly</option>\n\
<option value='e'>is empty</option>\n\
<option value='ne'>is not empty</option>\n\
</select></td><td>\n");
        WriteLine(s, "<input name='value%d' id='value%d' size='20'>\n</td></tr>\n", i, i);
    }
    UUSendZ(s, "</table>\n");
    WriteLine(
        s, "<p><input onclick='return addrow(this);' type='button' value='Add Criterion'></p>\n");
    UUSendZ(s, "<p><input type='submit' name='search' value='Search'></p>\n");
    UUSendZ(s, "</form>\n\
<script>\n\
function auditempty(n) {\n\
  var c = document.getElementById('cond' + n);\n\
  var v = document.getElementById('value' + n);\n\
  \n\
  if (c.value == 'e' || c.value == 'ne') {\n\
    // v.style.display = 'none';\n\
    v.readOnly = true;\n\
    v.value = '';\n\
  } else {\n\
    // v.style.display = '';\n\
    v.readOnly = false;\n\
  }\n\
  return v.readonly;\n\
}\n\
\n\
function addrow() {\n\
var n;\n\
var el;\n\
for (n = 1;; n++) {\n\
  if (n > " STR(MAXAUDITFIELDS) " ) {\n\
    alert('Maximum rows added');\n\
    return;\n\
  }\n\
  el = document.getElementById('row' + n);\n\
  if (el.style.display === 'none') {\n\
    break;\n\
  }\n\
}\n\
el.style.display = '';\n\
document.getElementById('field' + n).selectedIndex = document.getElementById('field' + (n-1)).selectedIndex;\n\
document.getElementById('cond' + n).selectedIndex = document.getElementById('cond' + (n-1)).selectedIndex;\n\
if (!auditempty(n)) {\n\
  document.getElementById('value' + n).focus();\n\
}\n\
location.hash = n;\n\
}\n\
\n\
m = location.hash.replace(/#/, '');\n\
for (n = 1; n <= m; n++) {\n\
  el = document.getElementById('row' + n);\n\
  el.style.display = '';\n\
}\n\
</script>\n");
}

void AdminAudit(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    char fn[64];
    const int AADATE = 0;
    const int AAANYALL = 1;
    const int AASEARCH = 2;
    const int AAEVENT = 3;
    const int AAREFRESH = 4;
    const int AAFIELD1 = 5;
    const int AACOND1 = 6;
    const int AAVALUE1 = 7;
    struct FORMFIELD ffs[] = {
        {"date",    NULL, 0, 0, 11},
        {"anyall",  NULL, 0, 0, 3 },
        {"search",  NULL, 0, 0, 0 },
        {"event",   NULL, 0, 0, 0 },
        {"refresh", NULL, 0, 0, 0 },
 // From here on, there can only be field, cond, value fields
  // followed by a NULL, not to exceed MAXAUDITFIELDS
        {"field1",  NULL, 0, 0, 0 },
        {"cond1",   NULL, 0, 0, 0 },
        {"value1",  NULL, 0, 0, 0 },
        {"field2",  NULL, 0, 0, 0 },
        {"cond2",   NULL, 0, 0, 0 },
        {"value2",  NULL, 0, 0, 0 },
        {"field3",  NULL, 0, 0, 0 },
        {"cond3",   NULL, 0, 0, 0 },
        {"value3",  NULL, 0, 0, 0 },
        {"field4",  NULL, 0, 0, 0 },
        {"cond4",   NULL, 0, 0, 0 },
        {"value4",  NULL, 0, 0, 0 },
        {"field5",  NULL, 0, 0, 0 },
        {"cond5",   NULL, 0, 0, 0 },
        {"value5",  NULL, 0, 0, 0 },
        {"field6",  NULL, 0, 0, 0 },
        {"cond6",   NULL, 0, 0, 0 },
        {"value6",  NULL, 0, 0, 0 },
        {"field7",  NULL, 0, 0, 0 },
        {"cond7",   NULL, 0, 0, 0 },
        {"value7",  NULL, 0, 0, 0 },
        {"field8",  NULL, 0, 0, 0 },
        {"cond8",   NULL, 0, 0, 0 },
        {"value8",  NULL, 0, 0, 0 },
        {"field9",  NULL, 0, 0, 0 },
        {"cond9",   NULL, 0, 0, 0 },
        {"value9",  NULL, 0, 0, 0 },
        {"field10", NULL, 0, 0, 0 },
        {"cond10",  NULL, 0, 0, 0 },
        {"value10", NULL, 0, 0, 0 },
        {"field11", NULL, 0, 0, 0 },
        {"cond11",  NULL, 0, 0, 0 },
        {"value11", NULL, 0, 0, 0 },
        {"field12", NULL, 0, 0, 0 },
        {"cond12",  NULL, 0, 0, 0 },
        {"value12", NULL, 0, 0, 0 },
        {"field13", NULL, 0, 0, 0 },
        {"cond13",  NULL, 0, 0, 0 },
        {"value13", NULL, 0, 0, 0 },
        {"field14", NULL, 0, 0, 0 },
        {"cond14",  NULL, 0, 0, 0 },
        {"value14", NULL, 0, 0, 0 },
        {"field15", NULL, 0, 0, 0 },
        {"cond15",  NULL, 0, 0, 0 },
        {"value15", NULL, 0, 0, 0 },
        {"field16", NULL, 0, 0, 0 },
        {"cond16",  NULL, 0, 0, 0 },
        {"value16", NULL, 0, 0, 0 },
        {"field17", NULL, 0, 0, 0 },
        {"cond17",  NULL, 0, 0, 0 },
        {"value17", NULL, 0, 0, 0 },
        {"field18", NULL, 0, 0, 0 },
        {"cond18",  NULL, 0, 0, 0 },
        {"value18", NULL, 0, 0, 0 },
        {"field19", NULL, 0, 0, 0 },
        {"cond19",  NULL, 0, 0, 0 },
        {"value19", NULL, 0, 0, 0 },
        {"field20", NULL, 0, 0, 0 },
        {"cond20",  NULL, 0, 0, 0 },
        {"value20", NULL, 0, 0, 0 },
        {NULL,      NULL, 0, 0, 0 }
    };
    int trs = 0;
    char *event = NULL;
    size_t eventLen;
    char dateField[16];
    char todayDateField[16];
    struct AUDITFILES *auditFiles, *afp;
    BOOL auditFiltersMatchAll = 0;
    char year[5], month[3], day[3];
    int refresh = 0;
    struct tm tm;
    struct AUDITFILTER auditFilters[MAXAUDITFIELDS + 1];
    int cAuditFilters = 0;
    char title[256];
    char *titlePrefix = NULL;
    char *titleSuffix = NULL;

    if (auditEventsMask == 0) {
        HTTPCode(t, 404, 1);
        return;
    }

    FindFormFields(query, post, ffs);

    if ((event = ffs[AAEVENT].value)) {
        eventLen = strlen(event);
        if (eventLen > 0) {
            titlePrefix = event;
        }
    }

    if (ffs[AADATE].value && strcmp(ffs[AADATE].value, "-1") == 0) {
        titleSuffix = "Since Yesterday";
    }

    if (ffs[AASEARCH].value) {
        int i;
        auditFiltersMatchAll = ffs[AAANYALL].value && stricmp(ffs[AAANYALL].value, "all") == 0;

        for (i = 0; ffs[AAFIELD1 + i].name != NULL; i += 3) {
            if (ffs[AAFIELD1 + i].value && *ffs[AAFIELD1 + i].value && ffs[AACOND1 + i].value &&
                *ffs[AACOND1 + i].value &&
                (strstr(ffs[AACOND1 + i].value, "e") ||
                 (ffs[AAVALUE1 + i].value && *ffs[AAVALUE1 + i].value))) {
                auditFilters[cAuditFilters].field = ffs[AAFIELD1 + i].value;
                auditFilters[cAuditFilters].cond = ffs[AACOND1 + i].value;
                auditFilters[cAuditFilters].value = ffs[AAVALUE1 + i].value;
                cAuditFilters++;
            }
        }
    }
    auditFilters[cAuditFilters].field = NULL;

    if (ffs[AAREFRESH].value) {
        refresh = atoi(ffs[AAREFRESH].value);
        if (refresh < 0 || refresh >= 1440)
            refresh = 0;
    }

    fn[0] = 0;
    if (ffs[AADATE].value || ffs[AASEARCH].value) {
        time_t when = 0;
        char *df;
        BOOL all;

        StrCpy3(dateField, ffs[AADATE].value ? ffs[AADATE].value : "0", sizeof(dateField));

        AllLower(dateField);

        df = dateField;
        if (*df == '-')
            df++;

        if ((all = stricmp(dateField, "all") == 0) || (IsAllDigits(df) && strlen(df) < 5)) {
            char earliest[9];
            if (all) {
                earliest[0] = 0;
            } else {
                struct tm tm;
                char *df = dateField;
                if (*df == '-')
                    df++;
                UUtime(&when);
                when -= 86400 * atoi(df);
                UUlocaltime_r(&when, &tm);
                tm.tm_year += 1900;
                tm.tm_mon++;
                sprintf(earliest, "%04d%02d%02d", tm.tm_year, tm.tm_mon, tm.tm_mday);
            }

            auditFiles = AdminAuditGetAuditFiles(0);
            if (auditFiles) {
                snprintf(title, sizeof(title), "%s%sAudit Events%s%s",
                         titlePrefix ? titlePrefix : "", titlePrefix ? " " : "",
                         titleSuffix ? " " : "", titleSuffix ? titleSuffix : "");
                AdminBaseHeader(t, AHSORTABLE, title, " | <a href='/audit'>Audit Events</a>");

                for (afp = auditFiles; afp; afp = afp->next) {
                    char *period = strchr(afp->name, '.');
                    if (period)
                        *period = 0;
                    if (strlen(afp->name) != 8)
                        continue;
                    if (stricmp(earliest, afp->name) > 0)
                        continue;
                    AdminAuditShowFile(t, 0, afp->name, event, &trs, -1, auditFilters,
                                       auditFiltersMatchAll, titlePrefix);
                }
                AdminAuditFreeAuditFiles(auditFiles);
                auditFiles = NULL;
                goto Finished;
            }
        }

        UUtime(&when);
        UUlocaltime_r(&when, &tm);
        sprintf(todayDateField, "%04d%02d%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);

        if (stricmp(dateField, "today") == 0) {
            strcpy(dateField, todayDateField);
        } else if (stricmp(dateField, "yesterday") == 0) {
            /* when was set up a few lines back to the current date/time */
            when -= 86400;
            UUlocaltime_r(&when, &tm);
            sprintf(dateField, "%04d%02d%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
        }

        if (strcmp(todayDateField, dateField) != 0)
            refresh = -1;

        AdminAuditShowFile(t, 1, dateField, event, &trs, refresh, auditFilters,
                           auditFiltersMatchAll, titlePrefix);
        goto Finished;
    }

    /* use AHNOEXPIRES so people can back up and revise the search form */
    AdminHeader(t, AHNOEXPIRES, "View Audit Events");

    if (auditFiles = AdminAuditGetAuditFiles(0)) {
        AdminAuditSearchForm(t);
        WriteLine(s, "<h2>View Audit Events for the following date%s</h2>\n",
                  (auditFiles->next == NULL ? "" : "s"));
        for (afp = auditFiles; afp; afp = afp->next) {
            StrCpy3(year, afp->name, sizeof(year));
            StrCpy3(month, afp->name + 4, sizeof(month));
            StrCpy3(day, afp->name + 6, sizeof(day));
            WriteLine(s, "<div><a href='audit?date=%s%s%s'>%s-%s-%s</a></div>\n", year, month, day,
                      year, month, day);
        }
        AdminAuditFreeAuditFiles(auditFiles);
        auditFiles = NULL;
    } else {
        UUSendZ(s, "<h2>No Audit Events are available.</h2>\n");
    }

Finished:
    AdminFooter(t, 0);
}

void AuditPurge(void) {
    struct AUDITFILES *auditFiles, *afp;
    struct tm tm;
    time_t when;
    char earliest[9];
    char fn[MAX_PATH];
    static int lastChecked = 0;

    if (auditPurge == 0)
        return;

    UUtime(&when);
    when -= 86400 * auditPurge;
    UUlocaltime_r(&when, &tm);
    if (tm.tm_mday == lastChecked)
        return;
    lastChecked = tm.tm_mday;
    tm.tm_year += 1900;
    tm.tm_mon++;
    sprintf(earliest, "%04d%02d%02d", tm.tm_year, tm.tm_mon, tm.tm_mday);

    auditFiles = AdminAuditGetAuditFiles(1);

    for (afp = auditFiles; afp; afp = afp->next) {
        if (strnicmp(earliest, afp->name, 8) <= 0)
            continue;
        sprintf(fn, "%s%s", AUDITDIR, afp->name);
        if (unlink(fn) == 0) {
            AuditEvent(NULL, AUDITSYSTEM, NULL, NULL, "Purged audit file %s", afp->name);
        }
    }

    AdminAuditFreeAuditFiles(auditFiles);
}

void CheckReopenAuditFile(time_t now) {
    struct tm tm;
    static char auditFileName[MAX_PATH] = {0};
    char tempFilename[MAX_PATH];

    if (now == 0)
        UUtime(&now);
    UUlocaltime_r(&now, &tm);
    tempFilename[sizeof(tempFilename) - 1] = 0;
    strftime(tempFilename, sizeof(tempFilename) - 1, auditFileTemplate, &tm);

    if (strcmp(auditFileName, tempFilename) == 0)
        return;

    if (auditFile >= 0)
        close(auditFile);
    strcpy(auditFileName, tempFilename);

    auditFile = UUopenCreateFD(auditFileName, O_CREAT | O_WRONLY | O_APPEND, defaultFileMode);
    if (auditFile < 0) {
        UUmkdir(AUDITDIR);
        auditFile = UUopenCreateFD(auditFileName, O_CREAT | O_WRONLY | O_APPEND, defaultFileMode);
        if (auditFile < 0) {
            Log("Unable to append %s error %d", auditFileName, errno);
        }
    }

    if (auditFile >= 0) {
        if (lseek(auditFile, 0, SEEK_END) == 0) {
            char *hdr = "Date/Time\tEvent\tIP\tUsername\tSession\tOther\n";
            write(auditFile, hdr, strlen(hdr));
        }
    }
}

/* These AUDITNAMES definitions must be synchronized with AUDIT* entries. */
char *AUDITNAMES[AUDITMAX + 1] = {"Login.Success",
                                  "Login.Success.Groups",
                                  "Login.Success.Password",
                                  "Login.Failure",
                                  "Login.Failure.Password",
                                  "Login.Intruder.IP",
                                  "Login.Intruder.User",
                                  "Logout",
                                  "UsageLimit",
                                  "Unauthorized",
                                  "System",
                                  "Z3950.Connect",
                                  "Z3950.Disconnect",
                                  "Z3950.Denied",
                                  "Info.usr",
                                  "Login.Denied",
                                  "BlockCountryChange",
                                  "UserObject.GetToken",
                                  "UserObject.GetUserObject",
                                  "UserObject.PutUserObject",
                                  "Session.IPChange",
                                  "Session.ReconnectBlocked",
                                  "Login.Success.Relogin",
                                  "MinProxy.Success.Connect",
                                  "MinProxy.Success.Blocked",
                                  "MinProxy.Failure",
                                  "IntrusionAPI.BadIP",
                                  "IntrusionAPI.Error",
                                  "IntrusionAPI.None",
                                  "IntrusionAPI.AllowIP",
                                  "Security",
                                  "Security.Exempt",
                                  "Security.Admin",
                                  NULL};

char *AuditEventNameSelect(int event) {
    char *p;

    if ((event < AUDITMAX) && (event >= 0)) {
        if (AUDITNAMES[event] == NULL) {
            PANICMSG0("AUDIT* definitions must be synchronized with AUDITNAMES entries");
        }
        p = AUDITNAMES[event];
    } else
        p = "";

    return p;
}

void AuditEvent(struct TRANSLATE *t,
                int event,
                const char *user,
                struct SESSION *e,
                char *other,
                ...) {
    time_t now;
    char dn[MAXASCIIDATE];
    char line[4096];
    char ipBuffer[INET6_ADDRSTRLEN];
    va_list ap;
    char *p;
    char *n;

    SecurityEnqueueLogin(t, event, user, e);

    if ((auditEventsMask & (1 << event)) == 0)
        return;

    UUAcquireMutex(&mAuditFile);
    UUtime(&now);
    CheckReopenAuditFile(now);
    if (auditFile < 0)
        goto Finished;

    sprintf(line, "%s\t", ASCIIDate(&now, dn));

    p = strchr(line, 0);

    n = AuditEventNameSelect(event);
    if (*n != 0) {
        strcpy(p, AuditEventNameSelect(event));
    } else {
        sprintf(p, "%d", event);
    }

    p = strchr(p, 0);

    if (t)
        ipToStr(&t->sin_addr, ipBuffer, sizeof ipBuffer);
    else
        ipBuffer[0] = 0;

    if (user == NULL)
        user = "";

    sprintf(p, "\t%s\t%s\t%s\t", ipBuffer, user, (e ? e->key : ""));

    if (other) {
        size_t max;
        p = strchr(p, 0);
        max = sizeof(line) - (p - line) - 1;
        *(p + max) = 0;
        va_start(ap, other);
        _vsnprintf(p, max, other, ap);
        va_end(ap);
        /* Don't allow a single trailing | */
        if (strcmp(p, "|") == 0)
            *p = 0;
    }
    Trim(line, TRIM_TRAIL);
    strcat(line, "\n");

    write(auditFile, line, strlen(line));

Finished:
    UUReleaseMutex(&mAuditFile);
}

void AuditEventLogin(struct TRANSLATE *t,
                     int event,
                     char *user,
                     struct SESSION *e,
                     char *password,
                     char *comment) {
    char *added = NULL;
    size_t len;
    struct GROUP *g;
    char *ipText;

    password = NULL;
    len = 0;

    if (password)
        len += strlen(password) + 20;

    if (comment)
        len += strlen(comment) + 1;

    if (event == AUDITLOGININTRUDERIP && t && t->intruderIP && (ipText = (t->intruderIP)->ipText) &&
        (ipText = strchr(ipText, ':'))) {
        ipText++;
        len += 20 /* Forwarded for */ + strlen(ipText);
    } else {
        ipText = NULL;
    }

    if (e && e->gm && event == AUDITLOGINSUCCESS &&
        (auditEventsMask & AUDITLOGINSUCCESSGROUPSMASK)) {
        len += 20;
        for (g = groups; g; g = g->next) {
            if (GroupMaskOverlap(g->gm, e->gm))
                len += strlen(g->name) + 1;
        }
    }

    if (len) {
        /* The +2 allowed for initial | and final null */
        if (!(added = malloc(len + 2)))
            PANIC;
        strcpy(added, "|");
    }

    if (added && comment && *comment) {
        strcat(added, comment);
        strcat(added, "|");
    }

    if (added && ipText) {
        strcat(added, "Forwarded for ");
        strcat(added, ipText);
        strcat(added, "|");
    }

    if (added && e && e->gm && event == AUDITLOGINSUCCESS &&
        (auditEventsMask & AUDITLOGINSUCCESSGROUPSMASK)) {
        strcat(added, "Groups ");
        for (g = groups; g; g = g->next) {
            if (GroupMaskOverlap(g->gm, e->gm)) {
                strcat(added, " ");
                strcat(added, g->name);
            }
        }
        strcat(added, "|");
    }

    if (added) {
        AuditEvent(t, event, user, e, "%s", added);
    } else {
        AuditEvent(t, event, user, e, NULL);
    }

    FreeThenNull(added);
}
