#define __UUFILE__ "globals.c"

#include "common.h"
#include "intrusion.h"

char *GUARDIANVERSION = EZPROXYVERSIONROOT;

int UUDEFAULT_FAMILY = AF_INET;
struct sockaddr_storage defaultIpInterface;
struct sockaddr_storage minProxyIpInterface;
struct IPTYPE *ipTypes = NULL;
struct IPTYPE *rejectIpTypes = NULL;
struct DATABASE *databases = NULL;
struct TRACKMUTEX *trackMutexes = NULL;
struct EBRARYSITE *ebrarySites = NULL;
struct OVERDRIVESITE *overDriveSites = NULL;
struct GARTNERCONFIG *gartnerConfigs = NULL;
BOOL gartnerKeys = 0;
GROUPMASK gartnerGm = NULL;
GROUPMASK gmAdminAny = NULL;
GROUPMASK gmAdminAudit = NULL;
GROUPMASK gmAdminDecryptVar = NULL;
GROUPMASK gmAdminFiddler = NULL;
GROUPMASK gmAdminGroups = NULL;
GROUPMASK gmAdminIdentifier = NULL;
GROUPMASK gmAdminIntrusion = NULL;
GROUPMASK gmAdminLDAP = NULL;
GROUPMASK gmAdminMessages = NULL;
GROUPMASK gmAdminRestart = NULL;
GROUPMASK gmAdminSecurity = NULL;
GROUPMASK gmAdminShibboleth = NULL;
GROUPMASK gmAdminSSLUpdate = NULL;
GROUPMASK gmAdminSSLView = NULL;
GROUPMASK gmAdminStatusUpdate = NULL;
GROUPMASK gmAdminStatusView = NULL;
GROUPMASK gmAdminToken = NULL;
GROUPMASK gmAdminUsage = NULL;
GROUPMASK gmAdminUsageLimits = NULL;
GROUPMASK gmAdminUser = NULL;
GROUPMASK gmAdminVariables = NULL;
struct SSOSITE *ssoSites = NULL;
struct DENYIFREQUESTHEADER *denyIfRequestHeaders = NULL;
struct HTTPMETHOD *httpMethods = NULL;
struct HTTPHEADER *globalHttpHeaders = NULL;
struct HTTPHEADER *serverHttpHeaders = NULL;
struct EXTRALOGINCOOKIE *extraLoginCookies = NULL;
struct DNSPORT *dnsPorts = NULL;
struct GROUP *groups = NULL;
struct SESSION *sessions = NULL;
struct UUAVLTREE avlSessions;
struct UUAVLTREE avlIntrusionIPs;
struct IPTYPE *whitelistIPs;
struct HOST *hosts = NULL;
struct UUAVLTREE avlHosts;
struct REDIRECTSAFEORNEVERPROXY *redirectSafeOrNeverProxy = NULL;
struct UUAVLTREE avlNeverProxyHosts;
struct NEVERPROXYHOST *neverProxyHosts = NULL;
struct NEVERPROXYHOST *neverProxyHostsLast = NULL;
struct PEER *peers = NULL;
struct HAPEER *haPeers = NULL;
struct LBPEER *lbPeers = NULL;
struct LBPEER *myLbPeer = NULL;

BOOL haPeersValid = 0;
struct REROUTE *reroutes = NULL;
struct TRANSLATE *translates = NULL;
struct TRANSLATE *firstWaitingTranslate = NULL;
struct TRANSLATE *lastWaitingTranslate = NULL;
struct PDFREFRESH *pdfRefreshes = NULL;
char *pdfRefreshPre = NULL;
char *pdfRefreshPost = NULL;
struct USAGELIMIT *usageLimits = NULL;
struct USAGELIMIT *intruderUsageLimit = NULL;
struct USAGELIMITEXCEEDED *usageLimitExceededs = NULL;
struct DATABASEUSAGELIMIT *localDatabaseUsageLimits = NULL;
struct INTRUDERIP *intruderIPs = NULL;
BOOL anyIntruderIPRejects = 0;
BOOL anySourceIP = 0;
struct STARTINGPOINTURLREFRESH *startingPointUrlRefreshes = NULL;
struct LOGFILTER *logFilters = NULL;
struct COOKIEFILTER *globalCookieFilters = NULL;
struct COOKIE *defaultCookies = NULL;
struct ANONYMOUSURL *anonymousUrls = NULL;
struct LOGINPORT *loginPorts = NULL;
struct PROXYHOSTNAMEEDIT *proxyHostnameEdits = NULL;
struct SPUEDIT *spuEdits = NULL;
struct SPUEDITVAR *spuEditVars = NULL;
struct STATICSTRINGCOPY *staticStringCopies = NULL;
struct MIMEFILTER *mimeFiltersDefault = NULL;
struct MIMEFILTER *mimeFiltersUser = NULL;
struct BYTESERVE *byteServes = NULL;

int ipcFile = -1;
struct GUARDIAN *guardian = NULL;

struct INTRUDERIPATTEMPT *intruderIPAttempts = NULL;
int intruderLog = 25;
int intruderReject = 100;
int intruderTimeout = 300;

struct LOCATIONRANGE *locationRanges = NULL;

#ifdef WIN32
char *guardianSharedMemoryID = NULL;
BOOL runningAsService = 0;
#else
int guardianSharedMemoryID = 0;
int mySharedMemoryID = 0;
#endif
pid_t guardianPid;
pid_t chargePid;
pid_t sessionPID;

char *mainMenu = NULL;
int cIpTypes = 0;
int cRejectIpTypes = 0;
int cDatabases = 0;
int cDnsPorts = 0;
int cGroups = 0;
int cWhitelistIPs = 0;
int cSessions = 0;
int cHosts = 0;
int cPeers = 0;
int cReroutes = 0;
int cTranslates = 0;
int cPdfRefreshes = 0;
int cStartingPointUrlRefreshes = 0;
int cLogFilters = 0;
int cAnonymousUrls = 0;
int cLoginPorts = 0;
int cProxyHostnameEdits = 0;
int cMimeFilters = 0;
int maxIncreaseProxyHostnameEdit = 0;
int mIpTypes = 0;
int mRejectIpTypes = 0;
int mWhitelistIPs = 0;
int mDatabases = 1; /* DEFMAXDATABASES; */
int mDnsPorts = 0;
size_t mGroupMask = 1;
int mSessions = DEFMAXSESSIONS;
int mHosts = DEFMAXHOSTS;
int mNeverProxyHosts = DEFMAXNEVERPROXYHOSTS;
int mPeers = 0;
int mReroutes = 0;
int mTranslates = DEFMAXTRANSLATES;
int mSessionLife = DEFSESSIONLIFE;
int mPdfRefreshes = 0;
int mStartingPointUrlRefreshes = 0;
int mLogFilters = 0;
int mAnonymousUrls = 0;
int mLoginPorts = 0;
int mProxyHostnameEdits = 0;
/* We set the peak for sessions and hosts to -1 so something is always recorded at startup. */
int pSessions = -1;
int pHosts = -1;
/*
   Since there are no translates active at startup, there is no peak to record for it,
   so base it from 0
*/
int pTranslates = 0;
int sessionKeySize = DEFAULTSESSIONKEYSIZE;

char *tokenSalt = NULL;
char *autoLoginIPBanner = NULL;
char *excludeIPBanner = NULL;
char excludeIPBannerOnce = 1;
struct STOPSOCKET *stopSockets = NULL;
int radiusTimeout = 6;
int radiusRetry = 1;
char *openURLResolver = NULL;
int loginSocketBacklog = 20;
int hostSocketBacklog = 5;
int dnsSocketBacklog = 20;

int ticksMain = 0;
int ticksCluster = 0;
int ticksStopSockets = 0;
int ticksRaw = 0;
int ticksTranslate = 0;

int mutexTimeout = 90;
#ifdef WIN32
DWORD dMutexTimeout = 90000; /* mutexTimeout * 1000 */
#endif

int potentialListeners = 0;
PORT firstPort = 2048;
PORT primaryLoginPort = 0;
PORT primaryLoginPortSsl = 0;

int wskeyExpirePeriod;
int wskeyThreadSleep;
int wskeyRecheckPeriod;
int wskeyRecheckFailPeriod;

BOOL licenseValid = FALSE;

const char *wsKeyServiceHost = NULL;
PORT wsKeyServicePort;
BOOL wsKeyServiceSsl;
BOOL wsKeyReadConfig = 0;  // let wsKey know the config.txt has been read
struct PROXY *wsKeyProxies;
int wsKeyProxiesCount = 0;
BOOL wsKeyForceProxy = FALSE;

time_t validation = 0;
int usageLogUser = ULUSESSIONCOUNT;
BOOL anyEncryptVar = 0;
BOOL anyInvalidDomains = 0;
BOOL logMail = 0;
BOOL logReferer = 0;
BOOL usageLogSession = 0;
BOOL optionEZproxyCookieHTTPOnly = 0;
BOOL optionRedirectUnknown = 0;
BOOL optionProxyUserObject = FALSE;
BOOL optionPrecreateHosts = FALSE;
BOOL optionBlockCountryChange = 0;
BOOL optionSuppressObfuscation = 0;
BOOL optionDprintf = 0;
BOOL optionAcceptXForwardedFor = 0;
BOOL optionRecordPeaks = 0;
BOOL optionCaptureSpur = 0;
BOOL anyOptionGroupInReferer = 0;
BOOL optionDispatchLog = FALSE;
BOOL optionLogThreadStartup = FALSE;
BOOL optionLoginReplaceGroups = 0;
BOOL optionDisableTestEZproxyUsr = 0;
BOOL optionDisableHTTP11 = 0;
BOOL optionTicketIgnoreExcludeIP = 0;
BOOL optionEbraryUnencodedTokens = 0;
BOOL optionUsernameCaretN = 0;
BOOL optionUserObjectTestMode = 0;
BOOL optionUserObjectGetTokenAlwaysRenew = FALSE;
GROUPMASK gmUserObject = NULL;
BOOL optionAllowHTTPLogin = 0;
BOOL optionForceHttpsAdmin = 0;
BOOL optionAnyDNSHostname = 0;
BOOL optionWildcardHTTPS = 0;
BOOL optionIgnoreWildcardCertificate = 0;
BOOL optionForceWildcardCertificate = 0;
BOOL optionLoginNoCache = 0;
BOOL optionLogDNS = 0;
BOOL optionLogSockets = 0;
BOOL optionFiddler = 1;
BOOL optionLogSAML = 0;
BOOL optionLogXML = 0;
BOOL optionLogSPUEdit = 0;
BOOL optionIgnoreSIGCHLD = 0;
BOOL optionRelaxedRADIUS = 0;
BOOL optionShibboleth = 0;
BOOL optionExcludeIPMenu = 0;
BOOL optionMenuByGroups = 0;
BOOL optionDisableTestLDAPInternal = 0;
BOOL optionHide192 = 0;
BOOL optionDisableSortable = 0;
BOOL optionAllowBadDomains = 0;
BOOL optionReboot = 0;
BOOL optionSafariCookiePatch = 0;
BOOL optionHeaders = 0;
BOOL optionCSRFToken = 0;
BOOL optionSilenceIPWarnings = 0;
int cacheControlMaxAge = 0;
int loginCookieSameSite = ~0;

struct XFFREMOTEIPRANGE *xffRemoteIPRanges = NULL;
char *xffRemoteIPHeader = NULL;

char *defaultCharset = NULL;

// hold additional fields to be displayed on admin status page for sessions.
struct ADMINUISESSION *adminUIsessFields = NULL;

/* SSL strength control */
char *SSLCipherSuiteInbound;
char *SSLCipherSuiteOutbound;

BOOL optionAllowSendChunking = 0;
int optionAllowSendGzip = 0;
BOOL optionUseRaw = 1;
BOOL optionKeepAlive = 0;
int keepAliveTimeout = 15;
int maxKeepAliveRequests = 100;
int receiveBufferSize = 0;
int sendBufferSize = 0;
int minProxyDelay = 0;
char *proxyUrlPassword = NULL;
char *pidFileName = NULL;
int pidFileGuardian = 2;
int pidFileCharge = 0;

int noSpaceHst = 0;

BOOL ttyDetach = FALSE;
BOOL ttyClosed = FALSE;
int msgFile = -1;
char *msgFilenameTemplate = EZPROXYMSG;
BOOL msgFilenameStrftime = FALSE;
BOOL msgFilenameChanged = FALSE;
char msgFilename[MAX_PATH] = EZPROXYMSG;

/* Must keep PROTOCOL values synchronized with protocolNames in common.h */
struct PROTOCOLDETAIL protocolDetails[] = {
    {PROTOCOLHTTP,  "http://",  7, 80 },
    {PROTOCOLHTTPS, "https://", 8, 443},
    {PROTOCOLFTP,   "ftp://",   6, 21 },
    {0,             NULL,       0, 0  }
};

char *auditFileTemplate = AUDITDIR "%Y%m%d.txt";
char *firstProxyHost = NULL;
PORT firstProxyPort = 0;
char *firstProxyAuth = NULL;
char *minProxyAuth = NULL;
int debugLevel = 0;
int connectWindow = 60;
int environment = 0;
char *shibbolethWAYF = NULL;
char *shibbolethProviderId = NULL;
BOOL optionSecurityStartupStats = FALSE;
BOOL optionSQLStats = 0;
BOOL optionStatus = 0;
BOOL optionAllowWebSubdirectories = 0;
BOOL optionAllowLogoutURLRedirect = FALSE;
BOOL optionPassiveCookie = 1;
BOOL optionPostCRLF = 0;
BOOL optionForceSsl = 0;
BOOL optionTicks = 0;
BOOL optionTrackSockets = 0;
BOOL optionSingleSession = 0;
BOOL optionSingleThreadedTranslations = 0;
BOOL optionDisableNagle = 1;
BOOL optionAllowDebugger = 0;
BOOL optionRefererInHostname = 0;
char *mallocCheck = NULL;
int binaryTimeout = 60;
int remoteTimeout = 60;
int clientTimeout = 60;
enum PROXYTYPE optionProxyType = PTPORT;
char *accountDefaultPassword = NULL;
BOOL optionRequireAuthenticate = 0;
struct LOGSPU *logSPUs = NULL;
struct LOGTAGMAP *logTagMaps = NULL;

#ifdef UUTIME_R0
UUMUTEX mTimeFuncs;
#endif

struct RWL rwlHosts;
struct RWL rwlSessions;
struct RWL rwlIntrusionIPs;
struct RWL rwlIpPortStatuses;
struct RWL rwlDnsTable;
UUMUTEX mIpPortStatuses;
UUMUTEX mDnsTable;
UUMUTEX mAccessFile;
UUMUTEX mAuditFile;
UUMUTEX mMsgFile;
UUMUTEX mStopSockets;
UUMUTEX mActive;
UUMUTEX mToken;
UUMUTEX mCookies;
UUMUTEX mInitSslKey;
UUMUTEX mInitSsl;
UUMUTEX mCluster;
UUMUTEX mGetHostByName;
UUMUTEX mGetHostByNameTree;
UUMUTEX mAccountFile;
UUMUTEX mTrackSocket;
UUMUTEX mPdfUrls;
UUMUTEX mAdminSsl;
UUMUTEX mSslCtxCache;
UUMUTEX mIntruders;
UUMUTEX mUsage;
UUMUTEX mLDAPBind;
UUMUTEX mShibbolethShire;
UUMUTEX mCasTickets;
UUMUTEX mFiddler;
UUMUTEX mSecurity;
UUMUTEX mGenerateUniqueKey;

char *loginCookieName = "ezproxy";
size_t loginCookieNameLen = 7;
char *privateComputerCookieName = "ezproxyPrivateComputer";
size_t privateComputerCookieNameLen = 22;
char *requireAuthenticateCookieName = "ezproxyrequireauthenticate2";
char *refererCookieName = "ezproxydest";
enum LOGINCOOKIEDOMAINTYPE loginCookieDomainType = LOGINCOOKIEDOMAINDEFAULT;
char *loginCookieDomain = NULL;
time_t lastModifiedMinimum = 0;
char ezproxyCfgDigest[33] = {0};
BOOL someKey = TRUE;
struct HOST *mvHost = NULL;
struct HOST *mvHostSsl = NULL;
char *cpipSecret = NULL;
struct SESSION *anonymousSession = NULL;

char *mailServer = NULL;
char *mailFrom = NULL;
char *mailTo = NULL;

int defaultFileMode = 0666;
int defaultUmask = 0077;

volatile int networkTestsRunning = 0;

char *monthNames[] = {"January", "February",  "March",   "April",    "May",      "June", "July",
                      "August",  "September", "October", "November", "December", NULL};
char *IPACCESSNAMES[] = {"ExcludeIP", "AutoLoginIP", "IncludeIP", "RejectIP"};
char *HTTPHEADER_DIRECTION_NAMES[] = {"", "-request", "-response"};
char *HTTPHEADER_PROCESSING_NAMES[] = {"",           "-process", "-block", "-rewrite",
                                       "-unrewrite", "-edit",    "-inject"};

char *MIMEACTIONNAMES[] = {"javascript", "html", "text", "pdf", "image", "none"};

#ifndef WIN32
uid_t runUid;
gid_t runGid;
char changedUid = 0;
char changedGid = 0;
#endif

char *base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
char *peerStatusName[] = {"Down", "Starting", "Syncing", "Up", "Stale"};
char *haPeerStatusName[] = {"Down", "Starting", "Syncing", "Up", "Stale"};

#define CYCLE_TIME_SECONDS (1)
#define CYCLE_TIME_MINUTES (60)
#define CYCLE_TIME_HOURS (60 * CYCLE_TIME_MINUTES)
#define CYCLE_TIME_DAYS (24 * CYCLE_TIME_HOURS)
char *guardianChargeName[] = {EZPROXYNAMEPROPER,
                              "HandleRaw",
                              "StopSockets",
                              "RefreshShibbolethMetadata",
                              "Cluster",
                              "DNS",
                              "SaveUsage",
                              "WskeyLicenseCheck",
                              "DNSCache",
                              "Security",
                              "Identifier",
                              "SendSecurityRuleDigestEmail",
                              "SendQueuedSecurityRuleEmail",
                              "Spur",
                              ""};
DWORD guardianChargeMaximumLatency[] = {
    (1 * CYCLE_TIME_MINUTES) * LATENCY_FACTOR,
    (1 * CYCLE_TIME_MINUTES) * LATENCY_FACTOR,
    (1 * CYCLE_TIME_MINUTES) * LATENCY_FACTOR,
    ((1 * CYCLE_TIME_DAYS) + (2 * CYCLE_TIME_HOURS)) * LATENCY_FACTOR,
    (1 * CYCLE_TIME_MINUTES) * LATENCY_FACTOR,
    (1 * CYCLE_TIME_MINUTES) * LATENCY_FACTOR,
    (10 * CYCLE_TIME_MINUTES) * LATENCY_FACTOR,
    (2 * CYCLE_TIME_HOURS) * LATENCY_FACTOR,
    0.0,
    (15 * CYCLE_TIME_MINUTES) * LATENCY_FACTOR,
    (15 * CYCLE_TIME_MINUTES) * LATENCY_FACTOR,
    (15 * CYCLE_TIME_MINUTES) * LATENCY_FACTOR,
    (5 * CYCLE_TIME_MINUTES) * LATENCY_FACTOR,
    (5 * CYCLE_TIME_MINUTES) * LATENCY_FACTOR};

int guardianChargeForceSeizureIndex = -1;
int guardianChargeForceSeizureDelay = 15;

#ifndef WIN32
pthread_attr_t pthreadCreateDetached;
#endif

time_t startupTime = 0;
time_t dnsSerial = 0;
time_t nextExpire;
time_t nextSaveCookies;
time_t hostsNeedSave = 0;
int hostsSaved = 0;
int usageSaved = 0;

char *myUrl = NULL;
char *myUrlHttp = NULL;
char *myUrlHttps = NULL;
char *myDomain;
char myName[MYNAMELEN] = {0};
size_t myNameLen = 0;
char myHttpsName[MYNAMELEN] = {0};
char myHttpName[MYNAMELEN] = {0};
char *serverHeader = "EZproxy";
char *haName = NULL;
char *myBinary = NULL;
char *myPath = NULL;
char *myStartupPath = NULL;
char *myChangedPath = NULL;
char *p3pHeader = NULL;

volatile int restarting = 0;
volatile int sigusr = 0;
volatile int rebuildMainMaster = 1;

void InitMutexes(void) {
    static char inited = 0;

    /* The Win32 service version calls early, the UNIX version calls late, so let it be called twice
     * safely */
    if (inited)
        return;

#ifdef UUTIME_R0
    UUInitMutex(&mTimeFuncs, "mTimeFuncs");
#endif

    RWLInit(&rwlHosts, "rwlHosts");
    RWLInit(&rwlSessions, "rwlSessions");
    RWLInit(&rwlIntrusionIPs, "intrusionIPs");
    RWLInit(&rwlIpPortStatuses, "rwlIpPortStatuses");
    RWLInit(&rwlDnsTable, "rwlDnsTable");
    UUInitMutex(&mIpPortStatuses, "mIpPortStatuses");
    UUInitMutex(&mDnsTable, "mDnsTable");
    UUInitMutex(&mAccessFile, "mAccessFile");
    UUInitMutex(&mAuditFile, "mAuditFile");
    UUInitMutex(&mMsgFile, "mMsgFile");
    UUInitMutex(&mStopSockets, "mStopSockets");
    UUInitMutex(&mActive, "mActive");
    UUInitMutex(&mCookies, "mCookies");
    UUInitMutex(&mInitSsl, "mInitSsl");
    UUInitMutex(&mInitSslKey, "mInitSslKey");
    UUInitMutex(&mCluster, "mCluster");
    UUInitMutex(&mGetHostByName, "mGetHostByName");
    UUInitMutex(&mGetHostByNameTree, "mGetHostByNameTree");
    UUInitMutex(&mAccountFile, "mAccountFile");
    UUInitMutex(&mTrackSocket, "mTrackSocket");
    UUInitMutex(&mPdfUrls, "mPdfUrls");
    UUInitMutex(&mAdminSsl, "mAdminSsl");
    UUInitMutex(&mSslCtxCache, "mSslCtxCache");
    UUInitMutex(&mIntruders, "mIntruders");
    UUInitMutex(&mUsage, "mUsage");
    UUInitMutex(&mLDAPBind, "mLDAPBind");
    UUInitMutex(&mShibbolethShire, "mShibbolethShire");
    UUInitMutex(&mToken, "mToken");
    UUInitMutex(&mCasTickets, "mCasTickets");
    UUInitMutex(&mFiddler, "mFiddler");
    UUInitMutex(&mSecurity, "mSecurity");
    UUInitMutex(&mGenerateUniqueKey, "mGenerateUniqueKey");

    /* Disable logging of duplicate attempts on these mutexes as these are the
       ones used to acquire logging access, potentially leading to an endless
       loop
    */
    mMsgFile.logDuplicateAttempts = 0;
#ifdef UUTIME_R0
    mTimeFuncs.logDuplicateAttempts = 0;
#endif

    inited = 1;
}

void InitUUAvlTrees(void) {
    UUAvlTreeInit(&avlSessions, "avlSessions", UUAvlTreeCmpSession, NULL, NULL);
    UUAvlTreeInit(&avlHosts, "avlHosts", UUAvlTreeCmpHost, NULL, NULL);
    UUAvlTreeInit(&avlNeverProxyHosts, "avlNeverProxyHosts", UUAvlTreeCmpNeverProxyHosts, NULL,
                  NULL);
    UUAvlTreeInit(&avlIpPortStatuses, "avlIpPortStatuses", UUAvlTreeCmpIpPortStatuses, NULL, NULL);
    UUAvlTreeInit(&avlIntrusionIPs, "avlIntrustionIPs", UUAvlTreeCmpIntrusionIPs, NULL, NULL);
}
