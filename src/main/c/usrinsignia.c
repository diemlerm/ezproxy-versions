/**
 * Insignia (a small ILS) user authentication
 *
 * This authentication method supports common conditions and actions.
 */

#define __UUFILE__ "usrinsignia.c"

#include "common.h"

#include "usr.h"
#include "usrinsignia.h"

#include "uuxml.h"

#include "html.h"

struct INSIGNIACTX {
    char *valid;
    char *invalid;
};

/*
int UsrInsigniaIfTest(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg)
{
    struct INSIGNIACTX *INSIGNIACTX = (struct INSIGNIACTX *) context;
    char *arg2;
    char compareOperator;
    char compareType;
    xmlChar *val;
    int result = 1;

    arg2 = SkipNonWS(arg);
    if (*arg2) {
        *arg2++ = 0;
        arg2 = SkipWS(arg2);
    }

    arg2 = UsrTestComparisonOperatorType(arg2, &compareOperator, &compareType);

    if (stricmp(arg, "ItemsOverdue") == 0)
        val = INSIGNIACTX->itemsOverdue;
    else if (stricmp(arg, "ItemsLost") == 0)
        val = INSIGNIACTX->itemsLost;
    else if (stricmp(arg, "Balance") == 0)
        val = INSIGNIACTX->balance;
    else if (stricmp(arg, "Location") == 0)
        val = INSIGNIACTX->location;
    else {
        UsrLog(uip, "HIP unknown test variable: %s", arg);
        return 1;
    }

    if (val == NULL)
        val = BAD_CAST "";

    result = UsrTestCompare((char *) val, arg2, compareOperator, compareType) ? 0 : 1;

    if (uip->debug)
        UsrLog(uip, "HIP test %s %s against %s result %s", arg, val, arg2, result ? "FALSE" :
"TRUE");

    return result;
}
*/

int UsrInsigniaValid(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct INSIGNIACTX *insigniaCtx = (struct INSIGNIACTX *)context;

    ReadConfigSetString(&insigniaCtx->valid, arg);
    return 0;
}

int UsrInsigniaInvalid(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct INSIGNIACTX *insigniaCtx = (struct INSIGNIACTX *)context;

    ReadConfigSetString(&insigniaCtx->invalid, arg);
    return 0;
}

int UsrInsigniaURL(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    int result = 1;
    char *user, *pass;
    struct INSIGNIACTX *insigniaCtx = (struct INSIGNIACTX *)context;

    htmlParserCtxtPtr ctxt = NULL;
    xmlChar *viewState = NULL;
    xmlChar *href = NULL;
    struct COOKIE *cookies = NULL;
    //  char *url = "http://www.thompsonlibrary.com/library/LoginPatron.aspx?App=MyAccount";
    //  char *url = "http://jakeepp.gotdns.com/library/LoginPatron.aspx?App=MyAccount";
    //  char *user = "26783000143827";
    //  char *pass = "sample";
    //
    char *valid = insigniaCtx->valid ? insigniaCtx->valid : "MyAccount";
    char *invalid = insigniaCtx->invalid ? insigniaCtx->invalid : "ErrMsg";

    user = uip->user ? uip->user : "";
    pass = uip->pass ? uip->pass : "";

    if (*user == 0 || strlen(user) > 32 || strlen(pass) > 32)
        goto Finished;

    ctxt = GetHTML(arg, NULL, &cookies, NULL, uip->pInterface, 0, 0, NULL, NULL, uip->debug);
    if (ctxt == NULL) {
        UsrLog(uip, "Insignia server did not respond for %s\n", arg);
        goto Finished;
    }
    viewState = UUxmlSimpleGetContent(ctxt->myDoc, NULL, "//input[@name = '__VIEWSTATE']/@value");
    UUxmlFreeParserCtxtAndDoc(&ctxt);

    if (viewState == NULL) {
        UsrLog(uip, "Failed to retrieve __VIEWSTATE");
        goto Finished;
    }

    sprintf(t->buffer, "__VIEWSTATE=");
    AddEncodedField(t->buffer, (char *)viewState, t->buffer + sizeof(t->buffer) - 1024);
    strcat(t->buffer, "&txtName=");
    AddEncodedField(t->buffer, user, NULL);
    strcat(t->buffer, "&txtPassword=");
    AddEncodedField(t->buffer, pass, NULL);
    strcat(t->buffer, "&B1=Log+In&App=MyAccount&Subscription=&ItemID=");

    ctxt = GetHTML(arg, t->buffer, &cookies, NULL, uip->pInterface, GETHTMLNOFOLLOWREDIRECTS, 0,
                   NULL, NULL, uip->debug);
    if (ctxt == NULL) {
        UsrLog(uip, "Insignia server did not respond for %s\n", arg);
        goto Finished;
    }
    href = UUxmlSimpleGetContent(ctxt->myDoc, NULL, "//h2/a/@href");
    UUxmlFreeParserCtxtAndDoc(&ctxt);

    if (StrIStr((char *)href, valid))
        uip->result = resultValid;
    else if (StrIStr((char *)href, invalid))
        uip->result = resultInvalid;
    else {
        UsrLog(uip, "Insignia unrecognized response: %s", href ? (char *)href : "");
    }

Finished:
    FreeThenNull(href);
    xmlFreeThenNull(viewState);
    GetHTMLFreeCookies(&cookies);

    if (uip->debug) {
        UsrLog(uip, "Insignia results for %s", user);
        if (uip->result == resultValid) {
            UsrLog(uip, "User valid");
        }
    }

    return result;
}

enum FINDUSERRESULT FindUserInsignia(struct TRANSLATE *t,
                                     struct USERINFO *uip,
                                     char *file,
                                     int f,
                                     struct FILEREADLINEBUFFER *frb,
                                     struct sockaddr_storage *pInterface) {
    struct USRDIRECTIVE udc[] = {
  /* { "IfTest",           UsrInsigniaIfTest,           0 }, */
        {"Invalid", UsrInsigniaInvalid, 0},
        {"URL",     UsrInsigniaURL,     0},
        {"Valid",   UsrInsigniaValid,   0},
        {NULL,      NULL,               0}
    };
    enum FINDUSERRESULT result;
    struct INSIGNIACTX insigniaCtx;

    memset(&insigniaCtx, 0, sizeof(insigniaCtx));

    uip->flags = 0;
    result = UsrHandler(t, "Insignia", udc, &insigniaCtx, uip, file, f, frb, pInterface, NULL);

    FreeThenNull(insigniaCtx.valid);
    FreeThenNull(insigniaCtx.invalid);

    return result;
}
