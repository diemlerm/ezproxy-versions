#define __UUFILE__ "usertoken.c"

#include "common.h"
#include "uustring.h"
#include "openssl/des.h"
#include <sys/stat.h>
#include "usertoken.h"

static int tokenFile = -1;

static int OpenTokenFile(void) {
    if (tokenFile == -1) {
#ifdef WIN32
        tokenFile = _sopen(EZPROXYTKN, _O_RDWR | _O_CREAT, _SH_DENYNO, _S_IREAD | _S_IWRITE);
#else
        tokenFile = MoveFileDescriptorHigh(open(EZPROXYTKN, O_RDWR | O_CREAT, defaultFileMode));
#endif
        if (debugLevel > 5)
            Log("%" SOCKET_FORMAT " Open %s, errno=%d", tokenFile, EZPROXYTKN,
                (tokenFile == INVALID_SOCKET) ? errno : 0);
        if (tokenFile == -1) {
            Log("Unable to open %s: %d", EZPROXYTKN, errno);
            return -1;
        }
    }
    return tokenFile;
}

/*
 * This is the userName to token machine for Books24x7.
 */
char *UserToken(struct TRANSLATE *t, unsigned char *tokenKey, char *userName) {
    DES_cblock ivec;
    char *input = NULL;
    char *output = NULL;
    unsigned char *hold = NULL;
    EVP_CIPHER_CTX *ctx = NULL;
    int updateLen, finalLen;

    if ((userName == NULL) || (tokenKey == NULL) || (t->session == NULL)) {
        return NULL;
    }

    if (!(input = strdup(userName)))
        PANIC;
    Trim(input, TRIM_LEAD | TRIM_TRAIL);
    AllLower(input);

    /* Can't grow by more than 1 for null, 16 for the possible pad */
    if (!(hold = malloc(strlen(input) + 17)))
        PANIC;

    if (!(ctx = EVP_CIPHER_CTX_new()))
        PANIC;
    EVP_CIPHER_CTX_init(ctx);
    memset(&ivec, 0, sizeof(ivec));
    EVP_EncryptInit(ctx, EVP_des_ede3_cbc(), (unsigned char *)tokenKey, ivec);

    /* Send the NULL as well; we check on receive to see if it is there */
    if (!EVP_EncryptUpdate(ctx, hold, &updateLen, (unsigned char *)input, strlen(input) + 1)) {
        goto Cleanup;
    }

    if (!EVP_EncryptFinal(ctx, hold + updateLen, &finalLen)) {
        goto Cleanup;
    }

    output = Encode64Binary(NULL, hold, updateLen + finalLen);

Cleanup:
    if (ctx) {
        EVP_CIPHER_CTX_free(ctx);
        ctx = NULL;
    }
    FreeThenNull(input);
    FreeThenNull(hold);

    return output;
}

char *UserToken2(char *prefix, char *user, BOOL createIfNotFound) {
    char line[256];
    char *colons[4];
    off_t where;
    char *result = NULL;
    char *lowerCopy = NULL;
    time_t now;
    int i;
    char *newToken = NULL;
    BOOL tokenTaken;
    EVP_MD_CTX *mdctx = NULL;
    unsigned int mdLen;
    int mdLenTrial;
    unsigned char digest[EVP_MAX_MD_SIZE];
    unsigned char *nextDigestChunk = NULL;
    const int tokenBytes = 5;
    char *obscurity = (char *)Obscurity(2);

    UUAcquireMutex(&mToken);

    if (user == NULL)
        goto Done;

    if (strcmp(prefix, "ebr") == 0 && optionEbraryUnencodedTokens) {
        if (!(result = strdup(user)))
            PANIC;
        AllLower(result);
        goto Done;
    }

    if (OpenTokenFile() == -1)
        goto Done;

    if (createIfNotFound) {
        if (!(newToken = malloc(strlen(prefix) + tokenBytes * 2 + 1)))
            PANIC;

        if (!(mdctx = EVP_MD_CTX_new()))
            PANIC;
        EVP_DigestInit(mdctx, EVP_sha512());

        if (tokenSalt)
            EVP_DigestUpdate(mdctx, tokenSalt, strlen(tokenSalt));

        if (!(lowerCopy = strdup(user)))
            PANIC;
        AllLower(lowerCopy);
        EVP_DigestUpdate(mdctx, lowerCopy, strlen(lowerCopy));
        FreeThenNull(lowerCopy);

        if (haName && *haName)
            EVP_DigestUpdate(mdctx, haName, strlen(haName));
        else
            EVP_DigestUpdate(mdctx, myName, strlen(myName));

        EVP_DigestUpdate(mdctx, prefix, strlen(prefix));

        EVP_DigestUpdate(mdctx, obscurity, strlen(obscurity));

        EVP_DigestFinal(mdctx, digest, &mdLen);
        nextDigestChunk = digest;
    }

    do {
        tokenTaken = 0;
        mdLenTrial = mdLen;

        if (newToken) {
            StrCpyOverlap(newToken, prefix);
            DigestToHex(AtEnd(newToken), nextDigestChunk, tokenBytes, 0);
            nextDigestChunk += tokenBytes;
            mdLenTrial -= tokenBytes;
            if (mdLenTrial < 0) {
                Log("Unable to derive unique token for %s %s", prefix, user);
                goto Done;
            }
        }

        lseek(tokenFile, 0, SEEK_SET);

        for (;;) {
            where = lseek(tokenFile, 0, SEEK_CUR);
            if (!(FileReadLine(tokenFile, line, sizeof(line), NULL)))
                break;
            colons[0] = strchr(line, ':');
            if (colons[0] == NULL)
                continue;
            *colons[0] = 0;
            if (stricmp(line, prefix) != 0)
                continue;
            for (i = 1; i < 4; i++) {
                colons[i] = strchr(colons[i - 1] + 1, ':');
                if (colons[i] == NULL)
                    break;
                *colons[i] = 0;
            }
            if (i < 4)
                continue;
            if (stricmp(colons[3] + 1, user) == 0) {
                if (!(result = strdup(colons[0] + 1)))
                    PANIC;
                UUtime(&now);
                sprintf(colons[2] + 1, "%" PRIdMAX "", (intmax_t)(now));
                for (i = 0; i < 4; i++)
                    *colons[i] = ':';
                lseek(tokenFile, where, SEEK_SET);
                goto Update;
            }

            /* If our proposed new token is in use, note need to try again
               if we don't find a token that matches this prefix/user combo
            */
            if (newToken && strcmp(newToken, colons[0] + 1) == 0) {
                tokenTaken = 1;
            }
        }

        if (newToken == NULL)
            goto Done;

    } while (tokenTaken);

    UUtime(&now);
    sprintf(line, "%s:%s:%" PRIdMAX ":%" PRIdMAX ":%s\n", prefix, newToken, (intmax_t)(now),
            (intmax_t)(now), user);
    result = newToken;
    newToken = NULL;

Update:
    write(tokenFile, line, strlen(line));

Done:
    if (mdctx) {
        EVP_MD_CTX_free(mdctx);
        mdctx = NULL;
    }
    UUReleaseMutex(&mToken);

    FreeThenNull(newToken);

    return result;
}

char *UserTokenDecrypt(char *input, unsigned char *key) {
    DES_cblock ivec;
    unsigned char *hold = NULL;
    size_t len;
    EVP_CIPHER_CTX *ctx = NULL;
    int updateLen, finalLen;
    unsigned char *output = NULL;

    if (!(hold = malloc(strlen(input) + 1)))
        PANIC;
    Decode64Binary(hold, &len, input);
    if (len == 0 || len % 8 != 0)
        goto Invalid;

    if (!(ctx = EVP_CIPHER_CTX_new()))
        PANIC;
    EVP_CIPHER_CTX_init(ctx);
    memset(&ivec, 0, sizeof(ivec));
    EVP_DecryptInit(ctx, EVP_des_ede3_cbc(), key, ivec);

    /* The decrypted version cannot be larger than the encrypted version */
    if (!(output = malloc(len)))
        PANIC;

    if (!EVP_DecryptUpdate(ctx, output, &updateLen, hold, len))
        goto Invalid;

    if (!EVP_DecryptFinal(ctx, output + updateLen, &finalLen))
        goto Invalid;

    EVP_CIPHER_CTX_cleanup(ctx);
    len = updateLen + finalLen;
    if (output[len - 1] == 0)
        goto Cleanup;

Invalid:
    FreeThenNull(output);

Cleanup:
    if (ctx) {
        EVP_CIPHER_CTX_free(ctx);
        ctx = NULL;
    }
    FreeThenNull(hold);

    return (char *)output;
}

char *UserTokenDecrypt2(char *key, char *prefix) {
    char line[256];
    char *colons[4];
    char *result = NULL;
    int i;

    UUAcquireMutex(&mToken);

    if (strcmp(prefix, "ebr") == 0 && optionEbraryUnencodedTokens) {
        if (!(result = strdup(key)))
            PANIC;
        AllLower(result);
        goto Done;
    }

    if (OpenTokenFile() == -1)
        goto Done;

    lseek(tokenFile, 0, SEEK_SET);

    while (FileReadLine(tokenFile, line, sizeof(line), NULL)) {
        colons[0] = strchr(line, ':');
        if (colons[0] == NULL)
            continue;
        *colons[0] = 0;
        if (stricmp(line, prefix) != 0)
            continue;
        for (i = 1; i < 4; i++) {
            colons[i] = strchr(colons[i - 1] + 1, ':');
            if (colons[i] == NULL)
                break;
            *colons[i] = 0;
        }
        if (i < 4)
            continue;
        if (stricmp(colons[0] + 1, key) == 0) {
            if (!(result = strdup(colons[3] + 1)))
                PANIC;
            break;
        }
    }

Done:
    UUReleaseMutex(&mToken);

    return result;
}

/*
 * This is the token to tokenSignature machine for Books24x7.
 */
char *UserTokenSignature(struct TRANSLATE *t, struct DATABASE *b) {
    char *token = NULL;
    DES_cblock ivec;
    char *input = NULL;
    unsigned char *hold = NULL;
    int updateLen, finalLen;
    size_t len;
    unsigned char *from;
    char *to;
    size_t i;
    EVP_CIPHER_CTX *ctx = NULL;
    char *output = NULL;
    time_t now;
    MD5_CTX md5Context;
    unsigned char md5Signature[16];
    char ipBuffer[INET6_ADDRSTRLEN];

    if ((t == NULL) || (b == NULL) || (b->tokenSignatureKey == NULL)) {
        goto Cleanup;
    }

    token = UserToken(t, b->tokenKey, (t->session)->logUserBrief);
    if (token == NULL)
        if (!(token = strdup("")))
            PANIC;

    /* The 128 allows more than enough for the 32 char MD5, the 2 characters of salt, the 15 char
     * max IP, and the 10 digit time, and the | between all */
    if (!(input = malloc((token ? strlen(token) : 0) + 128)))
        PANIC;
    UUtime(&now);
    sprintf(input + 33, "%c%c|%" PRIdMAX "|%s|%s", RandChar(), RandChar(), (intmax_t)now,
            ipToStr(&t->sin_addr, ipBuffer, sizeof ipBuffer), token);

    MD5_Init(&md5Context);
    MD5_Update(&md5Context, (unsigned char *)(input + 33), (unsigned int)strlen(input + 33));
    MD5_Final(md5Signature, &md5Context);
    MD5DigestToHex(input, md5Signature, 1);
    /*
    for (i = 0, from = md5Signature, to = input; i < 16; from++, i++, to += 2)
        sprintf(to, "%02X", *from);
    */
    to = strchr(input, 0);
    *to++ = '|';

    /* With PKCS5 padding, it will grow by no more than 8 bytes */
    if (!(hold = malloc(strlen(input) + 8)))
        PANIC;

    if (!(ctx = EVP_CIPHER_CTX_new()))
        PANIC;
    EVP_CIPHER_CTX_init(ctx);
    memset(&ivec, 0, sizeof(ivec));
    EVP_EncryptInit(ctx, EVP_des_ede3_cbc(), b->tokenSignatureKey, ivec);

    if (!EVP_EncryptUpdate(ctx, hold, &updateLen, (unsigned char *)input, strlen(input))) {
        goto Cleanup;
    }

    if (!EVP_EncryptFinal(ctx, hold + updateLen, &finalLen)) {
        goto Cleanup;
    }

    len = updateLen + finalLen;

    if (!(output = malloc(len * 2 + 1)))
        PANIC;

    for (i = 0, from = hold, to = output; i < len; from++, i++, to += 2)
        sprintf(to, "%02X", *from);

Cleanup:
    if (ctx) {
        EVP_CIPHER_CTX_free(ctx);
        ctx = NULL;
    }

    FreeThenNull(token);
    FreeThenNull(hold);
    FreeThenNull(input);

    return output;
}

static int CaretLen(char *s) {
    if (*s == '^')
        return 1;
    if (strnicmp(s, "%5e", 3) == 0)
        return 3;
    return 0;
}

void SendTokenizedURL2(struct TRANSLATE *t,
                       struct UUSOCKET *s,
                       struct DATABASE *b,
                       char *url,
                       char **token,
                       char **tokenSignature,
                       BOOL allowUsernamePassword) {
    char *p;
    struct SESSION *e = t->session;
    int v;
    int cl;

    if (url == NULL)
        return;

    /* If there is no database, or no token encryption logic, or no ^ to substitute into,
       then nothing special is needed and we can just send it and go back
    */
    if (b == NULL || (b->tokenKey == NULL && b->allowVars == NULL)) {
        UUSendZ(s, url);
        return;
    }

    /* p already points to first ^ by text above */
    for (p = url; *p; p++) {
        if ((cl = CaretLen(p)) == 0) {
            UUSend(s, p, 1, 0);
            continue;
        }

        p += cl;

        if (cl = CaretLen(p)) {
            UUSend(s, p, cl, 0);
            if (cl > 1)
                p += (cl - 1);
            continue;
        }

        switch (*p) {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            if (b->allowVars && strchr(b->allowVars, *p) != NULL) {
                v = *p - '0';
                if (e->vars[v])
                    SendUrlEncodedEncrypted(s, e->vars[v], b, *p);
            }
            break;

        case 'B':
        case 'b':
            UUSendZ(s, "site=");
            if (b->books24X7Site)
                SendUrlEncoded(s, b->books24X7Site);
            UUSendZ(s, "&stoken=");

            /* flow does intentionally continue into S */

        case 'S':
        case 's':
            if (*tokenSignature == NULL)
                *tokenSignature = UserTokenSignature(t, b);
            if (*tokenSignature)
                SendUrlEncoded(s, *tokenSignature);
            break;

        case 'T':
        case 't':
            if (b) {
                if (*token == NULL)
                    *token = UserToken(t, b->tokenKey, e->logUserBrief);
                if (*token)
                    SendUrlEncoded(s, *token);
            }
            break;

        case 'U':
        case 'u':
            if (b->allowVars && strchr(b->allowVars, 'u') != NULL && e && e->logUserBrief != NULL) {
                if (b->eblSecret == NULL) {
                    SendUrlEncodedEncrypted(s, e->logUserBrief, b, 'u');
                } else {
                    char *encryptUser;
                    char *sendUser;
                    char ts[16];
                    EVP_MD_CTX *mdctx = NULL;
                    unsigned char md_value[EVP_MAX_MD_SIZE];
                    unsigned int md_len;
                    char *hex;
                    if (e->logUserBrief) {
                        encryptUser = EncryptVar(e->logUserBrief, b, 'u');
                        sendUser = encryptUser ? encryptUser : e->logUserBrief;

                        if (t->eblSecretTimestamp == 0)
                            UUtime(&t->eblSecretTimestamp);

                        sprintf(ts, "%" PRIdMAX "", (intmax_t)(t->eblSecretTimestamp));

                        if (!(mdctx = EVP_MD_CTX_new()))
                            PANIC;
                        EVP_DigestInit(mdctx, EVP_sha1());
                        EVP_DigestUpdate(mdctx, sendUser, strlen(sendUser));
                        EVP_DigestUpdate(mdctx, ts, strlen(ts));
                        EVP_DigestUpdate(mdctx, b->eblSecret, strlen(b->eblSecret));
                        EVP_DigestFinal(mdctx, md_value, &md_len);
                        hex = DigestToHex(NULL, md_value, md_len, 1);
                        EVP_MD_CTX_free(mdctx);
                        mdctx = NULL;

                        SendUrlEncoded(s, sendUser);
                        if (hex) {
                            WriteLine(s, "&tstamp=%s&id=%s", ts, hex);
                            FreeThenNull(hex);
                        }
                        FreeThenNull(encryptUser);
                    }
                }
            }
            break;

        case 'P':
        case 'p':
            /*
            if (allowUsernamePassword)
                SendUrlEncoded(password);
            */
            break;

        default:
            break;
        }
    }
}

void SendTokenizedURL(struct TRANSLATE *t,
                      struct DATABASE *b,
                      char *url,
                      char **token,
                      char **tokenSignature,
                      BOOL allowUsernamePassword) {
    SendTokenizedURL2(t, &t->ssocket, b, url, token, tokenSignature, allowUsernamePassword);
}
