/**
 * Follet (small ILS) user authentication
 *
 * This authentication method supports common conditions and actions.
 */

#define __UUFILE__ "usrfollett.c"

#include "common.h"

#include "usr.h"
#include "usrfollett.h"

struct FOLLETTCTX {
    char *welcome;
    char *presentLoginForm;
    char *handleLoginForm;
    char *mainForm;
};

int UsrFollettWelcome(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct FOLLETTCTX *follettCtx = (struct FOLLETTCTX *)context;

    ReadConfigSetString(&follettCtx->welcome, arg);
    return 0;
}

int UsrFollettPresentLoginForm(struct TRANSLATE *t,
                               struct USERINFO *uip,
                               void *context,
                               char *arg) {
    struct FOLLETTCTX *follettCtx = (struct FOLLETTCTX *)context;

    ReadConfigSetString(&follettCtx->presentLoginForm, arg);
    return 0;
}

int UsrFollettHandleLoginForm(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct FOLLETTCTX *follettCtx = (struct FOLLETTCTX *)context;

    ReadConfigSetString(&follettCtx->handleLoginForm, arg);
    return 0;
}

int UsrFollettMainForm(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct FOLLETTCTX *follettCtx = (struct FOLLETTCTX *)context;

    ReadConfigSetString(&follettCtx->mainForm, arg);
    return 0;
}

int UsrFollettURL(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    int result = 1;
    char *user, *pass;
    struct UUSOCKET rr, *r = &rr;
    char line[512];
    BOOL header;
    char save;
    struct PROTOCOLDETAIL *protocolDetail;
    char *host;
    char *colon;
    char *endOfHost, *endOfHostPort;
    PORT port;
    char *p;
    char *session = NULL;
    char *redirect = NULL;
    int phase;
    struct FOLLETTCTX *follettCtx = (struct FOLLETTCTX *)context;
    char *welcome = follettCtx->welcome ? follettCtx->welcome : "/";
    char *presentLoginForm = follettCtx->presentLoginForm ? follettCtx->presentLoginForm
                                                          : "/common/servlet/presentloginform.do";
    char *handleLoginForm = follettCtx->handleLoginForm ? follettCtx->handleLoginForm
                                                        : "/common/servlet/handleloginform.do";
    char *mainForm = follettCtx->mainForm ? follettCtx->mainForm : "/common/servlet/main.do";

    UUInitSocket(r, INVALID_SOCKET);

    user = uip->user ? uip->user : "";
    pass = uip->pass ? uip->pass : "";

    if (*user == 0 || strlen(user) > 32 || strlen(pass) > 32)
        goto Finished;

    if (!(ParseProtocolHostPort(arg, "http://", &protocolDetail, &host, &endOfHost, &colon, &port,
                                NULL, &endOfHostPort)))
        goto Finished;

    if (protocolDetail->protocol > PROTOCOLHTTPS)
        goto Finished;

    if (endOfHostPort)
        *endOfHostPort = 0;

    uip->result = resultRefused;

    for (phase = 0; phase < 3; phase++) {
        if (phase > 0 && session == NULL)
            break;

        if (save = *endOfHost)
            *endOfHost = 0;

        if (UUConnectWithSource2(r, host, port, "FindUserFollett", uip->pInterface,
                                 (char)protocolDetail->protocol))
            goto Finished;

        if (save)
            *endOfHost = save;

        if (phase == 0)
            uip->result = resultInvalid;

        if (phase == 2) {
            strcpy(line, "reasonForLogin=&fromLoginLink=true&userLoginName=");
            AddEncodedField(line, user, NULL);
            strcat(line, "&userLoginPassword=");
            AddEncodedField(line, pass, NULL);
            strcat(line, "&submit.x=0&submit.y=0");
        }

        WriteLine(r, "%s ", phase == 2 ? "POST" : "GET");

        if (phase == 0)
            UUSendZ(r, welcome);
        else if (phase == 1)
            WriteLine(r, "%s;jsessionid=%s?fromLoginLink=true", presentLoginForm, session);
        else
            WriteLine(r, "%s", handleLoginForm);

        UUSendZ(r, " HTTP/1.0\r\n");

        save = *endOfHostPort;
        *endOfHostPort = 0;
        WriteLine(r, "Host: %s\r\n", host);
        *endOfHostPort = save;

        if (phase > 0) {
            WriteLine(r, "Cookie: JSESSIONID=%s\r\n", session);
            if (phase == 2) {
                UUSendZ(r, "Content-Type: application/x-www-form-urlencoded\r\n");
                WriteLine(r, "Content-Length: %d\r\n", strlen(line));
            }
        }

        UUSendZ(r, "Connection: close\r\n\r\n");

        if (phase == 2) {
            UUSendZCRLF(r, line);
        }

        header = 1;
        while (ReadLine(r, line, sizeof(line))) {
            if (uip->debug)
                UsrLog(uip, "Follett response %s", line);
            if (header) {
                if (line[0] == 0) {
                    header = 0;
                    continue;
                }
                if (phase == 0) {
                    if (p = StartsIWith(line, "Set-Cookie: ")) {
                        p = SkipWS(p);
                        if (p = StartsIWith(p, "JSESSIONID=")) {
                            FreeThenNull(session);
                            if (!(session = strdup(p)))
                                PANIC;
                            if (p = strchr(session, ';'))
                                *p = 0;
                            if (strlen(session) > 64)
                                *(session + 64) = 0;
                        }
                    }
                    if (p = StartsIWith(line, "Location: ")) {
                        char *redirectEndOfHostPort;
                        p = SkipWS(p);
                        ParseProtocolHostPort(p, "", NULL, NULL, NULL, NULL, NULL, NULL,
                                              &redirectEndOfHostPort);
                        if (redirectEndOfHostPort) {
                            FreeThenNull(redirect);
                            if (!(redirect = strdup(redirectEndOfHostPort)))
                                PANIC;
                        }
                    }
                }
                if (phase == 2) {
                    if (p = StartsIWith(line, "Location: ")) {
                        p = SkipWS(p);
                        if (StrIStr(p, mainForm)) {
                            uip->result = resultValid;
                        }
                    }
                }
            }
        }

        UUStopSocket(r, 0);
    }

Finished:

    FreeThenNull(redirect);
    FreeThenNull(session);

    return result;
}

enum FINDUSERRESULT FindUserFollett(struct TRANSLATE *t,
                                    struct USERINFO *uip,
                                    char *file,
                                    int f,
                                    struct FILEREADLINEBUFFER *frb,
                                    struct sockaddr_storage *pInterface) {
    struct USRDIRECTIVE udc[] = {
        {"Welcome",          UsrFollettWelcome,          0},
        {"PresentLoginForm", UsrFollettPresentLoginForm, 0},
        {"HandleLoginForm",  UsrFollettHandleLoginForm,  0},
        {"MainForm",         UsrFollettMainForm,         0},
        {"URL",              UsrFollettURL,              0},
        {NULL,               NULL,                       0}
    };
    enum FINDUSERRESULT result;
    struct FOLLETTCTX follettCtx;

    memset(&follettCtx, 0, sizeof(follettCtx));

    uip->flags = 0;
    result = UsrHandler(t, "Follett", udc, &follettCtx, uip, file, f, frb, pInterface, NULL);

    FreeThenNull(follettCtx.welcome);
    FreeThenNull(follettCtx.presentLoginForm);
    FreeThenNull(follettCtx.handleLoginForm);
    FreeThenNull(follettCtx.mainForm);

    return result;
}
