#ifndef __SQL_H__
#define __SQL_H__

#include "sqlite3.h"

struct SQLNode {
    struct SQLNode *next;
    unsigned char *element;
};

#define SQLPANIC(_result) SQLPanic(__UUFILE__, __LINE__, result, NULL)
#define SQLPANICMSG(result, errMsg) SQLPanic(__UUFILE__, __LINE__, result, errMsg)
#define SQLOK(x)                        \
    do {                                \
        int _result = (x);              \
        if (_result != SQLITE_OK)       \
            SQLPANICMSG(_result, NULL); \
    } while (FALSE)
#define SQLDONE(x)                      \
    do {                                \
        int _result = (x);              \
        if (_result != SQLITE_DONE)     \
            SQLPANICMSG(_result, NULL); \
    } while (FALSE)
#define SQLOKORDONE(x)                                        \
    do {                                                      \
        int _result = (x);                                    \
        char *_errMsg = NULL;                                 \
        if (_result != SQLITE_OK && _result != SQLITE_DONE) { \
            SQLPANICMSG(_result, _errMsg);                    \
        }                                                     \
        sqlite3_free(_errMsg);                                \
    } while (FALSE)
#define SQLEXEC(db, sql, callback, arg)                               \
    do {                                                              \
        char *_errMsg = NULL;                                         \
        int _result = sqlite3_exec(db, sql, callback, arg, &_errMsg); \
        if (_result != SQLITE_OK) {                                   \
            SQLPANICMSG(_result, _errMsg);                            \
        }                                                             \
        sqlite3_free(_errMsg);                                        \
    } while (FALSE)
#define SQLOPTIONAL(x)                                            \
    do {                                                          \
        int _result = (x);                                        \
        char *_errMsg = NULL;                                     \
        if (_result != SQLITE_OK && _result != SQLITE_NOTFOUND) { \
            SQLPANICMSG(_result, _errMsg);                        \
        }                                                         \
        sqlite3_free(_errMsg);                                    \
    } while (FALSE)

#define SQLNEVER 0x7fffffff
#define SQLEXEMPT 1

void SQLPanic(const char *file, int line, int sqlErrno, const char *errMsg);
void SQLBeginTransaction(sqlite3 *db);
void SQLCommitTransaction(sqlite3 *db);
void SQLRollbackTransaction(sqlite3 *db);
void SQLPrepare(sqlite3 *db, const char *sql, sqlite3_stmt **stmt);
sqlite3_int64 SQLMaxAutoIncrement(sqlite3 *db, char *table);

int SQLBindTextByName(sqlite3_stmt *stmt,
                      const char *name,
                      const char *str,
                      void (*destructor)(void *));
int SQLBindIntByName(sqlite3_stmt *stmt, const char *name, int val);
int SQLBindInt64ByName(sqlite3_stmt *stmt, const char *name, sqlite3_int64 val);
int SQLBindNullByName(sqlite3_stmt *stmt, const char *name);

struct SQLNode *SQLGetTable(sqlite3_stmt *stmt, int *rows, int *cols);
void SQLFreeTable(struct SQLNode **table);

#endif
