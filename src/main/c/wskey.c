#define __UUFILE__ "wskey.c"

#include "wskey.h"
#include "ezproxyversion.h"

#define WSKEY_USER_AGENT_VERSION EZPROXYNAMEPROPER "/" EZPROXYVERSIONNUMBER

#if EZPROXYRELEASELEVEL == 1
#define WSKEY_USER_AGENT_SUBVERSION "-BETA"
#else
#define WSKEY_USER_AGENT_SUBVERSION ""
#endif

#ifdef WIN32
#define WSKEY_USER_AGENT_OS "Windows"
#else
#define WSKEY_USER_AGENT_OS "Linux"
#endif

#define WSKEY_USER_AGENT_ARCH "x86_64"

#define WSKEY_USER_AGENT                                                          \
    WSKEY_USER_AGENT_VERSION WSKEY_USER_AGENT_SUBVERSION " (" WSKEY_USER_AGENT_OS \
                                                         "; " WSKEY_USER_AGENT_ARCH ")"

// prototypes for static functions
static enum WSKEY_STATES WSkeySetSignature(xmlXPathContextPtr xpathCtx,
                                           struct WSKEYLICENSE *license);
static enum WSKEY_STATES WSkeyParseStatus(char *xmlMsg,
                                          int contentLength,
                                          struct WSKEYLICENSE *license);
static BOOL WSKeyConnectTunnelSSL(struct UUSOCKET *s,
                                  struct PROXY *proxy,
                                  char const *host,
                                  const PORT port);
static BOOL WSKeyConnectProxy(struct UUSOCKET *s,
                              const char *host,
                              const PORT port,
                              const BOOL ssl,
                              struct PROXY *proxy);
static enum WSKEY_STATES WSKeyVerifyRead(struct UUSOCKET *s, struct WSKEYLICENSE *license);
static void WSKeySendBaseRequest(struct UUSOCKET *s,
                                 const char *host,
                                 const PORT port,
                                 const struct WSKEYLICENSE *license,
                                 const BOOL ssl);

/**
 * Get signature node from WSKEY xml and verify the signature
 * signature = Base64Encode(RSAencrypt(SHA1(nonce+"SUCCESSAUTHORIZED")))
 *
 * so to check the signature...
 * 1. sig = Base64Decode(signature)
 * 2. hash = SHA1(nonce+"SUCCESSAUTHORIZED")
 * 3. RSAverify(sig, hash, pubKey)
 */
static enum WSKEY_STATES WSkeySetSignature(xmlXPathContextPtr xpathCtx,
                                           struct WSKEYLICENSE *license) {
    xmlXPathObjectPtr xpathObj;
    xmlChar *path = "/authResponse/authorizeResponse/signature/text()";
    xpathObj = xmlXPathEvalExpression(path, xpathCtx);

    if (xpathObj == NULL)
        return WSKEY_NOSIG;

    xmlNodeSetPtr nodes = xpathObj->nodesetval;
    int size = (nodes) ? nodes->nodeNr : 0;

    if (size == 0)
        return WSKEY_NOSIG;

    unsigned char *signature = nodes->nodeTab[0]->content;

    // store the signature into the license
    strncpy(license->signature, (char *)signature, license->sigSize);

    return WSKEY_SIG_SET;
}

/**
 * Parse the wskey server xml and return the state
 */
static enum WSKEY_STATES WSkeyParseStatus(char *xmlMsg,
                                          int contentLength,
                                          struct WSKEYLICENSE *license) {
    char *authorized = "AUTHORIZED";
    enum WSKEY_STATES result = WSKEY_INIT;

    xmlDocPtr xmlDoc = xmlParseMemory(xmlMsg, contentLength);
    if (xmlDoc == NULL) {
        return WSKEY_BADXML;
    }

    xmlXPathContextPtr xpathCtx = xmlXPathNewContext(xmlDoc);
    if (xpathCtx == NULL) {
        xmlFreeDoc(xmlDoc);
        return WSKEY_BADXML;
    }

    xmlXPathObjectPtr xpathObj;
    xmlChar *path = "/authResponse/authorizeResponse/status/text()";
    xpathObj = xmlXPathEvalExpression(path, xpathCtx);
    if (xpathObj == NULL) {
        xmlFreeDoc(xmlDoc);
        return WSKEY_BADXPATH;
    }

    xmlNodeSetPtr nodes = xpathObj->nodesetval;
    int size = (nodes) ? nodes->nodeNr : 0;

    if (size == 0) {
        result = WSKEY_BADXPATH;
    } else {
        result = WSKEY_UNAUTHORIZED;
        char *status = (char *)nodes->nodeTab[0]->content;
        if (strlen(status) == strlen(authorized) &&
            strnicmp(status, authorized, strlen(authorized)) == 0) {
            // set the signature
            result = WSkeySetSignature(xpathCtx, license);
            licenseValid = TRUE;
        } else if (debugLevel) {
            Log("Wskey was not %s.  Status was %s.", authorized, nodes->nodeTab[0]->content);
            licenseValid = FALSE;
        }
    }

    /* Cleanup */
    xmlXPathFreeObject(xpathObj);
    xmlXPathFreeContext(xpathCtx);
    xmlFreeDoc(xmlDoc);

    return result;
}

/**
 * Make tunneled SSL connection to host
 */
static BOOL WSKeyConnectTunnelSSL(struct UUSOCKET *s,
                                  struct PROXY *proxy,
                                  const char *host,
                                  const PORT port) {
    WriteLine(s, "CONNECT %s:%d HTTP/1.0\r\n", host, port);
    WriteLine(s, "Host: %s:%d\r\n", host, port);

    if (proxy->auth != NULL)
        WriteLine(s, "Proxy-Authorization: basic %s\r\n", proxy->auth);

    // end of header
    UUSendCRLF(s);

    char line[4096];
    line[sizeof(line) - 1] = 1;

    while (ReadLine(s, line, sizeof(line))) {
        if (line[sizeof(line) - 1] != 1) {
            if (debugLevel)
                Log("ProxySSL connection sent a line longer than %d, abort", (sizeof(line) - 1));
            return FALSE;
        }

        if (line[0] == 0)
            break;

        if (strnicmp(line, "HTTP/", 5) == 0) {
            char *arg = SkipNonWS(SkipWS(line));
            int result = atoi(arg);

            if (result < 200 || result > 299) {
                if (debugLevel)
                    Log("ProxySSL tunnel connection refused: %s", line);

                return FALSE;
            }
        }
        line[sizeof(line) - 1] = 1;
    }

    return TRUE;
}
/**
 * Connect through proxy and send initial request headers
 */
static BOOL WSKeyConnectProxy(struct UUSOCKET *s,
                              const char *host,
                              const PORT port,
                              const BOOL ssl,
                              struct PROXY *proxy) {
    if (debugLevel)
        Log("WSkey attempting to use proxy for connection: %s", proxy->host);

    // setup socket connection with proxy
    int error = -1;
    error = UUConnect2(s, proxy->host, proxy->port, WSKEY_FACILITY " (Proxied)", 0);

    // connection error
    if (error != 0) {
        if (debugLevel)
            Log("Proxy connection failed for %s:%d", proxy->host, proxy->port);
        return FALSE;
    }

    // setup secure connection if needed with destination host, though the proxy
    if (ssl && !WSKeyConnectTunnelSSL(s, proxy, host, port)) {
        if (debugLevel)
            Log("Failed to tunnel wskey service connection.");
        return FALSE;
    }

    // switch connection to ssl if proxy and tunneling
    if (ssl) {
        UUInitSocketSsl(s, s->o, UUSSLCONNECT, LICENSE_SERVER_CERT_IDX, host);
        if (s->ssl == NULL) {
            if (debugLevel)
                Log("WSkey license check unable to negotiate secure connection with proxy.");
            return FALSE;
        }
    }

    return TRUE;
}

/**
 * Read response from server.  A request should have already been written
 * to socket.
 */
static enum WSKEY_STATES WSKeyVerifyRead(struct UUSOCKET *s, struct WSKEYLICENSE *license) {
    int responseCode = 0;
    int contentLength = HTTPHeaderParse(s, &responseCode);

    // bad response code
    if (responseCode != 200)
        return WSKEY_NOT200;

    int maxMsgLen = contentLength > 0 ? contentLength + 1 : 2056;
    char xmlMsg[maxMsgLen];

    HTTPBodyParse(xmlMsg, maxMsgLen, &contentLength, s);
    xmlMsg[contentLength] = '\0';

    if (debugLevel > 9000)
        Log("Wskey response message:\n%s", xmlMsg);

    return WSkeyParseStatus(xmlMsg, contentLength + 1, license);
}

static void WSKeySendBaseRequest(struct UUSOCKET *s,
                                 const char *host,
                                 const PORT port,
                                 const struct WSKEYLICENSE *license,
                                 const BOOL ssl) {
    const char *origin;

    if (debugLevel) {
        Log("Using SSL: %s", (ssl ? "TRUE" : "FALSE"));
        Log("WSKEY Request w/ license: %s", license->key);
        Log("WSKEY Request w/ nonce  : %s", license->nonce);
    }

    // send request to wskey server
    UUSendZ(s, "GET ");
    if (!ssl) {
        // only write entire host and port when not in a tunnel, aka destination host is not SSL
        WriteLine(s, "http%s://%s", ((ssl) ? "s" : ""), host);
        if ((port == 443 && !ssl) || (port == 80 && ssl) || (port != 80 && port != 443)) {
            WriteLine(s, ":%d", port);
        }
    }
    WriteLine(s, "/wskey/authorize.action?service=ezproxy&wskey=%s HTTP/1.0\r\n", license->key);

    // send host information, wsey service does not like port numbers.
    // so only send if necessary
    WriteLine(s, "Host: %s", host);
    if (!ssl && ((port == 443 && !ssl) || (port == 80 && ssl) || (port != 80 && port != 443))) {
        WriteLine(s, ":%d", port);
    }
    UUSendZ(s, "\r\n");

    UUSendZ(s, "Pragma: no-cache\r\n");
    UUSendZ(s, "User-Agent: " WSKEY_USER_AGENT "\r\n");
    // Test if an origin is available. This specifically covers the case
    // of command line use of the -k option for which the server origin
    // is not yet known.
    origin = myUrlHttps ? myUrlHttps : (myUrlHttp ? myUrlHttp : "unavailable");
    WriteLine(s, "Origin: %s\r\n", origin);
    UUSendZ(s, "Accept: */*\r\n");
    UUSendZ(s, "Connection: close\r\n");
    UUSendZ(s, "Cache-Control: no-cache\r\n");
    WriteLine(s, "nonce: %s\r\n", license->nonce);
}

/**
 * Send non proxied HTTP request
 */
enum WSKEY_STATES WSKeyVerify(struct UUSOCKET *s,
                              const char *host,
                              const PORT port,
                              const BOOL ssl,
                              struct WSKEYLICENSE *license) {
    WSKeySendBaseRequest(s, host, port, license, ssl);
    UUSendCRLF(s);
    UUFlush(s);

    return WSKeyVerifyRead(s, license);
}

/**
 * Cycle through each proxy and attempt to validate licence.  Only quit
 * when out of proxies or (WSKEY_AUTHORIZED or WSKEY_UNAUTHORIZED or WSKEY_SIG_SET)
 */
enum WSKEY_STATES WSKeyVerifyUsingProxies(struct UUSOCKET *s,
                                          const char *host,
                                          const PORT port,
                                          const BOOL ssl,
                                          struct WSKEYLICENSE *license) {
    enum WSKEY_STATES keystate = WSKEY_SOCKETERR;
    BOOL success = FALSE;

    struct PROXY *proxy = wsKeyProxies;

    if (debugLevel) {
        if (proxy == NULL)
            Log("No proxy connections defined.");
        else
            Log("Attempting proxy connections for license validation");
    }

    while (!success && proxy != NULL) {
        success = WSKeyConnectProxy(s, host, port, ssl, proxy);
        if (!success) {
            if (debugLevel)
                Log("Failed to connect with wskey server(host=%s).", host);
            proxy = proxy->next;
            continue;
        }

        // send basic headers and nonce
        WSKeySendBaseRequest(s, host, port, license, ssl);

        if (proxy->auth != NULL && !ssl)
            WriteLine(s, "Proxy-Authorization: basic %s\r\n", proxy->auth);

        // send end of header
        UUSendCRLF(s);

        keystate = WSKeyVerifyRead(s, license);

        if (keystate == WSKEY_AUTHORIZED || keystate == WSKEY_UNAUTHORIZED ||
            keystate == WSKEY_SIG_SET)
            break;

        if (debugLevel)
            Log("Successfully connected through proxy, but XML verification failed.");

        // didnt get a valid state, keep trying
        proxy = proxy->next;
    }

    return keystate;
}
