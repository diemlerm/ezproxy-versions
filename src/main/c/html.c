#define __UUFILE__ "html.c"

#include "common.h"

#ifdef WIN32
/* io.h, fcntl.h and share.h needed for _sopen in Win32 */
#include <io.h>
#include <fcntl.h>
#include <share.h>
#include <lm.h>
#endif

#include <sys/stat.h>

#include "usr.h"

#include "uuxml.h"

#include "html.h"

void GetHTMLFreeCookies(struct COOKIE **cookies) {
    struct COOKIE *k;

    if (cookies) {
        for (k = *cookies; k; k = ClusterTossCookie(k))
            ;
        *cookies = NULL;
    }
}

struct COOKIE *GetHTMLAddCookie(struct COOKIE **cookies, char *cookie, char *host) {
    struct COOKIE *k, *n, *l;
    char *equals;
    size_t len;
    char *save;
    char *semi;
    char *arg;
    time_t now, expires;
    char localDomain[512];
#define SDCDOMAIN 0
#define SDCPATH 1
#define SDCEXPIRES 2
    struct {
        char *name;
        char *ptr;
    } *fps, fields[] = {"domain", NULL, "path", NULL, "expires", NULL, NULL, NULL};

    if (cookies == NULL)
        return NULL;

    for (semi = cookie; semi = strchr(semi, ';');) {
        *semi++ = 0;
        semi = SkipWS(semi);
        for (fps = fields; fps->name; fps++) {
            len = strlen(fps->name);
            if (strnicmp(semi, fps->name, len) != 0)
                continue;
            arg = semi + len;
            if (*arg != '=')
                continue;
            fps->ptr = SkipWS(arg + 1);
            /* RFC 2965 introduces the possibility that domain won't have a leading period, and
               should have one automagically appended It's OK to change things at this point since a
               domain was found and this routine will definitely deal with the consequences
            */
            if (strcmp(fps->name, "domain") == 0 && *(fps->ptr) != '.') {
                fps->ptr = fps->ptr - 1;
                *(fps->ptr) = '.';
            }
        }
    }

    if (fields[SDCDOMAIN].ptr == NULL) {
        StrCpy3(localDomain, host, sizeof(localDomain));
        /* I thought the port should be there, but alas, IE will send anyhting to any port */
        /* sprintf(strchr(localDomain, 0), ":%d", (t->host)->remport); */
        fields[SDCDOMAIN].ptr = localDomain;
    }

    if (fields[SDCPATH].ptr == NULL)
        fields[SDCPATH].ptr = "/";

    Trim(cookie, TRIM_LEAD | TRIM_TRAIL);

    for (fps = fields; fps->name; fps++) {
        if (fps->ptr)
            Trim(fps->ptr, TRIM_LEAD | TRIM_TRAIL);
    }

    /*  printf("Hey, that's a domain cookie so let's store it\n"); */
    /* Check to see if it's in the correct domain; if not, reject */
    /*
    if (! HostInDomain((t->host)->hostname, (t->host)->remport, fields[SDCDOMAIN].ptr)) {
         printf("Wrong domain, reject cookie\n");
         return 1;
    }
    */

    UUtime(&now);
    expires = ParseHttpDate(fields[SDCEXPIRES].ptr, now + TWO_DAYS_IN_SECONDS);
    if (expires < now)
        expires = 0;

    equals = strchr(cookie, '=');
    if (equals == NULL)
        len = strlen(cookie);
    else
        len = equals - cookie;

    l = NULL;
    for (k = *cookies; k; k = k->next) {
        l = k;
        if (stricmp(k->domain, fields[SDCDOMAIN].ptr) != 0)
            continue;
        if (strncmp(k->name, cookie, len) != 0)
            continue;
        if (strcmp(k->path, fields[SDCPATH].ptr) != 0)
            continue;
        /* If the cookie to set is just a label, then we match to only the label */
        if (equals == NULL && strlen(k->name) != len)
            continue;
        /* We're just updating the cookie */
        k->expires = expires;
        save = k->name;
        if (!(k->name = strdup(cookie)))
            PANIC;
        FreeThenNull(save);
        goto Done;
    }
    /* If this cookie doesn't exist, and expires is 0, this is a delete, so we don't do anything */
    if (expires == 0)
        goto Done;

    if (!(n = calloc(1, sizeof(struct COOKIE))))
        PANIC;
    if (!(n->name = strdup(cookie)))
        PANIC;
    if (!(n->domain = strdup(fields[SDCDOMAIN].ptr)))
        PANIC;
    if (!(n->path = strdup(fields[SDCPATH].ptr)))
        PANIC;
    n->expires = expires;

    n->next = NULL;
    if (l == NULL)
        *cookies = n;
    else
        l->next = n;
    k = n;

Done:
    return k;
}

char *AbsoluteURL(char *baseURL, char *relativeURL) {
    char *absoluteURL = NULL;
    struct PROTOCOLDETAIL *protocol;
    char *host;
    char *endOfHostPort;
    size_t len;
    BOOL addSlash = 0;

    if (StartsIWith(relativeURL, "http:") || StartsIWith(relativeURL, "https:")) {
        if (!(absoluteURL = strdup(relativeURL)))
            PANIC;
        return absoluteURL;
    }

    if (ParseProtocolHostPort(baseURL, NULL, &protocol, &host, NULL, NULL, NULL, NULL,
                              &endOfHostPort))
        return NULL;

    if (StartsIWith(relativeURL, "//")) {
        if (!(absoluteURL = malloc(protocol->nameLen + strlen(relativeURL) + 1)))
            PANIC;
        sprintf(absoluteURL, "%s%s", protocol->name, relativeURL);
        return absoluteURL;
    }

    if (StartsIWith(relativeURL, "/")) {
        len = endOfHostPort - baseURL;
    } else {
        char *lastSlash = NULL;
        char *p;

        for (p = SkipHTTPslashesAt(baseURL, NULL); *p; p++) {
            if (*p == '?')
                break;
            if (*p == '/')
                lastSlash = p;
        }

        /* At this point, *p either points at ? or 0, and either way, that's end of URL */

        if (lastSlash == NULL) {
            len = p - baseURL;
        } else {
            len = lastSlash - baseURL;
        }

        addSlash = 1;
    }

    if (!(absoluteURL = malloc(len + addSlash + strlen(relativeURL) + 1)))
        PANIC;
    memcpy(absoluteURL, baseURL, len);
    if (addSlash) {
        *(absoluteURL + len) = '/';
        len++;
    }
    strcpy(absoluteURL + len, relativeURL);

    return absoluteURL;
}

htmlParserCtxtPtr GetHTML(char *url,
                          char *post,
                          struct COOKIE **cookies,
                          char **finalURL,
                          struct sockaddr_storage *pInterface,
                          int flags,
                          int sslIdx,
                          BOOL *ssl,
                          char **peerCN,
                          BOOL debug) {
    struct UUSOCKET rr, *r = &rr;
    BOOL inHeader;
    char *p;
    char line[8192];
    htmlParserCtxtPtr ctxt = NULL;
    struct PROTOCOLDETAIL *protocol;
    char *host, *endOfHost, *colon, *endOfHostPort;
    PORT port;
    BOOL isDefaultPort;
    char *urlCopy = NULL;
    char saveEndOfHost = 0;
    char saveEndOfHostPort = 0;
    char *location = NULL;
    int redirects = 0;
    char *contLine;
    char *endLine = line + sizeof(line) - 1;
    char save = 0;
    int len;
    BOOL getXml = (flags & GETHTMLACTUALLYXML) != 0;

    if (finalURL) {
        *finalURL = NULL;
    }

    if (peerCN) {
        *peerCN = NULL;
    }

    if (ssl) {
        *ssl = FALSE;
    }

    do {
        UUInitSocket(r, INVALID_SOCKET);

        /* If location is set, this is a second or subsequent attempt through redirect */
        if (location) {
            if (flags & GETHTMLNOFOLLOWREDIRECTS) {
                if (debug) {
                    Log("Request to redirect to %s but NoFollowRedirects in place", location);
                }
                FreeThenNull(location);
                break;
            }
            if (debug)
                Log("Redirecting to %s", location);
            FreeThenNull(urlCopy);
            UUxmlFreeParserCtxtAndDoc(&ctxt);
            urlCopy = location;
            location = NULL;
            redirects++;
            if (redirects > 10) {
                Log("Exceeded redirect limit trying to retrieve %s", url);
                break;
            }
        } else {
            if (!(urlCopy = strdup(url)))
                PANIC;
        }

        UUxmlFreeParserCtxtAndDoc(&ctxt);

        if (finalURL) {
            FreeThenNull(*finalURL);
            if (!(*finalURL = strdup(urlCopy)))
                PANIC;
        }

        ParseProtocolHostPort(urlCopy, NULL, &protocol, &host, &endOfHost, &colon, &port,
                              &isDefaultPort, &endOfHostPort);

        if (protocol->protocol && UUSslEnabled() == 0) {
            Log("Dynamic form requiring https failed since SSL is disabled");
            goto Finished;
        }

        if (endOfHost) {
            saveEndOfHost = *endOfHost;
        }

        if (endOfHostPort) {
            saveEndOfHostPort = *endOfHostPort;
        }

        if (endOfHost)
            *endOfHost = 0;

        if (UUConnectWithSource3(r, host, port, "GetHTML", pInterface, protocol->protocol,
                                 sslIdx)) {
            Log("Dynamic form failed to connect to %s:%d", host, port);
            goto Finished;
        }

        if (ssl) {
            *ssl = protocol->protocol == PROTOCOLHTTPS;
        }

        if (peerCN) {
            X509 *cert = NULL;

            FreeThenNull(*peerCN);
            if (SSL_get_verify_result(r->ssl) == X509_V_OK) {
                if ((cert = SSL_get_peer_certificate(r->ssl)) != NULL) {
                    if (!(*peerCN = malloc(257)))
                        PANIC;
                    X509_NAME_get_text_by_NID(X509_get_subject_name(cert), NID_commonName, *peerCN,
                                              256);
                    X509_free(cert);
                }
            }
        }

        if (post && *post) {
            UUSendZ(r, "POST ");
        } else {
            UUSendZ(r, "GET ");
        }

        if (endOfHostPort) {
            /* in case endOfHost and endOfHostPort are the same, restore saved value */
            *endOfHostPort = saveEndOfHostPort;
            UUSendZ(r, endOfHostPort);
        } else {
            UUSendZ(r, "/");
        }

        if (endOfHost)
            *endOfHost = 0;

        WriteLine(r,
                  " HTTP/1.0\r\n\
Host: %s",
                  host);

        if (protocol->defaultPort != port)
            WriteLine(r, ":%d", port);

        UUSendZ(r,
                "\r\n\
Pragma: no-cache\r\n\
User-Agent: Mozilla/4.0 (compatible; EZproxy)\r\n\
Accept: */*\r\n");

        if (cookies) {
            struct COOKIE *k;
            BOOL firstCookie = 1;
            for (k = *cookies; k; k = k->next) {
                if (firstCookie) {
                    UUSendZ(r, "Cookie: ");
                    firstCookie = 0;
                } else {
                    UUSendZ(r, "; ");
                }
                UUSendZ(r, k->name);
            }
            if (firstCookie == 0)
                UUSendCRLF(r);
        }

        if (post && *post) {
            if (flags & GETHTMLPOSTXML) {
                WriteLine(r, "Content-Type: text/xml\r\n");
                if (flags & GETHTMLPOSTSOAPSECURITY)
                    WriteLine(r, "SOAPAction: http://www.oasis-open.org/committees/security\r\n");
            } else {
                WriteLine(r, "Content-Type: application/x-www-form-urlencoded\r\n");
            }

            WriteLine(r, "Content-Length: %d\r\n", strlen(post));
        }

        UUSendZ(r,
                "Connection: close\r\n\
Cache-Control: no-cache\r\n\
\r\n");

        if (post && *post)
            UUSendZ(r, post);

        if (debug)
            Log("The response");

        inHeader = 1;
        contLine = line;
        while (inHeader && ReadLine(r, contLine, endLine - contLine) > 0) {
            if (debug)
                Log("%s", line);
            if (inHeader) {
                if (*contLine == 0)
                    inHeader = 0;
                else {
                    if (IsWS(*contLine)) {
                        contLine = AtEnd(contLine);
                        continue;
                    }
                }
                if (line != contLine) {
                    save = *contLine;
                    *contLine = 0;
                }
                if (p = StartsIWith(line, "Set-Cookie:")) {
                    /* Insure host is just host for cookie manipulation */
                    if (endOfHost)
                        *endOfHost = 0;
                    GetHTMLAddCookie(cookies, p, host);
                    if (endOfHost)
                        *endOfHost = saveEndOfHost;
                }
                if (p = StartsIWith(line, "Location:")) {
                    p = SkipWS(p);
                    if (location == NULL)
                        location = AbsoluteURL(urlCopy, p);
                }
                if (line != contLine) {
                    *contLine = save;
                    StrCpyOverlap(line, contLine);
                }
                continue;
            }
        }

        while ((len = UURecv(r, line, sizeof(line), 0)) > 0) {
            if (ctxt == NULL) {
                if (getXml)
                    ctxt = xmlCreatePushParserCtxt(NULL, NULL, NULL, 0, NULL);
                else
                    ctxt =
                        htmlCreatePushParserCtxt(NULL, NULL, NULL, 0, url, XML_CHAR_ENCODING_NONE);
                if (ctxt == NULL) {
                    Log("Unable to create html context");
                    PANIC;
                }
                if (!getXml)
                    htmlCtxtUseOptions(ctxt, HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING);
            }
            if (getXml)
                xmlParseChunk(ctxt, line, len, 0);
            else
                htmlParseChunk(ctxt, line, len, 0);
            if (debug) {
                char *str;
                char *token;
                for (str = NULL, token = StrTok_R(line, "\r\n", &str); token;
                     token = StrTok_R(NULL, "\r\n", &str)) {
                    if (*token)
                        Log("%s", token);
                }
            }
        }

        UUStopSocket(r, 0);

        if (ctxt) {
            if (getXml)
                xmlParseChunk(ctxt, line, 0, 1);
            else
                htmlParseChunk(ctxt, line, 0, 1);
        }

        if (location == NULL && ctxt && ctxt->myDoc) {
            if (p = (char *)UUxmlSimpleGetContent(ctxt->myDoc, NULL,
                                                  "//meta[@http-equiv = 'refresh']/@content")) {
                while (IsWS(*p) || IsDigit(*p) || *p == ';')
                    p++;
                location = AbsoluteURL(urlCopy, p);
                FreeThenNull(p);
            }
        }
    } while (location != NULL);

Finished:
    UUStopSocket(r, 0);
    FreeThenNull(urlCopy);
    FreeThenNull(location);
    if (ctxt == NULL && debug) {
        Log("Web server did not respond for %s.", url);
    }
    return ctxt;
}
