#define __UUFILE__ "security.c"

#include "common.h"
#include "adminsecurity.h"
#include "environment.h"
#include "security.h"
#include "sql.h"
#include "queue.h"
#include "adminaudit.h"
#include "identifier.h"
#include <sys/stat.h>

char *builtinFilename = "built-in";
char *days[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

int securityEvidenceRetention = 0;
int securityResolvedRetention = 0;
static char *securityEvidenceRetentionFile = NULL;
static char *securityResolvedRetentionFile = NULL;
static int securityEvidenceRetentionLineno = 0;
static int securityResolvedRetentionLineno = 0;
static time_t lastCleaned = 0;
int securityVacuumDay = -1;
static char *securityVacuumDayFile = NULL;
static int securityVacuumDayLineno = 0;
int securityPurgeHour = -1;
int securityPurgeMin = -1;
static char *securityPurgeTimeFile = NULL;
static int securityPurgeTimeLineno = 0;
time_t securityNextPurge = 0;
const char *securityVacuumDayName = "Off";
time_t securityPurgeStarted = 0;
time_t securityVacuumStarted = 0;
BOOL securityForcePurge = FALSE;
BOOL securityForceVacuum = FALSE;

// This connection should not be shared. It is separate to ensure its values
// for changes is restricted just to changes happening in here.
static sqlite3 *db = NULL;
// This separate connection can be shared by multiple threads.
static sqlite3 *sharedDb = NULL;

// The user field must be forced to lowercase.
// Could use COLLATE NOCASE but avoiding to sidestep any performance penalty.
static const char *startupCommands[] = {
        "PRAGMA foreign_keys=ON",

        "CREATE TABLE IF NOT EXISTS registry("
            "key TEXT NOT NULL PRIMARY KEY,"
            "value TEXT"
            ")",

        "CREATE TABLE IF NOT EXISTS rule("
            "id INTEGER PRIMARY KEY," // alias for ROWID
            "at INTEGER NOT NULL,"
            "rulename TEXT NOT NULL,"
            "active INTEGER NOT NULL,"
            "file TEXT NOT NULL,"
            "lineno INT NOT NULL,"
            "criterion TEXT NOT NULL,"
            "boundary INT NOT NULL,"
            "period INT NOT NULL,"
            "action TEXT NOT NULL,"
            "duration INT NOT NULL,"
            "facttable TEXT NOT NULL,"
            "extraconstraint TEXT NOT NULL DEFAULT '',"
            "extraconstraintvalid INT NOT NULL DEFAULT 1"
            ")",

        "CREATE TABLE IF NOT EXISTS login("
                "id INTEGER PRIMARY KEY," // alias for ROWID
                "at INT NOT NULL,"
                "user TEXT NOT NULL,"
                "session TEXT,"
                "ip TEXT NOT NULL,"
                "country TEXT,"
                "region TEXT,"
                "city TEXT,"
                "status TEXT,"
                "network TEXT NOT NULL"
                ")",

        "CREATE TABLE IF NOT EXISTS request("
            "id INTEGER PRIMARY KEY," // alias for ROWID
            "at INT NOT NULL,"
            "user TEXT,"
            "session TEXT,"
            "ip TEXT,"
            "country TEXT,"
            "region TEXT,"
            "city TEXT,"
            "origin TEXT NOT NULL,"
            "path TEXT NOT NULL,"
            "content_type TEXT NOT NULL,"
            "status INT NOT NULL,"
            "transferred INT NOT NULL,"
            "network TEXT NOT NULL"
            ")",

        "CREATE INDEX IF NOT EXISTS"
            " idx_request_at_user_transferred"
            " ON request(at, user, transferred)",

        "CREATE INDEX IF NOT EXISTS"
            " idx_request_content_type_at_user"
            " ON request(content_type, at, user)",

        // We use AUTOINCREMENT on tripped as the id values are going to be stored to the audit file
        // so we can't allow their reuse.
        "CREATE TABLE IF NOT EXISTS tripped("
            "id INTEGER PRIMARY KEY AUTOINCREMENT," // alias for ROWID
            "at INT NOT NULL,"
            "ruleid INTEGER NOT NULL,"
            "user TEXT NOT NULL,"
            "expires INT NOT NULL," // 0 log, 0x7fffffff never, else epoch time that block ends
            "value INT NOT NULL,"
            "FOREIGN KEY(ruleid)"
                " REFERENCES rule(id)"
                " ON DELETE RESTRICT"
                " ON UPDATE RESTRICT"
            ")",

        "CREATE INDEX IF NOT EXISTS"
            " idx_tripped_ruleid_user"
            " ON tripped(ruleid, user)",

        "CREATE TABLE IF NOT EXISTS evidence("
            "trippedid INTEGER NOT NULL,"
            "factid INTEGER NOT NULL,"
            "FOREIGN KEY(trippedid)"
                " REFERENCES tripped(id)"
                " ON DELETE CASCADE"
                " DEFERRABLE INITIALLY DEFERRED"
            ")",

        "CREATE INDEX IF NOT EXISTS"
            " idx_evidence_factid"
            " ON evidence(factid)",

        "CREATE TABLE IF NOT EXISTS exemption("
            "user TEXT NOT NULL PRIMARY KEY,"
            "at INT NOT NULL DEFAULT (strftime('%s', 'now')),"
            "expires INT NOT NULL DEFAULT " mkstr(SQLNEVER) ","
            "applied_at INT,"
            "applied INT NOT NULL DEFAULT 0,"
            "comment TEXT"
            ")",

        NULL
};

static const int maxAllowedPeriod = THIRTY_DAYS_IN_SECONDS;

struct SECURITY_RULE {
    char *name;
    int period;
    int expiration;
};

struct SECURITY_LOGIN {
    time_t accessed;
    struct sockaddr_storage sin_addr;
    char *user;
    char *session;
    char *status_s;
};

// Fields ending _s are static and do not need to be free'd
struct SECURITY_REQUEST {
    struct SECURITY_REQUEST *next;
    time_t accessed;
    char *ip;
    char *network;
    char *user;
    char *session;
    char *origin;
    char *path;
    char *contentType;
    int status;
    sqlite3_int64 transferred;
    char *country;
    char *region;
    char *city;
};

struct SECURITY_NODE {
    struct SECURITY_LOGIN *login;
    struct SECURITY_REQUEST *request;
    BOOL updateBlocked;
};

struct QUEUE *securityQueue = NULL;

struct TRIGGER {
    char *facttable;
    char *criterion;
    char *value;
    char *constraint;
};

struct TRIGGER triggers[] = {
    {"request", "bytes_transferred",     "SUM(transferred)",               ""                      },
    {"request", "pdf_download",          "COUNT(DISTINCT origin || path)",
     "AND content_type = 'application/pdf'"                                                        },
    {"login",   "login_failure",         "COUNT(*)",                       "AND status = 'failure'"},
    {"login",   "login_success",         "COUNT(*)",                       "AND status = 'success'"},
    {"login",   "login_relogin",         "COUNT(*)",                       "AND status = 'relogin'"},
    {"request", "country",               "COUNT(DISTINCT country)",        ""                      },
    {"request", "ip_address",            "COUNT(DISTINCT ip)",             ""                      },
    {"request", "network_address",       "COUNT(DISTINCT network)",        ""                      },
    {"request", "pdf_bytes_transferred", "SUM(transferred)",
     "AND content_type = 'application/pdf'"                                                        },
    {NULL,      NULL,                    NULL,                             NULL                    }
};

// Keep data for at least an hour, or more as rule specifies
static int maxRequestPeriod = 3600;
static int maxLoginPeriod = 3600;

struct UUAVLTREE avlBlocked;
struct RWL rwlBlocked;

static char *SnipField(char *s, int start, int stop) {
    int len = stop - start;
    char *n;
    if (!(n = malloc(len + 1)))
        PANIC;
    memcpy(n, s + start, len);
    n[len] = 0;
    return n;
}

static void SecurityNetwork(sqlite3_context *context, int argc, sqlite3_value **argv) {
    if (argc == 1) {
        const unsigned char *value = sqlite3_value_text(argv[0]);
        if (value) {
            char result[strlen(value) + 1];
            strcpy(result, value);
            char *period = strrchr(result, '.');
            if (period && *(period + 1)) {
                strcpy(period + 1, "0");
            }
            sqlite3_result_text(context, result, -1, SQLITE_TRANSIENT);
        } else {
            sqlite3_result_null(context);
        }
    } else {
        sqlite3_result_null(context);
    }
}

static void SecurityKMGT(sqlite3_context *context, int argc, sqlite3_value **argv) {
    char sign[2];
    char *suffix;
    char *suffixes = "PTGMK";
    sqlite3_int64 divisor = 1000000000000000;

    if (argc == 1) {
        sqlite3_int64 value = sqlite3_value_int64(argv[0]);
        char result[24];
        char sign;
        if (value < 0) {
            sign = '-';
            value = -value;
        } else {
            sign = ' ';
        }
        for (suffix = suffixes;; suffix++, divisor /= 1000) {
            if (*suffix == 0) {
                snprintf(result, sizeof(result), " %" PRIdMAX, value);
                break;
            }
            if (value >= divisor) {
                value = value * 10 / divisor;
                snprintf(result, sizeof(result), " %" PRIdMAX ".%d%c", value / 10, value % 10,
                         *suffix);
                break;
            }
        }
        result[0] = sign;
        Trim(result, TRIM_LEAD);
        sqlite3_result_text(context, result, -1, SQLITE_TRANSIENT);
    } else {
        sqlite3_result_null(context);
    }
}

static void SecurityNEVER(sqlite3_context *context, int argc, sqlite3_value **argv) {
    sqlite3_result_int(context, SQLNEVER);
}

static void SecurityEXEMPT(sqlite3_context *context, int argc, sqlite3_value **argv) {
    sqlite3_result_int(context, SQLEXEMPT);
}

/**
 * Extract FQDN and create lower case reverse DNS version with a trailing period.
 */
static void SecurityReverseDNS(sqlite3_context *context, int argc, sqlite3_value **argv) {
    BOOL failed = TRUE;

    if (argc == 1) {
        const unsigned char *value = sqlite3_value_text(argv[0]);
        if (value) {
            char origin[strlen(value) + 1];
            char *host;
            char *endOfHost;
            int isMe;
            strcpy(origin, value);
            VerifyHost2(origin, NULL, NULL, NULL, &isMe);
            if (isMe) {
                sqlite3_result_text(context, "local.", -1, SQLITE_TRANSIENT);
                failed = FALSE;
            } else {
                ParseProtocolHostPort(origin, NULL, NULL, &host, &endOfHost, NULL, NULL, NULL,
                                      NULL);
                if (host && endOfHost) {
                    *endOfHost = 0;
                    StrCpyOverlap(origin, host);
                    // Include space for adding extra '.' at the end
                    char rdns[strlen(origin) + 2];
                    rdns[0] = 0;
                    char *period;
                    while ((period = strrchr(origin, '.'))) {
                        if (*(period + 1)) {
                            strcat(rdns, period + 1);
                            strcat(rdns, ".");
                        }
                        *period = 0;
                    }
                    if (*origin) {
                        strcat(rdns, origin);
                        strcat(rdns, ".");
                    }
                    AllLower(rdns);
                    sqlite3_result_text(context, rdns, -1, SQLITE_TRANSIENT);
                    failed = FALSE;
                }
            }
        }
    }

    if (failed) {
        sqlite3_result_null(context);
    }
}

static BOOL SecurityLoadRuleVacuumDay(char *line, char *file, int lineno) {
    char *p = StartsIWith(line, "VacuumDay ");
    int day;

    if (!p || strlen(p) < 3) {
        return FALSE;
    }

    if (stricmp(p, "off") == 0) {
        day = -2;
    } else {
        for (day = 0;; day++) {
            if (day == 7) {
                return FALSE;
            }
            if (strnicmp(p, days[day], strlen(p)) == 0) {
                break;
            }
        }
    }

    if (securityVacuumDay == -1) {
        securityVacuumDay = day;
        if (!(securityVacuumDayFile = strdup(file)))
            PANIC;
        securityVacuumDayLineno = lineno;
    } else {
        Log("%s at %s:%d ignored, duplicates %s:%d", line, file, lineno, securityVacuumDayFile,
            securityVacuumDayLineno);
    }

    return TRUE;
}

static BOOL SecurityLoadRulePurgeTime(char *line, char *file, int lineno) {
    char *p = StartsIWith(line, "PurgeTime ");
    int hour;
    int min;
    BOOL valid = FALSE;

    if (!p) {
        return FALSE;
    }

    if (sscanf(p, "%d:%d", &hour, &min) == 2 && hour >= 0 && hour < 24 && min >= 0 && min < 60) {
        // nothing special to do
    } else {
        return FALSE;
    }

    if (securityPurgeHour == -1) {
        securityPurgeHour = hour;
        securityPurgeMin = min;
        if (!(securityPurgeTimeFile = strdup(file)))
            PANIC;
        securityPurgeTimeLineno = lineno;
    } else {
        Log("%s at %s:%d ignored, duplicates %s:%d", line, file, lineno, securityPurgeTimeFile,
            securityPurgeTimeLineno);
    }

    return TRUE;
}

static void SecurityLoadRuleFile(char *file, int f, struct FILEREADLINEBUFFER *frb) {
    const int fieldCount = 7;
    char line[1024];
    int lineno = 0;
    int len;
    time_t now;
    pcre *rulePattern = NULL;
    pcre_extra *rulePatternExtra = NULL;
    pcre *retentionPattern = NULL;
    pcre_extra *retentionPatternExtra = NULL;
    unsigned int optionsMask;
    char *error;
    int ovec[(fieldCount + 1) * 3];
    int nmat;
    char *field[fieldCount];
    sqlite3_stmt *updateStmt = NULL;
    sqlite3_stmt *insertStmt = NULL;
    sqlite3_stmt *selectStmt = NULL;
    const char *sql;
    int result;
    char *fieldNames[] = {":rulename", ":criterion", ":boundary",        ":period",
                          ":action",   ":duration",  ":extraconstraint", NULL};
    struct TRIGGER *trigger;
    int i;
    int loaded = 0;
    char *p;

    // clang-format off
    sql =
        "UPDATE"
            " rule"
        " SET"
            " active = 1,"
            " file = :file,"
            " lineno = :lineno,"
            " at = :now,"
            " facttable = :facttable,"
            " extraconstraintvalid = 1"
        " WHERE"
            " rulename = :rulename"
            " AND criterion = :criterion"
            " AND boundary = :boundary"
            " AND period = :period * 60"
            " AND action = :action"
            " AND duration = :duration * 60"
            " AND extraconstraint = :extraconstraint";
    // clang-format on

    SQLPrepare(db, sql, &updateStmt);
    SQLOK(SQLBindTextByName(updateStmt, ":file", file, SQLITE_STATIC));

    sql = "SELECT file, lineno FROM rule WHERE rulename = ? AND active = 1";
    SQLPrepare(db, sql, &selectStmt);

    // clang-format off
    sql =
        "INSERT INTO rule("
            "active,"
            "at,"
            "rulename,"
            "criterion,"
            "boundary,"
            "period,"
            "action,"
            "duration,"
            "file,"
            "lineno,"
            "facttable,"
            "extraconstraint)"
        " VALUES ("
            "1,"
            ":now,"
            ":rulename,"
            ":criterion,"
            ":boundary,"
            ":period * 60,"
            ":action,"
            ":duration * 60,"
            ":file,"
            ":lineno,"
            ":facttable,"
            ":extraconstraint)";
    // clang-format on

    SQLPrepare(db, sql, &insertStmt);
    SQLOK(SQLBindTextByName(insertStmt, ":file", file, SQLITE_STATIC));
    UUtime(&now);
    SQLOK(SQLBindInt64ByName(insertStmt, ":now", now));
    SQLOK(SQLBindInt64ByName(updateStmt, ":now", now));

    // Limit period and duration to 7 digits to ensure when multiplied by 60 they still fit into an
    // int32. Limit boundary to 18 digits to ensure it fits into an int64.
    error = ParseRegularExpression(
        "@^([_a-z0-9]{1,50}) if ([_a-z]+) over (\\d{1,18}) per (\\d{1,7}) then (?|(log)|(block)(?: "
        "for (\\d{1,7}))?)(?: where (.+))?$@i",
        &rulePattern, &rulePatternExtra, &optionsMask, NULL, NULL);
    if (error) {
        Log("Unable to parse rule pattern: %s", error);
        PANIC;
    }

    error =
        ParseRegularExpression("@^(ResolvedRetentionDays|EvidenceRetentionDays) +(\\d{1,3})$@i",
                               &retentionPattern, &retentionPatternExtra, &optionsMask, NULL, NULL);
    if (error) {
        Log("Unable to parse retention pattern: %s", error);
        PANIC;
    }

    for (lineno = 1; FileReadLine(f, line, sizeof(line), frb); lineno++) {
        Trim(line, TRIM_COMPRESS);
        len = strlen(line);
        if (len == 0 || line[0] == '#') {
            continue;
        }

        if (SecurityLoadRuleVacuumDay(line, file, lineno) ||
            SecurityLoadRulePurgeTime(line, file, lineno)) {
            continue;
        }

        nmat = pcre_exec(retentionPattern, retentionPatternExtra, line, len, 0, 0, ovec,
                         sizeof(ovec) / sizeof(ovec[0]));
        if (nmat > 0) {
            char *retentionType = SnipField(line, ovec[2], ovec[3]);
            char *retentionValue = SnipField(line, ovec[4], ovec[5]);
            int retention = atoi(retentionValue);

            if (retention == 0) {
                Log("%s invalid value at %s:%d ignored", retentionType, file, lineno);
            } else {
                if (stricmp(retentionType, "EvidenceRetentionDays") == 0) {
                    if (retention > securityEvidenceRetention) {
                        if (securityEvidenceRetention != 0) {
                            Log("%s %d at %s:%d overrides %d at %s:%d", retentionType, retention,
                                file, lineno, securityEvidenceRetention,
                                securityEvidenceRetentionFile, securityEvidenceRetentionLineno);
                        }
                        securityEvidenceRetention = retention;
                        FreeThenNull(securityEvidenceRetentionFile);
                        if (!(securityEvidenceRetentionFile = strdup(file)))
                            PANIC;
                        securityEvidenceRetentionLineno = lineno;
                        loaded++;
                    } else {
                        Log("%s %d at %s:%d overridden by %d at %s:%d", retentionType, retention,
                            file, lineno, securityEvidenceRetention, securityEvidenceRetentionFile,
                            securityEvidenceRetentionLineno);
                    }
                } else {
                    if (retention > securityResolvedRetention) {
                        if (securityResolvedRetention != 0) {
                            Log("%s %d at %s:%d overrides %d at %s:%d", retentionType, retention,
                                file, lineno, securityResolvedRetention,
                                securityResolvedRetentionFile, securityResolvedRetentionLineno);
                        }
                        securityResolvedRetention = retention;
                        FreeThenNull(securityResolvedRetentionFile);
                        if (!(securityResolvedRetentionFile = strdup(file)))
                            PANIC;
                        securityResolvedRetentionLineno = lineno;
                        loaded++;
                    } else {
                        Log("%s %d at %s:%d overridden by %d at %s:%d", retentionType, retention,
                            file, lineno, securityResolvedRetention, securityResolvedRetentionFile,
                            securityResolvedRetentionLineno);
                    }
                }
            }
            FreeThenNull(retentionType);
            FreeThenNull(retentionValue);
            continue;
        }

        nmat = pcre_exec(rulePattern, rulePatternExtra, line, len, 0, 0, ovec,
                         sizeof(ovec) / sizeof(ovec[0]));
        if (nmat < 0) {
            Log("Invalid security rule at %s:%d: %s", file, lineno, line);
            continue;
        }

        sqlite3_reset(updateStmt);
        sqlite3_reset(insertStmt);
        SQLOK(SQLBindIntByName(updateStmt, ":lineno", lineno));
        SQLOK(SQLBindIntByName(insertStmt, ":lineno", lineno));
        for (i = 1; i <= fieldCount; i++) {
            if (i < nmat) {
                field[i - 1] = SnipField(line, ovec[i * 2], ovec[i * 2 + 1]);
            } else {
                // All optional fields default to 0 except the final where which defaults to the
                // empty string
                if (i < fieldCount) {
                    field[i - 1] = strdup("0");
                } else {
                    field[i - 1] = strdup("");
                }
            }
            SQLOK(SQLBindTextByName(updateStmt, fieldNames[i - 1], field[i - 1], SQLITE_STATIC));
            SQLOK(SQLBindTextByName(insertStmt, fieldNames[i - 1], field[i - 1], SQLITE_STATIC));
        }

        for (trigger = triggers; trigger->facttable != NULL; trigger++) {
            if (strcmp(trigger->criterion, field[1]) == 0) {
                SQLOK(
                    SQLBindTextByName(updateStmt, ":facttable", trigger->facttable, SQLITE_STATIC));
                SQLOK(
                    SQLBindTextByName(insertStmt, ":facttable", trigger->facttable, SQLITE_STATIC));
                break;
            }
        }

        if (trigger->facttable == NULL) {
            Log("Invalid criterion in security rule at %s:%d: %s", file, lineno, line);
        } else {
            int period = atoi(field[3]) * 60;
            if (period > maxAllowedPeriod) {
                Log("Period may not exceed %d in security rule at %s:%d: %s", maxAllowedPeriod / 60,
                    file, lineno, line);
            } else {
                if (stricmp(trigger->facttable, "login") == 0) {
                    maxLoginPeriod = UUMAX(maxLoginPeriod, period);
                } else {
                    maxRequestPeriod = UUMAX(maxRequestPeriod, period);
                }
                sqlite3_reset(selectStmt);
                SQLOK(sqlite3_bind_text(selectStmt, 1, field[0], -1, SQLITE_STATIC));
                result = sqlite3_step(selectStmt);
                if (result == SQLITE_ROW) {
                    const char *dupFile = sqlite3_column_text(selectStmt, 0);
                    int dupLineno = sqlite3_column_int(selectStmt, 1);
                    Log("Rule %s at %s:%d ignored, duplicates %s:%d", field[0], file, lineno,
                        dupFile, dupLineno);
                    while (sqlite3_step(selectStmt) == SQLITE_ROW) {
                    }
                } else if (result != SQLITE_DONE) {
                    SQLPANIC(result);
                } else {
                    SQLDONE(sqlite3_step(updateStmt));
                    if (sqlite3_changes(db) == 0) {
                        SQLDONE(sqlite3_step(insertStmt));
                    }
                    loaded++;
                }
            }
        }

        for (i = 1; i <= fieldCount; i++) {
            free(field[i - 1]);
        }
    }

    sqlite3_finalize(updateStmt);
    sqlite3_finalize(selectStmt);
    sqlite3_finalize(insertStmt);

    if (loaded == 0) {
        Log("Security no valid rules found in %s", file);
    }
}

static BOOL SecurityLoadRule(char *base, char *file, int flags, void *context) {
    char path[MAX_PATH];
    int f;
    char *defaultname;

    FilesSecurityDefault(&defaultname, NULL, NULL);
    if (stricmp(defaultname, file) == 0) {
        return TRUE;
    }

    sprintf(path, "%s%s%s", base, DIRSEP, file);

    if (EndsIWith(path, ".db")) {
        return TRUE;
    }

    if (!EndsIWith(path, ".txt")) {
        Log("Skipping non-.txt security file: %s", file);
        return TRUE;
    }

    if (debugLevel) {
        Log("Loading security file: %s", file);
    }

    f = SOPEN(path);
    if (f < 0)
        PANIC;

    SecurityLoadRuleFile(file, f, NULL);

    close(f);

    return TRUE;
}

static void SecurityLoadDefaultRule() {
    int f;
    char *filename, *fullname, *filetext;
    char *slash;
    struct FILEREADLINEBUFFER frb;

    FilesSecurityDefault(&filename, &fullname, &filetext);

    InitializeFileReadLineBuffer(&frb);
    f = SOPEN(fullname);
    if (f < 0) {
        if (errno != ENOENT)
            PANIC;
        f = -1;
        frb.pseudoFile = filetext;
        filename = builtinFilename;
    } else {
        Log("Security built-in rules are overridden");
    }

    SecurityLoadRuleFile(filename, f, &frb);
    if (f >= 0) {
        close(f);
    }
}

static void SecurityStartupStatsCheckpoint(int idx) {
    static DWORD lastSecurityStartupStatsTick = 0;

    if (optionSecurityStartupStats) {
        DWORD endTick = GetTickCount();
        if (idx >= 0) {
            Log("Security startup command %d completed in %d ms", idx,
                GetTickDiff(endTick, lastSecurityStartupStatsTick));
        }
        lastSecurityStartupStatsTick = endTick;
    }
}

static void SecurityLoadRules() {
    int idx = 3000;
    sqlite3_stmt *updateStmt = NULL;

    // clang-format off
    const char *inactiveSql =
        "UPDATE"
            " rule"
        " SET"
            " active = 0"
        " WHERE"
            " active <> 0";
    // clang-format on

    // clang-format off
    const char *deleteSql =
        "DELETE"
        " FROM"
            " rule"
        " WHERE"
            " ACTIVE = 0 "
            " AND NOT EXISTS ("
                " SELECT"
                    " *"
                " FROM"
                    " tripped"
                " WHERE"
                    " tripped.ruleid = rule.id"
            ")";
    // clang-format on

    // clang-format off
    const char *updateSql =
        "UPDATE"
            " rule"
        " SET"
            " lineno = 0"
        " WHERE"
            " active = 0"
            " OR file = :file";
    // clang-format on

    SQLEXEC(db, inactiveSql, NULL, NULL);
    SecurityStartupStatsCheckpoint(idx++);

    SecurityLoadDefaultRule();
    SecurityStartupStatsCheckpoint(idx++);

    ScanDirectory("security", SCAN_DIRECTORY_FILE, SecurityLoadRule, NULL);
    SecurityStartupStatsCheckpoint(idx++);

    SQLEXEC(db, deleteSql, NULL, NULL);
    SecurityStartupStatsCheckpoint(idx++);

    SQLPrepare(db, updateSql, &updateStmt);
    SQLBindTextByName(updateStmt, ":file", builtinFilename, SQLITE_STATIC);
    SQLDONE(sqlite3_step(updateStmt));
    sqlite3_finalize(updateStmt);
    SecurityStartupStatsCheckpoint(idx++);

    if (securityResolvedRetention == 0) {
        securityResolvedRetention = 14;
    }

    if (securityEvidenceRetention == 0) {
        securityEvidenceRetention = 14;
    }

    securityResolvedRetention *= ONE_DAY_IN_SECONDS;
    securityEvidenceRetention *= ONE_DAY_IN_SECONDS;

    // We need to retain tripped data for at least the largest period of any rule
    securityResolvedRetention =
        UUMAX(UUMAX(maxLoginPeriod, maxRequestPeriod), securityResolvedRetention);

    // Default to 03:30
    if (securityPurgeHour == -1) {
        securityPurgeHour = 3;
        securityPurgeMin = 30;
    }

    // Default to Wednesday
    if (securityVacuumDay == -1) {
        securityVacuumDay = 3;
    }

    if (securityVacuumDay >= 0) {
        securityVacuumDayName = days[securityVacuumDay];
    }

    FreeThenNull(securityEvidenceRetentionFile);
    FreeThenNull(securityResolvedRetentionFile);
    FreeThenNull(securityVacuumDayFile);
    FreeThenNull(securityPurgeTimeFile);
}

struct BLOCKED {
    char *user;
    time_t expires;
    BOOL updated;
};

static int UUAvlTreeCmpBlocked(const void *v1, const void *v2) {
    struct BLOCKED *b1 = (struct BLOCKED *)v1;
    struct BLOCKED *b2 = (struct BLOCKED *)v2;

    return strcmp(b1->user, b2->user);
}

static void SecurityUpdateBlocked() {
    DWORD startTick, endTick;
    time_t now;

    // clang-format off
    const char *sql =
        "SELECT"
            " user,"
            " expires"
        " FROM"
            " tripped"
        " WHERE"
            " expires > :now";
    // clang-format on

    sqlite3_stmt *stmt;
    int result;
    struct BLOCKED *p, *n, *next;
    time_t expires;

    if (optionSQLStats) {
        startTick = GetTickCount();
    }

    UUtime(&now);
    SQLPrepare(db, sql, &stmt);
    SQLBindInt64ByName(stmt, ":now", now);

    result = sqlite3_step(stmt);
    RWLAcquireWrite(&rwlBlocked);

    UUAVLTREEITERCTX uatic;

    for (p = (struct BLOCKED *)UUAvlTreeIterFirst(&avlBlocked, &uatic); p; p = next) {
        next = UUAvlTreeIterNext(&avlBlocked, &uatic);
        p->updated = 0;
    }

    n = NULL;
    for (; result == SQLITE_ROW; result = sqlite3_step(stmt)) {
        if (n == NULL) {
            n = calloc(1, sizeof(*n));
        }
        n->user = (char *)sqlite3_column_text(stmt, 0);
        expires = sqlite3_column_int64(stmt, 1);
        p = UUAvlTreeFind(&avlBlocked, n);
        if (p != NULL) {
            if (p->updated == 0 || expires > p->expires) {
                p->updated = 1;
                p->expires = expires;
            }
            n->user = NULL;
        } else {
            if (!(n->user = strdup(n->user)))
                PANIC;
            n->expires = expires;
            n->updated = 1;
            UUAvlTreeInsert(&avlBlocked, n);
            n = NULL;
        }
    }

    FreeThenNull(n);
    SQLDONE(result);
    sqlite3_finalize(stmt);

    for (p = (struct BLOCKED *)UUAvlTreeIterFirst(&avlBlocked, &uatic); p; p = next) {
        next = UUAvlTreeIterNext(&avlBlocked, &uatic);
        if (p->updated == 0) {
            UUAvlTreeDelete(&avlBlocked, p);
            FreeThenNull(p->user);
            FreeThenNull(p);
        }
    }

    RWLReleaseWrite(&rwlBlocked);

    if (optionSQLStats) {
        endTick = GetTickCount();
        Log("SQL UpdateBlocked %d", GetTickDiff(endTick, startTick));
    }
}

static void SecurityUpgradeDatabase() {
    int idx = 2000;
    const char *sqlAddColumnExemptionExpiresAudited =
        "ALTER TABLE exemption ADD expiresaudited INT NOT NULL DEFAULT 0";
    const char *sqlAddColumnRuleExtraconstraint =
        "ALTER TABLE RULE ADD extraconstraint TEXT NOT NULL DEFAULT ''";
    const char *sqlAddColumnRuleExtraconstraintvalid =
        "ALTER TABLE RULE ADD extraconstraintvalid INT NOT NULL DEFAULT 1";
    const char *sqlAddColumnRequestRdns = "ALTER TABLE request ADD rdns TEXT";
    const char *sqlUpdateRequestRdns =
        "UPDATE request SET rdns = ReverseDNS(origin) WHERE rdns IS NULL";
    const char *sqlAddColumnVariableNetwork = "ALTER TABLE %s ADD network TEXT";

    // clang-format off
    const char *sqlUpdateVariableNetwork =
        "UPDATE"
            " %s"
        " SET"
            " network = NETWORK(ip)"
        " WHERE"
            " network IS NULL";
    // clang-format on

    char sql[256];
    char *tables[] = {"login", "request", NULL};
    char **table;
    int result;

    // In this logic, the update statements happen every time, even if the alter fails. The theory
    // is that a user may have downgraded to an earlier version of EZproxy that did not populate
    // the new columns, so there may be columns that need to be updated in a return to a later
    // version.

    // The alter statements will fail if the columns already exist, so their errors are ignored.

    for (table = tables; *table; table++) {
        snprintf(sql, sizeof(sql), sqlAddColumnVariableNetwork, *table);
        sqlite3_exec(db, sql, NULL, NULL, NULL);
        SecurityStartupStatsCheckpoint(idx++);
        snprintf(sql, sizeof(sql), sqlUpdateVariableNetwork, *table);
        SQLEXEC(db, sql, NULL, NULL);
        SecurityStartupStatsCheckpoint(idx++);
    }

    result = sqlite3_exec(db, sqlAddColumnRequestRdns, NULL, NULL, NULL);
    SecurityStartupStatsCheckpoint(idx++);
    // Unlike other migration commands, initializing the value of the rdns column only occurs if the
    // column was just added. See EZPROX-2026 for details.
    if (result == SQLITE_OK) {
        SQLEXEC(db, sqlUpdateRequestRdns, NULL, NULL);
        SecurityStartupStatsCheckpoint(idx);
    }
    idx++;

    sqlite3_exec(db, sqlAddColumnRuleExtraconstraint, NULL, NULL, NULL);
    SecurityStartupStatsCheckpoint(idx++);
    sqlite3_exec(db, sqlAddColumnRuleExtraconstraintvalid, NULL, NULL, NULL);
    SecurityStartupStatsCheckpoint(idx++);

    sqlite3_exec(db, sqlAddColumnExemptionExpiresAudited, NULL, NULL, NULL);
    SecurityStartupStatsCheckpoint(idx++);
}

static void SecurityOpenDatabase(sqlite3 **db) {
    SQLOK(sqlite3_open_v2(
        SECURITYDB, db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX, NULL));
    SQLOK(sqlite3_busy_timeout(*db, 60000));

    SQLOK(sqlite3_create_function(*db, "KMGT", 1, SQLITE_UTF8 | SQLITE_DETERMINISTIC, NULL,
                                  &SecurityKMGT, NULL, NULL));
    SQLOK(sqlite3_create_function(*db, "NEVER", 0, SQLITE_UTF8 | SQLITE_DETERMINISTIC, NULL,
                                  &SecurityNEVER, NULL, NULL));
    SQLOK(sqlite3_create_function(*db, "EXEMPT", 0, SQLITE_UTF8 | SQLITE_DETERMINISTIC, NULL,
                                  &SecurityEXEMPT, NULL, NULL));
    SQLOK(sqlite3_create_function(*db, "NETWORK", 1, SQLITE_UTF8 | SQLITE_DETERMINISTIC, NULL,
                                  &SecurityNetwork, NULL, NULL));
    SQLOK(sqlite3_create_function(*db, "ReverseDNS", 1, SQLITE_UTF8 | SQLITE_DETERMINISTIC, NULL,
                                  &SecurityReverseDNS, NULL, NULL));
}

void SecurityInit() {
    const char **sql;
    int idx = 1000;

    SecurityStartupStatsCheckpoint(-1);

    UUAvlTreeInit(&avlBlocked, "avlSecurityBlocked", UUAvlTreeCmpBlocked, NULL, NULL);
    RWLInit(&rwlBlocked, "rwlSecurityBlocked");

    SecurityOpenDatabase(&db);
    SecurityOpenDatabase(&sharedDb);

    for (sql = startupCommands; *sql; sql++) {
        SQLEXEC(db, *sql, NULL, NULL);
        SecurityStartupStatsCheckpoint(idx++);
    }

    SecurityUpgradeDatabase();

    securityQueue = QueueNew("securityQueue", FALSE);
    SecurityLoadRules();
    SecurityUpdateBlocked();
    SecurityStartupStatsCheckpoint(idx++);
}

static const char *SecurityIpToNetwork(const struct sockaddr_storage *sa,
                                       char *ipBuffer,
                                       const size_t size) {
    char buffer[32];
    ipBuffer[0] = 0;
    const char *result = NULL;
    int i;

    if (sa->ss_family == AF_INET) {
        memcpy(buffer, get_in_addr2(sa), 4);
        buffer[3] = 0;
        result = inet_ntop(sa->ss_family, buffer, ipBuffer, size);
    } else if (sa->ss_family == AF_INET6) {
        memcpy(buffer, get_in_addr2(sa), 16);
        // The standard network size for IPv6 appears to be 64 bits (8 bytes)
        for (i = 8; i < 16; i++) {
            buffer[i] = 0;
        }
        result = inet_ntop(sa->ss_family, buffer, ipBuffer, size);
    }
    if (result == NULL) {
        ipBuffer[0] = 0;
        result = ipBuffer;
    }
    return result;
}

static void SecurityInsertLogin(struct SECURITY_LOGIN *login) {
    static sqlite3_stmt *insertStmt = NULL;
    int col = 1;
    char ipBuffer[INET6_ADDRSTRLEN];
    char networkBuffer[INET6_ADDRSTRLEN];
    struct LOCATION location;

    const char *sql =
        "INSERT INTO login(at, user, session, ip, country, region, city, status, network) VALUES "
        "(?, ?, ?, ?, ?, ?, ?, ?, ?)";
    if (insertStmt == NULL) {
        SQLPrepare(db, sql, &insertStmt);
    }

    ipToStr(&login->sin_addr, ipBuffer, sizeof ipBuffer);
    SecurityIpToNetwork(&login->sin_addr, networkBuffer, sizeof(networkBuffer));
    FindLocationByNetwork(&location, &login->sin_addr);

    SQLOK(sqlite3_bind_int(insertStmt, col++, login->accessed));
    SQLOK(sqlite3_bind_text(insertStmt, col++, login->user, -1, SQLITE_TRANSIENT));
    SQLOK(sqlite3_bind_text(insertStmt, col++, login->session, -1, SQLITE_STATIC));
    SQLOK(sqlite3_bind_text(insertStmt, col++, ipBuffer, -1, SQLITE_STATIC));
    SQLOK(sqlite3_bind_text(insertStmt, col++, location.countryCode, -1, SQLITE_TRANSIENT));
    SQLOK(sqlite3_bind_text(insertStmt, col++, location.region, -1, SQLITE_TRANSIENT));
    SQLOK(sqlite3_bind_text(insertStmt, col++, location.city, -1, SQLITE_TRANSIENT));
    SQLOK(sqlite3_bind_text(insertStmt, col++, login->status_s, -1, SQLITE_STATIC));
    SQLOK(sqlite3_bind_text(insertStmt, col++, networkBuffer, -1, SQLITE_STATIC));

    SQLDONE(sqlite3_step(insertStmt));
    sqlite3_reset(insertStmt);
    sqlite3_clear_bindings(insertStmt);

    FreeThenNull(login->user);
    FreeThenNull(login);
}

static void SecurityInsertRequest(struct SECURITY_REQUEST *request) {
    static sqlite3_stmt *insertStmt = NULL;
    int col = 1;
    const char *sql =
        "INSERT INTO request(at, user, session, ip, country, region, city, origin, path, "
        "content_type, status, transferred, network, rdns) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
        "?, ?, ?, ReverseDNS(?))";
    if (insertStmt == NULL) {
        SQLPrepare(db, sql, &insertStmt);
    }

    Trim(request->contentType, TRIM_TRAIL);
    AllLower(request->contentType);

    SQLOK(sqlite3_bind_int(insertStmt, col++, request->accessed));
    SQLOK(sqlite3_bind_text(insertStmt, col++, request->user, -1, SQLITE_TRANSIENT));
    SQLOK(sqlite3_bind_text(insertStmt, col++, request->session, -1, SQLITE_TRANSIENT));
    SQLOK(sqlite3_bind_text(insertStmt, col++, request->ip, -1, SQLITE_TRANSIENT));
    SQLOK(sqlite3_bind_text(insertStmt, col++, request->country, -1, SQLITE_TRANSIENT));
    SQLOK(sqlite3_bind_text(insertStmt, col++, request->region, -1, SQLITE_TRANSIENT));
    SQLOK(sqlite3_bind_text(insertStmt, col++, request->city, -1, SQLITE_TRANSIENT));
    SQLOK(sqlite3_bind_text(insertStmt, col++, request->origin, -1, SQLITE_STATIC));
    SQLOK(sqlite3_bind_text(insertStmt, col++, request->path, -1, SQLITE_TRANSIENT));
    SQLOK(sqlite3_bind_text(insertStmt, col++, request->contentType, -1, SQLITE_TRANSIENT));
    SQLOK(sqlite3_bind_int(insertStmt, col++, request->status));
    SQLOK(sqlite3_bind_int64(insertStmt, col++, request->transferred));
    SQLOK(sqlite3_bind_text(insertStmt, col++, request->network, -1, SQLITE_STATIC));
    SQLOK(sqlite3_bind_text(insertStmt, col++, request->origin, -1, SQLITE_STATIC));

    SQLDONE(sqlite3_step(insertStmt));
    sqlite3_reset(insertStmt);
    sqlite3_clear_bindings(insertStmt);

    FreeThenNull(request->user);
    FreeThenNull(request->origin);
    FreeThenNull(request->path);
    FreeThenNull(request->contentType);
    FreeThenNull(request->ip);
    FreeThenNull(request->country);
    FreeThenNull(request->region);
    FreeThenNull(request->city);
    FreeThenNull(request->session);
    FreeThenNull(request->network);
    FreeThenNull(request);
}

static void SecurityEnqueue(struct SECURITY_LOGIN *login,
                            struct SECURITY_REQUEST *request,
                            BOOL updateBlocked) {
    struct SECURITY_NODE *n;

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;
    n->login = login;
    n->request = request;
    n->updateBlocked = updateBlocked;
    QueueEnqueue(securityQueue, n);
}

void SecurityEnqueueLogin(struct TRANSLATE *t,
                          int auditEvent,
                          const char *user,
                          struct SESSION *e) {
    struct SECURITY_LOGIN *login;
    time_t accessed;
    char *status_s;

    if (auditEvent == AUDITLOGINSUCCESS) {
        accessed = e->created;
        status_s = "success";
    } else {
        UUtime(&accessed);
        if (auditEvent == AUDITLOGINFAILURE) {
            status_s = "failure";
        } else if (auditEvent == AUDITLOGINSUCCESSRELOGIN) {
            status_s = "relogin";
        } else {
            return;
        }
    }

    login = calloc(1, sizeof(*login));
    login->accessed = accessed;
    login->sin_addr = e ? e->sin_addr : t->sin_addr;
    if (!(login->user = strdup(user)))
        PANIC;
    AllLower(login->user);
    if (e && !(login->session = strdup(e->key)))
        PANIC;
    login->status_s = status_s;

    SecurityEnqueue(login, NULL, FALSE);
}

void SecurityEnqueueRequest(struct TRANSLATE *t) {
    struct SECURITY_REQUEST *request;
    char ipBuffer[INET6_ADDRSTRLEN];
    char networkBuffer[INET6_ADDRSTRLEN];
    int defaultPort;

    if (t->host == NULL || *(t->method) == 0) {
        return;
    }

    request = calloc(1, sizeof(*request));
    UUtime(&request->accessed);
    if (t->session) {
        if (t->session->logUserFull) {
            if (!(request->user = strdup((t->session)->logUserFull)))
                PANIC;
            AllLower(request->user);
        }
        if (!(request->session = strdup(t->session->key)))
            PANIC;
    }
    ipToStr(&t->sin_addr, ipBuffer, sizeof ipBuffer);
    if (!(request->ip = strdup(ipBuffer)))
        PANIC;
    SecurityIpToNetwork(&t->sin_addr, networkBuffer, sizeof(networkBuffer));
    if (!(request->network = strdup(networkBuffer)))
        PANIC;

    FindLocationByTranslate(t);
    request->country = t->location.countryCode ? strdup(t->location.countryCode) : NULL;
    request->region = t->location.region ? strdup(t->location.region) : NULL;
    request->city = t->location.city ? strdup(t->location.city) : NULL;

    if (t->host->myDb == NULL) {
        if (!(request->origin = strdup("local")))
            PANIC;
    } else {
        // length = https:// (8) + host () + :##### (6) + null (1) + a little breathing space =
        // strlen(host) + 20
        if (!(request->origin = malloc(strlen(t->host->hostname) + 20)))
            PANIC;
        if ((t->host)->ftpgw) {
            strcpy(request->origin, "ftp");
            defaultPort = t->host->remport;
        } else if ((t->host != hosts && (t->host)->useSsl) || (t->host == hosts && t->useSsl)) {
            strcpy(request->origin, "https");
            defaultPort = 443;
        } else {
            strcpy(request->origin, "http");
            defaultPort = 80;
        }
        sprintf(AtEnd(request->origin), "://%s", t->host->hostname);
        if (t->host->remport != defaultPort) {
            sprintf(AtEnd(request->origin), ":%d", t->host->remport);
        }
    }
    if (!(request->path = strdup(t->urlCopy)))
        PANIC;
    if (!(request->contentType = strdup(t->contentType)))
        PANIC;
    request->transferred = (t->ssocket).sendCount;
    request->status = t->finalStatus;

    SecurityEnqueue(NULL, request, FALSE);
}

void SecurityEnqueueUpdateBlocked() {
    SecurityEnqueue(NULL, NULL, TRUE);
}

static void SecurityCleanTripped() {
    DWORD startTick, endTick;
    int idx = 0;

    // clang-format off
    const char *deleteCommands[] = {
        "DELETE"
        " FROM"
            " tripped"
        " WHERE"
            " expires < :now"
            " AND at < :earliestresolved"
            " AND at < ("
                " SELECT"
                    " :now - period"
                " FROM"
                    " rule"
                " WHERE"
                    " rule.id = tripped.ruleid"
            ")",

        "DELETE"
        " FROM"
            " evidence"
        " WHERE"
            " NOT EXISTS("
                " SELECT"
                    " *"
                " FROM"
                    " tripped"
                " WHERE"
                    " tripped.id = evidence.trippedid"
                    " AND tripped.at >= :earliestevidence"
            ")",

        "DELETE"
        " FROM"
            " login"
        " WHERE"
            " at < :loginexpires"
            " AND NOT EXISTS("
                " SELECT"
                    " *"
                " FROM"
                    " evidence"
                    " INNER JOIN tripped ON tripped.id = evidence.trippedid"
                    " INNER JOIN rule ON rule.id = tripped.ruleid"
                " WHERE"
                    " factid = login.id"
                    " AND rule.facttable = 'login'"
            ")",

        "DELETE"
        " FROM"
            " request"
        " WHERE"
            " at < :requestexpires"
            " AND NOT EXISTS("
                " SELECT"
                    " *"
                " FROM"
                    " evidence"
                    " INNER JOIN tripped ON tripped.id = evidence.trippedid"
                    " INNER JOIN rule ON rule.id = tripped.ruleid"
                " WHERE"
                    " factid = request.id"
                    " AND rule.facttable = 'request'"
            ")",

        NULL
    };
    // clang-format on

    sqlite3_stmt *deleteStmt = NULL;
    int deleted;
    time_t now;
    const char **sql;

    UUtime(&now);

    Log("Security database purge");

    // Need to change block to 0 after expires and then delete once we are far enough out that we
    // would not look back at previous data (new field: unblock and

    for (sql = deleteCommands; *sql; sql++, idx++) {
        if (optionSQLStats) {
            startTick = GetTickCount();
        }

        ChargeAlive(GCSECURITY, NULL);
        SQLPrepare(db, *sql, &deleteStmt);
        SQLOPTIONAL(SQLBindInt64ByName(deleteStmt, ":now", now));
        SQLOPTIONAL(SQLBindInt64ByName(deleteStmt, ":loginexpires", now - maxLoginPeriod));
        SQLOPTIONAL(SQLBindInt64ByName(deleteStmt, ":requestexpires", now - maxRequestPeriod));
        SQLOPTIONAL(
            SQLBindIntByName(deleteStmt, ":earliestevidence", now - securityEvidenceRetention));
        SQLOPTIONAL(
            SQLBindIntByName(deleteStmt, ":earliestresolved", now - securityResolvedRetention));
        SQLDONE(sqlite3_step(deleteStmt));
        deleted = sqlite3_changes(db);
        sqlite3_reset(deleteStmt);
        sqlite3_clear_bindings(deleteStmt);

        if (optionSQLStats) {
            endTick = GetTickCount();
            Log("SQL CleanTripped %d deleted %d in %d", idx, deleted,
                GetTickDiff(endTick, startTick));
        }
    }

    SecurityUpdateBlocked();

    UUtime(&lastCleaned);
}

static void SecurityAuditExpiredExemptions() {
    // clang-format off
    const char *selectExpiredSql =
        "SELECT"
            " user,"
            " expires"
        " FROM"
            " exemption"
        " WHERE"
            " expires <= strftime('%s', 'now')"
            " AND expiresaudited = 0";
    // clang-format on

    // clang-format off
    const char *updateExpiredSql =
        "UPDATE"
            " exemption"
        " SET"
            " expiresaudited = 1"
        " WHERE"
            " user = :user"
            " AND expires = :expires";
    // clang-format off

    sqlite3_stmt *selectExpiredStmt = NULL;
    sqlite3_stmt *updateExpiredStmt = NULL;
    int result;

    SQLPrepare(db, selectExpiredSql, &selectExpiredStmt);
    SQLPrepare(db, updateExpiredSql, &updateExpiredStmt);

    while ((result = sqlite3_step(selectExpiredStmt)) == SQLITE_ROW) {
        const char *user = sqlite3_column_text(selectExpiredStmt, 0);
        time_t expires = sqlite3_column_int64(selectExpiredStmt, 1);
        SQLOK(SQLBindTextByName(updateExpiredStmt, ":user", user, SQLITE_STATIC));
        SQLOK(SQLBindInt64ByName(updateExpiredStmt, ":expires", expires));
        SQLDONE(sqlite3_step(updateExpiredStmt));
        sqlite3_reset(updateExpiredStmt);
        AdminSecurityAuditEvent(NULL, "expired ", user, expires);
    }
    SQLDONE(result);
    sqlite3_finalize(selectExpiredStmt);
    sqlite3_finalize(updateExpiredStmt);
}

static BOOL ShouldTrippedRulesNotificationEmailsBeSentAsTheyHappen() {
    sqlite3 *configdb = EnvironmentGetSharedDatabase();

    // clang-format off
    const char *selectNotificationEmailAddressCountSql =
        "SELECT"
            " count(emailaddress) > 0 as hasemails"
        " FROM"
            " notificationemailaddress"
        " ORDER BY"
            " emailaddress";
    // clang-format on

    int rows, cols;
    char **result;
    char *errMsg = NULL;
    BOOL hasEmailAddresses = 0;
    SQLOK(sqlite3_get_table(configdb, selectNotificationEmailAddressCountSql, &result, &rows, &cols,
                            &errMsg));
    if (rows > 0) {
        hasEmailAddresses = *result[1] == '1';
    }
    sqlite3_free_table(result);
    if (hasEmailAddresses == 0) {
        return hasEmailAddresses;
    }

    // clang-format off
    const char *selectReceivesDailyDigestSql = ""
        "SELECT"
            " receivesdailydigest"
        " FROM"
            " receivesdailydigest";
    // clang-format on

    BOOL receivesDailyDigest = 0;
    SQLOK(
        sqlite3_get_table(configdb, selectReceivesDailyDigestSql, &result, &rows, &cols, &errMsg));
    if (rows > 0) {
        receivesDailyDigest = *result[1] == '1';
    }
    sqlite3_free_table(result);
    return receivesDailyDigest == 0;
}

static void SecurityProcessRules() {
    static BOOL reportInvalidWhere = TRUE;
    const char *unused;
    DWORD startTick, endTick;
    time_t now;
    time_t expires;
    int col;
    sqlite3 *sharedEnvironmentDb = EnvironmentGetSharedDatabase();
    sqlite3_int64 trippedid = 0;

    // clang-format off
    const char *selectExtraconstraint =
        "SELECT"
            " DISTINCT"
            " extraconstraint"
        " FROM"
            " rule"
        " WHERE"
            " criterion = :criterion"
            " AND extraconstraintvalid = 1";
    // clang-format on

    // clang-format off
    const char *updateExtraconstraintvalid =
        "UPDATE"
            " rule"
        " SET"
            " extraconstraintvalid = 0"
        " WHERE"
            " active = 1"
            " AND extraconstraint = :extraconstraint";
    // clang-format on

    // clang-format off
    const char *selectTemplate =
        "SELECT"
            " rule.id,"
            " rule.rulename,"
            " rule.action,"
            " rule.period,"
            " rule.duration,"
            " fact.user,"
            " %s" // value
        " FROM"
            " rule"
            " CROSS JOIN %s fact" // table
        " WHERE"
            " rule.active = 1"
            " AND criterion = '%s'" // criterion
            " AND extraconstraint = :extraconstraint"
            " AND fact.user IS NOT NULL"
            " AND fact.user <> ''"
            " %s" // constraint
            " %s" // extraConstraint
            " AND fact.at >= %d - period" // now
            " AND fact.at > ("
                " SELECT"
                    " IFNULL(MAX(at), 0)"
                " FROM"
                    " tripped"
                " WHERE"
                    " tripped.ruleid = rule.id"
                    " AND tripped.user = fact.user"
            ")"
        " GROUP BY"
            " fact.user,"
            " rule.id"
        " HAVING"
            " boundary < %s"; // value
    // clang-format on

    // clang-format off
    const char *exemptionSql =
        "UPDATE"
            " exemption"
        " SET"
            " applied = applied + 1,"
            " applied_at = :now"
        " WHERE"
            " :user GLOB user"
            " AND expires > :now";
    // clang-format on

    /*
        Sample expansion of SQL

        SELECT
            rule.id,
            rule.rulename,
            rule.action,
            rule.period,
            rule.duration,
            fact.user,
            SUM(transferred)
        FROM
            rule
            CROSS JOIN request fact
            LEFT OUTER JOIN tripped
                ON tripped.rulename = rule.rulename
                AND rule.active = 1
                AND tripped.user = fact.user
        WHERE
            rule.active = 1
            AND criterion = 'bytes_transferred'
            AND fact.user IS NOT NULL"
            AND fact.user <> ''
            AND fact.at >= MAX(IFNULL(tripped.at, 0), 12345 - period)
            AND IFNULL(tripped.blockuntil, 0) = 0
        GROUP BY
            fact.user,
            rule.id
        HAVING
            boundary < SUM(transferred)
    */

    // clang-format off
    const char *evidenceTemplate =
        "INSERT INTO"
            " evidence("
                "trippedid,"
                "factid"
            ")"
        " SELECT"
            " :trippedid,"
            " fact.id"
        " FROM"
            " rule"
            " CROSS JOIN %s fact" // table
        " WHERE"
            " rule.active = 1"
            " AND criterion = '%s'" // criterion
            " AND extraconstraint = :extraconstraint"
            " %s" // constraint
            " %s" // extraConstraint
            " AND fact.at >= %d - period" // now
            " AND fact.at > ("
                " SELECT"
                    " IFNULL(MAX(at), 0)"
                " FROM"
                    " tripped"
                " WHERE"
                    " tripped.ruleid = rule.id"
                    " AND tripped.user = fact.user"
            ")"
            " AND rule.id = :ruleid"
            " AND fact.user = :user";
    // clang-format on

    sqlite3_stmt *selectExtraConstraintStmt = NULL;
    sqlite3_stmt *updateExtraconstraintvalidStmt = NULL;
    sqlite3_stmt *selectStmt = NULL;
    sqlite3_stmt *evidenceStmt = NULL;
    const char *insertSql =
        "INSERT INTO tripped(at, ruleid, user, expires, value) VALUES (?, ?, ?, ?, ?)";
    static sqlite3_stmt *exemptionStmt = NULL;
    static sqlite3_stmt *insertStmt = NULL;
    int result, extraconstraintResult;
    struct TRIGGER *trigger;

    if (exemptionStmt == NULL) {
        SQLPrepare(db, exemptionSql, &exemptionStmt);
    }

    if (insertStmt == NULL) {
        SQLPrepare(db, insertSql, &insertStmt);
    }

    BOOL sendNotificationEmails = ShouldTrippedRulesNotificationEmailsBeSentAsTheyHappen();

    UUtime(&now);
    int rule = 0;
    SQLPrepare(db, selectExtraconstraint, &selectExtraConstraintStmt);
    SQLPrepare(db, updateExtraconstraintvalid, &updateExtraconstraintvalidStmt);
    for (trigger = triggers; trigger->facttable != NULL; trigger++, rule++) {
        SQLOK(SQLBindTextByName(selectExtraConstraintStmt, ":criterion", trigger->criterion,
                                SQLITE_STATIC));
        while ((extraconstraintResult = sqlite3_step(selectExtraConstraintStmt)) == SQLITE_ROW) {
            const char *extraConstraint = sqlite3_column_text(selectExtraConstraintStmt, 0);
            char andExtraConstraint[strlen(extraConstraint) + 20];
            char sql[2048 + strlen(extraConstraint)];
            if (*extraConstraint) {
                sprintf(andExtraConstraint, " AND (%s)", extraConstraint);
            } else {
                andExtraConstraint[0] = 0;
            }
            snprintf(sql, sizeof(sql), selectTemplate, trigger->value, trigger->facttable,
                     trigger->criterion, trigger->constraint, andExtraConstraint, now,
                     trigger->value);
            result = sqlite3_prepare_v3(db, sql, -1, 0, &selectStmt, &unused);
            if (result != SQLITE_OK) {
                if (reportInvalidWhere) {
                    Log("Security rule with invalid where detected; see administration security "
                        "rules page for details");
                    reportInvalidWhere = FALSE;
                }
                SQLOK(SQLBindTextByName(updateExtraconstraintvalidStmt, ":extraconstraint",
                                        extraConstraint, SQLITE_STATIC));
                SQLDONE(sqlite3_step(updateExtraconstraintvalidStmt));
                sqlite3_reset(updateExtraconstraintvalidStmt);
                continue;
            }

            snprintf(sql, sizeof(sql), evidenceTemplate, trigger->facttable, trigger->criterion,
                     trigger->constraint, andExtraConstraint, now);
            SQLPrepare(db, sql, &evidenceStmt);

            if (optionSQLStats) {
                startTick = GetTickCount();
            }

            SQLBeginTransaction(db);

            SQLOK(
                SQLBindTextByName(selectStmt, ":extraconstraint", extraConstraint, SQLITE_STATIC));
            while ((result = sqlite3_step(selectStmt)) == SQLITE_ROW) {
                col = 0;
                const sqlite_int64 ruleid = sqlite3_column_int64(selectStmt, col++);
                const char *rulename = sqlite3_column_text(selectStmt, col++);
                const char *action = sqlite3_column_text(selectStmt, col++);
                const int period = sqlite3_column_int(selectStmt, col++);
                const int duration = sqlite3_column_int(selectStmt, col++);
                const char *user = sqlite3_column_text(selectStmt, col++);
                const sqlite_int64 value = sqlite3_column_int64(selectStmt, col++);

                SQLOK(SQLBindTextByName(exemptionStmt, ":user", user, SQLITE_STATIC));
                SQLOK(SQLBindInt64ByName(exemptionStmt, ":now", now));
                SQLDONE(sqlite3_step(exemptionStmt));
                const int exempt = sqlite3_changes(db);
                sqlite3_reset(exemptionStmt);
                sqlite3_clear_bindings(exemptionStmt);

                if (trippedid == 0) {
                    trippedid = SQLMaxAutoIncrement(db, "tripped");
                }
                trippedid++;

                // Only store evidence for non-exempt users
                if (exempt == 0) {
                    col = 1;
                    SQLOK(SQLBindInt64ByName(evidenceStmt, ":trippedid", trippedid));
                    SQLOK(SQLBindTextByName(evidenceStmt, ":extraconstraint", extraConstraint,
                                            SQLITE_STATIC));
                    SQLOK(SQLBindInt64ByName(evidenceStmt, ":ruleid", ruleid));
                    SQLOK(SQLBindTextByName(evidenceStmt, ":user", user, SQLITE_STATIC));
                    SQLDONE(sqlite3_step(evidenceStmt));
                    sqlite3_reset(evidenceStmt);
                    sqlite3_clear_bindings(evidenceStmt);
                }

                col = 1;
                SQLOK(sqlite3_bind_int(insertStmt, col++, now));
                SQLOK(sqlite3_bind_int64(insertStmt, col++, ruleid));
                SQLOK(sqlite3_bind_text(insertStmt, col++, user, -1, 0));
                if (strcmp(action, "block") == 0) {
                    if (exempt) {
                        expires = 1;
                    } else {
                        expires = duration > 0 ? now + duration : SQLNEVER;
                    }
                } else {
                    expires = 0;
                }
                SQLOK(sqlite3_bind_int64(insertStmt, col++, expires));
                SQLOK(sqlite3_bind_int64(insertStmt, col++, value));
                SQLDONE(sqlite3_step(insertStmt));
                sqlite3_reset(insertStmt);
                sqlite3_clear_bindings(insertStmt);
                if (sendNotificationEmails) {
                    int idResult;
                    sqlite3_stmt *selectTrippedIdStmt = NULL;
                    const char *selectTrippedIdSql = "SELECT MAX(id) FROM tripped;";
                    SQLPrepare(db, selectTrippedIdSql, &selectTrippedIdStmt);
                    if ((idResult = sqlite3_step(selectTrippedIdStmt)) == SQLITE_ROW) {
                        const sqlite_int64 rowid = sqlite3_column_int64(selectTrippedIdStmt, 0);

                        // clang-format off
                        const char *queuedtrippedrulessql =
                            "INSERT OR IGNORE INTO"
                                " queuedtrippedrules(id)"
                            " VALUES(:id);";
                        // clang-format on

                        sqlite3_stmt *queuedtrippedrulesstmt = NULL;
                        int queuedtrippedrulesresult;
                        SQLPrepare(sharedEnvironmentDb, queuedtrippedrulessql,
                                   &queuedtrippedrulesstmt);
                        SQLOK(SQLBindInt64ByName(queuedtrippedrulesstmt, ":id", rowid));
                        queuedtrippedrulesresult = sqlite3_step(queuedtrippedrulesstmt);
                        sqlite3_finalize(queuedtrippedrulesstmt);
                        if (queuedtrippedrulesresult != SQLITE_CONSTRAINT) {
                            SQLOKORDONE(queuedtrippedrulesresult);
                        }
                    }
                    sqlite3_finalize(selectTrippedIdStmt);
                }
                AuditEvent(NULL, exempt > 0 ? AUDITSECURITYEXEMPT : AUDITSECURITY, user, NULL,
                           "|%" PRIdMAX "|%s|%" PRIdMAX "|%s|%d|%d", trippedid, rulename, value,
                           action, period / 60, duration / 60);
            }

            SQLDONE(result);
            SQLCommitTransaction(db);
            sqlite3_finalize(selectStmt);
            sqlite3_finalize(evidenceStmt);

            if (optionSQLStats) {
                endTick = GetTickCount();
                Log("SQL ProcessRules criterion %s%s%s in %d", trigger->criterion,
                    *extraConstraint ? " where " : "", extraConstraint,
                    GetTickDiff(endTick, startTick));
            }
        }
        SQLDONE(extraconstraintResult);
        sqlite3_reset(selectExtraConstraintStmt);
    }

    sqlite3_finalize(selectExtraConstraintStmt);
    SecurityUpdateBlocked();
    SecurityAuditExpiredExemptions();
}

static void SecurityComputeNextPurge() {
    time_t now;
    struct tm tm;
    char buffer[MAXASCIIDATE];

    UUtime(&now);
    // Add 1 to force to next day if we are at the exact same time
    now += 1;
    UUlocaltime_r(&now, &tm);
    tm.tm_hour = securityPurgeHour;
    tm.tm_min = securityPurgeMin;
    tm.tm_sec = 0;
    securityNextPurge = mktime(&tm);
    if (securityNextPurge < now) {
        securityNextPurge += ONE_DAY_IN_SECONDS;
    }

    if (debugLevel > 0) {
        Log("Security database next purge at %s", ASCIIDate(&securityNextPurge, buffer));
    }
}

static void SecurityVacuum() {
    ChargeAlive(GCSECURITY, NULL);
    const char *sql = "VACUUM";
    sqlite3_stmt *vacuumStmt = NULL;

    SQLPrepare(db, "VACUUM", &vacuumStmt);
    ChargeStopped(GCSECURITY, NULL);
    Log("Security database vacuum starting");
    SQLDONE(sqlite3_step(vacuumStmt));
    sqlite3_finalize(vacuumStmt);
    Log("Security database vacuum finished");
    ChargeAlive(GCSECURITY, NULL);
}

static void SecurityRunPurge() {
    UUtime(&securityPurgeStarted);
    SecurityCleanTripped();
    IdentifierPurge();
    securityForcePurge = FALSE;
    securityPurgeStarted = 0;
}

static void SecurityRunVacuum() {
    UUtime(&securityVacuumStarted);
    SecurityVacuum();
    IdentifierVacuum();
    securityForceVacuum = FALSE;
    securityVacuumStarted = 0;
}

static void DoSecurity(void *v) {
    struct SECURITY_NODE *n;
    DWORD currTick, lastTick;
    DWORD interval = 60 * 1000;
    time_t now;
    struct tm tm;

    lastTick = 0;
    SecurityComputeNextPurge();

    for (;;) {
        ChargeAlive(GCSECURITY, &now);
        if ((n = (struct SECURITY_NODE *)QueueDequeue(securityQueue))) {
            if (n->login) {
                SecurityInsertLogin(n->login);
            }
            if (n->request) {
                SecurityInsertRequest(n->request);
            }
            if (n->updateBlocked) {
                SecurityUpdateBlocked();
            }
            free(n);
        } else if (now >= securityNextPurge) {
            // Make sure we take the day of the week from the same time that drove this purge
            // Use this in case someone sets purge time to 23:59 and purge doesn't end until
            // after midnight.
            UUlocaltime_r(&securityNextPurge, &tm);
            SecurityRunPurge();
            if (tm.tm_wday == securityVacuumDay) {
                SecurityRunVacuum();
            }
            SecurityComputeNextPurge();
        } else if (securityForcePurge) {
            SecurityRunPurge();
        } else if (securityForceVacuum) {
            SecurityRunVacuum();
        } else {
            currTick = GetTickCount();
            if (GetTickDiff(currTick, lastTick) > interval) {
                SecurityProcessRules();
                lastTick = currTick;
            } else {
                SubSleep(0, 100);
            }
        }
    }
}

// This is a shared database connection. It must not be closed by its users.
sqlite3 *SecurityGetSharedDatabase(BOOL shared) {
    return sharedDb;
}

BOOL SecurityUserBlocked(const char *user) {
    BOOL result = FALSE;
    if (user != NULL && *user) {
        struct BLOCKED searchBlocked;
        char lowerUser[strlen(user) + 1];
        strcpy(lowerUser, user);
        AllLower(lowerUser);
        searchBlocked.user = lowerUser;
        RWLAcquireRead(&rwlBlocked);
        result = UUAvlTreeFind(&avlBlocked, &searchBlocked) != NULL;
        RWLReleaseRead(&rwlBlocked);
    }
    return result;
}

void SecurityStart() {
    if (UUBeginThread(DoSecurity, NULL, guardianChargeName[GCSECURITY]))
        PANIC;
}
