#ifndef __HTML_H__
#define __HTML_H__

#include "common.h"

#include "uuxml.h"
#include <libxml/HTMLparser.h>

#define GETHTMLNOFOLLOWREDIRECTS 1
#define GETHTMLPOSTXML 2
#define GETHTMLPOSTSOAPSECURITY (GETHTMLPOSTXML | 4)
#define GETHTMLACTUALLYXML 8

htmlParserCtxtPtr GetHTML(char *url,
                          char *post,
                          struct COOKIE **cookies,
                          char **finalURL,
                          struct sockaddr_storage *pInterface,
                          int flags,
                          int sslIdx,
                          BOOL *ssl,
                          char **peerCN,
                          BOOL debug);
void GetHTMLFreeCookies(struct COOKIE **cookies);
char *AbsoluteURL(char *baseURL, char *relativeURL);

#endif  // __HTML_H__
