/**
 * NCIP user authentication
 *
 * This authentication method supports common conditions and actions.
 */

#define __UUFILE__ "usrncip.c"

#include "common.h"

#ifdef WIN32
/* io.h, fcntl.h and share.h needed for _sopen in Win32 */
#include <io.h>
#include <fcntl.h>
#include <share.h>
#include <lm.h>
#endif

#include <sys/stat.h>

#include "usr.h"
#include "usrncip.h"

#include "uuxml.h"

struct NCIPAUTHENTICATIONINPUT {
    struct NCIPAUTHENTICATIONINPUT *next;
    char *key;
    char *value;
};

char *NCIPAUTHENTICATIONINPUT1 =
    "<AuthenticationInput>\n\
<AuthenticationInputData>";
char *NCIPAUTHENTICATIONINPUT2 =
    "</AuthenticationInputData>\n\
<AuthenticationDataFormatType>\n\
<Scheme>http://www.iana.org/assignments/media-types</Scheme>\n\
<Value>Text</Value>\n\
</AuthenticationDataFormatType>\n\
<AuthenticationInputType>\n\
<Scheme>http://www.niso.org/ncip/v1_0/imp1/schemes/authenticationinputtype/authenticationinputtype.scm</Scheme>\n\
<Value>";
char *NCIPAUTHENTICATIONINPUT3 =
    "</Value>\n\
</AuthenticationInputType>\n\
</AuthenticationInput>\n";

struct NCIPCTX {
    struct NCIPAUTHENTICATIONINPUT *nai, *naiLast;
    BOOL badAuthenticationInput;
    size_t inputLen;
};

void CatEncodedField(char *s, char *f) {
    if (s == NULL || f == NULL)
        return;

    s = strchr(s, 0);
    for (; *f; f++) {
        if (*f == '&' || *f == '<' || *f == '>') {
            sprintf(s, "&#%d;", *f);
            s = strchr(s, 0);
        } else {
            *s++ = *f;
        }
    }
    *s = 0;
}

int UsrNCIPAuthenticationInput(struct TRANSLATE *t,
                               struct USERINFO *uip,
                               void *context,
                               char *arg) {
    struct NCIPCTX *ncipCtx = (struct NCIPCTX *)context;
    struct NCIPAUTHENTICATIONINPUT *n;
    char *arg2;
    char *value = NULL;

    if (arg2 = StartsIWith(arg, "user ")) {
        value = uip->user;
    } else if (arg2 = StartsIWith(arg, "pass ")) {
        value = uip->pass;
    } else if (arg2 = StartsIWith(arg, "pin ")) {
        value = uip->pin;
    } else {
        UsrLog(uip, "Unrecognized value for AuthenticationInput %s", arg);
        ncipCtx->badAuthenticationInput = 1;
        return 1;
    }

    arg2 = SkipWS(arg2);

    if (*arg2 == 0) {
        UsrLog(uip, "AuthenticationInput %s missing input type", arg);
        ncipCtx->badAuthenticationInput = 1;
        return 1;
    }

    if (!(n = (struct NCIPAUTHENTICATIONINPUT *)calloc(1, sizeof(*n))))
        PANIC;
    n->key = StaticStringCopy(arg2);
    n->value = value ? value : "";

    if (ncipCtx->nai == NULL) {
        ncipCtx->nai = n;
    } else {
        (ncipCtx->naiLast)->next = n;
    }

    ncipCtx->naiLast = n;

    ncipCtx->inputLen += strlen(NCIPAUTHENTICATIONINPUT1) + strlen(NCIPAUTHENTICATIONINPUT2) +
                         strlen(NCIPAUTHENTICATIONINPUT3) + (strlen(n->key) + strlen(n->value)) * 5;

    return 0;
}

int UsrNCIPInit(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    /*
    uip->result = resultValid;
    */
    return 0;
}

int UsrNCIPServer(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct NCIPCTX *ncipCtx = (struct NCIPCTX *)context;
    char *xml1 =
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
<!DOCTYPE NCIPMessage SYSTEM \"http://www.niso.org/ncip/v1_0/imp1/dtd/ncip_v1_0.dtd\">\n\
<NCIPMessage version=\"http://www.niso.org/ncip/v1_0/imp1/dtd/ncip_v1_0.dtd\">\n\
  <AuthenticateUser>\n\
    <InitiationHeader>\n\
      <FromAgencyId>\n\
        <UniqueAgencyId>\n\
          <Scheme>http://www.sirsi.com/ncip/v1_0/imp1/schemes/uniqueagencyid/uniqueagencyid.scm</Scheme>\n\
          <Value></Value>\n\
        </UniqueAgencyId>\n\
      </FromAgencyId>\n\
      <ToAgencyId>\n\
        <UniqueAgencyId>\n\
          <Scheme>http://www.sirsi.com/ncip/v1_0/imp1/schemes/uniqueagencyid/uniqueagencyid.scm</Scheme>\n\
          <Value></Value>\n\
        </UniqueAgencyId>\n\
      </ToAgencyId>\n\
    </InitiationHeader>\n";
    char *xml2 =
        "  </AuthenticateUser>\n\
</NCIPMessage>\n";

    char *xmlRequest = NULL;
    struct UUSOCKET rr, *r = &rr;
    BOOL inHeader;
    int result = 1;

    char line[8192];
    xmlParserCtxtPtr ctxt = NULL;
    xmlDocPtr doc = NULL; /* the resulting document tree */
    int res;
    xmlXPathContextPtr xpathCtx = NULL;
    xmlXPathObjectPtr xpathObj = NULL;
    xmlNodeSetPtr nodes = NULL;
    char *host, *colon, *endOfHost;
    char useSsl;
    PORT port;
    char save = 0;
    struct NCIPAUTHENTICATIONINPUT *nai;

    UUInitSocket(r, INVALID_SOCKET);

    if (uip->user == NULL || uip->pass == NULL || strlen(uip->user) > 64 ||
        strlen(uip->pass) > 64 || ncipCtx->badAuthenticationInput)
        goto Finished;

    host = SkipHTTPslashesAt(arg, &useSsl);
    ParseHost(host, &colon, &endOfHost, 0);

    if (endOfHost) {
        save = *endOfHost;
        *endOfHost = 0;
    }

    port = 0;

    if (colon) {
        *colon = 0;
        port = atoi(colon + 1);
    } else if (host != arg) {
        port = (useSsl ? 443 : 80);
    }

    if (port == 0) {
        UsrLog(uip, "NCIP Server port missing");
        goto Finished;
    }

    if (UUConnectWithSource2(r, host, port, "NCIP", uip->pInterface, useSsl))
        goto Finished;

    if (colon)
        *colon = ':';

    if (ncipCtx->nai == NULL) {
        UsrNCIPAuthenticationInput(t, uip, ncipCtx, "user Barcode ID");
        UsrNCIPAuthenticationInput(t, uip, ncipCtx, "pass PIN");
    }

    /* The *5 allows space to expand &, < and > to &#xx; format */
    if (!(xmlRequest = calloc(1, strlen(xml1) + strlen(xml2) + ncipCtx->inputLen + 1)))
        PANIC;
    strcpy(xmlRequest, xml1);
    for (nai = ncipCtx->nai; nai; nai = nai->next) {
        strcat(xmlRequest, NCIPAUTHENTICATIONINPUT1);
        CatEncodedField(xmlRequest, nai->value);
        strcat(xmlRequest, NCIPAUTHENTICATIONINPUT2);
        CatEncodedField(xmlRequest, nai->key);
        strcat(xmlRequest, NCIPAUTHENTICATIONINPUT3);
    }
    strcat(xmlRequest, xml2);

    if (uip->debug)
        printf("%s", xmlRequest);

    if (host != arg) {
        UUSendZ(r, "POST ");

        if (endOfHost) {
            *endOfHost = save;
            UUSendZ(r, endOfHost);
            *endOfHost = 0;
        } else {
            UUSendZ(r, "/");
        }

        WriteLine(r,
                  " HTTP/1.0\r\n\
Host: %s\r\n\
Pragma: no-cache\r\n\
User-Agent: Mozilla/4.0 (compatible; EZproxy)\r\n\
Accept: */*\r\n\
Content-Type: application/xml; charset=\"utf-8\"\r\n\
Content-Length: %d\r\n\
Connection: close\r\n\
Cache-Control: no-cache\r\n\
\r\n",
                  host, strlen(xmlRequest));
    }

    UUSendZ(r, xmlRequest);

    if (uip->debug)
        UsrLog(uip, "The response");

    inHeader = host != arg;
    while (ReadLine(r, line, sizeof(line)) > 0) {
        if (uip->debug)
            UsrLog(uip, "%s", line);
        if (inHeader) {
            if (line[0] == 0)
                inHeader = 0;
            continue;
        }

        if (ctxt == NULL) {
            ctxt = xmlCreatePushParserCtxt(NULL, NULL, line, strlen(line), NULL);
            if (ctxt == NULL) {
                UsrLog(uip, "Non-XML response from NCIP");
                goto Finished;
            }
        } else {
            xmlParseChunk(ctxt, line, strlen(line), 0);
        }
        if (StrIStr(line, "</NCIPMessage>"))
            break;
    }

    if (ctxt == NULL) {
        UsrLog(uip, "NCIP server did not respond\n");
        goto Finished;
    }

    xmlParseChunk(ctxt, line, 0, 1);

    doc = ctxt->myDoc;
    res = ctxt->wellFormed;

    if (uip->debug)
        printf("\n\n");

    xpathCtx = xmlXPathNewContext(doc);
    if (xpathCtx == NULL) {
        UsrLog(uip, "Error: unable to create new XPath context");
        goto Finished;
    }

    xpathObj = xmlXPathEvalExpression(
        BAD_CAST "/NCIPMessage/AuthenticateUserResponse/UniqueUserId/UserIdentifierValue",
        xpathCtx);
    if (xpathObj == NULL || (nodes = xpathObj->nodesetval) == NULL || (nodes->nodeNr == 0)) {
        /* ||
        (ii = (char *) xmlGetProp(nodes->nodeTab[0], BAD_CAST "IssueInstant")) == NULL) {
        */
        goto Finished;
    }

    uip->result = resultValid;

    result = 0;

Finished:
    UUStopSocket(r, 0);

    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    if (xpathCtx) {
        xmlXPathFreeContext(xpathCtx);
        xpathCtx = NULL;
    }

    if (ctxt) {
        xmlFreeParserCtxt(ctxt);
        ctxt = NULL;
    }

    if (doc) {
        xmlFreeDoc(doc);
        doc = NULL;
    }

    FreeThenNull(xmlRequest);

    return result;
}

enum FINDUSERRESULT FindUserNCIP(struct TRANSLATE *t,
                                 struct USERINFO *uip,
                                 char *file,
                                 int f,
                                 struct FILEREADLINEBUFFER *frb,
                                 struct sockaddr_storage *pInterface) {
    struct USRDIRECTIVE udc[] = {
        {"",                    UsrNCIPInit,                0},
        {"AuthenticationInput", UsrNCIPAuthenticationInput, 0},
        {"Server",              UsrNCIPServer,              0},
        {NULL,                  NULL,                       0}
    };
    struct NCIPCTX ncipCtx;
    enum FINDUSERRESULT result;
    struct NCIPAUTHENTICATIONINPUT *nai, *next;

    memset(&ncipCtx, 0, sizeof(ncipCtx));

    uip->flags = 0;
    result = UsrHandler(t, "NCIP", udc, &ncipCtx, uip, file, f, frb, pInterface, NULL);

    for (nai = ncipCtx.nai; nai; nai = next) {
        next = nai->next;
        FreeThenNull(nai);
    }

    /*
    if (result == resultValid && expired) {
        HTMLHeader(t, htmlHeaderOmitCache);
        SendEditedFile(t, uip, EXPIREDHTM, 1, url, 1, NULL);
        result = resultNotified;
    }

    if (result == resultRefused) {
        result = resultInvalid;
    }

    */

    return result;
}
