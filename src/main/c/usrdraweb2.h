#ifndef __USRDRAWEB2_H__
#define __USRDRAWEB2_H__

#include "common.h"

int FindUserDraweb2(struct TRANSLATE *t,
                    struct USERINFO *uip,
                    char *file,
                    int f,
                    struct FILEREADLINEBUFFER *frb,
                    struct sockaddr_storage *pInterface);

#endif  // __USRDRAWEB2_H__
