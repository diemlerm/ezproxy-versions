#ifndef __QUEUE_H__
#define __QUEUE_H__

#include "common.h"

struct QUEUE;

struct QUEUE *QueueNew(char *name, BOOL allowNullElement);
BOOL QueueIsEmpty(struct QUEUE *q);
int QueueSize(struct QUEUE *q);
void QueueEnqueue(struct QUEUE *q, void *element);
void *QueueDequeue(struct QUEUE *q);
void *QueuePeek(struct QUEUE *q);
void QueueDestroy(struct QUEUE **q);

#endif
