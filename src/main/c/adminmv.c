#define __UUFILE__ "adminmv.c"

#include "common.h"
#include "uustring.h"

#include "adminmv.h"

void AdminCheckMV(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket;

    if (cHosts >= mHosts) {
        UUSendZ(
            s,
            "<p><span class='failure'>Your server has reached its MaxVirtualHosts limit.</span>\n\
See <a href='https://www.oclc.org/support/services/ezproxy/documentation/cfg/maxvirtualhosts.en.html'>https://www.oclc.org/support/services/ezproxy/documentation/cfg/maxvirtualhosts.en.html</a>\n\
</p>\n");
    }
}

void AdminMv(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    char *text = NULL;

    HTTPCode(t, 503, 0);
    if (t->host == mvHost) {
        text = "";
    } else if (t->host == mvHostSsl) {
        text = "ssl ";
    } else {
        text = "*** ";
    }

    if (debugLevel > 90) {
        Log("maximum virtual %shosts reached, cTranslates %d, mTranslates %d", text, cTranslates,
            mTranslates);
    }
    AdminMinimalHeader(t, "MaxVirtualHosts Error");
    if (SendEditedFile(t, NULL, MVHTM, 0, t->buffer, 1, NULL) != 0) {
        UUSendZ(s,
                "<p><strong>EZproxy Server Error</strong>\n</p><p>Please tell your server "
                "administrator to check " EZPROXYMSG " for a MaxVirtualHosts error</p>\n");
    }
    AdminMinimalFooter(t);
}
