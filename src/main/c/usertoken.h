#ifndef __USERTOKEN_H__
#define __USERTOKEN_H__

#include "common.h"

void SendTokenizedURL(struct TRANSLATE *t,
                      struct DATABASE *b,
                      char *url,
                      char **token,
                      char **tokenSignature,
                      BOOL allowUsernamePassword);
void SendTokenizedURL2(struct TRANSLATE *t,
                       struct UUSOCKET *s,
                       struct DATABASE *b,
                       char *url,
                       char **token,
                       char **tokenSignature,
                       BOOL allowUsernamePassword);
char *UserToken(struct TRANSLATE *t, unsigned char *tokenKey, char *userName);
char *UserToken2(char *prefix, char *user, BOOL createIfNotFound);
char *UserTokenDecrypt(char *input, unsigned char *key);
char *UserTokenDecrypt2(char *key, char *prefix);
char *UserTokenSignature(struct TRANSLATE *t, struct DATABASE *b);

#endif  // __USERTOKEN_H__
