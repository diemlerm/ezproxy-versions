/**
 * /userObject URL to request user object tokens and user object content
 */

#define __UUFILE__ "adminuserobject.c"

#include "common.h"
#include "uuxml.h"
#include "adminaudit.h"
#include "adminuserobject.h"
#include "usr.h"
#include "usrfinduser.h"

#define EZPROXYUSEROBJECTIP "X-EZproxy-UserObject-IP"

struct WSKEY {
    struct WSKEY *next;
    char *wskey;
    GROUPMASK gm;
    char *proxyUrl;
    char *id;
    char *ip;
    size_t proxyUrlLen;
};

struct WSKEY *wskeys = NULL;

struct USEROBJECTELEMENTCTX {
    struct SESSION *session;
    xmlTextWriterPtr writer;
    GROUPMASK gm;
    BOOL addressOpen;
    int count;
};

void ReadConfigLocalWSKey(char *arg, GROUPMASK gm) {
    struct WSKEY *n;
    char *arg2;
    char *p;
    char *proxyUrl = NULL;
    char *ip = NULL;
    char *id = NULL;

    for (;;) {
        if (*arg != '-')
            break;
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (p = StartsIWith(arg, "-proxy="))
            proxyUrl = p;
        else if (p = StartsIWith(arg, "-id="))
            id = p;
        else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized LocalWSKey option: %s", arg);
        }
        arg = arg2;
    }

    if (id && (*id == 0 || !IsAllAlphameric(id))) {
        Log("For LocalWSKey, -id must be only letters and digits");
        return;
    }

    if (arg && *arg) {
        if (!(n = calloc(1, sizeof(*n))))
            PANIC;
        if (!(n->wskey = strdup(arg)))
            PANIC;
        n->gm = GroupMaskCopy(NULL, gm);
        if (proxyUrl) {
            if (!(n->proxyUrl = strdup(proxyUrl)))
                PANIC;
            n->proxyUrlLen = strlen(n->proxyUrl);
        }
        if (id)
            if (!(n->id = strdup(id)))
                PANIC;
        if (ip)
            if (!(n->ip = strdup(ip)))
                PANIC;
        n->next = wskeys;
        wskeys = n;
    }
}

struct WSKEY *FindWSKey(char *wskey) {
    struct WSKEY *p;

    if (wskey == NULL)
        return FALSE;
    if (*wskey == 0)
        return FALSE;

    for (p = wskeys; p; p = p->next) {
        if (strcmp(wskey, p->wskey) == 0)
            return p;
        ;
    }

    return NULL;
}

void AdminUserObjectError(struct TRANSLATE *t, char *error) {
    struct UUSOCKET *s = &t->ssocket;

    HTTPCode(t, 200, 0);
    HTTPContentType(t, NULL, 1);
    UUSendZ(s, TAG_DOCTYPE_HTML_HEAD "<title>UserObject Error</title></head><body><p>");
    UUSendZ(s, error);
    UUSendZ(s, "</p></body></html>\n");
}

void AdminUserObjectServiceHash(char *service, unsigned char *hash) {
    MD5_CTX md5Context;

    if (service == NULL || *service == 0) {
        memset(hash, 0, 16);
    } else {
        MD5_Init(&md5Context);
        MD5_Update(&md5Context, (unsigned char *)service, (unsigned int)strlen(service));
        MD5_Final(hash, &md5Context);
    }
}

void AdminUserObjectOpenUserDocument(struct USEROBJECTELEMENTCTX *uoec) {
    if (uoec->count == 0)
        if (xmlTextWriterStartElement(uoec->writer, BAD_CAST "userDocument") < 0)
            PANIC;
    uoec->count++;
}

void AdminUserObjectWriteElement(struct USEROBJECTELEMENTCTX *uoec, char *element) {
    struct SESSION *e = uoec->session;

    if (e) {
        char *val;
        char *sessionVar = NULL;
        if (!(sessionVar = malloc(strlen(element) + 16)))
            PANIC;
        sprintf(sessionVar, SESSIONVARPREFIX "%s", element);
        val = VariablesGetValueByGroupMask(e->sessionVariables, sessionVar, uoec->gm);
        if (val && *val) {
            AdminUserObjectOpenUserDocument(uoec);
            if (xmlTextWriterWriteElement(uoec->writer, BAD_CAST element, BAD_CAST val) < 0)
                PANIC;
        }
        FreeThenNull(val);
        FreeThenNull(sessionVar);
    }
}

void AdminUserObjectWriteAddressElement(struct USEROBJECTELEMENTCTX *uoec, char *element) {
    struct SESSION *e = uoec->session;

    if (e) {
        char *val;
        char *sessionVar = NULL;
        if (!(sessionVar = malloc(strlen(element) + 16)))
            PANIC;
        sprintf(sessionVar, SESSIONVARPREFIX "%s", element);
        val = VariablesGetValueByGroupMask(e->sessionVariables, sessionVar, uoec->gm);
        if (val && *val) {
            AdminUserObjectOpenUserDocument(uoec);
            if (uoec->addressOpen == 0) {
                if (xmlTextWriterStartElement(uoec->writer, BAD_CAST "address") < 0)
                    PANIC;
                uoec->addressOpen = 1;
            }
            if (xmlTextWriterWriteElement(uoec->writer, BAD_CAST element, BAD_CAST val) < 0)
                PANIC;
        }
        FreeThenNull(val);
        FreeThenNull(sessionVar);
    }
}

void AdminUserObjectWriteAddressPhoneElement(struct USEROBJECTELEMENTCTX *uoec, char *element) {
    char *parts[] = {"countryCode", "areaCode", "number", "extension", NULL};
    char **part;
    BOOL phoneOpen = 0;
    struct SESSION *e = uoec->session;

    if (e) {
        char *val;
        char *sessionVar = NULL;
        if (!(sessionVar = malloc(strlen(element) + 32)))
            PANIC;
        for (part = parts; *part; part++) {
            sprintf(sessionVar, SESSIONVARPREFIX "%s%c%s", element, toupper(**part), *part + 1);
            val = VariablesGetValueByGroupMask(e->sessionVariables, sessionVar, uoec->gm);
            if (val && *val) {
                AdminUserObjectOpenUserDocument(uoec);
                if (uoec->addressOpen == 0) {
                    if (xmlTextWriterStartElement(uoec->writer, BAD_CAST "address") < 0)
                        PANIC;
                    uoec->addressOpen = 1;
                }
                if (phoneOpen == 0) {
                    if (xmlTextWriterStartElement(uoec->writer, BAD_CAST element) < 0)
                        PANIC;
                    phoneOpen = 1;
                }
                if (xmlTextWriterWriteElement(uoec->writer, BAD_CAST * part, BAD_CAST val) < 0)
                    PANIC;
            }
            FreeThenNull(val);
        }
        if (phoneOpen) {
            if (xmlTextWriterEndElement(uoec->writer) < 0)
                PANIC;
        }
        FreeThenNull(sessionVar);
    }
}

BOOL AdminUserObjectGetUserObjectLoadBalancer(struct TRANSLATE *t,
                                              char *userObjectTicket,
                                              char **errorCode,
                                              char *errorBuffer) {
    char *period;
    size_t idLen;
    struct LBPEER *lbp;
    char *forwardIpHeader;
    char ipBuffer[INET6_ADDRSTRLEN];
    struct sockaddr_storage forwardIp;

    if (userObjectTicket == NULL || (period = strchr(userObjectTicket, '.')) == NULL)
        return FALSE;

    if (forwardIpHeader = FindField(EZPROXYUSEROBJECTIP, t->requestHeaders)) {
        LinkFields(forwardIpHeader);

        int family = NumericHostIp4(forwardIpHeader) ? AF_INET : AF_INET6;

        init_storage(&forwardIp, family, TRUE, 0);
        inet_pton_st(family, forwardIpHeader, &forwardIp);

        if (!is_addrnone(&forwardIp)) {
            /* Check to see if the party making the forward is one of our peers;
               reject if not
            */
            for (lbp = lbPeers; lbp; lbp = lbp->next) {
                if (is_ip_equal(&lbp->ipInterface, &t->sin_addr))
                    break;
            }
            if (lbp == NULL) {
                /* Not a valid site to forward request, return error */
                *errorCode = "999";
                sprintf(errorBuffer, "Unauthorized source IP for %s: %s", EZPROXYUSEROBJECTIP,
                        forwardIpHeader);
                return FALSE;
            }
        }
    }

    /* The purpose of UUMIN is to guard against some ticket getting
       in with a ridiculous length id; technically, impossible, but
       want to guard to avoid any chance of overwriting error buffer
    */
    idLen = UUMIN(period - userObjectTicket, MAXUSEROBJECTTICKET + 1);

    lbp = FindLBPeer(userObjectTicket, idLen, FALSE);

    if (lbp == NULL) {
        *errorCode = "999";
        sprintf(errorBuffer, "Unknown load-balancer peer: %.*s", (int)idLen, userObjectTicket);
        return FALSE;
    }

    if (lbp != myLbPeer) {
        if (forwardIpHeader) {
            *errorCode = "999";
            sprintf(errorBuffer, "Forward loop detected for load-balancer peer: %.*s", (int)idLen,
                    userObjectTicket);
            return FALSE;
        }

        if (UUConnectWithSource2(t->wsocketPtr, lbp->host, lbp->tcpPort,
                                 "AdminUserObjectGetUserObjectLoadBalancer", &myLbPeer->ipInterface,
                                 lbp->useSsl)) {
            *errorCode = "999";
            sprintf(errorBuffer, "Unable to connect to load-balancer peer: %.*s", (int)idLen,
                    userObjectTicket);
            return FALSE;
        }

        UUSend2(t->wsocketPtr, "GET ", 4, t->urlCopy, strlen(t->urlCopy), 0);

        WriteLine(t->wsocketPtr, " HTTP/1.0\r\n%s: %s\r\nConnection: close\r\n\r\n",
                  EZPROXYUSEROBJECTIP, ipToStr(&t->sin_addr, ipBuffer, sizeof ipBuffer));

        if (optionUseRaw) {
            StartRaw(t, 0, 0);
        } else {
            ProcessNonHTML(t);
        }

        return TRUE;
    }

    /* If we got a forwarded IP, change our address to match to adjust auditing and logging */
    if (!is_addrnone(&forwardIp))
        memcpy(&t->sin_addr, &forwardIp, sizeof forwardIp);

    return FALSE;
}

BOOL AdminUserObjectProxyGetUserObject(struct TRANSLATE *t,
                                       char *target,
                                       char **errorCode,
                                       char *errorBuffer) {
    struct UUSOCKET *r = t->wsocketPtr;
    struct WSKEY *p;
    struct PROTOCOLDETAIL *protocolDetail;
    char *host;
    char *colon;
    char *endOfHost, *endOfHostPort;
    PORT port;
    char saveEndOfHost = 0;
    char saveEndOfHostPort = 0;
    char ipBuffer[INET6_ADDRSTRLEN];

    for (p = wskeys; p; p = p->next)
        if (p->proxyUrl && strnicmp(target, p->proxyUrl, p->proxyUrlLen) == 0)
            break;

    if (p == NULL) {
        *errorCode = "999";
        sprintf(errorBuffer, "Missing or invalid proxy wskey");
        return FALSE;
    }

    if (!(ParseProtocolHostPort(target, "http://", &protocolDetail, &host, &endOfHost, &colon,
                                &port, NULL, &endOfHostPort)) ||
        protocolDetail->protocol > PROTOCOLHTTPS) {
        *errorCode = "999";
        sprintf(errorBuffer, "Invalid proxy target");
        return FALSE;
    }

    if (endOfHost)
        saveEndOfHost = *endOfHost;

    if (endOfHostPort)
        saveEndOfHostPort = *endOfHostPort;

    if (endOfHost)
        *endOfHost = 0;

    if (UUConnect2(r, host, port, "AdminUserObjectProxyGetUserObject", protocolDetail->protocol)) {
        *errorCode = "999";
        if (strlen(host) > 255)
            host[255] = 0;
        sprintf(errorBuffer, "Unable to connect to proxy: %s:%d", host, port);
        return FALSE;
    }

    UUSendZ(r, "GET ");

    if (endOfHostPort) {
        /* in case endOfHost and endOfHostPort are the same, restore saved value */
        *endOfHostPort = saveEndOfHostPort;
        UUSendZ(r, endOfHostPort);
    } else {
        UUSendZ(r, "/");
    }

    if (endOfHost)
        *endOfHost = 0;

    WriteLine(r, "&service=getUserObject&wskey=");
    SendHTMLEncoded(r, p->wskey);
    WriteLine(r, " HTTP/1.0\r\nHost: %s", host);

    if (protocolDetail->defaultPort != port)
        WriteLine(r, ":%d", port);

    WriteLine(r, "\r\n%s: %s\r\nConnection: close\r\n\r\n", EZPROXYUSEROBJECTIP "-Proxy",
              ipToStr(&t->sin_addr, ipBuffer, sizeof ipBuffer));

    if (optionUseRaw) {
        StartRaw(t, 0, 0);
    } else {
        ProcessNonHTML(t);
    }

    return TRUE;
}

void AdminUserObjectGetUserObject(struct TRANSLATE *t,
                                  char *userObjectTicket,
                                  char *wskey,
                                  char *target) {
    struct UUSOCKET *s = &t->ssocket;
    xmlBufferPtr buf = NULL;
    char *errorCode = NULL;
    char *error = NULL;
    char errorBuffer[512];
    struct WSKEY *wskeyObject;
    struct USEROBJECTELEMENTCTX uoec;
    char *auditError = NULL;

    memset(&uoec, 0, sizeof(uoec));

    if (AdminUserObjectGetUserObjectLoadBalancer(t, userObjectTicket, &errorCode, errorBuffer))
        return;

    if (errorCode)
        error = errorBuffer;
    else {
        if (optionUserObjectTestMode && (wskey == NULL || *wskey == 0)) {
            if (userObjectTicket && (*userObjectTicket != 0) &&
                (stricmp(userObjectTicket, "proxy") == 0) && target && *target) {
                if (AdminUserObjectProxyGetUserObject(t, target, &errorCode, errorBuffer))
                    return;
                if (errorCode)
                    error = errorBuffer;
            } else if (t->session) {
                uoec.session = t->session;
                uoec.gm = uoec.session->gm;
            } else if (userObjectTicket && (*userObjectTicket != 0)) {
                uoec.session = FindSessionByUserObjectTicket(userObjectTicket);
                if (uoec.session == NULL) {
                    errorCode = "902";
                    error = "Invalid ticket";
                }
            } else {
                errorCode = "902";
                error = "Invalid ticket";
            }
        } else {
            if (wskeyObject = FindWSKey(wskey)) {
                int ipResult = 1;
                if (wskeyObject->ip) {
                    char *range = NULL;
                    if (!(range = strdup(wskeyObject->ip)))
                        PANIC;
                    ipResult = FindUserIP(t, range);
                    FreeThenNull(range);
                }
                if (ipResult == 0) {
                    errorCode = "901";
                    error = "Missing or invalid wskey";
                    auditError = "Valid WSKey from unauthorized IP address";
                } else if (userObjectTicket && (*userObjectTicket != 0)) {
                    if (strcmp(userObjectTicket, "proxy") == 0 && target && *target) {
                        if (AdminUserObjectProxyGetUserObject(t, target, &errorCode, errorBuffer))
                            return;
                        if (errorCode)
                            error = errorBuffer;
                    } else {
                        uoec.session = FindSessionByUserObjectTicket(userObjectTicket);
                        if (uoec.session == NULL) {
                            errorCode = "902";
                            error = "Invalid ticket";
                        } else {
                            uoec.gm = wskeyObject->gm;
                        }
                    }
                } else {
                    errorCode = "902";
                    error = "Invalid ticket";
                }
            } else {
                errorCode = "901";
                error = "Missing or invalid wskey";
            }
        }
    }

    if (auditError == NULL)
        auditError = error ? error : "OK";

    AuditEvent(t, AUDITUSEROBJECTGETUSEROBJECT, uoec.session ? uoec.session->logUserFull : NULL,
               uoec.session, "|Ticket: %s|WSKey: %s|Status: %s",
               userObjectTicket ? userObjectTicket : "", wskey ? wskey : "", auditError);

    if (!(buf = xmlBufferCreate()))
        PANIC;
    if (!(uoec.writer = xmlNewTextWriterMemory(buf, 0)))
        PANIC;
    if (xmlTextWriterStartDocument(uoec.writer, NULL, "UTF-8", NULL) < 0)
        PANIC;

    /* Start root element */
    if (xmlTextWriterStartElement(uoec.writer, BAD_CAST "userObjectResponse") < 0)
        PANIC;

    if (xmlTextWriterStartElement(uoec.writer, BAD_CAST "serviceStatus") < 0)
        PANIC;
    if (xmlTextWriterWriteFormatString(uoec.writer, (uoec.session ? "OK" : "Error")) < 0)
        PANIC;
    /* close serviceStatus */
    if (xmlTextWriterEndElement(uoec.writer) < 0)
        PANIC;

    if (uoec.session != NULL && errorCode == NULL) {
        struct tm rtm;
        char lastAuthenticated[64];

        AdminUserObjectOpenUserDocument(&uoec);

        strftime(lastAuthenticated, sizeof(lastAuthenticated), "%Y-%m-%dT%H:%M:%SZ",
                 UUgmtime_r(&uoec.session->lastAuthenticated, &rtm));
        if (xmlTextWriterWriteElement(uoec.writer, BAD_CAST "lastAuthenticated",
                                      BAD_CAST lastAuthenticated) < 0)
            PANIC;

        AdminUserObjectWriteElement(&uoec, "groupNumber");
        AdminUserObjectWriteElement(&uoec, "groupSymbol");
        AdminUserObjectWriteElement(&uoec, "instNumber");
        AdminUserObjectWriteElement(&uoec, "instSymbol");

        // if (e && e->logUserBrief && *e->logUserBrief) {
        //   if (xmlTextWriterWriteElement(uoec.writer, BAD_CAST "username", BAD_CAST
        //   e->logUserBrief) < 0) PANIC;
        // }

        AdminUserObjectWriteElement(&uoec, "uid");
        AdminUserObjectWriteElement(&uoec, "location");
        AdminUserObjectWriteElement(&uoec, "category");
        AdminUserObjectWriteElement(&uoec, "title");
        AdminUserObjectWriteElement(&uoec, "forename");
        AdminUserObjectWriteElement(&uoec, "middleName");
        AdminUserObjectWriteElement(&uoec, "surname");
        AdminUserObjectWriteElement(&uoec, "nameSuffix");
        AdminUserObjectWriteElement(&uoec, "emailAddress");
        AdminUserObjectWriteElement(&uoec, "dateFormat");
        AdminUserObjectWriteElement(&uoec, "joinDate");
        AdminUserObjectWriteElement(&uoec, "expiryDate");
        AdminUserObjectWriteElement(&uoec, "userGroups");
        AdminUserObjectWriteElement(&uoec, "bannedInRemoteCirculation");
        AdminUserObjectWriteElement(&uoec, "canRequestIfBanned");
        AdminUserObjectWriteElement(&uoec, "clientPresignedCopyright");
        AdminUserObjectWriteElement(&uoec, "attributes");
        AdminUserObjectWriteElement(&uoec, "note1");
        AdminUserObjectWriteElement(&uoec, "note2");
        AdminUserObjectWriteElement(&uoec, "note3");
        AdminUserObjectWriteElement(&uoec, "note4");
        AdminUserObjectWriteElement(&uoec, "note5");
        AdminUserObjectWriteElement(&uoec, "note6");
        AdminUserObjectWriteElement(&uoec, "note7");
        AdminUserObjectWriteElement(&uoec, "note8");
        AdminUserObjectWriteElement(&uoec, "note9");
        AdminUserObjectWriteElement(&uoec, "note10");

        AdminUserObjectWriteAddressElement(&uoec, "addressee");
        AdminUserObjectWriteAddressElement(&uoec, "building");
        AdminUserObjectWriteAddressElement(&uoec, "street");
        AdminUserObjectWriteAddressElement(&uoec, "district");
        AdminUserObjectWriteAddressElement(&uoec, "city");
        AdminUserObjectWriteAddressElement(&uoec, "region");
        AdminUserObjectWriteAddressElement(&uoec, "country");
        AdminUserObjectWriteAddressElement(&uoec, "poBox");
        AdminUserObjectWriteAddressElement(&uoec, "postCode");
        AdminUserObjectWriteAddressPhoneElement(&uoec, "phoneNumber");
        AdminUserObjectWriteAddressPhoneElement(&uoec, "faxNumber");
        if (uoec.addressOpen) {
            if (xmlTextWriterEndElement(uoec.writer) < 0)
                PANIC;
        }

        /* close userDocument if it was ever opened */
        if (uoec.count > 0) {
            if (xmlTextWriterEndElement(uoec.writer) < 0)
                PANIC;
        }
    }

    if (uoec.count == 0) {
        if (errorCode == NULL)
            errorCode = "999";
        if (error == NULL)
            error = "Unknown error";
        if (xmlTextWriterStartElement(uoec.writer, BAD_CAST "errorDetails") < 0)
            PANIC;
        if (xmlTextWriterWriteElement(uoec.writer, BAD_CAST "errorCode", BAD_CAST errorCode) < 0)
            PANIC;
        if (xmlTextWriterWriteElement(uoec.writer, BAD_CAST "errorText", BAD_CAST error) < 0)
            PANIC;
        if (xmlTextWriterEndElement(uoec.writer) < 0)
            PANIC;
    }

    /* close userObjectResponse */
    if (xmlTextWriterEndElement(uoec.writer) < 0)
        PANIC;

    if (xmlTextWriterEndDocument(uoec.writer) < 0)
        PANIC;

    if (xmlTextWriterFlush(uoec.writer) < 0)
        PANIC;

    HTTPCode(t, 200, 0);
    NoCacheHeaders(s);
    HTTPContentType(t, "text/xml", 1);

    UUSendZCRLF(s, (char *)buf->content);

    if (uoec.writer) {
        xmlFreeTextWriter(uoec.writer);
        uoec.writer = NULL;
    }

    if (buf) {
        xmlBufferFree(buf);
        buf = NULL;
    }
}

void provideUserObjectTicket(struct TRANSLATE *t, struct SESSION *e) {
    if (e->userObjectTicket[0] == 0) {
        char *deferredVal = VariablesGetValue(e->sessionVariables, SESSIONDEFERUSEROBJECT);
        BOOL deferred = ExpressionValueTrue(deferredVal);
        FreeThenNull(deferredVal);

        if (deferred) {
            struct USERINFO ui;
            int userLimit;

            VariablesDelete(e->sessionVariables, SESSIONDEFERUSEROBJECT);
            e->cookiesChanged = 1;
            InitUserInfo(t, &ui, NULL, NULL, NULL, NULL);
            FindUser(t, &ui, "userobject.txt", NULL, NULL, &userLimit, 0);
            GroupMaskFree(&ui.gm);
        }

        for (;;) {
            char *q;
            char ticket[MAXUSEROBJECTTICKET];
            BOOL exists;
            if (myLbPeer) {
                StrCpy3(ticket, myLbPeer->id, sizeof(ticket) - 16);
                strcat(ticket, ".");
                q = AtEnd(ticket);
            } else {
                q = ticket;
            }

            for (; q < ticket + MAXUSEROBJECTTICKET - 1;) {
                *q++ = RandChar();
            }
            *q = 0;
            RWLAcquireWrite(&rwlSessions);
            exists = FindSessionByUserObjectTicket(ticket) != NULL;
            if (exists == 0)
                StrCpy3(e->userObjectTicket, ticket, sizeof(e->userObjectTicket));
            RWLReleaseWrite(&rwlSessions);
            if (exists == 0) {
                NeedSave();
                break;
            }
        }
    }
}

// Translate the content of the ezproxyPrivateComputer cookie to attribute-value pairs
//   in the query string of the HTTP URL.
void addPrivateComputerParam(struct TRANSLATE *t, BOOL encode) {
    char *p;
    char *val;
    char *eob = t->buffer + sizeof(t->buffer) - 1;

    p = strchr(t->buffer, 0);  // borrow some buffer space
    strcat(p, "cookie:");
    strcat(p, privateComputerCookieName);
    val = NULL;
    val = ExpressionValue(t, NULL, NULL, p);
    *p = 0;  // return buffer space
    if (val && encode) {
        if (encode) {
            if (strchr(t->buffer, '?') == NULL) {
                AddEncodedField(t->buffer, "?", eob);
            } else {
                AddEncodedField(t->buffer, "&", eob);
            }
            AddEncodedField(t->buffer, privateComputerCookieName, eob);
            AddEncodedField(t->buffer, "=", eob);
            AddEncodedField(t->buffer, val, eob);
            FreeThenNull(val);
        } else {
            if (strchr(t->buffer, '?') == NULL) {
                strcat(t->buffer, "?");
            } else {
                strcat(t->buffer, "&");
            }
            strcat(t->buffer, privateComputerCookieName);
            strcat(t->buffer, "=");
            AddEncodedField(t->buffer, val, eob);
            FreeThenNull(val);
        }
    }
}

void AdminUserObjectGetToken(struct TRANSLATE *t,
                             char *returnURL,
                             char *context,
                             char *groupNumber,
                             char *groupSymbol,
                             char *instNumber,
                             char *instSymbol,
                             char *renew) {
    struct UUSOCKET *s = &t->ssocket;
    struct SESSION *e;
    char *eob = t->buffer + sizeof(t->buffer) - 1;
    BOOL isRenew;

    if (renew && *renew) {
        isRenew = strcmp(renew, "false") != 0;
    } else {
        isRenew = FALSE;
        /* In this scenario, we don't destroy the default ticket since
           we may actually be able to reuse it; in this case, we're just
           forcing login reevaluation to see if the same ticket
           should be reused or if a re-authentication should be
           forced
        */
        if (t->session && optionUserObjectGetTokenAlwaysRenew)
            t->session->reloginRequired = TRUE;
    }

    if ((e = t->session) == NULL || e->reloginRequired || isRenew) {
        strcpy(t->buffer, "/login?");
        if (groupNumber && *groupNumber) {
            strcat(t->buffer, "groupNumber=");
            AddEncodedField(t->buffer, groupNumber, NULL);
            strcat(t->buffer, "&");
        }
        if (groupSymbol && *groupSymbol) {
            strcat(t->buffer, "groupSymbol=");
            AddEncodedField(t->buffer, groupSymbol, NULL);
            strcat(t->buffer, "&");
        }
        if (instNumber && *instNumber) {
            strcat(t->buffer, "instNumber=");
            AddEncodedField(t->buffer, instNumber, NULL);
            strcat(t->buffer, "&");
        }
        if (instSymbol && *instSymbol) {
            strcat(t->buffer, "instSymbol=");
            AddEncodedField(t->buffer, instSymbol, NULL);
            strcat(t->buffer, "&");
        }
        if (isRenew) {
            strcat(t->buffer, "renew=true&");
            if (e) {
                e->reloginRequired = 1;
                e->userObjectTicket[0] = 0;
            }
        }
        sprintf(
            AtEnd(t->buffer), "url=%s%s",
            primaryLoginPortSsl && (t->useSsl || primaryLoginPort == 0) ? myUrlHttps : myUrlHttp,
            "/userObject?service=getToken");

        /* If we are in always renew mode, append renew=false to avoid an endless loop */
        if (optionUserObjectGetTokenAlwaysRenew)
            strcat(t->buffer, "&renew=false");

        /* Note: returnURL and context both enter into EZproxy through t->url, which is always
           dramatically smaller than t->buffer, so the encoded version of these values will always
           fit
        */
        if (returnURL && *returnURL) {
            strcat(t->buffer, "&returnURL=");
            AddEncodedField(t->buffer, returnURL, NULL);
        }
        if (context && *context) {
            strcat(t->buffer, "&context=");
            AddEncodedField(t->buffer, context, NULL);
        }

        MyNameRedirect(t, t->buffer, 0);
        return;
    }

    if (returnURL && *returnURL == 0) {
        returnURL = NULL;
    }

    provideUserObjectTicket(t, e);
    AuditEvent(t, AUDITUSEROBJECTGETTOKEN, e->logUserFull, e, "|Ticket: %s|returnURL: %s",
               e->userObjectTicket, returnURL ? returnURL : "");

    if (returnURL) {
        StrCpyOverlap(t->buffer, returnURL);
        if (strchr(t->buffer, '?') == NULL) {
            strcat(t->buffer, "?");
        } else {
            strcat(t->buffer, "&");
        }
        strcat(t->buffer, "token=");
        AddEncodedField(t->buffer, t->myUrl, eob);
        AddEncodedField(t->buffer, "/userObject?ticket=", eob);
        AddEncodedField(t->buffer, e->userObjectTicket, eob);
        addPrivateComputerParam(t, TRUE);
        if (context && *context) {
            strcat(t->buffer, "&context=");
            AddEncodedField(t->buffer, context, eob);
        }

        SimpleRedirect(t, t->buffer, 0);
    } else {
        AdminHeader(t, AHNOTOPMENU, "User Object Test");

        UUSendZ(
            s,
            "<p>Normally, a returnURL is required, but since Option UserObjectTestMode is enabled, "
            "a direct URL to retrieve the userObject is provided.</p>\n<p>The token returned for "
            "this request is ");

        strcpy(t->buffer, t->myUrl);
        strcat(t->buffer, "/userObject?ticket=");
        AddEncodedField(t->buffer, e->userObjectTicket, eob);
        addPrivateComputerParam(t, FALSE);
        if (context && *context) {
            strcat(t->buffer, "&context=");
            AddEncodedField(t->buffer, context, eob);
        }
        SendHTMLEncoded(s, t->buffer);

        UUSendZ(
            s,
            "</p><p>To request the User Object, an application would take the token and append "
            "service=getUserObject and wskey= followed by a valid wskey and retrieve the resulting "
            "URL directly.</p><p><a href='");

        UUSendZ(s, t->buffer);

        UUSendZ(s, "&service=getUserObject'>");
        UUSendZ(
            s,
            "View User Object</a></p>\n<p><a href='/logout?url=/userObject?service=getToken'>Log "
            "in again for another User Object Test</a></p>\n");

        AdminFooter(t, FALSE);
    }
}

void AdminUserObjectProxyGetToken(struct TRANSLATE *t, char *returnURL, char *token) {
    struct UUSOCKET *s = &t->ssocket;
    char *eob = t->buffer + sizeof(t->buffer) - 1;

    if (returnURL && *returnURL) {
        StrCpyOverlap(t->buffer, returnURL);
    } else {
        strcpy(t->buffer, t->myUrl);
    }

    if (strchr(t->buffer, '?') == NULL) {
        strcat(t->buffer, "?");
    } else {
        strcat(t->buffer, "&");
    }

    strcat(t->buffer, "token=");
    AddEncodedField(t->buffer, t->myUrl, eob);
    AddEncodedField(t->buffer, "/userObject?ticket=proxy&target=", eob);
    AddDoubleEncodedField(t->buffer, token, eob);

    addPrivateComputerParam(t, TRUE);

    if (returnURL && *returnURL) {
        SimpleRedirect(t, t->buffer, 0);
        return;
    }

    if (optionUserObjectTestMode == 0) {
        AdminUserObjectError(t, "No valid return URL provided for user object proxy");
        return;
    }

    AdminHeader(t, AHNOTOPMENU, "User Object Test");

    UUSendZ(s,
            "<p>Normally, a returnURL is required, "
            "but since Option UserObjectTestMode is enabled, a direct URL to retrieve the "
            "userObject is provided.</p>\n"
            "<p>The token returned for this request is ");
    SendHTMLEncoded(s, t->buffer);
    UUSendZ(s,
            "</p><p>To request the User Object, an application would take the token and append "
            "service=getUserObject"
            " and wskey= followed by a valid wskey and retrieve the resulting URL directly.</p>"
            "<p><a href='");
    UUSendZ(s, t->buffer);
    UUSendZ(s, "&service=getUserObject'>");
    UUSendZ(s,
            "View User Object</a></p>\n\
<p><a href='/logout?url=/userObject?service=getToken'>Log in again for another User Object Test</a></p>\n");
    AdminFooter(t, FALSE);
}

void AdminUserObjectPutUserObject(struct TRANSLATE *t, char *query, char *post, char *wskey) {
    static const int FiveMinutesInSeconds = 5 * 60;
    char *errorCode = NULL;
    char *error = NULL;
    char *auditMessage = NULL;
    char *eob = t->buffer + sizeof(t->buffer) - 1;
    char *val;
    struct WSKEY *wskeyObject;
    int i;
    int k;
    int l;
    int totalVariableDataLength = 0;
    char *name;
    char *endOfName;
    char *prevCellValue;
    char *varIndex;
    char *indexValue;
    char *name2 = NULL;
    char *buffer;
    struct VARIABLES *lastIndex;
    struct VARIABLES *newVariables;
    enum TOKENTYPE tokenType;
    struct SESSION *newSession = NULL;
    char *login = NULL;

    // Check Content-Type for "application/x-www-form-urlencoded".
    val = NULL;
    val = ExpressionValue(t, NULL, NULL, "http:Content-Type");
    if (val && (stricmp(val, "application/x-www-form-urlencoded") == 0)) {
        // All is well.
    } else {
        errorCode = "903";
        error = "Content-Type should be 'application/x-www-form-urlencoded'";
    }
    FreeThenNull(val);

    // Check WSKey
    if (error == NULL) {
        if (optionUserObjectTestMode && (wskey == NULL || *wskey == 0)) {
            // All is well.
        } else {
            if (wskeyObject = FindWSKey(wskey)) {
                int ipResult = 1;
                if (wskeyObject->ip) {
                    char *range = NULL;
                    if (!(range = strdup(wskeyObject->ip)))
                        PANIC;
                    ipResult = FindUserIP(t, range);
                    FreeThenNull(range);
                }
                if (ipResult == 0) {
                    errorCode = "902";
                    error = "Valid WSKey from unauthorized IP address";
                } else {
                    // All is well.
                }
            } else {
                errorCode = "901";
                error = "Missing or invalid wskey";
            }
        }
    }

    if (error == NULL) {
        /* TODO: TEST ALL USEROBJECT STUFF. XXX FIXME */
        lastIndex = VariablesNew("lastIndex", FALSE);
        newVariables = VariablesNew("newVariables", FALSE);
        if (!(name2 = (char *)malloc(128 + 1)))
            PANIC;
        /* Loop through the query's and the post's URL encoded properties and define the session
         * variables. */
        /* This was upgraded to allow multi-valued properties to come in and be represented as an
         * array. */
        for (i = 0; (!error) && (i < 2); i++) {
            buffer = (i == 0 ? query : post);
            while ((!error) && (NextKEV(buffer, &name, &val, &buffer, 0))) {
                if (debugLevel > 8000) {
                    Log("NextKEV '%s', '%s', '%s'", NULLSAVESTR(name), NULLSAVESTR(val),
                        NULLSAVESTR(buffer));
                }
                l = strlen(name) + sizeof(SESSIONVARPREFIX) + 1;
                if (l > sizeof(name2)) {
                    free(name2);
                    if (!(name2 = (char *)malloc(l)))
                        PANIC;
                }
                *name2 = 0;
                if (!StartsWith(name, SESSIONVARPREFIX)) {
                    strcat(name2, SESSIONVARPREFIX);
                }
                strcat(name2, name);
                if (debugLevel > 8000) {
                    Log("name '%s'", NULLSAVESTR(name2));
                }
                tokenType = IsVarNameOrCompareOper(name2, &endOfName);
                if ((tokenType == NAME) && ((*endOfName == 0) || (*endOfName == '['))) {
                    if (val == NULL) {
                        val = "";
                    }
                    if (debugLevel > 8000) {
                        Log("value '%s'", val);
                    }
                    totalVariableDataLength += strlen(val);
                    if (totalVariableDataLength <= (64 * 1024)) {
                        indexValue = VariablesGetValue(lastIndex, name2);
                        if (indexValue == NULL) {
                            VariablesSetValue(
                                lastIndex, name2,
                                "0                        "); /* the extra spaces allocate space for
                                                                 future values in the 'else' block
                                                               */
                            k = 0;
                        } else {
                            k = atoi(indexValue);
                            k++;
                            sprintf(indexValue, "%" PRIdMAX "", (intmax_t)k);
                            VariablesSetValue(lastIndex, name2, indexValue);
                            FreeThenNull(indexValue);
                        }
                        if (k == 0) {
                            /* store it as a scalar variable for now, may get changed later. */
                            VariablesSetValue(newVariables, name2, val);
                        } else if (k == 1) {
                            /* change a scalar variable into an array. */
                            prevCellValue = VariablesGetValue(newVariables, name2);
                            varIndex = ExpressionVariableIndexNum(name2, 0);
                            VariablesSetValue(newVariables, varIndex, prevCellValue);
                            FreeThenNull(varIndex);
                            VariablesDelete(newVariables, name2);
                            varIndex = ExpressionVariableIndexNum(name2, 1);
                            VariablesSetValue(newVariables, varIndex, val);
                            FreeThenNull(varIndex);
                        } else if (k <= 999) {
                            /* store the next sequential cell into the array. */
                            varIndex = ExpressionVariableIndexNum(name2, k);
                            VariablesSetValue(newVariables, varIndex, val);
                            FreeThenNull(varIndex);
                        } else {
                            errorCode = "909";
                            error = "Invalid number of variables > 1000";
                        }
                    } else {
                        errorCode = "910";
                        error = "Invalid data > 65536 bytes";
                    }
                } else {
                    errorCode = "908";
                    error = "Invalid variable name char";
                }
            }
        }
        FreeThenNull(name2);
    }
    if (debugLevel > 8000) {
        LogVariables(lastIndex, "INDEX LIST");
    }
    VariablesFree(&lastIndex);
    if (debugLevel > 8000) {
        LogVariables(newVariables, "NEW INCOMING");
    }

    if (error == NULL) {
        VariablesMerge(t->localVariables, newVariables, NULL);
        login = VariablesGetValue(t->localVariables, SESSIONVARPREFIX "uid");
        if (debugLevel > 8000) {
            Log("login user name for new session '%s'", login);
        }
        if ((login == NULL) || (*login == 0)) {
            login = VariablesGetValue(t->localVariables, SESSIONVARPREFIX "uid[0]");
            if (debugLevel > 8000) {
                Log("login user name[0] for new session '%s'", login);
            }
        }
        if ((login != NULL) && (*login != 0)) {
            time_t now;
            UUtime(&now);
            newSession = StartSession(login, 0, NULL);
            if (newSession != NULL) {
                if (debugLevel > 8000) {
                    Log("new session logUserFull '%s'", NULLSAVESTR(newSession->logUserFull));
                }
                newSession->accessed = newSession->confirmed = newSession->lastAuthenticated =
                    newSession->created;
                newSession->active = 1;
                newSession->expirable = 1;
                newSession->lifetime = FiveMinutesInSeconds;  // session dies 5 minutes from now.
                newSession->sin_addr = t->sin_addr;
                GroupMaskClear(newSession->gm);
                GroupMaskNot(newSession->gm);
                SetLogUserBrief(newSession);
                provideUserObjectTicket(t, newSession);
                MergeSessionVariables(t, newSession);
                VariablesReset(t->localVariables);  // because the variables belong to newSession,
                                                    // not to t->session.
                if (debugLevel > 8000) {
                    LogVariables(newSession->sessionVariables, "SESSION");
                }
            } else {
                errorCode = "905";
                error = "User count limit exceeded";
            }
        } else {
            errorCode = "906";
            error = "The " SESSIONVARPREFIX "uid variable isn't defined";
        }
        FreeThenNull(login);
    }
    VariablesFree(&newVariables);

    if (auditMessage == NULL) {
        auditMessage = error ? error : "OK";
    }

    AuditEvent(t, AUDITUSEROBJECTPUTUSEROBJECT, newSession ? newSession->logUserFull : NULL,
               newSession, "|Ticket: %s|WSKey: %s|Status: %s",
               newSession ? NULLSAVESTR(newSession->userObjectTicket) : "", NULLSAVESTR(wskey),
               auditMessage);

    HTTPCode(t, 200, 0);
    NoCacheHeaders(&(t->ssocket));
    HTTPContentType(t, "text/xml", 1);
    UUSendZ(&(t->ssocket), "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
    if (error == NULL) {
        UUSendZ(&(t->ssocket), "<userObjectResponse><serviceStatus>OK</serviceStatus><token>");
        *(t->buffer) = 0;
        AddHTMLEncodedField(t->buffer, t->myUrl, eob);
        AddHTMLEncodedField(t->buffer, "/userObject?ticket=", eob);
        AddHTMLEncodedField(t->buffer, newSession->userObjectTicket, eob);
        UUSendZ(&(t->ssocket), t->buffer);
        UUSendZ(&(t->ssocket), "</token></userObjectResponse>");
    } else {
        UUSendZ(
            &(t->ssocket),
            "<userObjectResponse><serviceStatus>Error</serviceStatus><errorDetails><errorCode>");
        UUSendZ(&(t->ssocket), errorCode);
        UUSendZ(&(t->ssocket), "</errorCode><errorText>");
        UUSendZ(&(t->ssocket), error);
        UUSendZ(&(t->ssocket), "</errorText></errorDetails></userObjectResponse>");
    }
}

void AdminUserObject(struct TRANSLATE *t, char *query, char *post) {
    const int FFSERVICE = 0;
    const int FFTICKET = 1;
    const int FFRETURNURL = 2;
    const int FFCONTEXT = 3;
    const int FFGROUPNUMBER = 4;
    const int FFINSTNUMBER = 5;
    const int FFGROUPSYMBOL = 6;
    const int FFINSTSYMBOL = 7;
    const int FFWSKEY = 8;
    const int FFTARGET = 9;
    const int FFTOKEN = 10;
    const int FFRENEW = 11;
    /* FindFormFields normally takes the first value provided.  In the case
       of serivce and wskey, an application is to append these values, so we
       want to always take the last value provided, overriding any instance
       where a user may have tried to plug in different values to confuse
       things in the token URL that is returned to an application
    */
    struct FORMFIELD ffs[] = {
        {"service", NULL, 0, 0, 0, FORMFIELDTAKELASTVALUE},
        {"ticket", NULL, 0, 0, MAXUSEROBJECTTICKET},
        {"returnURL", NULL, 0, 0, 0},
        {"context", NULL, 0, 0, 0, FORMFIELDNOTRIMCTRL},
        {"groupNumber", NULL, 0, 0, 10},
        {"instNumber", NULL, 0, 0, 10},
        {"groupSymbol", NULL, 0, 0, 10},
        {"instSymbol", NULL, 0, 0, 10},
        {"wskey", NULL, 0, 0, 80, FORMFIELDTAKELASTVALUE},
        {"target", NULL, 0, 0, 0},
        {"token", NULL, 0, 0, 0},
        {"renew", NULL, 0, 0, 0},
        {NULL, NULL, 0, 0, 0}
    };
    char *service;
    char *postCopy;
    char *queryCopy;
    char *groupNumber;
    char *instNumber;
    char *groupSymbol;
    char *instSymbol;

    if ((usageLogUser & ULUUSEROBJECT) == 0) {
        HTTPCode(t, 404, 1);
        return;
    }

    if (wskeys == NULL) {
        static BOOL warn = TRUE;

        if (warn) {
            warn = FALSE;
            Log("No LocalWSKey values defined; User Object will not be retrievable");
        }
    }

    if (post) {
        if (!(postCopy = strdup(post)))
            PANIC;
    } else {
        postCopy = NULL;
    }
    if ((!post) && query) {
        if (!(queryCopy = strdup(query)))
            PANIC;
    } else {
        queryCopy = NULL;
    }
    FindFormFields(query, post, ffs);

    groupNumber = IsOneOrMoreDigits(ffs[FFGROUPNUMBER].value) ? ffs[FFGROUPNUMBER].value : NULL;
    instNumber = IsOneOrMoreDigits(ffs[FFINSTNUMBER].value) ? ffs[FFINSTNUMBER].value : NULL;
    groupSymbol = ffs[FFGROUPSYMBOL].value;
    instSymbol = ffs[FFINSTSYMBOL].value;

    Trim(ffs[FFTICKET].value, TRIM_CTRL);
    Trim(ffs[FFRETURNURL].value, TRIM_CTRL);
    Trim(ffs[FFWSKEY].value, TRIM_CTRL);
    struct REDIRECTSAFEORNEVERPROXYURLDATABASE *RedirectSafeOrNeverProxyURLDatabase;
    if (!(RedirectSafeOrNeverProxyURLDatabase =
              calloc(1, sizeof(struct REDIRECTSAFEORNEVERPROXYURLDATABASE))))
        PANIC;
    RedirectSafeOrNeverProxyURLDatabase =
        RedirectSafeOrNeverProxyURL(ffs[FFRETURNURL].value, 1, RedirectSafeOrNeverProxyURLDatabase);

    if (ffs[FFRETURNURL].value && *ffs[FFRETURNURL].value &&
        RedirectSafeOrNeverProxyURLDatabase->result == 0) {
        HTMLErrorPage(t, "Unauthorized URL",
                      "<p>The hostname for returnURL must be explicitly authorized using <a "
                      "href='http://www.oclc.org/support/documentation/ezproxy/cfg/redirectsafe/"
                      "'>RedirectSafe</a>.</p>\n");
        FreeThenNull(RedirectSafeOrNeverProxyURLDatabase);
        return;
    }

    if (service = ffs[FFSERVICE].value) {
        if (strcmp(service, "getUserObject") == 0) {
            AdminUserObjectGetUserObject(t, ffs[FFTICKET].value, ffs[FFWSKEY].value,
                                         ffs[FFTARGET].value);
        } else if (strcmp(service, "getToken") == 0) {
            if ((ffs[FFRETURNURL].value == NULL || *ffs[FFRETURNURL].value == 0) &&
                optionUserObjectTestMode == FALSE) {
                AdminUserObjectError(t, "ReturnURL missing");
            } else {
                AdminUserObjectGetToken(t, ffs[FFRETURNURL].value, ffs[FFCONTEXT].value,
                                        groupNumber, groupSymbol, instNumber, instSymbol,
                                        ffs[FFRENEW].value);
            }
        } else if (optionProxyUserObject && strcmp(service, "proxyGetToken") == 0) {
            AdminUserObjectProxyGetToken(t, ffs[FFRETURNURL].value, ffs[FFTOKEN].value);
        } else if (optionProxyUserObject && strcmp(service, "putUserObject") == 0) {
            AdminUserObjectPutUserObject(t, queryCopy, postCopy, ffs[FFWSKEY].value);
        } else {
            AdminUserObjectError(t, "No valid service specified");
        }
    } else {
        AdminUserObjectError(t, "No service specified");
    }

    FreeThenNull(RedirectSafeOrNeverProxyURLDatabase);
    FreeThenNull(postCopy);
    FreeThenNull(queryCopy);
}
