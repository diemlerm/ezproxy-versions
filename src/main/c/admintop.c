/**
 * / (root) URL handling.
 */

#define __UUFILE__ "admintop.c"

#include "common.h"
#include "uustring.h"

#include "admintop.h"

void AdminTop(struct TRANSLATE *t, char *query, char *post) {
    const int ATURL = 0;
    const int ATQURL = 1;
    struct FORMFIELD ffs[] = {
        {"url",  NULL, 0, 0, 0},
        {"qurl", NULL, 0, 0, 0},
        {NULL,   NULL, 0, 0, 0}
    };

    FindFormFields(query, post, ffs);

    if (ffs[ATURL].value || ffs[ATQURL].value) {
        char *q = ffs[ATQURL].value ? "q" : "";

        HTMLHeader(t, htmlHeaderOmitCache);
        WriteLine(&t->ssocket,
                  "<p>This URL starts with an unsupported format %s/?%surl=.</p><p>You probably "
                  "intended to start with %s/<strong>login</strong>?%surl=.</p><p>Please change "
                  "your URL to include login before ?%surl.</p>",
                  t->myUrl, q, t->myUrl, q, q);
        return;
    }

    MyNameRedirect(t, "/login", 0);
}
