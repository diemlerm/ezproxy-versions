/**
 * EZproxy 3.x and later LDAP user authentication
 *
 * This module implements LDAP user authentication using OpenLDAP.  This
 * version supercedes the original support which implemented a limited,
 * direct bind approach.  The original method still exists in usrldap.c.
 *
 * This authentication method supports common conditions and actions.
 *
 * https://github.com/rroemhild/docker-test-openldap works very well for testing.
 * docker run --rm -p 10389:10389 -p 10636:10636 rroemhild/test-openldap
 * See the github page for the various pre-existing accounts.
 */

#define __UUFILE__ "usrldap2.c"

#include "common.h"
#include "expression.h"
#include "variables.h"
#include "obscure.h"
#include "usrldap2.h"

#ifdef WIN32
/* io.h, fcntl.h and share.h needed for _sopen in Win32 */
#include <io.h>
#include <fcntl.h>
#include <share.h>
#include <lm.h>
#endif

/* Active Directory error codes that may be unknown on other platforms */
#ifndef UF_ACCOUNTDISABLE
#define UF_ACCOUNTDISABLE 0x0002
#endif
#ifndef UF_LOCKOUT
#define UF_LOCKOUT 0x0010
#endif
#ifndef UF_PASSWD_CANT_CHANGE
#define UF_PASSWD_CANT_CHANGE 0x0040
#endif
#ifndef UF_DONT_EXPIRE_PASSWD
#define UF_DONT_EXPIRE_PASSWD 0x10000
#endif

#ifndef ERROR_LOGON_FAILURE
#define ERROR_LOGON_FAILURE 1326
#endif
#ifndef ERROR_PASSWORD_EXPIRED
#define ERROR_PASSWORD_EXPIRED 1330
#endif
#ifndef ERROR_ACCOUNT_EXPIRED
#define ERROR_ACCOUNT_EXPIRED 1793
#endif
#ifndef ERROR_PASSWORD_MUST_CHANGE
#define ERROR_PASSWORD_MUST_CHANGE 1907
#endif

/* NetWare error codes that may be unknown on other platforms */

/* Password expired, was able to login on grace login, should change password */
#ifndef DSERR_PASSWORD_EXPIRED
#define DSERR_PASSWORD_EXPIRED -223
#endif

/* Password expired, all grace logins used up */
#ifndef DSERR_BAD_PASSWORD
#define DSERR_BAD_PASSWORD -222
#endif

/* Account disabled by expiration */
#ifndef DSERR_LOG_ACCOUNT_EXPIRED
#define DSERR_LOG_ACCOUNT_EXPIRED -220
#endif

#include <sys/stat.h>

#include "usr.h"

#include <ldap.h>

/* If there are ever more than 10 LDAPTYPE values, need to adjust ltc in make context */

enum LDAPTYPE { LTUNKNOWN, LTAD, LTNDS, LTLAST };

char *ldapContextHeader = "ldap:";

struct MEMBERATTRIBUTE {
    struct MEMBERATTRIBUTE *next;
    char *attribute;
};

struct LDAPCTX {
    char *dn;
    LDAP *ldTest;
    char *searchFilter;
    char *logDn;
    char *bindUser;
    char *bindPass;
    char *bindUri;
    char *sslCipherSuite;
    struct MEMBERATTRIBUTE *maSpecified;
    LDAP *ld, *ld2;
    LDAPMessage *res;
    LDAPURLDesc *lud;
    SSL_CTX *bindCertCtx;

    int bindCert;
    int version;
    int timeout;
    int maxResults;
    int rcUserBind;
    enum STARTTLSTYPE startTLS;

    BOOL useSearchFilterWhenReadingAttributes;
    BOOL loginDisabled;
    BOOL lastDisabled;
    BOOL lastExpired;
    BOOL expired;
    BOOL ignorePassword;
    BOOL disableReferralChasing;
    BOOL memberUser;

    char passwordForm[MAXMENU];

    /* This function called each time a "cmd reset" occurs,
           which happens when a new line directive is about to be parsed that is not
           linked to a previous directive by ;
        */
    void (*cmdResetFunc)(struct TRANSLATE *t, struct USERINFO *uip, void *context);
};

/**
 * This function allows the SSLCipherSuite to be set on a specific LDAP structure
 * at SSL connect time.
 *
 * Originally, it seemed that ldap_set_option on LDAP_OPT_X_TLS_CIPHER_SUITE would
 * behave in this manner, but in testing, it only operated globally which was not
 * acceptable when users may be connecting to varied LDAP servers.
 */
void FindUserLDAP2SetSSLCipherSuite(LDAP *ld, SSL *ssl, SSL_CTX *ctx, char *sslCipherSuite) {
    if (sslCipherSuite && *sslCipherSuite) {
        if (!(SSL_set_cipher_list(ssl, sslCipherSuite))) {
            Log("LDAP SSLCipherSuite invalid: %s", sslCipherSuite);
        }
    }
}

char *UUToUnicode(char *u, char *a) {
    for (;; a++) {
        *u++ = *a;
        *u++ = 0;
        if (*a == 0)
            break;
    }

    return u - 2;
}

int UU_ldap_bind_st(struct LDAPCTX *ldapCtx, LDAP *ld, const char *who, const char *cred) {
    int rc;
    int method = LDAP_AUTH_SIMPLE;

    if (ldapCtx->timeout) {
        struct timeval tv;
        tv.tv_sec = ldapCtx->timeout;
        tv.tv_usec = 0;
        ldap_set_option(ld, LDAP_OPT_NETWORK_TIMEOUT, &tv);
    }

    // StartTLS is only supported by ldap, not ldaps
    if (ldapCtx->startTLS == STENABLED && stricmp(ldapCtx->lud->lud_scheme, "ldap") == 0) {
        // Only attempt to start TLS if the handlers aren't already in place
        if (ldap_tls_inplace(ld) == 0) {
            rc = ldap_start_tls_s(ld, NULL, NULL);
            if (rc != LDAP_SUCCESS) {
                return rc;
            }
        }
    }

    rc = ldap_bind_s(ld, who, cred, method);
}

int LDAPExtendedErrorCode(char *extendedError, enum LDAPTYPE *lt) {
    char *p;
    int rc = -1;
    unsigned int rc2;

    if (lt)
        *lt = LTUNKNOWN;

    if (extendedError && *extendedError) {
        if (strncmp(extendedError, "80090308", 8) == 0) {
            if (lt)
                *lt = LTAD;

            p = strstr(extendedError, ", data ");
            if (p) {
                if (sscanf(p + 7, "%x", &rc2) == 0) {
                    rc = 0;
                } else {
                    /* In th original version of this code, there was no unsigned int rc2 involved.
                       My guess is that a compiler is picking up the discrepancy of %x reading an
                       unsigned value and plopping it into a signed variable, which led to
                       this code that transforms the unsigned back to signed.
                       INT_MAX is undefined on Windows, so this code doesn't work. There is
                       other use of INT_MAX in main.c with an unsigned int, so I'm concerned that
                       a correction there may impact here.  Since they are both int, they are both
                       the same size, and the logic is just trying to get the value from one to the
                       other without really transforming its internal representation, so I've
                       changed the code to use a typecast to move the value without transformation.
                     */
                    *((unsigned int *)&rc) = rc2;
                    /* rc = (rc2 > INT_MAX) ? -((~rc2)+1) : rc2; */
                }
                if (rc != ERROR_LOGON_FAILURE && rc != ERROR_PASSWORD_EXPIRED &&
                    rc != ERROR_ACCOUNT_EXPIRED && rc != ERROR_PASSWORD_MUST_CHANGE)
                    rc = 0;
            }
        }
        if (strncmp(extendedError, "NDS error:", 10) == 0) {
            if (lt)
                *lt = LTNDS;

            p = strrchr(extendedError, '(');
            if (p) {
                if (sscanf(p + 1, "%d", &rc) == 0)
                    rc = 0;
                if (rc != DSERR_PASSWORD_EXPIRED && rc != DSERR_PASSWORD_EXPIRED &&
                    rc != DSERR_BAD_PASSWORD && rc != DSERR_LOG_ACCOUNT_EXPIRED)
                    rc = 0;
            }
        }
    }

    return rc;
}

int LDAPADChangeUserPassword(struct USERINFO *uip,
                             LDAP *ld,
                             char *dn,
                             char *oldPassword,
                             char *newPassword,
                             BOOL debug) {
    int err = 1;
    LDAPMod modNewPassword;
    LDAPMod modOldPassword;
    LDAPMod *modEntry[3];
    struct berval newPwdBerVal;
    struct berval oldPwdBerVal;
    struct berval *newPwd_attr[2];
    struct berval *oldPwd_attr[2];
    char uOldPassword[512];
    char uNewPassword[512];
    char *uOldPasswordEnd;
    char *uNewPasswordEnd;

    /* Build an array of LDAPMod. */

    /* For setting unicodePwd, this MUST be a double op. */
    modEntry[0] = &modOldPassword;
    modEntry[1] = &modNewPassword;
    modEntry[2] = NULL;

    /* Build mod struct for unicodePwd Delete. */
    modOldPassword.mod_op = LDAP_MOD_DELETE | LDAP_MOD_BVALUES;
    modOldPassword.mod_type = "unicodePwd";
    modOldPassword.mod_vals.modv_bvals = oldPwd_attr;

    /* Build mod struct for unicodePwd Add. */
    modNewPassword.mod_op = LDAP_MOD_ADD | LDAP_MOD_BVALUES;
    modNewPassword.mod_type = "unicodePwd";
    modNewPassword.mod_vals.modv_bvals = newPwd_attr;

    uOldPasswordEnd = UUToUnicode(uOldPassword, "\"");
    uOldPasswordEnd = UUToUnicode(uOldPasswordEnd, oldPassword);
    uOldPasswordEnd = UUToUnicode(uOldPasswordEnd, "\"");

    uNewPasswordEnd = UUToUnicode(uNewPassword, "\"");
    uNewPasswordEnd = UUToUnicode(uNewPasswordEnd, newPassword);
    uNewPasswordEnd = UUToUnicode(uNewPasswordEnd, "\"");

    /* Password will be single valued, so we only have one element. */
    oldPwd_attr[0] = &oldPwdBerVal;
    oldPwd_attr[1] = NULL;
    newPwd_attr[0] = &newPwdBerVal;
    newPwd_attr[1] = NULL;

    /* Build the BER structures with the UNICODE passwords w/quotes. */
    newPwdBerVal.bv_len = uNewPasswordEnd - uNewPassword;
    newPwdBerVal.bv_val = uNewPassword;
    oldPwdBerVal.bv_len = uOldPasswordEnd - uOldPassword;
    oldPwdBerVal.bv_val = uOldPassword;

    /* Perform single modify. */
    err = ldap_modify_s(ld, dn, modEntry);

    if (debug && err != LDAP_SUCCESS) {
        char *extendedError = NULL;
        ldap_get_option(ld, LDAP_OPT_ERROR_STRING, &extendedError);
        UsrLog(uip, "LDAP AD password change failed for %s: %d %s", dn, err,
               extendedError ? extendedError : "(no extended error available)");
        FreeThenNull(extendedError);
    }

    return err;
}

int LDAPNDSChangeUserPassword(struct USERINFO *uip,
                              LDAP *ld,
                              char *dn,
                              char *oldPassword,
                              char *newPassword,
                              BOOL debug) {
    /* see http://www.phpdiscuss.com/article.php?group=php.bugs&id=%3Cbug-27060%40bugs.php.net%3E */
    int err = 1;
    char *strvalsold[2];
    char *strvalsnew[2];
    LDAPMod modNewPassword;
    LDAPMod modOldPassword;
    LDAPMod *modEntry[3];
    LDAPControl simplePassword_control;
    LDAPControl *ldap_controls[2];

    strvalsold[0] = oldPassword;
    strvalsold[1] = NULL;
    strvalsnew[0] = newPassword;
    strvalsnew[1] = NULL;

    /* Build an array of LDAPMod. */

    modEntry[0] = &modOldPassword;
    modEntry[1] = &modNewPassword;
    modEntry[2] = NULL;

    /* Build mod struct for userPassword Delete. */
    modOldPassword.mod_op = LDAP_MOD_DELETE;
    modOldPassword.mod_type = "userPassword";
    modOldPassword.mod_vals.modv_strvals = strvalsold;

    /* Build mod struct for userPassword Add. */
    modNewPassword.mod_op = LDAP_MOD_ADD;
    modNewPassword.mod_type = "userPassword";
    modNewPassword.mod_vals.modv_strvals = strvalsnew;

    /* This is needed for simple password, which we aren't addressing at this time */
    /* Setup the SimplePassword server side ldap control*/
    simplePassword_control.ldctl_oid = "2.16.840.1.113719.1.27.101.5";
    simplePassword_control.ldctl_iscritical = 1;
    simplePassword_control.ldctl_value.bv_val = NULL;
    simplePassword_control.ldctl_value.bv_len = 0;

    ldap_controls[0] = &simplePassword_control;
    ldap_controls[1] = NULL;
    /* End unused simple password variables */

    /* Perform single modify. */
    err = ldap_modify_s(ld, dn, modEntry);

    if (debug && err != LDAP_SUCCESS) {
        char *extendedError = NULL;
        ldap_get_option(ld, LDAP_OPT_ERROR_STRING, &extendedError);
        UsrLog(uip, "LDAP NDS password change failed for %s: %d %s", dn, err,
               extendedError ? extendedError : "(no extended error available)");
        FreeThenNull(extendedError);
    }

    return err;
}

char *LDAPFilter(char *baseFilter, char *attr, char *user) {
    char *fp, *filter;
    size_t filterLen;
    char *u;

    if (baseFilter && *baseFilter == 0)
        baseFilter = NULL;

    if (user == NULL)
        user = "";

    filterLen = strlen(attr) +
                strlen(user) * 2 + /* double in case we have to escape any meta characters */
                (baseFilter == NULL ? 0 : strlen(baseFilter)) +
                20; /* padding for inserted parentheses, equal sign, and null */

    if (!(fp = filter = malloc(filterLen)))
        PANIC;

    /* If we have a filter, combine with other filter by forming (&(existingfilter)(attr=value)) but
       if there is no filter, just use (attr=value)
    */
    if (baseFilter) {
        if (*user) {
            *fp++ = '(';
            *fp++ = '&';
        }
        if (*baseFilter != '(')
            *fp++ = '(';
        strcpy(fp, baseFilter);
        fp = strchr(fp, 0);
        if (*baseFilter != '(')
            *fp++ = ')';
    }

    if (*user) {
        sprintf(fp, "(%s=", attr);
        fp = strchr(fp, 0);

        for (u = user; *u; u++) {
            if (strchr("*()\\", *u))
                *fp++ = '\\';
            *fp++ = *u;
        }

        *fp++ = ')'; /* close (attr=value) */

        if (baseFilter)
            *fp++ = ')'; /* close (&...) */
    }

    *fp = 0;

    return filter;
}

char *FindUserLDAP2MakeContext(char *ldapUrl, char *dn, char *password, enum LDAPTYPE lt) {
    EVP_MD_CTX *mdctx = NULL;
    unsigned char md_value[EVP_MAX_MD_SIZE];
    unsigned int md_len;
    char *hex1, *hex2;
    char *ctx;
    char ltc;

    ltc = lt + '0';

    if (!(mdctx = EVP_MD_CTX_new()))
        PANIC;
    EVP_DigestInit(mdctx, EVP_sha1());
    EVP_DigestUpdate(mdctx, &ltc, 1);
    EVP_DigestUpdate(mdctx, ldapUrl, strlen(ldapUrl));
    EVP_DigestUpdate(mdctx, dn, strlen(dn));
    EVP_DigestFinal(mdctx, md_value, &md_len);
    hex1 = Encode64Binary(NULL, md_value, md_len);

    EVP_DigestInit(mdctx, EVP_sha1());
    EVP_DigestUpdate(mdctx, &ltc, 1);
    EVP_DigestUpdate(mdctx, ldapUrl, strlen(ldapUrl));
    EVP_DigestUpdate(mdctx, password, strlen(password));
    EVP_DigestUpdate(mdctx, dn, strlen(dn));
    EVP_DigestFinal(mdctx, md_value, &md_len);
    hex2 = Encode64Binary(NULL, md_value, md_len);

    /* The +2 is for the colon and the terminating null */
    if (!(ctx = malloc(strlen(ldapContextHeader) + strlen(hex1) + strlen(hex2) + 3)))
        PANIC;
    sprintf(ctx, "%s%c%s:%s", ldapContextHeader, ltc, hex1, hex2);
    FreeThenNull(hex1);
    FreeThenNull(hex2);
    if (mdctx) {
        EVP_MD_CTX_free(mdctx);
        mdctx = NULL;
    }
    return ctx;
}

void FindUserLDAP2ParseContext(char *context,
                               enum LDAPTYPE *lt,
                               char **hex1,
                               size_t *len1,
                               char **hex2,
                               size_t *len2) {
    size_t len = strlen(ldapContextHeader);
    char ltc;
    char *colon;

    if (hex1)
        *hex1 = NULL;

    if (len1)
        *len1 = 0;

    if (hex2)
        *hex2 = NULL;

    if (len2)
        *len2 = 0;

    if (lt)
        *lt = LTUNKNOWN;

    if (context == NULL || strncmp(context, ldapContextHeader, len) != 0)
        return;

    context += len;

    ltc = *context++;
    if (ltc < '0')
        return;
    ltc -= '0';
    if (ltc >= LTLAST)
        return;
    if (lt)
        *lt = ltc;

    colon = strchr(context, ':');
    if (colon == NULL)
        return;

    if (hex1)
        *hex1 = context;

    if (len1)
        *len1 = colon - context;

    colon++;

    if (hex2)
        *hex2 = colon;

    if (len2)
        *len2 = strlen(colon);
}

int FindUserLDAP2Password(struct TRANSLATE *t,
                          struct USERINFO *uip,
                          LDAP *ld,
                          char *dn,
                          char *file,
                          enum LDAPTYPE lt,
                          char *ldapUrl) {
    int rc = 1;
    char *reason;
    char *pass, *newPass, *verifyPass;
    char *ctx = NULL;
    char *hexIn1, *hexIn2, *hexOut1, *hexOut2;
    size_t lenIn1, lenIn2, lenOut1, lenOut2;
    enum LDAPTYPE ltIn, ltOut;

    if (file == NULL || *file == 0 || lt == LTUNKNOWN)
        return 1;

    pass = (uip->pass ? uip->pass : "");
    newPass = (uip->newPass ? uip->newPass : "");
    verifyPass = (uip->verifyPass ? uip->verifyPass : "");

    if (!FileExists(file))
        return 1;

    FindUserLDAP2ParseContext(uip->context, &ltIn, &hexIn1, &lenIn1, &hexIn2, &lenIn2);

    ctx = FindUserLDAP2MakeContext(ldapUrl, dn, uip->pass, lt);
    FindUserLDAP2ParseContext(ctx, &ltOut, &hexOut1, &lenOut1, &hexOut2, &lenOut2);

    if (uip->newPass == NULL && lenIn2 == 0) {
        reason = "You must change your password before you can proceed";
    } else {
        if (strcmp(pass, newPass) == 0) {
            reason = "You must select a new password.";
        } else if (strcmp(newPass, verifyPass) != 0) {
            reason = "The two copies of your new password did not match.";
        } else if (lenIn2 && lenOut2 &&
                   (lenIn2 != lenOut2 || strncmp(hexIn2, hexOut2, lenIn2) != 0)) {
            reason = "Your old password was incorrect, please try again.";
        } else {
            if (lt == LTAD)
                rc = LDAPADChangeUserPassword(uip, ld, dn, pass, newPass, uip->debug);
            else if (lt == LTNDS)
                rc = LDAPNDSChangeUserPassword(uip, ld, dn, pass, newPass, uip->debug);
            if (rc == LDAP_SUCCESS)
                return 0;
            reason = "Your new password was invalid, please try again.";
        }
    }

    HTMLHeader(t, htmlHeaderNoCache);
    SendEditedFile(t, uip, file, 1, uip->url, 1, reason, uip->user, (uip->auth ? uip->auth : ""),
                   ctx, NULL);

    FreeThenNull(ctx);

    return -1;
}

void CheckPasswordExpirationAttributes(struct USERINFO *uip,
                                       LDAP *ld,
                                       char *dn,
                                       char *searchFilter,
                                       int *passwordExpired,
                                       int *passwordAllowChange,
                                       BOOL debug) {
    char *attrs[] = {"passwordExpirationTime", "passwordAllowChange", "loginDisabled", NULL};
    LDAPMessage *res = NULL, *entry = NULL;
    int rc;
    struct tm tm;
    time_t now;
    char nows[16];

    if (passwordExpired)
        *passwordExpired = -1;

    if (passwordAllowChange)
        *passwordAllowChange = -1;

    if (ld == NULL)
        return;

    rc = ldap_search_s(ld, dn, LDAP_SCOPE_BASE, searchFilter, attrs, 0, &res);
    if (rc != LDAP_SUCCESS)
        return;

    UUtime(&now);
    UUgmtime_r(&now, &tm);
    sprintf(nows, "%04d%02d%02d%02d%02d%02dZ", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
            tm.tm_hour, tm.tm_min, tm.tm_sec);

    for (entry = ldap_first_entry(ld, res); entry; entry = ldap_next_entry(ld, entry)) {
        char *a;
        char **vals;
        BerElement *ber;
        int i;

        for (a = ldap_first_attribute(ld, entry, &ber); a != NULL;
             a = ldap_next_attribute(ld, entry, ber)) {
            if ((vals = (char **)ldap_get_values(ld, entry, a)) != NULL) {
                for (i = 0; vals[i] != NULL; i++) {
                    if (stricmp(a, "passwordExpirationTime") == 0) {
                        int result = strcmp(vals[i], nows) < 0;
                        if (debug)
                            UsrLog(uip, "LDAP %s %s %s %sexpired", dn, a, vals[i],
                                   result ? "" : "not ");
                        if (passwordExpired)
                            *passwordExpired = result;
                    }
                    if (stricmp(a, "passwordAllowChange") == 0) {
                        if (debug)
                            UsrLog(uip, "LDAP %s %s %s", dn, a, vals[i]);
                        if (passwordAllowChange)
                            *passwordAllowChange = stricmp(vals[i], "true") == 0;
                    }
                }
                ldap_value_free(vals);
                vals = NULL;
            }
            ldap_memfree(a);
            a = NULL;
        }
    }

    if (debug) {
        if (passwordExpired && *passwordExpired == -1)
            UsrLog(uip, "LDAP %s no passwordExpirationTime found", dn);
        if (passwordAllowChange && *passwordAllowChange == -1) {
            UsrLog(uip, "LDAP %s no passwordAllowChange found, assume password cannot be changed",
                   dn);
        }
    }
}

char *FindUserLDAP2AuthGetValue(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                void *context,
                                const char *var,
                                const char *index) {
    int sizeLimit0 = 0;
    char *getVar[2];
    int rc;
    LDAPMessage *res = NULL, *entry = NULL;
    int needSet = 1;
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;
    char *val = NULL;
    int indexVal = index ? atoi(index) : 0;
    int indexCurrent = 0;

    if (ldapCtx->ldTest == NULL || ldapCtx->dn == NULL)
        goto Finished;

    if (stricmp(var, "user") == 0) {
        if (!(val = strdup(uip->user)))
            PANIC;
        goto Finished;
    }

    if (stricmp(var, "dn") == 0) {
        if (!(val = strdup(ldapCtx->dn)))
            PANIC;
        goto Finished;
    }

    getVar[0] = (char *)var;
    getVar[1] = NULL;

    (void)ldap_set_option(ldapCtx->ldTest, LDAP_OPT_SIZELIMIT, &sizeLimit0);
    rc = ldap_search_s(ldapCtx->ldTest, ldapCtx->dn, LDAP_SCOPE_BASE,
                       ldapCtx->useSearchFilterWhenReadingAttributes ? ldapCtx->searchFilter : NULL,
                       getVar, 0, &res);
    if (rc != LDAP_SUCCESS) {
        UsrLog(uip, "LDAP Set failed: %d %s", rc, ldap_err2string(rc));
        goto Finished;
    }

    for (entry = ldap_first_entry(ldapCtx->ldTest, res); entry;
         entry = ldap_next_entry(ldapCtx->ldTest, entry)) {
        char *a;
        char **vals;
        BerElement *ber;
        int i;

        for (a = ldap_first_attribute(ldapCtx->ldTest, entry, &ber); a != NULL;
             a = ldap_next_attribute(ldapCtx->ldTest, entry, ber)) {
            if ((vals = (char **)ldap_get_values(ldapCtx->ldTest, entry, a)) != NULL) {
                if (stricmp(var, a) == 0) {
                    for (i = 0; vals[i] != NULL; i++, indexCurrent++) {
                        if (indexCurrent == indexVal && needSet) {
                            if (!(val = strdup(vals[i])))
                                PANIC;
                            needSet = 0;
                        }
                    }
                }
                ldap_value_free(vals);
                vals = NULL;
            }
            ldap_memfree(a);
            a = NULL;
        }
    }
    ldap_msgfree(res);
    res = NULL;

Finished:
    return val;
}

BOOL FindUserLDAP2AggregateTest(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                void *context,
                                const char *var,
                                const char *testVal,
                                enum AGGREGATETESTOPTIONS ato,
                                struct VARIABLES *rev) {
    struct AGGREGATETESTCONTEXT atc;
    int sizeLimit0 = 0;
    char *getVar[2];
    int rc;
    LDAPMessage *res = NULL, *entry = NULL;
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    if (ldapCtx->ldTest == NULL || ldapCtx->dn == NULL)
        return FALSE;

    if (!ExpressionAggregateTestInitialize(&atc, uip, testVal, ato))
        return FALSE;

    if (stricmp(var, "user") == 0) {
        ExpressionAggregateTestValue(&atc, uip->user);
    } else if (stricmp(var, "dn") == 0) {
        ExpressionAggregateTestValue(&atc, ldapCtx->dn);
    } else {
        getVar[0] = (char *)var;
        getVar[1] = NULL;

        (void)ldap_set_option(ldapCtx->ldTest, LDAP_OPT_SIZELIMIT, &sizeLimit0);
        rc = ldap_search_s(
            ldapCtx->ldTest, ldapCtx->dn, LDAP_SCOPE_BASE,
            ldapCtx->useSearchFilterWhenReadingAttributes ? ldapCtx->searchFilter : NULL, getVar, 0,
            &res);
        if (rc != LDAP_SUCCESS) {
            UsrLog(uip, "LDAP Set failed: %d %s", rc, ldap_err2string(rc));
        } else {
            for (entry = ldap_first_entry(ldapCtx->ldTest, res); entry;
                 entry = ldap_next_entry(ldapCtx->ldTest, entry)) {
                char *a;
                char **vals;
                BerElement *ber;
                int i;

                for (a = ldap_first_attribute(ldapCtx->ldTest, entry, &ber); a != NULL;
                     a = ldap_next_attribute(ldapCtx->ldTest, entry, ber)) {
                    if ((vals = (char **)ldap_get_values(ldapCtx->ldTest, entry, a)) != NULL) {
                        if (stricmp(var, a) == 0) {
                            for (i = 0; vals[i] != NULL; i++) {
                                ExpressionAggregateTestValue(&atc, vals[i]);
                            }
                        }
                        ldap_value_free(vals);
                        vals = NULL;
                    }
                    ldap_memfree(a);
                    a = NULL;
                }
            }
            ldap_msgfree(res);
            res = NULL;
        }
    }

    return ExpressionAggregateTestTerminate(&atc, rev);
}

void UsrLDAPCloseConnections(struct LDAPCTX *ldapCtx) {
    if (ldapCtx->dn) {
        ldap_memfree(ldapCtx->dn);
        ldapCtx->dn = NULL;
        ldapCtx->logDn = "";
    }

    if (ldapCtx->res) {
        ldap_msgfree(ldapCtx->res);
        ldapCtx->res = NULL;
    }

    if (ldapCtx->lud) {
        ldap_free_urldesc(ldapCtx->lud);
        ldapCtx->lud = NULL;
    }

    FreeThenNull(ldapCtx->bindUri);

    if (ldapCtx->ld2) {
        ldap_unbind(ldapCtx->ld2);
        ldapCtx->ld2 = NULL;
    }

    if (ldapCtx->ld) {
        ldap_unbind(ldapCtx->ld);
        ldapCtx->ld = NULL;
    }
}

static int UsrLDAPBindCertificate(struct TRANSLATE *t,
                                  struct USERINFO *uip,
                                  void *context,
                                  char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    if ((ldapCtx->bindCert = atoi(arg))) {
        if ((ldapCtx->bindCertCtx = UUSslLoadKey(ldapCtx->bindCert, 0, NULL, 1, NULL))) {
            ldap_set_option(NULL, LDAP_OPT_X_TLS_CTX, ldapCtx->bindCertCtx);
            return 0;
        }
        UsrLog(uip, "BindCertificate %d references a non-existent certificate");
        return 1;
    }
    return 0;
}

static int UsrLDAPBindPassword(struct TRANSLATE *t,
                               struct USERINFO *uip,
                               void *context,
                               char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    char *obscure, *expr;
    char *val = NULL;

    FreeThenNull(ldapCtx->bindPass);

    // if this is obscured password, unobscure it
    if ((obscure = StartsIWith(arg, "-Obscure "))) {
        obscure = SkipWS(obscure);
        val = UnobscureString(obscure);
        if (val == NULL) {
            UsrLog(uip, "BindPassword -Obscure references an invalid password");
            return 1;
        }
        // check it we have received an expression, EXPROX-526
    } else if ((expr = StartsIWith(arg, "-expr "))) {
        // Expression will be evaluated as any other expression would.
        // If expression is invalid, val will come back as NULL, but that's
        // OK as the NULL value will just be stored in what is already
        // NULL anyway.
        val = ExpressionValue(t, uip, context, expr);
    } else {
        if (!(val = strdup(arg)))
            PANIC;
    }

    // val has been duplicated into new memory by now; transfer that for storage
    ldapCtx->bindPass = val;
    val = NULL;

    return 0;
}

static int UsrLDAPBindUser(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;
    char *val = NULL;

    FreeThenNull(ldapCtx->bindUser);

    // check it we have received an expression, EXPROX-526
    char *expr;
    if ((expr = StartsIWith(arg, "-expr "))) {
        // Expression will be evaluated as any other expression would.
        // If expression is invalid, val will come back as NULL, but that's
        // OK as the NULL value will just be stored in what is already
        // NULL anyway.
        val = ExpressionValue(t, uip, context, expr);
    } else {
        if (!(val = strdup(arg)))
            PANIC;
    }

    // val has been duplicated into new memory by now; transfer that for storage
    ldapCtx->bindUser = val;
    val = NULL;

    return 0;
}

static int UsrLDAPDisableReferralChasing(struct TRANSLATE *t,
                                         struct USERINFO *uip,
                                         void *context,
                                         char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    ldapCtx->disableReferralChasing = 1;
    return 0;
}

static int UsrLDAPIfDisabled(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    uip->expiredSeen = 1;
    if (ldapCtx->loginDisabled) {
        ldapCtx->lastDisabled = 1;
        return 0;
    }
    return 1;
}

static int UsrLDAPIfExpired(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    uip->expiredSeen = 1;
    if (ldapCtx->expired == 1) {
        ldapCtx->lastExpired = 1;
        if (uip->debug)
            UsrLog(uip, "LDAP %s expired true", ldapCtx->logDn);
        return 0;
    }
    if (uip->debug)
        UsrLog(uip, "LDAP %s expired false", ldapCtx->logDn);
    return 1;
}

static int UsrLDAPIfMember(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;
    struct MEMBERATTRIBUTE maDefault = {NULL, "member"}, maDefault2 = {NULL, "uniqueMember"};
    struct MEMBERATTRIBUTE *maNext;
    int rc;
    char *collapseDn = NULL;

    maDefault.next = &maDefault2;

    if (ldapCtx->ldTest == NULL || ldapCtx->dn == NULL)
        return 1;

    if (ldapCtx->memberUser == 0 && strchr(ldapCtx->dn, ' ') != NULL) {
        if (!(collapseDn = strdup(ldapCtx->dn)))
            PANIC;
        Trim(collapseDn, TRIM_LDAP_DN);
        if (strcmp(collapseDn, ldapCtx->dn) == 0) {
            FreeThenNull(collapseDn);
        }
    }

    for (maNext = ldapCtx->maSpecified ? ldapCtx->maSpecified : &maDefault; maNext;
         maNext = maNext->next) {
        rc = ldap_compare_s(ldapCtx->ldTest, arg, maNext->attribute,
                            ldapCtx->memberUser ? uip->user : ldapCtx->dn);
        if (rc == LDAP_COMPARE_TRUE)
            break;
        if (ldapCtx->memberUser == 0 && collapseDn) {
            rc = ldap_compare_s(ldapCtx->ldTest, arg, maNext->attribute, collapseDn);
            if (rc == LDAP_COMPARE_TRUE)
                break;
        }
    }

    FreeThenNull(collapseDn);

    if (uip->debug)
        UsrLog(uip, "LDAP %s member %s %s", ldapCtx->logDn, arg,
               ASCIITrueOrFalse(rc == LDAP_COMPARE_TRUE));
    if (rc == LDAP_COMPARE_TRUE)
        return 0;
    return 1;
}

static int UsrLDAPIfTest(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    BOOL reTest = 0;
    char *reAttr[2];
    BOOL reMatch = 0;
    char *testEZproxyVar = NULL;
    BOOL testResult;
    char *arg2;
    int rc;
    int sizeLimit0 = 0;
    LDAPMessage *res;
    LDAPMessage *entry = NULL;

    for (;;) {
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }

        if (stricmp(arg, "-wild") == 0) {
            reTest = 1;
            arg = arg2;
            continue;
        }

        break;
    }

    if (stricmp(arg, "-user") == 0) {
        testEZproxyVar = (uip->user ? uip->user : "");
    }

    if (stricmp(arg, "-auth") == 0) {
        testEZproxyVar = (uip->auth ? uip->auth : "");
    }

    if (testEZproxyVar) {
        if (reTest == 0) {
            testResult = stricmp(testEZproxyVar, arg2) == 0;
            if (uip->debug)
                UsrLog(uip, "LDAP %s test %s %s %s", ldapCtx->logDn, arg, arg2,
                       ASCIITrueOrFalse(testResult));
            if (testResult)
                return 0;
        } else {
            testResult = WildCompare(testEZproxyVar, arg2);
            if (uip->debug)
                UsrLog(uip, "LDAP %s test -wild %s %s %s", ldapCtx->logDn, arg, arg2,
                       ASCIITrueOrFalse(testResult));
            if (testResult)
                return 0;
        }
        return 1;
    }

    if (ldapCtx->ldTest == NULL || ldapCtx->dn == NULL)
        return 1;

    if (reTest == 0) {
        rc = ldap_compare_s(ldapCtx->ldTest, ldapCtx->dn, arg, arg2);
        testResult = (rc == LDAP_COMPARE_TRUE);
        if (uip->debug)
            UsrLog(uip, "LDAP %s test %s %s %s", ldapCtx->logDn, arg, arg2,
                   ASCIITrueOrFalse(testResult));
        if (testResult)
            return 0;
        return 1;
    }

    reAttr[0] = arg;
    reAttr[1] = NULL;

    if (stricmp(arg, "dn") == 0) {
        testResult = WildCompare(ldapCtx->dn, arg2);
        if (uip->debug)
            UsrLog(uip, "LDAP %s test -wild %s %s %s", ldapCtx->logDn, arg, arg2,
                   ASCIITrueOrFalse(testResult));
        if (testResult)
            return 0;
        return 1;
    }

    (void)ldap_set_option(ldapCtx->ldTest, LDAP_OPT_SIZELIMIT, &sizeLimit0);
    /*
    rc = ldap_search_s(ld, ldapCtx->dn, LDAP_SCOPE_BASE, "(objectclass=*)", reAttr, 0, &res);
    */

    rc = ldap_search_s(ldapCtx->ldTest, ldapCtx->dn, LDAP_SCOPE_BASE,
                       ldapCtx->useSearchFilterWhenReadingAttributes ? ldapCtx->searchFilter : NULL,
                       reAttr, 0, &res);
    if (rc != LDAP_SUCCESS) {
        UsrLog(uip, "LDAP test -wild failed: %d %s", rc, ldap_err2string(rc));
        return 1;
    }

    for (entry = ldap_first_entry(ldapCtx->ldTest, res); entry;
         entry = ldap_next_entry(ldapCtx->ldTest, entry)) {
        char *a;
        char **vals;
        BerElement *ber;
        int i;

        for (a = ldap_first_attribute(ldapCtx->ldTest, entry, &ber); a != NULL;
             a = ldap_next_attribute(ldapCtx->ldTest, entry, ber)) {
            if ((vals = (char **)ldap_get_values(ldapCtx->ldTest, entry, a)) != NULL) {
                if (stricmp(arg, a) == 0) {
                    for (i = 0; vals[i] != NULL; i++) {
                        if (reMatch == 0) {
                            if ((testResult = WildCompare(vals[i], arg2)))
                                reMatch = 1;
                            if (uip->debug)
                                UsrLog(uip, "LDAP %s test -wild %s %s against %s %s",
                                       ldapCtx->logDn, arg, arg2, vals[i],
                                       ASCIITrueOrFalse(testResult));
                            continue;
                        }
                    }
                }
                ldap_value_free(vals);
                vals = NULL;
            }
            ldap_memfree(a);
            a = NULL;
        }
    }
    ldap_msgfree(res);
    res = NULL;

    if (reMatch)
        return 0;
    return 1;
}

static int UsrLDAPIgnore(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    UsrIgnore(t, uip, NULL, "");
    if (ldapCtx->lastDisabled) {
        ldapCtx->loginDisabled = 0;
        if (uip->debug)
            UsrLog(uip, "LDAP %s ignore account disabled", ldapCtx->logDn);
    }
    if (ldapCtx->lastExpired) {
        ldapCtx->expired = 0;
        if (uip->debug)
            UsrLog(uip, "LDAP %s ignore account expired", ldapCtx->logDn);
    }
    return 0;
}

static int UsrLDAPIgnorePassword(struct TRANSLATE *t,
                                 struct USERINFO *uip,
                                 void *context,
                                 char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    ldapCtx->ignorePassword = 1;
    return 0;
}

static int UsrLDAPLDAPV2(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    ldapCtx->version = LDAP_VERSION2;
    return 0;
}

static int UsrLDAPLDAPV3(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    ldapCtx->version = LDAP_VERSION3;
    return 0;
}

/*
This is part of the start of a change to allow EZproxy to check more than one returned object
It is not complete, so it is commented out for now
static int UsrLDAPMaxResults(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg)
{
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *) context;

    ldapCtx->maxResults = atoi(arg);
    return 0;
}
*/

static int UsrLDAPMemberAttribute(struct TRANSLATE *t,
                                  struct USERINFO *uip,
                                  void *context,
                                  char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;
    struct MEMBERATTRIBUTE *maNext;

    if (!(maNext = calloc(1, sizeof(*maNext))))
        PANIC;
    maNext->next = ldapCtx->maSpecified;
    if (!(maNext->attribute = strdup(arg)))
        PANIC;
    ldapCtx->maSpecified = maNext;
    return 0;
}

static int UsrLDAPMemberDN(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    ldapCtx->memberUser = 0;
    return 0;
}

static int UsrLDAPMemberUser(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    ldapCtx->memberUser = 1;
    return 0;
}

static int UsrLDAPPasswordForm(struct TRANSLATE *t,
                               struct USERINFO *uip,
                               void *context,
                               char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    if (*arg && ValidMenu(arg)) {
        sprintf(ldapCtx->passwordForm, "%s%s", DOCSDIR, arg);
        return 0;
    }
    ldapCtx->passwordForm[0] = 0;

    return *arg == 0;
}

static int UsrLDAPSSLCipherSuite(struct TRANSLATE *t,
                                 struct USERINFO *uip,
                                 void *context,
                                 char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    FreeThenNull(ldapCtx->sslCipherSuite);
    if (*arg) {
        if (!(ldapCtx->sslCipherSuite = strdup(arg)))
            PANIC;
    }

    return *arg == 0;
}

static int UsrLDAPStartTLS(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    // STLDAPS is only used internally
    if (stricmp(arg, "disabled") == 0) {
        ldapCtx->startTLS = STDISABLED;
    } else if (stricmp(arg, "enabled") == 0) {
        ldapCtx->startTLS = STENABLED;
    } else {
        Log("Unrecognized StartTLS option: %s", arg);
        return 1;
    }
    return 0;
}

static int UsrLDAPTestWithBind(struct TRANSLATE *t,
                               struct USERINFO *uip,
                               void *context,
                               char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    if (ldapCtx->ld == NULL) {
        UsrLog(uip, "For LDAP, TestWithBind must appear after URL");
        return 1;
    }
    ldapCtx->ldTest = ldapCtx->ld;
    return 0;
}

static int UsrLDAPTestWithUser(struct TRANSLATE *t,
                               struct USERINFO *uip,
                               void *context,
                               char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    if (ldapCtx->ld == NULL) {
        UsrLog(uip, "For LDAP, TestWithUser must appear after URL");
        return 1;
    }
    ldapCtx->ldTest =
        (ldapCtx->rcUserBind == LDAP_SUCCESS && ldapCtx->ld2) ? ldapCtx->ld2 : ldapCtx->ld;
    return 0;
}

static int UsrLDAPTimeout(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    ldapCtx->timeout = atoi(arg);
    return 0;
}

static int UsrLDAPURL(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;
    struct sockaddr_storage sa;
    char ipBuffer[INET6_ADDRSTRLEN + 2];  // add 2 to allow for IPv6 brackets
    int tries = 0;
    int sizeLimit2 = 2;
    int sizeLimit0 = 0;
    char *attrs[] = {"dn", "loginDisabled", NULL};
    LDAPMessage *entry = NULL;
    int count;
    enum LDAPTYPE lt;
    int rc = 1, rc2 = 1, rc3 = 1;
    int passwordExpired = -1, passwordAllowChange = -1;

    // OpenLDAP doesn't allow ready support of IPv6 with MinGW
    // due to lack of inet_ntop as a standard function.  On that
    // platform, force AF_INET (IPv4), but on others, allow
    // IPv4 or IPv6 (AF_UNSPEC).
#ifdef WIN32
    const int family = AF_INET;
#else
    const int family = AF_UNSPEC;
#endif

    if ((uip->pass == NULL || *uip->pass == 0) && ldapCtx->ignorePassword == 0) {
        /* Starting with 5.1, even with bad logic, flow continues forward through user.txt
        uip->skipping = 1;
        continue;
        */
        return 1;
    }

    ldapCtx->loginDisabled = 0;

    UsrLDAPCloseConnections(ldapCtx);

    ldapCtx->ldTest = NULL;

    uip->result = resultInvalid;

    rc = ldap_url_parse(arg, &ldapCtx->lud);

    if (rc != 0) {
        UsrLog(uip, "Invalid LDAP URL: %s", arg);
        /* Starting with 5.1, even with bad logic, flow continues forward through user.txt
        uip->skipping = 1;
        */
        return 1;
    }

    if (ldapCtx->lud->lud_attrs == NULL) {
        UsrLog(uip, "For LDAP, the URL must contain the attribute to use for searching");
        /* Starting with 5.1, even with bad logic, flow continues forward through user.txt
        uip->skipping = 1;
        */
        return 1;
    }

    FreeThenNull(ldapCtx->searchFilter);
    ldapCtx->searchFilter =
        LDAPFilter(ldapCtx->lud->lud_filter, *ldapCtx->lud->lud_attrs, uip->user);

    if (stricmp(ldapCtx->lud->lud_scheme, "ldaps") == 0 && !UUSslEnabled()) {
        UsrLog(uip, "ldaps is disabled");
        return 1;
    }

    /* The + 20 allows for the :// after the scheme, 40 for IP address, the : before the port, and
     * the port, plus a little safety */
    if (!(ldapCtx->bindUri = malloc(strlen(ldapCtx->lud->lud_scheme) + sizeof(ipBuffer) + 20)))
        PANIC;

Retry:
    if (UUGetHostByName(ldapCtx->lud->lud_host, &sa, (PORT)ldapCtx->lud->lud_port, family)) {
        UsrLog(uip, "LDAP host not found: %s", ldapCtx->lud->lud_host);
        /* Starting with 5.1, even with bad logic, flow continues forward through user.txt
        uip->skipping = 1;
        continue;
        */
        return 1;
    }

    ipToStrV6Bracketed(&sa, ipBuffer, sizeof(ipBuffer));
    sprintf(ldapCtx->bindUri, "%s://%s:%d", ldapCtx->lud->lud_scheme, ipBuffer,
            ldapCtx->lud->lud_port);

    rc = ldap_initialize(&ldapCtx->ld, ldapCtx->bindUri);
    if ((ldapCtx->ld == NULL)) {
        UsrLog(uip, "ldap_initialize failed: %d %s", rc, ldap_err2string(rc));
        /* Starting with 5.1, even with bad logic, flow continues forward through user.txt
        uip->skipping = 1;
        */
        return 1;
    }

    (void)ldap_set_option(ldapCtx->ld, LDAP_OPT_PROTOCOL_VERSION, &ldapCtx->version);

    (void)ldap_set_option(ldapCtx->ld, LDAP_OPT_X_TLS_CONNECT_ARG, ldapCtx->sslCipherSuite);
    (void)ldap_set_option(ldapCtx->ld, LDAP_OPT_X_TLS_CONNECT_CB, FindUserLDAP2SetSSLCipherSuite);

    /*
    if (stricmp(lud->lud_scheme, "ldaps") == 0 && bindCertCtx) {
        rc = ldap_set_option( ld, LDAP_OPT_X_TLS_SSL_CTX, bindCertCtx);
    }
    */

    rc = UU_ldap_bind_st(ldapCtx, ldapCtx->ld, ldapCtx->bindUser, ldapCtx->bindPass);

    if (rc != LDAP_SUCCESS) {
        UsrLog(uip, "LDAP bind failed to %s: %d %s", ldapCtx->lud->lud_host, rc,
               ldap_err2string(rc));
        ldap_unbind(ldapCtx->ld);
        ldapCtx->ld = NULL;
        if (rc == LDAP_SERVER_DOWN) {
            tries++;
            if (tries < UUGetHostByNameFailedAddress(ldapCtx->lud->lud_host, &sa, family)) {
                goto Retry;
            }
            uip->result = resultRefused;
            return 1;
        }
        /* Starting with 5.1, even with bad logic, flow continues forward through user.txt
        uip->skipping = 1;
        */
        return 1;
    }

    (void)ldap_set_option(ldapCtx->ld, LDAP_OPT_REFERRALS,
                          (ldapCtx->disableReferralChasing ? LDAP_OPT_OFF : LDAP_OPT_ON));

    /* should really be maxResults, but stick to traditional logic for now
       until later point of pursuing ability to match to multiple objects
    */
    (void)ldap_set_option(ldapCtx->ld, LDAP_OPT_SIZELIMIT, &sizeLimit2);
    rc = ldap_search_s(ldapCtx->ld, ldapCtx->lud->lud_dn, ldapCtx->lud->lud_scope,
                       ldapCtx->searchFilter, attrs, 0, &ldapCtx->res);
    if (rc != LDAP_SUCCESS) {
        if (rc == LDAP_SIZELIMIT_EXCEEDED) {
            UsrLog(uip, "LDAP search matched more than %d object%s, results ignored: %s",
                   ldapCtx->maxResults, SIfNotOne(ldapCtx->maxResults), uip->user);
        } else if (rc != LDAP_NO_SUCH_OBJECT) {
            UsrLog(uip, "LDAP search failed: %d %s", rc, ldap_err2string(rc));
        }
        /* Starting with 5.1, even with bad logic, flow continues forward through user.txt
        uip->skipping = 1;
        */
        return 1;
    }

    count = ldap_count_entries(ldapCtx->ld, ldapCtx->res);

    if (count == 0 || count > ldapCtx->maxResults) {
        if (count > 0)
            UsrLog(uip, "LDAP search matched more than %d object%s, results ignored: %s",
                   ldapCtx->maxResults, SIfNotOne(ldapCtx->maxResults), uip->user);
        ldap_msgfree(ldapCtx->res);
        ldapCtx->res = NULL;
        /* Starting with 5.1, even with bad logic, flow continues forward through user.txt
        uip->skipping = 1;
        */
        return 1;
    }

    for (entry = ldap_first_entry(ldapCtx->ld, ldapCtx->res); entry;
         entry = ldap_next_entry(ldapCtx->ld, entry)) {
        char *a;
        char **vals;
        BerElement *ber;
        int i;

        ldapCtx->dn = ldap_get_dn(ldapCtx->ld, entry);
        ldapCtx->logDn = ldapCtx->dn && *ldapCtx->dn ? ldapCtx->dn : "";

        for (a = ldap_first_attribute(ldapCtx->ld, entry, &ber); a != NULL;
             a = ldap_next_attribute(ldapCtx->ld, entry, ber)) {
            if ((vals = (char **)ldap_get_values(ldapCtx->ld, entry, a)) != NULL) {
                if (stricmp("loginDisabled", a) == 0) {
                    for (i = 0; vals[i] != NULL; i++) {
                        if (uip->debug)
                            UsrLog(uip, "LDAP %s %s %s", ldapCtx->logDn, a, vals[i]);
                        if (vals[i] && stricmp(vals[i], "true") == 0)
                            ldapCtx->loginDisabled = 1;
                    }
                }
                ldap_value_free(vals);
                vals = NULL;
            }
            ldap_memfree(a);
            a = NULL;
        }
    }
    ldap_msgfree(ldapCtx->res);
    ldapCtx->res = NULL;

    if (uip->debug)
        UsrLog(uip, "LDAP %s mapped to %s", uip->user, ldapCtx->logDn);

    ldapCtx->ldTest = ldapCtx->ld;

    if (ldapCtx->ignorePassword) {
        uip->result = resultValid;
        ldapCtx->ld2 = NULL;
        return 0;
    }

    if (uip->context) {
        char *hexIn, *hexIn2, *hexOut, *hexOut2;
        size_t lenIn, lenOut, lenIn2, lenOut2;

        FindUserLDAP2ParseContext(uip->context, &lt, &hexIn, &lenIn, &hexIn2, &lenIn2);
        if (hexIn && lenIn) {
            char *ctx = FindUserLDAP2MakeContext(arg, ldapCtx->dn, uip->pass, lt);
            FindUserLDAP2ParseContext(ctx, &lt, &hexOut, &lenOut, &hexOut2, &lenOut2);
            rc3 = 1;
            if (lenIn == lenOut && strncmp(hexIn, hexOut, lenIn) == 0) {
                rc3 = FindUserLDAP2Password(t, uip, ldapCtx->ld, ldapCtx->dn, ldapCtx->passwordForm,
                                            lt, arg);
                if (rc3 == 0) {
                    uip->pass = uip->newPass;
                    uip->newPass = uip->verifyPass = NULL;
                    /* do not go to ReBind from here since ctx has not been freed */
                }
            }
            FreeThenNull(ctx);
            if (rc3 == 0)
                goto ReBind;
            if (rc3 == -1) {
                uip->result = resultNotified;
                return 1;
            }
            ldapCtx->expired = 1;
            uip->result = resultValid;
            return 0;
        }
    }

ReBind:
    if (ldapCtx->ld2) {
        ldap_unbind(ldapCtx->ld2);
        ldapCtx->ld2 = NULL;
    }

    ldap_initialize(&ldapCtx->ld2, ldapCtx->bindUri);
    if (ldapCtx->ld2 == NULL) {
        perror("ldap_init");
        exit(EXIT_FAILURE);
    }

    (void)ldap_set_option(ldapCtx->ld2, LDAP_OPT_PROTOCOL_VERSION, &ldapCtx->version);

    (void)ldap_set_option(ldapCtx->ld2, LDAP_OPT_X_TLS_CONNECT_ARG, ldapCtx->sslCipherSuite);
    (void)ldap_set_option(ldapCtx->ld2, LDAP_OPT_X_TLS_CONNECT_CB, FindUserLDAP2SetSSLCipherSuite);

    ldapCtx->rcUserBind = UU_ldap_bind_st(ldapCtx, ldapCtx->ld2, ldapCtx->dn, uip->pass);

    {
        char *extendedError = NULL;

        if (ldapCtx->ld2)
            ldap_get_option(ldapCtx->ld2, LDAP_OPT_ERROR_STRING, &extendedError);

        rc2 = LDAPExtendedErrorCode(extendedError, &lt);

        if (uip->debug) {
            UsrLog(uip, "Bind for %s result %d type %d extended error %s", ldapCtx->logDn,
                   ldapCtx->rcUserBind, lt,
                   extendedError && *extendedError ? extendedError : "(none)");
        }

        FreeThenNull(extendedError);
    }

    CheckPasswordExpirationAttributes(
        uip, (ldapCtx->rcUserBind == LDAP_SUCCESS && lt != LTAD ? ldapCtx->ld2 : NULL), ldapCtx->dn,
        ldapCtx->useSearchFilterWhenReadingAttributes ? ldapCtx->searchFilter : NULL,
        &passwordExpired, &passwordAllowChange, uip->debug);

    if (lt == LTAD && ldapCtx->rcUserBind == LDAP_INVALID_CREDENTIALS && rc2) {
        if (rc2 == ERROR_ACCOUNT_EXPIRED) {
            ldapCtx->loginDisabled = 1;
            if (uip->debug)
                UsrLog(uip, "LDAP AD %s disabled since account expired", ldapCtx->logDn);
            uip->result = resultValid;
            return 0;
        }

        if (rc2 == ERROR_PASSWORD_MUST_CHANGE || rc2 == ERROR_PASSWORD_EXPIRED) {
            if (uip->debug)
                UsrLog(uip, "LDAP AD %s expired", ldapCtx->logDn);
            if (ldapCtx->passwordForm[0] == 0 || passwordAllowChange == 0) {
                ldapCtx->expired = 1;
                uip->result = resultValid;
                return 0;
            } else {
                rc3 = FindUserLDAP2Password(t, uip, ldapCtx->ld, ldapCtx->dn, ldapCtx->passwordForm,
                                            lt, arg);
                if (rc3 == 0) {
                    uip->pass = uip->newPass;
                    uip->newPass = uip->verifyPass = NULL;
                    goto ReBind;
                }
                if (rc3 == -1) {
                    uip->result = resultNotified;
                    return 1;
                }
                ldapCtx->expired = 1;
                uip->result = resultValid;
                return 0;
            }
        }
    }

    if (lt == LTUNKNOWN) {
        if (passwordExpired > 0) {
            if (uip->debug)
                UsrLog(uip, "LDAP %s expired by passwordExpirationDate", ldapCtx->logDn);
            if (ldapCtx->passwordForm[0] && passwordAllowChange > 0) {
                rc3 = FindUserLDAP2Password(t, uip, ldapCtx->ld, ldapCtx->dn, ldapCtx->passwordForm,
                                            LTNDS, arg);
                if (rc3 == 0) {
                    uip->pass = uip->newPass;
                    uip->newPass = uip->verifyPass = NULL;
                    goto ReBind;
                }
                if (rc3 == -1) {
                    uip->result = resultNotified;
                    return 1;
                }
            }
            ldapCtx->expired = 1;
            uip->result = resultValid;
            return 0;
        }
    }

    if (lt == LTNDS) {
        if ((ldapCtx->rcUserBind == LDAP_UNWILLING_TO_PERFORM &&
             rc2 == DSERR_LOG_ACCOUNT_EXPIRED)) {
            if (uip->debug)
                UsrLog(uip, "LDAP NDS %s disabled since account expired", ldapCtx->logDn);
            ldapCtx->loginDisabled = 1;
            uip->result = resultValid;
            return 0;
        }

        if ((ldapCtx->rcUserBind == LDAP_SUCCESS && rc2 == DSERR_PASSWORD_EXPIRED)) {
            if (uip->debug)
                UsrLog(uip, "LDAP NDS %s expired", ldapCtx->logDn);
            if (passwordAllowChange == 0 || ldapCtx->passwordForm[0] == 0) {
                ldapCtx->expired = 1;
                uip->result = resultValid;
                return 0;
            }

            rc3 = FindUserLDAP2Password(t, uip, ldapCtx->ld, ldapCtx->dn, ldapCtx->passwordForm, lt,
                                        arg);
            if (rc3 == 0) {
                uip->pass = uip->newPass;
                uip->newPass = uip->verifyPass = NULL;
                goto ReBind;
            }
            if (rc3 == -1) {
                uip->result = resultNotified;
                return 1;
            }
            ldapCtx->expired = 1;
            uip->result = resultValid;
            return 0;
        }
    }

    if (ldapCtx->rcUserBind != LDAP_SUCCESS) {
        if (uip->debug)
            UsrLog(uip, "LDAP %s rejected", ldapCtx->logDn);
        /* Starting with 5.1, even with bad logic, flow continues forward through user.txt
        uip->skipping = 1;
        continue;
        */
        return 1;
    }

    uip->result = resultValid;
    if (uip->debug)
        UsrLog(uip, "LDAP %s accepted", ldapCtx->logDn);

    return 0;
}

static int UsrLDAPUseSearchFilterWhenReadingAttributes(struct TRANSLATE *t,
                                                       struct USERINFO *uip,
                                                       void *context,
                                                       char *arg) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    ldapCtx->useSearchFilterWhenReadingAttributes = TRUE;
    return 0;
}

static void UsrLDAPCmdReset(struct TRANSLATE *t, struct USERINFO *uip, void *context) {
    struct LDAPCTX *ldapCtx = (struct LDAPCTX *)context;

    ldapCtx->lastDisabled = ldapCtx->lastExpired = 0;
}

int FindUserLDAP2(struct TRANSLATE *t,
                  struct USERINFO *uip,
                  char *file,
                  int f,
                  struct FILEREADLINEBUFFER *frb,
                  struct sockaddr_storage *pInterface) {
    struct LDAPCTX *ldapCtx, ldapCtxBuffer;
    int never = LDAP_OPT_X_TLS_NEVER;
    struct MEMBERATTRIBUTE *maNext, *maLast;
    struct USRDIRECTIVE udc[] = {
        {"BindCert",                             UsrLDAPBindCertificate,                      0},
        {"BindCertificate",                      UsrLDAPBindCertificate,                      0},
        {"BindPass",                             UsrLDAPBindPassword,                         0},
        {"BindPassword",                         UsrLDAPBindPassword,                         0},
        {"BindUser",                             UsrLDAPBindUser,                             0},
        {"Disabled",                             UsrLDAPIfDisabled,                           0},
        {"DisableReferralChasing",               UsrLDAPDisableReferralChasing,               0},
        {"Expired",                              UsrLDAPIfExpired,                            0},
        {"IfDisabled",                           UsrLDAPIfDisabled,                           0},
        {"IfExpired",                            UsrLDAPIfExpired,                            0},
        {"IfMember",                             UsrLDAPIfMember,                             0},
        {"IfTest",                               UsrLDAPIfTest,                               0},
        {"Ignore",                               UsrLDAPIgnore,                               0},
        {"IgnorePassword",                       UsrLDAPIgnorePassword,                       0},
        {"LDAPV2",                               UsrLDAPLDAPV2,                               0},
        {"LDAPV3",                               UsrLDAPLDAPV3,                               0},
 //      { "MaxResults",             UsrLDAPMaxResults,             0 },
        {"Member",                               UsrLDAPIfMember,                             0},
        {"MemberAttribute",                      UsrLDAPMemberAttribute,                      0},
        {"MemberDN",                             UsrLDAPMemberDN,                             0},
        {"MemberUser",                           UsrLDAPMemberUser,                           0},
        {"PasswordForm",                         UsrLDAPPasswordForm,                         0},
        {"SSLCipherSuite",                       UsrLDAPSSLCipherSuite,                       0},
        {"StartTLS",                             UsrLDAPStartTLS,                             0},
        {"Test",                                 UsrLDAPIfTest,                               0},
        {"TestWithBind",                         UsrLDAPTestWithBind,                         0},
        {"TestWithUser",                         UsrLDAPTestWithUser,                         0},
        {"Timeout",                              UsrLDAPTimeout,                              0},
        {"U",                                    UsrLDAPURL,                                  0},
        {"URL",                                  UsrLDAPURL,                                  0},
        {"UseSearchFilterWhenReadingAttributes", UsrLDAPUseSearchFilterWhenReadingAttributes, 0},
        {NULL,                                   NULL,                                        0}
    };

    memset(&ldapCtxBuffer, 0, sizeof(ldapCtxBuffer));
    ldapCtx = &ldapCtxBuffer;

    ldapCtx->version = LDAP_VERSION3;
    ldapCtx->maxResults = 1;
    ldapCtx->rcUserBind = -1;
    ldapCtx->startTLS = STDISABLED;

    uip->result = resultInvalid;

    (void)ldap_set_option(NULL, LDAP_OPT_X_TLS_REQUIRE_CERT, &never);

    uip->skipping = uip->user == NULL || *uip->user == 0;

    uip->authGetValue = FindUserLDAP2AuthGetValue;
    uip->authAggregateTest = FindUserLDAP2AggregateTest;

    uip->result =
        UsrHandler(t, "LDAP", udc, ldapCtx, uip, file, f, frb, pInterface, UsrLDAPCmdReset);

    if (uip->result == resultValid && (ldapCtx->expired || ldapCtx->loginDisabled)) {
        uip->result = resultInvalid;
    }

    if (uip->result == resultRefused) {
        uip->result = resultInvalid;
    }

    UsrLDAPCloseConnections(ldapCtx);

    FreeThenNull(ldapCtx->searchFilter);
    FreeThenNull(ldapCtx->bindUser);
    FreeThenNull(ldapCtx->bindPass);
    FreeThenNull(ldapCtx->sslCipherSuite);

    for (maNext = ldapCtx->maSpecified; maNext; maNext = maLast) {
        maLast = maNext->next;
        FreeThenNull(maNext->attribute);
        FreeThenNull(maNext);
    }

    return uip->result;
}
