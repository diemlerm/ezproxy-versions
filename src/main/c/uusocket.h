#ifndef __UUSOCKET_H__
#define __UUSOCKET_H__

#include "common.h"

/* Define shutdown codes if they're not available already */
#ifndef SHUT_RD
#define SHUT_RD 0
#endif

#ifndef SHUT_WR
#define SHUT_WR 1
#endif

#ifndef SHUT_RDWR
#define SHUT_RDWR 2
#endif

/* Win32 uses INVALID_SOCKET with accept, listen, etc., whereas BSD calls return -1 */
#ifndef INVALID_SOCKET
#define INVALID_SOCKET -1
#endif
#ifndef SOCKET_ERROR
#define SOCKET_ERROR -1
#endif

/*
 * Win32 uses SOCKET, UNIX uses int.
 * SOCKET is an unsigned 32-bit int on Windows, making the minimum
 * value here 0.
 */
#ifndef WIN32
#define SOCKET int
#define SOCKET_MIN INT_MIN
#define SOCKET_MAX INT_MAX
#define SOCKET_FORMAT "d"
#define PORT in_port_t
#define PORT_FORMAT "hu"
#else
#define SOCKET_MIN 0
#define SOCKET_MAX 0xffffu
#define SOCKET_FORMAT "u"
#define PORT u_short
#define PORT_FORMAT "hu"
#endif

#define MAXUUSOCKETBUFFERS 10

struct SOCKETBUFFER {
    void *msg;
    size_t len;
};

struct UUSOCKETBUFFERS {
    int count;
    struct SOCKETBUFFER sb[MAXUUSOCKETBUFFERS];
};

#define UUWANTRECV SSL_ERROR_WANT_READ
#define UUWANTSEND SSL_ERROR_WANT_WRITE
extern int sslActiveIndex;

#define UUMIN(a, b) ((a) < (b) ? (a) : (b))
#define UUMAX(a, b) ((a) > (b) ? (a) : (b))

#define UUSOCKETRECVZBUFFERSIZE 512
#define UUSOCKETRECVBUFFERSIZE 2048
#define UUSOCKETSENDBUFFERSIZE 2048
#define UUSOCKETSENDZIPBUFFERSIZE 2048
#define UUSOCKETUTF16BUFFERSIZE 16

enum UUSOCKETTYPE { STSTREAM, STSSL, STMEMORY, STNULL };

enum SSLLOADKEYERROR { SLKENONE, SLKEBADCRT, SLKEBADKEY, SLKECRTKEYMISMATCH, SLKEOTHER };

struct UUMEMORYBUFFER {
    char *buffer;
    int readEob;
    int maxLen;
    int readOfs;
    int writeOfs;
    char reallocFailed;
};

struct UUSSLFILES {
    struct UUSSLFILES *flink, *blink;
    int serial;
    int flags;
};

#define UUSSLFILESCNF 1
#define UUSSLFILESERR 2
#define UUSSLFILESKEY 4
#define UUSSLFILESCSR 8
#define UUSSLFILESCRT 16

enum UUSTMEMORYACCESS { STMEMORYREAD, STMEMORYWRITE, STMEMORYREADWRITE };

enum SSLLAST { SSLLASTUNKOWN, SSLLASTRECV, SSLLASTSEND };

enum LOGFILEDIRECTION {
    LFNOLOG,
    LFLOG,
    LFOPEN = 'O',
    LFSEND = 'S',
    LFRECV = 'R',
    LFNOTE = 'N',
    LFEOF = 'E',
    LFCLOSE = 'C'
};

struct UUSOCKET {
    time_t lastActivity;
    time_t expires;
    int timeout;
    int savedSendErrno;
    int savedRecvErrno;
    int lastRecvWant;
    int lastSendWant;
    int logFile;
    int fiddlerRecvFile;
    int fiddlerSendFile;
    enum LOGFILEDIRECTION logFileDirection;
#ifdef WIN32
    DWORD logFileActivity;
#else
    time_t logFileActivity;
#endif
    size_t sendBufferEob;
    size_t recvBufferOfs;
    size_t recvBufferEob;
    intmax_t recvChunkRemain;
    size_t recvMarkChunkRemain;
    size_t sendChunkRemain;
    char sendChunkHeader[16];
    size_t utf16BufferEob;
    uintmax_t sendCount;
    uintmax_t recvCount;
    /* These should probably be a union based on socket type, but the overhead is very
       low for now, so that's being overlooked */
    SOCKET o;
    struct UUMEMORYBUFFER *mb;
    enum UUSTMEMORYACCESS mbAccess;
#ifdef USEPOLL
    short pollIdx;
#endif
    char recvChunking;
    char sendChunking;
    char utf16;
    char utf16Eof;
    char sendOK;
    char recvOK;
    char recvEof;
    char shut_wr;
    char shut_rd;
    char autoFlushOnRecv;
    enum UUSOCKETTYPE socketType;
    char sendBuffer[UUSOCKETSENDBUFFERSIZE];
#define sendZipBuffer sendBuffer
    /* char sendZipBuffer[UUSOCKETSENDZIPBUFFERSIZE]; */
    char recvBuffer[UUSOCKETRECVBUFFERSIZE];
    char recvZBuffer[UUSOCKETRECVZBUFFERSIZE];
    unsigned char utf16Buffer[UUSOCKETUTF16BUFFERSIZE];
    char chunkLen[10];
    SSL *ssl;
    BIO *bio;
    enum SSLLAST sslLast;
    z_stream sendZStream;
    z_stream recvZStream;
    int sslLastStatus;
    int sendZipBytesSinceLastSend;
    char sslWriteWanting;
    char acceptedHttp;
    char lastSendBlocked;
    char sendZip;
    char recvZip;
    char recvZEof;
    char recvZNeedRead;
    char recvMarkSet;
    char recvMarkChunking;
    char recvMarkUtf16;
    char readyToReuse;
};

#define TSMAXSOCKET 1024

struct TRACKSOCKET {
    struct UUSOCKET *s;
    const char *facility;
    time_t opened;
    time_t stopped;
    char host[128];
};

extern struct TRACKSOCKET *ts;

#ifdef USEPOLL
/* If poll is used, then use its native definitions */
#define UUPOLLFD pollfd
#define UUPOLLIN POLLIN
#define UUPOLLPRI POLLPRI
#define UUPOLLOUT POLLOUT
#else
/* But if poll is not, create our own version to make sure we've got
   something to work with
*/
struct UUPOLLFD {
    SOCKET fd;
    short int events;
    short int revents;
};

#define UUPOLLIN 0x001
#define UUPOLLOUT 0x004
#endif

#define UUSNOWAIT 1      /* goes with UUSend/UURecv */
#define UUSSTOPNICE 2    /* goes with UUStopSocket */
#define UUSFLUSH 4       /* goes with UUSend */
#define UUSMARKSET 8     /* goes with UURecv */
#define UUSMARKREWIND 16 /* goes with UURecv */
#define UUSMARKCLEAR 32  /* goes with UURecv */

/* UUSSL* are bit masks, not sequential entries */
#define UUSSLACCEPT 0
#define UUSSLCONNECT 1
#define UUSSLHTTP \
    2 /* indicates that if http encountered during connect, handle cleanly and report back */

#define UUSEHOSTNOTFOUND 1
#define UUSESOCKETFAIL 2
#define UUSECONNECTFAIL 3
#define UUSEBINDFAIL 4
#define UUSESSLFAIL 5
#define UUSECACHEFAIL 6

#define UURECVZIPNONE 0
#define UURECVZIPGZIP 1
#define UURECVZIPDEFLATE 2
#define UURECVZIPINITDEFLATE 3
#define UURECVZIPPROCESSING 4

struct IPPORTSTATUS {
    // unsigned int netIp;
    struct sockaddr_storage netIp;
    int waiting;
    time_t lastSuccess;
    time_t firstFailure;
    time_t lastFailure;
    int failures;
    BOOL down;
    PORT netPort;
};

extern struct UUAVLTREE avlIpPortStatuses;
extern struct UUAVLTREE dnsTable;

void UUInitSocket(struct UUSOCKET *s, SOCKET o);
void UUInitSocketRecvChunking(struct UUSOCKET *s, intmax_t contentLength);
void UUInitSocketStream(struct UUSOCKET *s, SOCKET o);
void UUInitSocketNull(struct UUSOCKET *s);
void UUInitSocketMemory(struct UUSOCKET *s,
                        struct UUMEMORYBUFFER *mb,
                        enum UUSTMEMORYACCESS mbAccess);
char *UUGetHostByAddr(const struct sockaddr_storage *sa, char *host, size_t hostlen);
int UUGetHostByName(const char *host, struct sockaddr_storage *sa, PORT port, int family);
int UUGetHostByNameFailedAddress(const char *host, struct sockaddr_storage *sa, int family);
int UUGetIpInterfaceByName(char *host, struct sockaddr_storage *sa);
int UUConnect(struct UUSOCKET *s, const char *host, PORT port, const char *facility);
int UUConnectWithSource(struct UUSOCKET *s,
                        const char *host,
                        PORT port,
                        const char *facility,
                        struct sockaddr_storage *sl);
int UUConnect2(struct UUSOCKET *s, const char *host, PORT port, const char *facility, int useSsl);
int UUConnectWithSource2(struct UUSOCKET *s,
                         const char *host,
                         PORT port,
                         const char *facility,
                         struct sockaddr_storage *sl,
                         int useSsl);
int UUConnectWithSource3(struct UUSOCKET *s,
                         const char *host,
                         PORT port,
                         const char *facility,
                         struct sockaddr_storage *sl,
                         int useSsl,
                         int sslIdx);
int UUConnectWithSource4(struct UUSOCKET *s,
                         const char *host,
                         PORT port,
                         const char *facility,
                         struct sockaddr_storage *sl,
                         int useSsl,
                         int sslIdx,
                         int maxTries);
int UUConnectWithSource5(struct UUSOCKET *s,
                         const char *host,
                         PORT port,
                         const char *facility,
                         struct sockaddr_storage *sl,
                         int useSsl,
                         int sslIdx,
                         int maxTries,
                         int timeout);
int UURecv(struct UUSOCKET *s, void *msg, size_t len, int uusflags);
int UURecv2(struct UUSOCKET *s, void *msg1, size_t len1, void *msg2, size_t len2, int uusflags);
void UURecvMarkSet(struct UUSOCKET *s);
void UURecvMarkRewind(struct UUSOCKET *s);
int UUSend(struct UUSOCKET *s, const void *msg, size_t len, int uusflags);
int UUSend2(struct UUSOCKET *s, void *msg1, size_t len1, void *msg2, size_t len2, int uusflags);
int UUSendBuffering(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbs, int uusflags);
int UUFlush(struct UUSOCKET *s);
void UUSendReset(struct UUSOCKET *s, int uusflags);
void UURecvReset(struct UUSOCKET *s, int uusflags);
int UURecvUTF16(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbs, int uusflags);
/* declare UUSendBuffering to be the first level send handler */
#define UUSendUUSocketBuffers(s, sbs, uusflags) UUSendBuffering(s, sbs, uusflags)
/* declare UURecvUTF16 to be the first level receive handler */
#define UURecvUUSocketBuffers(s, sbs, uusflags) UURecvUTF16(s, sbs, uusflags)
int UUSendCRLF(struct UUSOCKET *s);
int UUSendHR(struct UUSOCKET *s);
int UUSendZ(struct UUSOCKET *s, const char *str);
int UUSendZCRLF(struct UUSOCKET *s, const char *str);
int UUStopSocket(struct UUSOCKET *s, int uusflags);
int UUStopNormalSocket(SOCKET s);
int UUShutdown(struct UUSOCKET *s, int how);
BOOL UUValidSocketRecv(struct UUSOCKET *s);
BOOL UUSocketEof(struct UUSOCKET *s);
BOOL UUValidSocketSend(struct UUSOCKET *s);
void TSInit(void);
void TSOpenSocket(struct UUSOCKET *s, const char *host, const char *facility);
void TSCloseSocket(SOCKET o);
void UUInitSocketCore(struct UUSOCKET *s, enum UUSOCKETTYPE socketType);
int UUPoll(struct UUPOLLFD *fds, int nfds, int timeout);
int UURecvDataWaiting(struct UUSOCKET *s);
int UUSendDataWaiting(struct UUSOCKET *s);
void MemoryBufferInit(struct UUMEMORYBUFFER *mb, char *buffer, int readEob, int bufferSize);
/* Special case: if stopTime is less than 3600, treat as relative to now instead of absolute time */
int UUWant(SOCKET o, int want, time_t stopTime);
#ifdef WIN32
#define MoveFileDescriptorHigh(x) x
#else
SOCKET MoveFileDescriptorHigh(SOCKET s);
#endif

void LogSsl(char *string);
void UUInitSsl(void);
void UUInitSslKey(int newIndex, BOOL force);
int RandNumber(int max);
BOOL UUIsRFC952Hostname(char *n, BOOL allowUnderscores, BOOL allowColons);
BOOL UUSslInitialized(void);
void UURandomForSsl(char *buffer, int len);
int UURecvSsl(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags);
int UUSendSsl(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbs, int uusflags);
void UUInitSocketSsl(struct UUSOCKET *s, SOCKET o, int flags, int sslIdx, const char *host);
char *UUinet_ntoa(char *outbuffer, struct in_addr in);
void UUSSLThreadCleanup(void);
void UUSslFilesFreeList(struct UUSSLFILES *head);
struct UUSSLFILES *UUSslFiles(int *highest, BOOL returnList);
int UUSslHighestFile(void);
int UUSslCnf(int keysize,
             char *algorithm,
             char *country,
             char *state,
             char *locality,
             char *organization,
             char *unit,
             char *cn,
             char *email,
             char *alt1,
             char *alt2);
SSL_CTX *UUSslLoadKey(int index,
                      BOOL logErrors,
                      X509 **cert,
                      BOOL useCache,
                      enum SSLLOADKEYERROR *slke);
BOOL UUSslEnabled(void);
char *UUSslKeyCreated(int index, char *dFindIpPortStatusateBuffer, size_t dateBufferSize);
BOOL UUSslAvailable(void);
char *UUSslSubjectAltNamesFromCSR(X509_REQ *csr);
char *UUSslSubjectAltNamesFromCRT(X509 *crt);
char *UUNetIPLongToASCII(char *outbuffer, unsigned long un);
char *UUHostIPLongToASCII(char *outbuffer, unsigned long uh);
char *UUActiveCertExpiration(void);
const char *UUActiveCertCN(void);
char *UUActiveCertSubjectAltNames(void);
BOOL UUSSLActive(void);
void UUSocketBuffersRemoveSendBytes(struct UUSOCKETBUFFERS *sbs, int len);
size_t UUSocketBuffersTotalLen(const struct UUSOCKETBUFFERS *sbs);
void UUSocketBuffersCopy(struct UUSOCKETBUFFERS *dest, const struct UUSOCKETBUFFERS *src);

int UUAvlTreeCmpIpPortStatuses(const void *v1, const void *v2);
int UUAvlTreeCmpDnsCache(const void *v1, const void *v2);

struct IPPORTSTATUS *FindIpPortStatus(struct sockaddr_storage netIp, PORT netPort);

int GetIpPortStatus(struct IPPORTSTATUS *ips, BOOL haveMutex);
void SetIpPortStatus(struct IPPORTSTATUS *ips, BOOL success, int waitingAdjustment);

uint16_t get_port(const struct sockaddr_storage *sa);

/* helpers for ipv6 */
// BOOL compareInAddr(struct sockaddr_storage s1, struct sockaddr_storage s2);
int get_size(const struct sockaddr_storage *sa);
void *get_in_addr2(const struct sockaddr_storage *sa);
void *get_in_addr(const struct sockaddr *sa);
const char *ipToStr(const struct sockaddr_storage *sa, char *ipBuffer, const size_t size);
const char *ipToStrV6Brackets(const struct sockaddr_storage *sa, char *ipBuffer, const size_t size);
void set_inaddr_any(struct sockaddr_storage *sa);
void set_port(struct sockaddr_storage *sa, PORT port);
void set_ip(struct sockaddr_storage *sa,
            unsigned long b32_1,
            unsigned long b32_2,
            unsigned long b32_3,
            unsigned long b32_4);
BOOL set_ip_bit(struct sockaddr_storage *sa, int bit, int value);
void increment_ip(struct sockaddr_storage *sa, int amount);
int inet_pton_st(const int family, const char *host, struct sockaddr_storage *sa);
int inet_pton_st2(const char *addr, struct sockaddr_storage *sa);
uint16_t get_port(const struct sockaddr_storage *sa);
BOOL is_unspecified(struct sockaddr_storage *sa);
BOOL is_ip_zero(struct sockaddr_storage *sa);
BOOL is_addrnone(const struct sockaddr_storage *sa);
BOOL is_inet(struct sockaddr_storage *sa);
BOOL is_ip_equal(struct sockaddr_storage *sa1, struct sockaddr_storage *sa2);
int is_max_range(struct sockaddr_storage *lower, struct sockaddr_storage *upper);
int compare_ip(const struct sockaddr_storage *sa1, const struct sockaddr_storage *sa2);
struct sockaddr_storage *get_max_ip(struct sockaddr_storage *sa1, struct sockaddr_storage *sa2);
struct sockaddr_storage *get_min_ip(struct sockaddr_storage *sa1, struct sockaddr_storage *sa2);
unsigned long get_range32(struct sockaddr_storage *lower,
                          struct sockaddr_storage *upper,
                          char *buffer);
void init_storage(struct sockaddr_storage *sa,
                  const int family,
                  const BOOL inAddrAny,
                  const PORT port);
void UUfreeaddrinfo(struct addrinfo *ai);
void DnsCacheBeginThread(void *v);
const char *ipToStrV6Bracketed(const struct sockaddr_storage *sa,
                               char *ipBuffer,
                               const size_t size);

/* missing function */
#ifdef WIN32
int inet_pton(int af, const char *src, void *dst);
const char *inet_ntop(int af, const void *src, char *dst, socklen_t cnt);
#endif

void initTavls(void);

#endif  // __UUSOCKET_H__
