#define __UUFILE__ "adminauth.c"

#include "common.h"
#include "uustring.h"

#include "adminauth.h"

void AdminAuth(struct TRANSLATE *t, char *query, char *post) {
    char *verb1, *verb2;
    struct UUSOCKET *s = &t->ssocket;
    char state;
    const int forceIdx = 0;
    struct SESSION *e;
    struct FORMFIELD ffs[] = {
        {"force", NULL, 0, 0, 64},
        {NULL,    NULL, 0, 0, 0 }
    };

    if (optionRequireAuthenticate == 0) {
        HTTPCode(t, 404, 1);
        return;
    }

    /* Preserve the old way of working */
    state = REQUIREAUTHENTICATEMAX;
    if (query != NULL) {
        if (strcmp(query, "0") == 0)
            state = REQUIREAUTHENTICATENEVER;
        else if (strcmp(query, "1") == 0)
            state = REQUIREAUTHENTICATEPERM;
    }

    FindFormFields(query, post, ffs);

    if (state == REQUIREAUTHENTICATEMAX) {
        if (ffs[forceIdx].value) {
            state = ffs[forceIdx].value[0];
            if (state < '0' || state > '2')
                state = REQUIREAUTHENTICATEMAX;
            else
                state = state - '0';
        }
    }

    HTTPCode(t, 200, 0);
    if (t->version) {
        HTTPContentType(t, NULL, 0);
        NoCacheHeaders(s);
        if (state >= 0 && state <= 2) {
            if (state == REQUIREAUTHENTICATEPERM) {
                WriteLine(s, "Set-Cookie: %s=%c; expires=Tue, 01-Jan-2030 00:00:00 GMT",
                          requireAuthenticateCookieName, state + '0');
            } else if (state == REQUIREAUTHENTICATENEVER) {
                WriteLine(s, "Set-Cookie: %s=0; expires=Fri, 01-Jan-1999 00:00:00 GMT",
                          requireAuthenticateCookieName);
            } else if (state == REQUIREAUTHENTICATETEMP) {
                WriteLine(s, "Set-Cookie: %s=%c", requireAuthenticateCookieName, state + '0');
            }
            UUSendZ(s, "; Path=/");
            if (loginCookieDomain) {
                if (optionSafariCookiePatch && t->safari)
                    WriteLine(s, "; Domain=.%s", myName);
                else
                    WriteLine(s, "; Domain=%s", loginCookieDomain);
            }
            UUSendCRLF(s);
        }
    }

    HeaderToBody(t);

    if (state == REQUIREAUTHENTICATEMAX) {
        verb1 = "are ";
        verb2 = "";
        state = t->requireAuthenticate;
    } else {
        verb1 = "will ";
        verb2 = " be";
    }

    if (state >= 0 && state < REQUIREAUTHENTICATEMAX) {
        char localFile[MAX_PATH];
        sprintf(localFile, DOCSDIR "auth%d.htm", state);
        if (FileExists(localFile)) {
            SendFile(t, localFile, SFREPORTIFMISSING);
            goto Finished;
        }
    }

    if ((e = t->session) && e->priv)
        AdminHeader(t, AHNOHTTPHEADER, "Force Authentication");

    WriteLine(s, "<p>EZproxy users of this computer %s%s%s forced to authenticate%s", verb1,
              (state == 0 ? "not " : ""), verb2,
              (state == REQUIREAUTHENTICATETEMP ? " until you close all browser windows" : ""));

    if (state != 0)
        UUSendZ(s, ", overriding AutoLoginIP and ExcludeIP");

    UUSendZ(s, "</p>\n<p>To change this behavior, use one of the following links:</p>\n");

    if (state != REQUIREAUTHENTICATENEVER)
        UUSendZ(s, "<div><a href='/auth?force=0'>Never force authentication</div>\n");

    if (state != REQUIREAUTHENTICATETEMP)
        UUSendZ(s,
                "<div><a href='/auth?force=2'>Force authentication until you close all browser "
                "windows</div>\n");

    if (state != REQUIREAUTHENTICATEPERM)
        UUSendZ(s, "<div><a href='/auth?force=1'>Always force authentication</div>\n");

    if ((e = t->session) && e->priv)
        AdminFooter(t, 0);

Finished:;
}
