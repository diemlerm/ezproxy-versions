#define __UUFILE__ "adminip.c"

#include "common.h"
#include "uustring.h"

#include "adminip.h"

void AdminIP(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    char ipBuffer[INET6_ADDRSTRLEN];
    BOOL anyExcluded;

    const int AIADMIN = 0;
    const int AIDETAILS = 1;

    struct FORMFIELD ffs[] = {
        {"admin", NULL, 0, 0, 10},
        {"details", NULL, 0, 0, INET6_ADDRSTRLEN},
        {NULL, NULL, 0, 0}
    };
    BOOL adminHeader = 0;
    char *detail = NULL;

    FindFormFields(query, post, ffs);

    adminHeader = ffs[AIADMIN].value != NULL;

    if (detail = ffs[AIDETAILS].value) {
        struct SESSION *e = t->session;
        if (e == NULL || e->priv == 0) {
            AdminUnauthorized(t);
            return;
        }
        adminHeader = 1;
    }

    if (adminHeader)
        AdminHeader(t, 0, detail ? "View Your IP Address Details" : "View Your IP Address");
    else {
        AdminMinimalHeader(t, "View Your IP Address");
    }

    if (detail) {
        struct LOCATION location;
        FindLocationByASCIIIP(&location, detail);
        WriteLine(s, "<table class='bordered-table th-row-nobold'>\n");
        WriteLine(s,
                  "<tr><th scope='col'>Field</th><th scope='col'>Code</th><th "
                  "scope='col'>Value</th></tr>\n");
        WriteLine(s, "<tr><th scope='row'>IP</th><td>");
        SendHTMLEncoded(s, detail);
        WriteLine(s, "</td><td>&nbsp;</td></tr>\n");

        WriteLine(s, "<tr><th scope='row'>Country</th><td>");
        SendHTMLEncoded(s, location.countryCode);
        WriteLine(s, "</td><td>");
        if (location.countryName[0]) {
            SendHTMLEncoded(s, location.countryName);
        } else {
            WriteLine(s, "&nbsp;");
        }
        WriteLine(s, "</td></tr>\n");

        if (location.region[0]) {
            WriteLine(s, "<tr><th scope='row'>Region</th><td>");
            SendHTMLEncoded(s, location.region);
            WriteLine(s, "</td><td>");
            if (location.regionName[0]) {
                WriteLine(s, " ");
                SendHTMLEncoded(s, location.regionName);
            } else {
                WriteLine(s, "&nbsp;");
            }

            WriteLine(s, "</td></tr>\n");
        }
        if (location.city[0]) {
            WriteLine(s, "<tr><th scope='row'>City</th><td>");
            SendHTMLEncoded(s, location.city);
            WriteLine(s, "</td><td>&nbsp;</td></tr>\n");
        }
        WriteLine(s, "</table>");
    } else {
        WriteLine(s, "<div>You are connecting from %s</div>\n",
                  ipToStr(&t->sin_addr, ipBuffer, sizeof ipBuffer));
        URLIpAccess(t, NULL, NULL, NULL, &anyExcluded, NULL);
        if (anyExcluded) {
            UUSendZ(s,
                    "<div>This address is excluded from proxying by at least one database "
                    "definition.</div>\n");
        }
    }

    if (adminHeader) {
        AdminFooter(t, 0);
    } else {
        AdminMinimalFooter(t);
    }
}
