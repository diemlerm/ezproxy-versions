#ifndef __USRFOLLETT_H__
#define __USRFOLLETT_H__

#include "common.h"

enum FINDUSERRESULT FindUserFollett(struct TRANSLATE *t,
                                    struct USERINFO *uip,
                                    char *file,
                                    int f,
                                    struct FILEREADLINEBUFFER *frb,
                                    struct sockaddr_storage *pInterface);

#endif  // __USRFOLLETT_H__
