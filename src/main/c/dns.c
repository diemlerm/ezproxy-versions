#define __UUFILE__ "dns.c"

#include "common.h"
#include <stdarg.h>

typedef unsigned char UINT8;
typedef unsigned short UINT16;
typedef unsigned int UINT32;

struct DNSMSG {
    UINT16 id;

    UINT8 flags1;
    UINT8 flags2;

    UINT16 numQuestion;
    UINT16 numAnsRR;
    UINT16 numAuthRR;
    UINT16 numAddRR;
    UINT8 body[1024];
};

UINT8 get_flags_qr(struct DNSMSG *dm) {
    return (dm->flags1 >> 7) & 1;
}

UINT8 get_flags_opcode(struct DNSMSG *dm) {
    return (dm->flags1 >> 3) & 15;
}

UINT8 get_flags_aa(struct DNSMSG *dm) {
    return (dm->flags1 >> 2) & 1;
}

UINT8 get_flags_tc(struct DNSMSG *dm) {
    return (dm->flags1 >> 1) & 1;
}

UINT8 get_flags_rd(struct DNSMSG *dm) {
    return dm->flags1 & 1;
}

UINT8 get_flags_ra(struct DNSMSG *dm) {
    return (dm->flags2 >> 7) & 1;
}

UINT8 get_flags_rcode(struct DNSMSG *dm) {
    return dm->flags2 & 15;
}

void set_flags_qr(struct DNSMSG *dm, UINT8 v) {
    v = v << 7;
    dm->flags1 = (dm->flags1 & 127) | v;
}

void set_flags_opcode(struct DNSMSG *dm, UINT8 v) {
    v = v << 3;
    dm->flags1 = (dm->flags1 & 135) | v;
}

void set_flags_aa(struct DNSMSG *dm, UINT8 v) {
    v = v << 2;
    dm->flags1 = (dm->flags1 & 251) | v;
}

void set_flags_tc(struct DNSMSG *dm, UINT8 v) {
    v = v << 1;
    dm->flags1 = (dm->flags1 & 253) | v;
}

void set_flags_rd(struct DNSMSG *dm, UINT8 v) {
    dm->flags1 = (dm->flags1 & 254) | v;
}

void set_flags_ra(struct DNSMSG *dm, UINT8 v) {
    v = v << 7;
    dm->flags2 = (dm->flags2 & 127) | v;
}

void set_flags_z(struct DNSMSG *dm, UINT8 v) {
    v = v << 4;
    dm->flags2 = (dm->flags2 & 143) | v;
}

void set_flags_rcode(struct DNSMSG *dm, UINT8 v) {
    dm->flags2 = (dm->flags2 & 240) | v;
}

int StartDnsSockets(void) {
    int i;
    struct DNSPORT *dp;
    int one = 1;
    struct sockaddr_storage sl;
    int rc;
    char ipBuffer[INET6_ADDRSTRLEN];

    for (i = 0, dp = dnsPorts; i < cDnsPorts; i++, dp++) {
        init_storage(&sl, dp->ipInterface.ss_family, TRUE, 0);

        dp->udpSocket = MoveFileDescriptorHigh(socket(dp->ipInterface.ss_family, SOCK_DGRAM, 0));
        if (debugLevel > 5)
            Log("%" SOCKET_FORMAT " Opened socket, errno=%d", dp->udpSocket,
                (dp->udpSocket == INVALID_SOCKET) ? errno : 0);
        if (dp->udpSocket == INVALID_SOCKET)
            goto Error;
        if (setsockopt(dp->udpSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&one, sizeof(one)))
            goto Error;
        memcpy(&sl, &dp->ipInterface, sizeof(sl));

        set_port(&sl, dp->port);

        if (bind(dp->udpSocket, (struct sockaddr *)&sl, get_size(&sl)))
            goto Error;

        SocketNonBlocking(dp->udpSocket);

        dp->tcpSocket = StartListener(dp->port, &dp->ipInterface, dnsSocketBacklog);
        if (dp->tcpSocket == INVALID_SOCKET) {
            inet_ntop(dp->ipInterface.ss_family, get_in_addr2(&dp->ipInterface), ipBuffer,
                      sizeof ipBuffer);
            Log("Unable to create DNS TCP listener on %s", ipBuffer);
            return 1;
        }
    }

    return 0;

Error:
    rc = socket_errno;
    if (dp->udpSocket != INVALID_SOCKET) {
        closesocket(dp->udpSocket);
    }
    inet_ntop(dp->ipInterface.ss_family, get_in_addr2(&dp->ipInterface), ipBuffer, sizeof ipBuffer);
    Log("Unable to create DNS UDP listener on %s (%d)", ipBuffer, rc);
    return 2;
}

/*
int ClusterSendRawMsg(struct PEER *c, struct PEERMSG *msg)
{
    int msglen;
    int rc;
    short datalen;

    rc = sendto(clusterSocket, (char *) msg, msglen, 0, (struct sockaddr *) &c->peerAddress,
sizeof(c->peerAddress)); if (rc == SOCKET_ERROR) { printf("sendto failed: %d", socket_errno);
    }
}
*/

BOOL FindProxyHost(struct TRANSLATE *t, BOOL *me);

UINT8 *DNSInt(UINT8 *ptr, UINT32 val, int intLen) {
    int i;

    ptr += intLen;
    for (i = 0; i < intLen; i++) {
        ptr--;
        *ptr = val & 0xff;
        val >>= 8;
    }
    return ptr + intLen;
}

UINT8 *DNSAddSOA(UINT8 *rs, UINT8 domainOffset) {
    *rs++ = 0xc0;         /* host name refers back to */
    *rs++ = domainOffset; /* host name provided at offset 12 */

    rs = DNSInt(rs, 6, 2);    /* SOA */
    rs = DNSInt(rs, 1, 2);    /* Internet address */
    rs = DNSInt(rs, 3600, 4); /* TTL 3600 */
    rs = DNSInt(rs, 29, 2);   /* data length */
    *rs++ = 0xc0;             /* mname */
    *rs++ = domainOffset;
    *rs++ = 4; /* rname root.(mydomain) */
    *rs++ = 'r';
    *rs++ = 'o';
    *rs++ = 'o';
    *rs++ = 't';
    *rs++ = 0xc0;
    *rs++ = domainOffset;
    rs = DNSInt(rs, dnsSerial, 4); /* serial */
    rs = DNSInt(rs, 900, 4);       /* refresh */
    rs = DNSInt(rs, 450, 4);       /* retry */
    rs = DNSInt(rs, 86400, 4);     /* expire */
    rs = DNSInt(rs, 86400, 4);     /* minimum */

    return rs;
}

BOOL OneOfMyDNSNames(char *host) {
    int i;
    struct HOST *h;
    struct DATABASE *b;
    struct INDOMAIN *m;

    for (i = 0, h = hosts; i < cHosts; i++, h++) {
        if (CompareJustHost(h->proxyHostname, host, TRUE))
            return 1;
    }

    /* Now check for the possibility that a proxy by hostname was used for which no virtual
       web server has been started.  Also allows the bare-host (non 80-) form to be recognized.
    */
    for (i = 0, b = databases; i < cDatabases; i++, b++) {
        for (m = b->dbdomains; m; m = m->next) {
            if (m->hostOnly == 0 || m->proxyHostname == NULL)
                continue;
            if (CompareJustHost(host, m->proxyHostname, TRUE))
                return 1;
            if (strncmp(host, "80-", 3) == 0 && CompareJustHost(host + 3, m->proxyHostname, TRUE))
                return 1;
            if (strnicmp(host, "s443-", 5) == 0 &&
                CompareJustHost(host + 5, m->proxyHostname, TRUE))
                return 1;
        }
    }

    return 0;
}

BOOL ProcessDNSRequest(struct DNSPORT *dp,
                       BOOL isTcp,
                       struct sockaddr_storage *reqAddr,
                       struct DNSMSG *msgResponse,
                       int *mrsLen,
                       struct DNSMSG *msgRequest,
                       int mrqLen) {
    struct QTQC {
        UINT16 qtype;
        UINT16 qclass;
    } qtqc;
    char prefix[32];
    UINT8 domainOffset;
    UINT16 qtype;
    UINT8 *rq, *rs, *rqe;
    char requestName[2048];
    char *rn;
    size_t requestNameLen;
    struct DNSADDR *da, *daMatch;
    BOOL me = 0;
    char msgBuffer[64], ipBuffer[INET6_ADDRSTRLEN];

    if (dnsSerial == 0)
        UUtime(&dnsSerial);

    if (optionLogDNS) {
        sprintf(msgBuffer, "DNS %s request from %s", (isTcp ? "TCP" : "UDP"),
                ipToStr(reqAddr, ipBuffer, sizeof ipBuffer));
        LogBinaryPacket(msgBuffer, (unsigned char *)msgRequest, mrqLen);
    }

    daMatch = NULL;
    for (da = dp->da; da; da = da->next) {
        if ((da->start).ss_family == reqAddr->ss_family &&
            (da->stop).ss_family == reqAddr->ss_family && compare_ip(&da->start, reqAddr) <= 0 &&
            compare_ip(&da->stop, reqAddr) >= 0)
            daMatch = da;
    }

    if (daMatch == NULL)
        goto Refused;

    if (optionLogDNS) {
        char ipBuffer1[INET6_ADDRSTRLEN], ipBuffer2[INET6_ADDRSTRLEN], ipBuffer3[INET6_ADDRSTRLEN];

        Log("DNS source matched %s-%s; response address %s",
            ipToStr(&daMatch->start, ipBuffer1, sizeof ipBuffer1),
            ipToStr(&daMatch->stop, ipBuffer2, sizeof ipBuffer2),
            ipToStr(&daMatch->addr, ipBuffer3, sizeof ipBuffer3));
    }

    rqe = ((UINT8 *)msgRequest) + mrqLen;

    *mrsLen = 0;

    memset(msgResponse, 0, sizeof(*msgResponse));

    memcpy(msgResponse, msgRequest, 4);
    set_flags_qr(msgResponse, 1);
    set_flags_aa(msgResponse, 0);
    set_flags_tc(msgResponse, 0);
    set_flags_ra(msgResponse, 0);
    set_flags_z(msgResponse, 0);
    set_flags_rcode(msgResponse, 0);
    msgResponse->numQuestion = htons(1);
    msgResponse->numAnsRR = 0;
    msgResponse->numAuthRR = 0;
    msgResponse->numAddRR = 0;

    if (get_flags_qr(msgRequest) != 0) {
        if (optionLogDNS)
            Log("DNS got QR=1, refused");
        goto Refused;
    }

    if (get_flags_opcode(msgRequest) != 0)
        goto NotImplemented;

    /* Behavior of BIND when QDCOUNT not 1 */
    if (ntohs(msgRequest->numQuestion) != 1)
        goto FormatError;

    /* Transfer question hostname */

    rn = requestName;
    *rn = 0;

    for (rq = msgRequest->body, rs = msgResponse->body; rq < rqe;) {
        if (*rq == 0) {
            rq++;
            *rs++ = 0;
            break;
        }
        /* At this point in the question, there can be no back-references and things
        should not ever point beyond end of packet */
        if (*rq > 63 || rq + *rq > rqe)
            goto FormatError;
        memcpy(rs, rq, *rq + 1);
        if (rn > requestName)
            *rn++ = '.';
        memcpy(rn, rq + 1, *rq);
        rn += *rq;
        *rn = 0;
        rq += *rq + 1;
        rs += *rs + 1;
    }

    if (rq + 4 > rqe)
        goto FormatError;

    memcpy(&qtqc, rq, sizeof(qtqc));

    if (ntohs(qtqc.qclass) != 1 /* IN */)
        goto NotImplemented;

    qtype = ntohs(qtqc.qtype);

    memcpy(rs, rq, 4);

    rq += 4;
    rs += 4;

    requestNameLen = strlen(requestName);

    if (requestNameLen > 255)
        goto FormatError;

    /* If the request is not within this server domain, then we can't help them */
    if (requestNameLen < myNameLen ||
        stricmp(requestName + requestNameLen - myNameLen, myName) != 0 ||
        (requestNameLen > myNameLen && *(requestName + requestNameLen - myNameLen - 1) != '.')) {
        set_flags_rcode(msgResponse, 2);
        *mrsLen = rs - msgResponse->body + 12;
        if (optionLogDNS)
            LogBinaryPacket("DNS not domain response", (unsigned char *)msgResponse, *mrsLen);
        return 1;
    }

    domainOffset = 0x0c + (requestNameLen - myNameLen);

    /* Now we know we are in our domain, so set authority true */
    set_flags_aa(msgResponse, 1);

    me = myNameLen == requestNameLen;

    /* if it's not me and it's not login.(myDomain), check it explicitly */
    /* The "login." version is checked by ValidRefererHostname now */
    if (me == 0 && (!ValidRefererHostname(requestName, requestNameLen, NULL))) {
        if (optionAnyDNSHostname == 0) {
            if (OneOfMyDNSNames(requestName) == 0) {
                set_flags_rcode(msgResponse, 3);
                goto AddAuthority;
            }
        }
    }

    msgResponse->numAnsRR = htons(0);

    if (qtype == 1 || qtype == 255) {
        msgResponse->numAnsRR = htons((short)(ntohs(msgResponse->numAnsRR) + 1));
        *rs++ = 0xc0; /* host name refers back to */
        *rs++ = 0x0c; /* host name provided at offset 12 */

        // TODO: Make sure this is correct, prolly check with Chris
        if (daMatch->addr.ss_family == AF_INET)
            rs = DNSInt(rs, 1, 2); /* 1 return type A */
        else
            rs = DNSInt(rs, 28, 2); /* return type AAAA */

        rs = DNSInt(rs, 1, 2);    /* 1 Internet Address */
        rs = DNSInt(rs, 3600, 4); /* TTL 3600 */
        rs = DNSInt(rs, 4, 2);    /* data length 4 */

        if (daMatch->addr.ss_family == AF_INET) {
            memcpy(rs, &((struct sockaddr_in *)&daMatch->addr)->sin_addr, 4);
            rs += 4;
        } else {
            memcpy(rs, &((struct sockaddr_in6 *)&daMatch->addr)->sin6_addr, 16);
            rs += 16;
        }
    }

    if (me && (qtype == 2 || qtype == 255)) {
        msgResponse->numAnsRR = htons((short)(ntohs(msgResponse->numAnsRR) + 1));
        *rs++ = 0xc0; /* host name refers back to */
        *rs++ = 0x0c; /* host name provided at offset 12 */

        rs = DNSInt(rs, 2, 2);    /* 2 return type NS */
        rs = DNSInt(rs, 1, 2);    /* 1 Internet Address */
        rs = DNSInt(rs, 3600, 4); /* TTL 3600 */
        rs = DNSInt(rs, 2, 2);    /* data length 2 */
        *rs++ = 0xc0;             /* hostname refers back to */
        *rs++ = domainOffset;
    }

    if (me && (qtype == 6 || qtype == 255)) {
        msgResponse->numAnsRR = htons((short)(ntohs(msgResponse->numAnsRR) + 1));
        rs = DNSAddSOA(rs, domainOffset);
    }

AddAuthority:
    /* Now provide authority */
    msgResponse->numAuthRR = htons(1);
    rs = DNSAddSOA(rs, domainOffset);

    /* Now provide additional to go with authority */
    msgResponse->numAddRR = htons(1);

    *rs++ = 0xc0;         /* host name refers back to */
    *rs++ = domainOffset; /* host name provided at offset 12 */

    // TODO: Make sure this is correct, prolly check with Chris
    if (daMatch->addr.ss_family == AF_INET)
        rs = DNSInt(rs, 1, 2); /* 1 return type A */
    else
        rs = DNSInt(rs, 28, 2); /* return type AAAA */

    rs = DNSInt(rs, 1, 2);    /* 1 Internet Address */
    rs = DNSInt(rs, 3600, 4); /* TTL 3600 */
    rs = DNSInt(rs, 4, 2);    /* data length 4 */

    if (daMatch->addr.ss_family == AF_INET) {
        memcpy(rs, &((struct sockaddr_in *)&daMatch->addr)->sin_addr, 4);
        rs += 4;
    } else {
        memcpy(rs, &((struct sockaddr_in6 *)&daMatch->addr)->sin6_addr, 16);
        rs += 16;
    }

    /* Packet ready */

    *mrsLen = rs - msgResponse->body + 12;

    goto LogReturn;

FormatError:
    set_flags_rcode(msgResponse, 1);
    goto ErrorBody;

NotImplemented:
    set_flags_rcode(msgResponse, 4);
    goto ErrorBody;

Refused:
    set_flags_rcode(msgResponse, 5);
    goto ErrorBody;

ErrorBody:
    msgResponse->numQuestion = 0;
    msgResponse->numAnsRR = 0;
    msgResponse->numAuthRR = 0;
    msgResponse->numAddRR = 0;
    *mrsLen = 12;

LogReturn:
    if (optionLogDNS) {
        sprintf(prefix, "DNS response %d", get_flags_rcode(msgResponse));
        LogBinaryPacket(prefix, (unsigned char *)msgResponse, *mrsLen);
    }

    return 1;
}

struct DNSTCP {
    struct DNSPORT *dp;
    struct sockaddr_storage peer;
    socklen_t_M peerLen;
    SOCKET s;
};

void DoDNSTcp(void *v) {
    struct DNSTCP *dt = (struct DNSTCP *)v;
    struct UUSOCKET rs;
    struct UUSOCKET *s = &rs;
    int len;
    struct DNSMSG msgRequest, msgResponse;
    int msgRequestLen, msgResponseLen;
    unsigned short lenBuffer;
    int got;

    UUInitSocket(s, dt->s);
    TSOpenSocket(s, "", "DoDNSTcp");

    for (;;) {
        for (got = 0; got < 2;) {
            len = UURecv(s, ((char *)&lenBuffer) + got, 2 - got, 0);
            if (len <= 0)
                goto Done;
            got += len;
        }
        msgRequestLen = ntohs(lenBuffer);
        if (msgRequestLen > 512)
            goto Done;

        for (got = 0; got < msgRequestLen;) {
            len = UURecv(s, ((char *)&msgRequest) + got, msgRequestLen - got, 0);
            if (len <= 0)
                goto Done;
            got += len;
        }
        if (ProcessDNSRequest(dt->dp, 1, &dt->peer, &msgResponse, &msgResponseLen, &msgRequest,
                              msgRequestLen)) {
            lenBuffer = htons((short)msgResponseLen);
            UUSend2(s, &lenBuffer, 2, &msgResponse, msgResponseLen, 0);
        } else
            goto Done;
    }

Done:
    UUStopSocket(s, 0);
    FreeThenNull(dt);
}

void DNSTcp(struct DNSPORT *dp) {
    struct DNSTCP *dt;
    struct linger linger;

    for (;;) {
        if (!(dt = calloc(1, sizeof(struct DNSTCP))))
            PANIC;
        dt->peerLen = sizeof(dt->peer);
        dt->s = MoveFileDescriptorHigh(
            accept(dp->tcpSocket, (struct sockaddr *)&dt->peer, &dt->peerLen));
        if (debugLevel > 5)
            Log("%" SOCKET_FORMAT " Accept socket, errno=%d", dt->s,
                (dt->s == INVALID_SOCKET) ? errno : 0);
        if (dt->s == INVALID_SOCKET) {
            FreeThenNull(dt);
            return;
        }
        SocketNonBlocking(dt->s);
        dt->dp = dp;
        if (UUBeginThread(DoDNSTcp, (void *)dt, "DNSTcp")) {
            /* turn linger off to make socket close faster */
            linger.l_onoff = 1;
            linger.l_linger = 0;
            setsockopt(dt->s, SOL_SOCKET, SO_LINGER, (char *)&linger, sizeof(linger));
            closesocket(dt->s);
            FreeThenNull(dt);
        }
    }
}

void DoDNS(void *v) {
    int rc;
    struct sockaddr_storage sf;
    socklen_t_M sfl;
    int i;
    struct DNSMSG msgRequest, msgResponse;
    int msgRequestLen, msgResponseLen;
#ifdef USEPOLL
    struct pollfd *pfds;
    int j;
#else
    fd_set sfds;
    struct timeval tv;
#endif
    time_t now;
    int status;
    struct DNSPORT *dp;

#ifdef USEPOLL
    if (!(pfds = calloc(cDnsPorts * 2, sizeof(struct pollfd))))
        PANIC;
#endif

    for (;;) {
        ChargeAlive(GCDNS, &now);

#ifdef USEPOLL
        for (dp = dnsPorts, i = 0, j = 0; i < cDnsPorts; dp++, i++, j++) {
            pfds[j].fd = dp->tcpSocket;
            pfds[j].events = POLLIN;
            j++;
            pfds[j].fd = dp->udpSocket;
            pfds[j].events = POLLIN;
        }
        rc = poll(pfds, cDnsPorts * 2, 1000);
        if (rc <= 0) {
#ifndef WIN32
            if ((errno == ECANCELED)) {
                break;
            }
#endif
            continue;
        }
        for (dp = dnsPorts, i = 0, j = 0; i < cDnsPorts; dp++, i++, j++) {
            if (pfds[j].revents & POLLIN) {
                DNSTcp(dp);
            }
            j++;
            if (pfds[j].revents & POLLIN) {
#else
        FD_ZERO(&sfds);
        for (dp = dnsPorts, i = 0; i < cDnsPorts; dp++, i++) {
            FD_SET(dp->udpSocket, &sfds);
            FD_SET(dp->tcpSocket, &sfds);
        }
        tv.tv_sec = 1;
        tv.tv_usec = 0;
        rc = select(dnsPorts->udpSocket + 1, &sfds, NULL, NULL, &tv);
        if (rc <= 0)
            continue;
        for (dp = dnsPorts, i = 0; i < cDnsPorts; dp++, i++) {
            if (FD_ISSET(dp->tcpSocket, &sfds)) {
                DNSTcp(dp);
            }
            if (FD_ISSET(dp->udpSocket, &sfds)) {
#endif
                sfl = sizeof(sf);
                msgRequestLen =
                    recvfrom(dp->udpSocket, (char *)&msgRequest, (size_t_M)sizeof(msgRequest), 0,
                             (struct sockaddr *)&sf, &sfl);
                if (msgRequestLen < 0) {
                    /* Linux indicates that there is data on the socket, but then returns this
                     * error? */
#ifdef ECONNREFUSED
                    if (socket_errno != ECONNREFUSED)
#endif
                        printf("DNS error receiving: %d\n", socket_errno);
                    continue;
                }

                if (msgRequestLen < 12)
                    continue;

                if (get_flags_qr(&msgRequest) != 0)
                    continue;

                if (ProcessDNSRequest(dp, 0, &sf, &msgResponse, &msgResponseLen, &msgRequest,
                                      msgRequestLen)) {
                    status = sendto(dp->udpSocket, (char *)&msgResponse, msgResponseLen, 0,
                                    (struct sockaddr *)&sf, sfl);
                }
            }
        }
    }
    ChargeStopped(GCDNS, NULL);
}
