/**
 * Future feature, currently unfinished and disabled.
 *
 * /sliptest admin URL to provide support for a local administrator
 * to perform testing with EZproxy configured as their real proxy server.
 */

#define __UUFILE__ "adminsliptest.c"

#include "common.h"
#include "uustring.h"

#include "adminsliptest.h"

struct SESSION *AdminSlipTestSession(struct TRANSLATE *t) {
    struct SESSION *e;

    for (e = NULL; e = FindNextSessionByIP(e, &t->sin_addr);) {
        if (e->priv & SESSION_PRIV_SLIPTEST)
            break;
    }

    return e;
}

void AdminSlipTest(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    struct SESSION *e = t->session;
    struct SESSION *activeSession;
    char ipBuffer[INET6_ADDRSTRLEN];

    AdminHeader(t, 0, "Enable Slip Testing");

    if ((e->priv & SESSION_PRIV_SLIPTEST) == 0) {
        if ((activeSession = AdminSlipTestSession(t)) && activeSession != e) {
            UUSendZ(s, "<p>Only one session per IP can perform slip testing at a time.</p>\n");
            WriteLine(s, "<p>Session %s is already performing testing on IP %s.</p>",
                      activeSession->key, ipToStr(&t->sin_addr, ipBuffer, sizeof ipBuffer));
            WriteLine(s,
                      "To take over slip testing for your session, click <a "
                      "href='/sliptest?override=%s'>here</a>.</p>\n",
                      activeSession->key);
            goto Finished;
        }
    }

    e->priv ^= SESSION_PRIV_SLIPTEST;
    NeedSave();

    if (e->priv & SESSION_PRIV_SLIPTEST) {
        UUSendZ(s, "<p>Slip Testing enabled</p>\n");
    } else {
        UUSendZ(s, "<p>Slip Testing disabled</p>\n");
    }

Finished:
    AdminFooter(t, 0);
}

BOOL AdminSlipTestReport(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket;
    struct SESSION *e = t->session;
    char *referer;

    if (StartsIWith(t->urlCopy, "http://") == NULL && StartsIWith(t->urlCopy, "https://") == NULL)
        return 0;

    if (e == NULL) {
        for (e = NULL; e = FindNextSessionByIP(e, &t->sin_addr);) {
            if (e->priv & SESSION_PRIV_SLIPTEST)
                break;
        }
        if (e == NULL)
            return 0;
    }

    t->finalStatus = 993;

    HTMLHeader(t, htmlHeaderOmitCache);

    UUSendZ(s, TAG_DOCTYPE_HTML_HEAD "</head><body>Attempt to access ");
    SendHTMLEncoded(s, t->urlCopy);

    if (referer = FindField("Referer", t->requestHeaders)) {
        LinkField(referer);
        UUSendZ(s, " from referring URL ");
        SendHTMLEncoded(s, referer);
    }
    UUSendZ(s, "</body></html>\n");
    return 1;
}
