#ifndef __ADMINPASSWORD_H__
#define __ADMINPASSWORD_H__

#include "common.h"

void AdminPassword(struct TRANSLATE *t, char *oldPass, char *newPass, char *verifyPass, char *url);

#endif  // __ADMINPASSWORD_H__
