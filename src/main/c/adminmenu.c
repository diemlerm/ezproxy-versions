#define __UUFILE__ "adminmenu.c"

#include "common.h"
#include "uustring.h"

#include "adminmenu.h"
#include "admintop.h"

void AdminMenu(struct TRANSLATE *t, char *query, char *post) {
    char *menu;
    BOOL anyExcluded;

    if (mainMenu) {
        LocalRedirect(t, mainMenu, 0, 0, NULL);
    } else if (t->session == NULL &&
               (optionExcludeIPMenu == 0 ||
                (URLIpAccess(t, NULL, NULL, NULL, &anyExcluded, NULL), anyExcluded == 0))) {
        AdminTop(t, query, post);
    } else {
        HTMLHeader(t, htmlHeaderMaxAgeCache);
        menu = (t->session && (t->session)->menu[0] ? (t->session)->menu : MENUHTM);
        SendEditedFile(t, NULL, menu, 1, NULL, 0, NULL);
    }
}
