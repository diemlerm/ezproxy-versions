/**
 * Original LDAP user authentication
 *
 * This module implements the original LDAP support that existed prior to
 * EZproxy 3.x.  In EZproxy 3.x, this module was superceded by a newer
 * version that uses OpenLDAP for user authentication and which appears
 * in usrldap2.c.
 *
 * This authentication method DOES NOT SUPPORT common conditions and actions.
 */

#define __UUFILE__ "usrldap.c"

#include "common.h"
#include "uustring.h"

#include "usr.h"
#include "usrldap.h"

/* Returns 0 if valid, 1 if not, 3 if unable to connect to ldap host */
int FindUserLdap(char *ldapHost,
                 char *user,
                 char *pass,
                 struct sockaddr_storage *pInterface,
                 char useSsl,
                 int sslIdx) {
    struct UUSOCKET rr, *r = &rr;
    int result;
    int state;
    int len;
    unsigned char line[256];
    char *comma, *semi;
    unsigned char *p, *us;
    unsigned char c;
    int remain;
    int inLen = 0;
    int rc;
    char *port;
    SSL_CTX *baseCtx = NULL;

    UUInitSocket(r, INVALID_SOCKET);

    result = 3;

    /* Don't allow null password since at least one server (Isocor GDS) uses this as an anonymous
       bind and it will succeed for any username. */
    if (ldapHost == NULL || user == NULL || *user == 0 || pass == NULL || *pass == 0 ||
        strlen(user) > 128 || strlen(pass) > 32)
        goto Finished;

    comma = NULL;
    for (semi = ldapHost; semi = strchr(semi, ';');) {
        if (comma == NULL) {
            *semi++ = 0;
            if (*semi)
                comma = semi;
        } else
            *semi = ',';
    }

    ParseHost(ldapHost, &port, NULL, TRUE);
    if (port) {
        *port++ = 0;
        if (*port == 0)
            port = NULL;
    }

    if (UUConnectWithSource3(r, ldapHost, (PORT)(port ? atoi(port) : (useSsl ? 636 : 389)), "LDAP",
                             pInterface, useSsl, sslIdx))
        goto Finished;

    p = line;

    *p++ = 0x30;
    *p++ = 0x81;
    p++; /* This is where length will eventually go */
    *p++ = 0x02;
    *p++ = 0x01;
    *p++ = 0x01;
    *p++ = 0x60;
    *p++ = 0x81;
    p++; /* Another length should be going here */
    *p++ = 0x02;
    *p++ = 0x01;
    *p++ = 0x02;
    *p++ = 0x04;
    *p++ = 0x81;
    p++; /* Username length will go here */
    us = p;
    if (comma == NULL) {
        strcpy((char *)p, user);
        p += strlen(user);
    } else {
        while (*comma) {
            if (*comma != '$') {
                *p++ = *comma++;
                continue;
            }
            comma++;
            if (*comma == '$') {
                *p++ = *comma++;
                continue;
            }
            if (*comma == 'u' || *comma == 'U') {
                strcpy((char *)p, user);
                p += strlen(user);
            }
            if (*comma == 's' || *comma == 'S') {
                *p++ = ';';
            }
            comma++;
        }
    }
    *(us - 1) = p - us;

    *p++ = 0x80;
    *p++ = 0x81;
    *p++ = strlen(pass);
    strcpy((char *)p, pass);
    p += strlen(pass);
    len = p - line;
    line[2] = len - 3;
    line[8] = len - 9;

    UUSend(r, (char *)line, len, 0);

    result = 1;
    for (state = 0, remain = 2; remain > 0; state++, remain--) {
        rc = UURecv(r, (char *)&c, sizeof(c), 0);
        if (rc <= 0)
            break;
        if (state == 99) {
            state--;
            continue;
        }
        if (state >= 100) {
            inLen = (inLen << 8) + c;
            if (remain == 1) {
                remain = inLen;
                state /= 100;
            }
            continue;
        }
        if (state == 0) {
            if (c != 0x30)
                break;
            continue;
        }
        if (state == 1) {
            if (c >= 0x80) {
                remain = c - 0x7f;
                state = 101;
                inLen = 0;
                continue;
            }
            remain = c;
            continue;
        }
        if (state == 2) {
            if (c != 0x02)
                break;
            continue;
        }
        if (state == 3) {
            if (c != 0x01)
                break;
            continue;
        }
        if (state == 4) {
            if (c != 0x01)
                break;
            continue;
        }
        if (state == 5) {
            if (c != 0x61)
                break;
            continue;
        }
        if (state == 6) {
            if (c >= 0x80) {
                remain = c - 0x7f;
                state = 601;
                inLen = 0;
                continue;
            }
            continue;
        }
        if (state == 7) {
            if (c != 0x0a)
                break;
            continue;
        }
        if (state == 8) {
            if (c != 0x01)
                break;
            continue;
        }
        if (state == 9) {
            if (c == 0)
                result = 0;
            continue;
        }
    }

    UUStopSocket(r, 0);

Finished:
    if (baseCtx) {
        SSL_CTX_free(baseCtx);
        baseCtx = NULL;
    }

    return result;
}
