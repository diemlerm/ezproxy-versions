#define __UUFILE__ "adminnetwork.c"

#include "common.h"
#include "uustring.h"

#include "adminnetwork.h"

BOOL AdminNetworkTest(struct TRANSLATE *t) {
    if (networkTestsRunning) {
        char *networkProbe = "/ezproxy-network-test";
        if (strcmp(t->method, "GET") == 0 && strcmp(t->url, networkProbe) == 0) {
            HTTPCode(t, 200, 0);
            NoCacheHeaders(&t->ssocket);
            HTTPContentType(t, "text/plain", 1);
            UUSendZ(&t->ssocket, "-ezproxy-connected-\n");
            return 1;
        }
    }

    return 0;
}

void AdminNetworkAddPorts(char *ranges, char *ports, char *kind) {
    char *pr;
    int i, start, stop;
    char *p;
    char type;

    pr = strchr(ranges, 0);

    for (i = 1, p = ports + i; i <= 65536;) {
        if (*p == 0) {
            i++, p++;
            continue;
        }
        start = i;
        type = *p;
        for (; i < 65536 && *p == type; i++, p++)
            ;
        stop = i - 1;
        if (kind) {
            strcat(pr, kind);
            kind = NULL;
        }
        pr = strchr(pr, 0);
        *pr++ = ';';
        sprintf(pr, "%c%d", type, start);
        pr = strchr(pr, 0);
        if (start != stop) {
            sprintf(pr, "-%d", stop);
            pr = strchr(pr, 0);
        }
    }
}

void AdminNetworkProxyChoiceDirect(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket;

    UUSendZ(s, "<p><div>How should " EZPROXYNAMEPROPER
               " connect to the OCLC server to perform this test?</div>\n");
    UUSendZ(&t->ssocket,
            "<div><label><input type='radio' name='proxy' value='direct' checked='checked'> "
            "Direct</label></div>\n");
}

void AdminNetworkInterface(struct TRANSLATE *t,
                           struct sockaddr_storage *ip,
                           struct sockaddr_storage *newIp,
                           BOOL *directNotShown) {
    struct UUSOCKET *s = &t->ssocket;
    char ipBuffer[INET6_ADDRSTRLEN];

    if (is_unspecified(newIp))
        return;

    if (!is_ip_equal(ip, newIp)) {
        if (*directNotShown) {
            *directNotShown = 0;
            UUSendZ(s, "<p><div>What source IP address should " EZPROXYNAMEPROPER
                       " use when performing this test?</div>\n");
            UUSendZ(s,
                    "<div><label><input type='radio' name='interface' value='any' "
                    "checked='checked'> Operating system default</label></div>\n");
        }
        memcpy(ip, newIp, sizeof(*ip));
        inet_ntop(ip->ss_family, get_in_addr2(ip), ipBuffer, sizeof ipBuffer);
        WriteLine(s,
                  "<div><label><input type='radio' name='interface' value='%s'> %s</label></div>",
                  ipBuffer, ipBuffer);
    }
}

void AdminNetworkProxyChoices(struct TRANSLATE *t, BOOL overview) {
    struct UUSOCKET *s = &t->ssocket;
    int i;
    struct DATABASE *b;
    char *proxyHost = NULL;
    PORT proxyPort = 0;
    struct LOGINPORT *lp;
    BOOL directNotShown = 1;
    struct sockaddr_storage ipInterface;
    char *ot =
        "\
<p>\n\
When you start this test, your EZproxy server will connect to the OCLC server.\n\
\n</p>\n\
<p>\n\
The OCLC server will verify your external DNS configuration and attempt to\n\
connect to each of the ports used by your EZproxy server to evaluate\n\
whether or not they can be reached by remote Internet users.\n\
</p>\n\
<p>\n\
Ports that cannot be reached through this test are typically blocked by a firewall, either on your EZproxy server or on your network.\n\
</p>\n";

    if (overview) {
        UUSendZ(s, ot);
    }

    UUSendZ(s, "<form action='/network' method='post'>\n");
    if (firstProxyHost && firstProxyPort) {
        AdminNetworkProxyChoiceDirect(t);
        directNotShown = 0;
        WriteLine(s,
                  "<div><label><input type='radio' name='proxy' value='first'> Proxy through "
                  "%s:%d</label></div>\n",
                  firstProxyHost, firstProxyPort);
        proxyPort = firstProxyPort;
        proxyHost = firstProxyHost;
    }

    for (i = 0, b = databases; i < cDatabases; i++, b++) {
        if (b->proxyHost == NULL || b->proxyPort == 0)
            continue;
        if (proxyHost == NULL || strcmp(proxyHost, b->proxyHost) != 0 ||
            proxyPort != b->proxyPort) {
            proxyHost = b->proxyHost;
            proxyPort = b->proxyPort;
            if (directNotShown) {
                AdminNetworkProxyChoiceDirect(t);
                directNotShown = 0;
            }
            WriteLine(s,
                      "<div><label><input type='radio' name='proxy' value='%d'> Proxy through "
                      "%s:%d</label></div>\n",
                      i, b->proxyHost, b->proxyPort);
        }
    }

    if (directNotShown)
        UUSendZ(s, "<input type='hidden' name='proxy' value='direct'>\n");
    else
        UUSendZ(s, "</p>\n");

    directNotShown = 1;
    memset(&ipInterface, 0, sizeof(ipInterface));
    ipInterface.ss_family = AF_INET;
    set_inaddr_any(&ipInterface);

    AdminNetworkInterface(t, &ipInterface, &defaultIpInterface, &directNotShown);

    for (i = 0, lp = loginPorts; i < cLoginPorts; i++, lp++) {
        AdminNetworkInterface(t, &ipInterface, &lp->ipInterface, &directNotShown);
    }

    for (i = 0, b = databases; i < cDatabases; i++, b++) {
        AdminNetworkInterface(t, &ipInterface, &b->ipInterface, &directNotShown);
    }

    /* Don't need to include a hidden field for interface not shown; automatically defaults to any
     * in this case */
    if (directNotShown == 0)
        UUSendZ(s, "</p>\n");

    UUSendZ(s, "<input type='submit' value='Start Test'>\n");
    UUSendZ(s, "</form>\n");
}

void AdminNetwork(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket, rsocket, *r = &rsocket;
    const int AFPROXY = 0;
    const int AFINTERFACE = 1;
    struct FORMFIELD ffs[] = {
        {"proxy",     NULL, 0, 0, 6               },
        {"interface", NULL, 0, 0, INET6_ADDRSTRLEN},
        {NULL,        NULL, 0, 0, 0               }
    };
    char *ports = NULL;
    int i;
    struct LOGINPORT *lp;
    struct HOST *h;
    char *ranges = NULL, *pr;
    int rc;
    struct sockaddr_storage sl, sm;
    socklen_t_M smLen;
    char ipBuffer[INET6_ADDRSTRLEN];
    BOOL header;
    char line[1024];
    BOOL gotStatus;
    char type;
    char *errorMsg;
    char *proxyHost = NULL;
    PORT proxyPort = 0;
    char *proxyAuth = NULL;
    struct DATABASE *b;
    char *proxyIdx;
    char *interfaceAddress;
    char sport[10];
    char *cu = CHECKURL;

    UUAcquireMutex(&mActive);
    networkTestsRunning++;
    UUReleaseMutex(&mActive);

    UUInitSocket(r, INVALID_SOCKET);

    FindFormFields(query, post, ffs);

    proxyIdx = ffs[AFPROXY].value;
    interfaceAddress = ffs[AFINTERFACE].value;

    AdminHeader(t, 0, "Test Network Connectivity");

    if (proxyIdx && *proxyIdx) {
        if (strcmp(proxyIdx, "direct") == 0) {
            proxyHost = NULL;
            proxyPort = 0;
            proxyAuth = NULL;
            goto Test;
        }
        if (strcmp(proxyIdx, "first") == 0) {
            proxyHost = firstProxyHost;
            proxyPort = firstProxyPort;
            proxyAuth = firstProxyAuth;
            goto Test;
        }
        i = atoi(proxyIdx);
        if (i >= 0 && i < cDatabases) {
            b = databases + i;
            proxyHost = b->proxyHost;
            proxyPort = b->proxyPort;
            proxyAuth = b->proxyAuth;
            goto Test;
        }
    }

    AdminNetworkProxyChoices(t, 1);
    goto Finished;

Test:
    if (UUGetHostByName(interfaceAddress, &sl, 0, 0) != 0) {
        // handle failure by resetting sockaddr_storage?
        init_storage(&sl, sl.ss_family, TRUE, 0);
    }

    /* The 65537 let's us cheat and go all the way to 65536, which is guaranteed to be unused */
    if (!(ports = calloc(1, 65537)))
        PANIC;

    if (!(ranges = malloc(7 * 65536)))
        PANIC;
    sprintf(ranges, "p%c;l", (optionProxyType == PTHOST ? 'h' : 'p'));
    pr = strchr(ranges, 0);

    for (i = 0, lp = loginPorts; i < cLoginPorts; i++, lp++) {
        switch (lp->useSsl) {
        case 0:
            type = 'H';
            break;
        case USESSLMINPROXY:
            type = 'M';
            break;
        case USESSLZ3950:
            type = 'Z';
            break;
        case 1:
            type = 'S';
            break;
        default:
            type = 0;
        }
        if (type) {
            *pr++ = ';';
            *pr++ = type;
            sprintf(pr, "%d", lp->port);
            pr = strchr(pr, 0);
        }
    }

    for (i = 1, h = hosts + i; i < cHosts; i++, h++) {
        if (h->myDb != NULL || h == mvHost || h == mvHostSsl)
            if (h->forceMyPort != 0 && h->myPort) {
                *(ports + h->myPort) = (h->useSsl ? 'S' : 'H');
                ;
            }
    }

    AdminNetworkAddPorts(ranges, ports, ";vf");

    memset(ports, 0, 65537);
    for (i = 1, h = hosts + i; i < cHosts; i++, h++) {
        if (h->myDb != NULL || h == mvHost || h == mvHostSsl)
            if (h->forceMyPort == 0 && h->myPort) {
                *(ports + h->myPort) = (h->useSsl ? 's' : 'h');
            }
    }

    AdminNetworkAddPorts(ranges, ports, ";vo");

    UUSendZ(s, "Connecting ");

    inet_ntop(sl.ss_family, get_in_addr((struct sockaddr *)&sl), ipBuffer, sizeof ipBuffer);
    WriteLine(s, "from source IP %s ", ipBuffer);

    if (proxyHost) {
        WriteLine(s, "through proxy %s:%d to %s<br>\n", proxyHost, proxyPort, CHECKHOST);
        UUFlush(s);
        rc = UUConnectWithSource4(r, proxyHost, proxyPort, "AdminNetwork-p", &sl, 0, 0, 2);
        if (rc) {
            errorMsg = ErrorMessage(socket_errno);
            UUSendZ(s, "Proxy server connect failed: ");
            SendHTMLEncoded(s, errorMsg);
            UUSendZ(s, "<br>\n");
            FreeThenNull(errorMsg);
            AdminNetworkProxyChoices(t, 0);
            goto Finished;
        }
    } else {
        cu += 8;
        cu = strchr(cu, '/');
        WriteLine(s, "directly to %s<br>\n", CHECKHOST);
        UUFlush(s);
        rc = UUConnectWithSource4(r, CHECKHOST, CHECKPORT, "AdminNetwork", &sl, 0, 0, 2);
        if (rc) {
            errorMsg = ErrorMessage(socket_errno);
            UUSendZ(s, "Direct connect failed: ");
            SendHTMLEncoded(s, errorMsg);
            UUSendZ(s, "<br>\n");
            FreeThenNull(errorMsg);
            AdminNetworkProxyChoices(t, 0);
            goto Finished;
        }
    }

    smLen = sizeof(sm);
    getsockname(r->o, (struct sockaddr *)&sm, &smLen);

    inet_ntop(sm.ss_family, get_in_addr((struct sockaddr *)&sm), ipBuffer, sizeof ipBuffer);

    WriteLine(r, "POST %s?%s:ranges:%s HTTP/1.0\r\n", cu, ipBuffer, myName);
    if (proxyAuth && *proxyAuth)
        WriteLine(r, "Proxy-Authorization: Basic %s\r\n", proxyAuth);
    if (CHECKPORT == 80)
        sport[0] = 0;
    else
        sprintf(sport, ":%d", CHECKPORT);
    WriteLine(r, "Host: %s%s\r\n", CHECKHOST, sport);
    WriteLine(r, "Cache-control: no-cache\r\nPragma: no-cache\r\nContent-Length: %d\r\n\r\n",
              strlen(ranges));
    UUSendZCRLF(r, ranges);

    header = 1;
    gotStatus = 0;
    while (ReadLine(r, line, sizeof(line))) {
        if (header) {
            if (line[0] == 0)
                header = 0;
            else {
                if (gotStatus == 0 && strncmp(line, "HTTP", 4) == 0) {
                    char *v = SkipWS(SkipNonWS(line));
                    gotStatus = 1;
                    if (atoi(v) != 200) {
                        if (proxyHost) {
                            UUSendZ(s, "Proxy server denied access<br>\n");
                            AdminNetworkProxyChoices(t, 0);
                            goto Finished;
                        }
                    }
                }
            }
            continue;
        }
        UUSendZCRLF(s, line);
        UUFlush(s);
    }

Finished:
    AdminFooter(t, 0);
    UUStopSocket(r, 0);
    FreeThenNull(ports);
    FreeThenNull(ranges);
    UUAcquireMutex(&mActive);
    networkTestsRunning--;
    UUReleaseMutex(&mActive);
}
