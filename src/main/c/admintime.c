/**
 * /time URL to display the server's current time in both local time
 * and UTC.
 */

#define __UUFILE__ "admintime.c"

#include "common.h"
#include "uustring.h"

#include "admintime.h"

void AdminTime(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    time_t now;
    struct tm tm;
    char localTime[32];
    char utcTime[32];

    UUtime(&now);
    UUlocaltime_r(&now, &tm);
    strftime(localTime, sizeof(localTime), "%Y-%m-%d %H:%M:%S", &tm);
    UUgmtime_r(&now, &tm);
    strftime(utcTime, sizeof(localTime), "%Y-%m-%d %H:%M:%S", &tm);

    AdminMinimalHeader(t, "Server Time");
    WriteLine(s,
              "<table class='th-row-nobold'><tr><th scope='row'>Local time: "
              "</th><td>%s</td></tr><tr><th scope='row'>UTC time: </th><td>%s</td></tr></table>\n",
              localTime, utcTime);
    AdminMinimalFooter(t);
}
