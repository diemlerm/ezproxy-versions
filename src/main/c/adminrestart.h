#ifndef __ADMINRESTART_H__
#define __ADMINRESTART_H__

#include "common.h"

void AdminRestart(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINRESTART_H__
