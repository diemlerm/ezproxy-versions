#ifndef __USRFINDUSER_H__
#define __USRFINDUSER_H__

int FindUser(struct TRANSLATE *t,
             struct USERINFO *uip,
             char *fileName,
             char *pseudoFileContent,
             struct FILEINCLUSION *fileIncludeStack,
             int *userLimit,
             enum INTRUDERSTATUS isIntruder);
int FindUserIP(struct TRANSLATE *t, char *range);

#endif  // __USRFINDUSER_H__
