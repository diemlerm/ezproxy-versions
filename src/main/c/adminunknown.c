/**
 * /unknown URL support to handle scenario where a user tries to access
 * a URL that EZproxy is not configured to handle.
 */

#define __UUFILE__ "adminunknown.c"

#include "common.h"
#include "uustring.h"

#include "adminunknown.h"

void AdminUnknown(struct TRANSLATE *t, char *url) {
    char *host, *colon, *endOfHost;
    char *protocolPrefix = "";
    PORT port, defaultPort = 0;
    size_t len;
    struct UUSOCKET *s = &t->ssocket;
    char *status = "unknown";

    struct REDIRECTSAFEORNEVERPROXYURLDATABASE *RedirectSafeOrNeverProxyURLDatabase;
    if (!(RedirectSafeOrNeverProxyURLDatabase =
              calloc(1, sizeof(struct REDIRECTSAFEORNEVERPROXYURLDATABASE))))
        PANIC;
    RedirectSafeOrNeverProxyURLDatabase->database = NULL;
    if (optionRedirectUnknown ||
        t->redirectSafeOrNeverProxyCache == redirectSafeOrNeverProxyCacheTrue ||
        (t->redirectSafeOrNeverProxyCache == redirectSafeOrNeverProxyCacheUninitialized &&
         (RedirectSafeOrNeverProxyURLDatabase =
              RedirectSafeOrNeverProxyURL(url, 1, RedirectSafeOrNeverProxyURLDatabase))
             ->result)) {
        LocalRedirect(t, url, 0, 0, NULL);
        if (optionRedirectUnknown == 0)
            status = "redirectsafe";
        goto Finished;
    }

    HTTPCode(t, 200, 0);
    HTTPContentType(t, NULL, 1);

    if (SendEditedFile(t, NULL, NEEDHOSTHTM, 0, url, 1, NULL) == 0)
        goto Finished;

    if (strnicmp(url, "http://", 7) == 0) {
        defaultPort = 80;
    } else if (strnicmp(url, "https://", 8) == 0) {
        protocolPrefix = "https://";
        defaultPort = 443;
    } else if (strnicmp(url, "ftp://", 6) == 0) {
        protocolPrefix = "ftp://";
        defaultPort = 21;
    }

    host = SkipFTPHTTPslashesAt(url, NULL, NULL);
    ParseHost(host, &colon, &endOfHost, 0);

    if (colon) {
        port = atoi(colon + 1);
        len = colon - host;
    } else {
        port = defaultPort;
        len = endOfHost - host;
    }

    UUSendZ(s, TAG_DOCTYPE_HTML_HEAD);
    UUSendZ(s, "<title>Host Needed</title></head>\n");
    UUSendZ(s, "<body>\n");
    UUSendZ(s,
            "<p>Oops! It looks like you have attempted to view a page that has not been configured "
            "for access.</p>\n");
    UUSendZ(s, "<p><strong>If you are a library patron...</strong></p>\n");
    UUSendZ(s,
            "<p>Please contact your library and provide the name of the resource you were trying "
            "to access and the Host line below so the library can work with you to correct this "
            "error.</p>\n");

    WriteLine(s, "<blockquote>Host %s", protocolPrefix);

    {
        char hostCopy[len + 1];
        memcpy(hostCopy, host, len);
        hostCopy[len] = 0;
        SendHTMLEncoded(s, hostCopy);
    }
    if (port != defaultPort)
        WriteLine(s, ":%d", port);
    UUSendZ(s, "</blockquote>\n");

    UUSendZ(s, "<p><strong>If you are an EZproxy administrator...</strong></p>");
    UUSendZ(s, "<p>To allow ");
    SendHTMLEncoded(s, url);
    UUSendZ(s,
            " to be used in a <a "
            "href='http://www.oclc.org/support/services/ezproxy/documentation/concept/"
            "starting_point_url.en.html'>starting point URL</a>, you need to either:</p>\n");
    UUSendZ(s, "<ol>\n");
    UUSendZ(s,
            "<li>Add the Host line above to the existing database stanza for this resource</li>");
    UUSendZ(s,
            "<li>Create a new database stanza for this resource and include the Host line above "
            "within that stanza</li>");
    UUSendZ(s, "</ol>\n");

    UUSendZ(s,
            "<p>Please do not add this Host line by itself to the config.txt file because this "
            "could cause problems in your configuration or when troubleshooting access to "
            "resources in the future.</p>\n");
    UUSendZ(s,
            "<p>For more information see <a "
            "href='http://www.oclc.org/support/services/ezproxy/documentation/cfg/"
            "spu-and-cfg.en.html'>Starting Point URLs &amp; config.txt</a>.</p>\n");
    UUSendZ(s,
            "<p>For details about how to update this page, see <a "
            "href='https://www.oclc.org/support/services/ezproxy/documentation/"
            "docs.en.html'>Default Web Pages</a>.</p>\n");
    UUSendZ(s, "</body>\n");
    UUSendZ(s, "</html>\n");

Finished:
    t->finalStatus = 599;
    LogSPUs(t, url, status, RedirectSafeOrNeverProxyURLDatabase->database);
    FreeThenNull(RedirectSafeOrNeverProxyURLDatabase);
}
