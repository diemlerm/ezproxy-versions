#define __UUFILE__ "check.c"

#include "common.h"
#include "url_parser.h"

/**
 * This is called by: ./exproxy.exe -c <[(ipv4|\[ipv6\]:]port>
 */
void CheckConnection(char *checkPort) {
    int l;
    socklen_t_M len;
    struct sockaddr_storage sl, sm, sll;
    BOOL tryProxy;
    char line[256];
    int rc;
    int one = 1;
    BOOL header;
    char *origName;
    struct UUSOCKET us;
    BOOL interfaceSpecified = 0;
    BOOL gotStatus = 0;
    char ipBuffer[INET6_ADDRSTRLEN];
    int family = UUDEFAULT_FAMILY;
    char *host = NULL;
    PORT port = 2048;

    if (!(checkPort = strdup(checkPort)))
        PANIC;
    if (!(origName = strdup(myName)))
        PANIC;

    ReadConfig(1);

    // ipv6 interface and potentially a port
    // or host/ipv4 interface and a port
    if (checkPort[0] == '[' || strchr(checkPort, ':')) {
        interfaceSpecified = 1;
        struct parsed_url *purl = parse_interface(checkPort);
        if (purl == NULL) {
            Log("Invalid syntax for interface %s", checkPort);
            parsed_url_free(purl);
            exit(1);
        }

        host = strdup(purl->host);

        // setup the port
        if (purl->port)
            port = atoi(purl->port);
        else if (primaryLoginPort)
            port = primaryLoginPort;

        parsed_url_free(purl);

        // invalid representation or just a port
    } else {
        port = atoi(checkPort);
        if (port == 0) {
            port = primaryLoginPort;
        }
    }

    init_storage(&sl, AF_UNSPEC, TRUE, 0);

    // we have an interface or host to check
    if (host != NULL && strlen(host)) {
        if (UUGetHostByName(host, &sl, 0, AF_UNSPEC) != 0) {
            Log("Unable to lookup IP address for %s", checkPort);
            exit(1);
        }

        family = sl.ss_family;
    }

    set_port(&sl, port);

    printf("\n");

    // create a socket for of given family
    l = socket(family, SOCK_STREAM, 0);
    if (l == INVALID_SOCKET) {
        Log("Unable to create local listener socket???");
        return;
    }

    rc = setsockopt(l, SOL_SOCKET, SO_REUSEADDR, (char *)&one, sizeof(one));
    if (rc != 0) {
        closesocket(l);
        Log("Unable to configure local socket for reuse???");
        return;
    }

    // try to bind using socket and setup sl
    rc = bind(l, (struct sockaddr *)&sl, get_size(&sl));
    if (rc == SOCKET_ERROR) {
        closesocket(l);
        if (interfaceSpecified) {
            Log("Unable to bind to %s:%d", ipToStr(&sl, ipBuffer, sizeof ipBuffer), get_port(&sl));
        } else
            Log("Unable to bind to port %d", get_port(&sl));

        return;
    }

    // try and listen with socket
    rc = listen(l, 1);
    if (rc != 0) {
        closesocket(l);
        Log("Unable to listen on port %d", port);
        return;
    }

    tryProxy = firstProxyPort != 0;

    memcpy(&sll, &sl, sizeof(sll));
    set_port(&sll, 0);

Retry:
    if (tryProxy) {
        Log("Check ability to connect through outgoing proxy %s", firstProxyHost);
        if (UUConnectWithSource3(&us, firstProxyHost, firstProxyPort, "check", &sll, 0, 0)) {
            /* Log("Unable to locate %s", firstProxyHost); */
            tryProxy = 0;
            goto Retry;
        }
    } else {
        Log("Checking ability to directly connect to " CHECKHOST);
        Log("Attempting to contact " CHECKHOST " by name");
        if (UUConnectWithSource3(&us, CHECKHOST, CHECKPORT, "check", &sll, 0, 0)) {
            Log("Unable to connect to " CHECKHOST " by name");
            Log("Unable to make connection, aborting");
            return;
        }
    }

    len = sizeof(sm);
    if (interfaceSpecified)
        memcpy(&sm, &sl, len);
    else
        getsockname(us.o, (struct sockaddr *)&sm, &len);

    /* UUInitSocket(&us, s); */
    Log("Connection established, sending my IP address (%s) for testing",
        ipToStr(&sm, ipBuffer, sizeof ipBuffer));
    Log("Waiting for response on port %d", get_port(&sl));
    WriteLine(&us, "GET " CHECKURL "?%s:%d:%s HTTP/1.0\r\n",
              ipToStr(&sm, ipBuffer, sizeof ipBuffer), get_port(&sl), myName);
    if (tryProxy && firstProxyAuth && *firstProxyAuth)
        WriteLine(&us, "Proxy-Authorization: Basic %s\r\n", firstProxyAuth);
    UUSendZ(&us, "Cache-control: no-cache\r\nPragma: no-cache\r\n\r\n");
    header = 1;
    Log("The test results follow:");
    gotStatus = 0;
    while (ReadLine(&us, line, sizeof(line))) {
        if (header) {
            if (line[0] == 0)
                header = 0;
            else {
                if (gotStatus == 0 && strncmp(line, "HTTP", 4) == 0) {
                    char *v = SkipWS(SkipNonWS(line));
                    gotStatus = 1;
                    if (atoi(v) != 200) {
                        if (tryProxy) {
                            Log("Proxy server denied access, will retry bypassing proxy");
                            tryProxy = 0;
                            goto Retry;
                        }
                    }
                }
            }
            continue;
        }
        Log("%s", line);
    }
    closesocket(us.o);
    closesocket(l);
}
