/**
 * HTTP basic user authentication
 *
 * This module implement user authentication by testing the username and
 * password against a specified URL using the HTTP bsaic user authentication
 * method.  If the web server approves access to the URL, user access
 * is granted.
 *
 * This authentication method DOES NOT SUPPORT common conditions and actions.
 */

#define __UUFILE__ "usrhttpbasic.c"

#include "common.h"
#include "uustring.h"

#include "usr.h"
#include "usrhttpbasic.h"

/* Returns 0 if valid, 1 if not, 3 if unable to connect to pop host */
int FindUserHttpBasic(char *url, struct USERINFO *uip, struct sockaddr_storage *pInterface) {
    int result = 1;
    struct UUSOCKET rr, *r = &rr;
    char *tempUnencoded = NULL, *tempEncoded = NULL;
    char line[256];
    BOOL first;
    int status;
    char *pass;
    char save;
    struct PROTOCOLDETAIL *protocolDetail;
    char *host;
    char *colon;
    char *endOfHost, *endOfHostPort;
    PORT port;
    char *verb;
    char *p;

    UUInitSocket(r, INVALID_SOCKET);

    result = resultInvalid;

    if (uip->user == NULL || *uip->user == 0)
        goto Finished;

    pass = uip->pass;
    if (pass == NULL)
        pass = "";

    if (!(tempUnencoded = malloc(strlen(uip->user) + strlen(uip->pass) + 2)))
        PANIC;
    sprintf(tempUnencoded, "%s:%s", uip->user, pass);
    tempEncoded = Encode64(NULL, tempUnencoded);
    FreeThenNull(tempUnencoded);

    if (p = StartsIWith(url, "GET;")) {
        verb = "GET";
        url = p;
    } else {
        verb = "HEAD";
    }

    if (!(ParseProtocolHostPort(url, "http://", &protocolDetail, &host, &endOfHost, &colon, &port,
                                NULL, &endOfHostPort)))
        goto Finished;

    if (protocolDetail->protocol > PROTOCOLHTTPS)
        goto Finished;

    result = resultRefused;

    if (save = *endOfHost)
        *endOfHost = 0;

    if (UUConnectWithSource2(r, host, port, "FindUserHttpBasic", pInterface,
                             (char)protocolDetail->protocol))
        goto Finished;

    if (save)
        *endOfHost = save;

    result = resultInvalid;

    WriteLine(r, "%s %s%s HTTP/1.0\r\n", verb, (*endOfHostPort != '/' ? "/" : ""), endOfHostPort);
    *endOfHostPort = 0;
    WriteLine(r, "Host: %s\r\nAuthorization: basic %s\r\nConnection: close\r\n\r\n", host,
              tempEncoded);

    first = 1;
    while (ReadLine(r, line, sizeof(line))) {
        if (uip->debug)
            Log("HTTPBasic response %s", line);
        if (first) {
            if (line[0] == 0)
                continue;
            first = 0;
            if (strnicmp(line, "HTTP/", 5) == 0) {
                status = atoi(SkipNonWS(SkipWS(line)));
                if (status >= 200 && status < 400) {
                    result = resultValid;
                }
                if (status == 404)
                    Log("HTTPBasic test page returned 404 page not found; URL likely incorrect");
            }
        }
    }

    UUStopSocket(r, 0);

Finished:
    FreeThenNull(tempUnencoded);
    FreeThenNull(tempEncoded);

    return result;
}
