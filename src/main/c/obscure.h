#ifndef __OBSCURE_H__
#define __OBSCURE_H__

char *ObscureString(char *input);
char *ObscureString2(char *input);
char *ObscureString3(char *input);
unsigned char *UnobscureBinary(char *input, size_t *outLen);
char *UnobscureString(char *input);
void ObscureFile(char *filename);
unsigned char *UnobscureFile(char *filename, size_t *outLen);
#endif  // __OBSCURE_H__
