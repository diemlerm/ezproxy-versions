#ifndef __IDENTIFIER_H__
#define __IDENTIFIER_H__

#include "common.h"
#include "sqlite3.h"
#include "security.h"

#define IDENTIFIERDB SECURITYDIR DIRSEP "identifier_v1.db"
#define IDENTIFIERHEADER "X-EZproxy-Identifier"
#define IDENTIFIERHISTORICHEADER "X-Transient-SubjectID"
#define IDENTIFIEROTHERSCOPE "other"

struct IDENTIFIERSCOPE {
    const char *name;
    char *header;
    char *domains;
    int id;
    int enabled;
    BOOL hidden;
};

extern int retentionDays;

void IdentifierStart();
struct IDENTIFIERSCOPE *IdentifierScope(const char *name, BOOL create);
struct UUAVLTREE IdentifierGetScopeTree();
sqlite3 *IdentifierGetSharedDatabase();
struct IDENTIFIERSCOPE *IdentifierScopeByHostname(const char *hostname);
char *IdentifierTextByScopeId(const char *user, int scopeid, time_t when);
void IdentifierConfig(char *arg, char *currentFilename, int currentLineNum);
struct IDENTIFIERSCOPE *IdentifierScopeByHost(struct HOST *h);
void IdentifierVacuum();
void IdentifierPurge();

#endif
