#define __UUFILE__ "cookie.c"

#include "common.h"

#ifdef WIN32
/* io.h, fcntl.h, share.h and sys/stat.h needed for _sopen in Win32 */
#include <io.h>
#include <fcntl.h>
#include <share.h>
#include <sys/locking.h>
#include <direct.h>
#else
#include <sys/uio.h>
#include <dirent.h>
#endif

#include <sys/stat.h>

#include "url_parser.h"

const char *OptionCookieNameSelect(int idx) {
    /* these values must be synchronized with enum OPTIONCOOKIE */
    static const char *optionCookieNames[] = {"Cookie", "NoCookie", "DomainCookieOnly",
                                              "CookiePassThrough"};

    if (idx >= 0 && idx < OCMAX)
        return optionCookieNames[idx];

    return "Invalid Cookie Option";
}

/* Compares a fully-qualified host name (fqhn) with a domain (either another fqhn or a
 * fully-qualified domain name */
/* that starts with a period) to see if the host is within the domain, return 1 if within domain, 0
 * if not */
BOOL HostInDomain2(const char *host, const PORT port, char *domain) {
    return 0;
}

BOOL HostInDomain(char *host, PORT port, char *domain) {
    char *dot;
    size_t hostLen, domainLen;
    char *colon;
    char justDomain[512];

    StrCpy3(justDomain, domain, sizeof(justDomain));
    domain = justDomain;

    colon = strchr(domain, ':');
    if (colon) {
        if (atoi(colon + 1) != port)
            return 0;
        StrCpy3(justDomain, domain, sizeof(justDomain));
        /* perhaps a really long domain means colon is out of reach, so don't assume we'll find it
           in new copy
        */
        if (colon = strchr(justDomain, ':'))
            *colon = 0;
    }

    domainLen = strlen(domain);
    hostLen = strlen(host);

    if (hostLen < domainLen) {
        /* This says that host X will match domain .X, which really isn't true to the RFC,
           but it is how IE behaves, so alas, we have to do it that way to make sure
           the behavior is consistent with that browser. Argh.
        */
        if (hostLen + 1 == domainLen && *domain == '.')
            return stricmp(host, domain + 1) == 0;
        return 0;
    }

    if (*domain != '.')
        return stricmp(host, domain) == 0;

    /* By RFC, numeric hosts can only match equivalent numeric hosts */
    if (NumericHost(host))
        return 0;

    dot = host + hostLen - domainLen;
    if (*dot != '.')
        return 0;

    /* Don't allow just .com */
    if (strchr(dot + 1, '.') == NULL)
        return 0;

    return stricmp(dot, domain) == 0;
}

struct COOKIE *FindCookie(struct HOST *h, struct COOKIE *k) {
    for (; k; k = k->next)
        if (HostInDomain(h->hostname, h->remport, k->domain))
            return k;
    return NULL;
}

void ClusterAddCookie(struct SESSION *e, char *name, char *domain, char *path, time_t expires) {
    struct COOKIE *k, *n, *l;
    char *equals;
    size_t len;
    char *save;

    equals = strchr(name, '=');
    if (equals == NULL)
        len = strlen(name);
    else
        len = equals - name;

    UUAcquireMutex(&mCookies);
    l = NULL;
    for (k = e->cookies; k; k = k->next) {
        l = k;
        if (stricmp(k->domain, domain) != 0)
            continue;
        if (strncmp(k->name, name, len) != 0)
            continue;
        if (strcmp(k->path, path) != 0)
            continue;
        /* If the cookie to set is just a label, then we match to only the label */
        if (equals == NULL && strlen(k->name) != len)
            continue;
        /* We're just updating the cookie */
        k->expires = expires;
        save = k->name;
        if (!(k->name = strdup(name)))
            PANIC;
        FreeThenNull(save);
        UUtime(&e->cookiesChanged);
        goto Done;
    }
    /* If this cookie doesn't exist, and expires is 0, this is a delete, so we don't do anything */
    if (expires == 0)
        goto Done;
    if (!(n = calloc(1, sizeof(struct COOKIE))))
        PANIC;
    if (!(n->name = strdup(name)))
        PANIC;
    if (!(n->domain = strdup(domain)))
        PANIC;
    if (!(n->path = strdup(path)))
        PANIC;
    n->expires = expires;

    n->next = NULL;
    if (l == NULL)
        e->cookies = n;
    else
        l->next = n;
    UUtime(&e->cookiesChanged);

Done:
    UUReleaseMutex(&mCookies);
    ;
}

void AddCookie(struct TRANSLATE *t, char *name, char *domain, char *path, time_t expires) {
    struct PEERMSG msg;
    /*
    char *slash;
    char save;
    */

    if (name == NULL)
        return;

    /*
    slash = NULL;
    */
    if (path == NULL)
        path = "/";
    /*
    {
        path = t->url;
        if (strnicmp(path, "http://", 7) == 0)
            path += 6;
        if (slash = strrchr(path, '/')) {
            slash++;
            save = *slash;
            *slash = 0;
        }
    }
    */

    if (domain == NULL)
        domain = "";

    if (!HostInDomain((t->host)->hostname, (t->host)->remport, domain))
        return;

    if (cPeers) {
        ClusterMsgCookie(t->session, name, domain, path, expires, &msg);
        /*
        ClusterInitMsg(&msg, 'A', 'C', 0);
        remain = sizeof(msg.data);
        EncodeFields(msg.data, &remain,
            EDACHAR, (t->session)->key,
            EDACHAR, name,
            EDACHAR, domain,
            EDACHAR, path,
            EDINT, expires,
            EDSTOP);
        msg.datalen = sizeof(msg.data) - remain;
        */
        ClusterRequest(&msg);
    } else
        ClusterAddCookie(t->session, name, domain, path, expires);
    /*
    if (slash)
        *slash = save;
    */
}

void MergeSessionCookies(struct TRANSLATE *t, char *srcSession) {
    struct SESSION *se;
    struct COOKIE *k;
    char *comma;

    if (srcSession == NULL)
        return;

    while (comma = strchr(srcSession, ','))
        srcSession = comma + 1;

    if ((se = FindSessionByKey(srcSession)) == NULL)
        return;

    for (k = se->cookies; k; k = k->next) {
        AddCookie(t, k->name, k->domain, k->path, k->expires);
    }
}

int CookiePart(char *s) {
    s = SkipWS(s);

    if (strnicmp(s, "path=", 5) == 0 || strnicmp(s, "domain=", 7) == 0 ||
        strnicmp(s, "expires=", 8) == 0)
        return 1;
    if (strnicmp(s, loginCookieName, loginCookieNameLen) == 0 && *(s + loginCookieNameLen) == '=')
        return 2;
    return 0;
}

void SendDomainCookies(struct TRANSLATE *t, char *receivedCookies) {
    struct SESSION *e = t->session;
    struct COOKIE *k;
    char *equal;
    BOOL first = 1;
    struct UUSOCKET *w = t->wsocketPtr;
    char *token;

    if (receivedCookies && strnicmp(receivedCookies, "Cookie:", 7) == 0) {
        receivedCookies = SkipWS(receivedCookies + 7);
    }

    if (t->museCookie) {
        char *semi, *lastSemi = NULL, *nextToken;
        BOOL strippingEZproxyCookie = 1;
        int cp;

        Trim(t->museCookie, TRIM_LEAD | TRIM_TRAIL);

        for (token = t->museCookie, semi = NULL; token && *token; token = nextToken) {
            lastSemi = semi;
            semi = strchr(token, ';');
            if (semi) {
                nextToken = SkipWS(semi + 1);
                *semi = 0;
                /* If this is a final, trailing semi-colon, treat it like it isn't there */
                if (*nextToken == 0) {
                    semi = NULL;
                }
            }

            if (semi == NULL) {
                semi = strchr(token, 0);
                /* add extra null to make double-null expected by SendMuseCookies */
                *(semi + 1) = 0;
                semi = NULL;
                nextToken = NULL;
            }

            cp = CookiePart(token);
            if (cp) {
                if (cp == 2) {
                    strippingEZproxyCookie = 1;
                }
                if (strippingEZproxyCookie) {
                    /* Copy the domain cookies from the global session over into our session
                       to fix issues with Muse proxy and Factiva
                    */
                    char *srcSession = token + loginCookieNameLen;
                    if (*srcSession == '=')
                        MergeSessionCookies(t, srcSession + 1);
                    if (nextToken) {
                        StrCpyOverlap(token, nextToken);
                        semi = lastSemi;
                        nextToken = token;
                        continue;
                    }
                    /* If anything came before, just add the double-null there */
                    if (lastSemi)
                        *(lastSemi + 1) = 0;
                    else {
                        /* but if this is the lone token and it is being stripped, set string to
                         * null so it is disposed */
                        *t->museCookie = 0;
                    }
                    break;
                }
                /* For a cookie part, restore the previous semi-colon */
                if (lastSemi)
                    *lastSemi = ';';
            } else {
                if (strippingEZproxyCookie)
                    strippingEZproxyCookie = 0;
                if (first) {
                    UUSendZ(w, "Cookie: ");
                    first = 0;
                } else {
                    UUSendZ(w, "; ");
                }
                UUSendZ(w, token);
            }
        }
        if (*t->museCookie == 0) {
            FreeThenNull(t->museCookie);
        }
    }

    if (receivedCookies && *receivedCookies) {
        if (first) {
            UUSendZ(w, "Cookie: ");
            first = 0;
        } else {
            UUSendZ(w, "; ");
        }
        UUSendZ(w, receivedCookies);
        first = 0;
    }

    time_t now;
    UUtime(&now);

    /* MORE really should verify secure more closely */
    for (k = FindCookie(t->host, e->cookies); k; k = FindCookie(t->host, k->next)) {
        /* When expires is 0, the cookie is deleted and should be ignored */
        if (k->expires < now)
            continue;
        /*      printf("Send cookie %s\n", c->name); */
        if ((equal = strchr(k->name, '=')) != NULL && *(equal + 1) == 0) {
            /*          printf("Skip blank cookie\n"); */
            continue;
        }
        if (k->path) {
            char c;
            size_t len;
            for (len = k->path ? strlen(k->path) : 0; len > 0 && k->path[len - 1] == '/'; len--)
                ;
            if (len > 0) {
                if (strnicmp(t->urlCopy, k->path, len) != 0)
                    continue;
                c = t->urlCopy[len];
                if (c != 0 && c != '/' && c != '\\' && c != '?' && c != ';')
                    continue;
            }
        }

        if (first) {
            UUSendZ(w, "Cookie: ");
            first = 0;
        } else {
            UUSendZ(w, "; ");
        }
        UUSendZ(w, k->name);
    }

    if (first == 0)
        UUSendCRLF(w);
}

BOOL SetDomainCookie(struct TRANSLATE *t, char *field) {
    char *semi;
    char *arg;
    int len;
    time_t now, expires;
    enum OPTIONCOOKIE optionCookie;
    char localDomain[512];
#define SDCDOMAIN 0
#define SDCPATH 1
#define SDCEXPIRES 2
    struct {
        char *name;
        char *ptr;
    } *fps, fields[] = {"domain", NULL, "path", NULL, "expires", NULL, NULL, NULL};

    /* In single session mode, also suppress all cookie setting */
    if ((t->session)->anonymous)
        return 1;

    localDomain[0] = 0;

    if (t->host == NULL || (t->host)->myDb == NULL)
        optionCookie = OCALL;
    else
        optionCookie = ((t->host)->myDb)->optionCookie;

    /*  printf("Hey! Set-Cookie: %s\n", field);  */
    for (semi = field; semi = strchr(semi, ';');) {
        semi = SkipWS(semi + 1);
        for (fps = fields; fps->name; fps++) {
            len = strlen(fps->name);
            if (strnicmp(semi, fps->name, len) != 0)
                continue;
            arg = semi + len;
            if (*arg != '=')
                continue;
            fps->ptr = SkipWS(arg + 1);
            /* RFC 2965 introduces the possibility that domain won't have a leading period, and
               should have one automagically appended It's OK to change things at this point since a
               domain was found and this routine will definitely deal with the consequences
            */
            if (strcmp(fps->name, "domain") == 0 && *(fps->ptr) != '.') {
                fps->ptr = fps->ptr - 1;
                *(fps->ptr) = '.';
            }
        }
    }

    if (fields[SDCDOMAIN].ptr == NULL) {
        if (optionCookie != OCDOMAIN)
            return 0;
        StrCpy3(localDomain, (t->host)->hostname, sizeof(localDomain) - 10);
        /* I thought the port should be there, but alas, IE will send anyhting to any port */
        /* sprintf(strchr(localDomain, 0), ":%d", (t->host)->remport); */
        fields[SDCDOMAIN].ptr = localDomain;
    }

    /*
        domain = fields[SDCDOMAIN].ptr;
        semi = strchr(domain, ';');
        if (semi == NULL)
            semi = strchr(domain, 0);
        ReplaceString(domain, semi - domain, myDomain);
        return 0;
    */

    if (semi = strchr(field, ';')) {
        *semi = 0;
        Trim(semi, TRIM_LEAD | TRIM_TRAIL);
    }

    for (fps = fields; fps->name; fps++) {
        if (fps->ptr == NULL)
            continue;
        if (semi = strchr(fps->ptr, ';'))
            *semi = 0;
        Trim(fps->ptr, TRIM_LEAD | TRIM_TRAIL);
    }

    if ((optionCookie == OCPASSTHROUGH) || (stricmp(fields[SDCDOMAIN].ptr, ".grolier.com") == 0)) {
        UUSendZ(&t->ssocket, "Set-Cookie: ");
        UUSendZ(&t->ssocket, field);
        if (fields[SDCPATH].ptr) {
            UUSendZ(&t->ssocket, "; path=");
            UUSendZ(&t->ssocket, fields[SDCPATH].ptr);
        }
        if (fields[SDCEXPIRES].ptr) {
            UUSendZ(&t->ssocket, "; expires=");
            UUSendZ(&t->ssocket, fields[SDCEXPIRES].ptr);
        }

        WriteLine(&t->ssocket, "; domain=%s\r\n", myDomain);

        return 1;
    }

    /*  printf("Hey, that's a domain cookie so let's store it\n"); */
    /* Check to see if it's in the correct domain; if not, reject */
    if (!HostInDomain((t->host)->hostname, (t->host)->remport, fields[SDCDOMAIN].ptr)) {
        /* printf("Wrong domain, reject cookie\n"); */
        return 1;
    }
    UUtime(&now);
    expires = ParseHttpDate(fields[SDCEXPIRES].ptr, now + TWO_DAYS_IN_SECONDS);
    if (expires < now)
        expires = 0;
    AddCookie(t, field, fields[SDCDOMAIN].ptr, fields[SDCPATH].ptr, expires);
    return 1;
}

struct COOKIE *ClusterTossCookie(struct COOKIE *k) {
    struct COOKIE *n;

    n = k->next;
    FreeThenNull(k->name);
    FreeThenNull(k->domain);
    FreeThenNull(k->path);
    FreeThenNull(k);
    return n;
}

struct COOKIE *TossCookie(struct COOKIE *k) {
    return ClusterTossCookie(k);
}

void TossCookies(struct SESSION *s) {
    struct COOKIE *k;

    UUAcquireMutex(&mCookies);
    for (k = s->cookies; k; k = TossCookie(k))
        ;
    s->cookies = NULL;
    UUReleaseMutex(&mCookies);
}

void SaveSessionCookies(struct SESSION *e) {
    struct COOKIE *k;
    int f;
    /* filename will be cookies followed by directory separator followed by session ID */
    char filename[MAXKEY + 32];
    char aexpires[16];
    int tries = 0;

    e->cookiesChanged = 0;
    UUtime(&e->cookiesSaved);
    sprintf(filename, "%s%s%s", COOKIESDIR, DIRSEP, e->key);

    UUAcquireMutex(&mCookies);

    for (tries = 0;; tries++) {
#ifdef WIN32
        f = _sopen(filename, _O_CREAT | _O_WRONLY | _O_TRUNC, _SH_DENYWR, _S_IWRITE | _S_IREAD);
#else
        f = UUopenCreateFD(filename, O_CREAT | O_WRONLY | O_TRUNC, defaultFileMode);
#endif

        if (f >= 0)
            break;

        if (errno == ENOENT && tries == 0) {
#ifdef WIN32
            UUmkdir(COOKIESDIR);
#else
            struct stat sb;

            /* Early versions created the directory 0600 instead of 0700, so compensate */
            if (stat(COOKIESDIR, &sb) == 0) {
                if ((sb.st_mode & 0700) == 0600)
                    chmod(COOKIESDIR, sb.st_mode | 0700);
            } else
                UUmkdir(COOKIESDIR);
#endif
            continue;
        }
        UUReleaseMutex(&mCookies);
        return;
    }

    for (k = e->cookies; k; k = k->next) {
        if (k->expires == 0)
            continue;
        if (k->domain)
            write(f, k->domain, strlen(k->domain));
        write(f, "\t", 1);
        if (k->path)
            write(f, k->path, strlen(k->path));
        write(f, "\t", 1);
        sprintf(aexpires, "%" PRIdMAX "", (intmax_t)(k->expires));
        write(f, aexpires, strlen(aexpires));
        write(f, "\t", 1);
        if (k->name)
            write(f, k->name, strlen(k->name));
        if (k->val) {
            write(f, "=", 1);
            write(f, k->val, strlen(k->val));
        }
        write(f, "\n", 1);
    }

    write(f, "---\n", 4);

    VariablesSave(e->sessionVariables, f);

    close(f);

    UUReleaseMutex(&mCookies);
}

void SaveCookies(BOOL mandatory) {
    struct SESSION *e;
    int i;

    for (i = 0, e = sessions; i < cSessions; i++, e++) {
        if (e->active != 1)
            continue;
        if (e->cookiesChanged == 0 && mandatory == 0)
            continue;
        SaveSessionCookies(e);
    }
}

void RestoreCookiesSession(char *key) {
    char filename[MAXKEY + 32];
    char line[4400];
    struct SESSION *e;
    int f;
    char *p;
    char *tab;
    char *domain;
    char *path;
    time_t expires;
    char *name;
    struct FILEREADLINEBUFFER frb;

    sprintf(filename, "%s%s%s", COOKIESDIR, DIRSEP, key);
    e = FindSessionByKey(key);
    if (e == NULL) {
        unlink(filename);
        return;
    }
    f = SOPEN(filename);
    InitializeFileReadLineBuffer(&frb);
    while (FileReadLine(f, line, sizeof(line), &frb)) {
        if (strcmp(line, "---") == 0) {
            if (e->sessionVariables == NULL)
                e->sessionVariables = VariablesNew("session", TRUE);
            VariablesLoad(e->sessionVariables, f, &frb);
            break;
        }
        domain = path = name = NULL;
        expires = 0;
        for (p = line; *p && name == NULL; p = tab + 1) {
            if (tab = strchr(p, '\t'))
                *tab = 0;
            if (domain == NULL)
                domain = p;
            else if (path == NULL)
                path = p;
            else if (expires == 0)
                expires = (time_t)atol(p);
            else if (name == NULL)
                name = p;
            if (tab == NULL)
                break;
        }
        if (name != NULL)
            ClusterAddCookie(e, name, domain, path, expires);
    }
    close(f);
}

void RestoreCookies(void) {
    char filename[MAXKEY + 32];
#ifdef WIN32
    WIN32_FIND_DATA fileData;
    HANDLE hSearch;

    sprintf(filename, "%s%s*.*", COOKIESDIR, DIRSEP);
    hSearch = FindFirstFile(filename, &fileData);
    if (hSearch != INVALID_HANDLE_VALUE) {
        do {
            if ((fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
                RestoreCookiesSession(fileData.cFileName);
        } while (FindNextFile(hSearch, &fileData));
        FindClose(hSearch);
    }
#else
    DIR *dir;
    struct dirent *dirent;
    struct stat statBuf;

    dir = opendir(COOKIESDIR);
    if (dir == NULL)
        return;
    while (dirent = readdir(dir)) {
        if (strcmp(dirent->d_name, ".") == 0 || strcmp(dirent->d_name, "..") == 0)
            continue;
        sprintf(filename, "%s%s%s", COOKIESDIR, DIRSEP, dirent->d_name);
        if (stat(filename, &statBuf) != 0)
            continue;
        if (S_ISREG(statBuf.st_mode))
            RestoreCookiesSession(dirent->d_name);
    }
    closedir(dir);
#endif
}

void SendEZproxyCookie(struct TRANSLATE *t, struct SESSION *e) {
    struct UUSOCKET *s = &t->ssocket;
    struct EXTRALOGINCOOKIE *p;
    int i;

    if ((t->excludeIPBanner && excludeIPBannerOnce) || (e && e->key[0])) {
        if (p3pHeader) {
            UUSendZ(s, "P3P: ");
            UUSendZCRLF(s, p3pHeader);
        }

        for (p = extraLoginCookies; p; p = p->next)
            WriteLine(s, "Set-Cookie: %s\r\n", p->cookie);

        for (i = 0; i < LOGIN_COOKIE_SAME_SITE_MAX; i++) {
            if ((loginCookieSameSite & (1 << i)) == 0) {
                continue;
            }
            WriteLine(s, "Set-Cookie: %s", loginCookieName);
            if (i == LOGIN_COOKIE_SAME_SITE_NONE) {
                WriteLine(s, "n");
            }
            if (i == LOGIN_COOKIE_SAME_SITE_LAX) {
                WriteLine(s, "l");
            }
            WriteLine(s, "=");

            if (t->excludeIPBanner) {
                time_t now;
                UUtime(&now);
                WriteLine(s, "e%" PRIdMAX "; Path=/", (intmax_t)now);
            } else {
                if (haName)
                    WriteLine(s, "%s,", myUrlHttp);
                WriteLine(s, "%s; Path=/", e->key);
            }
            if (loginCookieDomain) {
                if (optionSafariCookiePatch && t->safari)
                    WriteLine(s, "; Domain=.%s", myName);
                else
                    WriteLine(s, "; Domain=%s", loginCookieDomain);
            }
            if (i == LOGIN_COOKIE_SAME_SITE_NONE) {
                WriteLine(s, "; SameSite=None; Secure");
            }
            if (i == LOGIN_COOKIE_SAME_SITE_LAX) {
                WriteLine(s, "; SameSite=Lax");
            }
            if (optionEZproxyCookieHTTPOnly || i != LOGIN_COOKIE_SAME_SITE_CLASSIC) {
                WriteLine(s, "; HttpOnly");
            }
            UUSendCRLF(s);
        }
    }
}
