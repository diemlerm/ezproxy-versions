#define __UUFILE__ "files.c"

#include "common.h"
#include "files.h"
#ifdef WIN32
#include <direct.h>
#else
#include <sys/stat.h>
#endif

static BOOL LicenseTxtChanged(FILE *f, char *text) {
    MD5_CTX md5Context;
    unsigned char md5DigestFile[16], md5DigestText[16];
    char line[256];

    MD5_Init(&md5Context);

    while (fgets(line, sizeof(line), f)) {
        Trim(line, TRIM_TRAIL);
        MD5_Update(&md5Context, (unsigned char *)line, (unsigned int)strlen(line));
        MD5_Update(&md5Context, (unsigned char *)"\n", 1);
    }
    MD5_Final(md5DigestFile, &md5Context);

    MD5_Init(&md5Context);
    MD5_Update(&md5Context, (unsigned char *)text, (unsigned int)strlen(text));
    MD5_Final(md5DigestText, &md5Context);

    return memcmp(md5DigestText, md5DigestFile, sizeof(md5DigestText)) != 0;
}

enum RENAMERESULT { newExists, noneExists, renameFailed };

enum ANSWERYESORNO { answerNo, answerYes, answerInvalid };

enum ANSWERYESORNO YesOrNo(char *format, ...) {
    va_list ap;
    int tries;
    char answer[10];

    for (tries = 0;; tries++) {
        va_start(ap, format);
        vprintf(format, ap);
        va_end(ap);
        answer[0] = 0;
        fgets(answer, sizeof(answer), stdin);
        Trim(answer, TRIM_TRAIL);
        if (stricmp(answer, "no") == 0)
            return answerNo;
        if (stricmp(answer, "yes") == 0)
            return answerYes;
        if (tries > 3)
            return answerInvalid;
    }
}

void CheckForBackslash(char *newName) {
    FILE *f;
    char line[8192];
    int lineNo = 0;
    char *p;
    int count = 0;
    int problemCount = 0;
    BOOL skipping = FALSE;

    f = fopen(newName, "r");

    if (f) {
        while (fgets(line, sizeof(line), f) != NULL) {
            Trim(line, TRIM_LEAD | TRIM_TRAIL);
            lineNo++;
            if (line[0] == '/') {
                skipping = FALSE;
            }
            /* Previously, ::ODBC blocks allowed \, so don't complain about them */
            if (strncmp(line, "::", 2) == 0 && StrIStr(line, "ODBC") != NULL) {
                skipping = TRUE;
            }
            p = AtEnd(line);
            if (p > line && *(p - 1) == '\\') {
                count++;
                if (!skipping)
                    problemCount++;
            }
        }
        fclose(f);
        f = NULL;
    }

    if (problemCount > 0) {
        Log("");
        Log("%s has %d line%s ending with a backslash (\\)", newName, count, SIfNotOne(count));
        Log("Review upgrade documentation regarding possible changes needed for %s line%s",
            ThisThese(count, FALSE), SIfNotOne(count));
    }
}

static BOOL RenameFor51(char *newName, char *oldName, int *missing, BOOL checkBackslash) {
    int result;
    enum ANSWERYESORNO answer;

    if (FileExists(newName))
        return newExists;

    if (!FileExists(oldName))
        return noneExists;

    Log("Starting with " EZPROXYNAMEPROPER " 5.1, the file %s is now named %s", oldName, newName);

    answer = YesOrNo("\nMay " EZPROXYNAMEPROPER " rename %s to %s (yes/no)? ", oldName, newName);
    if (answer != answerYes)
        return renameFailed;

    result = rename(oldName, newName);
    if (result == 0) {
        Log("%s has been renamed to %s", oldName, newName);
        CheckForBackslash(newName);
        Log("");
        *missing = *missing + 1;
        return newExists;
    }

    Log("Unable to rename %s to %s: %d", oldName, newName);
    return renameFailed;
}

void InstallFiles(char *me, int ifmode, char ifsubtype) {
    struct EZFILE *ezf, *ezf2;
    FILE *f;
    char filename[256];
    char slash;
    char *p;
    BOOL first;
    int missing;
    int i;
    BOOL licenseTxtChanged;
    BOOL licenseUpdated = 0;

#ifdef WIN32
    slash = '\\';
#else
    slash = '/';
#endif

    if (isupper(ifsubtype))
        ifsubtype = tolower(ifsubtype);

    first = 0;
    missing = 0;

    if (ifmode == IFREPLACE) {
        if (YesOrNo("\nReally replace all files with original versions (yes/no)? ") != answerYes) {
            Log("Files left unchanged");
            exit(1);
        }
    } else if (ifmode == IFMISSING) {
        if (RenameFor51(EZPROXYCFG, "ezproxy.cfg", &missing, TRUE) == renameFailed ||
            RenameFor51(EZPROXYMSG, "ezproxy.msg", &missing, FALSE) == renameFailed ||
            RenameFor51(EZPROXYUSR, "ezproxy.usr", &missing, TRUE) == renameFailed) {
            Log("EZproxy unable to rename existing files to new names, aborting");
            exit(1);
        }
    }

#ifndef WIN32
    if (ifmode != IFTEST) {
        /* If we are running as root and there is a RunAs value,
         * lower privilege to avoid writing files with wrong ownership.
         */
        ReadConfig(1);
        SetUidGid();
    }
#endif

    for (i = 0;; i++) {
        if (i == 0) {
            ezf = ezfiles;
        } else {
            if (ifsubtype == 'm') {
                if (i == 1)
                    ezf = ezfilesg;
                else if (i == 2)
                    ezf = ezfilesi;
                else if (i == 3)
                    ezf = ezfilesh;
                else if (i == 4)
                    ezf = ezfilesd;
                else if (i == 5)
                    ezf = ezfilesa;
                else if (i == 6)
                    ezf = ezfilesw;
                else
                    break;
            } else {
                if (i > 1)
                    break;
                if (ifsubtype == 'g')
                    ezf = ezfilesg;
                else if (ifsubtype == 'i')
                    ezf = ezfilesi;
                else if (ifsubtype == 'h')
                    ezf = ezfilesh;
                else if (ifsubtype == 'd')
                    ezf = ezfilesd;
                else if (ifsubtype == 'a')
                    ezf = ezfilesa;
                else if (ifsubtype == 'w')
                    ezf = ezfilesw;
                else if (ifsubtype == 'l')
                    ezf = ezfilesl;
                else if (ifsubtype == 's')
                    ezf = ezfiless;
                else
                    break;
            }
        }
        for (; ezf->filename; ezf++) {
            licenseTxtChanged = 0;
            strcpy(filename, ezf->filename);
            if (slash != '/')
                for (p = filename; p = strchr(p, '/'); p++)
                    *p = slash;
            /* when the text is NULL, that means just the directory */
            if (ezf->filetext != NULL) {
                if (ifmode == IFREPLACE) {
                    Log("Replace %s", filename);
                } else {
                    /* Happens only at startup, so there will be a file descriptor available to
                     * fopen */
                    f = fopen(filename, "r");
                    if (f != NULL) {
                        if (stricmp(filename, "license.txt") == 0)
                            licenseTxtChanged = LicenseTxtChanged(f, ezf->filetext);
                        fclose(f);
                        if (ifmode == IFMISSING && licenseTxtChanged == 0)
                            continue;
                    } else
                        missing++;
                    if (licenseTxtChanged) {
                        Log("Updating %s", filename);
                        licenseUpdated = 1;
                    } else if (ifmode == IFTEST) {
                        if (f == NULL) {
                            Log("Missing %s", filename);
                        }
                        continue;
                    } else
                        Log("Install %s", filename);
                }
            }
            if (ezf == ezfiles)
                first = 1;
            for (p = filename; p = strchr(p, slash); p++) {
                *p = 0;
                UUmkdir(filename);
                *p = slash;
            }
            /* just the directory */
            if (ezf->filetext == NULL)
                continue;
            /* Happens only at startup, so there will be a file descriptor available to fopen */
            f = fopen(filename, "w");
            if (f == NULL)
                Log("Unable to create %s", filename);
            else {
                ezf2 = ezf;
                /* This loop is so that we can have very long files even though there's a limit on
                 * the length of string constants.  The limit applies after string constant
                 * concatenation, and does not count the trailing NUL. In C90, the limit was 509
                 * characters; in C99, it was raised to 4095.*/
                for (; (ezf->filename != NULL) && (strcmp(ezf->filename, ezf2->filename) == 0);
                     ezf++) {
                    fputs(ezf->filetext, f);
                }
                ezf--;
                fclose(f);
            }
        }
    }

    if (missing != 0 && ifmode == IFTEST) {
        Log("\nFiles are missing, please restore missing files with the command:\n  %s -m\n", me);
        exit(1);
    }
    if (first) {
        Log("Please edit %s as appropriate then run\n  %s\nagain.\n", ezfiles->filename, me);
        exit(1);
    }
    if (missing == 0 && ifmode == IFMISSING && licenseUpdated == 0) {
        Log("There were no missing files to restore.\n");
    }
    if (ifmode != IFTEST)
        exit(1);
}

void FilesSecurityDefault(char **filename, char **fullname, char **filetext) {
    static char *savename = NULL;
    char *slash;

    if (savename == NULL) {
        savename = ezfiless[0].filename;
        if ((slash = strrchr(savename, '/'))) {
            savename = slash + 1;
        }
    }

    if (filename) {
        *filename = savename;
    }

    if (fullname) {
        *fullname = ezfiless[0].filename;
    }

    if (filetext) {
        *filetext = ezfiless[0].filetext;
    }
}
