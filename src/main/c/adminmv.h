#ifndef __ADMINMV_H__
#define __ADMINMV_H__

#include "common.h"

void AdminCheckMV(struct TRANSLATE *t);
void AdminMv(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINMV_H__
