/**
 * Ticket user authentication
 *
 * This module implement Ticket user authentication.  A ticket is a
 * cryptographically signed URL created by an external system to authenticate
 * a user into EZproxy.
 *
 * This authentication method supports common conditions and actions.
 */

#define __UUFILE__ "usrticket.c"

#include "common.h"

#ifdef WIN32
/* io.h, fcntl.h and share.h needed for _sopen in Win32 */
#include <io.h>
#include <fcntl.h>
#include <share.h>
#include <lm.h>
#endif

#include <sys/stat.h>

#include "usr.h"
#include "usrticket.h"

struct TICKETCTX {
    char *ticket;
    char *dollar;
    int timeOffset;
    int timeValid;
    BOOL acceptGroups;
    BOOL seenMethod;
    GROUPMASK acceptGroupsMask;
};

static BOOL UsrTicketSeenMethod(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                struct TICKETCTX *ticketCtx,
                                char *cmd) {
    if (ticketCtx->seenMethod) {
        UsrLog(uip, "Ticket %s must appear before MD5, SHA1, SHA256 or SHA512", cmd);
    }
    return ticketCtx->seenMethod;
}

static int UsrTicketValidate(struct TRANSLATE *t,
                             struct USERINFO *uip,
                             struct TICKETCTX *ticketCtx,
                             char *arg,
                             const EVP_MD *algorithmType) {
    EVP_MD_CTX *mdctx = NULL;
    unsigned char md_value[EVP_MAX_MD_SIZE];
    unsigned int md_len;
    char *hex;
    BOOL verifies;
    BOOL endSeen = FALSE;
    BOOL junkBeyondEnd = FALSE;
    char *field, *fieldTrack;
    char *inGroups = NULL;
    struct tm tm;
    time_t dt = 0, now = 0;
    int diff;

    if (arg == NULL || uip->user == NULL || ticketCtx->dollar == NULL) {
        return 1;
    }

    ticketCtx->seenMethod = 1;

    if (!(mdctx = EVP_MD_CTX_new()))
        PANIC;
    EVP_DigestInit(mdctx, algorithmType);
    EVP_DigestUpdate(mdctx, arg, strlen(arg));
    EVP_DigestUpdate(mdctx, uip->user, strlen(uip->user));
    EVP_DigestUpdate(mdctx, ticketCtx->dollar, strlen(ticketCtx->dollar));
    EVP_DigestFinal(mdctx, md_value, &md_len);

    hex = DigestToHex(NULL, md_value, md_len, 1);
    if (mdctx) {
        EVP_MD_CTX_free(mdctx);
        mdctx = NULL;
    }
    verifies = strnicmp(hex, ticketCtx->ticket, ticketCtx->dollar - ticketCtx->ticket) == 0;
    if (uip->debug) {
        UsrLog(uip, "Ticket expected signature %s %s", hex,
               verifies ? "matches" : "does not match");
    }
    FreeThenNull(hex);
    if (!verifies) {
        return 1;
    }

    for (fieldTrack = NULL, field = StrTok_R(ticketCtx->dollar + 1, "$", &fieldTrack); field;
         field = StrTok_R(NULL, "$", &fieldTrack)) {
        if (endSeen) {
            junkBeyondEnd = TRUE;
        }

        if (*field == 'u' && IsAllDigits(field + 1)) {
            dt = atoi(field + 1);
        }

        if (*field == 'c' && IsAllDigits(field + 1)) {
            memset(&tm, 0, sizeof(tm));
            tm.tm_isdst = -1;
            if (sscanf(field + 1, "%4d%2d%2d%2d%2d%2d", &tm.tm_year, &tm.tm_mon, &tm.tm_mday,
                       &tm.tm_hour, &tm.tm_min, &tm.tm_sec) == 6) {
                tm.tm_mon--;
                tm.tm_year -= 1900;
                dt = mktime(&tm);
            }
        }

        if (*field == 'g') {
            inGroups = field + 1;
        }

        if (*field == 'e') {
            endSeen = TRUE;
        }
    }

    if (endSeen) { /* TODO: When everyone is using the new ticket with '$e', remove this outer 'if'
                      statement and it's else clause. */
        verifies = endSeen && (!junkBeyondEnd);
        if (!verifies) {
            if (uip->debug)
                UsrLog(uip, "Ticket is valid because %s",
                       (endSeen) ? "there was junk beyond the end of the ticket"
                                 : "the end of the ticket was not found (no $e)");
            return 1;
        }
    } else {
        if (uip->debug)
            UsrLog(uip, "Warning: an insecure ticket was detected.  It doesn't have a '$e' field.");
        Log("Warning: an insecure ticket was detected.  It doesn't have a '$e' field.");
    }

    if (dt == 0) {
        return 1;
    }

    dt += ticketCtx->timeOffset;

    UUtime(&now);

    diff = now < dt ? dt - now : now - dt;

    if (diff < ticketCtx->timeValid) {
        if (uip->debug)
            UsrLog(uip, "Ticket is valid");
        uip->result = resultValid;
        if (ticketCtx->acceptGroups && inGroups) {
            MakeGroupMask(uip->gm, inGroups, 0, '=');
            if (ticketCtx->acceptGroupsMask)
                GroupMaskAnd(uip->gm, ticketCtx->acceptGroupsMask);
        }
        return 0;
    } else {
        if (uip->debug)
            UsrLog(uip, "Ticket has expired");
        uip->result = resultExpired;
        return 1;
    }
}

static int UsrTicketAcceptGroups(struct TRANSLATE *t,
                                 struct USERINFO *uip,
                                 void *context,
                                 char *arg) {
    struct TICKETCTX *ticketCtx = (struct TICKETCTX *)context;

    if (UsrTicketSeenMethod(t, uip, ticketCtx, "AcceptGroups")) {
        return 1;
    }

    ticketCtx->acceptGroups = 1;
    if (*arg) {
        ticketCtx->acceptGroupsMask = MakeGroupMask(ticketCtx->acceptGroupsMask, arg, 0, '=');
    } else {
        GroupMaskFree(&ticketCtx->acceptGroupsMask);
    }
    return 0;
}

static int UsrTicketMD5(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return UsrTicketValidate(t, uip, context, arg, EVP_md5());
}

static int UsrTicketSHA1(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return UsrTicketValidate(t, uip, context, arg, EVP_sha1());
}

static int UsrTicketSHA256(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return UsrTicketValidate(t, uip, context, arg, EVP_sha256());
}

static int UsrTicketSHA512(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return UsrTicketValidate(t, uip, context, arg, EVP_sha512());
}

static int UsrTicketTimeOffset(struct TRANSLATE *t,
                               struct USERINFO *uip,
                               void *context,
                               char *arg) {
    struct TICKETCTX *ticketCtx = (struct TICKETCTX *)context;

    if (UsrTicketSeenMethod(t, uip, ticketCtx, "TimeOffset")) {
        return 1;
    }

    ticketCtx->timeOffset = atoi(arg) * 60;
    return 0;
}

static int UsrTicketTimeValid(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct TICKETCTX *ticketCtx = (struct TICKETCTX *)context;

    if (UsrTicketSeenMethod(t, uip, ticketCtx, "TimeValid")) {
        return 1;
    }

    ticketCtx->timeValid = atoi(arg) * 60;
    return 0;
}

/* Returns 0 if valid, 1 if not, 3 if unable to connect to imap host */
int FindUserTicket(struct TRANSLATE *t,
                   struct USERINFO *uip,
                   char *file,
                   int f,
                   struct FILEREADLINEBUFFER *frb,
                   struct sockaddr_storage *pInterface) {
    struct TICKETCTX *ticketCtx, ticketCtxBuffer;
    struct USRDIRECTIVE udc[] = {
        {"AcceptGroups", UsrTicketAcceptGroups, 0},
        {"MD5",          UsrTicketMD5,          0},
        {"SHA1",         UsrTicketSHA1,         0},
        {"SHA256",       UsrTicketSHA256,       0},
        {"SHA512",       UsrTicketSHA512,       0},
        {"TimeOffset",   UsrTicketTimeOffset,   0},
        {"TimeValid",    UsrTicketTimeValid,    0},
        {NULL,           NULL,                  0}
    };

    memset(&ticketCtxBuffer, 0, sizeof(ticketCtxBuffer));
    ticketCtx = &ticketCtxBuffer;

    ticketCtx->timeValid = ONE_HOUR_IN_SECONDS;

    if (uip->ticket)
        if (!(ticketCtx->ticket = strdup(uip->ticket)))
            PANIC;

    uip->result = resultInvalid;

    ticketCtx->dollar = ticketCtx->ticket ? strchr(ticketCtx->ticket, '$') : NULL;
    if (uip->debug && ticketCtx->dollar && uip->user)
        UsrLog(uip, "Ticket checking user %s ticket %s", uip->user, ticketCtx->ticket);

    uip->skipping = ticketCtx->ticket == NULL || uip->user == NULL || *uip->user == 0;

    uip->result = UsrHandler(t, "Ticket", udc, ticketCtx, uip, file, f, frb, pInterface, NULL);

    GroupMaskFree(&ticketCtx->acceptGroupsMask);
    FreeThenNull(ticketCtx->ticket);

    return uip->result;
}
