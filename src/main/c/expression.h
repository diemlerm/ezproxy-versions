#ifndef __EXPRESSION_H__
#define __EXPRESSION_H__

#include "common.h"

/* The ANY values must always be even, followed immediately by the corresponding ALL values that is
 * odd */
enum AGGREGATETESTOPTIONS {
    ATOANYEQ = 0,
    ATOALLEQ = 1,
    ATOANYRE = 2,
    ATOALLRE = 3,
    ATOANYXMLRE = 4,
    ATOALLXMLRE = 5,
    ATOANYWILD = 6,
    ATOALLWILD = 7,
    ATOCOUNT
};

struct AGGREGATETESTCONTEXT {
    const char *testVal;
    char *lastRealVal;
    xmlRegexpPtr xmlRePattern;
    pcre *rePattern;
    pcre_extra *rePatternExtra;
    struct USERINFO *uip;
    int nmat;
    int ovec[30]; /* 10 slots * 3 requires entries per slot */
    BOOL result;
    int count;
    BOOL any;
    BOOL all;
    BOOL more;
    enum AGGREGATETESTOPTIONS ato;
    size_t varLen;
};

#include "variables.h"

struct USERINFO;

BOOL ExpressionAggregateTestInitialize(struct AGGREGATETESTCONTEXT *atc,
                                       struct USERINFO *uip,
                                       const char *testVal,
                                       enum AGGREGATETESTOPTIONS ato);
BOOL ExpressionAggregateTestValue(struct AGGREGATETESTCONTEXT *atc, const char *realVal);
int ExpressionAggregateTestTerminate(struct AGGREGATETESTCONTEXT *atc, struct VARIABLES *rev);

char *ExpressionValueReportError(struct TRANSLATE *t,
                                 struct USERINFO *uip,
                                 void *context,
                                 char *expr,
                                 BOOL reportErrors);
char *ExpressionValue(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *expr);
char *ExpressionVariableValue(struct TRANSLATE *t,
                              struct USERINFO *uip,
                              void *context,
                              char *var,
                              char *index,
                              BOOL nullToEmptyString);
char **ExpressionValueList(struct TRANSLATE *t,
                           struct USERINFO *uip,
                           void *context,
                           char *exprCommaList);
BOOL ExpressionValueTrue(char *val);
char *ExpressionVariableIndexBufferStr(const char *var,
                                       const char *index,
                                       char *buffer,
                                       int start,
                                       int len);
char *ExpressionVariableIndexBufferNum(const char *var,
                                       int index,
                                       char *buffer,
                                       int start,
                                       int len);
char *ExpressionVariableIndexNum(const char *var, int index);
char *ExpressionVariableIndex(const char *var, const char *index);
BOOL IsScalarVarName(char *start);
BOOL IsVarName(char *start);

/*#ifdef EXPRESSION_INTERNAL_FUNCTIONS */
enum TOKENTYPE { /******************* KEEP IN SYNC WITH ExpressionCheckToken
                    **************************/
                 NONE,
                 NAME,
                 NAMEWITHQUOTES,
                 NUMBER,
                 STRING,
                 INVALID,
                 END,
                 NUMLE,              // <=
                 NUMGE,              // >=
                 NUMEQ,              // ==
                 NUMNE,              // !=
                 STRLT,              // LT
                 STRLE,              // LE
                 STREQ,              // EQ
                 STRNE,              // NE
                 STRGT,              // GT
                 STRGE,              // GE
                 WILDMATCH,          // =*
                 REMATCH,            // =~
                 NOREMATCH,          // !~
                 XMLREMATCH,         // =~<>
                 XMLNOREMATCH,       // !~<>
                 AND,                // &&
                 OR,                 // ||
                 ASSIGN_ADD,         //  +=
                 ASSIGN_SUB,         //  -=
                 ASSIGN_MUL,         //  *=
                 ASSIGN_DIV,         //  /=
                 ASSIGN_CAT,         //  .=
                 MINUS_MINUS = 'M',  //  --
                 PLUS_PLUS = 'P',    //  ++
                 PLUS = '+',
                 MINUS = '-',
                 MULTIPLY = '*',
                 DIVIDE = '/',
                 BITWISEAND = '&',
                 BITWISEOR = '|',
                 MODULUS = '%',
                 ASSIGN = '=',
                 LHPAREN = '(',
                 RHPAREN = ')',
                 QUESTION = '?',
                 COLON = ':',
                 COMMA = ',',
                 NOT = '!',
                 LHBRACKET = '[',
                 RHBRACKET = ']',
                 NUMLT = '<',
                 NUMGT = '>',
                 PERIOD = '.',
};

struct EXPRESSIONCTX {
    struct TRANSLATE *t;
    struct USERINFO *uip;
    void *context;
    char *start;
    char *prev;
    char *curr;
    const char *invalidMsg;
    char *token;
    size_t tokenMax;
    enum TOKENTYPE tokenType;
    int shortCircuiting;
};

enum TOKENTYPE IsVarNameOrCompareOper(char *start, char **end);

void ExpressionGetToken(struct EXPRESSIONCTX *ec);
char *ExpressionBoolean(BOOL b);
char *ExpressionStrdup(const char *val);
char *ExpressionExpression(struct EXPRESSIONCTX *ec, BOOL get);
void ExpressionSetInvalid(struct EXPRESSIONCTX *ec, const char *invalidMsg);
char *ExpressionCat(char *str1, char *str2);
BOOL ExpressionCheckToken(struct EXPRESSIONCTX *ec, enum TOKENTYPE tokenType);
char *ExpressionVariable(struct EXPRESSIONCTX *ec,
                         const char *var,
                         const char *index,
                         BOOL nullToEmptyString);
char *ExpressionDouble(double r);
char *ExpressionInt(int i);
/*#endif*/

#endif  // __EXPRESSION_H__
