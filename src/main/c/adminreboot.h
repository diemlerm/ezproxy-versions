#ifndef __ADMINREBOOT_H__
#define __ADMINREBOOT_H__

#include "common.h"

void AdminReboot(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINREBOOT_H__
