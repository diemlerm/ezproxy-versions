#ifndef __ADMINSHIBBOLETH_H__
#define __ADMINSHIBBOLETH_H__

#include "common.h"

void AdminShibboleth(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINSHIBBOLETH_H__
