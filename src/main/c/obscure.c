#define __UUFILE__ "obscure.c"

#include "common.h"
#include "uustring.h"
#include <sys/stat.h>
#include <openssl/des.h>

#include "obscure.h"

#define OBSCURESALTLEN 4

char *ObscureString2(char *input) {
    DES_cblock ivec;
    char *output = NULL;
    unsigned char *hold = NULL;
    unsigned char *salt;
    unsigned char *p;
    EVP_CIPHER_CTX *ctx = NULL;
    int updateLen, finalLen;
    int i;

    if (input == NULL)
        return NULL;

    /* Can't grow by more than 1 for header, OBSCURESALTLEN, 1 for null, 16 for the possible pad */
    if (!(hold = malloc(strlen(input) + 1 + OBSCURESALTLEN + 1 + 16)))
        PANIC;

    if (!(ctx = EVP_CIPHER_CTX_new()))
        PANIC;
    EVP_CIPHER_CTX_init(ctx);
    memset(&ivec, 0, sizeof(ivec));
    EVP_EncryptInit(ctx, EVP_des_ede3_cbc(), Obscurity(2), ivec);

    p = hold;

    *p++ = '2';
    salt = p;

    /* Create some salt */
    for (i = 0; i < OBSCURESALTLEN; i++)
        *p++ = RandChar();

    /* Start by adding in the salt */
    if (!EVP_EncryptUpdate(ctx, p, &updateLen, salt, OBSCURESALTLEN)) {
        goto Cleanup;
    }
    p += updateLen;

    /* Send the NULL as well; we check on receive to see if it is there */
    if (!EVP_EncryptUpdate(ctx, p, &updateLen, (unsigned char *)input, strlen(input) + 1)) {
        goto Cleanup;
    }
    p += updateLen;

    if (!EVP_EncryptFinal(ctx, p, &finalLen)) {
        goto Cleanup;
    }
    p += finalLen;

    output = Encode64Binary(NULL, hold, p - hold);

Cleanup:
    if (ctx) {
        EVP_CIPHER_CTX_free(ctx);
        ctx = NULL;
    }
    FreeThenNull(hold);

    return output;
}

char *ObscureBinary3(unsigned char *input, size_t len) {
    unsigned char ivec[16];
    char *output = NULL;
    unsigned char *hold = NULL;
    unsigned char *salt;
    unsigned char *p;
    EVP_CIPHER_CTX *ctx = NULL;
    int updateLen, finalLen;
    int i;
    unsigned char nullbyte = 0;

    if (input == NULL)
        return NULL;

    /* Can't grow by more than 1 for header, OBSCURESALTLEN, 1 for null, 32 for the possible pad */
    if (!(hold = malloc(len + 1 + OBSCURESALTLEN + 1 + 32)))
        PANIC;

    if (!(ctx = EVP_CIPHER_CTX_new()))
        PANIC;
    EVP_CIPHER_CTX_init(ctx);
    memset(&ivec, 0, sizeof(ivec));
    EVP_EncryptInit(ctx, EVP_aes_256_cbc(), Obscurity(3), ivec);

    p = hold;

    *p++ = '3';
    salt = p;

    /* Create some salt */
    for (i = 0; i < OBSCURESALTLEN; i++)
        *p++ = RandChar();

    /* Start by adding in the salt */
    if (!EVP_EncryptUpdate(ctx, p, &updateLen, salt, OBSCURESALTLEN)) {
        goto Cleanup;
    }
    p += updateLen;

    if (len > 0) {
        /* Send the input */
        if (!EVP_EncryptUpdate(ctx, p, &updateLen, (unsigned char *)input, len)) {
            goto Cleanup;
        }
        p += updateLen;
    }

    if (!EVP_EncryptFinal(ctx, p, &finalLen)) {
        goto Cleanup;
    }
    p += finalLen;

    output = Encode64Binary(NULL, hold, p - hold);

Cleanup:
    if (ctx) {
        EVP_CIPHER_CTX_free(ctx);
        ctx = NULL;
    }
    FreeThenNull(hold);

    return output;
}

char *ObscureString3(char *input) {
    if (input == NULL) {
        return NULL;
    } else {
        // strlen + 1 to include the final null
        return ObscureBinary3((unsigned char *)input, strlen(input) + 1);
    }
}

char *ObscureString(char *input) {
    // Uses ObscureString2 by default for compatibility for now
    return ObscureString2(input);
}

unsigned char *UnobscureBinary(char *input, size_t *outLen) {
    unsigned char ivec[16];
    unsigned char *hold = NULL;
    unsigned char *output = NULL;
    size_t len;
    EVP_CIPHER_CTX *ctx = NULL;
    int updateLen, finalLen;

    if (*outLen) {
        *outLen = 0;
    }

    if (input == NULL || outLen == NULL)
        goto Invalid;

    if (!(hold = malloc(strlen(input) + 1)))
        PANIC;
    Decode64Binary(hold, &len, input);
    if (len == 0) {
        if (debugLevel > 0) {
            Log("UB D64B 0");
        }
        goto Invalid;
    }

    if (*hold >= '1' || *hold <= '3') {
        if (len <= 1 + OBSCURESALTLEN || (len - 1 - OBSCURESALTLEN) % 8 != 0)
            goto Invalid;
        if (!(ctx = EVP_CIPHER_CTX_new()))
            PANIC;
        EVP_CIPHER_CTX_init(ctx);
        memset(&ivec, 0, sizeof(ivec));
        if (*hold == '3') {
            EVP_DecryptInit(ctx, EVP_aes_256_cbc(), Obscurity(*hold - '0'), ivec);
        } else {
            EVP_DecryptInit(ctx, EVP_des_ede3_cbc(), Obscurity(*hold - '0'), ivec);
        }

        /* The decrypted version cannot be larger than the encrypted version */
        if (!(output = malloc(len)))
            PANIC;
        *output = 0;

        if (!EVP_DecryptUpdate(ctx, output, &updateLen, hold + 1 + OBSCURESALTLEN,
                               len - 1 - OBSCURESALTLEN)) {
            if (debugLevel > 0) {
                Log("UB DU err");
            }
            goto Invalid;
        }

        if (!EVP_DecryptFinal(ctx, output + updateLen, &finalLen)) {
            if (debugLevel > 0) {
                Log("UB DF err");
            }
            goto Invalid;
        }

        EVP_CIPHER_CTX_cleanup(ctx);
        len = updateLen + finalLen;

        if (len < OBSCURESALTLEN ||
            strnicmp((char *)output, (char *)hold + 1, OBSCURESALTLEN) != 0) {
            if (debugLevel > 0) {
                Log("UB cmp");
            }
            goto Invalid;
        }

        *outLen = len - OBSCURESALTLEN;
        if (*outLen > 0) {
            memmove(output, output + OBSCURESALTLEN, *outLen);
        }
        goto Cleanup;
    }

Invalid:
    FreeThenNull(output);

Cleanup:
    if (ctx) {
        EVP_CIPHER_CTX_free(ctx);
        ctx = NULL;
    }
    FreeThenNull(hold);

    return output;
}

char *UnobscureString(char *input) {
    char *output = NULL;
    size_t len;

    output = (char *)UnobscureBinary(input, &len);
    // Any returned value must be null terminated to be valid
    if (output && (len == 0 || output[len - 1] != 0)) {
        FreeThenNull(output);
    }

    return output;
}

void ObscureFile(char *filename) {
    char *obscure = NULL;
    int f;
    struct stat buf;
    ssize_t len;
    char *content = NULL;
    ssize_t remain;
    char *p;

    f = SBOPEN(filename);
    if (f < 0) {
        Log("Unable to %s: %s", filename, ErrorMessage(errno));
    } else {
        if (fstat(f, &buf))
            PANIC;
        if (!(content = malloc(buf.st_size)))
            PANIC;
        p = content;
        for (remain = buf.st_size; remain > 0; remain -= len, p += len) {
            len = read(f, p, remain);
            if (len < 0)
                PANIC;
            if (len == 0) {
                break;
            }
        }
        close(f);
        obscure = ObscureBinary3(content, p - content);
        for (remain = strlen(obscure), p = obscure; remain > 0; remain -= len, p += len) {
            len = UUMIN(64, remain);
            printf("%.*s\n", len, p);
        }
    }
    FreeThenNull(content);
    FreeThenNull(obscure);
}

unsigned char *UnobscureFile(char *filename, size_t *outLen) {
    unsigned char *unobscure = NULL;
    int f;
    struct stat buf;
    ssize_t len;
    char *content = NULL;
    ssize_t remain;
    char *p;

    f = SBOPEN(filename);
    if (f < 0) {
        Log("Unable to %s: %s", filename, ErrorMessage(errno));
    } else {
        if (fstat(f, &buf))
            PANIC;
        if (!(content = malloc(buf.st_size + 1)))
            PANIC;
        p = content;
        for (remain = buf.st_size; remain > 0; remain -= len, p += len) {
            len = read(f, p, remain);
            if (len < 0)
                PANIC;
            if (len == 0) {
                break;
            }
        }
        *p = 0;
        close(f);
        unobscure = UnobscureBinary(content, outLen);
    }

    FreeThenNull(content);
    return unobscure;
}
