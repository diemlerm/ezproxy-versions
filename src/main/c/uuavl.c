/*
 * Copyright 1998-2003 The OpenLDAP Foundation, All Rights Reserved.
 * COPYING RESTRICTIONS APPLY, see COPYRIGHT file
 */
/*
 * Copyright (c) 1993 Regents of the University of Michigan.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that this notice is preserved and that due credit is given
 * to the University of Michigan at Ann Arbor. The name of the University
 * may not be used to endorse or promote products derived from this
 * software without specific prior written permission. This software
 * is provided ``as is'' without express or implied warranty.
 */

#define __UUFILE__ "uuavl.c"

#define UUAVL_INTERNAL
#include "uuavl.h"

#define ROTATERIGHT(x)                                    \
    {                                                     \
        struct uuavlnode *tmp;                            \
        if (*(x) == NULL || (*(x))->uuavl_left == NULL) { \
            (void)fputs("RR error\n", stderr);            \
            exit(EXIT_FAILURE);                           \
        }                                                 \
        tmp = (*(x))->uuavl_left;                         \
        (*(x))->uuavl_left = tmp->uuavl_right;            \
        tmp->uuavl_right = *(x);                          \
        *(x) = tmp;                                       \
    }
#define ROTATELEFT(x)                                      \
    {                                                      \
        struct uuavlnode *tmp;                             \
        if (*(x) == NULL || (*(x))->uuavl_right == NULL) { \
            (void)fputs("RL error\n", stderr);             \
            exit(EXIT_FAILURE);                            \
        }                                                  \
        tmp = (*(x))->uuavl_right;                         \
        (*(x))->uuavl_right = tmp->uuavl_left;             \
        tmp->uuavl_left = *(x);                            \
        *(x) = tmp;                                        \
    }

/*
 * ruuavl_insert - called from uuavl_insert() to do a recursive insert into
 * and balance of an uuavl tree.
 */

static int ruuavl_insert(struct uuavlnode **iroot,
                         char *name,
                         struct uuavlnode *closest,
                         void *data,
                         int *taller,
                         UUAVL_CMP fcmp, /* comparison function */
                         UUAVL_DUP fdup, /* function to call for duplicates */
                         int depth) {
    int rc, cmp, tallersub;
    struct uuavlnode *l, *r;

    if (*iroot == NULL) {
        if (!(*iroot = (struct uuavlnode *)malloc(sizeof(struct uuavlnode))))
            PANIC;
        (*iroot)->uuavl_left = NULL;
        (*iroot)->uuavl_right = NULL;
        (*iroot)->uuavl_bf = 0;
        (*iroot)->uuavl_data = data;
        if (closest) {
            (*iroot)->uuavl_next = closest;
            (*iroot)->uuavl_prev = closest->uuavl_prev;
            (*iroot)->uuavl_next->uuavl_prev = *iroot;
            (*iroot)->uuavl_prev->uuavl_next = *iroot;
        } else {
            (*iroot)->uuavl_prev = (*iroot)->uuavl_next = *iroot;
        }
        *taller = 1;
        return (0);
    }

    cmp = (*fcmp)(data, (*iroot)->uuavl_data);

    /* equal - duplicate name */
    if (cmp == 0) {
        *taller = 0;
        return ((*fdup)(name, (*iroot)->uuavl_data, data));
    }

    /* go right */
    else if (cmp > 0) {
        rc = ruuavl_insert(&((*iroot)->uuavl_right), name, closest, data, &tallersub, fcmp, fdup,
                           depth);
        if (tallersub)
            switch ((*iroot)->uuavl_bf) {
            case LH: /* left high - balance is restored */
                (*iroot)->uuavl_bf = EH;
                *taller = 0;
                break;
            case EH: /* equal height - now right heavy */
                (*iroot)->uuavl_bf = RH;
                *taller = 1;
                break;
            case RH: /* right heavy to start - right balance */
                r = (*iroot)->uuavl_right;
                switch (r->uuavl_bf) {
                case LH: /* double rotation left */
                    l = r->uuavl_left;
                    switch (l->uuavl_bf) {
                    case LH:
                        (*iroot)->uuavl_bf = EH;
                        r->uuavl_bf = RH;
                        break;
                    case EH:
                        (*iroot)->uuavl_bf = EH;
                        r->uuavl_bf = EH;
                        break;
                    case RH:
                        (*iroot)->uuavl_bf = LH;
                        r->uuavl_bf = EH;
                        break;
                    }
                    l->uuavl_bf = EH;
                    ROTATERIGHT((&r))
                    (*iroot)->uuavl_right = r;
                    ROTATELEFT(iroot)
                    *taller = 0;
                    break;
                case EH: /* This should never happen */
                    break;
                case RH: /* single rotation left */
                    (*iroot)->uuavl_bf = EH;
                    r->uuavl_bf = EH;
                    ROTATELEFT(iroot)
                    *taller = 0;
                    break;
                }
                break;
            }
        else
            *taller = 0;
    }

    /* go left */
    else {
        closest = *iroot;
        rc = ruuavl_insert(&((*iroot)->uuavl_left), name, closest, data, &tallersub, fcmp, fdup,
                           depth);
        if (tallersub)
            switch ((*iroot)->uuavl_bf) {
            case LH: /* left high to start - left balance */
                l = (*iroot)->uuavl_left;
                switch (l->uuavl_bf) {
                case LH: /* single rotation right */
                    (*iroot)->uuavl_bf = EH;
                    l->uuavl_bf = EH;
                    ROTATERIGHT(iroot)
                    *taller = 0;
                    break;
                case EH: /* this should never happen */
                    break;
                case RH: /* double rotation right */
                    r = l->uuavl_right;
                    switch (r->uuavl_bf) {
                    case LH:
                        (*iroot)->uuavl_bf = RH;
                        l->uuavl_bf = EH;
                        break;
                    case EH:
                        (*iroot)->uuavl_bf = EH;
                        l->uuavl_bf = EH;
                        break;
                    case RH:
                        (*iroot)->uuavl_bf = EH;
                        l->uuavl_bf = LH;
                        break;
                    }
                    r->uuavl_bf = EH;
                    ROTATELEFT((&l))
                    (*iroot)->uuavl_left = l;
                    ROTATERIGHT(iroot)
                    *taller = 0;
                    break;
                }
                break;
            case EH: /* equal height - now left heavy */
                (*iroot)->uuavl_bf = LH;
                *taller = 1;
                break;
            case RH: /* right high - balance is restored */
                (*iroot)->uuavl_bf = EH;
                *taller = 0;
                break;
            }
        else
            *taller = 0;
    }

    return (rc);
}

/*
 * uuavl_insert -- insert a node containing data data into the uuavl tree
 * with root root.  fcmp is a function to call to compare the data portion
 * of two nodes.  it should take two arguments and return <, >, or == 0,
 * depending on whether its first argument is <, >, or == its second
 * argument (like strcmp, e.g.).  fdup is a function to call when a duplicate
 * node is inserted.  it should return 0, or -1 and its return value
 * will be the return value from uuavl_insert in the case of a duplicate node.
 * the function will be called with the original node's data as its first
 * argument and with the incoming duplicate node's data as its second
 * argument.  this could be used, for example, to keep a count with each
 * node.
 *
 * NOTE: this routine may malloc memory
 */

int uuavl_insert(struct uuavlnode **root,
                 char *name,
                 struct uuavlnode *closest,
                 void *data,
                 UUAVL_CMP fcmp,
                 UUAVL_DUP fdup) {
    int taller;

    return (ruuavl_insert(root, name, closest, data, &taller, fcmp, fdup, 0));
}

int UUAvlTreeInsert(struct UUAVLTREE *tree, void *data) {
    int result;

    if (tree->first) {
        /* We start closest at tree->first; if no node is encountered that is greater
           than the current node before reaching the end of insertion, then the new
           node is the largest and its next pointer should be back to the first, lowest
           node, which is tree->first
        */
        result =
            uuavl_insert(&tree->root, tree->name, tree->first, data, tree->avlCmp, tree->avlDup);
        /* If an earlier node than tree->first was added,
           then tree->first->uuavl_left will be non-null and
           we can just follow to the left until we find a non-null
           to find the new first node
        */
        for (; tree->first->uuavl_left; tree->first = tree->first->uuavl_left)
            ;
    } else {
        /* we are inserting the very first node, so we need to create the initial
           tree->first value
        */
        result = uuavl_insert(&tree->root, tree->name, NULL, data, tree->avlCmp, tree->avlDup);
        tree->first = tree->root;
    }

    return result;
}

/*
 * right_balance() - called from delete when root's right subtree has
 * been shortened because of a deletion.
 */

static int right_balance(struct uuavlnode **root) {
    int shorter = -1;
    struct uuavlnode *r, *l;

    switch ((*root)->uuavl_bf) {
    case RH: /* was right high - equal now */
        (*root)->uuavl_bf = EH;
        shorter = 1;
        break;
    case EH: /* was equal - left high now */
        (*root)->uuavl_bf = LH;
        shorter = 0;
        break;
    case LH: /* was right high - balance */
        l = (*root)->uuavl_left;
        switch (l->uuavl_bf) {
        case RH: /* double rotation left */
            r = l->uuavl_right;
            switch (r->uuavl_bf) {
            case RH:
                (*root)->uuavl_bf = EH;
                l->uuavl_bf = LH;
                break;
            case EH:
                (*root)->uuavl_bf = EH;
                l->uuavl_bf = EH;
                break;
            case LH:
                (*root)->uuavl_bf = RH;
                l->uuavl_bf = EH;
                break;
            }
            r->uuavl_bf = EH;
            ROTATELEFT((&l))
            (*root)->uuavl_left = l;
            ROTATERIGHT(root)
            shorter = 1;
            break;
        case EH: /* right rotation */
            (*root)->uuavl_bf = LH;
            l->uuavl_bf = RH;
            ROTATERIGHT(root);
            shorter = 0;
            break;
        case LH: /* single rotation right */
            (*root)->uuavl_bf = EH;
            l->uuavl_bf = EH;
            ROTATERIGHT(root)
            shorter = 1;
            break;
        }
        break;
    }

    return (shorter);
}

/*
 * left_balance() - called from delete when root's left subtree has
 * been shortened because of a deletion.
 */

static int left_balance(struct uuavlnode **root) {
    int shorter = -1;
    struct uuavlnode *r, *l;

    switch ((*root)->uuavl_bf) {
    case LH: /* was left high - equal now */
        (*root)->uuavl_bf = EH;
        shorter = 1;
        break;
    case EH: /* was equal - right high now */
        (*root)->uuavl_bf = RH;
        shorter = 0;
        break;
    case RH: /* was right high - balance */
        r = (*root)->uuavl_right;
        switch (r->uuavl_bf) {
        case LH: /* double rotation left */
            l = r->uuavl_left;
            switch (l->uuavl_bf) {
            case LH:
                (*root)->uuavl_bf = EH;
                r->uuavl_bf = RH;
                break;
            case EH:
                (*root)->uuavl_bf = EH;
                r->uuavl_bf = EH;
                break;
            case RH:
                (*root)->uuavl_bf = LH;
                r->uuavl_bf = EH;
                break;
            }
            l->uuavl_bf = EH;
            ROTATERIGHT((&r))
            (*root)->uuavl_right = r;
            ROTATELEFT(root)
            shorter = 1;
            break;
        case EH: /* single rotation left */
            (*root)->uuavl_bf = RH;
            r->uuavl_bf = LH;
            ROTATELEFT(root);
            shorter = 0;
            break;
        case RH: /* single rotation left */
            (*root)->uuavl_bf = EH;
            r->uuavl_bf = EH;
            ROTATELEFT(root)
            shorter = 1;
            break;
        }
        break;
    }

    return (shorter);
}

/*
 * ruuavl_delete() - called from uuavl_delete to do recursive deletion of a
 * node from an uuavl tree.  It finds the node recursively, deletes it,
 * and returns shorter if the tree is shorter after the deletion and
 * rebalancing.
 */

static void *ruuavl_delete(struct uuavlnode **root, void *data, UUAVL_CMP fcmp, int *shorter) {
    int shortersubtree = 0;
    int cmp;
    void *savedata;
    struct uuavlnode *minnode, *savenode;

    if (*root == NULL)
        return (0);

    cmp = (*fcmp)(data, (*root)->uuavl_data);

    /* found it! */
    if (cmp == 0) {
        savenode = *root;
        savedata = savenode->uuavl_data;

        /* simple cases: no left child */
        if ((*root)->uuavl_left == NULL) {
            *root = (*root)->uuavl_right;
            *shorter = 1;
            savenode->uuavl_prev->uuavl_next = savenode->uuavl_next;
            savenode->uuavl_next->uuavl_prev = savenode->uuavl_prev;
            free((char *)savenode);
            return (savedata);
            /* no right child */
        } else if ((*root)->uuavl_right == NULL) {
            *root = (*root)->uuavl_left;
            *shorter = 1;
            savenode->uuavl_prev->uuavl_next = savenode->uuavl_next;
            savenode->uuavl_next->uuavl_prev = savenode->uuavl_prev;
            free((char *)savenode);
            return (savedata);
        }

        /*
         * uuavl_getmin will return to us the smallest node greater
         * than the one we are trying to delete.  deleting this node
         * from the right subtree is guaranteed to end in one of the
         * simple cases above.
         */

        minnode = (*root)->uuavl_right;
        while (minnode->uuavl_left != NULL)
            minnode = minnode->uuavl_left;

        /* swap the data */
        (*root)->uuavl_data = minnode->uuavl_data;
        minnode->uuavl_data = savedata;

        savedata = ruuavl_delete(&(*root)->uuavl_right, data, fcmp, &shortersubtree);

        if (shortersubtree)
            *shorter = right_balance(root);
        else
            *shorter = 0;
        /* go left */
    } else if (cmp < 0) {
        if ((savedata = ruuavl_delete(&(*root)->uuavl_left, data, fcmp, &shortersubtree)) == 0) {
            *shorter = 0;
            return (0);
        }

        /* left subtree shorter? */
        if (shortersubtree)
            *shorter = left_balance(root);
        else
            *shorter = 0;
        /* go right */
    } else {
        if ((savedata = ruuavl_delete(&(*root)->uuavl_right, data, fcmp, &shortersubtree)) == 0) {
            *shorter = 0;
            return (0);
        }

        if (shortersubtree)
            *shorter = right_balance(root);
        else
            *shorter = 0;
    }

    return (savedata);
}

/*
 * uuavl_delete() - deletes the node containing data (according to fcmp) from
 * the uuavl tree rooted at root.
 */

void *uuavl_delete(struct uuavlnode **root, void *data, UUAVL_CMP fcmp) {
    int shorter;

    return (ruuavl_delete(root, data, fcmp, &shorter));
}

void *UUAvlTreeDelete(struct UUAVLTREE *tree, void *data) {
    int cmpFirst;
    struct uuavlnode *second, *savedata;

    if (tree->root == NULL)
        return NULL;

    cmpFirst = (*tree->avlCmp)(tree->first->uuavl_data, data);
    second = tree->first->uuavl_next;
    if (savedata = uuavl_delete(&tree->root, data, tree->avlCmp)) {
        /* If we are deleting the first node,
           then either the tree is now empty and there is no first,
           or else the node that was previously second is now first
        */
        if (cmpFirst == 0) {
            tree->first = tree->root ? second : NULL;
        }
    }

    return savedata;
}

static int uuavl_inapply(struct uuavlnode *root, UUAVL_APPLY fn, void *arg, int stopflag) {
    if (root == 0)
        return (UUAVL_NOMORE);

    if (root->uuavl_left != 0)
        if (uuavl_inapply(root->uuavl_left, fn, arg, stopflag) == stopflag)
            return (stopflag);

    if ((*fn)(root->uuavl_data, arg) == stopflag)
        return (stopflag);

    if (root->uuavl_right == NULL)
        return (UUAVL_NOMORE);
    else
        return (uuavl_inapply(root->uuavl_right, fn, arg, stopflag));
}

static int uuavl_postapply(struct uuavlnode *root, UUAVL_APPLY fn, void *arg, int stopflag) {
    if (root == 0)
        return (UUAVL_NOMORE);

    if (root->uuavl_left != 0)
        if (uuavl_postapply(root->uuavl_left, fn, arg, stopflag) == stopflag)
            return (stopflag);

    if (root->uuavl_right != 0)
        if (uuavl_postapply(root->uuavl_right, fn, arg, stopflag) == stopflag)
            return (stopflag);

    return ((*fn)(root->uuavl_data, arg));
}

static int uuavl_preapply(struct uuavlnode *root, UUAVL_APPLY fn, void *arg, int stopflag) {
    if (root == 0)
        return (UUAVL_NOMORE);

    if ((*fn)(root->uuavl_data, arg) == stopflag)
        return (stopflag);

    if (root->uuavl_left != 0)
        if (uuavl_preapply(root->uuavl_left, fn, arg, stopflag) == stopflag)
            return (stopflag);

    if (root->uuavl_right == NULL)
        return (UUAVL_NOMORE);
    else
        return (uuavl_preapply(root->uuavl_right, fn, arg, stopflag));
}

/*
 * uuavl_apply -- uuavl tree root is traversed, function fn is called with
 * arguments arg and the data portion of each node.  if fn returns stopflag,
 * the traversal is cut short, otherwise it continues.  Do not use -6 as
 * a stopflag, as this is what is used to indicate the traversal ran out
 * of nodes.
 */

int uuavl_apply(struct uuavlnode *root, UUAVL_APPLY fn, void *arg, int stopflag, int type) {
    switch (type) {
    case UUAVL_INORDER:
        return (uuavl_inapply(root, fn, arg, stopflag));
    case UUAVL_PREORDER:
        return (uuavl_preapply(root, fn, arg, stopflag));
    case UUAVL_POSTORDER:
        return (uuavl_postapply(root, fn, arg, stopflag));
    default:
        fprintf(stderr, "Invalid traversal type %d\n", type);
        return (-1);
    }

    /* NOTREACHED */
}

/*
 * uuavl_prefixapply - traverse uuavl tree root, applying function fprefix
 * to any nodes that match.  fcmp is called with data as its first arg
 * and the current node's data as its second arg.  it should return
 * 0 if they match, < 0 if data is less, and > 0 if data is greater.
 * the idea is to efficiently find all nodes that are prefixes of
 * some key...  Like uuavl_apply, this routine also takes a stopflag
 * and will return prematurely if fmatch returns this value.  Otherwise,
 * UUAVL_NOMORE is returned.
 */

int uuavl_prefixapply(struct uuavlnode *root,
                      void *data,
                      UUAVL_CMP fmatch,
                      void *marg,
                      UUAVL_CMP fcmp,
                      void *carg,
                      int stopflag) {
    int cmp;

    if (root == 0)
        return (UUAVL_NOMORE);

    cmp = (*fcmp)(data, root->uuavl_data /* , carg */);
    if (cmp == 0) {
        if ((*fmatch)(root->uuavl_data, marg) == stopflag)
            return (stopflag);

        if (root->uuavl_left != 0)
            if (uuavl_prefixapply(root->uuavl_left, data, fmatch, marg, fcmp, carg, stopflag) ==
                stopflag)
                return (stopflag);

        if (root->uuavl_right != 0)
            return (uuavl_prefixapply(root->uuavl_right, data, fmatch, marg, fcmp, carg, stopflag));
        else
            return (UUAVL_NOMORE);

    } else if (cmp < 0) {
        if (root->uuavl_left != 0)
            return (uuavl_prefixapply(root->uuavl_left, data, fmatch, marg, fcmp, carg, stopflag));
    } else {
        if (root->uuavl_right != 0)
            return (uuavl_prefixapply(root->uuavl_right, data, fmatch, marg, fcmp, carg, stopflag));
    }

    return (UUAVL_NOMORE);
}

/*
 * uuavl_free -- traverse uuavltree root, freeing the memory it is using.
 * the dfree() is called to free the data portion of each node.  The
 * number of items actually freed is returned.
 */

int uuavl_free(struct uuavlnode *root, UUAVL_FREE dfree) {
    int nleft, nright;

    if (root == 0)
        return (0);

    nleft = nright = 0;
    if (root->uuavl_left != 0)
        nleft = uuavl_free(root->uuavl_left, dfree);

    if (root->uuavl_right != 0)
        nright = uuavl_free(root->uuavl_right, dfree);

    if (dfree)
        (*dfree)(root->uuavl_data);
    free(root);

    return (nleft + nright + 1);
}

int UUAvlTreeFree(struct UUAVLTREE *tree) {
    int result = uuavl_free(tree->root, tree->avlFree);
    tree->root = tree->first = NULL;
    return result;
}

/*
 * uuavl_find -- search uuavltree root for a node with data data.  the function
 * cmp is used to compare things.  it is called with data as its first arg
 * and the current node data as its second.  it should return 0 if they match,
 * < 0 if arg1 is less than arg2 and > 0 if arg1 is greater than arg2.
 */

struct uuavlnode *uuavl_find(struct UUAVLTREE *tree, const void *data) {
    struct uuavlnode *root = tree->root;
    int cmp;

    while (root) {
        cmp = (*tree->avlCmp)(data, root->uuavl_data);
        if (cmp < 0)
            root = root->uuavl_left;
        else if (cmp > 0)
            root = root->uuavl_right;
        else
            break;
    }

    return root;
}

void *UUAvlTreeFind(struct UUAVLTREE *tree, const void *data) {
    struct uuavlnode *root = uuavl_find(tree, data);
    return root ? root->uuavl_data : NULL;
}

static struct uuavlnode *uuavl_find_ge(struct UUAVLTREE *tree, const void *data) {
    struct uuavlnode *root = tree->root;
    int cmp;

    while (root) {
        cmp = (*tree->avlCmp)(data, root->uuavl_data);
        if (cmp < 0) {
            /* If we can go no further left, then this is the GT node */
            if (root->uuavl_left == NULL)
                break;
            root = root->uuavl_left;
        } else if (cmp > 0) {
            /* If we can go no further right, then the next node in line is the GT node,
               unless it happens to be the first node and we're wrapping back
            */
            if (root->uuavl_right == NULL) {
                root = root->uuavl_next;
                if (root == tree->first)
                    return NULL;
                break;
            }
            root = root->uuavl_right;
        } else {
            /* This is the EQ node */
            break;
        }
    }

    return root;
}

void *UUAvlTreeFindGE(struct UUAVLTREE *tree, const void *data) {
    struct uuavlnode *root = uuavl_find_ge(tree, data);

    return root ? root->uuavl_data : NULL;
}

/*
 * uuavl_find_lin -- search uuavltree root linearly for a node with data data.
 * the function cmp is used to compare things.  it is called with data as its
 * first arg and the current node data as its second.  it should return 0 if
 * they match, non-zero otherwise.
 */

void *uuavl_find_lin(struct uuavlnode *root, const void *data, UUAVL_CMP fcmp) {
    void *res;

    if (root == 0)
        return (NULL);

    if ((*fcmp)(data, root->uuavl_data) == 0)
        return (root->uuavl_data);

    if (root->uuavl_left != 0)
        if ((res = uuavl_find_lin(root->uuavl_left, data, fcmp)) != NULL)
            return (res);

    if (root->uuavl_right == NULL)
        return (NULL);
    else
        return (uuavl_find_lin(root->uuavl_right, data, fcmp));
}

int UUAvlTreeDupGeneric(char *name, void *v1, void *v2) {
    Log("Attempt to insert duplicate key into %s", name);
    PANIC;
    return 0;
}

void UUAvlTreeFreeGeneric(void *v) {
    return;
}

void UUAvlTreeInit(struct UUAVLTREE *tree,
                   char *name,
                   UUAVL_CMP avlCmp,
                   UUAVL_DUP avlDup,
                   UUAVL_FREE avlFree) {
    tree->root = NULL;
    tree->first = NULL;

    if (avlCmp == NULL) {
        Log("Compare routine may not be null for %s", name);
        PANIC;
    }

    if (!(tree->name = strdup(name)))
        PANIC;

    tree->avlCmp = avlCmp;
    tree->avlDup = avlDup ? avlDup : UUAvlTreeDupGeneric;
    tree->avlFree = avlFree ? avlFree : UUAvlTreeFreeGeneric;
}

/* NON-REENTRANT INTERFACE */

static void **uuavl_list;
static int uuavl_maxlist;
static int uuavl_nextlist;

#define UUAVL_GRABSIZE 100

/* ARGSUSED */
static int uuavl_buildlist(void *data, void *arg) {
    static int slots;

    if (uuavl_list == (void **)0) {
        if (!(uuavl_list = (void **)malloc(UUAVL_GRABSIZE * sizeof(void *))))
            PANIC;
        slots = UUAVL_GRABSIZE;
        uuavl_maxlist = 0;
    } else if (uuavl_maxlist == slots) {
        slots += UUAVL_GRABSIZE;
        uuavl_list = (void **)realloc((char *)uuavl_list, (unsigned)slots * sizeof(void *));
    }

    uuavl_list[uuavl_maxlist++] = data;

    return (0);
}

/*
 * uuavl_getfirst() and uuavl_getnext() are provided as alternate tree
 * traversal methods, to be used when a single function cannot be
 * provided to be called with every node in the tree.  uuavl_getfirst()
 * traverses the tree and builds a linear list of all the nodes,
 * returning the first node.  uuavl_getnext() returns the next thing
 * on the list built by uuavl_getfirst().  This means that uuavl_getfirst()
 * can take a while, and that the tree should not be messed with while
 * being traversed in this way, and that multiple traversals (even of
 * different trees) cannot be active at once.
 */

void *uuavl_getfirst(struct uuavlnode *root) {
    if (uuavl_list) {
        free((char *)uuavl_list);
        uuavl_list = (void **)0;
    }
    uuavl_maxlist = 0;
    uuavl_nextlist = 0;

    if (root == 0)
        return (0);

    (void)uuavl_apply(root, uuavl_buildlist, (void *)0, -1, UUAVL_INORDER);

    return (uuavl_list[uuavl_nextlist++]);
}

void *uuavl_getnext(void) {
    if (uuavl_list == 0)
        return (0);

    if (uuavl_nextlist == uuavl_maxlist) {
        free((void *)uuavl_list);
        uuavl_list = (void **)0;
        return (0);
    }

    return (uuavl_list[uuavl_nextlist++]);
}

/* end non-reentrant code */

int uuavl_dup_error(void *left, void *right) {
    return (-1);
}

int uuavl_dup_ok(void *left, void *right) {
    return (0);
}

int UUAvlTreeCmpExercise(const void *v1, const void *v2) {
    int *i1 = (int *)v1;
    int *i2 = (int *)v2;

    return *i1 - *i2;
}

void *UUAvlTreeIterFirst(struct UUAVLTREE *tree, UUAVLTREEITERCTX *uatic) {
    if (tree->first == NULL)
        return NULL;

    *uatic = tree->first;

    return (*uatic)->uuavl_data;
}

void *UUAvlTreeIterNext(struct UUAVLTREE *tree, UUAVLTREEITERCTX *uatic) {
    *uatic = (*uatic)->uuavl_next;

    return *uatic == tree->first ? NULL : (*uatic)->uuavl_data;
}

void *UUAvlTreeIterLast(struct UUAVLTREE *tree, UUAVLTREEITERCTX *uatic) {
    if (tree->first == NULL)
        return NULL;

    *uatic = tree->first->uuavl_prev;

    return (*uatic)->uuavl_data;
}

void *UUAvlTreeIterPrev(struct UUAVLTREE *tree, UUAVLTREEITERCTX *uatic) {
    *uatic = (*uatic)->uuavl_prev;

    return *uatic == tree->first->uuavl_prev ? NULL : (*uatic)->uuavl_data;
}

void *UUAvlTreeIterFind(struct UUAVLTREE *tree, UUAVLTREEITERCTX *uatic, void *data) {
    struct uuavlnode *root = tree->root;
    int cmp;

    while (root) {
        cmp = (*tree->avlCmp)(data, root->uuavl_data);
        if (cmp < 0)
            root = root->uuavl_left;
        else if (cmp > 0)
            root = root->uuavl_right;
        else
            break;
    }

    *uatic = root;
    return (root ? root->uuavl_data : NULL);
}

void *UUAvlTreeIterFindGE(struct UUAVLTREE *tree, UUAVLTREEITERCTX *uatic, const void *data) {
    *uatic = uuavl_find_ge(tree, data);
    return *uatic ? (*uatic)->uuavl_data : NULL;
}

void UUAvlTreeExercise(void) {
    struct UUAVLTREE avlExercise;
    int *p = NULL;
    int i;
    int count = 0;
    UUAVLTREEITERCTX uatic;

    UUAvlTreeInit(&avlExercise, "avlExercise", UUAvlTreeCmpExercise, NULL, NULL);

    for (i = 200; i <= 299; i += 2) {
        if (!(p = (int *)calloc(1, sizeof(*p))))
            PANIC;
        *p = i * 10;
        UUAvlTreeInsert(&avlExercise, p);
    }

    for (i = 899; i >= 800; i -= 2) {
        if (!(p = (int *)calloc(1, sizeof(*p))))
            PANIC;
        *p = i * 10;
        UUAvlTreeInsert(&avlExercise, p);
    }

    for (i = 0; i < 100; i++) {
        if (!(p = (int *)calloc(1, sizeof(*p))))
            PANIC;
        *p = i * 10;
        UUAvlTreeInsert(&avlExercise, p);
        if (!(p = (int *)calloc(1, sizeof(*p))))
            PANIC;
        *p = (999 - i) * 10;
        UUAvlTreeInsert(&avlExercise, p);
    }

    for (i = 898; i >= 800; i -= 2) {
        if (!(p = (int *)calloc(1, sizeof(*p))))
            PANIC;
        *p = i * 10;
        UUAvlTreeInsert(&avlExercise, p);
    }

    for (i = 201; i <= 299; i += 2) {
        if (!(p = (int *)calloc(1, sizeof(*p))))
            PANIC;
        *p = i * 10;
        UUAvlTreeInsert(&avlExercise, p);
    }

    /*
    for (i = 0; i < 10000; i++) {
        if (!(p = (int *) calloc(1, sizeof(*p)))) PANIC;
        *p = rand();
        UUAvlTreeInsert(&avlExercise, p);
    }
    */

    for (i = 0; i < 10000; i += 15) {
        UUAvlTreeDelete(&avlExercise, &i);
    }

    UUAvlTreeFind(&avlExercise, p);

    if (avlExercise.root) {
        int last = -1;
        for (p = (int *)UUAvlTreeIterFirst(&avlExercise, &uatic); p;
             p = (int *)UUAvlTreeIterNext(&avlExercise, &uatic)) {
            count++;
            printf("%d ", *p);
            if (last >= 0 && last >= *p) {
                printf("!!!");
            }
            last = *p;
        }
    }
    printf("\ncount %d\n", count);
}
