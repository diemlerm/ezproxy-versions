#ifndef __USRCOMMON_H__
#define __USRCOMMON_H__

#include "common.h"

enum FINDUSERRESULT FindUserCommon(struct TRANSLATE *t,
                                   struct USERINFO *uip,
                                   char *file,
                                   int f,
                                   struct FILEREADLINEBUFFER *frb,
                                   struct sockaddr_storage *pInterface);

#endif  // __USRCOMMON_H__
