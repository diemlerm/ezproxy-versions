#ifndef __USRTLC_H__
#define __USRTLC_H__

#include "common.h"

enum FINDUSERRESULT FindUserTLC(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                char *file,
                                int f,
                                struct FILEREADLINEBUFFER *frb,
                                struct sockaddr_storage *pInterface);

#endif  // __USRTLC_H__
