#define __UUFILE__ "adminaccountreset.c"

#include "common.h"
#include "uustring.h"
#include "usr.h"

#include "adminaccountreset.h"

void AdminAccountReset(struct TRANSLATE *t, char *query, char *post) {
    const int AAAUSER = 0;
    struct FORMFIELD ffs[] = {
        {"user", NULL, 0, 0, MAXUSERLEN},
        {NULL,   NULL, 0, 0, 0         }
    };
    struct UUSOCKET *s = &t->ssocket;
    enum ACCOUNTFINDRESULT accountResult;

    AdminHeader(t, 0, "Account Reset");

    FindFormFields(query, post, ffs);
    if (ffs[AAAUSER].value && strlen(ffs[AAAUSER].value) > 0) {
        accountResult = AccountFind(ffs[AAAUSER].value, NULL, accountDefaultPassword,
                                    accountDefaultPassword, 0);
        if (accountResult < accountWrong)
            UUSendZ(s, "Password reset to default.\n");
        else
            UUSendZ(s, "Account does not exist.\n");
        return;
    } else {
        SendFile(t, ARESETHTM, SFREPORTIFMISSING);
    }

    AdminFooter(t, 0);
}
