#define __UUFILE__ "efparsename.c"

#define EXPRESSION_INTERNAL_FUNCTIONS

#include "common.h"
#include "usr.h"
#include "usrfinduser.h"
#include "expression.h"
#include "variables.h"
#include "efparsename.h"

const char *parseNameTitlesVar = "ParseName:titles";
const char *parseNamePrefixesVar = "ParseName:prefixes";
const char *parseNameSuffixesVar = "ParseName:suffixes";

struct NAMESUBPART {
    char *subpart;
    BOOL used;
    BOOL title;
    BOOL prefix;
    BOOL suffix;
};

#define NAMEMAX 6
#define NAMESUBPARTMAX 20

char *ExpressionVariableParseName(struct EXPRESSIONCTX *ec, char *varIndex) {
    char *val = VariablesGetValue(ec->t->localVariables, varIndex);

    if (val == NULL) {
        if (strcmp(varIndex, parseNameTitlesVar) == 0)
            val = "Mr.,Miss,Ms.,Mrs.,Dr.";
        else if (strcmp(varIndex, parseNamePrefixesVar) == 0) {
            val = "da,dal,de,del,der,e,la,le,san,st.,ste.,van,vel,von";
        } else if (strcmp(varIndex, parseNameSuffixesVar) == 0) {
            val = "Junior,Jr.,Senior,Sr.,Esquire,Esq.,II,III,IV,2,2nd,3,3rd,4,4th";
        }
        if (val) {
            if (!(val = strdup(val)))
                PANIC;
        }
    }

    return val;
}

static char *ExpressionFuncParseNameMakeVar(const char *ns, const char *var) {
    char *p;

    if (!(p = malloc(strlen(ns) + strlen(var) + 2)))
        PANIC;
    sprintf(p, "%s:%s", ns, var);
    return p;
}

static BOOL ExpressionFuncParseNameSubstring(const char *str, const char *subs) {
    char *p;

    p = StrIStr(subs, str);

    if (p == NULL)
        return FALSE;

    /* If we are beyond the first char, then the prior char must be a comma for this to be a match
     */
    if (p > subs && *(p - 1) != ',')
        return FALSE;

    p = p + strlen(str);

    if (*p == 0 || *p == ',' || *p == '.')
        return TRUE;

    return FALSE;
}

static void ExpressionFuncNameParseDeleteSubpart(struct NAMESUBPART *nsp, int idx, int *max) {
    int bytes = sizeof(*nsp) * (*max - idx - 1);

    if (bytes > 0)
        memmove(nsp + idx, nsp + idx + 1, bytes);

    *max = *max - 1;
}

static void ExpressionFuncParseNameJoinSubparts(struct NAMESUBPART *nsp,
                                                int start,
                                                int end,
                                                int *max) {
    int bytes = sizeof(*nsp) * (*max - end);
    int i;

    for (i = start + 1; i < end; i++) {
        char *p = (nsp + i)->subpart;
        *(p - 1) = ' ';
    }
    if (bytes > 0)
        memmove(nsp + start + 1, nsp + end, bytes);
    *max = *max - (end - start);
}

char *ExpressionFuncParseName(struct EXPRESSIONCTX *ec) {
    char *titleVar = "title";
    char *forenameVar = "forename";
    char *middleNameVar = "middleName";
    char *surnameVar = "surname";
    char *suffixVar = "nameSuffix";

    char *name = NULL;
    char *format = NULL;
    char *ns = NULL;

    char *namePart[NAMEMAX];
    int nameParts = 1;
    char *formatPart[NAMEMAX];
    int formatParts = 1;
    int nameSubparts;
    struct NAMESUBPART nameSubpart[NAMESUBPARTMAX], *nsp;
    char *p;
    int i;
    int confidence = 0;
    int title, fore, middle, sur, suffix;
    int findSubparts = 0;

    memset(&nameSubpart, 0, sizeof(nameSubpart));

    name = ExpressionExpression(ec, TRUE);
    if (!ExpressionCheckToken(ec, COMMA))
        goto Finished;

    format = ExpressionExpression(ec, TRUE);
    if (format == NULL)
        goto Finished;

    if (ExpressionCheckToken(ec, COMMA)) {
        ns = ExpressionExpression(ec, TRUE);
        if (ns == NULL)
            goto Finished;
        if (*ns == 0) {
            FreeThenNull(ns);
        } else {
            titleVar = ExpressionFuncParseNameMakeVar(ns, titleVar);
            forenameVar = ExpressionFuncParseNameMakeVar(ns, forenameVar);
            middleNameVar = ExpressionFuncParseNameMakeVar(ns, middleNameVar);
            surnameVar = ExpressionFuncParseNameMakeVar(ns, surnameVar);
            suffixVar = ExpressionFuncParseNameMakeVar(ns, suffixVar);
        }
    }

    if (StrIStr(format, "K") == NULL) {
        VariablesDelete(ec->t->localVariables, titleVar);
        VariablesDelete(ec->t->localVariables, forenameVar);
        VariablesDelete(ec->t->localVariables, middleNameVar);
        VariablesDelete(ec->t->localVariables, surnameVar);
        VariablesDelete(ec->t->localVariables, suffixVar);

        if (ns && strcmp(ns, "session") == 0 && ec->t->session &&
            ec->t->session->sessionVariables) {
            VariablesDelete(ec->t->session->sessionVariables, titleVar);
            VariablesDelete(ec->t->session->sessionVariables, forenameVar);
            VariablesDelete(ec->t->session->sessionVariables, middleNameVar);
            VariablesDelete(ec->t->session->sessionVariables, surnameVar);
            VariablesDelete(ec->t->session->sessionVariables, suffixVar);
        }
    }

    namePart[0] = name;
    nameParts = 1;

    for (p = name; p = strchr(p, ',');) {
        *p++ = 0;
        if (nameParts < NAMEMAX)
            namePart[nameParts++] = p;
    }

    formatPart[0] = format;
    formatParts = 1;

    for (p = format; p = strchr(p, ',');) {
        *p++ = 0;
        if (formatParts < NAMEMAX)
            formatPart[formatParts++] = p;
    }

    for (i = 0; i < nameParts && i < formatParts; i++) {
        Trim(namePart[i], TRIM_COMPRESS);
        if (*namePart[i] == 0)
            continue;
        title = fore = middle = sur = suffix = -1;
        for (p = formatPart[i], findSubparts = 0; *p;) {
            int oldFindSubparts = findSubparts;

            if (islower(*p))
                *p = toupper(*p);

            switch (*p) {
            /* Title can only appear in initial position */
            case 'T':
                if (title == -1 && findSubparts == 0)
                    title = findSubparts++;
                break;
            case 'F':
                if (fore == -1)
                    fore = findSubparts++;
                break;
            case 'M':
                if (middle == -1)
                    middle = findSubparts++;
                break;
            case 'S':
                if (sur == -1)
                    sur = findSubparts++;
                break;
            /* Suffix can only appear in terminal position */
            case 'X':
                if (suffix == -1 && *(p + 1) == 0)
                    suffix = findSubparts++;
                break;
            default:
                UsrLog(ec->uip,
                       "Unrecognized character ('%c' %02X) in ParseName is being ignored, warning",
                       *p, *p);
                break;
            }

            /* If nothing matched, remove this character, but if it did match, move to next
             * character */
            if (findSubparts == oldFindSubparts)
                StrCpyOverlap(p, p + 1);
            else
                p++;
        }

        if (findSubparts == 0)
            continue;

        if (findSubparts == 1) {
            if (title != -1)
                VariablesSetValue(ec->t->localVariables, titleVar, namePart[i]);
            if (fore != -1)
                VariablesSetValue(ec->t->localVariables, forenameVar, namePart[i]);
            if (middle != -1)
                VariablesSetValue(ec->t->localVariables, middleNameVar, namePart[i]);
            if (sur != -1)
                VariablesSetValue(ec->t->localVariables, surnameVar, namePart[i]);
            if (suffix != -1)
                VariablesSetValue(ec->t->localVariables, suffixVar, namePart[i]);
            continue;
        }

        nameSubpart[0].subpart = namePart[i];
        nameSubparts = 1;

        nsp = &nameSubpart[0];

        for (p = namePart[i]; p = strchr(p, ' ');) {
            *p++ = 0;
            nameSubpart[nameSubparts++].subpart = p;
            nsp++;
        }

        /* Only allow title and suffix stripping if we have have more than one
           part remaining or if we are looking for just one part
        */
        if (title != -1 && (nameSubparts > 1 || findSubparts == 1)) {
            char *titles = ExpressionVariable(ec, parseNameTitlesVar, NULL, FALSE);
            if (titles) {
                nsp = &nameSubpart[0];
                if (ExpressionFuncParseNameSubstring(nsp->subpart, titles)) {
                    VariablesSetValue(ec->t->localVariables, titleVar, nsp->subpart);
                    ExpressionFuncNameParseDeleteSubpart(nameSubpart, 0, &nameSubparts);
                }
                FreeThenNull(titles);
            }
            findSubparts--;
        }

        if (suffix != -1 && (nameSubparts > 1 || findSubparts == 1)) {
            char *suffixes = ExpressionVariable(ec, parseNameSuffixesVar, NULL, FALSE);
            if (suffixes) {
                nsp = &nameSubpart[nameSubparts - 1];
                if (ExpressionFuncParseNameSubstring(nsp->subpart, suffixes)) {
                    VariablesSetValue(ec->t->localVariables, suffixVar, nsp->subpart);
                    ExpressionFuncNameParseDeleteSubpart(nameSubpart, nameSubparts - 1,
                                                         &nameSubparts);
                }
                FreeThenNull(suffixes);
            }
            findSubparts--;
        }

        /* If there is only one remaining part wanted,
           or if there is only one remaining part,
           assign to one variable by priority
        */
        if (findSubparts == 1 || nameSubparts == 1) {
            /* Rejoin remaining subparts */
            ExpressionFuncParseNameJoinSubparts(nameSubpart, 0, nameSubparts, &nameSubparts);

            /* assign based on priority for the one part remaining scenario;
               hence, the sur, fore, middle sequencing
            */
            if (sur != -1)
                VariablesSetValue(ec->t->localVariables, surnameVar, nameSubpart[0].subpart);
            else if (fore != -1)
                VariablesSetValue(ec->t->localVariables, forenameVar, nameSubpart[0].subpart);
            else if (middle != -1)
                VariablesSetValue(ec->t->localVariables, middleNameVar, nameSubpart[0].subpart);
            continue;
        }

        /* starting possible patterns
           fm, fs, fms, fsm
           mf, ms, mfs, msf
           sf, sm, sfm, smf
        */

        /* if no request for surname, make middle name greedy */
        if (sur == -1) {
            /* fm */
            if (fore < middle) {
                ExpressionFuncParseNameJoinSubparts(nameSubpart, 1, nameSubparts, &nameSubparts);
                fore = 0;
                middle = 1;
            } else {
                /* mf */
                ExpressionFuncParseNameJoinSubparts(nameSubpart, 0, nameSubparts - 1,
                                                    &nameSubparts);
                middle = 0;
                fore = 1;
            }
        } else {
            /* to reach here, we want a surname */

            /* if there are only two remaining subparts, and we want both fore and middle,
               middle loses
            */
            if (nameSubparts == 2 && fore != -1 && middle != -1)
                middle = -1;

            if (middle == -1) {
                if (fore < sur) {
                    /* fs */
                    ExpressionFuncParseNameJoinSubparts(nameSubpart, 1, nameSubparts,
                                                        &nameSubparts);
                    fore = 0;
                    sur = 1;
                } else {
                    /* sf */
                    ExpressionFuncParseNameJoinSubparts(nameSubpart, 0, nameSubparts - 1,
                                                        &nameSubparts);
                    sur = 0;
                    fore = 1;
                }
            } else if (fore == -1) {
                if (middle < sur) {
                    /* ms */
                    ExpressionFuncParseNameJoinSubparts(nameSubpart, 1, nameSubparts,
                                                        &nameSubparts);
                    middle = 0;
                    sur = 1;
                } else {
                    /* sm */
                    ExpressionFuncParseNameJoinSubparts(nameSubpart, 0, nameSubparts - 1,
                                                        &nameSubparts);
                    sur = 0;
                    middle = 1;
                }
            } else if (sur < fore && sur < middle) {
                /* sur at beginning */
                ExpressionFuncParseNameJoinSubparts(nameSubpart, 0, nameSubparts - 2,
                                                    &nameSubparts);
                sur = 0;
                if (fore < middle) {
                    /* sfm */
                    fore = 1;
                    middle = 2;
                } else {
                    /* smf */
                    middle = 1;
                    fore = 2;
                }
            } else if (sur > fore && sur > middle) {
                /* sur at end */
                ExpressionFuncParseNameJoinSubparts(nameSubpart, 2, nameSubparts, &nameSubparts);
                sur = 2;
                if (fore < middle) {
                    /* fms */
                    fore = 0;
                    middle = 1;
                } else {
                    /* mfs */
                    middle = 0;
                    fore = 1;
                }
            } else {
                /* otherwise sur in the middle */
                ExpressionFuncParseNameJoinSubparts(nameSubpart, 1, nameSubparts - 1,
                                                    &nameSubparts);
                sur = 1;
                if (fore < middle) {
                    /* fsm */
                    fore = 0;
                    middle = 2;
                } else {
                    /* msf */
                    middle = 0;
                    fore = 2;
                }
            }
        }

        if (fore != -1)
            VariablesSetValue(ec->t->localVariables, forenameVar, nameSubpart[fore].subpart);
        if (middle != -1)
            VariablesSetValue(ec->t->localVariables, middleNameVar, nameSubpart[middle].subpart);
        if (sur != -1)
            VariablesSetValue(ec->t->localVariables, surnameVar, nameSubpart[sur].subpart);
    }

Finished:
    if (ns) {
        FreeThenNull(titleVar);
        FreeThenNull(forenameVar);
        FreeThenNull(middleNameVar);
        FreeThenNull(surnameVar);
        FreeThenNull(suffixVar);
        FreeThenNull(ns);
    }
    FreeThenNull(name);
    FreeThenNull(format);

    return ExpressionInt(confidence);
}
