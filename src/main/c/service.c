#define __UUFILE__ "service.c"
#ifdef WIN32

/* THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF */
/* ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO */
/* THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A */
/* PARTICULAR PURPOSE. */
//
/* Copyright (C) 1993-1996  Microsoft Corporation.  All Rights Reserved. */
//
/*  MODULE:   service.c */
//
/*  PURPOSE:  Implements functions required by all services */
/*            windows. */
//
/*  FUNCTIONS: */
/*    main(int argc, char **argv); */
/*    service_ctrl(DWORD dwCtrlCode); */
/*    service_main(DWORD dwArgc, LPTSTR *lpszArgv); */
/*    CmdInstallService(); */
/*    CmdRemoveService(); */
/*    CmdDebugService(int argc, char **argv); */
/*    ControlHandler ( DWORD dwCtrlType ); */
/*    GetLastErrorText( LPTSTR lpszBuf, DWORD dwSize ); */
//
/*  COMMENTS: */
//
/*  AUTHOR: Craig Link - Microsoft Developer Support */
//

#include "common.h"

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <process.h>
#include <tchar.h>
#include <stdarg.h>

#include "service.h"

HANDLE mStopPending = NULL;

#define EZPROXYSVC "ezproxy.svc"

char *myServiceName;
char *myServiceDisplayName;

/* internal variables */
SERVICE_STATUS ssStatus; /* current status of the service */
SERVICE_STATUS_HANDLE sshStatusHandle;
DWORD dwErr = 0;
BOOL bDebug = FALSE;
TCHAR szErr[256];
typedef u_short PORT;
char startupDirectory[MAX_PATH];

/* internal function prototypes */
VOID WINAPI service_ctrl(DWORD dwCtrlCode);
VOID WINAPI service_main(DWORD dwArgc, LPTSTR *lpszArgv);
VOID CmdInstallService();
VOID CmdRemoveService();
VOID CmdListServices();
VOID CmdDebugService(int argc, char **argv);
BOOL WINAPI ControlHandler(DWORD dwCtrlType);
LPTSTR GetLastErrorText(LPTSTR lpszBuf, DWORD dwSize);
void CheckArguments(int argc,
                    char **argv,
                    int *ifmode,
                    char *ifsubtype,
                    char **checkPort,
                    char **guardianName,
                    BOOL started);

//
/*  FUNCTION: main */
//
/*  PURPOSE: entrypoint for service */
//
/*  PARAMETERS: */
/*    argc - number of command line arguments */
/*    argv - array of command line arguments */
//
/*  RETURN VALUE: */
/*    none */
//
/*  COMMENTS: */
/*    main() either performs the command line task, or */
/*    call StartServiceCtrlDispatcher to register the */
/*    main service thread.  When the this call returns, */
/*    the service has stopped, so exit. */
//
#ifndef UNITTESTING
int main(int argc, char **argv) {
    SERVICE_TABLE_ENTRY dispatchTable[] = {
        {NULL, (LPSERVICE_MAIN_FUNCTION)service_main},
        {NULL, NULL                                 }
    };
    int ifmode;
    char ifsubtype;
    char *checkPort = NULL;
    char *guardianName;

    mStopPending = CreateMutex(NULL, FALSE, NULL);
    if (mStopPending == NULL)
        PANIC;

    /* The mutexes must be initialized early since CheckArguments may use the mTimeFuncs mutex */
    InitMutexes();

    GetMyBinaryAndMyPath(argv[0]);

    ifmode = 0;
    checkPort = NULL;

    InstalledAsService(NULL);

    CheckArguments(argc, argv, &ifmode, &ifsubtype, &checkPort, &guardianName, 0);
    if (InstalledAsService(NULL) == 0 || ifmode != 0 || checkPort != NULL || guardianName != NULL) {
        ServiceStart(argc, argv, 0);
        exit(1);
    }

    if (argc > 1 && stricmp("-sd", argv[1]) == 0) {
        bDebug = TRUE;
        CmdDebugService(argc, argv);
        exit(0);
    }

    /* if it doesn't match any of the above parameters */
    /* the service control manager may be starting the service */
    /* so we must call StartServiceCtrlDispatcher */
    /* this is just to be friendly */
    printf("\n");
    printf("%s is configured to run as a service\n", SZAPPNAME);
    printf("If you no longer want it to run as a service, use the command:\n");
    printf("     %s -sr\n", argv[0]);
    printf("\n");
    printf("To exit this program, please press CTRL/C\n");

    dispatchTable[0].lpServiceName = myServiceName;

    if (!StartServiceCtrlDispatcher(dispatchTable))
        AddToMessageLog("StartServiceCtrlDispatcher failed *%d*.", GetLastError());
}
#endif

//
/*  FUNCTION: service_main */
//
/*  PURPOSE: To perform actual initialization of the service */
//
/*  PARAMETERS: */
/*    dwArgc   - number of command line arguments */
/*    lpszArgv - array of command line arguments */
//
/*  RETURN VALUE: */
/*    none */
//
/*  COMMENTS: */
/*    This routine performs the service initialization and then calls */
/*    the user defined ServiceStart() routine to perform majority */
/*    of the work. */
//
void WINAPI service_main(DWORD dwArgc, LPTSTR *lpszArgv) {
    /* register our service control handler: */
    //
#pragma warning(push)
#pragma warning(disable : 4024 4028)
    sshStatusHandle = RegisterServiceCtrlHandler(myServiceName, (LPHANDLER_FUNCTION)service_ctrl);
#pragma warning(pop)

    if (!sshStatusHandle) {
        AddToMessageLog("registerserivce failed %d", GetLastError());
        goto cleanup;
    }

    /* SERVICE_STATUS members that don't change in example */
    //
    ssStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    ssStatus.dwServiceSpecificExitCode = 0;

    /* report the status to the service control manager. */
    //
    if (!ReportStatusToSCMgr(SERVICE_START_PENDING, /* service state */
                             NO_ERROR,              /* exit code */
                             3000))                 /* wait hint */
        goto cleanup;

    ServiceStart(dwArgc, lpszArgv, 1);

cleanup:

    /* Wait for Guardian to terminate any charge process and clean itself up before we terminate */
    WaitForSingleObject(mStopPending, 30000);

    /* try to report the stopped status to the service control manager. */
    //
    if (sshStatusHandle)
        (VOID) ReportStatusToSCMgr(SERVICE_STOPPED, dwErr, 0);

    return;
}

//
/*  FUNCTION: service_ctrl */
//
/*  PURPOSE: This function is called by the SCM whenever */
/*           ControlService() is called on this service. */
//
/*  PARAMETERS: */
/*    dwCtrlCode - type of control requested */
//
/*  RETURN VALUE: */
/*    none */
//
/*  COMMENTS: */
//
VOID WINAPI service_ctrl(DWORD dwCtrlCode) {
    /* Handle the requested control code. */
    //
    switch (dwCtrlCode) {
    /* Stop the service. */
    //
    /* SERVICE_STOP_PENDING should be reported before */
    /* setting the Stop Event - hServerStopEvent - in */
    /* ServiceStop().  This avoids a race condition */
    /* which may result in a 1053 - The Service did not respond... */
    /* error. */
    case SERVICE_CONTROL_STOP:
        ReportStatusToSCMgr(SERVICE_STOP_PENDING, NO_ERROR, 0);
        ServiceStop();
        return;

    /* Update the service status. */
    //
    case SERVICE_CONTROL_INTERROGATE:
        break;

    /* invalid control code */
    //
    default:
        break;
    }

    ReportStatusToSCMgr(ssStatus.dwCurrentState, NO_ERROR, 0);
}

//
/*  FUNCTION: ReportStatusToSCMgr() */
//
/*  PURPOSE: Sets the current status of the service and */
/*           reports it to the Service Control Manager */
//
/*  PARAMETERS: */
/*    dwCurrentState - the state of the service */
/*    dwWin32ExitCode - error code to report */
/*    dwWaitHint - worst case estimate to next checkpoint */
//
/*  RETURN VALUE: */
/*    TRUE  - success */
/*    FALSE - failure */
//
/*  COMMENTS: */
//
BOOL ReportStatusToSCMgr(DWORD dwCurrentState, DWORD dwWin32ExitCode, DWORD dwWaitHint) {
    static DWORD dwCheckPoint = 1;
    BOOL fResult = TRUE;

    if (!bDebug) /* when debugging we don't report to the SCM */
    {
        if (dwCurrentState == SERVICE_START_PENDING)
            ssStatus.dwControlsAccepted = 0;
        else
            ssStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;

        ssStatus.dwCurrentState = dwCurrentState;
        ssStatus.dwWin32ExitCode = dwWin32ExitCode;
        ssStatus.dwWaitHint = dwWaitHint;

        if ((dwCurrentState == SERVICE_RUNNING) || (dwCurrentState == SERVICE_STOPPED))
            ssStatus.dwCheckPoint = 0;
        else
            ssStatus.dwCheckPoint = dwCheckPoint++;

        /* Report the status of the service to the service control manager. */
        //
        if (!(fResult = SetServiceStatus(sshStatusHandle, &ssStatus))) {
            AddToMessageLog("SetServiceStatus");
        }
    }
    return fResult;
}

//
/*  FUNCTION: AddToMessageLog(LPTSTR lpszMsg) */
//
/*  PURPOSE: Allows any thread to log an error message */
//
/*  PARAMETERS: */
/*    lpszMsg - text for message */
//
/*  RETURN VALUE: */
/*    none */
//
/*  COMMENTS: */
//
VOID AddToMessageLog(char *format, ...) {
    TCHAR szMsg[1024];
    HANDLE hEventSource;
    LPCSTR lpszStrings[2];
    BOOL err;
    WORD wType = EVENTLOG_ERROR_TYPE;
    DWORD eventID = 0;
    /*
    EVENTLOG_ERROR_TYPE
    EVENTLOG_WARNING_TYPE
    EVENTLOG_INFORMATION_TYPE
    EVENTLOG_AUDIT_SUCCESS
    EVENTLOG_AUDIT_FAILURE
    */

    va_list ap;
    char buffer[256];

    memset(buffer, 0, sizeof(buffer) - 1);
    va_start(ap, format);
    vsprintf(buffer, format, ap);
    va_end(ap);

    if (bDebug) {
        printf("%s\n", buffer);
    } else {
        dwErr = GetLastError();

        /* Use event logging to log the error. */
        //
        hEventSource = RegisterEventSource(NULL, myServiceName);
        _stprintf(szMsg, TEXT("%d"), myServiceName, dwErr);
        lpszStrings[0] = buffer;
        lpszStrings[1] = szMsg;

        if (hEventSource != NULL) {
            err = ReportEvent(hEventSource, /* handle of event source */
                              wType,        /* event type */
                              0,            /* event category */
                              eventID,      /* event ID */
                              NULL,         /* current user's SID */
                              1,            /* changed from 2 to 1 strings in lpszStrings */
                              0,            /* no bytes of raw data */
                              lpszStrings,  /* array of error strings */
                              NULL);        /* no raw data */
            (VOID) DeregisterEventSource(hEventSource);
        }
    }
}

/*///////////////////////////////////////////////////////////////// */
//
/*  The following code handles service installation and removal */
//

//
/*  FUNCTION: CmdInstallService() */
//
/*  PURPOSE: Installs the service */
//
/*  PARAMETERS: */
/*    none */
//
/*  RETURN VALUE: */
/*    none */
//
/*  COMMENTS: */
//
void CmdInstallService(BOOL iisDependent) {
    SC_HANDLE schService;
    SC_HANDLE schSCManager;
    HKEY hkey;
    LONG err;
    char keyname[256];
    DWORD typesSupported;
    int i;
    char name[16];
    struct INSTALLEDNAMES *in;
    char yes[4];

    if (InstalledAsService(&in)) {
        printf("EZproxy is already installed as a service.\n");
        exit(0);
    }

    if (in) {
        printf("There is at least one copy of " EZPROXYNAMEPROPER
               " installed to run as a service\nfrom another directory.\n\n");
        printf("From the command prompt, you can see a list of all copies of " EZPROXYNAMEPROPER
               "\nthat are installed to run as a service with \"ezproxy -sl\"\n\n");
        printf("To create an additional " EZPROXYNAMEPROPER
               " service from this directory, type YES: ");
        fgets(yes, sizeof(yes), stdin);
        printf("\n");
        if (yes[0] != 'Y' && yes[0] != 'y') {
            printf("Service installation cancelled.\n");
            exit(0);
        }
    }

    schSCManager = OpenSCManager(NULL,                 /* machine (NULL == local) */
                                 NULL,                 /* database (NULL == default) */
                                 SC_MANAGER_ALL_ACCESS /* access required */
    );

    if (schSCManager == NULL) {
        printf("OpenSCManager failed - %s\n", GetLastErrorText(szErr, 256));
        exit(1);
    }

    for (strcpy(name, EZPROXYNAMEPROPER), i = 0;; sprintf(name, EZPROXYNAMEPROPER "%d", ++i)) {
        schService =
            CreateService(schSCManager,                            /* SCManager database */
                          name,                                    /* name of service */
                          name,                                    /* name to display */
                          SERVICE_ALL_ACCESS,                      /* desired access */
                          SERVICE_WIN32_OWN_PROCESS,               /* service type */
                          SERVICE_AUTO_START,                      /* start type */
                          SERVICE_ERROR_NORMAL,                    /* error control type */
                          myBinary,                                /* service's binary */
                          NULL,                                    /* no load ordering group */
                          NULL,                                    /* no tag identifier */
                          (iisDependent ? "W3SVC\0" : "\0"), NULL, /* LocalSystem account */
                          NULL);                                   /* no password */

        if (schService != NULL)
            break;

        if (schService == NULL) {
            if (GetLastError() == ERROR_DUP_NAME || GetLastError() == ERROR_SERVICE_EXISTS)
                continue;
            printf("CreateService failed - %s\n", GetLastErrorText(szErr, 256));
            exit(1);
        }
    }

    printf("%s is now installed as service.\n", name);
    printf("\nIf you want to start this service now, issue the command:\n");
    printf("     NET START %s\n", name);
    printf("\nWhen you reboot this machine, the service will start automatically.\n");
    CloseServiceHandle(schService);

    SetCurrentDirectory(myPath);

    if (SZSERVICEMSGFILE != NULL) {
        sprintf(keyname, "SYSTEM\\CurrentControlSet\\Services\\EventLog\\Application\\%s", name);
        err = RegCreateKey(HKEY_LOCAL_MACHINE, keyname, &hkey);
        if (err == ERROR_SUCCESS) {
            RegSetValueEx(hkey, "EventMessageFile", 0, REG_EXPAND_SZ, SZSERVICEMSGFILE,
                          strlen(SZSERVICEMSGFILE) + 1);
            typesSupported = 7;
            RegSetValueEx(hkey, "TypesSupported", 0, REG_DWORD, (BYTE *)&typesSupported,
                          sizeof(typesSupported));
            RegCloseKey(hkey);
        }
    }

    CloseServiceHandle(schSCManager);
}

//
/*  FUNCTION: CmdRemoveService() */
//
/*  PURPOSE: Stops and removes the service */
//
/*  PARAMETERS: */
/*    none */
//
/*  RETURN VALUE: */
/*    none */
//
/*  COMMENTS: */
//
void CmdRemoveService(char *arg) {
    SC_HANDLE schService;
    SC_HANDLE schSCManager;
    struct INSTALLEDNAMES *in;

    if (arg == NULL || *arg == 0) {
        if (!InstalledAsService(&in)) {
            if (in == NULL)
                printf("EZproxy is not installed to run as a service.\n");
            else
                printf("EZproxy is not installed to run as a service from this directory.\n");
            exit(1);
        }
        arg = myServiceName;
    } else {
        if (strnicmp(arg, "ezproxy", 7) != 0) {
            fprintf(stderr, "Usage: \"ezproxy -sr\" or \"ezproxy -sr servicename\"\n");
            exit(1);
        }
    }

    schSCManager = OpenSCManager(NULL,                 /* machine (NULL == local) */
                                 NULL,                 /* database (NULL == default) */
                                 SC_MANAGER_ALL_ACCESS /* access required */
    );

    if (schSCManager == NULL) {
        printf(TEXT("OpenSCManager failed - %s\n"), GetLastErrorText(szErr, 256));
        exit(1);
    }

    schService = OpenService(schSCManager, arg, SERVICE_ALL_ACCESS);

    if (schService == NULL) {
        fprintf(stderr, "Unable to access service %s:\n%s\n", arg, GetLastErrorText(szErr, 256));
        exit(1);
    }

    /* try to stop the service */
    if (ControlService(schService, SERVICE_CONTROL_STOP, &ssStatus)) {
        printf("Stopping %s.", arg);
        Sleep(1000);

        while (QueryServiceStatus(schService, &ssStatus)) {
            if (ssStatus.dwCurrentState == SERVICE_STOP_PENDING) {
                printf(".");
                Sleep(1000);
            } else
                break;
        }

        if (ssStatus.dwCurrentState == SERVICE_STOPPED)
            printf("\n%s stopped.\n", arg);
        else
            printf("\n%s failed to stop.\n", arg);
    }

    /* now remove the service */
    if (DeleteService(schService))
        printf("%s is no longer installed as a service.\n", arg);
    else
        printf("DeleteService failed - %s\n", GetLastErrorText(szErr, 256));

    CloseServiceHandle(schService);
    CloseServiceHandle(schSCManager);
}

void StartMyService(void) {
    SC_HANDLE schService;
    SC_HANDLE schSCManager;

    schSCManager = OpenSCManager(NULL,                 /* machine (NULL == local) */
                                 NULL,                 /* database (NULL == default) */
                                 SC_MANAGER_ALL_ACCESS /* access required */
    );

    if (schSCManager == NULL) {
        printf(TEXT("OpenSCManager failed - %s\n"), GetLastErrorText(szErr, 256));
        exit(1);
    }

    schService = OpenService(schSCManager, myServiceName, SERVICE_ALL_ACCESS);

    if (schService == NULL) {
        fprintf(stderr, "Unable to access service %s:\n%s\n", myServiceName,
                GetLastErrorText(szErr, 256));
        exit(1);
    }

    if (StartService(schService, 0, NULL)) {
        printf("Starting %s.", myServiceName);
        Sleep(1000);

        while (QueryServiceStatus(schService, &ssStatus)) {
            if (ssStatus.dwCurrentState == SERVICE_START_PENDING) {
                printf(".");
                Sleep(1000);
            } else
                break;
        }

        if (ssStatus.dwCurrentState == SERVICE_RUNNING)
            printf("\n%s started.\n", myServiceName);
        else
            printf("\n%s failed to start.\n", myServiceName);
    }

    CloseServiceHandle(schService);
    CloseServiceHandle(schSCManager);
}

void CmdListServices(void) {
    struct INSTALLEDNAMES *in, *inp;
    char dirName[256];

    InstalledAsService(&in);

    if (in == NULL) {
        printf("EZproxy is not installed as a service\n");
        return;
    }

    printf("\nService Name   Directory\n");
    printf("------------   ---------\n");
    for (inp = in; inp; inp = inp->next) {
        strcpy(dirName, inp->servicePath);
        DirName(dirName);
        printf("%-14.14s %s\n", inp->displayName, dirName);
    }
}

int SortServices(const void *v1, const void *v2) {
    struct _ENUM_SERVICE_STATUSA *s1 = (struct _ENUM_SERVICE_STATUSA *)v1;
    struct _ENUM_SERVICE_STATUSA *s2 = (struct _ENUM_SERVICE_STATUSA *)v2;

    return -stricmp(s1->lpServiceName, s2->lpServiceName);
}

BOOL InstalledAsService(struct INSTALLEDNAMES **in) {
    SC_HANDLE schService;
    SC_HANDLE schSCManager;
    static BOOL first = 1;
    static BOOL isService;
    DWORD cbBytesNeeded, cbBytesAllocated = 0, dwServicesReturned, dwResumeHandle = 0;
    LPQUERY_SERVICE_CONFIG pqsc = NULL;
    LPENUM_SERVICE_STATUS pess;
    DWORD i;
    struct INSTALLEDNAMES *inn;

    if (first == 0 && in == NULL)
        return isService;

    if (in)
        *in = NULL;

    first = 0;

    isService = 0;

    schSCManager = OpenSCManager(NULL, /* machine (NULL == local) */
                                 NULL, /* database (NULL == default) */
                                 GENERIC_READ /* SC_MANAGER_ALL_ACCESS */ /* access required */
    );

    if (schSCManager == NULL) {
        AddToMessageLog("Unable to open service manager");
        exit(1);
    }

    /*
    schService = OpenService(schSCManager, SZSERVICENAME, GENERIC_READ /*SERVICE_ALL_ACCESS */ /*);

    if (schService != NULL) {
        isService = 1;
        CloseServiceHandle(schService);
    }
    */

    EnumServicesStatus(schSCManager, SERVICE_WIN32, SERVICE_STATE_ALL, NULL, 0, &cbBytesNeeded,
                       &dwServicesReturned, &dwResumeHandle);
    pess = (LPENUM_SERVICE_STATUS)malloc(cbBytesNeeded);
    EnumServicesStatus(schSCManager, SERVICE_WIN32, SERVICE_STATE_ALL, pess, cbBytesNeeded,
                       &cbBytesNeeded, &dwServicesReturned, &dwResumeHandle);
    qsort(pess, dwServicesReturned, sizeof(*pess), SortServices);
    for (i = 0; i < dwServicesReturned; i++) {
        if (strnicmp(pess[i].lpServiceName, SZSERVICENAME, strlen(SZSERVICENAME)) != 0)
            continue;

        schService = OpenService(schSCManager, pess[i].lpServiceName, GENERIC_READ);

        if (schService == NULL) {
            AddToMessageLog("Unable to open service %s", pess[i].lpServiceName);
            continue;
        }

        QueryServiceConfig(schService, NULL, 0, &cbBytesNeeded);
        if (cbBytesAllocated < cbBytesNeeded) {
            FreeThenNull(pqsc);
            cbBytesAllocated = cbBytesNeeded * 2;
            if (!(pqsc = malloc(cbBytesAllocated))) {
                AddToMessageLog("pqsc memory failed");
                exit(1);
            }
        }
        pqsc = calloc(1, cbBytesNeeded);
        if (pqsc == NULL) {
            AddToMessageLog("Unable to allocate memory for QueryServiceConfig");
            exit(1);
        }

        QueryServiceConfig(schService, pqsc, cbBytesNeeded, &cbBytesNeeded);
        if (in) {
            if (!(inn = calloc(1, sizeof(*inn))))
                PANIC;
            if (!(inn->serviceName = strdup(pess[i].lpServiceName)))
                PANIC;
            if (!(inn->displayName = strdup(pess[i].lpDisplayName)))
                PANIC;
            if (!(inn->servicePath = strdup(pqsc->lpBinaryPathName)))
                PANIC;
            inn->next = *in;
            *in = inn;
        }

        if (stricmp(pqsc->lpBinaryPathName, myBinary) == 0) {
            isService = 1;
            if (myServiceName == NULL)
                if (!(myServiceName = strdup(pess[i].lpServiceName)))
                    PANIC;
            if (myServiceDisplayName == NULL)
                if (!(myServiceDisplayName = strdup(pess[i].lpDisplayName)))
                    PANIC;
        }

        CloseServiceHandle(schService);
    }
    FreeThenNull(pqsc);
    FreeThenNull(pess);

    CloseServiceHandle(schSCManager);

    return isService;
}

/*///////////////////////////////////////////////////////////////// */
//
/*  The following code is for running the service as a console app */
//

//
/*  FUNCTION: CmdDebugService(int argc, char ** argv) */
//
/*  PURPOSE: Runs the service as a console application */
//
/*  PARAMETERS: */
/*    argc - number of command line arguments */
/*    argv - array of command line arguments */
//
/*  RETURN VALUE: */
/*    none */
//
/*  COMMENTS: */
//
void CmdDebugService(int argc, char **argv) {
    DWORD dwArgc;
    LPTSTR *lpszArgv;

#ifdef UNICODE
    lpszArgv = CommandLineToArgvW(GetCommandLineW(), &(dwArgc));
#else
    dwArgc = (DWORD)argc;
    lpszArgv = argv;
#endif

    printf("Debugging %s.\n", myServiceDisplayName);

#pragma warning(push)
#pragma warning(disable : 4024 4028)
    SetConsoleCtrlHandler(ControlHandler, TRUE);
#pragma warning(pop)

    ServiceStart(dwArgc, lpszArgv, 1);
}

//
/*  FUNCTION: ControlHandler ( DWORD dwCtrlType ) */
//
/*  PURPOSE: Handled console control events */
//
/*  PARAMETERS: */
/*    dwCtrlType - type of control event */
//
/*  RETURN VALUE: */
/*    True - handled */
/*    False - unhandled */
//
/*  COMMENTS: */
//
BOOL WINAPI ControlHandler(DWORD dwCtrlType) {
    switch (dwCtrlType) {
    case CTRL_BREAK_EVENT: /* use Ctrl+C or Ctrl+Break to simulate */
    case CTRL_C_EVENT:     /* SERVICE_CONTROL_STOP in debug mode */
        printf("Stopping %s.\n", myServiceDisplayName);
        ServiceStop();
        return TRUE;
        break;
    }
    return FALSE;
}

//
/*  FUNCTION: GetLastErrorText */
//
/*  PURPOSE: copies error message text to string */
//
/*  PARAMETERS: */
/*    lpszBuf - destination buffer */
/*    dwSize - size of buffer */
//
/*  RETURN VALUE: */
/*    destination buffer */
//
/*  COMMENTS: */
//
LPTSTR GetLastErrorText(LPTSTR lpszBuf, DWORD dwSize) {
    DWORD dwRet;
    LPTSTR lpszTemp = NULL;

    dwRet = FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ARGUMENT_ARRAY,
        NULL, GetLastError(), LANG_NEUTRAL, (LPTSTR)&lpszTemp, 0, NULL);

    /* supplied buffer is not long enough */
    if (!dwRet || ((long)dwSize < (long)dwRet + 14))
        lpszBuf[0] = TEXT('\0');
    else {
        lpszTemp[lstrlen(lpszTemp) - 2] = 0;  // remove cr and newline character
        sprintf(lpszBuf, "%s (0x%x)", lpszTemp, GetLastError());
    }

    if (lpszTemp)
        LocalFree((HLOCAL)lpszTemp);

    return lpszBuf;
}

char *MyServiceDisplayName(void) {
    return InstalledAsService(NULL) ? myServiceDisplayName : "";
}
#else
void junk190284719084751092847() {} /* ISO C forbids an empty source file */
#endif
