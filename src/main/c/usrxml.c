/**
 * XML user authentication
 *
 * This module provides support
 * for EZproxy to access XML servers as a XML client.  See also admuserobject.c, which
 * allows EZproxy to act as a XML server for other XML clients.
 *
 * This authentication method supports common conditions and actions.
 */

#define __UUFILE__ "usrxml.c"

#include "common.h"
#include "usrxml.h"

#ifdef WIN32
/* io.h, fcntl.h and share.h needed for _sopen in Win32 */
#include <io.h>
#include <fcntl.h>
#include <share.h>
#include <lm.h>
#endif
#include <limits.h>

#include "usr.h"

#include "uuxml.h"
#include "expression.h"

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define AT __UUFILE__ ":" TOSTRING(__LINE__) " "

#define STATEMENT_RESULT_FAILURE_INDEFINATE ((int)2)
#define STATEMENT_RESULT_UNCHANGED ((int)3)

#define NULLEMPTYFMTSTR(x) ((x) != NULL) ? (x) : ""
#define NULLDELETEFMTSTR(x) ((x) != NULL) ? (x) : "(DELETED)"
#define NULLNULLFMTSTR(x) ((x) != NULL) ? (x) : "(null)"
#define NULLFMTSTR(x, y) ((x) != NULL) ? (x) : y

#define AT_INDEX_TEXT(index) ((((index) == NULL) || (*(index) == 0)) ? "" : " at index ")
#define VAR_NAME_WITH_INDEX_FMT "%s%s%s"
#define VAR_NAME_WITH_INDEX_PARM(var, index) \
    var, AT_INDEX_TEXT(index), ((index == NULL) ? "" : index)

#define XML_NEED_NOT_BE_WELLFORMED 1
#define XML_MUST_BE_WELLFORMED 0

struct UsrXMLProcessBindingsClosure_STATE {
    struct XMLCTX *xmlCtx;
    struct USERINFO *uip;
    int count;
    STATEMENT_RESULT_TYPE result;
};

#define USRXML_LIBXML_CALLBACK_CONTEXT_STRUCT_ID "XMLCTXc"
#define IS_USRXML_LIBXML_CALLBACK_CONTEXT_STRUCT_VALID(x) \
    (!(memcmp((x)->structureIdentifier, USRXML_LIBXML_CALLBACK_CONTEXT_STRUCT_ID, 7) == 0))
struct USRXML_LIBXML_CALLBACK_CONTEXT {
    struct XMLCTX *xmlCtx;
    struct USERINFO *uip;
    char *customFile;
    STATEMENT_RESULT_TYPE result;
    STATEMENT_RESULT_TYPE resultIfParseFailure;
    char structureIdentifier[7];
};

/* Forward Declarations */
int UsrXMLMsgAuthLog(struct TRANSLATE *t,
                     struct USERINFO *uip,
                     struct XMLCTX *xmlCtx,
                     char *direction,
                     xmlDocPtr doc);

void UsrXMLErrorCallback(void *ctx, const char *msg, ...) {
    struct USRXML_LIBXML_CALLBACK_CONTEXT *xmlCallbackCtxPtr =
        (struct USRXML_LIBXML_CALLBACK_CONTEXT *)ctx;
    va_list ap;
    char buffer[1024];

    buffer[0] = 0;
    va_start(ap, msg);
    _SNPRINTF_BEGIN(buffer, sizeof(buffer));
    _vsnprintf(buffer + strlen(buffer), sizeof(buffer) - strlen(buffer), msg, ap);
    _SNPRINTF_END(buffer, sizeof(buffer));
    va_end(ap);
    if (ctx == NULL) {
        /* DTD parsing doesn't have a way to pass thru any context. */
        Log(AT "(no context) error %s", buffer);
    } else if (IS_USRXML_LIBXML_CALLBACK_CONTEXT_STRUCT_VALID(xmlCallbackCtxPtr)) {
        if (xmlCallbackCtxPtr->resultIfParseFailure != STATEMENT_RESULT_UNCHANGED) {
            xmlCallbackCtxPtr->result = xmlCallbackCtxPtr->resultIfParseFailure;
        }
        UsrLog(xmlCallbackCtxPtr->uip, "%s: in %s error %s", xmlCallbackCtxPtr->xmlCtx->inServiceOf,
               xmlCallbackCtxPtr->customFile, buffer);
    } else {
        /* Wrong context. */
        Log(AT "(unrecognized context) error %s", buffer);
    }
}

void UsrXMLWarningCallback(void *ctx, const char *msg, ...) {
    struct USRXML_LIBXML_CALLBACK_CONTEXT *xmlCallbackCtxPtr =
        (struct USRXML_LIBXML_CALLBACK_CONTEXT *)ctx;
    va_list ap;
    char buffer[1024];

    buffer[0] = 0;
    va_start(ap, msg);
    _SNPRINTF_BEGIN(buffer, sizeof(buffer));
    _vsnprintf(buffer + strlen(buffer), sizeof(buffer) - strlen(buffer), msg, ap);
    _SNPRINTF_END(buffer, sizeof(buffer));
    va_end(ap);
    if (ctx == NULL) {
        /* DTD parsing doesn't have a way to pass thru any context. */
        Log(AT "(no context) warning %s", buffer);
    } else if (IS_USRXML_LIBXML_CALLBACK_CONTEXT_STRUCT_VALID(xmlCallbackCtxPtr)) {
        if (xmlCallbackCtxPtr->uip->debug) {
            UsrLog(xmlCallbackCtxPtr->uip, "%s: in %s warning %s",
                   xmlCallbackCtxPtr->xmlCtx->inServiceOf, xmlCallbackCtxPtr->customFile, buffer);
        }
    } else {
        /* Wrong context. */
        Log(AT "(unrecognized context) warning %s", buffer);
    }
}

xmlSAXHandlerPtr UsrXMLSAXInitSAXHandler(void *context,
                                         struct USERINFO *uip,
                                         xmlSAXHandlerPtr hdlr) {
    memset(hdlr, 0, sizeof(*hdlr));
    xmlSAX2InitDefaultSAXHandler(hdlr, uip->debug || (debugLevel > 0)); /* true to get warnings */
    hdlr->error = UsrXMLErrorCallback;
    hdlr->warning = UsrXMLErrorCallback;
    hdlr->_private = context;
    hdlr->internalSubset = NULL /* xmlSAX2InternalSubset */;
    hdlr->externalSubset = NULL /* xmlSAX2ExternalSubset */;
    if (debugLevel > 7000) {
        LogBinaryPacket("SAXHandler", (unsigned char *)hdlr, sizeof(*hdlr));
    }

    return hdlr;
}

int UsrXMLVariableNameCompare(char *leftVariableName, char *rightBindingName) {
    int k;

    for (; ((k = (*((unsigned char *)leftVariableName) - *((unsigned char *)rightBindingName))) ==
            0) &&
           (*leftVariableName != 0) && (*rightBindingName != 0);
         leftVariableName++, rightBindingName++) {
    }
    if (*leftVariableName != *rightBindingName) {
        /* if they're not equal we have to translate a brackets to zeros, then try again. */
        if (((*leftVariableName == '[') && (*rightBindingName == 0)) ||
            ((*leftVariableName == 0) && (*rightBindingName == '['))) {
            return 0;
        } else {
            return (k < 0) ? -1 : (k == 0) ? 0 : 1;
        }
    } else {
        return 0;
    }
}

void UserXMLFreeBinding(struct XMLBINDING *binding) {
    struct XMLBINDINGHIERARCHY *xPathHierarchy, *nextXPathHierarchy;

    if (binding != NULL) {
        FreeThenNull(binding->ezpName);
        for (xPathHierarchy = binding->pathHierarchy; xPathHierarchy;
             xPathHierarchy = nextXPathHierarchy) {
            nextXPathHierarchy = xPathHierarchy->next;
            FreeThenNull(xPathHierarchy->pathExpr);
        }
        FreeThenNull(binding);
    }
}

void UserXMLFreeBindings(struct XMLCTX *xmlCtx) {
    struct XMLBINDING *binding, *nextBinding;

    if (xmlCtx) {
        for (binding = xmlCtx->bindings; binding; binding = nextBinding) {
            nextBinding = binding->next;
            UserXMLFreeBinding(binding);
        }
    }
}

void UserXMLFreeNamespacess(struct XMLCTX *xmlCtx) {
    struct XMLNS *nameSpace, *nextNameSpace;

    if (xmlCtx) {
        for (nameSpace = xmlCtx->xmlNs; nameSpace; nameSpace = nextNameSpace) {
            nextNameSpace = nameSpace->next;
            FreeThenNull(nameSpace->ns);
            FreeThenNull(nameSpace->urn);
            FreeThenNull(nameSpace);
        }
    }
}

void UserXMLInit(struct XMLCTX *xmlCtx,
                 struct sockaddr_storage *pInterface,
                 char *defaultContentTypeForPOST,
                 char *defaultContentTypeForDocument,
                 char *inServiceOf) {
    memset(xmlCtx, 0, sizeof(*xmlCtx));
    xmlCtx->pInterface = pInterface;
    if (!(xmlCtx->url = malloc(128)))
        PANIC;
    xmlCtx->urlMemMax = 128;
    xmlCtx->url[0] = 0;
    if (!(xmlCtx->outgoingHeaders = malloc(128)))
        PANIC;
    xmlCtx->outgoingHeadersMemMax = 128;
    xmlCtx->outgoingHeaders[0] = 0;
    if (!(xmlCtx->data = malloc(128)))
        PANIC;
    xmlCtx->dataMemMax = 128;
    xmlCtx->data[0] = 0;
    if (!(xmlCtx->contentTypeForPOST = strdup(defaultContentTypeForPOST)))
        PANIC;
    if (!(xmlCtx->defaultContentTypeForPOST = strdup(defaultContentTypeForPOST)))
        PANIC;
    if (!(xmlCtx->contentTypeForDocument = strdup(defaultContentTypeForDocument)))
        PANIC;
    if (!(xmlCtx->defaultContentTypeForDocument = strdup(defaultContentTypeForDocument)))
        PANIC;
    if (!(xmlCtx->inServiceOf = strdup(inServiceOf)))
        PANIC;
    strcpy(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER);
}

void UsrXMLClearXMLIn(struct XMLCTX *xmlCtx) {
    if (xmlCtx) {
        FreeThenNull(xmlCtx->incomingHeaders);
        if (xmlCtx->xpathCtxIn) {
            xmlXPathFreeContext(xmlCtx->xpathCtxIn);
            xmlCtx->xpathCtxIn = NULL;
        }
        if (xmlCtx->docIn) {
            xmlFreeDoc(xmlCtx->docIn);
            xmlCtx->docIn = NULL;
        }
    }
}

void UsrXMLClearXMLOut(struct XMLCTX *xmlCtx) {
    if (xmlCtx) {
        FreeThenNull(xmlCtx->contentTypeForPOST);
        if (!(xmlCtx->contentTypeForPOST = strdup(xmlCtx->defaultContentTypeForPOST)))
            PANIC;
        FreeThenNull(xmlCtx->contentTypeForDocument);
        if (!(xmlCtx->contentTypeForDocument = strdup(xmlCtx->defaultContentTypeForDocument)))
            PANIC;
        xmlCtx->newXMLDocOut = FALSE;
        xmlCtx->data[0] = 0;
        xmlCtx->outgoingHeaders[0] = 0;
        if (xmlCtx->xpathCtxOut) {
            xmlXPathFreeContext(xmlCtx->xpathCtxOut);
            xmlCtx->xpathCtxOut = NULL;
        }
        if (xmlCtx->docOut) {
            xmlFreeDoc(xmlCtx->docOut);
            xmlCtx->docOut = NULL;
        }
    }
}

void UserXMLDestroy(struct XMLCTX *xmlCtx) {
    if (xmlCtx) {
        UsrXMLClearXMLIn(xmlCtx);
        UsrXMLClearXMLOut(xmlCtx);
        FreeThenNull(xmlCtx->schemaOutName);
        if (xmlCtx->schemaOut != NULL) {
            xmlSchemaFree(xmlCtx->schemaOut);
            xmlCtx->schemaOut = NULL;
        }
        FreeThenNull(xmlCtx->schemaInName);
        if (xmlCtx->schemaIn != NULL) {
            xmlSchemaFree(xmlCtx->schemaIn);
            xmlCtx->schemaIn = NULL;
        }
        FreeThenNull(xmlCtx->dtdOutName);
        if (xmlCtx->dtdOut != NULL) {
            xmlFreeDtd(xmlCtx->dtdOut);
            xmlCtx->dtdOut = NULL;
        }
        FreeThenNull(xmlCtx->dtdInName);
        if (xmlCtx->dtdIn != NULL) {
            xmlFreeDtd(xmlCtx->dtdIn);
            xmlCtx->dtdIn = NULL;
        }
        FreeThenNull(xmlCtx->url);
        FreeThenNull(xmlCtx->outgoingHeaders);
        FreeThenNull(xmlCtx->contentTypeForPOST);
        FreeThenNull(xmlCtx->defaultContentTypeForPOST);
        FreeThenNull(xmlCtx->contentTypeForDocument);
        FreeThenNull(xmlCtx->defaultContentTypeForDocument);
        UserXMLFreeNamespacess(xmlCtx);
        UserXMLFreeBindings(xmlCtx);
    }
}

STATEMENT_RESULT_TYPE UsrXMLURLCat(struct TRANSLATE *t,
                                   struct USERINFO *uip,
                                   void *context,
                                   char *arg) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    int newMemMax;
    char *old;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    if (arg == NULL) {
        arg = "";
    }

    newMemMax = strlen(xmlCtx->url) + strlen(arg) + 1;
    if ((newMemMax > xmlCtx->urlMemMax) && (strlen(arg) != 0)) {
        newMemMax += 128; /* reduce the number of times we have to reallocate. */
        old = xmlCtx->url;
        if (!(xmlCtx->url = malloc(newMemMax)))
            PANIC;
        xmlCtx->urlMemMax = newMemMax;
        xmlCtx->url[0] = 0;
        strcat(xmlCtx->url, old);
        FreeThenNull(old);
    }
    strcat(xmlCtx->url, arg);

    return STATEMENT_RESULT_SUCCESS;
}

STATEMENT_RESULT_TYPE UsrXMLURLStart(struct TRANSLATE *t,
                                     struct USERINFO *uip,
                                     void *context,
                                     char *arg) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    xmlCtx->url[0] = 0;
    return UsrXMLURLCat(t, uip, context, arg);
}

STATEMENT_RESULT_TYPE UsrXMLOutgoingHeadersCat(struct TRANSLATE *t,
                                               struct USERINFO *uip,
                                               void *context,
                                               char *arg) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    int newMemMax;
    char *old;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    if (arg == NULL) {
        arg = "";
    }

    newMemMax = strlen(xmlCtx->outgoingHeaders) + strlen(arg) + 2 + 1;
    if ((newMemMax > xmlCtx->outgoingHeadersMemMax) && (strlen(arg) != 0)) {
        newMemMax += 128; /* reduce the number of times we have to reallocate. */
        old = xmlCtx->outgoingHeaders;
        if (!(xmlCtx->outgoingHeaders = malloc(newMemMax)))
            PANIC;
        xmlCtx->outgoingHeadersMemMax = newMemMax;
        xmlCtx->outgoingHeaders[0] = 0;
        strcat(xmlCtx->outgoingHeaders, old);
        FreeThenNull(old);
    }
    strcat(xmlCtx->outgoingHeaders, arg);
    strcat(xmlCtx->outgoingHeaders, "\r\n");

    return STATEMENT_RESULT_SUCCESS;
}

STATEMENT_RESULT_TYPE UsrXMLOutgoingHeadersStart(struct TRANSLATE *t,
                                                 struct USERINFO *uip,
                                                 void *context,
                                                 char *arg) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    xmlCtx->outgoingHeaders[0] = 0;
    return UsrXMLOutgoingHeadersCat(t, uip, context, arg);
}

STATEMENT_RESULT_TYPE UsrXMLDataCat(struct TRANSLATE *t,
                                    struct USERINFO *uip,
                                    void *context,
                                    char *arg) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    int newMemMax;
    char *old;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    if (arg == NULL) {
        arg = "";
    }

    newMemMax = strlen(xmlCtx->data) + strlen(arg) + 1;
    if ((newMemMax > xmlCtx->dataMemMax) && (strlen(arg) != 0)) {
        newMemMax += 128; /* reduce the number of times we have to reallocate. */
        old = xmlCtx->data;
        if (!(xmlCtx->data = malloc(newMemMax)))
            PANIC;
        xmlCtx->dataMemMax = newMemMax;
        xmlCtx->data[0] = 0;
        strcat(xmlCtx->data, old);
        FreeThenNull(old);
    }
    strcat(xmlCtx->data, arg);

    return STATEMENT_RESULT_SUCCESS;
}

STATEMENT_RESULT_TYPE UsrXMLDataCatURLEncoded(struct TRANSLATE *t,
                                              struct USERINFO *uip,
                                              void *context,
                                              char *arg) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    int newMemMax;
    int l;
    char *old;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    if (arg == NULL) {
        arg = "";
    }
    l = strlen(arg) * 3;

    newMemMax = strlen(xmlCtx->data) + l + 1;
    if ((newMemMax > xmlCtx->dataMemMax) && (l != 0)) {
        old = xmlCtx->data;
        if (!(xmlCtx->data = malloc(newMemMax)))
            PANIC;
        xmlCtx->dataMemMax = newMemMax;
        xmlCtx->data[0] = 0;
        strcat(xmlCtx->data, old);
        FreeThenNull(old);
    }
    AddEncodedField(
        xmlCtx->data, arg,
        xmlCtx->data +
            xmlCtx->dataMemMax); /* 'stop' should point one past the end of the buffer. */

    return STATEMENT_RESULT_SUCCESS;
}

STATEMENT_RESULT_TYPE UsrXMLDataStart(struct TRANSLATE *t,
                                      struct USERINFO *uip,
                                      void *context,
                                      char *arg) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    xmlCtx->data[0] = 0;

    return UsrXMLDataCat(t, uip, context, arg);
}

STATEMENT_RESULT_TYPE UsrXMLPOSTContentType(struct TRANSLATE *t,
                                            struct USERINFO *uip,
                                            void *context,
                                            char *arg) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    if (arg == NULL) {
        arg = "";
    }

    FreeThenNull(xmlCtx->contentTypeForPOST);
    if (!(xmlCtx->contentTypeForPOST = strdup(arg)))
        PANIC;

    return STATEMENT_RESULT_SUCCESS;
}

STATEMENT_RESULT_TYPE UsrXMLDocContentType(struct TRANSLATE *t,
                                           struct USERINFO *uip,
                                           void *context,
                                           char *arg) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    if (arg == NULL) {
        arg = "";
    }

    FreeThenNull(xmlCtx->contentTypeForDocument);
    if (!(xmlCtx->contentTypeForDocument = strdup(arg)))
        PANIC;

    return STATEMENT_RESULT_SUCCESS;
}

struct XMLNS *UsrXMLDeclareNamespace(struct TRANSLATE *t,
                                     struct USERINFO *uip,
                                     void *context,
                                     const char *ns,
                                     const char *urn) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    struct XMLNS *newNS = NULL;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    if (ns) {
        if (!(newNS = calloc(1, sizeof(*newNS))))
            PANIC;
        if (!(newNS->ns = strdup(ns)))
            PANIC;
        if (!(newNS->urn = strdup(urn ? urn : "")))
            PANIC;
        newNS->next = xmlCtx->xmlNs;
        xmlCtx->xmlNs = newNS;
    }

    return newNS;
}

STATEMENT_RESULT_TYPE UsrXMLNamespace(struct TRANSLATE *t,
                                      struct USERINFO *uip,
                                      void *context,
                                      char **argList) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_SUCCESS;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    if (argList && argList[0] && argList[1]) {
        if (!(UsrXMLDeclareNamespace(t, uip, xmlCtx, argList[0], argList[1])))
            PANIC;
    } else {
        UsrLog(uip, "%s: the namespace statement has too few parameters, aborting.",
               xmlCtx->inServiceOf);
        uip->skipping = TRUE;
        result = STATEMENT_RESULT_FAILURE;
    }

    if (argList && argList[0] && argList[1] && (argList[2] != NULL)) {
        UsrLog(uip, "%s: the namespace statement has too many parameters, warning.",
               xmlCtx->inServiceOf);
    }

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLSetLocalVar(struct TRANSLATE *t,
                                        struct USERINFO *uip,
                                        struct XMLCTX *xmlCtx,
                                        char *variableName,
                                        char *variableIndex,
                                        char *variableValue,
                                        BOOL nullable,
                                        BOOL append) {
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_SUCCESS;
    char *variableNameWithIndex = ExpressionVariableIndex(variableName, variableIndex);

    if (nullable) {
        if (append) {
            VariablesSetValueAppend(t->localVariables, variableNameWithIndex, variableValue);
        } else {
            VariablesSetValue(t->localVariables, variableNameWithIndex, variableValue);
        }
    } else {
        if (append) {
            VariablesSetValueAppend(t->localVariables, variableNameWithIndex, variableValue);
        } else {
            VariablesSetOrDeleteValue(t->localVariables, variableNameWithIndex, variableValue);
        }
    }
    if (uip->debug) {
        UsrLog(uip, "%s: binding variable " VAR_NAME_WITH_INDEX_FMT " with value \"%s\".",
               xmlCtx->inServiceOf, VAR_NAME_WITH_INDEX_PARM(variableName, variableIndex),
               NULLDELETEFMTSTR(variableValue));
    }

    if (variableName != variableNameWithIndex) {
        FreeThenNull(variableNameWithIndex);
    }

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLSetLocalVarCheckName(struct TRANSLATE *t,
                                                 struct USERINFO *uip,
                                                 struct XMLCTX *xmlCtx,
                                                 char *variableName,
                                                 char *variableIndex,
                                                 char *variableIndexLastChar,
                                                 char *variableValue,
                                                 BOOL nullable,
                                                 BOOL append) {
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_FAILURE;

    if (!_SNPRINTF_IS_OVERFLOW(variableIndexLastChar, 1)) {
        result = UsrXMLSetLocalVar(t, uip, xmlCtx, variableName, variableIndex, variableValue,
                                   nullable, append);
    } else {
        uip->skipping = TRUE;
        UsrLog(uip,
               "%s: binding variable " VAR_NAME_WITH_INDEX_FMT
               " has a name that's too long, aborting.",
               xmlCtx->inServiceOf, VAR_NAME_WITH_INDEX_PARM(variableName, variableIndex));
    }

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLSetLocalVarCheckNameAndValue(struct TRANSLATE *t,
                                                         struct USERINFO *uip,
                                                         struct XMLCTX *xmlCtx,
                                                         char *variableName,
                                                         char *variableIndex,
                                                         char *variableIndexLastChar,
                                                         char *variableValue,
                                                         char *variableValueLastChar,
                                                         BOOL nullable,
                                                         BOOL append) {
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_FAILURE;

    if (!_SNPRINTF_IS_OVERFLOW(variableIndexLastChar, 1)) {
        result = UsrXMLSetLocalVarCheckName(t, uip, xmlCtx, variableName, variableIndex,
                                            variableIndexLastChar, variableValue, nullable, append);
    } else {
        uip->skipping = TRUE;
        UsrLog(uip,
               "%s: binding variable " VAR_NAME_WITH_INDEX_FMT
               " has a value of \"%s\" that's too long, aborting.",
               xmlCtx->inServiceOf, VAR_NAME_WITH_INDEX_PARM(variableName, variableIndex),
               NULLDELETEFMTSTR(variableValue));
    }

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLUpdateVariableFromXML(struct TRANSLATE *t,
                                                  struct USERINFO *uip,
                                                  struct XMLCTX *xmlCtx,
                                                  xmlXPathContextPtr xPathCtx,
                                                  char *variableIndex,
                                                  char *variableIndexLastChar,
                                                  struct XMLBINDING *binding,
                                                  struct XMLBINDINGHIERARCHY *xPathHierarchy,
                                                  char *variableValue,
                                                  char *variableValueLastChar) {
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_FAILURE_INDEFINATE;
    xmlNodePtr savedNode = NULL;
    xmlXPathObjectPtr xpathObj = NULL;
    xmlNodeSetPtr nodes = NULL;
    xmlChar *val = NULL;
    char *tmpStr = NULL;
    int variableIndexBase;
    int variableValueBase;
    int i = 0;
    BOOL overflowVariableIndex = FALSE;

    variableIndexBase = strlen(variableIndex);
    variableValueBase = strlen(variableValue);
    xpathObj = xmlXPathEvalExpression(BAD_CAST xPathHierarchy->pathExpr, xPathCtx);
    if (xpathObj == NULL) {
        UsrLog(
            uip, "%s: binding variable %s to XPath expression %s at depth %d is invalid, aborting.",
            xmlCtx->inServiceOf, binding->ezpName, xPathHierarchy->pathExpr, xPathHierarchy->depth);
        uip->skipping = TRUE;
        result = STATEMENT_RESULT_FAILURE;
    } else if (xpathObj->type == XPATH_NODESET) {
        if (((nodes = xpathObj->nodesetval) == NULL) || ((nodes->nodeNr == 0))) {
            /* No nodes selected. */
        } else {
            for (; (i < nodes->nodeNr) && ((result == STATEMENT_RESULT_FAILURE_INDEFINATE) ||
                                           (result == STATEMENT_RESULT_SUCCESS));
                 i++) {
                if (debugLevel > 7000) {
                    UsrLog(uip, "at depth %d XPath %s[%d] %s", xPathHierarchy->depth,
                           xPathHierarchy->pathExpr, i, tmpStr = UUxmlNodeName(nodes->nodeTab[i]));
                    xmlFreeThenNull(tmpStr);
                }
                if (xPathHierarchy->next != NULL) {
                    savedNode = xPathCtx->node;
                    xPathCtx->node = nodes->nodeTab[i];
                    _snprintf(&(variableIndex[variableIndexBase]),
                              (variableIndexLastChar - &(variableIndex[variableIndexBase])) + 1,
                              "%d,", i);
                    _SNPRINTF_END(variableIndexLastChar, 1);
                    result = UsrXMLUpdateVariableFromXML(
                        t, uip, xmlCtx, xPathCtx, variableIndex, variableIndexLastChar, binding,
                        xPathHierarchy->next, variableValue, variableValueLastChar);
                    xPathCtx->node = savedNode;
                } else {
                    val = xmlNodeGetContent(nodes->nodeTab[i]);
                    if (binding->varComplexity) {
                        /* scalar */
                        if (i != 0) {
                            // TODO: what delimiter?
                            result = UsrXMLSetLocalVar(t, uip, xmlCtx, binding->ezpName, NULL,
                                                       (char *)"\t", TRUE, TRUE);
                            result = UsrXMLSetLocalVar(t, uip, xmlCtx, binding->ezpName, NULL,
                                                       (char *)val, TRUE, TRUE);
                        } else {
                            result = UsrXMLSetLocalVar(t, uip, xmlCtx, binding->ezpName, NULL,
                                                       (char *)val, binding->nullable, FALSE);
                        }
                    } else {
                        /* vector */
                        _snprintf(&(variableIndex[variableIndexBase]),
                                  (variableIndexLastChar - &(variableIndex[variableIndexBase])) + 1,
                                  "%d", i);
                        _SNPRINTF_END(variableIndexLastChar, 1);
                        result = UsrXMLSetLocalVarCheckName(t, uip, xmlCtx, binding->ezpName,
                                                            variableIndex, variableIndexLastChar,
                                                            (char *)val, binding->nullable, FALSE);
                    }
                    xmlFreeThenNull(val);
                }
                variableIndex[variableIndexBase] = 0;
                variableValue[variableValueBase] = 0;
            }
        }
        xmlXPathFreeObject(xpathObj);
    } else {
        _snprintf(&(variableIndex[variableIndexBase]),
                  (variableIndexLastChar - &(variableIndex[variableIndexBase])) + 1, "%d", 0);
        result = STATEMENT_RESULT_SUCCESS;
        i++;
        if (xpathObj->type == XPATH_BOOLEAN) {
            _snprintf(&(variableValue[variableValueBase]),
                      (variableValueLastChar - &(variableValue[variableValueBase])) + 1, "%d",
                      (xpathObj->boolval == 0) ? 0 : 1);
        } else if (xpathObj->type == XPATH_NUMBER) {
            _snprintf(&(variableValue[variableValueBase]),
                      (variableValueLastChar - &(variableValue[variableValueBase])) + 1, "%f",
                      xpathObj->floatval);
        } else if (xpathObj->type == XPATH_STRING) {
            _snprintf(&(variableValue[variableValueBase]),
                      (variableValueLastChar - &(variableValue[variableValueBase])) + 1, "%s",
                      xpathObj->stringval);
        } else {
            uip->skipping = TRUE;
            result = STATEMENT_RESULT_FAILURE;
            UsrLog(uip,
                   "%s: binding variable " VAR_NAME_WITH_INDEX_FMT
                   " to XPath expression %s at depth %d resulted in %s.  It is not useful in this "
                   "context.  Aborting.",
                   xmlCtx->inServiceOf, VAR_NAME_WITH_INDEX_PARM(binding->ezpName, variableIndex),
                   xPathHierarchy->pathExpr, xPathHierarchy->depth,
                   tmpStr = UUxmlXPathTypeToString(xpathObj->type));
            xmlFreeThenNull(tmpStr);
        }
        overflowVariableIndex = _SNPRINTF_IS_OVERFLOW(variableIndexLastChar, 1);
        _SNPRINTF_END(variableIndexLastChar, 1);
        if (binding->varComplexity) {
            result = (result == STATEMENT_RESULT_SUCCESS) &&
                     UsrXMLSetLocalVarCheckNameAndValue(
                         t, uip, xmlCtx, binding->ezpName, NULL, variableIndexLastChar,
                         variableValue, variableValueLastChar, binding->nullable, FALSE);
        } else {
            result = (result == STATEMENT_RESULT_SUCCESS) &&
                     UsrXMLSetLocalVarCheckNameAndValue(
                         t, uip, xmlCtx, binding->ezpName, variableIndex, variableIndexLastChar,
                         variableValue, variableValueLastChar, binding->nullable, FALSE);
        }
        variableIndex[variableIndexBase] = 0;
        variableValue[variableValueBase] = 0;
        xmlXPathFreeObject(xpathObj);
    }
    if (overflowVariableIndex) {
        uip->skipping = TRUE;
        result = STATEMENT_RESULT_FAILURE;
        UsrLog(
            uip,
            "%s: binding variable %s to XPath expression %s at depth %d is at too deep, aborting.",
            xmlCtx->inServiceOf, binding->ezpName, xPathHierarchy->pathExpr, xPathHierarchy->depth);
    }
    if (result == STATEMENT_RESULT_FAILURE_INDEFINATE) {
        result = STATEMENT_RESULT_SUCCESS;
        if (uip->debug) {
            UsrLog(uip,
                   "%s: binding variable %s has no effect because the XPath expression %s at depth "
                   "%d selects no nodes, warning.",
                   xmlCtx->inServiceOf, binding->ezpName, xPathHierarchy->pathExpr,
                   xPathHierarchy->depth);
        }
    }

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLProcessBindingsInFromXML(struct TRANSLATE *t,
                                                     struct USERINFO *uip,
                                                     struct XMLCTX *xmlCtx) {
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_SUCCESS;
    int variableIndexBase;
    char *variableIndex = t->buffer;
    char *variableIndexLastChar = variableIndex + (88 - 1);
    char *variableValue = variableIndexLastChar + 1;
    char *variableValueLastChar = variableValue + (sizeof(t->buffer) - 1);
    struct XMLBINDING *binding;

    for (binding = xmlCtx->bindings; (result == STATEMENT_RESULT_SUCCESS) && binding;
         binding = binding->next) {
        if (binding->inDir) {
            _SNPRINTF_BEGIN(variableIndexLastChar, 1);
            _SNPRINTF_BEGIN(variableValueLastChar, 1);
            variableIndex[0] = 0;
            variableValue[0] = 0;
            variableIndexBase = 0;
            result = UsrXMLUpdateVariableFromXML(
                t, uip, xmlCtx, xmlCtx->xpathCtxIn, variableIndex, variableIndexLastChar, binding,
                binding->pathHierarchy, variableValue, variableValueLastChar);
        }
    }

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLUpdateVariableFromURLEncodedData(
    struct TRANSLATE *t,
    struct USERINFO *uip,
    struct XMLCTX *xmlCtx,
    struct VARIABLES *urlEncodedVariables,
    char *variableIndex,
    char *variableIndexLastChar,
    struct XMLBINDING *binding,
    struct XMLBINDINGHIERARCHY *urlFieldNameHierarchy) {
    UsrLog(uip,
           "%s: binding variable %s to URL encoded field %s at depth %d is not yet implemented, "
           "aborting.",
           xmlCtx->inServiceOf, binding->ezpName, urlFieldNameHierarchy->pathExpr,
           urlFieldNameHierarchy->depth);
    uip->skipping = TRUE;
    return STATEMENT_RESULT_FAILURE;
}

STATEMENT_RESULT_TYPE UsrXMLProcessBindingsInFromURLEncodedData(struct TRANSLATE *t,
                                                                struct USERINFO *uip,
                                                                struct XMLCTX *xmlCtx) {
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_SUCCESS;
    int variableIndexBase;
    char *buffer = t->buffer;
    char *val = NULL;
    char *name = NULL;
    char *variableIndex = t->buffer + strlen(t->buffer);
    char *variableIndexLastChar = variableIndex + (88 - 1);
    char *variableValue = variableIndexLastChar + 1;
    char *variableValueLastChar = variableValue + (sizeof(t->buffer) - 1);
    struct XMLBINDING *binding;
    struct VARIABLES *urlEncodedVariables = NULL;

    urlEncodedVariables = VariablesNew(xmlCtx->inServiceOf, FALSE);
    // Loop through the URL encoded name-value-pairs and define variables to hold them.
    while (NextKEV(buffer, &name, &val, &buffer, TRUE)) {
        if (val == NULL || *val == 0) {
            val = "";
        }
        VariablesSetValue(urlEncodedVariables, name, val);
    }
    // VariablesForAll(t, uip, urlEncodedVariables, UsrXMLRenumberURLEncodedVariables);

    for (binding = xmlCtx->bindings; (result == STATEMENT_RESULT_SUCCESS) && binding;
         binding = binding->next) {
        if (binding->inDir) {
            _SNPRINTF_BEGIN(variableIndexLastChar, 1);
            _SNPRINTF_BEGIN(variableValueLastChar, 1);
            variableIndex[0] = 0;
            variableValue[0] = 0;
            variableIndexBase = 0;
            result = UsrXMLUpdateVariableFromURLEncodedData(t, uip, xmlCtx, urlEncodedVariables,
                                                            variableIndex, variableIndexLastChar,
                                                            binding, binding->pathHierarchy);
        }
    }

    VariablesFree(&urlEncodedVariables);

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLUpdateDocumentNode(struct TRANSLATE *t,
                                               struct USERINFO *uip,
                                               struct XMLCTX *xmlCtx,
                                               const char *variableName,
                                               BOOL nullable,
                                               xmlNodePtr *nodeTab,
                                               int nodeIndx,
                                               const char *val) {
    char *oldVal;
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_SUCCESS;

    if ((val == NULL) && (!nullable)) {
        val = "";
    }
    if (val != NULL) {
        /* Replace the node's content with the value from val. */
        if (uip->debug) {
            oldVal = (char *)xmlNodeGetContent(nodeTab[nodeIndx]);
            UsrLog(uip, "%s: binding variable %s value \"%s\" replaces value \"%s\".",
                   xmlCtx->inServiceOf, variableName, val, NULLEMPTYFMTSTR((char *)oldVal));
            FreeThenNull(oldVal);
        }
        xmlNodeSetContent(nodeTab[nodeIndx], BAD_CAST val);
    } else {
        /* Delete the node. */
        if (uip->debug) {
            oldVal = (char *)xmlNodeGetContent(nodeTab[nodeIndx]);
            UsrLog(uip, "%s: binding variable %s deletes value \"%s\".", xmlCtx->inServiceOf,
                   variableName, NULLEMPTYFMTSTR(val), NULLEMPTYFMTSTR((char *)oldVal));
            FreeThenNull(oldVal);
        }
        xmlUnlinkNode(nodeTab[nodeIndx]);
        xmlFreeNode(nodeTab[nodeIndx]);
        nodeTab[nodeIndx] = NULL;
    }

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLUpdateScalarVariableToXML(struct TRANSLATE *t,
                                                      struct USERINFO *uip,
                                                      struct XMLCTX *xmlCtx,
                                                      xmlXPathContextPtr xPathCtx,
                                                      struct XMLBINDING *binding,
                                                      struct XMLBINDINGHIERARCHY *xPathHierarchy,
                                                      char *value) {
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_FAILURE_INDEFINATE;
    xmlXPathContextPtr nextXPathCtx;
    xmlNodePtr *nodeTab;
    xmlNodeSetPtr nodes;
    xmlNodePtr node;
    xmlXPathObjectPtr xpathObj = NULL;
    char *tmpStr = NULL;
    int nodeNr;
    int j;

    xpathObj = xmlXPathEval(BAD_CAST xPathHierarchy->pathExpr, xPathCtx);
    if (xpathObj == NULL) {
        uip->skipping = TRUE;
        result = STATEMENT_RESULT_FAILURE;
        UsrLog(
            uip,
            "%s: binding variable %s to %s at depth %d is an invalid XPath expression, aborting.",
            xmlCtx->inServiceOf, xPathHierarchy->pathExpr, xPathHierarchy->depth, value);
    } else {
        if (xpathObj->type == XPATH_NODESET) {
            nodes = xpathObj->nodesetval;
            if ((nodes != NULL) && (nodes->nodeNr > 0)) {
                nodeTab = nodes->nodeTab;
                nodeNr = nodes->nodeNr;
                if ((xpathObj->nodesetval != NULL) && (xpathObj->nodesetval->nodeNr > 0)) {
                    nodes = xpathObj->nodesetval;
                    node = nodes->nodeTab[0];
                } else {
                    node = NULL;
                }

                if (debugLevel > 7000) {
                    UsrLog(uip, "at depth %d XPath %s %s", xPathHierarchy->depth,
                           xPathHierarchy->pathExpr, tmpStr = UUxmlNodeName(node));
                    xmlFreeThenNull(tmpStr);
                }
                if (node != NULL) {
                    if (xPathHierarchy->next != NULL) {
                        if (result = UsrXMLNewXpathCtx(t, uip, xmlCtx, xmlCtx->docOut,
                                                       &(nextXPathCtx))) {
                        } else {
                            nextXPathCtx->node = node;
                            result = UsrXMLUpdateScalarVariableToXML(
                                t, uip, xmlCtx, nextXPathCtx, binding, xPathHierarchy->next, value);
                            xmlXPathFreeContext(nextXPathCtx);
                            nextXPathCtx = NULL;
                        }
                    } else {
                        result = UsrXMLUpdateDocumentNode(t, uip, xmlCtx, binding->ezpName,
                                                          binding->nullable, nodeTab, 0, value);
                    }
                    /*  Leave only one instance. */
                    for (j = 1; j < nodeNr; j++) {
                        xmlUnlinkNode(nodeTab[j]);
                        xmlFreeNode(nodeTab[j]);
                        nodeTab[j] = NULL;
                    }
                }
            }
        } else {
            uip->skipping = TRUE;
            result = STATEMENT_RESULT_FAILURE;
            UsrLog(uip,
                   "%s: binding variable %s to XPath expression %s at depth %d resulted in %s.  It "
                   "is not useful in this context.  Aborting.",
                   xmlCtx->inServiceOf, binding->ezpName, xPathHierarchy->pathExpr,
                   xPathHierarchy->depth, tmpStr = UUxmlXPathTypeToString(xpathObj->type));
            xmlFreeThenNull(tmpStr);
        }
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }
    if (result == STATEMENT_RESULT_FAILURE_INDEFINATE) {
        result = STATEMENT_RESULT_SUCCESS;
        if (uip->debug) {
            UsrLog(uip,
                   "%s: binding variable %s has no effect because the XPath expression %s at depth "
                   "%d selects no nodes, warning.",
                   xmlCtx->inServiceOf, binding->ezpName, xPathHierarchy->pathExpr,
                   xPathHierarchy->depth);
        }
    }

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLUpdateVectorVariableToXML(struct TRANSLATE *t,
                                                      struct USERINFO *uip,
                                                      struct XMLCTX *xmlCtx,
                                                      xmlXPathContextPtr xPathCtx,
                                                      char *variableIndex,
                                                      struct XMLBINDING *binding,
                                                      struct XMLBINDINGHIERARCHY *xPathHierarchy,
                                                      char *value) {
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_FAILURE_INDEFINATE;
    xmlXPathContextPtr nextXPathCtx;
    char *variableIndexTerminus;
    char *variableIndexNext = NULL;
    xmlNodePtr *nodeTab = NULL;
    xmlNodeSetPtr nodes;
    xmlNodePtr node;
    xmlXPathObjectPtr xpathObj = NULL;
    char *tmpStr = NULL;
    char *p;
    int nodeNr;
    int i = 0;

    xpathObj = xmlXPathEval(BAD_CAST xPathHierarchy->pathExpr, xPathCtx);
    if (xpathObj == NULL) {
        uip->skipping = TRUE;
        result = STATEMENT_RESULT_FAILURE;
        UsrLog(
            uip,
            "%s: binding variable %s to %s at depth %d is an invalid XPath expression, aborting.",
            xmlCtx->inServiceOf, variableIndex, xPathHierarchy->pathExpr, xPathHierarchy->depth,
            value);
    } else {
        if (xpathObj->type == XPATH_NODESET) {
            nodes = xpathObj->nodesetval;
            if ((nodes != NULL) && (nodes->nodeNr > 0)) {
                variableIndexTerminus = strchr(variableIndex, ',');
                if (variableIndexTerminus != NULL) {
                    variableIndexNext = variableIndexTerminus + 1;
                } else {
                    variableIndexTerminus = strchr(variableIndex, ']');
                    variableIndexNext = variableIndexTerminus;
                }
                i = strtol(variableIndex, &p, 10);
                if (p != variableIndexTerminus) {
                    i = -1;
                    UsrLog(uip,
                           "%s: binding variable %s at depth %d, the index %s is not an integer.",
                           xmlCtx->inServiceOf, binding->ezpName, xPathHierarchy->pathExpr,
                           xPathHierarchy->depth, variableIndex);
                } else if (i > nodes->nodeNr) {
                    /* i addresses past the end of the existing children. */
                    node = xmlCopyNode(nodes->nodeTab[nodes->nodeNr - 1],
                                       1); /* copy recursively all children to create a new node */
                    xmlAddNextSibling(nodes->nodeTab[nodes->nodeNr - 1], node);
                    nodeTab = &node;
                    i = 0;
                    nodeNr = 1;
                    if (uip->debug) {
                        UsrLog(uip,
                               "%s: binding variable %s added a new node because the XPath "
                               "expression %s at depth %d found no existing node.",
                               xmlCtx->inServiceOf, binding->ezpName, xPathHierarchy->pathExpr,
                               xPathHierarchy->depth);
                    }
                } else if (i < 0) {
                    node = NULL;
                } else {
                    /* i addresses an existing child */
                    nodeTab = nodes->nodeTab;
                    node = nodes->nodeTab[i];
                    nodeNr = nodes->nodeNr;
                }
            } else {
                node = NULL;
            }

            if (debugLevel > 7000) {
                UsrLog(uip, "at depth %d XPath %s[%d] %s", xPathHierarchy->depth,
                       xPathHierarchy->pathExpr, i, tmpStr = UUxmlNodeName(node));
                xmlFreeThenNull(tmpStr);
            }
            if (node != NULL) {
                if (xPathHierarchy->next != NULL) {
                    if (result =
                            UsrXMLNewXpathCtx(t, uip, xmlCtx, xmlCtx->docOut, &(nextXPathCtx))) {
                    } else {
                        nextXPathCtx->node = node;
                        result = UsrXMLUpdateVectorVariableToXML(t, uip, xmlCtx, nextXPathCtx,
                                                                 variableIndexNext, binding,
                                                                 xPathHierarchy->next, value);
                        xmlXPathFreeContext(nextXPathCtx);
                        nextXPathCtx = NULL;
                    }
                } else {
                    result = UsrXMLUpdateDocumentNode(t, uip, xmlCtx, binding->ezpName,
                                                      binding->nullable, nodeTab, i, value);
                }
            }
        } else {
            uip->skipping = TRUE;
            result = STATEMENT_RESULT_FAILURE;
            UsrLog(uip,
                   "%s: binding variable %s to XPath expression %s at depth %d resulted in %s.  It "
                   "is not useful in this context.  Aborting.",
                   xmlCtx->inServiceOf, binding->ezpName, xPathHierarchy->pathExpr,
                   xPathHierarchy->depth, tmpStr = UUxmlXPathTypeToString(xpathObj->type));
            xmlFreeThenNull(tmpStr);
        }
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }
    if (result == STATEMENT_RESULT_FAILURE_INDEFINATE) {
        result = STATEMENT_RESULT_SUCCESS;
        if (uip->debug) {
            UsrLog(uip,
                   "%s: binding variable %s has no effect because the XPath expression %s at depth "
                   "%d selects no nodes, warning.",
                   xmlCtx->inServiceOf, binding->ezpName, xPathHierarchy->pathExpr,
                   xPathHierarchy->depth);
        }
    }

    return result;
}

void UsrXMLProcessBindingsOutToXMLFor1Variable(struct TRANSLATE *t,
                                               struct VARIABLES *v,
                                               void *context,
                                               const char *name,
                                               const char *value,
                                               int count) {
    struct UsrXMLProcessBindingsClosure_STATE *state =
        (struct UsrXMLProcessBindingsClosure_STATE *)context;
    struct XMLBINDING *binding;
    int subscriptCount;
    char *bracket;
    char *p;
    char *variableIndex;

    if (debugLevel > 7000) {
        UsrLog(state->uip, "%s: UsrXMLProcessBindingsOutToXMLFor1Variable, name=%s, value=%s.",
               state->xmlCtx->inServiceOf, NULLEMPTYFMTSTR(name), NULLEMPTYFMTSTR(value));
    }
    if (name != NULL) {
        bracket = strchr(name, '[');
        if (bracket != NULL) {
            variableIndex = bracket + 1;
            p = variableIndex;
            subscriptCount = 1;
            do {
                if ((p = strchr(p, ',')) != NULL) {
                    subscriptCount++;
                    p++;
                }
            } while (p != NULL);
        } else {
            variableIndex = NULL;
            subscriptCount = 0;
        }
        for (binding = state->xmlCtx->bindings;
             (state->result == STATEMENT_RESULT_SUCCESS) && binding; binding = binding->next) {
            if (binding->outDir &&
                (UsrXMLVariableNameCompare((char *)name, binding->ezpName) == 0)) {
                if (binding->varComplexity) {
                    state->count++;
                    state->result = UsrXMLUpdateScalarVariableToXML(
                        t, state->uip, state->xmlCtx, state->xmlCtx->xpathCtxOut, binding,
                        binding->pathHierarchy, (char *)value);
                } else if (binding->xPathDepth == subscriptCount) {
                    state->count++;
                    state->result = UsrXMLUpdateVectorVariableToXML(
                        t, state->uip, state->xmlCtx, state->xmlCtx->xpathCtxOut, variableIndex,
                        binding, binding->pathHierarchy, (char *)value);
                }
            }
        }
    }
}

STATEMENT_RESULT_TYPE UsrXMLProcessBindingsOutToXMLForAllVariables(struct TRANSLATE *t,
                                                                   struct USERINFO *uip,
                                                                   struct XMLCTX *xmlCtx) {
    struct UsrXMLProcessBindingsClosure_STATE state;
    int count;

    if (debugLevel > 7000) {
        UsrLog(uip, "%s: UsrXMLProcessBindingsOutToXMLForAllVariables.", xmlCtx->inServiceOf);
    }
    state.xmlCtx = xmlCtx;
    state.count = 0;
    state.uip = uip;
    state.result = STATEMENT_RESULT_SUCCESS;
    count =
        VariablesForAll(t, t->localVariables, &state, UsrXMLProcessBindingsOutToXMLFor1Variable);
    if (uip->debug) {
        UsrLog(uip, "%s: binding considered %d variables and acted on %d variables.",
               xmlCtx->inServiceOf, count, state.count);
    }

    return state.result;
}

STATEMENT_RESULT_TYPE UsrXMLAddVariableOutToURLEncoded(struct TRANSLATE *t,
                                                       struct USERINFO *uip,
                                                       struct XMLCTX *xmlCtx,
                                                       char *variableName,
                                                       char *fieldName,
                                                       struct XMLBINDING *binding,
                                                       struct XMLBINDINGHIERARCHY *xPathHierarchy,
                                                       char *val) {
    int l = 0;

    if ((l = strlen(xmlCtx->data)) != 0) {
        UsrXMLDataCat(t, uip, xmlCtx, "&");
    }

    UsrXMLDataCatURLEncoded(t, uip, xmlCtx, fieldName);
    UsrXMLDataCat(t, uip, xmlCtx, "=");
    UsrXMLDataCatURLEncoded(t, uip, xmlCtx, val);

    if (uip->debug) {
        UsrLog(uip,
               "%s: binding variable %s to path expression %s at depth %d appended \"%s\" to the "
               "URL encoded data.",
               xmlCtx->inServiceOf, variableName, xPathHierarchy->pathExpr, xPathHierarchy->depth,
               &(xmlCtx->data[l]));
    }

    return STATEMENT_RESULT_SUCCESS;
}

STATEMENT_RESULT_TYPE UsrXMLAddVariableOutToURLEncodedCheckName(
    struct TRANSLATE *t,
    struct USERINFO *uip,
    struct XMLCTX *xmlCtx,
    char *variableName,
    char *fieldName,
    char *fieldNameLastChar,
    struct XMLBINDING *binding,
    struct XMLBINDINGHIERARCHY *xPathHierarchy,
    char *val) {
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_FAILURE;

    if (!_SNPRINTF_IS_OVERFLOW(fieldNameLastChar, 1)) {
        result = UsrXMLAddVariableOutToURLEncoded(t, uip, xmlCtx, variableName, fieldName, binding,
                                                  binding->pathHierarchy, val);
    } else {
        uip->skipping = TRUE;
        UsrLog(uip, "%s: binding variable %s has a filed name that's too long, aborting.",
               xmlCtx->inServiceOf, variableName);
    }

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLUpdateVariableOutToURLEncoded(
    struct TRANSLATE *t,
    struct USERINFO *uip,
    struct XMLCTX *xmlCtx,
    char *variableIndex,
    char *fieldName,
    char *fieldNameLastChar,
    struct XMLBINDING *binding,
    struct XMLBINDINGHIERARCHY *xPathHierarchy,
    char *value) {
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_FAILURE_INDEFINATE;
    char *variableIndexTerminus;
    char *variableIndexNext = NULL;
    char *p;
    int fieldNameBase;
    int i = 0;

    if (debugLevel > 7000) {
        UsrLog(uip, "at depth %d path %s", xPathHierarchy->depth, xPathHierarchy->pathExpr);
    }
    if (xPathHierarchy->next != NULL) {
        fieldNameBase = strlen(fieldName);
        if (binding->varComplexity) {
            variableIndexTerminus = strchr(variableIndex, ',');
            if (variableIndexTerminus != NULL) {
                variableIndexNext = variableIndexTerminus + 1;
            } else {
                variableIndexTerminus = strchr(variableIndex, ']');
                variableIndexNext = variableIndexTerminus;
            }
            i = strtol(variableIndex, &p, 10);
            if (p != variableIndexTerminus) {
                i = -1;
            }
            if (i >= 0) {
                _snprintf(&(fieldName[fieldNameBase]),
                          (fieldNameLastChar - &(fieldName[fieldNameBase])) + 1, "(%d).%s", i,
                          xPathHierarchy->next->pathExpr);
            }
        } else {
            _snprintf(&(fieldName[fieldNameBase]),
                      (fieldNameLastChar - &(fieldName[fieldNameBase])) + 1, "().%s",
                      xPathHierarchy->next->pathExpr);
        }
        _SNPRINTF_END(fieldNameLastChar, 1);
        result = UsrXMLUpdateVariableOutToURLEncoded(t, uip, xmlCtx, variableIndexNext, fieldName,
                                                     fieldNameLastChar, binding,
                                                     xPathHierarchy->next, value);
        fieldName[fieldNameBase] = 0;
    } else {
        result = UsrXMLAddVariableOutToURLEncodedCheckName(t, uip, xmlCtx, binding->ezpName,
                                                           fieldName, fieldNameLastChar, binding,
                                                           binding->pathHierarchy, value);
    }
    if (result == STATEMENT_RESULT_FAILURE_INDEFINATE) {
        result = STATEMENT_RESULT_SUCCESS;
        if (uip->debug) {
            UsrLog(uip,
                   "%s: binding variable %s has no effect because the path expression %s at depth "
                   "%d selects no nodes, warning.",
                   xmlCtx->inServiceOf, binding->ezpName, xPathHierarchy->pathExpr,
                   xPathHierarchy->depth);
        }
    }

    return result;
}

void UsrXMLProcessBindingsOutToURLEncodedFor1Variable(struct TRANSLATE *t,
                                                      struct VARIABLES *v,
                                                      void *context,
                                                      const char *name,
                                                      const char *value,
                                                      int count) {
    struct UsrXMLProcessBindingsClosure_STATE *state =
        (struct UsrXMLProcessBindingsClosure_STATE *)context;
    struct XMLBINDING *binding;
    int subscriptCount;
    char *bracket;
    char *p;
    char *variableIndex;
    char *fieldName = t->buffer + strlen(t->buffer);
    char *fieldNameLastChar = fieldName + (88 - 1);

    if (debugLevel > 7000) {
        UsrLog(state->uip,
               "%s: UsrXMLProcessBindingsOutToURLEncodedFor1Variable, name=%s, value=%s.",
               state->xmlCtx->inServiceOf, NULLEMPTYFMTSTR(name), NULLEMPTYFMTSTR(value));
    }
    if (name != NULL) {
        bracket = strchr(name, '[');
        if (bracket != NULL) {
            variableIndex = bracket + 1;
            p = variableIndex;
            subscriptCount = 1;
            do {
                if ((p = strchr(p, ',')) != NULL) {
                    subscriptCount++;
                    p++;
                }
            } while (p != NULL);
        } else {
            variableIndex = NULL;
            subscriptCount = 0;
        }
        for (binding = state->xmlCtx->bindings;
             (state->result == STATEMENT_RESULT_SUCCESS) && binding; binding = binding->next) {
            if (binding->outDir &&
                (UsrXMLVariableNameCompare((char *)name, binding->ezpName) == 0)) {
                if ((binding->varComplexity) || (binding->xPathDepth == subscriptCount)) {
                    state->count++;
                    _snprintf(&(fieldName[0]), (fieldNameLastChar - &(fieldName[0])) + 1, "%s",
                              binding->ezpName);
                    _SNPRINTF_END(fieldNameLastChar, 1);
                    state->result = UsrXMLUpdateVariableOutToURLEncoded(
                        t, state->uip, state->xmlCtx, variableIndex, fieldName, fieldNameLastChar,
                        binding, binding->pathHierarchy, (char *)value);
                }
            }
        }
    }
}

STATEMENT_RESULT_TYPE UsrXMLProcessBindingsOutToURLEncodedForAllVariables(struct TRANSLATE *t,
                                                                          struct USERINFO *uip,
                                                                          struct XMLCTX *xmlCtx) {
    struct UsrXMLProcessBindingsClosure_STATE state;
    int count;

    state.xmlCtx = xmlCtx;
    state.count = 0;
    state.uip = uip;
    state.result = STATEMENT_RESULT_SUCCESS;
    count = VariablesForAll(t, t->localVariables, &state,
                            UsrXMLProcessBindingsOutToURLEncodedFor1Variable);
    if (uip->debug) {
        UsrLog(uip, "%s: binding considered %d variables and acted on %d variables.",
               xmlCtx->inServiceOf, count, state.count);
    }

    return state.result;
}

STATEMENT_RESULT_TYPE UsrXMLNewXpathCtx(struct TRANSLATE *t,
                                        struct USERINFO *uip,
                                        struct XMLCTX *xmlCtx,
                                        xmlDocPtr doc,
                                        xmlXPathContextPtr *xpathCtx) {
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_SUCCESS;
    struct XMLNS *namespace;

    if ((*xpathCtx = xmlXPathNewContext(doc)) == NULL) {
        uip->skipping = TRUE;
        result = STATEMENT_RESULT_FAILURE;
        UsrLog(uip, "%s: failed to obtain a new XPath context, aborting", xmlCtx->inServiceOf);
    } else {
        for (namespace = xmlCtx->xmlNs; namespace && (result == STATEMENT_RESULT_SUCCESS);
             namespace = namespace->next) {
            if (uip->debug) {
                UsrLog(uip, "%s: binding namespace ID \"%s\" to URN \"%s\".", xmlCtx->inServiceOf,
                       namespace->ns, namespace->urn);
            }
            if (xmlXPathRegisterNs(*xpathCtx, BAD_CAST namespace->ns, BAD_CAST namespace->urn) <
                0) {
                UsrLog(uip, "%s: binding namespace ID \"%s\" to URN \"%s\" has failed, aborting.",
                       xmlCtx->inServiceOf, namespace->ns, namespace->urn);
                uip->skipping = TRUE;
                result = STATEMENT_RESULT_FAILURE;
            }
        }
        if (result != STATEMENT_RESULT_SUCCESS) {
            if (*xpathCtx != NULL) {
                xmlXPathFreeContext(*xpathCtx);
            }
        }
    }

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLOutDocDefaultsFromSchema(struct TRANSLATE *t,
                                                     struct USERINFO *uip,
                                                     void *context,
                                                     char *arg) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    struct USRXML_LIBXML_CALLBACK_CONTEXT xmlCallbackCtx, *xmlCallbackCtxPtr = &xmlCallbackCtx;
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_FAILURE_INDEFINATE;
    char *p;
    int rc;
    char *customFile = NULL;
    xmlSchemaParserCtxtPtr pctxt = NULL;
    xmlSchemaValidCtxtPtr vctxt = NULL;
    xmlSchemaPtr schema = NULL;

    /*
        UsrLog(uip, "%s: the %s document was not created using the %s XSD because this operation is
       not yet implemented, aborting.", xmlCtx->inServiceOf, xmlCtx->inServiceOf, customFile);
        uip->skipping = TRUE;
        return STATEMENT_RESULT_FAILURE;
    */

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    if ((arg != NULL) && (arg[0] != 0)) {
        /* Create a new document by way of the given schema. */
        if (t->docsCustomDir && *t->docsCustomDir && (p = StartsIWith(arg, XMLDIR)) &&
            strchr(p, '/') == NULL && strchr(p, '\\') == NULL) {
            if (!(customFile = malloc(strlen(arg) + strlen(t->docsCustomDir) + 32)))
                PANIC;
            sprintf(customFile, "%scustom%s%s%s%s", XMLDIR, DIRSEP, t->docsCustomDir, DIRSEP, p);
        } else {
            /* Absolute path or no custom docs dir */
            if (!(customFile = strdup(arg)))
                PANIC;
        }
        if ((pctxt = xmlSchemaNewParserCtxt(customFile)) != NULL) {
            xmlCallbackCtxPtr = &xmlCallbackCtx;
            memset(xmlCallbackCtxPtr, 0, sizeof(*xmlCallbackCtxPtr));
            memcpy(xmlCallbackCtx.structureIdentifier, USRXML_LIBXML_CALLBACK_CONTEXT_STRUCT_ID,
                   sizeof(xmlCallbackCtx.structureIdentifier));
            xmlCallbackCtx.customFile = customFile;
            xmlCallbackCtx.uip = uip;
            xmlCallbackCtx.xmlCtx = xmlCtx;
            xmlCallbackCtx.result = result;
            xmlCallbackCtx.resultIfParseFailure = STATEMENT_RESULT_FAILURE;
            xmlSchemaSetParserErrors(pctxt, UsrXMLErrorCallback, UsrXMLWarningCallback,
                                     &xmlCallbackCtx);
            schema = xmlSchemaParse(pctxt);
            result = xmlCallbackCtx.result;
            if ((schema != NULL) && (result == STATEMENT_RESULT_FAILURE_INDEFINATE)) {
                if ((vctxt = xmlSchemaNewValidCtxt(schema)) != NULL) {
                    /* populate the XML tree with default values when a validity check is done */
                    xmlSchemaSetValidOptions(vctxt, XML_SCHEMA_VAL_VC_I_CREATE);
                    if (0 != (rc = xmlSchemaValidateDoc(vctxt, xmlCtx->docOut))) {
                        uip->skipping = TRUE;
                        UsrLog(
                            uip,
                            "%s: the %s document can't be updated using the %s XSD, aborting (%d)",
                            xmlCtx->inServiceOf, xmlCtx->inServiceOf, customFile, rc);
                    } else {
                        xmlCtx->newXMLDocOut = TRUE;
                        result = STATEMENT_RESULT_SUCCESS;
                        if (uip->debug) {
                            UsrLog(uip, "%s: the %s document was updated using the %s XSD",
                                   xmlCtx->inServiceOf, xmlCtx->inServiceOf, customFile);
                        }
                    }
                } else {
                    uip->skipping = TRUE;
                    UsrLog(uip, "%s: schema %s xmlSchemaNewValidCtxt failed, aborting.",
                           xmlCtx->inServiceOf, customFile);
                }
            } else {
                uip->skipping = TRUE;
                UsrLog(uip, "%s: schema %s parse failed, aborting.", xmlCtx->inServiceOf,
                       customFile);
            }
        } else {
            uip->skipping = TRUE;
            UsrLog(uip, "%s: schema %s xmlSchemaNewParserCtxt failed, aborting.",
                   xmlCtx->inServiceOf, customFile);
        }
    } else if (xmlCtx->schemaOut != NULL) {
        /* Create a new document by way of the assigned schema. */
        if ((vctxt = xmlSchemaNewValidCtxt(xmlCtx->schemaOut)) != NULL) {
            /* populate the XML tree with default values when a validity check is done */
            xmlSchemaSetValidOptions(vctxt, XML_SCHEMA_VAL_VC_I_CREATE);
            if (0 != xmlSchemaValidateDoc(vctxt, xmlCtx->docOut)) {
                uip->skipping = TRUE;
                UsrLog(uip, "%s: the XML document can't be updated using the %s XSD, aborting",
                       xmlCtx->inServiceOf, xmlCtx->schemaOutName);
            } else {
                xmlCtx->newXMLDocOut = TRUE;
                result = STATEMENT_RESULT_SUCCESS;
                if (uip->debug) {
                    UsrLog(uip, "%s: the %s document was updated using the %s XSD",
                           xmlCtx->inServiceOf, xmlCtx->inServiceOf, xmlCtx->schemaOutName);
                }
            }
        } else {
            uip->skipping = TRUE;
            UsrLog(uip, "%s: schema %s xmlSchemaNewValidCtxt failed, aborting.",
                   xmlCtx->inServiceOf, customFile);
        }
    } else {
        uip->skipping = TRUE;
        UsrLog(uip, "%s: can't updated the %s document, aborting.", xmlCtx->inServiceOf,
               xmlCtx->inServiceOf);
    }

    if (result == STATEMENT_RESULT_SUCCESS) {
        result = UsrXMLNewXpathCtx(t, uip, xmlCtx, xmlCtx->docOut, &(xmlCtx->xpathCtxOut));
    }

    FreeThenNull(customFile);
    if (pctxt != NULL) {
        xmlSchemaFreeParserCtxt(pctxt);
    }
    if (vctxt != NULL) {
        xmlSchemaFreeValidCtxt(vctxt);
    }
    if (schema != NULL) {
        xmlSchemaFree(schema);
        schema = NULL;
    }

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLNewOutDocFromTemplate(struct TRANSLATE *t,
                                                  struct USERINFO *uip,
                                                  void *context,
                                                  char *arg) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    struct USRXML_LIBXML_CALLBACK_CONTEXT xmlCallbackCtx, *xmlCallbackCtxPtr = &xmlCallbackCtx;
    // xmlSAXHandler hdlr;
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_FAILURE;
    char *p;
    char *customFile = NULL;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    UsrXMLClearXMLOut(xmlCtx);

    if ((arg != NULL) && (arg[0] != 0)) {
        /* Create a new document by way of the given schema. */
        if (t->docsCustomDir && *t->docsCustomDir && (p = StartsIWith(arg, XMLDIR)) &&
            strchr(p, '/') == NULL && strchr(p, '\\') == NULL) {
            if (!(customFile = malloc(strlen(arg) + strlen(t->docsCustomDir) + 32)))
                PANIC;
            sprintf(customFile, "%scustom%s%s%s%s", XMLDIR, DIRSEP, t->docsCustomDir, DIRSEP, p);
        } else {
            /* Absolute path or no custom docs dir */
            if (!(customFile = strdup(arg)))
                PANIC;
        }
        memset(xmlCallbackCtxPtr, 0, sizeof(*xmlCallbackCtxPtr));
        memcpy(xmlCallbackCtx.structureIdentifier, USRXML_LIBXML_CALLBACK_CONTEXT_STRUCT_ID,
               sizeof(xmlCallbackCtx.structureIdentifier));
        xmlCallbackCtx.customFile = xmlCtx->url;
        xmlCallbackCtx.uip = uip;
        xmlCallbackCtx.xmlCtx = xmlCtx;
        xmlCallbackCtx.result = STATEMENT_RESULT_SUCCESS;
        xmlCallbackCtx.resultIfParseFailure = STATEMENT_RESULT_FAILURE;
        xmlCtx->docOut =
            xmlSAXParseFileWithData(NULL /* UsrXMLSAXInitSAXHandler(&xmlCallbackCtx, uip, &hdlr) */,
                                    customFile, XML_NEED_NOT_BE_WELLFORMED, &xmlCallbackCtx);
        result = xmlCallbackCtx.result;
        if ((xmlCtx->docOut != NULL) && (result == STATEMENT_RESULT_SUCCESS)) {
            xmlCtx->newXMLDocOut = TRUE;
            if (uip->debug) {
                UsrLog(uip, "%s: the %s document was created using '%s'.", xmlCtx->inServiceOf,
                       xmlCtx->inServiceOf, customFile);
            }
        } else {
            uip->skipping = TRUE;
            UsrLog(uip, "%s: document %s parsing failed, aborting.", xmlCtx->inServiceOf,
                   customFile);
        }
    } else if ((xmlCtx->docOut = xmlNewDoc(BAD_CAST "1.0")) != NULL) {
        /* Created a new document with no schema. */
        xmlCtx->newXMLDocOut = TRUE;
        result = STATEMENT_RESULT_SUCCESS;
        if (uip->debug) {
            UsrLog(uip, "%s: the %s document was created", xmlCtx->inServiceOf,
                   xmlCtx->inServiceOf);
        }
    } else {
        uip->skipping = TRUE;
        UsrLog(uip, "%s: can't create a new %s document, aborting.", xmlCtx->inServiceOf,
               xmlCtx->inServiceOf);
    }

    if (result == STATEMENT_RESULT_SUCCESS) {
        result = UsrXMLNewXpathCtx(t, uip, xmlCtx, xmlCtx->docOut, &(xmlCtx->xpathCtxOut));
    }

    FreeThenNull(customFile);

    return result;
}

/*
    const xmlChar *ExternalID: External identifier for PUBLIC DTD, e.g., "-//SGMLSOURCE//DTD
   DEMO//EN"
    const xmlChar *SystemID: URI for a SYSTEM or PUBLIC DTD, a filename or URL, e.g.,
   "http://www.sgmlsource.com/dtds/memo.dtd"

    A document may have any of:

    <!DOCTYPE rootElementName PUBLIC "publicID" "systemID">
    <!DOCTYPE rootElementName SYSTEM "systemID">
    <!DOCTYPE rootElementName>
 */
STATEMENT_RESULT_TYPE UsrXMLValidateDocumentByDTD(struct TRANSLATE *t,
                                                  struct USERINFO *uip,
                                                  struct XMLCTX *xmlCtx,
                                                  xmlDtdPtr dtd,
                                                  char *systemID,
                                                  xmlDocPtr doc) {
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_FAILURE;
    xmlValidCtxtPtr vctxt = NULL;
    int rc;

    if ((vctxt = xmlNewValidCtxt()) != NULL) {
        if (1 != (rc = xmlValidateDtd(vctxt, doc, dtd))) {
            uip->skipping = TRUE;
            UsrLog(uip, "%s: the document can't be validated against the %s DTD, aborting",
                   xmlCtx->inServiceOf, systemID);
        } else {
            result = STATEMENT_RESULT_SUCCESS;
            if (uip->debug) {
                UsrLog(uip,
                       "%s: DTD %s has been used to successfully validate the incoming document.",
                       xmlCtx->inServiceOf, systemID);
            }
        }
    } else {
        PANIC;
    }
    if (vctxt != NULL) {
        xmlFreeValidCtxt(vctxt);
    }

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLValidateDocumentBySchema(struct TRANSLATE *t,
                                                     struct USERINFO *uip,
                                                     struct XMLCTX *xmlCtx,
                                                     xmlSchemaPtr schema,
                                                     char *schemaName,
                                                     xmlDocPtr doc) {
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_FAILURE;
    xmlSchemaValidCtxtPtr vctxt = NULL;

    if ((vctxt = xmlSchemaNewValidCtxt(schema)) != NULL) {
        if (0 != xmlSchemaValidateDoc(vctxt, doc)) {
            uip->skipping = TRUE;
            UsrLog(uip, "%s: the document can't be validated against the %s XSD, aborting",
                   xmlCtx->inServiceOf, schemaName);
        } else {
            result = STATEMENT_RESULT_SUCCESS;
            if (uip->debug) {
                UsrLog(
                    uip,
                    "%s: schema %s has been used to successfully validate the incoming document.",
                    xmlCtx->inServiceOf, schemaName);
            }
        }
    } else {
        PANIC;
    }
    if (vctxt != NULL) {
        xmlSchemaFreeValidCtxt(vctxt);
    }

    return result;
}

/* Convert form a string list to a string of lines. */
char *XMLStringListToString(char *stringList) {
    char *bol;
    char *eol;
    for (bol = stringList, eol = strchr(bol, 0); *bol; bol = ++eol, eol = strchr(bol, 0)) {
        *eol = '\n';
    }
    return stringList;
}

/* Convert form a string of lines to a string list.  It is assumed
 * that the string ends with the two bytes: newline, zero-byte. */
char *XMLStringToStringList(char *string) {
    char *bol;
    char *eol;
    for (bol = string, eol = strchr(bol, '\n'); eol; bol = ++eol, eol = strchr(bol, '\n')) {
        *eol = 0;
    }
    return string;
}

STATEMENT_RESULT_TYPE UsrXMLGETorPOST(struct TRANSLATE *t,
                                      struct USERINFO *uip,
                                      void *context,
                                      char *method,
                                      BOOL onlyGet,
                                      char *arg) {
#define PROTOCOL_SIMPLE 1
#define PROTOCOL_HTTP 0
#define PROTOCOL_MODE_SEND_NO_DATA 0
#define PROTOCOL_MODE_SEND_DATA 1
    char *modeName[2][2] = {
        {"GET",     "PUT" },
        {"REQUEST", "SEND"}
    };
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    struct UUSOCKET socketForXMLTransaction, *socketForXMLTransactionPtr = &socketForXMLTransaction;
    BOOL inHeader;
    BOOL socketOpen = FALSE;
    int wellFormedXML = 0;
    int rc;
    int remain;
    char *field;
    char *bol, *eol;
    char *host, *colon, *endOfHost, *endOfHttpResponseCode;
    char *contentType = NULL;
    char useSsl;
    PORT port;
    char save = 0;
    char saveEndOfHttpResponseCode = 0;
    int protocol = 0; /* 1 = simple, 0 = HTTP */
    int mode = 0;     /* 1 = data to send, 0 = no data to send */
    STATEMENT_RESULT_TYPE result;
    int responseLength = 0;
    int urlLen = 0; /* remember where the URL ends. */
    int len;
    char *httpResponseCode = NULL;
    char *headers = NULL;
    int headersSize;
    int contentLength;
    char *buffer = NULL;
    // xmlSAXHandler hdlr;
    xmlParserCtxtPtr ctxt = NULL;
    char *scratchPad = t->buffer;
    char *scratchPadLastChar =
        scratchPad +
        (sizeof(t->buffer) - 1); /* SCRATCHPAD2_SIZE is room for an HTTP response code */
    struct USRXML_LIBXML_CALLBACK_CONTEXT xmlCallbackCtx, *xmlCallbackCtxPtr = &xmlCallbackCtx;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    result = STATEMENT_RESULT_FAILURE;
    uip->skipping = TRUE;

    if (!onlyGet) {
        modeName[0][1] = method;
    }

    if (uip->debug) {
        UsrLog(uip, "%s %s: server address%s: %s", xmlCtx->inServiceOf, modeName[0][1],
               ((arg == NULL) ? "" : " before appending arg"), xmlCtx->url);
    }

    urlLen = strlen(xmlCtx->url);       /* remember where the URL ends. */
    UsrXMLURLCat(t, uip, context, arg); /* the "arg" is temporarily appended to xmlCtx->url. */

    if (uip->debug) {
        if (arg != NULL) {
            UsrLog(uip, "%s %s: server address after appending arg: %s", xmlCtx->inServiceOf,
                   modeName[0][1], xmlCtx->url);
        }
    }

    if (!onlyGet) {
        if ((xmlCtx->data[0] != 0) && (xmlCtx->newXMLDocOut)) {
            UsrLog(uip,
                   "%s %s: you've given both an %s document and some raw data to %s, aborting.",
                   xmlCtx->inServiceOf, modeName[0][1], xmlCtx->inServiceOf, modeName[0][1]);
            goto Finished;
        } else if (xmlCtx->data[0] != 0) {
            contentType = xmlCtx->contentTypeForPOST;
            if (!(buffer = strdup(xmlCtx->data)))
                PANIC;
        } else if (xmlCtx->newXMLDocOut) {
            if (optionLogXML) {
                LogXMLDoc(xmlCtx->docOut, NULL);
            }
            contentType = xmlCtx->contentTypeForDocument;
            if (result = UsrXMLProcessBindingsOutToXMLForAllVariables(t, uip, xmlCtx)) {
                goto Finished;
            }
            if (xmlCtx->schemaOutValidation) {
                if (result = UsrXMLValidateDocumentBySchema(
                        t, uip, xmlCtx, xmlCtx->schemaOut, xmlCtx->schemaOutName, xmlCtx->docOut)) {
                    goto Finished;
                }
            }
            if (xmlCtx->dtdOutValidation) {
                if (result = UsrXMLValidateDocumentByDTD(t, uip, xmlCtx, xmlCtx->dtdOut,
                                                         xmlCtx->dtdOutName, xmlCtx->docOut)) {
                    goto Finished;
                }
            }
            xmlDocDumpMemory(xmlCtx->docOut, ((xmlChar **)&buffer), &len);
        } else if (xmlCtx->anyOutBindings) {
            if (xmlCtx->data[0] != 0) {
                if (result = UsrXMLProcessBindingsOutToURLEncodedForAllVariables(t, uip, xmlCtx)) {
                    goto Finished;
                }
                contentType = xmlCtx->contentTypeForPOST;
                if (!(buffer = strdup(xmlCtx->data)))
                    PANIC;
            }
        } else {
            contentType = xmlCtx->contentTypeForPOST;
        }
    }

    mode = (buffer == NULL) ? PROTOCOL_MODE_SEND_NO_DATA : PROTOCOL_MODE_SEND_DATA;
    host = SkipHTTPslashesAt(xmlCtx->url, &useSsl);
    protocol = host == xmlCtx->url ? PROTOCOL_SIMPLE : PROTOCOL_HTTP;

    ParseHost(host, &colon, &endOfHost, 0);

    port = 0;
    if (endOfHost) {
        save = *endOfHost;
        *endOfHost = 0;
    }
    if (colon) {
        *colon = 0;
        port = atoi(colon + 1);
    } else if (protocol == PROTOCOL_HTTP) {
        port = (useSsl ? 443 : 80);
    }

    if (port == 0) {
        if (uip->debug) {
            UsrLog(uip, "%s %s: port missing, assume 80", xmlCtx->inServiceOf,
                   modeName[protocol][mode]);
        }
        port = 80;
    }

    if (uip->debug) {
        UsrLog(uip, "%s %s: connect with %s:%d useSsl=%d.", xmlCtx->inServiceOf,
               modeName[protocol][mode], host, port, useSsl);
    }
    UUInitSocket(socketForXMLTransactionPtr, INVALID_SOCKET);
    socketOpen = TRUE;
    uip->result = resultRefused;
    if (UUConnectWithSource5(socketForXMLTransactionPtr, host, port, xmlCtx->inServiceOf,
                             xmlCtx->pInterface, useSsl, xmlCtx->sslIdx, uip->connectionsTries,
                             uip->connectionsTimeout)) {
        UsrLog(uip, "%s %s: connection cannot be established.", xmlCtx->inServiceOf,
               modeName[protocol][mode]);
        uip->skipping = FALSE;
        goto Finished;
    }
    uip->result = resultInvalid;

    _SNPRINTF_BEGIN(scratchPadLastChar, 1);
    scratchPad[0] = 0;
    if (protocol == PROTOCOL_HTTP) {
        /* HTTP METHOD */
        _snprintf(scratchPad + strlen(scratchPad),
                  (&(scratchPadLastChar[0]) - &(scratchPad[strlen(scratchPad)])) + 1, "%s ",
                  modeName[protocol][mode]);
        _SNPRINTF_PANIC_OVERFLOW(scratchPadLastChar, 1);
        _SNPRINTF_END(scratchPadLastChar, 1);

        /* HTTP RESOURCE */
        if (endOfHost) {
            *endOfHost = save;
            _snprintf(scratchPad + strlen(scratchPad),
                      (&(scratchPadLastChar[0]) - &(scratchPad[strlen(scratchPad)])) + 1, "%s",
                      endOfHost);
            _SNPRINTF_PANIC_OVERFLOW(scratchPadLastChar, 1);
            _SNPRINTF_END(scratchPadLastChar, 1);
            *endOfHost = 0;
        } else {
            _snprintf(scratchPad + strlen(scratchPad),
                      (&(scratchPadLastChar[0]) - &(scratchPad[strlen(scratchPad)])) + 1, "/");
            _SNPRINTF_PANIC_OVERFLOW(scratchPadLastChar, 1);
            _SNPRINTF_END(scratchPadLastChar, 1);
        }

        /* HTTP HEADERS */
        _snprintf(scratchPad + strlen(scratchPad),
                  (&(scratchPadLastChar[0]) - &(scratchPad[strlen(scratchPad)])) + 1,
                  " HTTP/1.0\r\n"
                  "Host: %s\r\n"
                  "Pragma: no-cache\r\n"
                  "User-Agent: Mozilla/4.0 (compatible; EZproxy)\r\n"
                  "Accept: */*\r\n"
                  "Connection: close\r\n"
                  "Cache-Control: no-cache\r\n",
                  host);
        _SNPRINTF_PANIC_OVERFLOW(scratchPadLastChar, 1);
        _SNPRINTF_END(scratchPadLastChar, 1);
        if (xmlCtx->outgoingHeaders[0] != 0) {
            _snprintf(scratchPad + strlen(scratchPad),
                      (&(scratchPadLastChar[0]) - &(scratchPad[strlen(scratchPad)])) + 1, "%s",
                      xmlCtx->outgoingHeaders);
            _SNPRINTF_PANIC_OVERFLOW(scratchPadLastChar, 1);
            _SNPRINTF_END(scratchPadLastChar, 1);
        }
        if (mode == PROTOCOL_MODE_SEND_DATA) {
            _snprintf(scratchPad + strlen(scratchPad),
                      (&(scratchPadLastChar[0]) - &(scratchPad[strlen(scratchPad)])) + 1,
                      "Content-Type: %s\r\n", contentType);
            _SNPRINTF_PANIC_OVERFLOW(scratchPadLastChar, 1);
            _SNPRINTF_END(scratchPadLastChar, 1);
            _snprintf(scratchPad + strlen(scratchPad),
                      (&(scratchPadLastChar[0]) - &(scratchPad[strlen(scratchPad)])) + 1,
                      "Content-Length: %d\r\n", (int)strlen(buffer));
            _SNPRINTF_PANIC_OVERFLOW(scratchPadLastChar, 1);
            _SNPRINTF_END(scratchPadLastChar, 1);
        }
        UUSendZ(socketForXMLTransactionPtr, scratchPad);
        VariablesSendHttpHeaders(t);

        /* HTTP END OF HEADERS, START OF DATA */
        UUSendCRLF(socketForXMLTransactionPtr);
    }
    if (mode == PROTOCOL_MODE_SEND_DATA) {
        /* DATA */
        UUSendZ(socketForXMLTransactionPtr, buffer);
    }
    UUFlush(socketForXMLTransactionPtr);

    if (uip->debug) {
        if (protocol == PROTOCOL_HTTP) {
            UsrLog(uip, "%s %s: HTTP headers sent", xmlCtx->inServiceOf, modeName[protocol][mode]);
            UsrLog(uip, "%s", scratchPad);
        }
        if (mode == PROTOCOL_MODE_SEND_DATA) {
            if (uip->debug) {
                UsrLog(uip, "%s %s: raw data sent", xmlCtx->inServiceOf, modeName[protocol][mode]);
                UsrLog(uip, "%s", NULLEMPTYFMTSTR(buffer));
            }
        }
    }

    UsrXMLClearXMLIn(xmlCtx);

    _SNPRINTF_BEGIN(scratchPadLastChar, 1);
    if (protocol == PROTOCOL_HTTP) {
        if (uip->debug) {
            UsrLog(uip, "%s %s: HTTP response headers received", xmlCtx->inServiceOf,
                   modeName[protocol][mode]);
        }
        scratchPad[0] = 0;
        remain = ((scratchPadLastChar - scratchPad) + 1) -
                 1; /* -1 for an extra terminating null string; it's a string list. */
        inHeader = 1;
        headers = scratchPad;
        for (bol = scratchPad; remain > 1;) {
            if (!ReadLine(socketForXMLTransactionPtr, bol, remain)) {
                if (protocol == PROTOCOL_HTTP) {
                    UsrLog(uip, "%s %s: headers not terminated with a blank line, warning",
                           xmlCtx->inServiceOf, modeName[protocol][mode]);
                }
                break;
            }

            UURandomForSsl(bol, -1);
            if (*bol == 0) {
                break; /* a blank line, end of HTTP header */
            }

            eol = strchr(bol, 0);
            *++eol =
                0; /* because it's really a list of strings, and a NULL string means end-of-list. */
            remain -= (eol - bol);

            if ((inHeader == 1) && ((strnicmp(bol, "HTTP/", 5) == 0))) {
                inHeader = 2;

                httpResponseCode = SkipWS(SkipNonWS(bol));
                endOfHttpResponseCode = SkipNonWS(httpResponseCode);
                saveEndOfHttpResponseCode = *endOfHttpResponseCode;
                *endOfHttpResponseCode = 0;
                if (!(httpResponseCode = strdup(httpResponseCode)))
                    PANIC;
                *endOfHttpResponseCode = saveEndOfHttpResponseCode;
            }

            bol = eol;
        }
        headersSize = (bol - headers) + 1;
        if (!(xmlCtx->incomingHeaders = malloc(headersSize)))
            PANIC;
        memcpy(xmlCtx->incomingHeaders, headers, headersSize);
        LinkFields(xmlCtx->incomingHeaders); /* append continuation lines to their previous line. */

        if (uip->debug) {
            xmlCtx->incomingHeaders = XMLStringListToString(xmlCtx->incomingHeaders);
            UsrLog(uip, "%s", xmlCtx->incomingHeaders);
            xmlCtx->incomingHeaders = XMLStringToStringList(xmlCtx->incomingHeaders);
        }

        contentLength = (field = FindField("Content-Length", xmlCtx->incomingHeaders)) != NULL
                            ? atoi(field)
                            : -1; /* -1 so that decrementing it will not reach zero. */
        if (uip->debug) {
            UsrLog(uip, "%s %s: auth:http:Content-Length is %d", xmlCtx->inServiceOf,
                   modeName[protocol][mode], contentLength);
        }
        if (_SNPRINTF_IS_OVERFLOW(scratchPadLastChar, 1)) {
            UsrLog(uip, "%s %s: response header text is too long, aborting.", xmlCtx->inServiceOf,
                   modeName[protocol][mode]);
            goto Finished;
        }
    } else {
        if (!(xmlCtx->incomingHeaders = malloc(1)))
            PANIC;
        xmlCtx->incomingHeaders[0] = 0; /* A string list with one empty string. */
        contentLength = -1;             /* -1 so that decrementing it will not reach zero. */
    }
    _SNPRINTF_END(scratchPadLastChar, 1);

    if (uip->debug) {
        UsrLog(uip, "%s %s: response raw data received", xmlCtx->inServiceOf,
               modeName[protocol][mode]);
    }
    _SNPRINTF_BEGIN(scratchPadLastChar, 1);
    scratchPad[0] = 0;
    while ((contentLength != 0) &&
           ((rc = UURecv(socketForXMLTransactionPtr, scratchPad,
                         ((scratchPadLastChar - scratchPad) - 1) - 1, 0)) > 0)) {
        scratchPad[rc] = 0;
        if (uip->debug) {
            UsrLog(uip, "%s", scratchPad);
        }
        if (ctxt == NULL) {
            memset(xmlCallbackCtxPtr, 0, sizeof(*xmlCallbackCtxPtr));
            memcpy(xmlCallbackCtx.structureIdentifier, USRXML_LIBXML_CALLBACK_CONTEXT_STRUCT_ID,
                   sizeof(xmlCallbackCtx.structureIdentifier));
            xmlCallbackCtx.customFile = xmlCtx->url;
            xmlCallbackCtx.uip = uip;
            xmlCallbackCtx.xmlCtx = xmlCtx;
            ctxt = xmlCreatePushParserCtxt(
                NULL /* UsrXMLSAXInitSAXHandler(&xmlCallbackCtx, uip, &hdlr) */, &xmlCallbackCtx,
                scratchPad, rc, NULL);
            xmlCtxtUseOptions(ctxt, XML_PARSE_RECOVER | XML_PARSE_NOENT | XML_PARSE_NONET);
        } else {
            xmlParseChunk(ctxt, scratchPad, rc, FALSE);
        }
        responseLength += rc;
        if ((contentLength > 0) && ((contentLength - rc) < 0)) {
            contentLength = 0;
            if (uip->debug) {
                UsrLog(uip,
                       "%s %s: the actual content length is greater than the headers "
                       "Content-Length value, warning.",
                       xmlCtx->inServiceOf, modeName[protocol][mode]);
            }
        } else {
            contentLength -= rc;
        }
    }
    _SNPRINTF_END(scratchPadLastChar, 1);

    UUStopSocket(socketForXMLTransactionPtr, 0);
    socketOpen = FALSE;

    if (contentLength > 0) {
        if (uip->debug) {
            UsrLog(uip,
                   "%s %s: the actual content length is less than the headers Content-Length "
                   "value, warning.",
                   xmlCtx->inServiceOf, modeName[protocol][mode]);
        }
    }

    if (responseLength > 0) {
        if (ctxt != NULL) {
            scratchPad[0] = 0;
            xmlParseChunk(ctxt, scratchPad, 0, TRUE);
            xmlCtx->docIn = ctxt->myDoc;
        }
        if (xmlCtx->docIn != NULL) {
            wellFormedXML = ctxt->wellFormed;
            if (optionLogXML) {
                LogXMLDoc(xmlCtx->docIn, NULL);
            }
            if (result = UsrXMLNewXpathCtx(t, uip, xmlCtx, xmlCtx->docIn, &(xmlCtx->xpathCtxIn))) {
                goto Finished;
            }
            if (xmlCtx->schemaInValidation) {
                if (result = UsrXMLValidateDocumentBySchema(t, uip, xmlCtx, xmlCtx->schemaIn,
                                                            xmlCtx->schemaInName, xmlCtx->docIn)) {
                    goto Finished;
                }
            }
            if (xmlCtx->dtdInValidation) {
                if (result = UsrXMLValidateDocumentByDTD(t, uip, xmlCtx, xmlCtx->dtdIn,
                                                         xmlCtx->dtdInName, xmlCtx->docIn)) {
                    goto Finished;
                }
            }
            if (result = UsrXMLProcessBindingsInFromXML(t, uip, xmlCtx)) {
                goto Finished;
            }
            scratchPad[0] = 0; /* The raw XML is not retained in the buffer.  */
        } else {
            if (optionLogXML) {
                LogXMLDoc(NULL, (xmlChar *)t->buffer);
            }
            /* responseLength+1 is because the data will be converted to a string list to
             * accommodate calls to NextKEV() in UsrXMLProcessBindingsInFromURLEncodedData() and the
             * list needs to be terminated by a null string. */
            scratchPad[responseLength + 1] = 0;
            if (result = UsrXMLProcessBindingsInFromURLEncodedData(t, uip, xmlCtx)) {
                goto Finished;
            }
            scratchPad[0] = 0; /* The raw data is not retained in the buffer.  */
        }
    }

    result = STATEMENT_RESULT_SUCCESS;
    uip->skipping = FALSE;

Finished:

    _SNPRINTF_BEGIN(scratchPadLastChar, 1);
    _snprintf(scratchPad, (scratchPadLastChar - scratchPad) + 1, "%d", responseLength);
    if (!_SNPRINTF_IS_OVERFLOW(scratchPadLastChar, 1)) {
        _SNPRINTF_END(scratchPadLastChar, 1);
        if (UsrXMLSetLocalVar(t, uip, xmlCtx, "responseLength", NULL, scratchPad, FALSE, FALSE)) {
            UsrLog(uip, "%s: can't set responseLength = '%s'", xmlCtx->inServiceOf, scratchPad);
            uip->skipping = TRUE;
            result = STATEMENT_RESULT_FAILURE;
        }
    } else {
        PANIC;
    }

    if (httpResponseCode == NULL) {
        endOfHttpResponseCode = "999";
    } else {
        endOfHttpResponseCode = httpResponseCode;
    }
    if (UsrXMLSetLocalVar(t, uip, xmlCtx, "httpResponseCode", NULL, endOfHttpResponseCode, FALSE,
                          FALSE)) {
        result = STATEMENT_RESULT_FAILURE;
        uip->skipping = TRUE;
        UsrLog(uip, "%s %s: can't set httpResponseCode = '%s', aborting", xmlCtx->inServiceOf,
               modeName[protocol][mode], httpResponseCode);
    }
    if (endOfHttpResponseCode == httpResponseCode) {
        FreeThenNull(httpResponseCode);
    }

    _SNPRINTF_BEGIN(scratchPadLastChar, 1);
    _snprintf(scratchPad, (scratchPadLastChar - scratchPad) + 1, "%d", wellFormedXML);
    if (!_SNPRINTF_IS_OVERFLOW(scratchPadLastChar, 1)) {
        _SNPRINTF_END(scratchPadLastChar, 1);
        if (UsrXMLSetLocalVar(t, uip, xmlCtx, "wellFormedXML", NULL, scratchPad, FALSE, FALSE)) {
            UsrLog(uip, "%s: can't set wellFormedXML = %s", xmlCtx->inServiceOf, scratchPad);
            uip->skipping = TRUE;
            result = STATEMENT_RESULT_FAILURE;
        }
    } else {
        PANIC;
    }

    FreeThenNull(buffer);

    if (endOfHost) {
        *endOfHost = save;
        endOfHost = NULL;
    }

    if (colon) {
        *colon = ':';
        colon = NULL;
    }

    if (socketOpen) {
        UUStopSocket(socketForXMLTransactionPtr, 0);
    }

    if (ctxt != NULL) {
        xmlFreeParserCtxt(ctxt);
    }

    xmlCtx->url[urlLen] = 0; /* remove the "arg" that was appended. */

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLGET(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                void *context,
                                char *arg) {
    return UsrXMLGETorPOST(t, uip, context, "POST", TRUE, arg);
}

STATEMENT_RESULT_TYPE UsrXMLPOST(struct TRANSLATE *t,
                                 struct USERINFO *uip,
                                 void *context,
                                 char *arg) {
    return UsrXMLGETorPOST(t, uip, context, "POST", FALSE, arg);
}

STATEMENT_RESULT_TYPE UsrXMLPUT(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                void *context,
                                char *arg) {
    return UsrXMLGETorPOST(t, uip, context, "PUT", FALSE, arg);
}

STATEMENT_RESULT_TYPE UsrXMLBinding(struct TRANSLATE *t,
                                    struct USERINFO *uip,
                                    void *context,
                                    char **argList) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    STATEMENT_RESULT_TYPE result;
    struct XMLBINDING *newBinding, *binding, *nextBinding;
    struct XMLBINDINGHIERARCHY *newXPathBindingPoint, *xPathBindingPoint, *prevXPathBindingPoint;
    char *argVal = NULL;
    int xPathDepth = 0;
    int argNum = 0;
    BOOL anyOutBindings = FALSE;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    if (!(newBinding = calloc(1, sizeof(*newBinding))))
        PANIC;

    if (argList != NULL) {
        argVal = argList[argNum] == NULL ? "" : argList[argNum++];
    } else {
        UsrLog(uip, "%s: binding statement has no valid parameters, aborting.",
               xmlCtx->inServiceOf);
        goto Abort;
    }

    if (argVal[0] != 0) {
        if (!(newBinding->ezpName = strdup(argVal)))
            PANIC;
    } else {
        UsrLog(uip, "%s: binding statement has no variable name, aborting.", xmlCtx->inServiceOf);
        goto Abort;
    }
    if (uip->debug) {
        UsrLog(uip, "%s: binding statement variable name is \"%s\".", xmlCtx->inServiceOf,
               newBinding->ezpName);
    }

    argVal = argList[argNum] == NULL ? "" : argList[argNum++];
    if (stricmp(argVal, "in") == 0) {
        newBinding->inDir = TRUE;
    } else if (stricmp(argVal, "out") == 0) {
        newBinding->outDir = TRUE;
        anyOutBindings = TRUE;
    } else if (stricmp(argVal, "inout") == 0) {
        newBinding->inDir = TRUE;
        newBinding->outDir = TRUE;
    } else {
        UsrLog(uip,
               "%s: binding statement for variable %s has an invalid in/out value control "
               "indicator: %s.  Aborting.",
               xmlCtx->inServiceOf, newBinding->ezpName, argVal);
        goto Abort;
    }
    if (uip->debug) {
        UsrLog(uip, "%s: binding statement states that the direction for variable %s is %s.",
               xmlCtx->inServiceOf, newBinding->ezpName,
               (newBinding->inDir == TRUE)
                   ? ((newBinding->outDir == TRUE) ? "both input and output" : "input only")
                   : ((newBinding->outDir == TRUE) ? "output only" : ""));
    }

    argVal = argList[argNum] == NULL ? "" : argList[argNum++];
    if (stricmp(argVal, "null") == 0) {
        newBinding->nullable = 1;
    } else if (stricmp(argVal, "notnull") == 0) {
        newBinding->nullable = 0;
    } else {
        UsrLog(uip,
               "%s: binding statement for variable %s has an invalid null value control indicator: "
               "%s.  Aborting.",
               xmlCtx->inServiceOf, newBinding->ezpName, argVal);
        goto Abort;
    }
    if (uip->debug) {
        UsrLog(uip, "%s: binding statement states that null values are%s retained.",
               xmlCtx->inServiceOf, (newBinding->nullable == 1) ? "" : " not");
    }

    argVal = argList[argNum] == NULL ? "" : argList[argNum++];
    if (stricmp(argVal, "scalar") == 0) {
        newBinding->varComplexity = TRUE;
        if (!IsVarName(newBinding->ezpName)) {
            UsrLog(uip,
                   "%s: binding statement for variable %s has an invalid variable reference, "
                   "aborting.",
                   xmlCtx->inServiceOf, newBinding->ezpName);
            goto Abort;
        }
    } else if (stricmp(argVal, "vector") == 0) {
        newBinding->varComplexity = FALSE;
        if (!IsScalarVarName(newBinding->ezpName)) {
            /* we add the brackets later, so it's illegal to have them now. */
            UsrLog(uip,
                   "%s: binding statement for variable %s has an invalid array variable reference, "
                   "aborting.",
                   xmlCtx->inServiceOf, newBinding->ezpName);
            goto Abort;
        }
    } else {
        UsrLog(uip,
               "%s: binding statement for variable %s has an invalid complexity indicator: %s.  "
               "Aborting.",
               xmlCtx->inServiceOf, newBinding->ezpName, argVal);
        goto Abort;
    }
    if (uip->debug) {
        UsrLog(uip, "%s: binding statement states that the variable complexity is %s.",
               xmlCtx->inServiceOf, (newBinding->varComplexity == TRUE) ? "scalar" : "vector");
    }

    if (!(newXPathBindingPoint = calloc(1, sizeof(*newXPathBindingPoint))))
        PANIC;
    xPathBindingPoint = newXPathBindingPoint;
    while ((argVal = argList[argNum++]) != NULL) {
        prevXPathBindingPoint = newXPathBindingPoint;
        if (argVal) {
            if (!(newXPathBindingPoint->pathExpr = strdup(argVal)))
                PANIC;
        }
        if (uip->debug) {
            UsrLog(uip, "%s: binding statement path[%d] is %s.", xmlCtx->inServiceOf, xPathDepth,
                   newXPathBindingPoint->pathExpr);
        }
        xPathDepth++;
        if (!(newXPathBindingPoint = calloc(1, sizeof(*newXPathBindingPoint))))
            PANIC;
        prevXPathBindingPoint->next = newXPathBindingPoint;
        newXPathBindingPoint->depth = xPathDepth;
    }
    if (xPathDepth == 0) {
        UsrLog(uip, "%s: binding statement for variable %s has no path expression, aborting.",
               xmlCtx->inServiceOf, newBinding->ezpName);
        goto Abort;
    }
    FreeThenNull(prevXPathBindingPoint->next);
    newBinding->xPathDepth = xPathDepth;
    newBinding->pathHierarchy = xPathBindingPoint;

    for (binding = xmlCtx->bindings; binding; binding = nextBinding) {
        nextBinding = binding->next;
        if (strcmp(binding->ezpName, newBinding->ezpName) == 0) {
            UsrLog(uip,
                   "%s: binding statement for variable %s does not have a unique variable name, "
                   "aborting.",
                   xmlCtx->inServiceOf, newBinding->ezpName);
            goto Abort;
        }
    }
    newBinding->next = xmlCtx->bindings;
    xmlCtx->bindings = newBinding;
    xmlCtx->anyOutBindings = xmlCtx->anyOutBindings | anyOutBindings;

    result = STATEMENT_RESULT_SUCCESS;
    goto Finished;

Abort:
    uip->skipping = TRUE;
    result = STATEMENT_RESULT_FAILURE;
    UserXMLFreeBinding(newBinding);

Finished:
    return result;
}

STATEMENT_RESULT_TYPE UsrXMLPrepSchema(struct TRANSLATE *t,
                                       struct USERINFO *uip,
                                       struct XMLCTX *xmlCtx,
                                       char *arg,
                                       char *inOut,
                                       char **schemaName,
                                       xmlSchemaPtr *schema,
                                       BOOL *schemaValidation) {
    char *p;
    struct USRXML_LIBXML_CALLBACK_CONTEXT xmlCallbackCtx, *xmlCallbackCtxPtr;
    xmlSchemaParserCtxtPtr pctxt = NULL;
    xmlSchemaValidCtxtPtr vctxt = NULL;
    char *customFile = NULL;
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_FAILURE_INDEFINATE;

    FreeThenNull(*schemaName);
    if (*schema != NULL) {
        xmlSchemaFree(*schema);
        *schema = NULL;
    }

    if ((arg != NULL) && (arg[0] != 0)) {
        if (t->docsCustomDir && *t->docsCustomDir && (p = StartsIWith(arg, XMLDIR)) &&
            (strchr(p, '/') == NULL) && (strchr(p, '\\') == NULL)) {
            if (!(customFile = malloc(strlen(arg) + strlen(t->docsCustomDir) + 32)))
                PANIC;
            sprintf(customFile, "%scustom%s%s%s%s", XMLDIR, DIRSEP, t->docsCustomDir, DIRSEP, p);
        } else {
            /* Absolute path or no custom docs dir */
            if (!(customFile = strdup(arg)))
                PANIC;
        }
        if ((pctxt = xmlSchemaNewParserCtxt(customFile)) != NULL) {
            xmlCallbackCtxPtr = &xmlCallbackCtx;
            memset(xmlCallbackCtxPtr, 0, sizeof(*xmlCallbackCtxPtr));
            memcpy(xmlCallbackCtx.structureIdentifier, USRXML_LIBXML_CALLBACK_CONTEXT_STRUCT_ID,
                   sizeof(xmlCallbackCtx.structureIdentifier));
            xmlCallbackCtx.customFile = customFile;
            xmlCallbackCtx.uip = uip;
            xmlCallbackCtx.xmlCtx = xmlCtx;
            xmlCallbackCtx.result = STATEMENT_RESULT_SUCCESS;
            xmlCallbackCtx.resultIfParseFailure = STATEMENT_RESULT_FAILURE;
            xmlSchemaSetParserErrors(pctxt, UsrXMLErrorCallback, UsrXMLWarningCallback,
                                     xmlCallbackCtxPtr);
            *schema = xmlSchemaParse(pctxt);
            result = xmlCallbackCtx.result;
            if ((*schema != NULL) && (result == STATEMENT_RESULT_SUCCESS)) {
                if (uip->debug) {
                    UsrLog(uip, "%s: schema %s will be used to validate documents %s.",
                           xmlCtx->inServiceOf, customFile, inOut);
                }
                *schemaValidation = TRUE;
                *schemaName = customFile;
                customFile = NULL;
            } else {
                uip->skipping = TRUE;
                UsrLog(uip, "%s: schema %s failed to parse for documents %s, aborting.",
                       xmlCtx->inServiceOf, customFile, inOut);
            }
        } else {
            PANIC;
        }
    } else {
        *schemaValidation = FALSE;
        if (uip->debug) {
            UsrLog(uip, "%s: no schema name given, no validation will be done for documents %s.",
                   xmlCtx->inServiceOf, inOut);
        }
    }

    FreeThenNull(customFile);
    if (pctxt != NULL) {
        xmlSchemaFreeParserCtxt(pctxt);
    }
    if (vctxt != NULL) {
        xmlSchemaFreeValidCtxt(vctxt);
    }

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLSchemaValidation(struct TRANSLATE *t,
                                             struct USERINFO *uip,
                                             void *context,
                                             char **argList) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_SUCCESS;
    char *arg = NULL;
    char *outName = NULL;
    char *inName = NULL;
    int argNum = 0;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    if (argList != NULL) {
        arg = argList[argNum];
    } else {
        uip->skipping = TRUE;
        result = STATEMENT_RESULT_FAILURE;
    }

    if ((result == STATEMENT_RESULT_SUCCESS) && (arg != NULL)) {
        argNum++;
        if (stricmp(arg, "in") == 0) {
            xmlCtx->schemaInValidation = TRUE;
        } else if (stricmp(arg, "out") == 0) {
            xmlCtx->schemaOutValidation = TRUE;
        } else if (stricmp(arg, "inout") == 0) {
            xmlCtx->schemaInValidation = TRUE;
            xmlCtx->schemaOutValidation = TRUE;
        } else {
            uip->skipping = TRUE;
            result = STATEMENT_RESULT_FAILURE;
            UsrLog(uip,
                   "%s: the schema validation statement has an invalid in/out value control "
                   "indicator: %s.",
                   xmlCtx->inServiceOf, arg);
        }
    } else {
        uip->skipping = TRUE;
        result = STATEMENT_RESULT_FAILURE;
        UsrLog(uip,
               "%s: the schema validation statement is missing its in/out value control indicator.",
               xmlCtx->inServiceOf);
    }

    if ((result == STATEMENT_RESULT_SUCCESS) && (xmlCtx->schemaInValidation == TRUE)) {
        arg = argList[argNum];
        if (arg != NULL) {
            inName = arg;
            argNum++;
        } else {
            uip->skipping = TRUE;
            result = STATEMENT_RESULT_FAILURE;
            UsrLog(uip,
                   "%s: the schema validation statement is missing the filepath for the schema to "
                   "use on incoming documents.",
                   xmlCtx->inServiceOf);
        }
    }

    if ((result == STATEMENT_RESULT_SUCCESS) && (xmlCtx->schemaOutValidation == TRUE)) {
        arg = argList[argNum];
        if (arg != NULL) {
            outName = arg;
            argNum++;
        } else {
            uip->skipping = TRUE;
            result = STATEMENT_RESULT_FAILURE;
            UsrLog(uip,
                   "%s: the schema validation statement is missing the filepath for the schema to "
                   "use on outgoing documents.",
                   xmlCtx->inServiceOf);
        }
    }

    if ((result == STATEMENT_RESULT_SUCCESS) && xmlCtx->schemaOutValidation) {
        result = UsrXMLPrepSchema(t, uip, xmlCtx, outName, "sent", &(xmlCtx->schemaOutName),
                                  &(xmlCtx->schemaOut), &(xmlCtx->schemaOutValidation));
    }
    if ((result == STATEMENT_RESULT_SUCCESS) && xmlCtx->schemaInValidation) {
        result = UsrXMLPrepSchema(t, uip, xmlCtx, inName, "received", &(xmlCtx->schemaInName),
                                  &(xmlCtx->schemaIn), &(xmlCtx->schemaInValidation));
    }

    if ((result == STATEMENT_RESULT_SUCCESS) && (argList && argList[argNum] != NULL)) {
        UsrLog(uip, "%s: the schema validation statement has too many parameters, warning.",
               xmlCtx->inServiceOf);
    }

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLPrepDTD(struct TRANSLATE *t,
                                    struct USERINFO *uip,
                                    struct XMLCTX *xmlCtx,
                                    char *arg,
                                    char *inOut,
                                    char **dtdName,
                                    xmlDtdPtr *dtd,
                                    BOOL *dtdValidation) {
    char *p;
    char *customFile = NULL;
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_FAILURE;

    FreeThenNull(*dtdName);
    if (*dtd != NULL) {
        xmlFreeDtd(*dtd);
        *dtd = NULL;
    }

    if ((arg != NULL) && (arg[0] != 0)) {
        if (t->docsCustomDir && *t->docsCustomDir && (p = StartsIWith(arg, XMLDIR)) &&
            (strchr(p, '/') == NULL) && (strchr(p, '\\') == NULL)) {
            if (!(customFile = malloc(strlen(arg) + strlen(t->docsCustomDir) + 32)))
                PANIC;
            sprintf(customFile, "%scustom%s%s%s%s", XMLDIR, DIRSEP, t->docsCustomDir, DIRSEP, p);
        } else {
            /* Absolute path or no custom docs dir */
            if (!(customFile = strdup(arg)))
                PANIC;
        }
        *dtd = xmlParseDTD(NULL, (xmlChar *)customFile);
        if (*dtd != NULL) {
            if (uip->debug) {
                UsrLog(uip, "%s: DTD %s will be used to validate documents %s.",
                       xmlCtx->inServiceOf, customFile, inOut);
            }
            *dtdValidation = TRUE;
            *dtdName = customFile;
            customFile = NULL;
            result = STATEMENT_RESULT_SUCCESS;
        } else {
            uip->skipping = TRUE;
            UsrLog(uip, "%s: DTD %s failed to parse for documents %s, aborting.",
                   xmlCtx->inServiceOf, customFile, inOut);
        }
    } else {
        *dtdValidation = FALSE;
        if (uip->debug) {
            UsrLog(uip, "%s: no DTD name given, no validation will be done for documents %s.",
                   xmlCtx->inServiceOf, inOut);
        }
    }

    FreeThenNull(customFile);

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLDTDValidation(struct TRANSLATE *t,
                                          struct USERINFO *uip,
                                          void *context,
                                          char **argList) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_SUCCESS;
    char *arg = NULL;
    char *outName = NULL;
    char *inName = NULL;
    int argNum = 0;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    if (argList != NULL) {
        arg = argList[argNum];
    } else {
        uip->skipping = TRUE;
        result = STATEMENT_RESULT_FAILURE;
    }

    if ((result == STATEMENT_RESULT_SUCCESS) && (arg != NULL)) {
        argNum++;
        if (stricmp(arg, "in") == 0) {
            xmlCtx->dtdInValidation = TRUE;
        } else if (stricmp(arg, "out") == 0) {
            xmlCtx->dtdOutValidation = TRUE;
        } else if (stricmp(arg, "inout") == 0) {
            xmlCtx->dtdInValidation = TRUE;
            xmlCtx->dtdOutValidation = TRUE;
        } else {
            uip->skipping = TRUE;
            result = STATEMENT_RESULT_FAILURE;
            UsrLog(uip,
                   "%s: the DTD validation statement has an invalid in/out value control "
                   "indicator: %s.",
                   xmlCtx->inServiceOf, arg);
        }
    } else {
        uip->skipping = TRUE;
        result = STATEMENT_RESULT_FAILURE;
        UsrLog(uip,
               "%s: the DTD validation statement is missing its in/out value control indicator.",
               xmlCtx->inServiceOf);
    }

    if ((result == STATEMENT_RESULT_SUCCESS) && (xmlCtx->dtdInValidation == TRUE)) {
        arg = argList[argNum];
        if (arg != NULL) {
            inName = arg;
            argNum++;
        } else {
            uip->skipping = TRUE;
            result = STATEMENT_RESULT_FAILURE;
            UsrLog(uip,
                   "%s: the DTD validation statement is missing its filepath for the DTD to use on "
                   "incoming documents.",
                   xmlCtx->inServiceOf);
        }
    }

    if ((result == STATEMENT_RESULT_SUCCESS) && (xmlCtx->dtdOutValidation == TRUE)) {
        arg = argList[argNum];
        if (arg != NULL) {
            outName = arg;
            argNum++;
        } else {
            uip->skipping = TRUE;
            result = STATEMENT_RESULT_FAILURE;
            UsrLog(uip,
                   "%s: the DTD validation statement is missing its filepath for the DTD to use on "
                   "outgoing documents.",
                   xmlCtx->inServiceOf);
        }
    }

    if ((result == STATEMENT_RESULT_SUCCESS) && xmlCtx->dtdOutValidation) {
        result = UsrXMLPrepDTD(t, uip, xmlCtx, outName, "sent", &(xmlCtx->dtdOutName),
                               &(xmlCtx->dtdOut), &(xmlCtx->dtdOutValidation));
    }
    if ((result == STATEMENT_RESULT_SUCCESS) && xmlCtx->dtdInValidation) {
        result = UsrXMLPrepDTD(t, uip, xmlCtx, inName, "received", &(xmlCtx->dtdInName),
                               &(xmlCtx->dtdIn), &(xmlCtx->dtdInValidation));
    }

    if ((result == STATEMENT_RESULT_SUCCESS) && (argList && argList[argNum] != NULL)) {
        UsrLog(uip, "%s: the DTD validation statement has too many parameters, warning.",
               xmlCtx->inServiceOf);
    }

    return result;
}

BOOL UsrXMLAggregateTest(struct TRANSLATE *t,
                         struct USERINFO *uip,
                         void *context,
                         const char *var,
                         const char *testVal,
                         enum AGGREGATETESTOPTIONS ato,
                         struct VARIABLES *rev) {
    struct AGGREGATETESTCONTEXT atc;
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    xmlXPathObjectPtr xpathObj = NULL;
    xmlNodeSetPtr nodes = NULL;
    int i;
    xmlChar *xmlVal;
    char *field;
    char *subVar;
    char *val;
    char *tmpStr;
    char *tempTag = NULL;
    BOOL stop = FALSE;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    if (!ExpressionAggregateTestInitialize(&atc, uip, testVal, ato)) {
        return FALSE;
    }

    if ((subVar = StartsWith(var, "xpath:")) == 0) {
        if (uip->debug) {
            UsrLog(uip, "%s: searching for values from XPath %s; aggregate mode",
                   xmlCtx->inServiceOf, xmlCtx->inServiceOf, subVar);
        }

        if (xmlCtx->xpathCtxIn != NULL) {
            if (strchr(subVar, '/') == NULL) {
                if (!(tempTag = malloc(strlen(subVar) + 16)))
                    PANIC;
                sprintf(tempTag, "//*/%s", subVar);
                if (uip->debug) {
                    UsrLog(uip, "%s: tag lacks path; changed to %s", xmlCtx->inServiceOf, tempTag);
                }
                xpathObj = xmlXPathEvalExpression(BAD_CAST tempTag, xmlCtx->xpathCtxIn);
                subVar = tempTag;
            } else {
                xpathObj = xmlXPathEvalExpression(BAD_CAST subVar, xmlCtx->xpathCtxIn);
            }
            if (xpathObj == NULL) {
                UsrLog(uip, "%s: XPath expression %s is invalid.  Aborting.", xmlCtx->inServiceOf,
                       subVar);
                FreeThenNull(tempTag);
                return FALSE;
            }

            if (xpathObj->type == XPATH_NODESET) {
                if (!(xpathObj == NULL || (nodes = xpathObj->nodesetval) == NULL ||
                      (nodes->nodeNr == 0))) {
                    for (i = 0; (!stop) && (i < nodes->nodeNr); i++) {
                        xmlVal = xmlNodeGetContent(nodes->nodeTab[i]);
                        stop = !(ExpressionAggregateTestValue(&atc, (char *)xmlVal));
                        xmlFreeThenNull(xmlVal);
                    }
                } else {
                    if (uip->debug) {
                        UsrLog(uip, "%s: auth:%s didn't selected any node, warning",
                               xmlCtx->inServiceOf, var);
                    }
                }
            } else if (xpathObj->type == XPATH_BOOLEAN) {
                if (!(val = malloc(16)))
                    PANIC;
                _snprintf(val, 16, "%d", (xpathObj->boolval == 0) ? 0 : 1);
                stop = !(ExpressionAggregateTestValue(&atc, val));
                FreeThenNull(val);
            } else if (xpathObj->type == XPATH_NUMBER) {
                if (!(val = malloc(128)))
                    PANIC;
                _snprintf(val, 128, "%f", xpathObj->floatval);
                stop = !(ExpressionAggregateTestValue(&atc, val));
                FreeThenNull(val);
            } else if (xpathObj->type == XPATH_STRING) {
                stop = !(ExpressionAggregateTestValue(&atc, (char *)xpathObj->stringval));
            } else {
                UsrLog(uip,
                       "%s: XPath expression %s resulted in %s.  It is not useful in this context. "
                       " Warning.",
                       xmlCtx->inServiceOf, subVar,
                       tmpStr = UUxmlXPathTypeToString(xpathObj->type));
                xmlFreeThenNull(tmpStr);
            }

            if (xpathObj) {
                xmlXPathFreeObject(xpathObj);
                xpathObj = NULL;
            }
            FreeThenNull(tempTag);
        } else {
            if (uip->debug) {
                UsrLog(uip, "%s: there is no document; aggregate mode; warning",
                       xmlCtx->inServiceOf);
            }
        }
    } else if ((subVar = StartsWith(var, "http:")) == 0) {
        field = FindField(subVar, xmlCtx->incomingHeaders);
        while (field && (!stop)) {
            LinkField(field);
            stop = !(ExpressionAggregateTestValue(&atc, field));
            field = FindField(subVar, field);
        }
    } else {
        UsrLog(uip, "%s: undefined variable auth:%s, warning", xmlCtx->inServiceOf, var);
    }

    return ExpressionAggregateTestTerminate(&atc, rev);
}

BOOL UsrXMLIsReadOnly(struct TRANSLATE *t,
                      struct USERINFO *uip,
                      void *context,
                      const char *var,
                      const char *index) {
    if ((var = StartsWith(var, "xpath:")) != 0) {
        return FALSE;
    } else {
        return TRUE;
    }
}

char *UsrXMLGetValue(struct TRANSLATE *t,
                     struct USERINFO *uip,
                     void *context,
                     const char *var,
                     const char *index) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    xmlXPathObjectPtr xpathObj = NULL;
    xmlNodeSetPtr nodes = NULL;
    xmlChar *xmlVal;
    char *field;
    char *tmpStr;
    char *subVar;
    char *val = NULL;
    char *tempTag = NULL;
    char *varIndex = NULL;
    int indexVal = 0;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    indexVal = index ? atoi(index) : 0;
    varIndex = ExpressionVariableIndex(var, index); /* just for reporting */
    if ((subVar = StartsWith(var, "xpath:")) != 0) {
        if (uip->debug) {
            UsrLog(uip, "%s: searching for values from XPath %s item %d; singleton mode",
                   xmlCtx->inServiceOf, subVar, indexVal);
        }

        if (xmlCtx->xpathCtxIn != NULL) {
            if (strchr(subVar, '/') == NULL) {
                if (!(tempTag = malloc(strlen(subVar) + 16)))
                    PANIC;
                sprintf(tempTag, "//*/%s", subVar);
                if (uip->debug) {
                    UsrLog(uip, "%s: tag lacks path; changed to %s", xmlCtx->inServiceOf, tempTag);
                }
                xpathObj = xmlXPathEvalExpression(BAD_CAST tempTag, xmlCtx->xpathCtxIn);
                subVar = tempTag;
            } else {
                xpathObj = xmlXPathEvalExpression(BAD_CAST subVar, xmlCtx->xpathCtxIn);
                tempTag = NULL;
            }
            if (xpathObj == NULL) {
                UsrLog(uip, "%s: XPath expression %s is invalid.  Aborting.", xmlCtx->inServiceOf,
                       subVar);
                FreeThenNull(tempTag);
                return NULL;
            }

            if (xpathObj->type == XPATH_NODESET) {
                if (!(xpathObj == NULL || (nodes = xpathObj->nodesetval) == NULL ||
                      (nodes->nodeNr == 0) || (indexVal >= nodes->nodeNr) || (indexVal < 0))) {
                    xmlVal = xmlNodeGetContent(nodes->nodeTab[indexVal]);
                    if (!(val = strdup((char *)xmlVal)))
                        PANIC;
                    xmlFreeThenNull(xmlVal);
                } else {
                    if (uip->debug) {
                        UsrLog(uip, "%s: auth:%s didn't selected any node, warning",
                               xmlCtx->inServiceOf, varIndex);
                    }
                }
            } else if (xpathObj->type == XPATH_BOOLEAN) {
                if (index == NULL) {
                    if (!(val = malloc(16)))
                        PANIC;
                    _snprintf(val, 16, "%d", (xpathObj->boolval == 0) ? 0 : 1);
                }
            } else if (xpathObj->type == XPATH_NUMBER) {
                if (index == NULL) {
                    if (!(val = malloc(128)))
                        PANIC;
                    _snprintf(val, 128, "%f", xpathObj->floatval);
                }
            } else if (xpathObj->type == XPATH_STRING) {
                if (index == NULL) {
                    if (!(val = strdup((char *)xpathObj->stringval)))
                        PANIC;
                }
            } else {
                if (uip->debug) {
                    UsrLog(uip,
                           "%s: XPath expression %s resulted in %s.  It is not useful in this "
                           "context.  Warning.",
                           xmlCtx->inServiceOf, subVar,
                           tmpStr = UUxmlXPathTypeToString(xpathObj->type));
                    xmlFreeThenNull(tmpStr);
                }
            }

            if (xpathObj) {
                xmlXPathFreeObject(xpathObj);
                xpathObj = NULL;
            }
            FreeThenNull(tempTag);
        } else {
            if (uip->debug) {
                UsrLog(uip, "%s: there is no document; singleton mode; warning",
                       xmlCtx->inServiceOf);
            }
        }
    } else if ((subVar = StartsWith(var, "http:")) != 0) {
        field = NULL;
        field = FindFieldN(subVar, xmlCtx->incomingHeaders, indexVal);
        if (field != NULL) {
            LinkField(field);
            if (!(val = strdup(field)))
                PANIC;
        } else if (uip->debug) {
            UsrLog(uip, "%s: variable auth:%s has no value, warning", xmlCtx->inServiceOf,
                   varIndex);
        }
    } else {
        if (uip->debug) {
            UsrLog(uip, "%s: undefined variable auth:%s, warning", xmlCtx->inServiceOf, varIndex);
        }
    }
    if (varIndex != var) {
        FreeThenNull(varIndex);
    }

    return val;
}

void UsrXMLSetValue(struct TRANSLATE *t,
                    struct USERINFO *uip,
                    void *context,
                    const char *var,
                    const char *index,
                    const char *val) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    xmlXPathObjectPtr xpathObj = NULL;
    xmlNodeSetPtr nodes = NULL;
    xmlNodePtr newNode;
    char *tmpStr;
    char *subVar;
    char *varIndex = NULL;
    char *tempTag = NULL;
    int indexVal = 0;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    indexVal = index ? atoi(index) : 0;
    varIndex = ExpressionVariableIndex(var, index); /* just for reporting */
    if ((subVar = StartsWith(var, "xpath:")) != 0) {
        if (uip->debug) {
            UsrLog(uip, "%s: assigning value to XPath %s item %d; singleton mode",
                   xmlCtx->inServiceOf, subVar, indexVal);
        }

        if (xmlCtx->xpathCtxOut != NULL) {
            if (strchr(subVar, '/') == NULL) {
                if (!(tempTag = malloc(strlen(subVar) + 16)))
                    PANIC;
                sprintf(tempTag, "//*/%s", subVar);
                if (uip->debug) {
                    UsrLog(uip, "%s: tag lacks path; changed to %s", xmlCtx->inServiceOf, tempTag);
                }
                xpathObj = xmlXPathEvalExpression(BAD_CAST tempTag, xmlCtx->xpathCtxOut);
                subVar = tempTag;
            } else {
                xpathObj = xmlXPathEvalExpression(BAD_CAST subVar, xmlCtx->xpathCtxOut);
                tempTag = NULL;
            }
            if (xpathObj == NULL) {
                UsrLog(uip, "%s: XPath expression %s is invalid.  Aborting.", xmlCtx->inServiceOf,
                       subVar);
                FreeThenNull(tempTag);
                return;
            }

            if (xpathObj->type == XPATH_NODESET) {
                if (!(((nodes = xpathObj->nodesetval) == NULL) || (nodes->nodeNr == 0))) {
                    if (uip->debug) {
                        UsrLog(uip, "%s: assigning value %s to auth:%s.", xmlCtx->inServiceOf, val,
                               varIndex);
                    }
                    if (!((nodes->nodeNr == 0) || (indexVal >= nodes->nodeNr) || (indexVal < 0))) {
                        UsrXMLUpdateDocumentNode(t, uip, xmlCtx, var, TRUE,
                                                 xpathObj->nodesetval->nodeTab, indexVal, val);
                    } else {
                        newNode = xmlNewDocText(xmlCtx->docOut, BAD_CAST val);
                        xmlAddNextSibling(xpathObj->nodesetval->nodeTab[nodes->nodeNr - 1],
                                          newNode);
                    }
                } else {
                    UsrLog(uip, "%s: auth:%s didn't selected any node, no node created, warning",
                           xmlCtx->inServiceOf, varIndex);
                }
            } else {
                if (uip->debug) {
                    UsrLog(uip,
                           "%s: XPath expression %s resulted in %s.  It is not useful in this "
                           "context.  Warning.",
                           xmlCtx->inServiceOf, subVar,
                           tmpStr = UUxmlXPathTypeToString(xpathObj->type));
                    xmlFreeThenNull(tmpStr);
                }
            }

            if (xpathObj) {
                xmlXPathFreeObject(xpathObj);
                xpathObj = NULL;
            }
            FreeThenNull(tempTag);
        } else {
            if (uip->debug) {
                UsrLog(uip, "%s: there is no document; singleton mode; warning",
                       xmlCtx->inServiceOf);
            }
        }
    } else if ((subVar = StartsWith(var, "http:")) != 0) {
        if (uip->debug) {
            UsrLog(uip, "%s: read-only variable auth:%s, warning", xmlCtx->inServiceOf, varIndex);
        }
    } else {
        if (uip->debug) {
            UsrLog(uip, "%s: undefined variable auth:%s, warning", xmlCtx->inServiceOf, varIndex);
        }
    }
    if (varIndex != var) {
        FreeThenNull(varIndex);
    }
}

STATEMENT_RESULT_TYPE UsrXMLMsgAuthLog(struct TRANSLATE *t,
                                       struct USERINFO *uip,
                                       struct XMLCTX *xmlCtx,
                                       char *direction,
                                       xmlDocPtr doc) {
    xmlChar *buffer = NULL;
    int len = 0;

    if (doc == NULL)
        return STATEMENT_RESULT_SUCCESS;

    xmlDocDumpFormatMemory(doc, &buffer, &len, xmlIndentTreeOutput);
    if (buffer == NULL) {
        return STATEMENT_RESULT_SUCCESS;
    }

    UsrLog(uip, "%s: %s", xmlCtx->inServiceOf, direction);
    UsrLog(uip, "%s: %s", xmlCtx->inServiceOf, buffer);

    xmlFreeThenNull(buffer);
    return STATEMENT_RESULT_SUCCESS;
}

STATEMENT_RESULT_TYPE UsrXMLMsgAuth(struct TRANSLATE *t,
                                    struct USERINFO *uip,
                                    void *context,
                                    char **argList) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    char *arg;
    BOOL logToFile = FALSE;
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_SUCCESS;
    BOOL in = TRUE;
    BOOL out = FALSE;

    if (!(strcmp(xmlCtx->structureIdentifier, XMLCTX_STRUCTUREIDENTIFIER) == 0))
        PANIC;

    if (argList != NULL) {
        arg = argList[0];
    } else {
        uip->skipping = TRUE;
        result = STATEMENT_RESULT_FAILURE;
    }

    if ((result == STATEMENT_RESULT_SUCCESS) && (arg != NULL)) {
        if (stricmp(arg, "in") == 0) {
        } else if (stricmp(arg, "out") == 0) {
            in = FALSE;
            out = TRUE;
        } else if (stricmp(arg, "inout") == 0) {
            in = TRUE;
            out = TRUE;
        } else {
            uip->skipping = TRUE;
            result = STATEMENT_RESULT_FAILURE;
            UsrLog(uip,
                   "%s: the MsgAuth statement has an invalid in/out value control indicator %s, "
                   "aborting.",
                   xmlCtx->inServiceOf, arg);
        }
        arg = argList[1];
    }

    if ((result == STATEMENT_RESULT_SUCCESS) && (arg != NULL)) {
        if (stricmp(arg, "file") == 0) {
            logToFile = TRUE;
        } else if (stricmp(arg, "nofile") == 0) {
        } else {
            UsrLog(uip,
                   "%s: the MsgAuth statement has an invalid file logging indicator %s, aborting.",
                   xmlCtx->inServiceOf, arg);
            uip->skipping = TRUE;
            result = STATEMENT_RESULT_FAILURE;
        }
    }

    if ((result == STATEMENT_RESULT_SUCCESS) && in) {
        UsrXMLMsgAuthLog(t, uip, xmlCtx, "received", xmlCtx->docIn);
        if (logToFile) {
            LogXMLDoc(xmlCtx->docIn, NULL);
        }
    }
    if ((result == STATEMENT_RESULT_SUCCESS) && out) {
        UsrXMLMsgAuthLog(t, uip, xmlCtx, "will be sent", xmlCtx->docOut);
        if (logToFile) {
            LogXMLDoc(xmlCtx->docOut, NULL);
        }
    }

    if ((result == STATEMENT_RESULT_SUCCESS) && (argList && argList[2] != NULL)) {
        UsrLog(uip, "%s: the schema validation statement has too many parameters, warning.",
               xmlCtx->inServiceOf);
    }

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLInteger(struct TRANSLATE *t,
                                    struct USERINFO *uip,
                                    void *context,
                                    char *thisDirective,
                                    char *newValueStr,
                                    int *targetValue,
                                    int defaultValue,
                                    int previousValue) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    int newValueInt = 0;
    STATEMENT_RESULT_TYPE result = STATEMENT_RESULT_FAILURE;
    char *p;

    if ((newValueStr != NULL) && (*newValueStr != 0)) {
        p = SkipWS(newValueStr);
        if (stricmp(p, "default") == 0) {
            *targetValue = defaultValue;
        } else if (stricmp(p, "previous") == 0) {
            *targetValue = previousValue;
        } else if (IsOneOrMoreDigits(p)) {
            newValueInt = atoi(p);
            result = 1;
            *targetValue = newValueInt;
        } else {
            uip->skipping = TRUE;
            result = STATEMENT_RESULT_FAILURE;
            UsrLog(uip,
                   "%s: %s argument must be \"default\", \"previous\", or all decimal digits, not "
                   "\"%s\", aborting.",
                   xmlCtx->inServiceOf, thisDirective, p);
        }
    }

    return result;
}

STATEMENT_RESULT_TYPE UsrXMLSSLCertificateNumber(struct TRANSLATE *t,
                                                 struct USERINFO *uip,
                                                 void *context,
                                                 char *arg) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    return UsrXMLInteger(t, uip, context, "SSLCertificateNumber", arg, &(xmlCtx->sslIdx), 0,
                         xmlCtx->sslIdxSave);
}

STATEMENT_RESULT_TYPE UsrXMLConnectionTimeout(struct TRANSLATE *t,
                                              struct USERINFO *uip,
                                              void *context,
                                              char *arg) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    return UsrXMLInteger(t, uip, context, "ConnectionTimeout", arg, &(uip->connectionsTimeout), 0,
                         xmlCtx->timeoutSave);
}

STATEMENT_RESULT_TYPE UsrXMLConnectionTries(struct TRANSLATE *t,
                                            struct USERINFO *uip,
                                            void *context,
                                            char *arg) {
    struct XMLCTX *xmlCtx = (struct XMLCTX *)context;
    return UsrXMLInteger(t, uip, context, "ConnectionTries", arg, &(uip->connectionsTries), 0,
                         xmlCtx->triesSave);
}

STATEMENT_RESULT_TYPE UsrXMLInit(struct TRANSLATE *t,
                                 struct USERINFO *uip,
                                 void *context,
                                 char *argList) {
    uip->result = resultInvalid;
    return STATEMENT_RESULT_SUCCESS;
}

enum FINDUSERRESULT FindUserXML(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                char *file,
                                int f,
                                struct FILEREADLINEBUFFER *frb,
                                struct sockaddr_storage *pInterface) {
    struct XMLCTX *xmlCtx, xmlCtxBuffer;

    struct USRDIRECTIVE udc[] = {
        {"", UsrXMLInit, 0},
        {"Binding", NULL, USRDIRECTIVE_FLAG_EVAL_N_PARMS, UsrXMLBinding},
        {"ConnectionTimeout", UsrXMLConnectionTimeout, USRDIRECTIVE_FLAG_EVAL_1_PARM},
        {"ConnectionTries", UsrXMLConnectionTries, USRDIRECTIVE_FLAG_EVAL_1_PARM},
        {"Data", UsrXMLDataStart, USRDIRECTIVE_FLAG_EVAL_1_PARM},
        {"DataAppend", UsrXMLDataCat, USRDIRECTIVE_FLAG_EVAL_1_PARM},
        {"DataContentType", UsrXMLPOSTContentType, USRDIRECTIVE_FLAG_EVAL_1_PARM},
        {"DocumentContentType", UsrXMLDocContentType, USRDIRECTIVE_FLAG_EVAL_1_PARM},
        {"DocumentDefaultsFromXMLSchemaDefinition", UsrXMLOutDocDefaultsFromSchema,
         USRDIRECTIVE_FLAG_EVAL_1_PARM},
        {"DocumentFromTemplate", UsrXMLNewOutDocFromTemplate, USRDIRECTIVE_FLAG_EVAL_1_PARM},
        {"GET", UsrXMLGET, USRDIRECTIVE_FLAG_EVAL_1_PARM | USRDIRECTIVE_FLAG_EVAL_FALLBACK},
        {"HTTPHeader", UsrXMLOutgoingHeadersStart, USRDIRECTIVE_FLAG_EVAL_1_PARM},
        {"HTTPHeaderAppend", UsrXMLOutgoingHeadersCat, USRDIRECTIVE_FLAG_EVAL_1_PARM},
        {"MsgAuth", NULL, USRDIRECTIVE_FLAG_EVAL_N_PARMS, UsrXMLMsgAuth},
        {"Namespace", NULL, USRDIRECTIVE_FLAG_EVAL_N_PARMS, UsrXMLNamespace},
        {"POST", UsrXMLPOST, USRDIRECTIVE_FLAG_EVAL_1_PARM | USRDIRECTIVE_FLAG_EVAL_FALLBACK},
        {"PUT", UsrXMLPUT, USRDIRECTIVE_FLAG_EVAL_1_PARM | USRDIRECTIVE_FLAG_EVAL_FALLBACK},
        {"SSLCertificateNumber", UsrXMLSSLCertificateNumber, USRDIRECTIVE_FLAG_EVAL_1_PARM},
        {"URL", UsrXMLURLStart, USRDIRECTIVE_FLAG_EVAL_1_PARM},
        {"URLAppend", UsrXMLURLCat, USRDIRECTIVE_FLAG_EVAL_1_PARM},
        {"ValidateDocumentByDocumentTypeDefinition", NULL, USRDIRECTIVE_FLAG_EVAL_N_PARMS,
         UsrXMLDTDValidation},
        {"ValidateDocumentByXMLSchemaDefinition", NULL, USRDIRECTIVE_FLAG_EVAL_N_PARMS,
         UsrXMLSchemaValidation},
        {NULL, NULL, 0}
    };

    xmlCtx = &xmlCtxBuffer;
    UserXMLInit(xmlCtx, pInterface, "application/x-www-form-urlencoded",
                "application/xml; charset=\"utf-8\"", "XML");
    xmlCtxBuffer.triesSave = uip->connectionsTries;
    xmlCtxBuffer.timeoutSave = uip->connectionsTimeout;
    xmlCtx->sslIdxSave = sslActiveIndex;

    uip->authGetValue = UsrXMLGetValue;
    uip->authIsReadOnly = UsrXMLIsReadOnly;
    uip->authSetValue = UsrXMLSetValue;
    uip->authAggregateTest = UsrXMLAggregateTest;
    uip->flags = USRUSERMAYBENULL;
    uip->result =
        UsrHandler(t, xmlCtx->inServiceOf, udc, xmlCtx, uip, file, f, frb, pInterface, NULL);

    uip->connectionsTries = xmlCtxBuffer.triesSave;
    uip->connectionsTimeout = xmlCtxBuffer.timeoutSave;
    UserXMLDestroy(xmlCtx);

    return uip->result;
}
