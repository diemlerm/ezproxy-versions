/**
 * L4U (an small ILS) user authentication
 *
 * This authentication method supports common conditions and actions.
 */

#define __UUFILE__ "usrl4u.c"

#include "common.h"

#include "usr.h"
#include "usrl4u.h"

#include "uuxml.h"

#include "html.h"

struct L4UCTX {
    char *valid;
    char *invalid;
};

/*
int UsrL4UIfTest(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg)
{
    struct L4UCTX *L4UCTX = (struct L4UCTX *) context;
    char *arg2;
    char compareOperator;
    char compareType;
    xmlChar *val;
    int result = 1;

    arg2 = SkipNonWS(arg);
    if (*arg2) {
        *arg2++ = 0;
        arg2 = SkipWS(arg2);
    }

    arg2 = UsrTestComparisonOperatorType(arg2, &compareOperator, &compareType);

    if (stricmp(arg, "ItemsOverdue") == 0)
        val = L4UCTX->itemsOverdue;
    else if (stricmp(arg, "ItemsLost") == 0)
        val = L4UCTX->itemsLost;
    else if (stricmp(arg, "Balance") == 0)
        val = L4UCTX->balance;
    else if (stricmp(arg, "Location") == 0)
        val = L4UCTX->location;
    else {
        UsrLog(uip, "HIP unknown test variable: %s", arg);
        return 1;
    }

    if (val == NULL)
        val = BAD_CAST "";

    result = UsrTestCompare((char *) val, arg2, compareOperator, compareType) ? 0 : 1;

    if (uip->debug)
        UsrLog(uip, "HIP test %s %s against %s result %s", arg, val, arg2, result ? "FALSE" :
"TRUE");

    return result;
}
*/

int UsrL4UValid(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct L4UCTX *L4UCtx = (struct L4UCTX *)context;

    ReadConfigSetString(&L4UCtx->valid, arg);
    return 0;
}

int UsrL4UInvalid(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct L4UCTX *L4UCtx = (struct L4UCTX *)context;

    ReadConfigSetString(&L4UCtx->invalid, arg);
    return 0;
}

int UsrL4UURL(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    int result = 1;
    char *user, *pass;

    htmlParserCtxtPtr ctxt = NULL;
    xmlChar *loginURL = NULL;
    char *absoluteLoginURL = NULL;
    xmlChar *action = NULL;
    char *absoluteAction = NULL;
    xmlChar *profile = NULL;
    struct COOKIE *cookies = NULL;

    user = uip->user ? uip->user : "";
    pass = uip->pass ? uip->pass : "";

    if (*user == 0 || strlen(user) > 32 || strlen(pass) > 32)
        goto Finished;

    ctxt = GetHTML(arg, NULL, &cookies, NULL, uip->pInterface, 0, 0, NULL, NULL, uip->debug);
    if (ctxt == NULL) {
        UsrLog(uip, "L4U server did not respond for %s\n", arg);
        goto Finished;
    }
    loginURL = UUxmlSimpleGetContent(ctxt->myDoc, NULL, "//a[@alt = 'Login']/@href");
    UUxmlFreeParserCtxtAndDoc(&ctxt);

    if (loginURL == NULL) {
        UsrLog(uip, "Failed to retrieve Login");
        goto Finished;
    }

    absoluteLoginURL = AbsoluteURL(arg, (char *)loginURL);

    ctxt = GetHTML(absoluteLoginURL, NULL, &cookies, NULL, uip->pInterface, 0, 0, NULL, NULL,
                   uip->debug);
    if (ctxt == NULL) {
        UsrLog(uip, "L4U server did not respond for %s\n", loginURL);
        goto Finished;
    }
    action =
        UUxmlSimpleGetContent(ctxt->myDoc, NULL, "//form/input[@name = 'CL_1_Transit']/../@action");
    UUxmlFreeParserCtxtAndDoc(&ctxt);

    absoluteAction = AbsoluteURL(absoluteLoginURL, (char *)action);

    sprintf(t->buffer, "CL_1_Transit=0&web_Accession=&web_ClientCode=");
    AddEncodedField(t->buffer, user, NULL);
    strcat(t->buffer, "&web_ClientPIN=");
    AddEncodedField(t->buffer, pass, NULL);

    ctxt = GetHTML(absoluteAction, t->buffer, &cookies, NULL, uip->pInterface, 0, 0, NULL, NULL,
                   uip->debug);
    profile = UUxmlSimpleGetContent(ctxt->myDoc, NULL, "//a[@alt = 'My Profile']/@href");

    uip->result = profile != NULL ? resultValid : resultInvalid;

    UUxmlFreeParserCtxtAndDoc(&ctxt);

Finished:
    xmlFreeThenNull(profile);
    FreeThenNull(absoluteAction);
    xmlFreeThenNull(action);
    FreeThenNull(absoluteLoginURL);
    xmlFreeThenNull(loginURL);
    GetHTMLFreeCookies(&cookies);

    if (uip->debug) {
        UsrLog(uip, "L4U results for %s", user);
        if (uip->result == resultValid) {
            UsrLog(uip, "User valid");
        }
    }

    return result;
}

enum FINDUSERRESULT FindUserL4U(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                char *file,
                                int f,
                                struct FILEREADLINEBUFFER *frb,
                                struct sockaddr_storage *pInterface) {
    struct USRDIRECTIVE udc[] = {
  /* { "IfTest",           UsrL4UIfTest,           0 }, */
        {"URL", UsrL4UURL, 0},
        {NULL,  NULL,      0}
    };
    enum FINDUSERRESULT result;
    struct L4UCTX L4UCtx;

    memset(&L4UCtx, 0, sizeof(L4UCtx));

    uip->flags = 0;
    result = UsrHandler(t, "L4U", udc, &L4UCtx, uip, file, f, frb, pInterface, NULL);

    FreeThenNull(L4UCtx.valid);
    FreeThenNull(L4UCtx.invalid);

    return result;
}
