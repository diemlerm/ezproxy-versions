#ifndef __ADMINWEBSERVER_H__
#define __ADMINWEBSERVER_H__

#include "common.h"

void AdminWebServer(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINWEBSERVER_H__
