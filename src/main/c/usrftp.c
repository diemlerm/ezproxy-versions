/**
 * FTP user authentication
 *
 * This module implements user authentication by testing the username and
 * password against the specified FTP server.  If the FTP server grants access,
 * EZproxy grants access.
 *
 * This authentication method DOES NOT SUPPORT common conditions and actions.
 */

#define __UUFILE__ "usrftp.c"

#include "common.h"
#include "uustring.h"

#include "usr.h"
#include "usrftp.h"

/* Returns 0 if valid, 1 if not, 3 if unable to connect to ftp host */
int FindUserFtp(char *ftpHost,
                char *user,
                char *pass,
                struct sockaddr_storage *pInterface,
                char useSsl,
                BOOL debug) {
    struct UUSOCKET rr, *r = &rr;
    int result;
    int state;
    char line[100];
    char multi[4];
    PORT port;
    char *colon;

    UUInitSocket(r, INVALID_SOCKET);

    if (useSsl) {
        Log("SSL not currently supported for FTP authentication");
        return 3;
    }

    result = 3;

    multi[0] = 0;

    if (ftpHost == NULL || user == NULL || pass == NULL)
        goto Finished;

    ParseHost(ftpHost, &colon, NULL, TRUE);

    if (colon) {
        *colon = 0;
        port = atoi(colon + 1);
    } else {
        port = 21;
    }

    if (UUConnectWithSource(r, ftpHost, port, "FTP", pInterface))
        goto Finished;

    if (debug)
        Log("FTP connect to %s", ftpHost);

    result = 1;
    for (state = 0;;) {
        if (ReadLine(r, line, sizeof(line)) == 0) {
            result = 3;
            break;
        }
        if (debug)
            Log("Received %s", line);

        /* If we are receiving a multi-line response, skip all but final line */
        if (strlen(line) >= 4) {
            /* Multi-line responses end when the original code is received, followed by a space */
            if (multi[0]) {
                if (strncmp(line, multi, 3) == 0 && line[3] == ' ')
                    multi[0] = 0;
                /* Multi-line responses start when there is a - after the 3 digit code */
            } else if (line[3] == '-') {
                StrCpy3(multi, line, 4);
            }
        }
        if (multi[0])
            continue;

        if (state == 0) {
            if (strncmp(line, "120", 3) == 0)
                continue;
            if (strncmp(line, "220", 3) != 0)
                break;
            UUSendZ(r, "USER ");
            UUSendZCRLF(r, user);
            if (debug)
                Log("Sent USER %s", user);
        } else if (state == 1) {
            if (strncmp(line, "331", 3) != 0)
                break;
            UUSendZ(r, "PASS ");
            UUSendZCRLF(r, pass);
            if (debug)
                Log("Sent PASS (specified pass)");
        } else {
            if (strncmp(line, "230", 3) == 0)
                result = 0;
            break;
        }
        state++;
    }
    if (result != 3) {
        UUSendZ(r, "QUIT\r\n");
        UUFlush(r);
    }
    UUStopSocket(r, 0);

Finished:
    return result;
}
