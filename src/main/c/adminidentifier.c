#define __UUFILE__ "adminidentifier.c"

#include "common.h"
#include "adminidentifier.h"
#include "identifier.h"
#include "sql.h"

void AdminIdentifierScopes(struct TRANSLATE *t, char *query, char *post) {
    const int AIEXTENDED = 0;
    struct FORMFIELD ffs[] = {
        {"extended", NULL, 0, 0, 1},
        {NULL,       NULL, 0, 0, 0}
    };
    struct UUSOCKET *s = &t->ssocket;
    struct UUAVLTREE avlScope = IdentifierGetScopeTree();
    struct IDENTIFIERSCOPE *p;
    UUAVLTREEITERCTX uatic;
    BOOL extended;
    char *str, *next;
    char schar[2];

    AdminHeader(t, 0, "Identifier Scopes");
    FindFormFields(query, post, ffs);
    extended = ffs[AIEXTENDED].value != NULL;

    if (IdentifierGetSharedDatabase() == NULL) {
        WriteLine(s, "<p>Identifiers are not enabled</p>\n");
    } else {
        WriteLine(s,
                  "<table class='bordered-table'>\n<tr><th scope='col'>Scope</th><th "
                  "scope='col'>Status</th>%s</tr>\n",
                  extended ? "<th scope='col'>Header</th><th scope='col'>Domains</th>" : "");
        for (p = (struct IDENTIFIERSCOPE *)UUAvlTreeIterFirst(&avlScope, &uatic); p;
             p = (struct IDENTIFIERSCOPE *)UUAvlTreeIterNext(&avlScope, &uatic)) {
            if (p->enabled || !p->hidden) {
                WriteLine(s, "<tr><td>");
                SendHTMLEncoded(s, p->name);
                WriteLine(s, "</td><td>%s</td>", p->enabled ? "Enabled" : "Disabled");
                if (extended) {
                    WriteLine(s, "<td>");
                    SendHTMLEncoded(s, p->header);
                    WriteLine(s, "</td><td>");
                    schar[1] = 0;
                    for (str = p->domains; str; str = next) {
                        next = strchr(str, '|');
                        if (next == NULL) {
                            next = strchr(str, 0);
                        }
                        while (str < next) {
                            schar[0] = *str++;
                            SendHTMLEncoded(s, schar);
                        }
                        if (*next) {
                            WriteLine(s, "<br>");
                            next++;
                        } else {
                            next = NULL;
                        }
                    }
                    WriteLine(s, "</td>");
                }
                WriteLine(s, "</tr>\n");
            }
        }
        WriteLine(s, "</table>\n");
    }

    AdminFooter(t, 0);
}

static BOOL AdminIdentifierDetails(struct TRANSLATE *t, char *field, char *value) {
    struct UUSOCKET *s = &t->ssocket;

    // clang-format off
    const char *sqlTemplate =
        "SELECT"
            " identifier.user AS \"Username\","
            " scope.name AS \"Scope\","
            " DATETIME(identifier.from_at, 'unixepoch', 'localtime') AS \"From\","
            " DATETIME(identifier.to_at, 'unixepoch', 'localtime') AS \"To\","
            " identifier.pairwise AS \"Identifier\""
        " FROM"
            " identifier"
            " INNER JOIN scope ON scope.id = identifier.scopeid"
        " WHERE"
            " %s = ?";
    // clang-format on

    sqlite3_stmt *stmt = NULL;

    char sql[strlen(sqlTemplate) + strlen(field) + 10];
    sqlite3 *db = IdentifierGetSharedDatabase();
    int rows, cols;
    char *errMsg = NULL;
    int i;
    int j;
    char *facttable = NULL;
    char *rulename = NULL;
    char *user = NULL;
    struct SQLNode *table = NULL, *next;

    if (value == NULL || *value == 0) {
        return FALSE;
    }

    snprintf(sql, sizeof(sql), sqlTemplate, field);

    SQLPrepare(db, sql, &stmt);
    SQLOK(sqlite3_bind_text(stmt, 1, value, -1, SQLITE_STATIC));

    table = next = SQLGetTable(stmt, &rows, &cols);
    sqlite3_finalize(stmt);

    if (rows == 0) {
        goto NoDetail;
    } else {
        WriteLine(s, "<table id='security-tripped-detail' class='bordered-table sortable'>\n");

        const char *td = "th";
        const char *tda = " scope='col'";
        for (i = 0; i < rows; i++) {
            WriteLine(s, "<tr>\n");
            for (j = 0; j < cols; j++) {
                WriteLine(s, "<%s%s>", td, tda);
                SendHTMLEncoded(s, next->element);
                next = next->next;
                WriteLine(s, "</%s>", td);
            }
            WriteLine(s, "</tr>\n");
            td = "td";
            tda = "";
        }
        WriteLine(s, "</table><p></p>\n");
    }

    goto Finished;

NoDetail:
    WriteLine(s, "<p>No identifiers match that value.</p>\n");

Finished:
    FreeThenNull(facttable);
    FreeThenNull(rulename);
    FreeThenNull(user);
    SQLFreeTable(&table);
    return TRUE;
}

void AdminIdentifier(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    const int AIVALUE = 0;
    const int AIACTION = 1;
    struct FORMFIELD ffs[] = {
        {"value",  NULL, 0, 0, 128},
        {"action", NULL, 0, 0, 7  },
        {NULL,     NULL, 0, 0, 0  }
    };
    char *field = NULL;
    char *value;
    BOOL decode = TRUE;

    FindFormFields(query, post, ffs);

    value = ffs[AIVALUE].value;
    Trim(value, TRIM_LEAD | TRIM_TRAIL);
    if (value && *value) {
        if (strcmp(ffs[AIACTION].value, "encode") == 0) {
            decode = FALSE;
            field = "user";
            AllLower(value);
        } else if (strcmp(ffs[AIACTION].value, "decode") == 0) {
            field = "pairwise";
        }
    }

    AdminHeader(t, 0, "Identifiers");

    if (IdentifierGetSharedDatabase() == NULL) {
        WriteLine(s, "<p>Identifiers are not enabled</p>\n");
    } else {
        if (field && value && *value) {
            AdminIdentifierDetails(t, field, value);
        }

        WriteLine(s, "<h2>Lookup Identifier</h2>\n");

        UUSendZ(s, "<form action='/identifier' method='post'>\n");

        WriteLine(s,
                  "\
    <select name='action'>\n\
    <option value='decode'%s>Identifier to Username</option>\n\
    <option value='encode'%s>Username to Identifier</option>\n\
    </select>\n",
                  (decode ? " selected" : ""), (decode ? "" : " selected"));

        UUSendZ(s, "<input type='text' name='value' value='");
        if (value) {
            SendHTMLEncoded(s, value);
        }
        UUSendZ(s, "'> <input type='submit' value='Lookup'>\n</form>\n");
    }

    WriteLine(s, "<p><a href='/identifier/scopes'>View enabled/disabled scopes</a></p>\n");
    WriteLine(s, "<p>Retention: %d days</p>", retentionDays);

    AdminFooter(t, 0);
}
