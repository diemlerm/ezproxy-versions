include $(SRCDIR)/libdirs.inc

.PHONY: all clean default dist reset

default: all

reset:
	rm -f *.o

ISBETA ?= 1
RELSTR ?= beta_
# VERSION := $(shell mvn help:evaluate -Dexpression=project.version -DforceStdout)
VERSION ?= 7.0.0-DEV

NOW := $(shell TZ="GMT0" date)
BUILDDATE := $(shell TZ="GMT0" date +"%Y-%m-%dT%H:%M:%SZ" -d"$(NOW)")
COPYRIGHTYEAR := $(shell date +"%Y" -d"$(NOW)")
EXPIRES := $(shell date -d"next-wednesday +119 days")
EXPIRESYEAR := $(shell date +"%Y" -d"$(EXPIRES)")
EXPIRESMONTH := $(shell date +"%-m" -d"$(EXPIRES)")
EXPIRESDAY := $(shell date +"%-d" -d"$(EXPIRES)")
VERSIONPARTS := $(subst ., ,$(subst -, ,$(VERSION)))
VERSIONMAJOR := $(word 1, $(VERSIONPARTS))
VERSIONMINOR := $(word 2, $(VERSIONPARTS))
VERSIONMINI := $(word 3, $(VERSIONPARTS))
BETA := $(ISBETA)
RELEASE := $(RELSTR)
LOCALDIR = ../local

BUILDDEFINES := \
  -D EZPROXYBUILD='"$(BUILDDATE)"' \
  -D COPYRIGHTYEAR=$(COPYRIGHTYEAR) \
  -D EXPIRESYEAR=$(EXPIRESYEAR) \
  -D EXPIRESMONTH=$(EXPIRESMONTH) \
  -D EXPIRESDAY=$(EXPIRESDAY) \
  -D VERSIONSOURCE='"$(VERSION)"' \
  -D VERSIONMAJOR=$(VERSIONMAJOR) \
  -D VERSIONMINOR=$(VERSIONMINOR) \
  -D VERSIONMINI=$(VERSIONMINI) \
  -D EZPROXYRELEASELEVEL=$(BETA)

INCLUDEDIRSCOMMON = \
  -I ${LOCALDIR}/include \
  -I ${LOCALDIR}/include/libxml2 \
  -I ${LOCALDIR}/include/xmlsec1 \
  -I ../sqlite-autoconf-*
#  -I${GEOIPDIR}/libGeoIP \
#  -I${LIBXML2DIR}/include \
#  -I${OPENLDAPDIR}/include \
#  -I${OPENSSLDIR}/include \
#  -I${PCREDIR} \
#  -I${XMLSEC1DIR}/include \
#  -I${ZLIBDIR}

LIBSCOMMON = \
  ${LIBDIR}/libcurl.a \
  ${LIBDIR}/libmaxminddb.a \
  ${LIBDIR}/libxmlsec1-openssl.a \
  ${LIBDIR}/libxmlsec1.a \
  ${LIBDIR}/libxml2.a \
  ${LIBDIR}/libldap.a \
  ${LIBDIR}/liblber.a \
  ${LIBDIR}/libjwt.a \
  ${LIBDIR}/libssl.a \
  ${LIBDIR}/libcrypto.a \
  ${LIBDIR}/libpcre.a \
  ${LIBDIR}/libz.a \
  ${LIBDIR}/libjansson.a
#  ${GEOIPDIR}/libGeoIP/.libs/libGeoIP.a \
#  ${XMLSEC1DIR}/src/openssl/.libs/libxmlsec1-openssl.a \
#  ${XMLSEC1DIR}/src/.libs/libxmlsec1.a \
#  ${LIBXML2DIR}/.libs/libxml2.a \
#  ${OPENLDAPDIR}/libraries/libldap_r/.libs/libldap_r.a \
#  ${OPENLDAPDIR}/libraries/liblber/.libs/liblber.a \
#  ${OPENSSLDIR}/libssl.a \
#  ${OPENSSLDIR}/libcrypto.a \
#  ${PCREDIR}/.libs/libpcre.a \
#  ${ZLIBDIR}/libz.a

OBJSCOMMON = \
  account.o \
  admin.o \
  adminabout.o \
  adminaccountadd.o \
  adminaccountreset.o \
  adminadmin.o \
  adminaudit.o \
  adminauth.o \
  adminconflicts.o \
  admincookie.o \
  admincas.o \
  admincpip.o \
  adminebrary.o \
  adminfiddler.o \
  adminfmg.o \
  adminform.o \
  admingartner.o \
  admingroups.o \
  adminheaders.o \
  adminidentifier.o \
  adminintrusion.o \
  adminip.o \
  adminldap.o \
  adminlogin.o \
  adminlogout.o \
  adminmenu.o \
  adminmessages.o \
  adminmv.o \
  adminmygroups.o \
  adminnetlibrary.o \
  adminnetwork.o \
  adminoverdrive.o \
  adminpassword.o \
  adminproxyurl.o \
  adminreboot.o \
  adminrestart.o \
  adminrobots.o \
  adminscifinder.o \
  adminsecurity.o \
  adminshibboleth.o \
  adminsliptest.o \
  adminsortable.o \
  adminsso.o \
  adminssl.o \
  adminstatus.o \
  admintime.o \
  admintoken.o \
  admintop.o \
  adminunauthorized.o \
  adminunknown.o \
  adminusage.o \
  adminusagelimits.o \
  adminuserobject.o \
  adminuser.o \
  adminwebserver.o \
  apps.o \
  check.o \
  cluster.o \
  cookie.o \
  cvtutf.o \
  database.o \
  dns.o \
  efgeneral.o \
  efparsename.o \
  efrereplace.o \
  environment.o \
  expression.o \
  ezproxyversion.o \
  files.o \
  globals.o \
  group.o \
  ha.o \
  host.o \
  html.o \
  httpparser.o \
  identifier.o \
  intrusion.o \
  intrusiontxt.o \
  license.o \
  location.o \
  main.o \
  misc.o \
  ncpu.o \
  obscure.o \
  queue.o \
  req.o \
  restart.o \
  saml.o \
  security.o \
  session.o  \
  shib.o \
  spur.o \
  sql.o \
  sqlite3.o \
  ssl.o \
  telnet.o \
  thread.o \
  translate.o \
  usertoken.o \
  usr.o \
  usrcas.o \
  usrcommon.o \
  usrdomain.o \
  usrdraweb2.o \
  usrexternal.o \
  usrfinduser.o \
  usrfollett.o \
  usrftp.o \
  usrhip.o \
  usrhttpbasic.o \
  usriii.o \
  usrimap.o \
  usrinsignia.o \
  usrl4u.o \
  usrldap.o \
  usrldap2.o \
  usrncip.o \
  usrpop.o \
  usrproxy.o \
  usrradius.o \
  usrsagebrush.o \
  usrshibboleth.o \
  usrsip.o \
  usrticket.o \
  usrtlc.o \
  usrxml.o \
  uuavl.o \
  uusocket.o \
  uustring.o \
  uuxml.o \
  variables.o \
  wskey.o \
  wskeylicense.o \
  z3950.o \
  url_parser.o

