#ifndef __ADMINMESSAGES_H__
#define __ADMINMESSAGES_H__

#include "common.h"

void AdminMessages(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINMESSAGES_H__
