/**
 * The version number and optional expiration date for EZproxy are defined here.
 * Whenever changing this file, a complete rebuild should be performed to insure that
 * all dependencies are reflected.
 */

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#ifndef __EZPROXYVERSION_H__
#define __EZPROXYVERSION_H__

#define EZPROXYNAMEPROPER_ALL_UPPER "EZPROXY"
#define EZPROXYNAMEPROPER_ALL_LOWER "ezproxy"
// Data collection from WSKey server depends on EZPROXYNAMEPROPER being EZproxy.
// If changing this constant, please review wskey.c.
#define EZPROXYNAMEPROPER "EZproxy"
#define EZPROXYCOPYRIGHTSTATEMENT \
    "Copyright (c) 1993-" STR(COPYRIGHTYEAR) " OCLC (ALL RIGHTS RESERVED)"
#define EZPROXYHELPEMAIL "support@oclc.org"
#define EZPROXYURLROOT "http://www.oclc.org/ezproxy/"
#define EZPROXYHELPURLROOT "http://www.oclc.org/support/documentation/ezproxy/"
#define UNIX_NAME "UNIX"
#define WINDOWS_NAME "Windows"

#define EZPROXYMAJORSTR STR(VERSIONMAJOR)
#define EZPROXYMINORSTR STR(VERSIONMINOR)
#if !defined(EZPROXYVARSTR)
#define EZPROXYVARSTR STR(VERSIONMINI)
#endif
#define EZSOURCEVERSION VERSIONSOURCE

/* If you want this build of EZproxy to expire,
// change EZPROXYRELEASELEVEL.  It will set the
// year, month, and day of expiration.
// BETAs should probably expire.
// Note: a GA version cannot have an expiration date, and all developer builds must expire. */
#ifndef EZPROXYRELEASELEVEL
#error EZPROXYRELEASELEVEL must be passed in during linking with value 1 (beta) or 0 (release)
#endif
#if EZPROXYRELEASELEVEL == 1
#define EXPIREYEAR EXPIRESYEAR
#define EXPIREMONTH EXPIRESMONTH
#define EXPIREDAY EXPIRESDAY
#endif

// **********************************************
// No changes should be required below this point
// **********************************************

#if EZPROXYRELEASELEVEL == 1
#define EZPROXYTYPE "BETA"
#define EZPROXYTYPENUM 0
#else
#define EZPROXYTYPE "GA"
#define EZPROXYTYPENUM 2
#if EXPIREYEAR && EXPIREMONTH && EXPIREDAY
#error GA versions should not have an expiration date; see ezproxyversion.h
#endif
#endif

// Data collection from WSKey server depends on EZPROXYVERSIONNUMBER being in #.#.# format.
// If changing this constant, please review wskey.c.
#define EZPROXYVERSIONNUMBER EZPROXYMAJORSTR "." EZPROXYMINORSTR "." EZPROXYVARSTR
/* EZPROXYVERSIONROOT must never be longer than 32 characters because of struct GUARDIAN->version.
 */
#define EZPROXYVERSIONROOT EZPROXYVERSIONNUMBER " " EZPROXYTYPE
#define EZPROXYVERSIONSHORT EZPROXYVERSIONROOT

#endif  // __EZPROXYVERSION_H__
