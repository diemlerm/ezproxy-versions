#define __UUFILE__ "group.c"

#include "common.h"
#include "usr.h"
#include <sys/stat.h>

#ifndef WIN32
#include <dirent.h>
#endif

#ifndef S_ISDIR
#define S_ISDIR(m) (((m)&S_IFMT) == S_IFDIR)
#endif

struct GROUP *FindGroup(char *name, size_t len, BOOL add) {
    struct GROUP *g, *last, *n;
    int c;

    if (len == 0)
        len = strlen(name);

    for (last = NULL, g = groups; g; last = g, g = g->next) {
        c = strnicmp(g->name, name, len);
        if (c == 0) {
            c = strlen(g->name) - len;
            if (c == 0)
                return g;
        }

        /* Force the default group to always be first in the list */
        if (g->index == 0)
            continue;
        if (c > 0)
            break;
    }

    if (!add)
        return NULL;

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;
    mGroupMask = (cGroups >> 5) + 1;
    if (!(n->name = malloc(len + 1)))
        PANIC;
    StrCpy3(n->name, name, len + 1);
    n->nameLen = len;
    n->gm = GroupMaskNew(0);
    n->index = cGroups;
    GroupMaskSet(n->gm, cGroups);
    n->next = g;
    if (last == NULL)
        groups = n;
    else
        last->next = n;
    cGroups++;
    return n;
}

void MakeGroupMask1(GROUPMASK gm, char mode, char *name, size_t len, BOOL add) {
    char *p;
    struct GROUP *g;

    /* Skip leading white space */
    for (; len > 0 && IsWS(*name); name++, len--)
        ;
    /* Skip trailing white space */
    for (p = name + len - 1; p >= name && IsWS(*p); p--, len--)
        ;
    if (len > 0) {
        if ((g = FindGroup(name, len, add)) != NULL) {
            switch (mode) {
            case '=':
                GroupMaskCopy(gm, g->gm);
                break;
            case '+':
                GroupMaskOr(gm, g->gm);
                break;
            case '-':
                GroupMaskMinus(gm, g->gm);
                break;
            }
        } else if (mode == '=')
            GroupMaskClear(gm);
    }
}

BOOL SingletonGroupName(char *name) {
    if (name == NULL || *name == 0 || strchr(name, '+') || strchr(name, '-') || strchr(name, '=') ||
        stricmp(name, "NULL") == 0)
        return 0;

    return 1;
}

GROUPMASK MakeGroupMask(GROUPMASK gm, char *names, BOOL add, char mode) {
    char *p, *s;
    /* char mode; */

    if (gm == NULL)
        gm = GroupMaskNew(0);

    if (names == NULL) {
        return GroupMaskDefault(gm);
    }

    names = SkipWS(names);

    if (*names == 0) {
        return GroupMaskDefault(gm);
    }

    if (stricmp(names, "NULL") == 0) {
        GroupMaskClear(gm);
        return gm;
    }

    /*
    mode = '=';
    */
    for (p = s = names; *p; p++) {
        if (*p == '+' || *p == '-' || *p == '=') {
            if (s < p)
                MakeGroupMask1(gm, mode, s, p - s, add);
            mode = *p;
            s = p + 1;
        }
    }
    if (s < p)
        MakeGroupMask1(gm, mode, s, p - s, add);

    return gm;
}

static size_t groupMaskMinSize = 1;

GROUPMASK GroupMaskNew(size_t size) {
    GROUPMASK g1;

    if (size < mGroupMask)
        size = mGroupMask;
    if (size < groupMaskMinSize)
        size = groupMaskMinSize;

    if (size > groupMaskMinSize)
        groupMaskMinSize = size;

    if (!(g1 = calloc(1, sizeof(*g1))))
        PANIC;
    if (!(g1->bits = calloc(size, sizeof(*g1->bits))))
        PANIC;
    g1->size = size;
    return g1;
}

GROUPMASK GroupMaskMinSize(GROUPMASK g1, size_t minSize) {
    if (g1 == NULL)
        return GroupMaskNew(minSize);

    if (minSize < groupMaskMinSize)
        minSize = groupMaskMinSize;
    else if (minSize > groupMaskMinSize)
        groupMaskMinSize = minSize;

    if (g1->size < minSize) {
        if (!(g1->bits = realloc(g1->bits, minSize * sizeof(*g1->bits))))
            PANIC;
        memset(g1->bits + g1->size, 0, (minSize - g1->size) * sizeof(*g1->bits));
        g1->size = minSize;
    }
    return g1;
}

void GroupMaskFree(GROUPMASK *g1) {
    if (g1) {
        if (*g1) {
            FreeThenNull((*g1)->bits);
            FreeThenNull(*g1);
        }
    }
}

GROUPMASK GroupMaskAnd(GROUPMASK g1, GROUPMASK g2) {
    size_t i;
    unsigned int *b1, *b2;

    if (g2 == NULL) {
        GroupMaskClear(g1);
        return g1;
    }

    for (i = 0, b1 = g1->bits, b2 = g2->bits; i < g1->size; i++) {
        if (i < g2->size)
            *b1++ &= *b2++;
        else
            *b1++ = 0;
    }

    return g1;
}

GROUPMASK GroupMaskOr(GROUPMASK g1, GROUPMASK g2) {
    size_t i, stop;
    unsigned int *b1, *b2;

    if (g2 == NULL)
        return g1;

    GroupMaskMinSize(g1, g2->size);

    stop = UUMIN(g1->size, g2->size);

    for (i = 0, b1 = g1->bits, b2 = g2->bits; i < stop; i++) {
        *b1++ |= *b2++;
    }

    return g1;
}

GROUPMASK GroupMaskMinus(GROUPMASK g1, GROUPMASK g2) {
    size_t i, stop;
    unsigned int *b1, *b2;

    if (g2 == NULL)
        return g1;

    stop = UUMIN(g1->size, g2->size);

    for (i = 0, b1 = g1->bits, b2 = g2->bits; i < stop; i++) {
        *b1++ &= ~*b2++;
    }

    return g1;
}

GROUPMASK GroupMaskNot(GROUPMASK g1) {
    size_t i;
    unsigned int *b1;

    for (i = 0, b1 = g1->bits; i < g1->size; i++, b1++) {
        *b1 = ~*b1;
    }

    return g1;
}

BOOL GroupMaskOverlap(GROUPMASK g1, GROUPMASK g2) {
    size_t i, stop;
    unsigned int *b1, *b2;

    if (g1 == NULL || g2 == NULL)
        return 0;

    stop = UUMIN(g1->size, g2->size);

    for (i = 0, b1 = g1->bits, b2 = g2->bits; i < stop; i++, b1++, b2++) {
        if (*b1 & *b2)
            return 1;
    }

    return 0;
}

GROUPMASK GroupMaskCopy(GROUPMASK g1, GROUPMASK g2) {
    if (g2 == NULL) {
        GroupMaskClear(g1);
        return g1;
    }

    g1 = GroupMaskMinSize(g1, g2->size);

    memcpy(g1->bits, g2->bits, g2->size * sizeof(*g1->bits));
    if (g1->size > g2->size)
        memset(g1->bits + g2->size, 0, (g1->size - g2->size) * sizeof(*g1->bits));

    return g1;
}

void GroupMaskClear(GROUPMASK g1) {
    if (g1)
        memset(g1->bits, 0, g1->size * sizeof(*g1->bits));
}

GROUPMASK GroupMaskDefault(GROUPMASK g1) {
    if (g1 == NULL)
        g1 = GroupMaskNew(0);
    else
        GroupMaskClear(g1);

    GroupMaskSet(g1, 0);

    return g1;
}

void GroupMaskSet(GROUPMASK g1, unsigned int group) {
    unsigned int i = group >> 5;
    unsigned int j = group & 31;

    if (g1) {
        GroupMaskMinSize(g1, i + 1);
        g1->bits[i] |= (1 << j);
    }
}

void GroupMaskReset(GROUPMASK g1, int group) {
    unsigned int i = group >> 5;
    unsigned int j = group & 31;

    if (g1) {
        GroupMaskMinSize(g1, i + 1);
        g1->bits[i] &= (~(1 << j));
    }
}

BOOL GroupMaskIsSet(GROUPMASK g1, int group) {
    unsigned int i = group >> 5;
    unsigned int j = group & 31;

    return (g1 && i < g1->size ? (g1->bits[i] & (1 << j)) != 0 : 0);
}

size_t GroupMaskCardinality(GROUPMASK g1) {
    size_t i;
    size_t cardinality = 0;
    unsigned int *b1, b1v;

    if (g1 == NULL)
        return 0;

    for (i = 0, b1 = g1->bits; i < g1->size; i++, b1++) {
        for (b1v = *b1; b1v; b1v >>= 1)
            if (b1v & 1)
                cardinality++;
    }

    return cardinality;
}

BOOL GroupMaskIsNull(GROUPMASK g1) {
    return GroupMaskCardinality(g1) == 0;
}

BOOL GroupMaskEqual(GROUPMASK g1, GROUPMASK g2) {
    size_t i, stop;
    unsigned int *b1, *b2, b1v, b2v;

    if (g1 == NULL)
        return GroupMaskIsNull(g2);

    if (g2 == NULL)
        return GroupMaskIsNull(g1);

    stop = UUMAX(g1->size, g2->size);

    for (i = 0, b1 = g1->bits, b2 = g2->bits; i < stop; i++) {
        b1v = (i < g1->size ? *b1++ : 0);
        b2v = (i < g2->size ? *b2++ : 0);
        if (b1v != b2v)
            return 0;
    }
    return 1;
}

BOOL GroupMaskWrite(GROUPMASK gm, struct UUSOCKET *s, struct USERINFO *uip) {
    BOOL any = 0;
    struct GROUP *g;

    for (g = groups; g; g = g->next) {
        if (GroupMaskOverlap(g->gm, gm)) {
            if (any) {
                if (s != NULL) {
                    UUSendZ(s, " ");
                }
            } else {
                any = 1;
            }
            if (s == NULL) {
                UsrLog(uip, " %s ", g->name);
            } else {
                SendHTMLEncoded(s, g->name);
            }
        }
    }
    return any;
}

void GroupMaskDebug(GROUPMASK gm) {
    struct GROUP *g;

    for (g = groups; g; g = g->next)
        if (GroupMaskOverlap(gm, g->gm))
            printf("%s ", g->name);
    printf("\n");
}

void ResizeAllGroupMasks(void) {
    struct GROUP *g;

    if (mGroupMask > 1)
        for (g = groups; g; g = g->next)
            if (g->gm)
                GroupMaskMinSize(g->gm, mGroupMask);
}

void LoggedInGroups(void) {
    char *fn;
    char *searchDir = DOCSDIR "loggedin" DIRSEP;
    char checkDir[MAX_PATH];
    struct stat buf;
#ifdef WIN32
    char searchName[MAX_PATH];
    WIN32_FIND_DATA fileData;
    HANDLE hSearch;
#else
    DIR *dir;
    struct dirent *dirent;
#endif

#ifdef WIN32
    sprintf(searchName, "%s*.*", searchDir);
    hSearch = FindFirstFile(searchName, &fileData);
    if (hSearch == INVALID_HANDLE_VALUE)
        return;
    do {
        fn = fileData.cFileName;
#else
    dir = opendir(searchDir);
    if (dir == NULL) {
        return;
    }

    while (dirent = readdir(dir)) {
        fn = dirent->d_name;
#endif
        if (*fn == '.')
            continue;
        sprintf(checkDir, "%s%s", searchDir, fn);
        if (stat(checkDir, &buf) < 0)
            continue;
        if (S_ISDIR(buf.st_mode)) {
            FindGroup(fn, 0, 1);
        }
#ifdef WIN32
    } while (FindNextFile(hSearch, &fileData));
    FindClose(hSearch);
#else
    }
    closedir(dir);
#endif
}

BOOL IsAnAdminGroup(struct GROUP *g) {
    return g && StartsIWith(g->name, "Admin.");
}
