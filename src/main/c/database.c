#define __UUFILE__ "database.c"

#include "ezproxyversion.h"
#include "common.h"
#include "url_parser.h"

#ifndef WIN32
#include <pwd.h>
#include <grp.h>
#else
#include <direct.h> /* needed for getcwd */
#endif

#include <sys/stat.h>

#include "adminaudit.h"
#include "admincas.h"
#include "admingartner.h"
#include "adminuserobject.h"
#include "identifier.h"
#include "intrusion.h"
#include "saml.h"
#include "security.h"

#define TAB "\t"

int currentLineNum;

enum OPTIONCOOKIE optionCookie;
BOOL optionRedirectPatch;
BOOL optionSendNoCache;
BOOL optionHideEZproxy;
BOOL optionXForwardedFor;
BOOL optionProxyFtp;
BOOL optionMetaEZproxyRewriting;
BOOL optionLawyeePatch;
BOOL intruderAttemptsClassic = 1;
BOOL optionNoHttpsHyphens = 0;
BOOL optionUTF16;
BOOL optionGroupInReferer;
BOOL sslHonorCipherOrder = 1;
int sslIdx = 0;
struct OPENSSLCONFCMD *openSSLConfCmds = NULL;
char *sqliteTmpDir = NULL;

char *addUserHeader = NULL;
struct REFERER *referer = NULL;
size_t addUserHeaderColon = 0;
BOOL addUserHeaderBase64 = 0;

struct VALIDATE *validate = NULL;
GROUPMASK gm;
GROUPMASK refererGm;

struct sockaddr_storage ipInterface;

char *proxyHost = NULL;
PORT proxyPort = 0;
char *proxyAuth = NULL;
char *proxySslHost = NULL;
PORT proxySslPort = 0;
char *proxySslAuth = NULL;
char *rerouteTo = NULL;
BOOL rerouteQuote = 0;

struct PATTERN *lastPattern = NULL;
struct PATTERN *nextPattern = NULL;

char *dbvars[MAXDBVARS];

char *optionAllowBadDomainsText =
    "I choose to use Domain lines that threaten the security of my network";

BOOL validateReferersRequired = 0;

struct IPBLOCK {
    struct IPBLOCK *flink, *blink;
    struct sockaddr_storage start;
    struct sockaddr_storage stop;
};

BOOL IPUnion(struct sockaddr_storage *start1,
             struct sockaddr_storage *stop1,
             struct sockaddr_storage *start2,
             struct sockaddr_storage *stop2,
             struct sockaddr_storage *overStart,
             struct sockaddr_storage *overStop) {
    /* If the first range starts after the second, reverse the ranges.
       This simplifies the adjacent range test.
    */
    if (compare_ip(start1, start2) > 0) {
        struct sockaddr_storage swap;

        memcpy(&swap, start1, sizeof start1);
        memcpy(start1, start2, sizeof start2);
        memcpy(start2, &swap, sizeof swap);

        memcpy(&swap, stop1, sizeof stop1);
        memcpy(stop1, stop2, sizeof stop2);
        memcpy(stop2, &swap, sizeof swap);
    }
    /* If first range stops before the second starts, there is no
       overlap unless they are adjacent.
    */
    if (compare_ip(start2, stop1) > 0) {
        /* Check for the case where the two ranges are adjacent.
           No risk of stop1 + 1 overflowing since stop1 < start2
           so even if start2 = 0xffffffff, stop1 can't be 0xffffffff
        */
        char empty[12];
        if (get_range32(stop1, start2, empty) == 1) {
            // if (stop1 + 1 == start2) { The old unsigned long comparison
            memcpy(overStart, start1, sizeof start1);
            memcpy(overStop, stop2, sizeof stop2);
            return TRUE;
        }
        /* Since ranges aren't adjacent, no overlap */
        return FALSE;
    }
    /* If there is any overlap, return from the smallest start to the largest stop */
    struct sockaddr_storage *umin, *lmax;
    lmax = get_max_ip(start1, start2);
    umin = get_min_ip(stop1, stop2);

    // make sure we have valid pointers and that the upper min is >= lower max
    if (lmax != NULL && umin != NULL && compare_ip(lmax, umin) >= 0) {
        memcpy(overStart, lmax, sizeof lmax);
        memcpy(overStop, umin, sizeof umin);
        return TRUE;
    }
    return FALSE;
}

struct IPBLOCK *NewIPBlock(void) {
    struct IPBLOCK *ipb;

    if (!(ipb = (struct IPBLOCK *)calloc(1, sizeof(*ipb))))
        PANIC;
    ipb->flink = ipb->blink = ipb;
    return ipb;
}

struct IPBLOCK *InsertBeforeIPBlock(struct IPBLOCK *insertBefore,
                                    struct sockaddr_storage *start,
                                    struct sockaddr_storage *stop) {
    struct IPBLOCK *n;

    if (!(n = (struct IPBLOCK *)calloc(1, sizeof(*n))))
        PANIC;

    memcpy(&n->start, start, sizeof start);
    memcpy(&n->stop, stop, sizeof stop);

    n->flink = insertBefore;
    n->blink = insertBefore->blink;
    n->flink->blink = n;
    n->blink->flink = n;
    return n;
}

void UnlinkIPBlock(struct IPBLOCK *ipb) {
    if (ipb) {
        ipb->flink->blink = ipb->blink;
        ipb->blink->flink = ipb->flink;
        free(ipb);
    }
}

void AddIPBlock(struct IPBLOCK *ipb,
                struct sockaddr_storage *start,
                struct sockaddr_storage *stop) {
    struct IPBLOCK *p;
    struct sockaddr_storage overStart, overStop;

    for (p = ipb->flink;; p = p->flink) {
        if (p == ipb) {
            p = InsertBeforeIPBlock(ipb, start, stop);
            break;
        }
        if (IPUnion(&p->start, &p->stop, start, stop, &overStart, &overStop)) {
            memcpy(&p->start, &overStart, sizeof overStart);
            memcpy(&p->stop, &overStop, sizeof overStop);
            break;
        }
        if (compare_ip(&p->start, stop) > 0) {
            p = InsertBeforeIPBlock(p, start, stop);
            break;
        }
    }

    while (p->blink != ipb) {
        if (!IPUnion(&p->blink->start, &p->blink->stop, &p->start, &p->stop, &overStart, &overStop))
            break;

        memcpy(&p->start, &overStart, sizeof overStart);
        memcpy(&p->stop, &overStop, sizeof overStop);
        UnlinkIPBlock(p->blink);
    }

    while (p->flink != ipb) {
        if (!IPUnion(&p->flink->start, &p->flink->stop, &p->start, &p->stop, &overStart, &overStop))
            break;

        memcpy(&p->start, &overStart, sizeof overStart);
        memcpy(&p->stop, &overStop, sizeof overStop);

        UnlinkIPBlock(p->flink);
    }
}

void PrintIPBlock(struct IPBLOCK *ipb) {
    struct IPBLOCK *p;
    char ipBuffer1[INET6_ADDRSTRLEN], ipBuffer2[INET6_ADDRSTRLEN];

    for (p = ipb->flink; p != ipb; p = p->flink) {
        printf("%s-%s\n", ipToStr(&p->start, ipBuffer1, sizeof ipBuffer1),
               ipToStr(&p->stop, ipBuffer2, sizeof ipBuffer2));
    }
    printf("******\n");
}

void DeleteIPBlock(struct IPBLOCK *ipb,
                   struct sockaddr_storage *start,
                   struct sockaddr_storage *stop) {
    struct IPBLOCK *p, *next;

    for (p = ipb->flink; p != ipb; p = next) {
        next = p->flink;
        if (compare_ip(&p->start, stop) > 0)
            break;
        if (compare_ip(start, &p->stop) > 0)
            continue;
        if (compare_ip(&p->start, start) >= 0) {
            /* If this entire block is within the deletion range, remove it */
            if (compare_ip(stop, &p->stop) >= 0) {
                UnlinkIPBlock(p);
                continue;
            }

            /* Shift the start of this block over to beyond the deletion point */
            // aka just add 1 to the address
            // used to be: p->start = stop + 1;
            memcpy(&p->start, stop, sizeof stop);
            increment_ip(&p->start, 1);
            continue;
        }

        /* If the start is not within the range, but the stop is, truncate this range */
        if (compare_ip(stop, &p->stop) >= 0) {
            memcpy(&p->stop, start, sizeof start);

            // seems like it is still possible to get here with an address of 0
            // which this will break on
            increment_ip(&p->stop, -1);
            continue;
        }

        /* The deletion is a subset of a main range, so we need to split the
           main range into two ranges setting the upper bound of the first to
           the deletion start - 1 and the lower bound of the second to
           the deletion stop + 1
        */
        struct sockaddr_storage temp;
        memcpy(&temp, stop, sizeof stop);
        increment_ip(&temp, 1);

        InsertBeforeIPBlock(p->flink, &temp, &p->stop);

        memcpy(&p->stop, start, sizeof start);
        increment_ip(&p->stop, -1);

        break;
    }
}

void FreeIPBlock(struct IPBLOCK *ipb) {
    struct IPBLOCK *p, *next;

    /* Set the last node to point to null, which we use to know when we hit the end */
    (ipb->blink)->flink = NULL;

    for (p = ipb; p; p = next) {
        next = p->flink;
        free(p);
    }
}

void AbortFatalConfig() {
    if (guardian) {
        guardian->chargeStatus = chargeStopping;
        guardian->chargeStop = 1;
        guardian->guardianStop = 1;
    }

    exit(1);
}

void WarnIfLowPort(int port, char checkingFirstPort) {
#ifndef WIN32
    static char warned = 0;

    if (port >= 1024 || warned)
        return;

    if (geteuid() != 0 || (runUid != 0 && checkingFirstPort)) {
        Log("WARNING: " EZPROXYNAMEPROPER " can only use ports below 1024 if it is run by root");
        warned = 1;
    }
#endif
}

BOOL ParseAddressRange(char *range,
                       unsigned long *start,
                       unsigned long *stop,
                       char *fileName,
                       int line) {
    char *hyphen;
    char *comment;
    BOOL anyBad = 0;

    if (comment = strchr(range, '#'))
        *comment = 0;

    Trim(range, TRIM_COLLAPSE);

    hyphen = strchr(range, '-');
    if (hyphen)
        *hyphen++ = 0;

    *start = ntohl(inet_addr(range));
    if (*start == INADDR_NONE && strcmp(range, "255.255.255.255") != 0) {
        anyBad = 1;
        Log("Invalid address %s(%d): %s", fileName, line, range);
    }

    if (hyphen) {
        *stop = ntohl(inet_addr(hyphen));
        if (*stop == INADDR_NONE && strcmp(hyphen, "255.255.255.255") != 0) {
            anyBad = 1;
            Log("Invalid address %s(%d): %s", fileName, line, hyphen);
        }
        *(hyphen - 1) = '-';
    } else
        *stop = *start;

    if (anyBad)
        return 0;

    if (*stop < *start) {
        Log("Stopping address before starting address; line ignored: %s", range);
        return 0;
    }

    return 1;
}

/* range is manipulated by this routine */
BOOL ParseAddressRangeNew(char *range,
                          struct sockaddr_storage *start,
                          struct sockaddr_storage *stop,
                          const char *fileName,
                          int line) {
    char *hyphen;
    char *comment;
    BOOL anyBad = 0;
    char *slash;

    if (comment = strchr(range, '#'))
        *comment = 0;

    Trim(range, TRIM_COLLAPSE);

    hyphen = strchr(range, '-');
    slash = strchr(range, '/');

    if (hyphen && slash) {
        Log("Ignoring invalid IP range: %s", range);
        return 0;
    }

    if (hyphen) {
        *hyphen++ = 0;
    }

    if (slash) {
        *slash++ = 0;
    }

    // ipv6 comes in brackets
    int family = (NumericHostIp4(range)) ? AF_INET : AF_INET6;

    init_storage(start, family, 0, 0);
    init_storage(stop, family, 0, 0);

    int result = inet_pton_st(family, range, start);

    if (result <= 0 || is_addrnone(start)) {
        anyBad = 1;
        Log("Invalid address %s(%d): %s", fileName, line, range);
    }

    if (hyphen) {
        int family2 = (NumericHostIp4(hyphen)) ? AF_INET : AF_INET6;

        if (family != family2) {
            anyBad = 1;
            Log("Address families did not match, %s(%d): %s", fileName, line, hyphen);
        } else {
            init_storage(stop, family, 0, 0);
            inet_pton_st(family, hyphen, stop);
        }

        *(hyphen - 1) = '-';
    } else if (slash) {
        int netSize;
        if (sscanf(slash, "%d", &netSize) != 1 || netSize < 0) {
            anyBad = 1;
            Log("Invalid network size %s(%d): %s", fileName, line, slash);
        } else {
            int i;
            inet_pton_st(family, range, stop);
            for (i = netSize; set_ip_bit(start, i, 0) && set_ip_bit(stop, i, 1); i++) {
            }
        }

        *(slash - 1) = '/';
    } else {
        inet_pton_st(family, range, stop);
    }

    if (anyBad)
        return 0;

    if (compare_ip(stop, start) < 0) {
        Log("Stopping address before starting address; line ignored: %s", range);
        return 0;
    }

    return 1;
}

/**
 * Basically:
 * <code>
 * char *orig = *string;
 * *string = strdup(val);
 * FreeThenNull(orig);
 * </code>
 *
 * Just copies the val into your pointer and then free the old value.
 */
void ReadConfigSetString(char **string, char *val) {
    char *oldString;
    char *newString;

    oldString = *string;

    if (val == NULL || *val == 0) {
        newString = NULL;
        goto Swap;
    }

    if (oldString) {
        if (strcmp(oldString, val) == 0)
            return;
    }

    if (!(newString = strdup(val)))
        PANIC;

Swap:
    *string = newString;
    FreeThenNull(oldString);
}

static void ReadConfigSQLiteTmpDir(char *arg) {
    struct stat statBuf;

    if (*arg && stat(arg, &statBuf) == 0 && S_ISDIR(statBuf.st_mode)) {
        ReadConfigSetString(&sqliteTmpDir, arg);
    } else {
        Log("SQLiteTmpDir invalid or non-existent directory: %s", arg);
    }
}

static void ReportAthensDiscontinued(const char *cmd) {
    Log("%s is no longer valid; Eduserv discontinued the Athens API required for this directive",
        cmd);
}

static void ReadConfigValidateFree(void);

static void ReadConfigAddUserHeader(char *arg) {
    char *arg2;
    char *colon;

    addUserHeaderBase64 = 0;
    addUserHeaderColon = 0;

    for (;;) {
        if (*arg != '-')
            break;
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (stricmp(arg, "-base64") == 0)
            addUserHeaderBase64 = 1;
        else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized AddUserHeader option: %s", arg);
        }
        arg = arg2;
    }

    if (*arg == 0) {
        addUserHeader = NULL;
        addUserHeaderBase64 = 0;
        return;
    }

    if (!(addUserHeader = malloc(strlen(arg) + 3)))
        PANIC;
    strcpy(addUserHeader, arg);
    colon = strchr(addUserHeader, ':');
    if (colon == NULL) {
        strcat(addUserHeader, ":");
        colon = strchr(addUserHeader, ':');
    }
    addUserHeaderColon = colon - addUserHeader + 1;
    strcat(addUserHeader, " ");
    usageLogUser |= ULUADDUSERHEADER;
}

struct DATABASE *ReadConfigAdvance(void) {
    struct DATABASE *b;

    ReadConfigValidateFree();

    b = databases + cDatabases;

    /* Close out any lingering find without replace */
    if (nextPattern) {
        Log(EZPROXYCFG " FIND lacking a REPLACE ignored");
        FreeThenNull(nextPattern->matchString);
        nextPattern = NULL;
    }
    lastPattern = NULL;

    if (b->title != NULL && (b->getName != NULL || b->dbdomains != NULL || b->desc != NULL)) {
        if (b->url == NULL) {
            if (!(b->url = strdup("")))
                PANIC;
        }
        if (debugLevel > 20) {
            Log("Database defined: C %d, T %s, U %s, D %s, desc %s", cDatabases, b->title, b->url,
                b->dbdomains ? (b->dbdomains->domain ? b->dbdomains->domain : "") : "",
                b->desc ? b->desc : "");
        }
        b++;
        cDatabases++;
    } else {
        b->title = NULL;
        b->url = NULL;
        b->dbdomains = NULL;
    }
    return b;
}

static void ReadConfigTitle(char *arg);

struct DATABASE *ReadConfigCurrentDatabase(void) {
    struct DATABASE *b;

    for (;;) {
        b = databases + cDatabases;
        if (b->title != NULL) {
            b->optionCookie = optionCookie;
            b->redirectPatch = optionRedirectPatch;
            b->sendNoCache = optionSendNoCache;
            b->optionHideEZproxy = optionHideEZproxy;
            b->optionXForwardedFor = optionXForwardedFor;
            b->optionProxyFtp = optionProxyFtp;
            b->optionMetaEZproxyRewriting = optionMetaEZproxyRewriting;
            b->optionUTF16 = optionUTF16;
            b->optionGroupInReferer = optionGroupInReferer;
            b->proxyHost = proxyHost;
            b->proxyPort = proxyPort;
            b->proxyAuth = proxyAuth;
            b->proxySslHost = proxySslHost;
            b->proxySslPort = proxySslPort;
            b->proxySslAuth = proxySslAuth;
            b->addUserHeader = addUserHeader;
            b->addUserHeaderBase64 = addUserHeaderBase64;
            b->addUserHeaderColon = addUserHeaderColon;
            b->referer = referer;
            return b;
        }
        ReadConfigTitle(NULL);
    }
}

static void ReadConfigAccount(char *arg) {
    ReadConfigSetString(&accountDefaultPassword, arg);
}

static void ReadConfigAllowVars(char *arg) {
    struct DATABASE *b;
    char *p;

    b = ReadConfigCurrentDatabase();
    if (b->allowVars) {
        if (StrIStr(b->allowVars, arg))
            return;
        if (!(p = malloc(strlen(b->allowVars) + strlen(arg) + 2)))
            PANIC;
        sprintf(p, "%s,%s", b->allowVars, arg);
        FreeThenNull(b->allowVars);
        b->allowVars = p;
    } else {
        if (!(b->allowVars = strdup(arg)))
            PANIC;
    }
    AllLower(b->allowVars);

    /* If username will be allowed, we have to track what it is */
    if (strchr(b->allowVars, 'u'))
        usageLogUser |= ULUALLOWVARSU;
}

static void ReadConfigAnonymousUrl(char *arg, char *file, int lineNumber) {
    char *arg2;
    xmlRegexpPtr xrp = NULL;
    struct ANONYMOUSURL *au;
    BOOL re = 0;
    BOOL cs = 0;
    BOOL options = 0;
    char activeIP = 0;

    for (;;) {
        if ((arg2 = StartsIWith(arg, "-OPTIONS "))) {
            options = 1;
        } else if (arg2 = StartsIWith(arg, "-RE ")) {
            re = 1;
        } else if (arg2 = StartsIWith(arg, "-CS ")) {
            cs = 1;
        } else if (arg2 = StartsIWith(arg, "-ActiveIP ")) {
            activeIP = 1;
        } else {
            if (arg2 = StartsIWith(arg, "-- ")) {
                arg = SkipWS(arg2);
            }
            break;
        }
        arg = SkipWS(arg2);
    }

    if (*arg == 0)
        return;

    if (re) {
        xrp = xmlRegexpCompile(BAD_CAST(*arg == '+' || *arg == '-' ? arg + 1 : arg));
        if (xrp == NULL) {
            Log("Invalid regular expression for AnonymousURL -RE: %s", arg);
            return;
        }
    }

    au = anonymousUrls + cAnonymousUrls++;
    if (*arg == '+' || *arg == '-')
        au->mode = *arg++;
    else
        au->mode = '+';
    au->regularExpression = xrp;
    au->caseSensitive = cs;
    au->activeIP = activeIP;
    au->options = options;
    if (!(au->anonymousUrl = strdup(arg)))
        PANIC;
}

static void ReadConfigAudit(char *arg) {
    char *str, *token;
    char *n;
    uint64_t i;
    int j;
    BOOL any;
    size_t tokenLen;
    BOOL exact;
    BOOL minus;

    for (str = NULL, token = StrTok_R(arg, WS, &str); token; token = StrTok_R(NULL, WS, &str)) {
        if (minus = *token == '-')
            token++;
        if (*token == 0)
            continue;
        tokenLen = strlen(token);
        if (stricmp(token, "Most") == 0) {
            if (minus)
                auditEventsMask &= ~AUDITMOSTMASK;
            else
                auditEventsMask |= AUDITMOSTMASK;
        } else {
            for (any = 0, i = 1, j = 0;; i <<= 1, j++) {
                n = AuditEventNameSelect(j);
                if (*n == 0) {
                    if (any == 0)
                        Log("Unrecognized Audit option ignored: %s", token);
                    break;
                }
                if (strnicmp(token, n, tokenLen) == 0 &&
                    ((exact = (n)[tokenLen] == 0) || (n)[tokenLen] == '.')) {
                    if (exact && (i & AUDITREMOVEDMASK)) {
                        Log("Audit option withdrawn: %s", n);
                        break;
                    } else {
                        /* if (exact || (AUDITMOSTMASK & i)) { */
                        if (exact) {
                            if (minus)
                                auditEventsMask &= ~i;
                            else
                                auditEventsMask |= i;
                            any = 1;
                        }
                    }
                }
            }
        }
    }

    if (auditEventsMask)
        usageLogUser |= ULUAUDIT;
}

static void ReadConfigAuditPurge(char *arg) {
    auditPurge = atoi(arg);
}

/*
static void ReadConfigAuth(char *arg)
{
    if (!(externAuth = strdup(arg))) PANIC;
}
*/

static void ReadConfigAutoLoginIPBanner(char *arg) {
    if (ValidMenu(arg)) {
        if (autoLoginIPBanner == NULL)
            if (!(autoLoginIPBanner = malloc(MAXMENU)))
                PANIC;
        sprintf(autoLoginIPBanner, "%s%s", DOCSDIR, arg);
    } else {
        Log("Invalid AutoLoginIPBanner file: %s", arg);
    }
}

static void ReadConfigCacheControlMaxAge(char *arg) {
    cacheControlMaxAge = atoi(arg);
}

static void ReadConfigCASServiceURL(char *arg) {
    struct CASSERVICEURL *n;
    char *arg2;
    BOOL anonymous = FALSE;
    char *p;
    char *scope = "NULL";

    for (;;) {
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (*arg != '-')
            break;
        if (stricmp(arg, "-anonymous") == 0) {
            anonymous = TRUE;
        } else if ((p = StartsIWith(arg, "-scope"))) {
            scope = p;
        } else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized CASServiceURL option: %s", arg);
        }
        arg = arg2;
    }

    if (*arg == 0) {
        Log("CAS requires a wildcard URL for an authorized service URL");
        return;
    }

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;
    if (!(n->url = strdup(arg)))
        PANIC;
    n->next = casServiceUrls;
    n->anonymous = anonymous;
    n->gm = GroupMaskCopy(NULL, gm);
    n->scope = MakeGroupMask(NULL, scope, 1, '=');

    casServiceUrls = n;
    usageLogUser |= ULUCAS;
}

static void ReadConfigExcludeIPBanner(char *arg) {
    char once = 1;
    char *p;

    if (p = StartsIWith(arg, "-always ")) {
        once = 0;
        arg = SkipWS(p);
    }

    if (ValidMenu(arg)) {
        if (excludeIPBanner == NULL)
            if (!(excludeIPBanner = malloc(MAXMENU)))
                PANIC;
        sprintf(excludeIPBanner, "%s%s", DOCSDIR, arg);
        excludeIPBannerOnce = once;
    } else {
        Log("Invalid ExcludeIPBanner file: %s", arg);
    }
}

static void ReadConfigBinaryTimeout(char *arg) {
    int i;

    i = atoi(arg);
    if (i > 0)
        binaryTimeout = i;
    else {
        Log("Invalid BinaryTimeout value: %s", arg);
    }
}

static void ReadConfigBooks24X7Site(char *arg) {
    struct DATABASE *b;

    b = ReadConfigCurrentDatabase();

    ReadConfigSetString(&b->books24X7Site, arg);
}

static void ReadConfigChargeForceSeizure(char *arg) {
    char *name;
    int i;
    int delay = 0;

    if (guardian->gpid == 0) {
        Log("ChargeForceSeizure disabled when EZproxy is not running under Guardian");
        return;
    }

    name = arg;
    arg = SkipNonWS(arg);
    if (arg) {
        *arg++ = 0;
        arg = SkipWS(arg);
        delay = atoi(arg);
    }

    i = GetChargeIndexFromName(name);
    if (i == -1) {
        Log("Invalid ChargeForceSeizure name: %s", arg);
        return;
    }
    if (delay > 0)
        guardianChargeForceSeizureDelay = delay;
    guardianChargeForceSeizureIndex = i;
}

static void ReadConfigChargeSetLatency(char *arg, BOOL nameOnly) {
    char *name;
    int i;
    int latency = 0;
    // Latency computations wrap at 49.7 days, so it is illegal to have a latency value that large
    int maxLatency = 49 * ONE_DAY_IN_SECONDS;

    if (guardian != NULL && guardian->gpid == 0) {
        Log("ChargeSetLatency disabled when EZproxy is not running under Guardian");
        return;
    }

    name = arg;
    arg = SkipNonWS(arg);
    if (arg) {
        *arg++ = 0;
        arg = SkipWS(arg);
        latency = atoi(arg);
    }

    i = GetChargeIndexFromName(name);

    if (i == -1) {
        if (!nameOnly) {
            Log("Invalid ChargeSetLatency name: %s", arg);
        }
        return;
    }

    if (latency < 0) {
        if (!nameOnly) {
            Log("ChargeSetLatency may not be set to a negative number");
        }
        return;
    }

    if (latency > maxLatency) {
        if (!nameOnly) {
            Log("ChargeSetLatency may not be set larger than %d", maxLatency);
        }
        return;
    }

    guardianChargeMaximumLatency[i] = latency * LATENCY_FACTOR;

    if (!nameOnly) {
        Log("%s latency changed to %d second%s", guardianChargeName[i], latency,
            SIfNotOne(latency));
    }
}

static void ReadConfigCookie(char *arg) {
    char *name = NULL;
    char *val = NULL;
    char *domain = NULL;
    char *path = "/";
    char *equal;
    char *token;
    char *str;
    char *argCopy = NULL;
    struct COOKIE *kn;

    if (!(argCopy = strdup(arg)))
        PANIC;

    for (str = NULL, token = StrTok_R(arg, ";", &str); token; token = StrTok_R(NULL, ";", &str)) {
        Trim(token, TRIM_LEAD | TRIM_TRAIL);
        if (name == NULL) {
            name = token;
            continue;
        }
        equal = strchr(token, '=');
        if (equal == NULL) {
            continue;
        }
        *equal++ = 0;
        if (name == NULL) {
            name = token;
            val = equal;
            continue;
        }

        if (stricmp(token, "domain") == 0)
            domain = equal;

        if (stricmp(token, "path") == 0)
            path = equal;
    }

    if (name == NULL || domain == NULL) {
        Log("Invalid Cookie line in " EZPROXYCFG ": %s", arg);
    } else {
        if (!(kn = calloc(1, sizeof(*kn))))
            PANIC;

        if (!(kn->name = strdup(name)))
            PANIC;
        if (!(kn->domain = strdup(domain)))
            PANIC;
        if (!(kn->path = strdup(path)))
            PANIC;
        kn->gm = GroupMaskCopy(NULL, gm);
        kn->next = defaultCookies;
        defaultCookies = kn;
    }

    FreeThenNull(argCopy);
}

static void ReadConfigEBLSecret(char *arg) {
    struct DATABASE *b;

    b = ReadConfigCurrentDatabase();

    ReadConfigSetString(&b->eblSecret, arg);
}

static void ReadConfigEbrarySite(char *arg) {
    char *arg2;
    char *url = NULL;
    char *host = NULL;
    PORT port;
    char useSsl;
    struct EBRARYSITE *ebs, *last;
    BOOL redirectHttp = 0;

    for (;;) {
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (*arg != '-')
            break;
        if (stricmp(arg, "-redirectHTTP") == 0) {
            redirectHttp = 1;
        } else if (strnicmp(arg, "-URL=", 5) == 0)
            url = arg + 5;
        else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized ebrarySite option: %s", arg);
        }
        arg = arg2;
    }

    if (*arg == 0) {
        Log("ebrarySite missing site name");
        return;
    }

    if (url == NULL) {
        host = "site.ebrary.com";
        port = 80;
        useSsl = 0;
    } else {
        struct parsed_url *purl = parse_url(url);

        if (purl->host == NULL) {
            Log("ebrarySite -URL missing hostname");
            parsed_url_free(purl);
            return;
        }

        if (purl->scheme == NULL || strnicmp("http", purl->scheme, 4) != 0) {
            Log("ebrarySite -URL must start with http:// or https:// %s", url);
            parsed_url_free(purl);
            return;
        }

        // setup parsed values
        useSsl = (stricmp("https", purl->scheme) == 0) ? 1 : 0;
        if (!(host = strdup(purl->host)))
            PANIC;
        port = (purl->port == NULL) ? (useSsl ? 443 : 80) : atoi(purl->port);

        // free allocated memory
        parsed_url_free(purl);
    }

    for (last = NULL, ebs = ebrarySites; ebs; last = ebs, ebs = ebs->next) {
        if (stricmp(ebs->site, arg) == 0) {
            Log("Redefinition of ebrarySite %s ignored", arg);
            return;
        }
    }

    if (!(ebs = calloc(1, sizeof(*ebs))))
        PANIC;
    if (!(ebs->site = strdup(arg)))
        PANIC;
    ebs->host = host;
    ebs->port = port;
    ebs->useSsl = useSsl;
    ebs->gm = GroupMaskCopy(NULL, gm);
    memcpy(&ebs->ipInterface, &ipInterface, sizeof(ebs->ipInterface));
    ebs->proxyHost = proxyHost;
    ebs->proxyPort = proxyPort;
    ebs->proxyAuth = proxyAuth;
    ebs->proxySslHost = proxySslHost;
    ebs->proxySslPort = proxySslPort;
    ebs->proxySslAuth = proxySslAuth;
    ebs->redirectHttp = redirectHttp;

    ebs->next = NULL;

    if (last)
        last->next = ebs;
    else
        ebrarySites = ebs;

    usageLogUser |= ULUEBRARY;
}

static void ReadConfigCookieFilter(char *arg,
                                   BOOL nameOnly,
                                   enum COOKIEFILTER_PROCESSING processingDefault) {
    enum COOKIEFILTER_PROCESSING processing = processingDefault;
    BOOL global = TRUE;

    if (nameOnly) {
        return;
    }

    for (;;) {
        if (*arg != '-') {
            break;
        }
        char *arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (stricmp(arg, "-block") == 0) {
            processing = CFPBLOCK;
        } else if (stricmp(arg, "-process") == 0) {
            processing = CFPPROCESS;
        } else if (stricmp(arg, "-global") == 0) {
            global = TRUE;
        } else if (stricmp(arg, "-local") == 0) {
            global = FALSE;
        } else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized CookieFilter option: %s", arg);
        }
        arg = arg2;
    }

    if (*arg == 0) {
        Log("CookieFilter missing cookie");
        return;
    }

    struct COOKIEFILTER **list;
    if (global) {
        list = &globalCookieFilters;
    } else {
        struct DATABASE *b = databases + cDatabases;
        if (b->title == NULL) {
            Log("CookieFilter without -global must be preceded by a Title directive");
            return;
        }
        list = &b->cookieFilters;
    }
    struct COOKIEFILTER *p, *last;
    for (p = *list, last = NULL; p; last = p, p = p->next) {
    }

    struct COOKIEFILTER *n;
    if (!(n = calloc(1, sizeof(*n))))
        PANIC;
    if (!(n->cookieName = strdup(arg)))
        PANIC;
    n->processing = processing;
    if (last) {
        last->next = n;
    } else {
        *list = n;
    }
}

static void ReadConfigCPIPSecret(char *arg) {
    static char error = 0;

    if (cpipSecret != NULL) {
        if (error == 0) {
            Log("CPIPSecret may only appear once in " EZPROXYCFG);
            error = 1;
        }
        return;
    }

    if (*arg == 0) {
        Log("CPIPSecret may not be blank");
        return;
    }

    if (!(cpipSecret = strdup(arg)))
        PANIC;
}

static void ReadConfigDbVar(char *cmd, char *arg) {
    int i;

    if (strlen(cmd) < 5)
        return;

    if (strlen(cmd) == 5) {
        memset(&dbvars, 0, sizeof(dbvars));
        return;
    }

    if (IsDigit(*(cmd + 5))) {
        i = atoi(cmd + 5);
        if (i >= 0 && i < MAXDBVARS) {
            if (arg == NULL || *arg == 0)
                dbvars[i] = NULL;
            else if (!(dbvars[i] = strdup(arg)))
                PANIC;
        }
    }
}

void ReadConfigDebug(char *arg) {
    int x;

    if ((arg == NULL) || (*arg == 0)) {
        return;
    }

    x = atoi(arg);
    if (x > debugLevel) {
        debugLevel = x;
    }
    if (debugLevel) {
        Log("XDEBUG is %d or %08X", debugLevel, debugLevel);
    }
    if (debugLevel >= 32767) {
        optionDprintf = TRUE;
        logMail = TRUE;
        logReferer = TRUE;
        usageLogSession = TRUE;
        optionDispatchLog = TRUE;
        optionLogDNS = TRUE;
        optionLogSockets = 2;
        optionLogSAML = TRUE;
        optionLogXML = TRUE;
        optionLogSPUEdit = TRUE;
        optionStatus = TRUE;
    }
    optionLogThreadStartup = (debugLevel > 15);
    optionSingleThreadedTranslations = (debugLevel & 32768);
}

static void ReadConfigDescription(char *desc) {
    struct DATABASE *b;

    b = ReadConfigCurrentDatabase();

    if (b->desc == NULL) {
        if (!(b->desc = strdup(desc)))
            PANIC;
        return;
    }

    if (!(b->desc = realloc(b->desc, strlen(b->desc) + strlen(desc) + 2)))
        PANIC;

    strcat(b->desc, " ");
    strcat(b->desc, desc);
}

static void ReadConfigDenyIfRequestHeader(char *arg) {
    static struct DENYIFREQUESTHEADER *last = NULL, *n;
    char *fn;

    fn = arg;
    arg = SkipNonWS(fn);
    if (*arg) {
        *arg++ = 0;
        arg = SkipWS(arg);
    }

    if (*arg == 0)
        return;

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;
    n->next = NULL;
    if (!(n->file = strdup(fn)))
        PANIC;
    if (!(n->header = strdup(arg)))
        PANIC;

    if (last == NULL)
        denyIfRequestHeaders = n;
    else
        last->next = n;

    last = n;
}

static void ReadConfigDns(char *arg, BOOL nameOnly, char *fileName, int line) {
    char *str, *token;
    char *listen = NULL;
    char *response = NULL;
    char *range = NULL;
    struct DNSPORT *dp;
    struct DNSADDR *da, *daLast;
    // unsigned long start, stop;
    struct sockaddr_storage start, stop;
    int i;
    struct sockaddr_storage listenAddress, responseAddress;

    if (nameOnly) {
        if (*arg)
            mDnsPorts++;
        return;
    }

    for (str = NULL, token = StrTok_R(arg, WS, &str); token; token = StrTok_R(NULL, WS, &str)) {
        if (listen == NULL) {
            listen = token;
            continue;
        }
        if (response == NULL) {
            response = token;
            continue;
        }
        if (range == NULL) {
            range = token;
            break;
        }
    }

    if (listen == NULL)
        return;

    if (response == NULL)
        response = listen;

    if (UUGetHostByName(listen, &listenAddress, 0, 0) != 0) {
        return;
    }

    if (UUGetHostByName(response, &responseAddress, 0, 0) != 0) {
        return;
    }

    if (range) {
        if (!ParseAddressRangeNew(range, &start, &stop, fileName, line))
            return;
    } else {
        init_storage(&start, responseAddress.ss_family, 0, 0);
        init_storage(&stop, responseAddress.ss_family, 0, 0);

        unsigned long max = 0xffffffff;
        set_ip(&stop, max, max, max, max);
    }

    for (dp = dnsPorts, i = 0; i < cDnsPorts; dp++, i++) {
        if (memcmp(&dp->ipInterface, &listenAddress, sizeof(dp->ipInterface)) == 0)
            break;
    }

    if (i == cDnsPorts) {
        memcpy(&dp->ipInterface, &listenAddress, sizeof(dp->ipInterface));
        dp->port = 53;
        /* what was this supposed to do?
        dp->ipInterface;
        */
        dp->tcpSocket = dp->udpSocket = INVALID_SOCKET;
        cDnsPorts++;
    }

    for (da = dp->da, daLast = NULL; da; daLast = da, da = da->next)
        ;

    if (!(da = calloc(1, sizeof(struct DNSADDR))))
        PANIC;
    if (daLast == NULL)
        dp->da = da;
    else
        daLast->next = da;

    memcpy(&da->addr, &responseAddress, sizeof(da->addr));

    da->start = start;
    da->stop = stop;
}

BOOL BadDomain(char *d) {
    BOOL result = 1;
    char *s = NULL;
    char *p;

    if (d == NULL)
        goto Done;

    if (!(s = strdup(d)))
        PANIC;

    /* Ignore anything after a colon */
    ParseHost(s, &p, NULL, TRUE);
    if (p)
        *p = 0;

    TrimHost(s, "[]", s);

    /* Removing leading *. from test string */
    while (strnicmp(s, "*.", 2) == 0)
        StrCpyOverlap(s, s + 2);

    /* If we have a blank string, that's invalid */
    if (*s == 0)
        goto Done;
    /* we know it is a non-null string by blank string previous test,
       so the *p check and p++ is safe in the this loop
    */
    /* Strings that only contains * and . are invalid */
    for (p = s;;) {
        if (*p != '*' && *p != '.')
            break;
        p++;
        if (*p == 0)
            goto Done;
    }

    /* If there is no domain, that's invalid (e.g. "Domain com" kicked out here) */
    // changed this to allow ipv6 addresses to work, since ipv4 addresses work
    if (strchr(s, '.') == NULL && !NumericHostIp6(s))
        goto Done;

    /* Disallow the UK equivalents of com and edu */
    if (strcmp(s, "co.uk") == 0 || strcmp(s, "ac.uk") == 0)
        goto Done;

    result = 0;

Done:
    FreeThenNull(s);
    return result;
}

static void ReadConfigDomain(BOOL javaScript, char *arg, BOOL hostOnly) {
    struct DATABASE *b;
    struct INDOMAIN *m;
    char *domain = NULL;
    char *fp = NULL;
    PORT port;
    char *colon;
    char *endOfHost;
    char useSsl;
    PORT forceMyPort;
    char gateway = 0;
    char *str;
    char *token;
    char ftpgw;
    char saveEndOfHost = 0;

    for (str = NULL, token = StrTok_R(arg, WS, &str); token; token = StrTok_R(NULL, WS, &str)) {
        if (domain == NULL) {
            domain = token;
            continue;
        }
        if (stricmp(token, "gateway") == 0) {
            gateway = hostOnly;
            continue;
        }
        if (fp == NULL) {
            fp = token;
            continue;
        }
    }

    if (domain == NULL || *domain == 0)
        return;

    forceMyPort = 0;
    if (fp) {
        if (hostOnly)
            forceMyPort = atoi(fp);
    }

    b = ReadConfigCurrentDatabase();

    domain = SkipFTPHTTPslashesAt(domain, &useSsl, &ftpgw);

    ParseHost(domain, &colon, &endOfHost, 1);

    if (endOfHost) {
        saveEndOfHost = *endOfHost;
        *endOfHost = 0;
    }

    if (hostOnly == 0 && optionAllowBadDomains == 0 && BadDomain(domain)) {
        if (endOfHost)
            *endOfHost = saveEndOfHost;
        Log("Invalid Domain ignored: %s", domain);
        anyInvalidDomains = 1;
        return;
    }

    port = 0;

    // colon is only non-null if a port was found
    if (colon) {
        *colon++ = 0;
        port = atoi(colon);
    }
    if (port == 0) {
        if (hostOnly)
            port = (ftpgw ? 21 : (useSsl ? 443 : 80));
        else
            port = 0;
    }

    /* Check to see if this new entry duplicates an existing entry, and if so, just update the
       existing entry as appropriate
    */

    for (m = b->dbdomains; m; m = m->next) {
        if (m->hostOnly == hostOnly && stricmp(m->domain, domain) == 0 && m->port == port &&
            m->useSsl == useSsl && m->ftpgw == ftpgw) {
            if (validate != NULL && m->validate == NULL) {
                m->validate = validate;
                /* validate is known to be non-null if we are here, so this is safe */
                validate->referenced = 1;
            }
            if (javaScript) {
                m->javaScript = 1;
                b->anyJavaScript = 1;
            }
            if (forceMyPort) {
                if (m->forceMyPort != 0 && m->forceMyPort != forceMyPort) {
                    Log("%s:%d will use port %d, ignoring subsequent request for use of port %d",
                        m->domain, m->port, m->forceMyPort, forceMyPort);
                    return;
                }
                m->forceMyPort = forceMyPort;
            }
            if (gateway)
                m->gateway = 1;
            return;
        }
    }

    if (!(m = calloc(1, sizeof(struct INDOMAIN))))
        PANIC;
    if (!(m->domain = strdup(domain)))
        PANIC;
    m->validate = validate;
    if (validate)
        validate->referenced = 1;
    m->len = strlen(m->domain);
    m->port = port;
    m->hostOnly = hostOnly;
    m->wildcard = strchr(domain, '*') != NULL && hostOnly == 0;
    if (m->javaScript = javaScript) {
        b->anyJavaScript = 1;
    }
    m->forceMyPort = forceMyPort;
    m->gateway = gateway;
    m->useSsl = useSsl;
    m->ftpgw = ftpgw;
    m->next = b->dbdomains;
    b->dbdomains = m;
}

static void ReadConfigEncryptVar(char *arg) {
    struct DATABASE *b;
    char *var, *key;
    struct ENCRYPTVAR *p, *n, *last;

    b = ReadConfigCurrentDatabase();

    var = arg;

    key = SkipNonWS(arg);
    if (*key) {
        *key++ = 0;
        key = SkipWS(key);
    }

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;
    if (!(n->var = strdup(var)))
        PANIC;
    AllLower(n->var);

    n->keyLen = EVP_BytesToKey(EVP_des_ede3_cbc(), EVP_md5(), NULL, (unsigned char *)key,
                               strlen(key), 2, n->key, n->ivec);

    for (last = NULL, p = b->ev; p; last = p, p = p->next) {
    }

    if (last == NULL)
        b->ev = n;
    else
        last->next = n;

    anyEncryptVar = 1;

    ReadConfigAllowVars(arg);
}

unsigned long ReadConfigFindReplaceStatesMask(struct DATABASE *b, char *states) {
    char *str, *token;
    struct PATTERNSTATE *ps, *last;
    unsigned long mask = 0;

    if (stricmp(states, "all") == 0) {
        /* mask is preset to 0, so we return inverse of the unsigned long */
        return ~mask;
    }

    for (str = NULL, token = StrTok_R(states, "+", &str); token;
         token = StrTok_R(NULL, "+", &str)) {
        for (last = NULL, ps = b->patterns.patternState;; last = ps, ps = ps->next) {
            if (ps == NULL) {
                if (last && (last->mask << 1) == 0) {
                    Log("Find/Replace is limited to %d states; %s ignored", last->idx, token);
                    break;
                }
                if (!(ps = calloc(1, sizeof(*ps))))
                    PANIC;
                if (!(ps->state = strdup(token)))
                    PANIC;
                if (last) {
                    ps->mask = last->mask << 1;
                    ps->idx = last->idx + 1;
                    last->next = ps;
                } else {
                    ps->mask = 1;
                    ps->idx = 0;
                    b->patterns.patternState = ps;
                }
            }
            if (stricmp(ps->state, token) == 0) {
                mask |= ps->mask;
                break;
            }
        }
    }
    return mask;
}

static void ReadConfigFind(char *arg) {
    struct DATABASE *b;
    char *state;
    unsigned long mask = 0;

    b = ReadConfigCurrentDatabase();

    if (b->patterns.patternState == NULL) {
        ReadConfigFindReplaceStatesMask(b, "start");
    }

    if (state = StartsIWith(arg, "-state=")) {
        arg = SkipNonWS(state);
        *arg++ = 0;
        arg = SkipWS(arg);
        mask = ReadConfigFindReplaceStatesMask(b, state);
    }

    if (nextPattern) {
        Log(EZPROXYCFG " contains two FINDs without an intervening REPLACE: %s", arg);
        FreeThenNull(nextPattern->matchString);
    } else {
        if (!(nextPattern = calloc(1, sizeof(*nextPattern))))
            PANIC;
    }

    if (!(nextPattern->matchString = strdup(arg)))
        PANIC;
    nextPattern->currentStates = mask;
}

static void ReadConfigExtraLoginCookie(char *arg) {
    static struct EXTRALOGINCOOKIE *last = NULL, *n;

    if (arg == NULL || *arg == 0)
        return;

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;
    n->next = NULL;
    if (!(n->cookie = strdup(arg)))
        PANIC;

    if (last == NULL)
        extraLoginCookies = n;
    else
        last->next = n;

    last = n;
}

static void ReadConfigFirstPort(char *arg) {
    int val;

    val = atoi(arg);
    if (val > 0) {
        firstPort = val;
        WarnIfLowPort(val, 1);
    }
}

static void ReadConfigFMG(char *arg) {
    char *arg2;
    char *p;
    char *key = NULL;
    char *group = NULL;
    char *userClass = "user";
    struct DATABASE *b;
    struct FMGCONFIG *last, *n;
    BOOL anonymous = TRUE;

    for (;;) {
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (*arg != '-')
            break;
        if (p = StartsIWith(arg, "-Group="))
            group = p;
        else if (p = StartsIWith(arg, "-Key="))
            key = p;
        else if (p = StartsIWith(arg, "-Class="))
            userClass = p;
        else if (stricmp(arg, "-UserToken") == 0)
            anonymous = FALSE;
        else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized FMG option: %s", arg);
        }
        arg = arg2;
    }

    if (*arg == 0) {
        Log("FMG missing institution");
        return;
    }

    if (key == NULL) {
        Log("FMG missing -Key: %s", arg);
        return;
    }

    b = ReadConfigCurrentDatabase();

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;

    n->inst = StaticStringCopy(arg);
    n->key = StaticStringCopy(key);
    n->userClass = StaticStringCopy(userClass);
    n->anonymous = anonymous;

    if (group)
        n->gm = MakeGroupMask(NULL, group, 1, '=');
    else
        n->gm = GroupMaskCopy(NULL, gm);
    GroupMaskMinus(n->gm, gmAdminAny);

    if (b->fmgConfigs == NULL) {
        b->fmgConfigs = n;
        GroupMaskCopy(b->gm, n->gm);
    } else {
        struct FMGCONFIG *p;

        GroupMaskOr(b->gm, n->gm);
        for (last = NULL, p = b->fmgConfigs; p; last = p, p = p->next)
            ;
        last->next = n;
    }

    /* If URL referring authentication is on, turn it off */
    b->referer = NULL;

    usageLogUser |= ULUFMG;
}

static void ReadConfigFormSelect(char *arg) {
    struct DATABASE *b = ReadConfigCurrentDatabase();

    ReadConfigSetString(&b->formSelect, arg);
}

static void ReadConfigFormSubmit(char *arg) {
    struct DATABASE *b;

    b = ReadConfigCurrentDatabase();

    if (b->formSubmit == NULL) {
        if (!(b->formSubmit = strdup(arg)))
            PANIC;
        return;
    }

    if (!(b->formSubmit = realloc(b->formSubmit, strlen(b->formSubmit) + strlen(arg) + 2)))
        PANIC;

    strcat(b->formSubmit, " ");
    strcat(b->formSubmit, arg);
}

/**
 * AddHeader <name> <expression>
 * name: The header name, no spaces
 * expression: The expression, can contain spaces
 */
static void ReadConfigAddHeader(char *arg) {
    struct DATABASE *b = ReadConfigCurrentDatabase();
    struct ADDHEADER *ptr;
    char *argPtr = arg;
    char *name = arg;
    char *expression;

    if (!b) {
        Log("Failed to lookup current database configuration, skipping AddHeader '%s'", arg);
        return;
    }

    // set the header name and value
    name = SkipWS(name);
    argPtr = SkipNonWS(name);
    *argPtr = 0;
    expression = SkipWS(++argPtr);

    // these are special headers that we don't want to allow people to override
    if (stricmp("content-length", name) == 0 || stricmp("cookie", name) == 0 ||
        stricmp("te", name) == 0 || stricmp("connection", name) == 0 ||
        stricmp("authorization", name) == 0) {
        Log("Protected header, cannot call AddHeader '%s'. Skipping...", name);
        return;
    }

    // initialize the database->addHeader
    if (!b->addHeader) {
        if (!(b->addHeader = calloc(1, sizeof(struct ADDHEADER))))
            PANIC;
        ptr = b->addHeader;

        // find last addHeader and initialize
    } else {
        // move to last addHeader
        for (ptr = b->addHeader; ptr->next; ptr = ptr->next)
            ;

        // allocate for structure
        if (!(ptr->next = calloc(1, sizeof(struct ADDHEADER))))
            PANIC;
        ptr = ptr->next;
    }

    // copy the strings
    if (!(ptr->name = strdup(name)))
        PANIC;
    if (!(ptr->expression = strdup(expression)))
        PANIC;

    ptr->len = strlen(name);
}

static void ReadConfigFormVariable(char *arg) {
    struct FORMVARS *p, *l, *n;
    struct DATABASE *b;
    char *equal;
    char *arg2;
    BOOL re = 0;
    BOOL suppress = 0;
    BOOL expr = 0;
    xmlRegexpPtr varRE = NULL;

    b = ReadConfigCurrentDatabase();

    if (b->formMethod == NULL) {
        Log("FormVariable only valid when used with URL -form");
        return;
    }

    for (;;) {
        if (arg2 = StartsIWith(arg, "-RE ")) {
            re = 1;
        } else if (arg2 = StartsIWith(arg, "-Suppress ")) {
            suppress = 1;
        } else if (arg2 = StartsIWith(arg, "-expr ")) {
            expr = 1;
        } else {
            if (arg2 = StartsIWith(arg, "-- "))
                arg = SkipWS(arg2);
            break;
        }
        arg = SkipWS(arg2);
    }

    for (p = b->formVars, l = NULL; p; l = p, p = p->next)
        ;

    equal = strchr(arg, '=');
    if (equal) {
        *equal++ = 0;
    }

    if (re) {
        varRE = xmlRegexpCompile(BAD_CAST arg);
        if (varRE == NULL) {
            Log("Invalid regular expression for tag in FormVariable -RE: %s", arg);
            return;
        }
    }

    if (!(n = (struct FORMVARS *)calloc(1, sizeof(*n))))
        PANIC;

    n->next = NULL;
    if (!(n->var = strdup(arg)))
        PANIC;
    /* If there is an equal sign, we set a fixed value, but if no equal sign, implies that
       pass-through of existing value should be used
    */
    if (equal) {
        if (!(n->val = strdup(equal)))
            PANIC;
        if (StrIStr(equal, "^u"))
            usageLogUser |= ULUFORMVARS;
    }
    if (stricmp(arg, "submit") == 0) {
        Log("WARNING: FormVariable submit in %s prevents automatic access", b->title);
    }
    n->varRE = varRE;
    n->suppress = suppress;
    n->expr = expr;

    if (l == NULL)
        b->formVars = n;
    else
        l->next = n;
}

static void ReadConfigGartner(char *arg, GROUPMASK gm) {
    char *arg2;
    char *p;
    size_t len;
    struct GARTNERCONFIG *gc, *last;
    char *url = "http://www.gartner.com/enterprise_access/common/eacportal.jsp";
    char *secret = NULL;
    BOOL jdk = 0;

    for (;;) {
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (*arg != '-')
            break;
        if (p = StartsIWith(arg, "-URL="))
            url = p;
        else if (p = StartsIWith(arg, "-Secret="))
            secret = p;
        else if (stricmp(p, "-JDK") == 0)
            jdk = 1;
        else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized Gartner option: %s", arg);
        }
        arg = arg2;
    }

    Trim(arg, TRIM_COLLAPSE);

    if (*arg == 0) {
        Log("Gartner missing short name");
        return;
    }

    len = strlen(arg);

    if (len > 32)
        *(arg + 32) = 0;

    if (!IsAllAlphameric(arg)) {
        Log("Gartner short name may only contains letters and digits: %s", arg);
        return;
    }

    for (last = NULL, gc = gartnerConfigs; gc; last = gc, gc = gc->next) {
        if (stricmp(gc->shortName, arg) == 0) {
            Log("Redefinition of Gartner short name %s ignored", arg);
            return;
        }
    }

    if (jdk == 0 && secret == NULL) {
        char fn[MAX_PATH];
        sprintf(fn, "%s%s.pem", GARTNERDIR DIRSEP, arg);
        if (!FileExists(fn)) {
            Log("Gartner missing -Secret");
            return;
        }
    }

    if (!(gc = (struct GARTNERCONFIG *)calloc(1, sizeof(*gc))))
        PANIC;
    if (!(gc->shortName = strdup(arg)))
        PANIC;
    if (!(gc->url = strdup(url)))
        PANIC;

    if (secret) {
        if (!(gc->secret = strdup(secret)))
            PANIC;
    } else {
        gartnerKeys = 1;
    }

    gc->gm = GroupMaskCopy(NULL, gm);
    gc->next = NULL;

    if (last) {
        last->next = gc;
        GroupMaskOr(gartnerGm, gm);
    } else {
        gartnerConfigs = gc;
        gartnerGm = GroupMaskCopy(NULL, gm);
    }

    AdminGartnerLoadKey(gc);

    if (gc->dsa == NULL && gc->secret == NULL)
        Log("Gartner %s key not configured; see setup documentation", arg);

    usageLogUser |= ULUGARTNER;
}

static void ReadConfigGroup(char *arg, GROUPMASK gm) {
    char *denyFile;
    struct GROUP *g;

    if (denyFile = StrIStr(arg, " deny=")) {
        *denyFile = 0;
        denyFile += 6;
    }

    gm = MakeGroupMask(gm, arg, 1, '=');
    if (GroupMaskOverlap(gm, gmAdminAny)) {
        Log("Admin groups cannot be assigned in config.txt: %s", arg);
        GroupMaskMinus(gm, gmAdminAny);
    }

    if (denyFile && *denyFile) {
        for (g = groups; g; g = g->next) {
            if (GroupMaskOverlap(g->gm, gm)) {
                if (denyFile && *denyFile) {
                    if (g->denyFile) {
                        FreeThenNull(g->denyFile);
                    }
                    if (!(g->denyFile = malloc(strlen(DOCSDIR) + strlen(denyFile) + 1)))
                        PANIC;
                    sprintf(g->denyFile, "%s%s", DOCSDIR, denyFile);
                    if (!FileExists(g->denyFile) && FileExists(denyFile))
                        strcpy(g->denyFile, denyFile);
                }
            }
        }
    }
}

static void ReadConfigHAName(char *arg) {
    arg = SkipHTTPslashesAt(arg, NULL);
    if (*arg == 0)
        return;
    AllLower(arg);

    ReadConfigSetString(&haName, arg);
}

struct HAPEER *FindHAPeer(char *name, BOOL add) {
    struct HAPEER *p;

    for (p = haPeers; p; p = p->next)
        if (strcmp(p->haPeerName, name) == 0)
            return p;

    if (add == 0)
        return NULL;

    if (!(p = (struct HAPEER *)calloc(1, sizeof(struct HAPEER))))
        PANIC;
    if (!(p->haPeerName = strdup(name)))
        PANIC;
    UUInitMutex(&p->mutex, name);
    p->next = haPeers;
    haPeers = p;
    return p;
}

static void ReadConfigHAPeer(char *arg) {
    char *host;
    char *endOfHost;
    char useSsl;
    char *colon;
    struct HAPEER *hap;
    char *arg2;
    PORT port;

    host = SkipHTTPslashesAt(arg, &useSsl);
    if (host == arg) {
        Log("HAPeer must start with http:// or https://: %s", arg);
        return;
    }

    arg2 = SkipNonWS(arg);
    if (*arg2) {
        *arg2++ = 0;
        arg2 = SkipWS(arg2);
    }

    ParseHost(host, &colon, &endOfHost, 0);
    if (endOfHost)
        *endOfHost = 0;

    AllLower(arg);

    hap = FindHAPeer(arg, 1);
    if (colon)
        *colon = 0;
    if (hap->host == NULL) {
        char *ptr;
        if (!(ptr = strdup(host)))
            PANIC;

        TrimHost(ptr, "[]", ptr);

        hap->host = ptr;
        hap->useSsl = useSsl;

        if (colon) {
            *colon = ':';
            hap->port = atoi(colon + 1);
        }
        if (hap->port == 0)
            hap->port = useSsl ? 443 : 80;
    }

    if (arg2) {
        ParseHost(arg2, &colon, NULL, FALSE);
        if (colon) {
            *colon++ = 0;
            TrimHost(arg2, "[]", arg2);
            host = arg2;
            port = atoi(colon);
        } else {
            host = hap->host;
            port = atoi(arg2);
        }
        if (port)
            UUGetHostByName(host, &hap->haPeerAddress, port, 0);
    }
}

void ReadConfigHAPeerValidate(void) {
    struct HAPEER *hap, *last;

    if (haPeers == NULL)
        return;

    haPeersValid = 1;

    for (hap = haPeers; hap; hap = hap->next) {
        if (get_port(&hap->haPeerAddress) == 0)
            haPeersValid = 0;
    }

    for (last = NULL, hap = haPeers; hap; last = hap, hap = hap->next) {
        if (stricmp(myUrlHttp, hap->haPeerName) == 0 ||
            (myUrlHttps && stricmp(myUrlHttps, hap->haPeerName) == 0)) {
            if (hap != haPeers) {
                if (last)
                    last->next = hap->next;
                hap->next = haPeers;
                haPeers = hap;
            }
            return;
        }
    }

    haPeersValid = 0;

    Log("No HAPeer directives match %s%s%s", myUrlHttp, (myUrlHttps ? " or " : ""),
        myUrlHttps ? myUrlHttps : "");
}

static void ReadConfigHTTPMethod(char *arg, BOOL admin) {
    struct HTTPMETHOD *p, *last;

    for (p = httpMethods, last = NULL; p; last = p, p = p->next) {
        if (stricmp(arg, p->method) == 0)
            return;
    }

    if (!(p = (struct HTTPMETHOD *)calloc(1, sizeof(*p))))
        PANIC;
    p->next = NULL;
    if (!(p->method = strdup(arg)))
        PANIC;
    AllUpper(p->method);
    p->len = strlen(p->method);
    p->admin = admin;
    if (last)
        last->next = p;
    else
        httpMethods = p;
}

static void ReadConfigHTTPHeader(char *arg, BOOL nameOnly) {
    if (nameOnly) {
        return;
    }

    int argCount = 0;
    char *arg2;
    BOOL ignoreGlobalHttpHeaders = 0;
    BOOL global = 0;
    BOOL server = 0;
    enum HTTPHEADER_DIRECTION direction = HHDNONE;
    ;
    enum HTTPHEADER_PROCESSING processing = HHPNONE;
    for (; *arg == '-'; arg = arg2) {
        arg2 = NextWSArg(arg);
        if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        }
        argCount++;
        if (stricmp(arg, "-ignoreGlobal") == 0) {
            ignoreGlobalHttpHeaders = 1;
        } else if (stricmp(arg, "-global") == 0) {
            global = 1;
        } else if (stricmp(arg, "-server") == 0) {
            server = 1;
            direction = HHDRESPONSE;
            processing = HHINJECT;
        } else if (stricmp(arg, "-request") == 0) {
            direction = HHDREQUEST;
        } else if (stricmp(arg, "-response") == 0) {
            direction = HHDRESPONSE;
        } else if (stricmp(arg, "-process") == 0) {
            processing = HHPPROCESS;
        } else if (stricmp(arg, "-block") == 0) {
            processing = HHPBLOCK;
        } else if (stricmp(arg, "-rewrite") == 0) {
            processing = HHPREWRITE;
        } else if (stricmp(arg, "-unrewrite") == 0) {
            processing = HHPUNREWRITE;
        } else if (stricmp(arg, "-edit") == 0) {
            processing = HHEDIT;
        } else if (stricmp(arg, "-inject") == 0) {
            processing = HHINJECT;
        } else {
            Log("Invalid HTTPHeader argument ignored: %s", arg);
        }
    }

    char *expr = NextWSArg(arg);

    if (ignoreGlobalHttpHeaders && (argCount > 1 || *arg != 0)) {
        Log("HTTPHeader -ignoreGlobal may not be used with any other options");
        return;
    }

    if (server && (argCount > 1)) {
        Log("HTTPHeader -server may not be used with any other options");
    }

    // If no options, default to original behavior
    if (argCount == 0) {
        global = 1;
        direction = HHDREQUEST;
        processing = HHPPROCESS;
        argCount = 2;
    }

    struct DATABASE *b;
    if (global || server) {
        b = NULL;
    } else {
        b = databases + cDatabases;

        if (b->title == NULL) {
            Log("HTTPHeader without -global or -server must be preceded by a Title directive");
            return;
        }
    }

    if (ignoreGlobalHttpHeaders && b) {
        b->ignoreGlobalHttpHeaders = 1;
        return;
    }

    if (direction == HHDNONE) {
        Log("HTTPHeader missing -request or -response: %s", arg);
        return;
    }

    if (processing == HHPNONE) {
        Log("HTTPHeader missing -block, -edit, -inject, -process, -rewrite or -unrewrite: %s", arg);
        return;
    }

    if (processing == HHEDIT || processing == HHINJECT) {
        if (*expr == 0) {
            if (server) {
                Log("HTTPHeader -server %s missing expression after the header", arg);
            } else {
                Log("HTTPHeader %s %s missing expression after the header",
                    HTTPHEADER_PROCESSING_NAMES[processing], arg);
            }
            return;
        }
    } else {
        expr = NULL;
    }

    struct HTTPHEADER **list;
    if (server) {
        list = &serverHttpHeaders;
    } else if (global) {
        list = &globalHttpHeaders;
    } else {
        list = &b->httpHeaders;
    }

    struct HTTPHEADER *p, *last;
    for (p = *list, last = NULL; p; last = p, p = p->next) {
    }

    struct HTTPHEADER *n;
    if (!(n = calloc(1, sizeof(*n))))
        PANIC;
    if (!(n->header = strdup(arg)))
        PANIC;
    n->wild = strchr(n->header, '*') != NULL || strchr(n->header, '?') != NULL;
    n->direction = direction;
    n->processing = processing;
    if (expr) {
        if (!(n->expr = strdup(expr)))
            PANIC;
    }
    if (last) {
        last->next = n;
    } else {
        *list = n;
    }
}

static void ReadConfigIdentifier(char *arg, char *currentFilename, int currentLineNum) {
    SetEUidGid();
    IdentifierConfig(arg, currentFilename, currentLineNum);
    ResetEUidGid();
}

/* Called at startup with NULL, which is ignored for storing default;
   called at end of init with "" which causes INADDR_ANY to store if nothing else comes first
*/
static void ReadConfigInterface(char *arg, BOOL nameOnly) {
    int family = UUDEFAULT_FAMILY;
    struct sockaddr_storage newsock;
    SOCKET s;
    int one = 1;
    static BOOL initDefault = 0;

    if (nameOnly == 2)
        return;

    if (arg == NULL || *arg == 0 || stricmp(arg, "ANY") == 0) {
        init_storage(&ipInterface, family, TRUE, 0);

        goto CheckDefault;
    }

    if (UUGetHostByName(arg, &newsock, 0, 0) != 0) {
        if (nameOnly)
            Log("Unable to lookup IP address of interface %s", arg);
        return;
    }

    s = MoveFileDescriptorHigh(socket(newsock.ss_family, SOCK_STREAM, 0));
    if (s == INVALID_SOCKET) {
        Log("Unable to create socket for interface test");
        PANIC;
    }
    if (debugLevel > 5)
        Log("%" SOCKET_FORMAT " Opened socket, errno=%d", s, (s == INVALID_SOCKET) ? errno : 0);

    setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *)&one, sizeof(one));

    if (bind(s, (struct sockaddr *)&newsock, get_size(&newsock))) {
        closesocket(s);
        if (nameOnly)
            Log("INTERFACE %s is not on this system", arg);
        return;
    }

    closesocket(s);
    memcpy(&ipInterface, &newsock, sizeof(ipInterface));

CheckDefault:
    /* This routine is called at start of config file parsing with arg == NULL, so use
       this to determine when to initialize defaultIpInterface, and to know that
       file parsing is started so we should watch for the first interface statement
       that follows to establish a different default IP interface
    */
    if (arg == NULL || initDefault) {
        memcpy(&defaultIpInterface, &ipInterface, sizeof(defaultIpInterface));
        /* Make initDefault true if we are starting file parsing, which means the next
           interface statement seen should be taken to be the default interface; that
           subsequent statement will cause initDefault to go to false, which prevents
           EZproxy from taking any subsequent interface statement and making it the default
        */
        initDefault = arg == NULL;
    }
}

static void ReadConfigIntruderIPAttempts(char *arg, char *fileName, int line) {
    int maxAttempts = -1;
    char *str, *token;
    struct sockaddr_storage start, stop;
    struct INTRUDERIPATTEMPT *a;
    static struct INTRUDERIPATTEMPT *last = NULL;
    BOOL xForwardedFor = 0;
    int expires = 0;
    int interval = 0;
    int rejectAttempts = 0;
    BOOL haveIpRange = 0;

    init_storage(&start, 0, 0, 0);
    init_storage(&stop, 0, 0, 0);
    set_ip(&start, 0, 0, 0, 0);
    set_ip(&stop, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff);

    for (str = NULL, token = StrTok_R(arg, WS, &str); token; token = StrTok_R(NULL, WS, &str)) {
        if (*token == 0)
            continue;
        if (strnicmp(token, "-ip=", 4) == 0) {
            if (!(ParseAddressRangeNew(token + 4, &start, &stop, fileName, line)))
                return;
            haveIpRange = 1;
            continue;
        }

        if (strnicmp(token, "-reject=", 8) == 0) {
            rejectAttempts = atoi(token + 8);
            continue;
        }

        if (stricmp(token, "-x-forwarded-for") == 0) {
            xForwardedFor = 1;
            continue;
        }

        if (strnicmp(token, "-expires=", 9) == 0) {
            expires = atoi(token + 9);
            continue;
        }

        if (strnicmp(token, "-interval=", 10) == 0) {
            interval = atoi(token + 10);
            continue;
        }

        maxAttempts = atoi(token);
    }

    if (maxAttempts == -1)
        return;

    if (intruderReject < rejectAttempts)
        intruderReject = rejectAttempts;

    if (!(a = calloc(1, sizeof(*a))))
        PANIC;
    a->next = NULL;

    a->haveIpRange = haveIpRange;
    memcpy(&a->start, &start, sizeof start);
    memcpy(&a->stop, &stop, sizeof stop);

    a->maxAttempts = maxAttempts;
    if (a->rejectAttempts = rejectAttempts)
        anyIntruderIPRejects = 1;

    a->intruderInterval = a->intruderExpires = intruderTimeout;

    if (expires && interval == 0)
        interval = expires;

    if (interval && expires == 0)
        expires = interval;

    if (interval) {
        a->intruderInterval = interval * 60;
        intruderAttemptsClassic = 0;
    }

    if (expires) {
        a->intruderExpires = expires * 60;
        intruderAttemptsClassic = 0;
    }

    a->xForwardedFor = xForwardedFor;

    if (last == NULL)
        intruderIPAttempts = a;
    else
        last->next = a;

    last = a;
}

static void ReadConfigIntruderLog(char *arg) {
    intruderLog = atoi(arg);
}

static void ReadConfigIntruderReject(char *arg) {
    intruderReject = atoi(arg);
}

static void ReadConfigIntruderTimeout(char *arg) {
    if (atoi(arg) > 0)
        intruderTimeout = atoi(arg);
}

static void ReadConfigIntruderUserAttempts(char *arg, char *file, int lineNumber) {
    char *arg2;
    int interval = 0;
    int expires = 0;
    static BOOL first = 1;

    if (*arg == 0)
        return;

    for (;;) {
        if (*arg != '-')
            break;
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (strnicmp(arg, "-interval=", 10) == 0) {
            interval = atoi(arg + 10);
        } else if (strnicmp(arg, "-expires=", 9) == 0) {
            expires = atoi(arg + 9);
        } else if (strcmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized IntruderUserAttempts option %s in %s(%d)", arg, file, lineNumber);
        }
        arg = arg2;
    }

    if (atoi(arg) <= 0) {
        Log("IntruderUserAttempts missing number of attempts in %s(%d)", file, lineNumber);
        return;
    }

    if (intruderUsageLimit = FindUsageLimit("IntruderUserAttempts", 1)) {
        intruderUsageLimit->open = 0;
    }
    usageLogUser |= ULUUSAGELIMIT;

    intruderUsageLimit->maxIntruderUserAttempts = atoi(arg);

    if (interval && expires == 0)
        expires = interval;

    if (expires && interval == 0)
        interval = expires;

    if (first)
        intruderUsageLimit->interval = intruderTimeout;

    if (interval)
        intruderUsageLimit->interval = interval * 60;

    if (first)
        intruderUsageLimit->expires = intruderTimeout;

    if (expires)
        intruderUsageLimit->expires = expires * 60;

    intruderUsageLimit->granularity = intruderUsageLimit->interval / 12;
    if (intruderUsageLimit->granularity < 30)
        intruderUsageLimit->granularity = 30;

    intruderUsageLimit->oldestInterval =
        intruderUsageLimit->interval + intruderUsageLimit->granularity;

    first = 0;
}

static void ReadConfigIntrusionAPI(char *arg, char *file, int lineNumber) {
    char *arg2;
    int interval = 0;
    int expires = 0;
    static BOOL first = 1;

    if (!first) {
        Log("IntrusionAPI may only appear once");
        return;
    }

    for (;;) {
        if (*arg != '-')
            break;
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (stricmp(arg, "-enforce") == 0) {
            intrusionEnforce = 1;
        } else if (strcmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized IntrusionAPI option %s in %s(%d)", arg, file, lineNumber);
        }
        arg = arg2;
    }

    if (stricmp(arg, "Evade") == 0) {
        if (IntrusionReadConfig()) {
            intrusionEvade = 1;
            intrusionProxySslHost = proxySslHost;
            intrusionProxySslPort = proxySslPort;
            intrusionProxySslAuth = proxySslAuth;
        }
    } else if (stricmp(arg, "Reject") == 0) {
        if (IntrusionReadConfig()) {
            intrusionReject = 1;
            intrusionProxySslHost = proxySslHost;
            intrusionProxySslPort = proxySslPort;
            intrusionProxySslAuth = proxySslAuth;
        }
    } else {
        Log("Invalid IntrusionAPI mode: %s", arg);
    }
}

/**
 * Checks if this is a Unique Local Address
 */
BOOL PublicAddress(struct sockaddr_storage *la) {
    if (la->ss_family == AF_INET) {
        unsigned char *ubytes = (unsigned char *)&(((struct sockaddr_in *)la)->sin_addr.s_addr);

        /* 10.0.0.0/8 */
        if (*ubytes == 10)
            return 0;
        /* 127.0.0.0/8 */
        if (*ubytes == 127) {
            return 0;
        }
        /* 169.254.0.0/16 */
        if (*ubytes == 169 && *(ubytes + 1) == 254)
            return 0;
        /* 172.16.0.0/12 */
        if (*ubytes == 172 && (*(ubytes + 1) & 240) == 16)
            return 0;
        /* 192.168.0.0/16 */
        if (*ubytes == 192 && *(ubytes + 1) == 168)
            return 0;
        return 1;

        // ipv6
    } else {
        struct sockaddr_storage upper, lower;
        init_storage(&lower, AF_INET6, TRUE, 0);
        init_storage(&upper, AF_INET6, TRUE, 0);

        // fc00::/7
        // 1111 1100 0000 0000 = 0xfc00
        // 1111 1110 0000 0000 = /7
        //-------------------
        // 1111 110X XXXX XXXX = 0xfdff

        // fc00::/7 mean between fc00:: and fdff::
        set_ip(&upper, 0xfdffffff, 0xffffffff, 0xffffffff, 0xffffffff);
        set_ip(&lower, 0xfc000000, 0, 0, 0);

        if (compare_ip(la, &lower) >= 0 && compare_ip(&upper, la) >= 0) {
            return 0;
        }

        return 1;
    }
}

/**
 * Given arguments that may contain comma separated parts, determine the upper bound of how many
 * parts may be present as the number of commas present plus one. This is used during the nameOnly
 * scan as directives determine their upper bounds for their static array allocations.
 *
 * @param arg Arguments.
 * @return Count of commas plus one.
 */
static int ReadConfigCommaCountPlus1(const char *arg) {
    int count = 1;
    const char *comma;
    for (comma = arg; comma = strchr(comma, ','); comma++) {
        count++;
    }
    return count;
}

static void ReadConfigIpRange(const char *autoUser,
                              char *range,
                              enum IPACCESS ipAccess,
                              const char *fileName,
                              int line) {
    struct IPTYPE *l;
    char qty[12];

    Trim(range, TRIM_COMPRESS);
    if (*range == 0) {
        return;
    }

    if (ipAccess == IPAREJECT) {
        l = rejectIpTypes + cRejectIpTypes;
    } else {
        l = ipTypes + cIpTypes;
    }
    if (!(ParseAddressRangeNew(range, &l->start, &l->stop, fileName, line))) {
        return;
    }

    // need min/max for compare
    struct sockaddr_storage min, max;
    init_storage(&min, (l->start).ss_family, TRUE, 0);
    init_storage(&max, (l->stop).ss_family, TRUE, 0);
    set_ip(&min, 0, 0, 0, 0);
    set_ip(&max, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff);

    // testing that the first octet is in common
    if (PublicAddress(&l->start) || PublicAddress(&l->stop)) {
        unsigned long affected = 0;
        if (compare_ip(&l->start, &min) == 0 && compare_ip(&l->stop, &max) == 0) {
            strcpy(qty, "all");
        } else {
            // puts the last 32 bit range into qty or "overflow" if range is larger
            affected = get_range32(&l->start, &l->stop, qty);
        }
        if (!optionSilenceIPWarnings && (affected == 0 || affected > 65536)) {
            Log("WARNING: address range applies to %s hosts: %s", qty, range);
        }
    }
    l->ipAccess = ipAccess;
    if (ipAccess == IPAAUTO) {
        if (autoUser)
            if (!(l->autoUser = strdup(autoUser)))
                PANIC;
    }
    l->gmAuto = GroupMaskCopy(NULL, gm);
    // RejectIP addresses are stored separately from the AutoLoginIP/ExcludeIP/IncludeIP
    if (ipAccess == IPAREJECT) {
        cRejectIpTypes++;
    } else {
        cIpTypes++;
    }
}

static void ReadConfigIpTypes(char *arg, enum IPACCESS ipAccess, const char *fileName, int line) {
    char qty[12];
    char *autoUser;
    char *str;
    char *range;

    if (autoUser = StartsIWith(arg, "-user=")) {
        arg = SkipNonWS(autoUser);
        *arg++ = 0;
        arg = SkipWS(arg);
    }

    for (str = NULL, range = StrTok_R(arg, ",", &str); range; range = StrTok_R(NULL, ",", &str)) {
        ReadConfigIpRange(autoUser, range, ipAccess, fileName, line);
    }
}

static void ReadConfigWhitelistRange(char *range, const char *fileName, int line) {
    struct IPTYPE *l;
    char qty[12];

    Trim(range, TRIM_COLLAPSE);
    if (*range == 0)
        return;

    l = whitelistIPs + cWhitelistIPs;
    if (!(ParseAddressRangeNew(range, &l->start, &l->stop, fileName, line)))
        return;

    // need min/max for compare
    struct sockaddr_storage min, max;
    init_storage(&min, (l->start).ss_family, TRUE, 0);
    init_storage(&max, (l->stop).ss_family, TRUE, 0);
    set_ip(&min, 0, 0, 0, 0);
    set_ip(&max, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff);

    // testing that the first octet is in common
    if (PublicAddress(&l->start) || PublicAddress(&l->stop)) {
        unsigned long affected = 0;
        if (compare_ip(&l->start, &min) == 0 && compare_ip(&l->stop, &max) == 0) {
            strcpy(qty, "all");
        } else {
            // puts the last 32 bit range into qty or "overflow" if range is larger
            affected = get_range32(&l->start, &l->stop, qty);
        }
        if (!optionSilenceIPWarnings && (affected == 0 || affected > 65536)) {
            Log("WARNING: address range applies to %s hosts: %s", qty, range);
        }
    }
    l->ipAccess = IPAWHITELIST;
    cWhitelistIPs++;
}

static void ReadConfigWhitelistIP(char *arg, const char *fileName, int line) {
    char *str;
    char *range;
    for (str = NULL, range = StrTok_R(arg, ",", &str); range; range = StrTok_R(NULL, ",", &str)) {
        ReadConfigWhitelistRange(range, fileName, line);
    }
}

static void ReadConfigKeepAliveTimeout(char *arg) {
    int i = atoi(arg);

    if (i == 0) {
        Log("Invalid KeepAliveTimeout value: %s", arg);
    } else {
        keepAliveTimeout = i;
    }
}

struct LBPEER *FindLBPeer(char *id, size_t idLen, BOOL add) {
    struct LBPEER *p;

    if (idLen == 0)
        idLen = strlen(id);

    for (p = lbPeers; p; p = p->next)
        if (strnicmp(p->id, id, idLen) == 0 && p->idLen == idLen)
            return p;

    if (!add)
        return NULL;

    if (!(p = (struct LBPEER *)calloc(1, sizeof(struct LBPEER))))
        PANIC;
    if (!(p->id = malloc(idLen + 1)))
        PANIC;
    memcpy(p->id, id, idLen);
    *(p->id + idLen) = 0;
    p->idLen = idLen;
    p->next = lbPeers;
    lbPeers = p;
    return p;
}

static void ReadConfigLBPeer(char *arg) {
    char *arg2;
    char *p;
    char *host = NULL;
    PORT tcpPort = 0;
    PORT udpPort = 0;
    char useSsl = 0;
    struct LBPEER *lbp;
    SOCKET s;
    struct sockaddr_storage ipInterface;
    int one = 1;

    for (;;) {
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (*arg != '-')
            break;
        if (p = StartsIWith(arg, "-host="))
            host = p;
        else if (p = StartsIWith(arg, "-http=")) {
            tcpPort = atoi(p);
            useSsl = 0;
        } else if (p = StartsIWith(arg, "-https=")) {
            tcpPort = atoi(p);
            useSsl = 1;
        } else if (p = StartsIWith(arg, "-udp="))
            udpPort = atoi(p);
        else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized LBPeer option: %s", arg);
        }
        arg = arg2;
    }

    if (host == NULL) {
        Log("LBPeer missing required -Host parameter");
        goto Finished;
    }

    if (tcpPort == 0) {
        Log("LBPeer missing required -http=port or -https=port");
        goto Finished;
    }

    if (!IsAllAlphameric(arg)) {
        Log("LBPeer ID may only be letters and digits: %s", arg);
        goto Finished;
    }

    if (strlen(arg) > MAXLBPEERID) {
        Log("LBPeer %s is too long (maximum of %d character allowed)", arg, MAXLBPEERID);
        goto Finished;
    }

    if (UUGetHostByName(host, &ipInterface, 0, 0)) {
        Log("LBPeer host unknown: %s", host);
        goto Finished;
    }

    lbp = FindLBPeer(arg, 0, TRUE);

    if (lbp->host) {
        Log("Duplicate LBPeer for %s ignored", arg);
        goto Finished;
    }

    s = MoveFileDescriptorHigh(socket(ipInterface.ss_family, SOCK_DGRAM, 0));
    if (debugLevel > 5)
        Log("%" SOCKET_FORMAT " Opened socket, errno=%d", s, (s == INVALID_SOCKET) ? errno : 0);
    if (s == INVALID_SOCKET)
        PANIC;

    setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *)&one, sizeof(one));

    if (bind(s, (struct sockaddr *)&ipInterface, get_size(&ipInterface)) == 0) {
        if (myLbPeer == NULL)
            myLbPeer = lbp;
        else
            Log("IP addresses for LBPeers %s and %s both appear on local system", myLbPeer->id,
                lbp->id);
    }

    if (!(lbp->host = strdup(host)))
        PANIC;
    lbp->tcpPort = tcpPort;
    lbp->udpPort = udpPort;
    lbp->useSsl = useSsl;
    memcpy(&lbp->ipInterface, &ipInterface, sizeof(lbp->ipInterface));

Finished:;
}

static void ReadConfigLocation(char *arg, char *fileName, int line) {
    char *arg2;
    char *p;
    char *country = NULL;
    char *region = NULL;
    char *city = NULL;
    double latitude = 0;
    double longitude = 0;
    struct sockaddr_storage start, stop;
    char *token;
    struct LOCATIONRANGE *n;
    static struct LOCATIONRANGE *last = NULL;

    arg = SkipWS(arg);

    for (;;) {
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (*arg != '-') {
            break;
        }
        if (strcmp(arg, "--") == 0) {
            arg = arg2;
            break;
        }
        if ((p = StartsIWith(arg, "-File="))) {
            LoadLocation(p);
            return;
        } else if ((p = StartsIWith(arg, "-Latitude="))) {
            latitude = atof(p);
        } else if ((p = StartsIWith(arg, "-Longitude="))) {
            longitude = atof(p);
        } else {
            Log("Invalid Location option: %s", arg);
        }
        arg = arg2;
    }

    if (*arg2 == 0) {
        return;
    }

    if (!(ParseAddressRangeNew(arg, &start, &stop, fileName, line))) {
        return;
    }

    for (token = strtok(arg2, "/"); token; token = strtok(NULL, "/")) {
        Trim(token, TRIM_LEAD | TRIM_TRAIL);
        if (country == NULL)
            country = token;
        else if (region == NULL)
            region = token;
        else if (city == NULL)
            city = token;
    }

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;

    memcpy(&n->start, &start, sizeof start);
    memcpy(&n->stop, &stop, sizeof stop);

    n->countryCode = StaticStringCopy(country);
    n->region = StaticStringCopy(region);
    n->city = StaticStringCopy(city);
    n->latitude = latitude;
    n->longitude = longitude;
    if (last)
        last->next = n;
    else
        locationRanges = n;
    last = n;
}

static void ReadConfigLoginCookieName(char *arg) {
    char *p;
    char error = 0;
    static int changedLoginCookieName = 0;

    for (p = strchr(arg, 0) - 1; p >= arg; p--) {
        if (IsDigit(*p) || IsAlpha(*p))
            continue;
        if (error == 0) {
            Log("Only letters and numbers are valid in LoginCookieName; all others ignored");
            error = 1;
        }
        StrCpyOverlap(p, p + 1);
    }
    if (*arg == 0) {
        Log("Invalid LoginCookieName ignored");
        return;
    }

    loginCookieNameLen = strlen(arg);
    if (loginCookieNameLen > 16) {
        loginCookieNameLen = 16;
        *(arg + loginCookieNameLen) = 0;
        Log("LoginCookieName truncated to %s", arg);
    }

    if (changedLoginCookieName == 0)
        changedLoginCookieName = 1;
    else {
        FreeThenNull(loginCookieName);
        FreeThenNull(refererCookieName);
        FreeThenNull(requireAuthenticateCookieName);
    }

    if (!(loginCookieName = strdup(arg)))
        PANIC;

    if (!(refererCookieName = malloc(loginCookieNameLen + 5)))
        PANIC;
    sprintf(refererCookieName, "%sdest", loginCookieName);

    if (!(requireAuthenticateCookieName = malloc(loginCookieNameLen + 21)))
        PANIC;
    sprintf(requireAuthenticateCookieName, "%srequireauthenticate2", loginCookieName);
}

static void ReadConfigLoginCookieSameSite(char *arg) {
    char *p;
    char error = 0;
    static int changedLoginCookieName = 0;
    char *str;
    char *token;
    int newLoginCookieSameSite = 0;

    for (str = NULL, token = StrTok_R(arg, " \t", &str); token;
         token = StrTok_R(NULL, " \t", &str)) {
        if (stricmp(token, "Classic") == 0) {
            newLoginCookieSameSite |= 1 << LOGIN_COOKIE_SAME_SITE_CLASSIC;
        } else if (stricmp(token, "Lax") == 0) {
            newLoginCookieSameSite |= 1 << LOGIN_COOKIE_SAME_SITE_LAX;

        } else if (stricmp(token, "None") == 0) {
            newLoginCookieSameSite |= 1 << LOGIN_COOKIE_SAME_SITE_NONE;
        } else {
            Log("Invalid LoginCookieSameSite option ignored: %s", token);
        }
    }

    if (newLoginCookieSameSite == 0) {
        Log("LoginCookieSameSite requires one or more of Classic Lax None");
        return;
    }

    loginCookieSameSite = newLoginCookieSameSite;
}

static void ReadConfigPrivateComputerCookieName(char *arg) {
    char *p;
    char error = 0;
    static int changedPrivateComputerCookieName = 0;

    for (p = strchr(arg, 0) - 1; p >= arg; p--) {
        if (IsDigit(*p) || IsAlpha(*p))
            continue;
        if (error == 0) {
            Log("Only letters and numbers are valid in PrivateComputerCookieName; all others "
                "ignored");
            error = 1;
        }
        StrCpyOverlap(p, p + 1);
    }
    if (*arg == 0) {
        Log("Invalid PrivateComputerCookieName ignored");
        return;
    }

    privateComputerCookieNameLen = strlen(arg);
    if (privateComputerCookieNameLen > 32) {
        privateComputerCookieNameLen = 32;
        *(arg + privateComputerCookieNameLen) = 0;
        Log("PrivateComputerCookieName truncated to %s", arg);
    }

    if (changedPrivateComputerCookieName == 0)
        changedPrivateComputerCookieName = 1;
    else {
        FreeThenNull(privateComputerCookieName);
    }

    if (!(privateComputerCookieName = strdup(arg)))
        PANIC;
}

static void ReadConfigLoginCookieDomain(char *arg) {
    char *arg2;

    Trim(arg, TRIM_COMPRESS);
    arg2 = SkipNonWS(arg);
    if (*arg2) {
        *arg2++ = 0;
        arg2 = SkipWS(arg2);
    }

    if (stricmp(arg, "Default") == 0) {
        loginCookieDomainType = LOGINCOOKIEDOMAINDEFAULT;
        ReadConfigSetString(&loginCookieDomain, NULL);
        return;
    }

    if (stricmp(arg, "Hostname") == 0) {
        loginCookieDomainType = LOGINCOOKIEDOMAINHOSTNAME;
        ReadConfigSetString(&loginCookieDomain, NULL);
        return;
    }

    if (stricmp(arg, "DomainName") == 0) {
        loginCookieDomainType = LOGINCOOKIEDOMAINDOMAINNAME;
        ReadConfigSetString(&loginCookieDomain, NULL);
        return;
    }

    if (stricmp(arg, "Manual") == 0) {
        if (*arg2 == 0)
            Log("ConfigLoginDomain Manual must include the domain name " EZPROXYNAMEPROPER
                " should use when setting its cookie");
        else {
            loginCookieDomainType = LOGINCOOKIEDOMAINMANUAL;
            ReadConfigSetString(&loginCookieDomain, arg2);
        }
        return;
    }

    if (stricmp(arg, "None") == 0) {
        loginCookieDomainType = LOGINCOOKIEDOMAINNONE;
        ReadConfigSetString(&loginCookieDomain, NULL);
        return;
    }

    Log("Invalid LoginCookieDomain option: %s", arg);
}

static void ReadConfigLoginMenu(char *arg) {
    Trim(arg, TRIM_LEAD | TRIM_TRAIL);
    ReadConfigSetString(&mainMenu, arg);
}

/**
 * Basically the loginPorts points to the beginning of the block of memory
 * storing the loginPorts.  As we call this, we are initializing the loginPorts.
 * This does NOT create the socket however.
 */
struct LOGINPORT *ReadConfigLoginPort(char *arg, BOOL nameOnly, char useSsl) {
    int val;
    struct LOGINPORT *lp = NULL;
    static BOOL anyExplicit = 0;
    BOOL virtualPort = 0;
    BOOL forceHttp = 0;
    char *p;
    char *arg2;
    int acceptSslIdx = 0;
    static BOOL loginportsslError = 0;

    /* This routine called with NULL at end of file parsing to set up a default if no other
     * explicitly defined */
    if (arg == NULL) {
        if (anyExplicit)
            return NULL;
        val = firstPort;
        useSsl = 0;
    } else {
        for (;;) {
            if (*arg != '-')
                break;
            arg2 = SkipNonWS(arg);
            if (*arg2) {
                *arg2++ = 0;
                arg2 = SkipWS(arg2);
            }
            if (p = StartsIWith(arg, "-cert=")) {
                acceptSslIdx = atoi(p);
                if (acceptSslIdx == 0 || UUSslLoadKey(acceptSslIdx, 1, NULL, 1, NULL) == NULL) {
                    Log("LoginPortSSL -Cert=%s ignored since certificate does not exist", p);
                    acceptSslIdx = 0;
                }
            } else if (stricmp(arg, "-virtual") == 0) {
                virtualPort = 1;
            } else if (stricmp(arg, "-http") == 0) {
                if (useSsl) {
                    if (optionForceWildcardCertificate || optionIgnoreWildcardCertificate) {
                        forceHttp = 1;
                    } else {
                        Log("Option ForceWildcardCertificate or Option IgnoreWildcardCertificate "
                            "must appear on a separate line before LoginPortSSL -http");
                        return NULL;
                    }
                }
            } else if (stricmp(arg, "--") == 0) {
                arg = arg2;
                break;
            } else {
                Log("Unrecognized LoginPort%s option: %s", (useSsl ? "SSL" : ""), arg);
            }
            arg = arg2;
        }
        val = atoi(arg);
        if (val == 0)
            return NULL;
        if (useSsl == 0) {
            anyExplicit = 1;
        }

        if (useSsl && !forceHttp) {
            if (!UUSslAvailable()) {
                Log("LoginPortSSL is ignored until you configure SSL");
                loginportsslError = 1;
                return NULL;
            }
        }
    }
    if (primaryLoginPort == 0 && useSsl == 0)
        primaryLoginPort = val;
    if (primaryLoginPortSsl == 0 && useSsl == 1)
        primaryLoginPortSsl = val;
    if (nameOnly)
        mLoginPorts++;
    else {
        lp = loginPorts + cLoginPorts;
        lp->port = val;
        memcpy(&lp->ipInterface, &ipInterface, sizeof(lp->ipInterface));
        lp->useSsl = useSsl;
        lp->forceHttp = forceHttp;
        lp->virtualPort = virtualPort;
        lp->acceptSslIdx = acceptSslIdx;
        lp->gm = GroupMaskCopy(NULL, gm);
        cLoginPorts++;
        WarnIfLowPort(val, 0);
    }
    return lp;
}

static void ReadConfigLogTag(char *arg) {
    char *arg2;
    xmlRegexpPtr tagRE = NULL;
    xmlRegexpPtr urlRE = NULL;
    BOOL re = 0;
    BOOL cs = 0;
    BOOL approve = 0;
    static struct LOGTAGMAP *last = NULL;
    struct LOGTAGMAP *n;

    for (;;) {
        if (arg2 = StartsIWith(arg, "-RE ")) {
            re = 1;
        } else if (arg2 = StartsIWith(arg, "-CS ")) {
            cs = 1;
        } else if (arg2 = StartsIWith(arg, "-Approve ")) {
            approve = 1;
        } else {
            if (arg2 = StartsIWith(arg, "-- "))
                arg = SkipWS(arg2);
            break;
        }
        arg = SkipWS(arg2);
    }

    if (*arg == 0)
        return;

    if (approve) {
        if (re) {
            tagRE = xmlRegexpCompile(BAD_CAST arg);
            if (tagRE == NULL) {
                Log("Invalid regular expression for tag in LogTag -Approve -RE: %s", arg);
                goto Finished;
            }
        }
    }

    arg2 = SkipNonWS(arg);
    if (*arg2) {
        *arg2++ = 0;
        arg2 = SkipWS(arg2);
    }
    if (!IsValidLogTag(arg)) {
        if (approve == 0 || *arg2) {
            Log("Invalid tag for LogTag: %s", arg);
            goto Finished;
        }
    }

    if (re && *arg2) {
        urlRE = xmlRegexpCompile(BAD_CAST arg2);
        if (urlRE == NULL) {
            Log("Invalid regular expression for URL in LogTag -RE: %s", arg2);
            goto Finished;
        }
    }

    if (!(n = (struct LOGTAGMAP *)calloc(1, sizeof(*n))))
        PANIC;
    if (!(n->logTag = strdup(arg)))
        PANIC;
    if (arg2)
        if (!(n->url = strdup(arg2)))
            PANIC;
    n->tagRE = tagRE;
    tagRE = NULL;
    n->urlRE = urlRE;
    urlRE = NULL;
    n->caseSensitive = cs;
    n->approve = approve;

    if (last == NULL) {
        logTagMaps = n;
    } else {
        last->next = n;
    }

    last = n;

Finished:
    if (tagRE) {
        xmlRegFreeRegexp(tagRE);
        tagRE = NULL;
    }

    if (urlRE) {
        xmlRegFreeRegexp(urlRE);
        urlRE = NULL;
    };
}

static void ReadConfigMetaFind(char *arg, BOOL nameOnly) {
    struct DATABASE *b;

    if (nameOnly)
        return;

    if (stricmp(arg, "MUSECOOKIE") == 0) {
        b = ReadConfigCurrentDatabase();
        b->metaFindMuseCookie = 1;
    } else {
        Log("Unrecognized MetaFind option: %s", arg);
    }
}

static void ReadConfigMinProxy(char *arg, BOOL nameOnly) {
    char *space;

    space = SkipNonWS(arg);
    if (*space) {
        *space++ = 0;
        if (nameOnly == 0) {
            space = SkipWS(space);
            if (*space) {
                FreeThenNull(minProxyAuth);
                minProxyAuth = Encode64(NULL, space);
            }
        }
        memcpy(&minProxyIpInterface, &ipInterface, sizeof(minProxyIpInterface));

    } else {
        if (nameOnly == 0)
            Log("MinProxy missing username:password");
        return;
    }

    ReadConfigLoginPort(arg, nameOnly, USESSLMINPROXY);
}

static void ReadConfigMinProxyDelay(char *arg) {
    minProxyDelay = atoi(arg);
    if (minProxyDelay < 0)
        minProxyDelay = 0;
}

static void ReadConfigMutexTimeout(char *arg) {
    mutexTimeout = atoi(arg);
    if (mutexTimeout < 0)
        mutexTimeout = 0;
#ifdef WIN32
    if (mutexTimeout > 0)
        dMutexTimeout = mutexTimeout * 1000;
    else
        dMutexTimeout = INFINITE;
#endif
}

static void ReadConfigMyiLibrary(char *arg) {
    char *arg2;
    char *p;
    char *key = NULL;
    char *group = NULL;
    struct DATABASE *b;
    struct MYILIBRARYCONFIG *last, *n;
    enum SSOHASH hash = SSOHASHMD5;

    for (;;) {
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (*arg != '-')
            break;
        if (p = StartsIWith(arg, "-Group="))
            group = p;
        else if (p = StartsIWith(arg, "-Key="))
            key = p;
        else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else if (p = StartsIWith(arg, "-Hash=")) {
            if (stricmp(p, "SHA1") == 0)
                hash = SSOHASHSHA1;
            else if (stricmp(p, "MD5") == 0)
                hash = SSOHASHMD5;
            else if (stricmp(p, "SHA256") == 0)
                hash = SSOHASHSHA256;
            else {
                Log("Unrecognized MyiLibrary -Hash: %s", p);
                return;
            }
        } else {
            Log("Unrecognized MyiLibrary option: %s", arg);
        }
        arg = arg2;
    }

    if (*arg == 0) {
        Log("MyiLibrary missing institution");
        return;
    }

    if (key == NULL) {
        Log("MyiLibrary mssing -Key: %s", arg);
        return;
    }

    b = ReadConfigCurrentDatabase();

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;

    n->inst = StaticStringCopy(arg);
    n->key = StaticStringCopy(key);
    n->hash = hash;

    if (group)
        n->gm = MakeGroupMask(NULL, group, 1, '=');
    else
        n->gm = GroupMaskCopy(NULL, gm);
    GroupMaskMinus(n->gm, gmAdminAny);

    if (b->myiLibraryConfigs == NULL) {
        b->myiLibraryConfigs = n;
        GroupMaskCopy(b->gm, n->gm);
    } else {
        struct MYILIBRARYCONFIG *p;

        GroupMaskOr(b->gm, n->gm);
        for (last = NULL, p = b->myiLibraryConfigs; p; last = p, p = p->next)
            ;
        last->next = n;
    }

    /* If URL referring authentication is on, turn it off */
    b->referer = NULL;

    usageLogUser |= ULUMYILIBRARY;
}

static void ReadConfigLogFile(char *arg) {
    if (logSPUs->strftimeFilename = (strnicmp(arg, "-strftime ", 10) == 0)) {
        arg = SkipWS(SkipNonWS(arg));
    }

    if (*arg == 0)
        return;

    ReadConfigSetString(&logSPUs->filename, arg);
    logSPUs->activeFilename[0] = 0;
}

static void ReadConfigLogFilter(char *arg) {
    if (*arg)
        if (!(logFilters[cLogFilters++].filter = strdup(arg)))
            PANIC;
}

static void ReadConfigLogFormat(char *arg) {
    /* Can't use ReadConfigSetString here as a pointer to a 0-length string has special meaning */
    Trim(arg, TRIM_LEAD | TRIM_TRAIL);
    if (logSPUs->format) {
        if (strcmp(logSPUs->format, arg) == 0)
            return;
        FreeThenNull(logSPUs->format);
    }
    if (!(logSPUs->format = strdup(arg)))
        PANIC;
}

static void ReadConfigLogSPU(char *arg) {
    struct LOGSPU *p, *last, *n;
    BOOL strftimeFilename;

    char *arg2;

    if (*arg == 0)
        return;

    if (strftimeFilename = (strnicmp(arg, "-strftime ", 10) == 0)) {
        arg = SkipWS(SkipNonWS(arg));
    }

    arg2 = SkipNonWS(arg);
    if (*arg2) {
        *arg2++ = 0;
        arg2 = SkipWS(arg2);
    }

    for (p = logSPUs, last = NULL; p; p = p->next) {
        last = p;
    }

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;

    n->file = NULL;
    n->filename = strdup(arg);
    n->format = strdup(*arg2 ? arg2 : "%h %l %u %t \"%r\" %s %b");
    n->strftimeFilename = strftimeFilename;
    n->next = NULL;
    if (last == NULL)
        logSPUs = n;
    else
        last->next = n;
}

static void ReadConfigMailFrom(char *arg) {
    ReadConfigSetString(&mailFrom, arg);
}

static void ReadConfigMailServer(char *arg) {
    ReadConfigSetString(&mailServer, arg);
}

static void ReadConfigMailTo(char *arg) {
    ReadConfigSetString(&mailTo, arg);
}

int ReadConfigMax(char *cmd, size_t cmdLen, char *arg, BOOL nameOnly) {
    int val;

    if (cmdLen == 2) {
        if (*cmd != 'M' && *cmd != 'm')
            return 0;
        cmd++;
        cmdLen--;
    } else {
        if (cmdLen < 4 || strnicmp(cmd, "MAX", 3) != 0)
            return 0;
        cmd += 3;
        cmdLen -= 3;
    }

    val = atoi(arg);
    if (val <= 0) {
        if (nameOnly)
            Log("Invalid " EZPROXYCFG " value: %s", cmd);
        return 1;
    }

    if (MatchCommand(cmd, cmdLen, "KeepAliveRequests", 0)) {
        maxKeepAliveRequests = val;
        return 1;
    }

    if (MatchCommand(cmd, cmdLen, "LIFETIME", 1)) {
        mSessionLife = val * 60;
        return 1;
    }

    if (nameOnly == 0)
        return 1;

    if (MatchCommand(cmd, cmdLen, "VIRTUALHOSTS", 1)) {
        mHosts = val;
        return 1;
    }

    if (MatchCommand(cmd, cmdLen, "CONCURRENTTRANSFERS", 1)) {
        mTranslates = val;
        return 1;
    }

    if (MatchCommand(cmd, cmdLen, "SESSIONS", 1)) {
        mSessions = val;
        return 1;
    }

    /*
    if (MatchCommand(cmd, cmdLen, "DATABASES")) {
        mDatabases = val;
        return 1;
    }
    */

    return 2;

    /*
    case 'I':
        mIpTypes = val;
        break;
    */
}

static void ReadConfigName(char *arg, BOOL nameOnly) {
    char *colon;

    if (nameOnly && arg && *arg && *arg != ':') {
        StrCpy3(myName, arg, sizeof(myName));
        if (colon = strchr(myName, ':')) {
            *colon = 0;
            Log("In your NAME entry in " EZPROXYCFG ", everything after the : has been ignored.");
        }
        AllLower(myName);
        myNameLen = strlen(myName);
    }
}

static void ReadConfigNetLibrary(char *arg) {
    char *arg2;
    char *p;
    char *str, *token;
    char *hashEnd;
    char *dataBegin;
    size_t hashLen;
    char *confirmLibraryId = NULL;
    char *signatureKey = NULL;
    char *privateKey = NULL;
    char *libraryId = NULL;
    char *partnerId = NULL;
    char *group = NULL;
    char *wideNetLibrarySignatureKeyEol;
    struct DATABASE *b;
    struct NETLIBRARYCONFIG *last, *n;
    BOOL noUserToken = 0;
    char fn[MAX_PATH];
    int f = -1;
    char *base64Text = NULL;
    size_t base64Size;
    unsigned char *crypted = NULL;
    size_t cryptedLen;
    EVP_MD_CTX *mdctx = NULL;
    unsigned int mdLen;
    unsigned char sha256[EVP_MAX_MD_SIZE];
    char sha256Hex[EVP_MAX_MD_SIZE * 2 + 1];
    EVP_CIPHER_CTX *ctx = NULL;
    char *plainText = NULL;
    int updateLen, finalLen, totalLen;
    int i;
    struct stat statBuf;
    char key[17], *pk;
    /* missingId is also used as the initial vector, so don't change this text,
       or at least don't change the first 16 characters
    */
    char *missingId = "NetLibrary missing library ID";

    for (;;) {
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (*arg != '-')
            break;
        if (p = StartsIWith(arg, "-Group"))
            group = p;
        else if (stricmp(arg, "-NoUserToken") == 0)
            noUserToken = 1;
        else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized NetLibrary option: %s", arg);
        }
        arg = arg2;
    }

    if (*arg == 0) {
        Log(missingId);
        goto Finished;
    }

    libraryId = arg;

    if (strlen(arg) > 32 || !IsAllAlphameric(arg)) {
        Log("NetLibrary invalid library ID: %s", arg);
        goto Finished;
    }

    for (i = 0, pk = key, p = arg; i < 16; i++) {
        if (*p == 0)
            p = arg;
        *pk++ = *p++;
    }
    *pk = 0;

    sprintf(fn, "netlibrary%s%s.txt", DIRSEP, arg);
    /* This is at startup time; it's OK to use fopen */
    f = SOPEN(fn);
    if (f < 0) {
        if (errno == EACCES) {
            Log("NetLibrary file %s exists has the wrong file permissions", fn);
            goto Finished;
        }
        if (errno == ENOENT) {
            Log("NetLibrary %s requires file %s to work; contact " EZPROXYHELPEMAIL, arg, fn);
            goto Finished;
        }
        Log("NetLibrary unable to open file %s: %d", fn, errno);
        goto Finished;
    }

    if (fstat(f, &statBuf) != 0 || statBuf.st_size <= 0)
        goto BadFile;

    if (!(base64Text = malloc((size_t)statBuf.st_size + 1)))
        PANIC;

    base64Size = read(f, base64Text, (size_t)statBuf.st_size);
    if (base64Size <= 0)
        goto BadFile;

    *(base64Text + base64Size) = 0;

    crypted = Decode64Binary(NULL, &cryptedLen, base64Text);
    if (cryptedLen == 0)
        goto BadFile;

    if (!(ctx = EVP_CIPHER_CTX_new()))
        PANIC;
    EVP_CIPHER_CTX_init(ctx);
    EVP_DecryptInit(ctx, EVP_aes_128_cbc(), (unsigned char *)key, (unsigned char *)missingId);

    if (!(plainText = malloc(cryptedLen)))
        PANIC;

    updateLen = finalLen = 0;
    if (!EVP_DecryptUpdate(ctx, (unsigned char *)plainText, &updateLen, crypted, cryptedLen))
        goto BadFile;

    if (!EVP_DecryptFinal(ctx, (unsigned char *)plainText + updateLen, &finalLen))
        goto BadFile;

    EVP_CIPHER_CTX_cleanup(ctx);

    totalLen = updateLen + finalLen;

    if (totalLen <= 1 || plainText[totalLen - 1] != 0)
        goto BadFile;

    for (p = plainText + totalLen - 2; p >= plainText; p--) {
        if (*p == 13 || *p == 10)
            continue;
        if (*p < ' ' || *p > '~')
            goto BadFile;
    }

    hashEnd = SkipNonWS(plainText);
    hashLen = hashEnd - plainText;
    dataBegin = SkipWS(hashEnd);
    if (hashLen != 64 || *dataBegin == 0)
        goto BadFile;

    if (!(mdctx = EVP_MD_CTX_new()))
        PANIC;
    EVP_DigestInit(mdctx, EVP_sha256());
    /* Digest up to but not including the &sig= */
    EVP_DigestUpdate(mdctx, dataBegin, strlen(dataBegin));
    EVP_DigestFinal(mdctx, sha256, &mdLen);
    DigestToHex(sha256Hex, sha256, mdLen, 0);
    if (strnicmp(sha256Hex, plainText, hashLen) != 0)
        goto BadFile;

    for (str = NULL, token = StrTok_R(dataBegin, "\r\n", &str); token;
         token = StrTok_R(NULL, "\r\n", &str)) {
        if (*token == 0)
            continue;
        if (confirmLibraryId == NULL) {
            confirmLibraryId = token;
            continue;
        }
        if (signatureKey == NULL) {
            signatureKey = token;
            continue;
        }
        if (privateKey == NULL) {
            privateKey = token;
            continue;
        }
        if (partnerId == NULL) {
            partnerId = token;
            continue;
        }
    }

    if (partnerId == NULL || strcmp(libraryId, confirmLibraryId) != 0)
        goto BadFile;

    b = ReadConfigCurrentDatabase();

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;

    if (!(n->wideNetLibrarySignatureKey = malloc(strlen(signatureKey) * 2 + 2)))
        PANIC;
    wideNetLibrarySignatureKeyEol = UUStrCpyToWide(n->wideNetLibrarySignatureKey, signatureKey);
    n->wideNetLibrarySignatureKeyLen =
        wideNetLibrarySignatureKeyEol - n->wideNetLibrarySignatureKey;

    ReadConfigSetString(&n->netLibraryPrivateKey, privateKey);
    ReadConfigSetString(&n->netLibraryLibId, libraryId);
    ReadConfigSetString(&n->netLibraryPartnerId, partnerId);
    n->noUserToken = noUserToken;

    if (group)
        n->gm = MakeGroupMask(NULL, group, 1, '=');
    else
        n->gm = GroupMaskCopy(NULL, gm);
    GroupMaskMinus(n->gm, gmAdminAny);

    if (b->netLibraryConfigs == NULL) {
        b->netLibraryConfigs = n;
        GroupMaskCopy(b->gm, n->gm);
    } else {
        struct NETLIBRARYCONFIG *p;

        GroupMaskOr(b->gm, n->gm);
        for (last = NULL, p = b->netLibraryConfigs; p; last = p, p = p->next)
            ;
        last->next = n;
    }

    /* If URL referring authentication is on, turn it off */
    b->referer = NULL;

    usageLogUser |= ULUNETLIBRARY;
    goto Finished;

BadFile:
    Log("NetLibrary file %s does not contain valid information", fn);
    goto Finished;

Finished:
    if (ctx) {
        EVP_CIPHER_CTX_free(ctx);
        ctx = NULL;
    }
    if (mdctx) {
        EVP_MD_CTX_free(mdctx);
        mdctx = NULL;
    }
    FreeThenNull(plainText);
    FreeThenNull(crypted);
    FreeThenNull(base64Text);

    if (f >= 0) {
        close(f);
        f = -1;
    }
}

static void ReadConfigRedirectSafeOrNeverProxyDomain(char *arg, BOOL redirectSafe) {
    struct REDIRECTSAFEORNEVERPROXY *p, *n;
    char *colon, *endOfHost;
    PORT port;

    if (arg == NULL)
        return;

    Trim(arg, TRIM_COMPRESS);
    AllLower(arg);

    arg = SkipFTPHTTPslashesAt(arg, NULL, NULL);

    if (*arg == 0)
        return;

    ParseHost(arg, &colon, &endOfHost, 1);

    if (endOfHost)
        *endOfHost = 0;

    if (colon) {
        *colon++ = 0;
        port = atoi(colon);
    } else {
        port = 0;
    }

    char temp[255];
    TrimHost(arg, "[]", temp);

    for (p = redirectSafeOrNeverProxy; p; p = p->next) {
        if (strcmp(temp, p->domain) == 0 && port == p->port)
            return;
    }

    if (!(n = (struct REDIRECTSAFEORNEVERPROXY *)calloc(1, sizeof(*n))))
        PANIC;
    if (!(n->domain = strdup(temp)))
        PANIC;
    n->port = port;
    n->next = redirectSafeOrNeverProxy;
    n->anyWild = strchr(arg, '*') != NULL;
    n->redirectSafe = redirectSafe;
    // There was a request to make these directives have the ability to log the corresponding
    // stanza, but change of scope broke a few long-standing configurations. The change is reversed
    // by commenting out the following but can be restored later if desired. n->database = null;
    // //ReadConfigCurrentDatabase();
    redirectSafeOrNeverProxy = n;
}

static void ReadConfigRedirectSafeOrNeverProxy(char *arg, BOOL redirectSafe) {
    char *str;
    char *domain;
    for (str = NULL, domain = StrTok_R(arg, ",", &str); domain;
         domain = StrTok_R(NULL, ",", &str)) {
        ReadConfigRedirectSafeOrNeverProxyDomain(domain, redirectSafe);
    }
}

static void ReadConfigRemoteIPHeader(char *arg) {
    if (*arg) {
        ReadConfigSetString(&xffRemoteIPHeader, arg);
    }
}

static void ReadConfigRemoteIP(char *arg, BOOL trustInternal, char *fileName, int line) {
    char *str;
    char *token;
    struct sockaddr_storage start, stop;
    struct XFFREMOTEIPRANGE *n;

    for (str = NULL, token = StrTok_R(arg, WS, &str); token; token = StrTok_R(NULL, WS, &str)) {
        if (!(ParseAddressRangeNew(arg, &start, &stop, fileName, line))) {
            continue;
        }
        if (!(n = calloc(1, sizeof(*n))))
            PANIC;
        memcpy(&n->start, &start, sizeof(n->start));
        memcpy(&n->stop, &stop, sizeof(n->stop));
        n->trustInternal = trustInternal;
        n->next = xffRemoteIPRanges;
        xffRemoteIPRanges = n;
        optionAcceptXForwardedFor = 1;
    }
}

static void ReadConfigOpenURLResolver(char *arg) {
    ReadConfigSetString(&openURLResolver, arg);
}

static void ReadConfigOption(char *arg, BOOL nameOnly, BOOL firstLine) {
    char *p;

    Trim(arg, TRIM_COMPRESS);

    if (*arg == 'd' && stricmp(arg + 1, "printf") == 0) {
        optionDprintf = 1;
        return;
    }

    if (p = StartsIWith(arg, "AdminSession")) {
        // only want a single pass
        if (nameOnly == 0)
            return;

        char *title;
        char *exp;
        char *end;

        title = SkipWS(p);

        end = SkipNonWS(title);
        *end = '\0';

        exp = SkipWS(++end);

        struct ADMINUISESSION *ptr = adminUIsessFields;

        // have to initialize
        if (!ptr) {
            if (!(adminUIsessFields = calloc(1, sizeof(struct ADMINUISESSION))))
                PANIC;

            ptr = adminUIsessFields;

            // go to last ptr
        } else {
            for (; ptr->next; ptr = ptr->next)
                ;
            if (!(ptr->next = calloc(1, sizeof(struct ADMINUISESSION))))
                PANIC;

            ptr = ptr->next;
        }

        if (!(ptr->title = strdup(title)))
            PANIC;
        if (!(ptr->expression = strdup(exp)))
            PANIC;

        return;
    }

    // set the default family to use ipv6
    if (stricmp(arg, "IPv6") == 0) {
        UUDEFAULT_FAMILY = AF_INET6;
        return;
    }

    if (stricmp(arg, "UserObjectGetTokenAlwaysRenew") == 0) {
        optionUserObjectGetTokenAlwaysRenew = TRUE;
        return;
    }

    if (stricmp(arg, "LogThreadStartup") == 0) {
        optionLogThreadStartup = TRUE;
        return;
    }

    if (stricmp(arg, "DispatchLog") == 0) {
        optionDispatchLog = TRUE;
        return;
    }

    if (stricmp(arg, "ProxyUserObject") == 0) {
        optionProxyUserObject = TRUE;
        return;
    }

    if (stricmp(arg, "PrecreateHosts") == 0) {
        optionPrecreateHosts = TRUE;
        return;
    }

    if (stricmp(arg, "BlockCountryChange") == 0) {
        optionBlockCountryChange = TRUE;
        return;
    }

    if (stricmp(arg, "UserObjectTestMode") == 0) {
        optionUserObjectTestMode = TRUE;
        gmUserObject = GroupMaskCopy(gmUserObject, gm);
        return;
    }

    if (stricmp(arg, "AllowLogoutURLRedirect") == 0) {
        optionAllowLogoutURLRedirect = TRUE;
        return;
    }

    if (stricmp(arg, "CASTestAttributes") == 0) {
        optionCasTestAttributes = TRUE;
        return;
    }

    if (stricmp(arg, "LogSAML") == 0) {
        optionLogSAML = 1;
        return;
    }

    if (stricmp(arg, "LogXML") == 0) {
        optionLogXML = TRUE;
        return;
    }

    if (stricmp(arg, "DisableTestEZproxyUsr") == 0) {
        optionDisableTestEZproxyUsr = 1;
        return;
    }

    if (stricmp(arg, "SQLStats") == 0) {
        optionSQLStats = 1;
        return;
    }

    if (stricmp(arg, "SecurityStartupStats") == 0) {
        optionSecurityStartupStats = TRUE;
        return;
    }

    if (stricmp(arg, "SuppressObfuscation") == 0) {
        optionSuppressObfuscation = 1;
        return;
    }

    if (stricmp(arg, "RefererInHostname") == 0) {
        optionRefererInHostname = 1;
        return;
    }

    if (stricmp(arg, "AllowSendChunking") == 0) {
        optionAllowSendChunking = 1;
        return;
    }

    if (stricmp(arg, "UseRaw") == 0) {
        optionUseRaw = 1;
        return;
    }

    if (stricmp(arg, "NoUseRaw") == 0) {
        optionUseRaw = 0;
        return;
    }

    if (p = StartsIWith(arg, "AllowSendGzip")) {
        if (IsWS(*p)) {
            optionAllowSendGzip = atoi(SkipWS(p));
        } else {
            optionAllowSendGzip = Z_DEFAULT_COMPRESSION;
        }
        return;
    }

    if (stricmp(arg, "KeepAlive") == 0) {
        optionKeepAlive = 1;
        /* This only works if AllowSendChunking is also on */
        optionAllowSendChunking = 1;
        /* and the use of DoHandleRaw must be off or else we can seize */
        optionUseRaw = 0;
        return;
    }

    if (stricmp(arg, "AcceptX-Forwarded-For") == 0) {
        optionAcceptXForwardedFor = 1;
        return;
    }

    if (stricmp(arg, "RecordPeaks") == 0) {
        optionRecordPeaks = 1;
        return;
    }

    if (stricmp(arg, "CaptureSPUR") == 0) {
        optionCaptureSpur = 1;
        return;
    }

    if (stricmp(arg, "GroupInReferer") == 0) {
        optionGroupInReferer = 1;
        anyOptionGroupInReferer = 1;
        return;
    }

    if (stricmp(arg, "NoGroupInReferer") == 0) {
        optionGroupInReferer = 0;
        return;
    }

    if (stricmp(arg, "LoginReplaceGroups") == 0) {
        optionLoginReplaceGroups = 1;
        return;
    }

    if (stricmp(arg, "LogSPUEdit") == 0) {
        optionLogSPUEdit = 1;
        return;
    }

    if (stricmp(arg, "DisableHTTP1.1") == 0) {
        optionDisableHTTP11 = 1;
        return;
    }

    if (stricmp(arg, "ebraryUnencodedTokens") == 0) {
        optionEbraryUnencodedTokens = 1;
        return;
    }

    if (stricmp(arg, "TicketIgnoreExcludeIP") == 0) {
        optionTicketIgnoreExcludeIP = 1;
        return;
    }

    if (stricmp(arg, "EnableSSLv3") == 0) {
        Log("Option EnableSSLv3 has been deprecated. SSLv3 is always disabled for security "
            "reasons.");
        return;
    }

    if (stricmp(arg, "DisableSSLv2") == 0) {
        Log("Option DisableSSLv2 has been deprecated.  SSLv2 is always disabled for security "
            "reasons.");
        return;
    }

    if (stricmp(arg, "DisableSSL40bit") == 0) {
        Log("Option DisableSSL40bit has been deprecated.  This is disabled by default.  Please see "
            "SSLCipherSuite directive.");
        return;
    }

    if (stricmp(arg, "DisableSSL56bit") == 0) {
        Log("Option DisableSSL56bit has been deprecated.  This is disabled by default.  Please see "
            "SSLCipherSuite directive.");
        return;
    }

    if (stricmp(arg, "NoHttpsHyphens") == 0) {
        optionNoHttpsHyphens = 1;
        return;
    }

    if (stricmp(arg, "HttpsHyphens") == 0) {
        optionNoHttpsHyphens = 0;
        return;
    }

    if (stricmp(arg, "Hide192") == 0) {
        optionHide192 = 1;
        return;
    }

    if (stricmp(arg, "UsernameCaretN") == 0 || stricmp(arg, "Username^N") == 0) {
        optionUsernameCaretN = 1;
        usageLogUser |= ULUUSERNAMECARETN;
        return;
    }

    if (stricmp(arg, "SafariCookiePatch") == 0) {
        optionSafariCookiePatch = 1;
        return;
    }

    if (stricmp(arg, "Headers") == 0) {
        optionHeaders = 1;
        return;
    }

    if (stricmp(arg, "AllowWebSubdirectories") == 0) {
        optionAllowWebSubdirectories = 1;
        return;
    }

    if (stricmp(arg, "LogSockets") == 0) {
        optionLogSockets = 1;
        return;
    }

    if (stricmp(arg, "LogSocketsDetailed") == 0) {
        optionLogSockets = 2;
        return;
    }

    if (stricmp(arg, "Fiddler") == 0) {
        optionFiddler = 1;
        return;
    }

    if (stricmp(arg, "NoFiddler") == 0) {
        optionFiddler = 0;
        return;
    }

    if (stricmp(arg, "Reboot") == 0) {
        optionReboot = 1;
        return;
    }

    if (stricmp(arg, "LawyeePatch") == 0) {
        optionLawyeePatch = 1;
        return;
    }

    if (stricmp(arg, "DisableTestLDAPInternal") == 0) {
        optionDisableTestLDAPInternal = 1;
        return;
    }

    if (stricmp(arg, "DisableSortable") == 0) {
        optionDisableSortable = 1;
        return;
    }

    if (stricmp(arg, optionAllowBadDomainsText) == 0) {
        if (firstLine) {
            optionAllowBadDomains = 1;
            if (nameOnly == 0) {
                Log("EZproxy has been configured to allow Domain lines that pose a security risk");
            }
        } else {
            Log("\"Option %s\" is ignored unless it is the first line of %s",
                optionAllowBadDomainsText, EZPROXYCFG);
        }
        return;
    }

    if (stricmp(arg, "MenuByGroups") == 0) {
        optionMenuByGroups = 1;
        return;
    }

    if (stricmp(arg, "IgnoreWildcardCertificate") == 0) {
        optionIgnoreWildcardCertificate = 1;
        optionForceWildcardCertificate = 0;
        return;
    }

    if (stricmp(arg, "ForceWildcardCertificate") == 0) {
        optionForceWildcardCertificate = 1;
        optionIgnoreWildcardCertificate = 0;
        return;
    }

    if (stricmp(arg, "X-Shibboleth") == 0) {
        return;
    }

    if (stricmp(arg, "LogDNS") == 0) {
        optionLogDNS = 1;
        return;
    }

    if (stricmp(arg, "RelaxedRADIUS") == 0) {
        optionRelaxedRADIUS = 1;
        return;
    }

    if (stricmp(arg, "ExcludeIPMenu") == 0) {
        optionExcludeIPMenu = 1;
        return;
    }

    if (stricmp(arg, "IgnoreSIGCHLD") == 0) {
        optionIgnoreSIGCHLD = 1;
        return;
    }

    if (stricmp(arg, "AnyDNSHostname") == 0) {
        optionAnyDNSHostname = 1;
        return;
    }

    if (stricmp(arg, "PostCRLF") == 0) {
        optionPostCRLF = 1;
        return;
    }
    if (stricmp(arg, "AggressiveCookie") == 0) {
        optionPassiveCookie = 0;
        return;
    }
    if (stricmp(arg, "PassiveCookie") == 0) {
        optionPassiveCookie = 1;
        return;
    }
    if (stricmp(arg, "LoginNoCache") == 0) {
        optionLoginNoCache = 1;
        return;
    }
    if (stricmp(arg, "AllowHTTPLogin") == 0) {
        optionAllowHTTPLogin = 1;
        return;
    }
    if (stricmp(arg, "ForceHTTPSLogin") == 0) {
        optionAllowHTTPLogin = 0;
        return;
    }
    if (stricmp(arg, "NoForceHTTPSLogin") == 0) {
        optionAllowHTTPLogin = 1;
        return;
    }
    if (stricmp(arg, "ForceHttpsAdmin") == 0) {
        optionForceHttpsAdmin = 1;
        return;
    }
    if (stricmp(arg, "AllowDebugger") == 0) {
        optionAllowDebugger = 1;
        return;
    }
    if (stricmp(arg, "ForceSSL") == 0) {
        if (UUSslEnabled())
            optionForceSsl = 1;
        return;
    }

    if (stricmp(arg, "EnableNagle") == 0) {
        optionDisableNagle = 0;
        return;
    }

    if (stricmp(arg, "DisableNagle") == 0) {
        optionDisableNagle = 1;
        return;
    }

    if (stricmp(arg, "RedirectPatch") == 0) {
        optionRedirectPatch = 1;
        return;
    }

    if (stricmp(arg, "noredirectpatch") == 0) {
        optionRedirectPatch = 0;
        return;
    }

    if (stricmp(arg, "sendnocache") == 0) {
        optionSendNoCache = 1;
        return;
    }

    if (stricmp(arg, "nosendnocache") == 0) {
        optionSendNoCache = 0;
        return;
    }

    if (stricmp(arg, "HideEZproxy") == 0) {
        optionHideEZproxy = 1;
        return;
    }

    if (stricmp(arg, "UTF16") == 0) {
        optionUTF16 = 1;
        return;
    }

    if (stricmp(arg, "NoUTF16") == 0) {
        optionUTF16 = 0;
        return;
    }

    if (stricmp(arg, "MetaEZproxyRewriting") == 0) {
        optionMetaEZproxyRewriting = 1;
        return;
    }

    if (stricmp(arg, "NoMetaEZproxyRewriting") == 0) {
        optionMetaEZproxyRewriting = 0;
        return;
    }

    if (stricmp(arg, "X-Forwarded-For") == 0) {
        optionXForwardedFor = 1;
        return;
    }

    if (stricmp(arg, "NoX-Forwarded-For") == 0) {
        optionXForwardedFor = 0;
        return;
    }

    if (stricmp(arg, "ProxyFTP") == 0) {
        optionProxyFtp = 1;
        return;
    }

    if (stricmp(arg, "NoProxyFTP") == 0) {
        optionProxyFtp = 0;
        return;
    }

    if (stricmp(arg, "NoHideEZproxy") == 0) {
        optionHideEZproxy = 0;
        return;
    }

    if (stricmp(arg, "singlesession") == 0) {
        optionSingleSession = 1;
        return;
    }

    if (stricmp(arg, "singlethreadedtranslations") == 0) {
        optionSingleThreadedTranslations = 1;
        return;
    }

    if (stricmp(arg, "nocookie") == 0) {
        optionCookie = OCNONE;
        return;
    }

    if (stricmp(arg, "cookie") == 0) {
        if (optionSingleSession) {
            if (!nameOnly)
                Log("Warning: OPTION SINGLESESSION overrides OPTION COOKIE");
        } else
            optionCookie = OCALL;
        return;
    }

    if (stricmp(arg, "domaincookieonly") == 0) {
        if (optionSingleSession) {
            if (!nameOnly)
                Log("Warning: OPTION SINGLESESSION overrides OPTION DOMAINCOOKIEONLY");
        } else
            optionCookie = OCDOMAIN;
        return;
    }

    if (stricmp(arg, "CookiePassThrough") == 0) {
        if (optionSingleSession) {
            if (!nameOnly)
                Log("Warning: OPTION SINGLESESSION overrides OPTION COOKIEPASSTHROUGH");
        } else
            optionCookie = OCPASSTHROUGH;
        return;
    }

    if (stricmp(arg, "loguser") == 0) {
        usageLogUser |= ULULOGUSER;
        return;
    }

    if (stricmp(arg, "statususer") == 0) {
        usageLogUser |= ULUSTATUSUSER;
        return;
    }

    if (stricmp(arg, "logsession") == 0) {
        usageLogSession = 1;
        return;
    }

    if (stricmp(arg, "UserObject") == 0) {
        usageLogUser |= ULUUSEROBJECT;
        return;
    }

    if (stricmp(arg, "status") == 0) {
        optionStatus = 1;
        return;
    }

    if (stricmp(arg, "logiii") == 0) {
        if (!nameOnly)
            Log("For Option LogIII, now use debug such as \"::iii,debug\"");
        return;
    }

    if (stricmp(arg, "logmail") == 0) {
        logMail = 1;
        return;
    }

    if (stricmp(arg, "logradius") == 0) {
        if (!nameOnly)
            Log("For Option LogRADIUS, now use debug such as \"::radius=serv.yourlib.org,debug\"");
        return;
    }

    if (stricmp(arg, "logauth") == 0) {
        if (!nameOnly)
            Log("For Option LogAuth, now use debug such as \"::pop=serv.yourlib.org,debug\"");
        return;
    }

    if (stricmp(arg, "logreferer") == 0) {
        logReferer = 1;
        return;
    }

    if (stricmp(arg, "redirectunknown") == 0) {
        if (nameOnly == 0) {
            Log("Option RedirectUnknown poses a security risk and was disabled "
                "in " EZPROXYNAMEPROPER " 5.1c");
            Log("For the preferred replacement, see RedirectSafe");
            Log("To renable the pre-5.1c behavior, use Option UnsafeRedirectUnknown");
        }
        return;
    }

    if (stricmp(arg, "UnsafeRedirectUnknown") == 0) {
        optionRedirectUnknown = 1;
        if (nameOnly == 0) {
            Log("Option UnsafeRedirectUnknown poses a security risk");
            Log("For the preferred replacement, see RedirectSafe");
        }
        return;
    }

    if (stricmp(arg, "ticks") == 0) {
        optionTicks = 1;
        return;
    }
    if (stricmp(arg, "tracksockets") == 0) {
        optionTrackSockets = 1;
        return;
    }

    if (stricmp(arg, "PROXYBYHOSTNAME") == 0) {
        optionProxyType = PTHOST;
        return;
    }

    if (stricmp(arg, "proxytypehost") == 0) {
        if (!nameOnly)
            Log("Please change PROXYTYPEHOST to PROXYBYHOSTNAME in your configuration files.");
        optionProxyType = PTHOST;
        return;
    }

    if (stricmp(arg, "REQUIREAUTHENTICATE") == 0) {
        optionRequireAuthenticate = 1;
        return;
    }

    if (stricmp(arg, "CSRFToken") == 0) {
        optionCSRFToken = 1;
        return;
    }

    if (stricmp(arg, "EZproxyCookieHTTPOnly") == 0) {
        optionEZproxyCookieHTTPOnly = 1;
        return;
    }

    if (stricmp(arg, "NoEZproxyCookieHTTPOnly") == 0) {
        optionEZproxyCookieHTTPOnly = 0;
        return;
    }

    if (stricmp(arg, "SilenceIPWarnings") == 0) {
        optionSilenceIPWarnings = 1;
        return;
    }

    if (!nameOnly)
        Log("Unrecognized Option: %s", arg);
}

static void ReadConfigOverDriveSite(char *arg) {
    char *arg2;
    char *p;
    char *url = NULL;
    char *secret = NULL;
    char *ilsName = NULL;
    char *libraryID = NULL;
    BOOL noTokens = 0;
    struct OVERDRIVESITE *ods, *last;

    for (;;) {
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (*arg != '-')
            break;
        if (stricmp(arg, "-NoTokens") == 0) {
            noTokens = 1;
        } else if (p = StartsIWith(arg, "-URL="))
            url = p;
        else if (p = StartsIWith(arg, "-Secret="))
            secret = p;
        else if (p = StartsIWith(arg, "-ILSName="))
            ilsName = p;
        else if (p = StartsIWith(arg, "-LibraryID="))
            libraryID = p;
        else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized OverDriveSite option: %s", arg);
        }
        arg = arg2;
    }

    if (*arg == 0) {
        Log("OverDriveSite missing site name");
        return;
    }

    if (url == NULL) {
        Log("OverDriveSite site %s missing URL", arg);
        return;
    }

    if (secret == NULL) {
        Log("OverDriveSite site %s missing shared secret", arg);
        return;
    }

    for (last = NULL, ods = overDriveSites; ods; last = ods, ods = ods->next) {
        if (stricmp(ods->site, arg) == 0) {
            Log("Redefinition of OverDriveSite %s ignored", arg);
            return;
        }
    }

    if (!(ods = calloc(1, sizeof(*ods))))
        PANIC;
    if (!(ods->site = strdup(arg)))
        PANIC;
    if (!(ods->url = strdup(url)))
        PANIC;
    if (!(ods->secret = strdup(secret)))
        PANIC;
    ods->noTokens = noTokens;
    if (ilsName)
        if (!(ods->ilsName = strdup(ilsName)))
            PANIC;
    if (libraryID)
        if (!(ods->libraryID = strdup(libraryID)))
            PANIC;
    ods->gm = GroupMaskCopy(NULL, gm);
    ods->next = NULL;

    if (last)
        last->next = ods;
    else
        overDriveSites = ods;

    usageLogUser |= ULUOVERDRIVE;
}

static void ReadConfigP3P(char *arg) {
    ReadConfigSetString(&p3pHeader, arg);
}

static void ReadConfigPdfRefresh(char *arg) {
    if (!(pdfRefreshes[cPdfRefreshes++].userAgent = strdup(arg)))
        PANIC;
}

static void ReadConfigPdfRefreshPre(char *arg) {
    ReadConfigSetString(&pdfRefreshPre, arg);
}

static void ReadConfigPdfRefreshPost(char *arg) {
    ReadConfigSetString(&pdfRefreshPost, arg);
}

static void ReadConfigPeer(char *arg) {
    struct PEER *c;
    char *colon;
    struct hostent *hp;
    PORT port;
    SOCKET s;
    int one = 1;

    c = peers + cPeers;

    if (colon = strchr(arg, ':')) {
        *colon++ = 0;
    }

    port = 0;
    if (colon)
        port = atoi(colon);
    if (port == 0)
        port = 2048;

    if (NumericHostIp4(arg)) {
        init_storage(&c->peerAddress, AF_INET, TRUE, port);
        inet_pton_st(AF_INET, arg, &c->peerAddress);

    } else if (NumericHostIp6(arg)) {
        init_storage(&c->peerAddress, AF_INET6, TRUE, port);
        inet_pton_st(AF_INET6, arg, &c->peerAddress);

    } else {
        if (UUGetHostByName(arg, &c->peerAddress, port, 0) != 0) {
            Log("Cluster peer host not found: %s", arg);
            return;
        }
    }

    // port should have already been set, but lets ensure it is
    set_port(&c->peerAddress, port);

    /* We don't add a peer that is already loaded; this allows this
       routine to load our address (by the CLUSTER statement), then
       allows a PEER to exist for us without conflict.  When combined
       with INCLUDEFILE, it allows a single EZPROXYCFG file to be
       used on multiple systems as-is
    */
    if (FindPeerByAddress(&c->peerAddress) != NULL)
        return;

    if (cPeers == 0) {
        s = MoveFileDescriptorHigh(socket(c->peerAddress.ss_family, SOCK_DGRAM, 0));
        if (debugLevel > 5)
            Log("%" SOCKET_FORMAT " Opened socket, errno=%d", s, (s == INVALID_SOCKET) ? errno : 0);
        if (s == INVALID_SOCKET) {
            Log("Unable to create socket for cluster test");
            PANIC;
        }

        setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *)&one, sizeof(one));

        if (bind(s, (struct sockaddr *)&c->peerAddress, get_size(&c->peerAddress))) {
            closesocket(s);
            Log("CLUSTER statement is not for this system");
            return;
        }
        closesocket(s);
    }

    if (!(c->peerName = strdup(arg)))
        PANIC;

    cPeers++;
}

static void ReadConfigPidFile(char *arg) {
    char *arg2;

    for (;;) {
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (*arg != '-')
            break;
        if (stricmp(arg, "-Guardian") == 0) {
            pidFileGuardian = 1;
        } else if (stricmp(arg, "-Charge") == 0) {
            pidFileCharge = 1;
            if (pidFileGuardian == 2)
                pidFileGuardian = 0;
        } else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized PidFile option: %s", arg);
        }
        arg = arg2;
    }

    if (*arg == 0) {
        Log("No file specified in PidFile");
        return;
    }

    ReadConfigSetString(&pidFileName, arg);
}

/* These next two routines may allocate strings that don't end up getting used, but it's not much
 * memory wasted */

static void ReadConfigProxy(char *arg, BOOL nameOnly) {
    char *colon, *endPort;
    char *space;

    proxyAuth = NULL;

    if (*arg == 0) {
        proxyHost = NULL;
        proxyPort = 0;
        return;
    }

    /* After the first one, ignore these in nameOnly pass
    if (nameOnly && firstProxyHost != NULL)
        return;
        */

    space = SkipNonWS(arg);
    if (*space) {
        *space++ = 0;
        space = SkipWS(space);
        if (*space) {
            proxyAuth = Encode64(NULL, space);
        }
    }

    ParseHost(arg, &colon, &endPort, FALSE);
    if (colon)
        *colon++ = 0;

    if (!(proxyHost = strdup(arg)))
        PANIC;

    TrimHost(arg, "[]", proxyHost);

    proxyPort = 0;
    if (colon)
        proxyPort = atoi(colon);
    if (proxyPort == 0)
        proxyPort = 80;

    if (firstProxyHost == NULL) {
        firstProxyHost = proxyHost;
        firstProxyPort = proxyPort;
        firstProxyAuth = proxyAuth;
    }

    struct PROXY *proxy = (struct PROXY *)malloc(sizeof(struct PROXY));
    if (proxy == NULL) {
        Log("**WARNING** Failed to build proxy list for license verification.");
        PANIC;
    }

    proxy->host = strdup(proxyHost);
    proxy->port = proxyPort;
    proxy->ssl = 0;
    proxy->next = NULL;
    proxy->auth = NULL;
    proxy->user = NULL;
    proxy->pass = NULL;

    if (proxyAuth != NULL)
        proxy->auth = strdup(proxyAuth);

    // get user/pass
    char *user, *pass;
    if ((colon = strchr(space, ':'))) {
        *colon = 0;
        user = space;
        pass = SkipWS(++colon);
        proxy->user = strdup(user);
        proxy->pass = strdup(pass);
    }
    wsKeyProxiesCount++;

    if (wsKeyProxies == NULL)
        wsKeyProxies = proxy;
    else {
        struct PROXY *temp = wsKeyProxies;
        while (temp->next != NULL)
            temp = temp->next;

        temp->next = proxy;
    }

    /*  printf("proxy on %s:%d", arg, proxyPort); */
}

static void ReadConfigProxySsl(char *arg, BOOL nameOnly) {
    char *colon, *endPort;
    char *space;

    /* Ignore this in nameOnly pass

    if (nameOnly)
        return;
    */

    proxySslAuth = NULL;

    if (*arg == 0) {
        proxySslHost = NULL;
        proxySslPort = 0;
        return;
    }

    space = SkipNonWS(arg);  // get past the host
    if (*space) {
        *space++ = 0;
        space = SkipWS(space);  // should be at username
        if (*space) {
            proxySslAuth = Encode64(NULL, space);  // encode 64 entire user:pass
        }
    }

    ParseHost(arg, &colon, &endPort, FALSE);
    if (colon)
        *colon++ = 0;

    if (!(proxySslHost = strdup(arg)))
        PANIC;

    TrimHost(arg, "[]", proxySslHost);

    proxySslPort = 0;
    if (colon)
        proxySslPort = atoi(colon);
    if (proxySslPort == 0)
        proxySslPort = 443;
    /*  printf("proxy on %s:%d", arg, proxyPort); */

    struct PROXY *proxy = (struct PROXY *)malloc(sizeof(struct PROXY));
    if (proxy == NULL) {
        Log("**WARNING** Failed to build proxy list for license verification.");
        PANIC;
    }

    proxy->host = strdup(proxySslHost);
    proxy->port = proxySslPort;
    proxy->ssl = 1;
    proxy->next = NULL;
    proxy->auth = NULL;
    proxy->user = NULL;
    proxy->pass = NULL;

    // if(proxySslAuth != NULL) proxy->auth = strdup(proxySslAuth);

    // get user/pass
    char *user, *pass;
    if ((colon = strchr(space, ':'))) {
        *colon = 0;
        user = space;
        pass = SkipWS(++colon);
        // proxy->user = strdup(user);
        // proxy->pass = strdup(pass);
    }
    wsKeyProxiesCount++;

    if (wsKeyProxies == NULL)
        wsKeyProxies = proxy;
    else {
        struct PROXY *temp = wsKeyProxies;
        while (temp->next != NULL)
            temp = temp->next;

        temp->next = proxy;
    }
}

static void ReadConfigProxyUrlPassword(char *arg) {
    ReadConfigSetString(&proxyUrlPassword, arg);
}

static void ReadConfigProxyHostnameEdit(char *arg, BOOL nameOnly) {
    struct PROXYHOSTNAMEEDIT *phe;
    char *str, *token;
    char *find, *replace;
    int increase;

    if (nameOnly) {
        mProxyHostnameEdits++;
        return;
    }

    phe = proxyHostnameEdits + cProxyHostnameEdits;

    find = replace = NULL;
    for (str = NULL, token = StrTok_R(arg, WS, &str); token; token = StrTok_R(NULL, WS, &str)) {
        if (find == NULL) {
            find = token;
        } else if (replace == NULL) {
            replace = token;
            break;
        }
    }

    if (replace == NULL) {
        Log("ProxyHostnameEdit statement missing either find or replace string");
        return;
    }

    if (!(phe->find = strdup(find)))
        PANIC;
    if (!(phe->replace = strdup(replace)))
        PANIC;

    /* The +1 allows for a ^ or $ that might be ignored in find */
    increase = strlen(replace) - strlen(find) + 1;
    if (increase > maxIncreaseProxyHostnameEdit)
        maxIncreaseProxyHostnameEdit = increase;

    cProxyHostnameEdits++;
}

static void ReadConfigReceiveBufferSize(char *arg) {
    receiveBufferSize = atoi(arg);
}

static void ReadConfigSendBufferSize(char *arg) {
    sendBufferSize = atoi(arg);
}

BOOL ReadConfigRefererValid(char *arg) {
    if (strlen(arg) > 32)
        return 0;

    for (; *arg; arg++) {
        if (IsAlpha(*arg))
            continue;
        if (IsDigit(*arg))
            continue;
        if (*arg == '_' || *arg == '-')
            continue;
        return 0;
    }

    return 1;
}

static void ReadConfigReferer(char *arg, BOOL nameOnly, char *fileName, int line) {
    char *arg2;
    char *p;
    struct sockaddr_storage start, stop;
    struct REFERER *n;

    // initialize the start and stop to max ranges
    init_storage(&start, 0, 0, 0);
    init_storage(&stop, 0, 0, 0);
    set_ip(&start, 0, 0, 0, 0);
    set_ip(&stop, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff);

    static struct REFERER *lastReferer = NULL;

    if (nameOnly)
        return;

    for (; arg;) {
        if (*arg != '-')
            break;
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (p = StartsIWith(arg, "-IP=")) {
            if (!(ParseAddressRangeNew(p, &start, &stop, fileName, line)))
                return;
        } else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized Referer option in %s:%d: %s", fileName, line, arg);
        }
        arg = arg2;
    }

    if (arg == NULL || *arg == 0) {
        /* When we switch from referring URL to non-referring, change group mask to
           be all of the groups that were authorized for referring URL
        */
        if (referer) {
            GroupMaskCopy(gm, refererGm);
        }
        referer = lastReferer = NULL;
        GroupMaskClear(refererGm);
        return;
    }

    if (!ReadConfigRefererValid(arg)) {
        Log("Referer may only be followed by up to 32 letters and/or digits: %s", arg);
        return;
    }

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;
    n->tag = StaticStringCopy(arg);
    n->gm = GroupMaskCopy(NULL, gm);

    memcpy(&n->start, &start, sizeof start);
    memcpy(&n->stop, &stop, sizeof stop);

    n->fileName = StaticStringCopy(fileName);
    n->line = line;
    if (referer) {
        lastReferer->next = n;
        GroupMaskOr(refererGm, gm);
    } else {
        referer = n;
        GroupMaskCopy(refererGm, gm);
    }
    lastReferer = n;

    if (is_max_range(&start, &stop))
        validateReferersRequired = 1;
}

static void ReadConfigReplace(char *arg) {
    struct DATABASE *b;
    char *state;
    unsigned long addStates = 0;
    unsigned long removeStates = ~addStates;

    b = ReadConfigCurrentDatabase();

    for (;;) {
        if (state = StartsIWith(arg, "-State=")) {
            arg = SkipNonWS(state);
            *arg++ = 0;
            arg = SkipWS(arg);
            addStates = removeStates = ReadConfigFindReplaceStatesMask(b, state);
            continue;
        }
        if (state = StartsIWith(arg, "-AddState=")) {
            arg = SkipNonWS(state);
            *arg++ = 0;
            arg = SkipWS(arg);
            addStates = ReadConfigFindReplaceStatesMask(b, state);
            continue;
        }
        if (state = StartsIWith(arg, "-RemoveState=")) {
            arg = SkipNonWS(state);
            *arg++ = 0;
            arg = SkipWS(arg);
            removeStates = ~ReadConfigFindReplaceStatesMask(b, state);
            continue;
        }
        break;
    }

    if (nextPattern == NULL) {
        Log(EZPROXYCFG " REPLACE missing a preceding FIND: %s", arg);
        return;
    }

    if (!(nextPattern->replaceString = strdup(arg)))
        PANIC;

    nextPattern->addStates = addStates;
    nextPattern->removeStates = removeStates;

    if (lastPattern)
        lastPattern->next = nextPattern;
    else
        b->patterns.pattern = nextPattern;

    lastPattern = nextPattern;
    nextPattern = NULL;
}

static void ReadConfigReroute(BOOL nameOnly, char *arg, char *type) {
    struct REROUTE *rr;

    if (nameOnly) {
        if (*type != 'T')
            mReroutes++;
        return;
    }

    if (*type == 'T') {
        if (rerouteQuote = (strnicmp(arg, "-quote", 6) == 0)) {
            arg = SkipWS(arg + 6);
        }
        if (!(rerouteTo = strdup(arg)))
            PANIC;
        return;
    }

    if (rerouteTo == NULL) {
        if (!(rerouteTo = strdup("")))
            PANIC;
    }

    rr = reroutes + cReroutes;

    rr->to = rerouteTo;
    if (!(rr->domain = strdup(arg)))
        PANIC;
    rr->domainLen = strlen(rr->domain);
    rr->hostOnly = *type == 'H';
    rr->quote = rerouteQuote;
    cReroutes++;
}

static void ReadConfigServerHeader(char *arg) {
    static BOOL first = 1;

    if (first) {
        serverHeader = NULL;
        first = 0;
    }

    ReadConfigSetString(&serverHeader, arg);
}

static void ReadConfigSessionKeySize(char *arg) {
    int sks = atoi(arg);

    // ORIGMAXKEY included room for null, sessionKeySize is actual key length without null
    if (ORIGMAXKEY - 1 <= sks && sks < MAXKEY) {
        sessionKeySize = sks;
    } else {
        Log("SessionKeySize must be between %d-%d", ORIGMAXKEY - 1, MAXKEY - 1);
    }
}

static void ReadConfigSetProxyHostnames(void) {
    struct DATABASE *b;
    struct INDOMAIN *m;
    int i;

    for (i = 0, b = databases; i < cDatabases; i++, b++) {
        for (m = b->dbdomains; m; m = m->next) {
            if (m->hostOnly)
                m->proxyHostname = ProxyHostname(m->domain, m->port, m->forceMyPort, m->useSsl, 0,
                                                 b->optionNoHttpsHyphens);
        }
    }
}

#define ShibbolethAcceptanceLogControlMask (7)
#define ShibbolethAcceptanceControlAccept (0)
#define ShibbolethAcceptanceControlReject (1)
#define ShibbolethAcceptanceControlAcceptIfHttpsGet (2)
#define ShibbolethAcceptanceLogControlLog (8)

char *ReadConfigShibbolethAcceptanceControl(char *p, int *value) {
    char *p2;

    if ((p2 = StartsIWith(p, "true")) && ((*p2 == 0) || (*p2 == ','))) {
        *value = ShibbolethAcceptanceControlAccept;
    } else if ((p2 = StartsIWith(p, "false")) && ((*p2 == 0) || (*p2 == ','))) {
        *value = ShibbolethAcceptanceControlReject;
    } else if ((p2 = StartsIWith(p, "IfHttpsGet")) && ((*p2 == 0) || (*p2 == ','))) {
        *value = ShibbolethAcceptanceControlAcceptIfHttpsGet;
    } else {
        return NULL;
    }
    if ((*p2 == ',') && (p2 = StartsIWith(++p2, "log")) && (*p2 == 0)) {
        *value |= ShibbolethAcceptanceLogControlLog;
    }
    return p2;
}

BOOL ReadConfigTrueFalse(char *p, BOOL *value) {
    if (strcmp(p, "true") == 0) {
        *value = TRUE;
        return TRUE;
    } else if (strcmp(p, "false") == 0) {
        *value = FALSE;
        return TRUE;
    } else {
        return FALSE;
    }
}

BOOL DoShibbolethAcceptanceControl(int control, int condition1, int httpsGet) {
    if (condition1 == 2) {
        if (((control & ShibbolethAcceptanceLogControlMask) == ShibbolethAcceptanceControlReject)) {
            return FALSE; /* not signed, so being rejected */
        } else if ((control & ShibbolethAcceptanceLogControlMask) ==
                   ShibbolethAcceptanceControlAccept) {
            return TRUE; /* not signed, Accept */
        } else if ((control & ShibbolethAcceptanceLogControlMask) ==
                       ShibbolethAcceptanceControlAcceptIfHttpsGet &&
                   (httpsGet == TRUE)) {
            return TRUE; /* not signed and HttpGet, Accept */
        }
    } else if ((condition1 == 1) || (condition1 == -1)) {
        return FALSE; /* signed and not valid, so being rejected */
    }
    return TRUE; /* signed and valid, Accept */
}

BOOL DoShibbolethAcceptanceLogControl(int control, int condition1) {
    if (condition1 == 2) {
        if ((control & ShibbolethAcceptanceLogControlLog) == ShibbolethAcceptanceLogControlLog) {
            return TRUE; /* Log */
        } else {
            return debugLevel > 1; /* Don't Log unless debugging*/
        }
    } else if (condition1 != 0) {
        return TRUE; /* Log */
    } else {
        return debugLevel > 1; /* Don't Log unless debugging*/
    }
}

static void ReadConfigShibbolethDisable(char *arg) {
    if (stricmp(arg, "1.2") == 0)
        shibbolethAvailable12 = SHIBBOLETHAVAILABLEDISABLED;
    else if (stricmp(arg, "1.3") == 0)
        shibbolethAvailable13 = SHIBBOLETHAVAILABLEDISABLED;
    else if (stricmp(arg, "2.0") == 0)
        shibbolethAvailable20 = SHIBBOLETHAVAILABLEDISABLED;
    else
        Log("ShibbolethDisable unrecognized version: %s", arg);
}

static void ReadConfigShibbolethProviderId(char *arg) {
    if (!ShibbolethAvailable())
        return;

    ReadConfigSetString(&shibbolethProviderId, arg);
}

/*
static void ReadConfigShibbolethClientCertificateNum(struct SHIBBOLETHSITE *shibSite, int cert)
{
    char buffer[32];

    if (shibSite->clientSignCert != NULL) {
        FreeThenNull(shibSite->clientSignCert);
    }
    shibSite->clientCertificate = cert;
    snprintf(buffer, 32, "%s%08d.key", SSLDIR, shibSite->clientCertificate ?
shibSite->clientCertificate : sslActiveIndex); if (!(shibSite->clientSignCert = strdup(buffer)))
PANIC;
}

static void ReadConfigShibbolethClientCertificateStr(struct SHIBBOLETHSITE *shibSite, char *arg)
{
    int cert;

    if (arg != NULL) {
        cert = atoi(arg);
    } else {
        cert = 0;
    }
    ReadConfigShibbolethClientCertificateNum(shibSite, cert);
}
*/

static void ReadConfigShibbolethMetadata(char *arg, BOOL nameOnly) {
#define SIGNCERT_ASYMPTOTE SIGNCERT_MAX + 1
#define SIGNCERT_MAX 127
    char *entityID = NULL;
    char *file = NULL;
    char *url = NULL;
    char *authnContextClassRef = NULL;
    char **signCertArray = NULL;
    int signCertArrayIndex = 0;
    int signCertArrayIndexAsymptote = 0;
    char *newSignCertList = NULL;
    xmlSecTransformId redirectSignatureMethod = xmlSecTransformRsaSha1Id;
    int newStrSize;
    char *nulStr = "";
    int nulStrSize = strlen(nulStr) + 1;
    int totalSize;
    int i;
    char *token, *str;
    struct SHIBBOLETHSITE *ss, *saLast;
    char useSsl = FALSE;
    char *p = NULL;
    char *p2 = NULL;
    char *colon, *endOfHost;
    PORT port = 0, defaultPort = 0;
    char *cert = NULL;
    char *skipHyphen;
    static BOOL allEntityIDsMatch = TRUE;
    BOOL signResponse = TRUE;
    BOOL signAssertion = FALSE;
    BOOL encryptAssertion = TRUE;
    BOOL signAuthnRequest = FALSE;
    BOOL slo = FALSE;

    if (nameOnly)
        return;

    if (!ShibbolethAvailable())
        return;

    if (!(signCertArray = calloc(SIGNCERT_ASYMPTOTE, sizeof(char *))))
        PANIC;

    for (str = NULL, token = StrTok_R(arg, WS, &str); token; token = StrTok_R(NULL, WS, &str)) {
        if (*token == 0)
            continue;

        skipHyphen = token;
        if (*skipHyphen == '-')
            skipHyphen++;

        if (p = StartsIWith(skipHyphen, "entityID=")) {
            entityID = p;
            continue;
        }

        if ((p = StartsIWith(skipHyphen, "File=")) || (p = StartsIWith(skipHyphen, "out="))) {
            file = p;
            continue;
        }

        if ((p = StartsIWith(skipHyphen, "Cert=")) || (p = StartsIWith(skipHyphen, "AACert="))) {
            cert = p;
            continue;
        }

        if (p = StartsIWith(skipHyphen, "URL=")) {
            url = p;
            continue;
        }

        if ((p = StartsIWith(skipHyphen, "SignResponse="))) {
            if (ReadConfigTrueFalse(p, &signResponse)) {
                continue;
            } else {
                Log("Invalid SignResponse argument: %s", p);
                return;
            }
        }

        if ((p = StartsIWith(skipHyphen, "SignAssertion="))) {
            if (ReadConfigTrueFalse(p, &signAssertion)) {
                continue;
            } else {
                Log("Invalid SignAssertion argument: %s", p);
                return;
            }
        }

        if ((p = StartsIWith(skipHyphen, "EncryptAssertion="))) {
            if (ReadConfigTrueFalse(p, &encryptAssertion)) {
                continue;
            } else {
                Log("Invalid EncryptAssertion argument: %s", p);
                return;
            }
        }

        if ((p = StartsIWith(skipHyphen, "SignAuthnRequest="))) {
            if (ReadConfigTrueFalse(p, &signAuthnRequest)) {
                continue;
            } else {
                Log("Invalid SignAuthnRequest argument: %s", p);
                return;
            }
        }

        if (stricmp(skipHyphen, "SignAuthnRequest") == 0) {
            signAuthnRequest = TRUE;
            continue;
        }

        if ((p = StartsIWith(skipHyphen, "URLValidate=")) ||
            (p = StartsIWith(skipHyphen, "SignCert="))) {
            if (signCertArrayIndex <= SIGNCERT_MAX) {
                signCertArray[signCertArrayIndex] = p;
            }
            signCertArrayIndex++;
            continue;
        }

        if ((p = StartsIWith(skipHyphen, "authnContextClassRef="))) {
            authnContextClassRef = p;
            continue;
        }

        if ((p = StartsIWith(skipHyphen, "RedirectSignature="))) {
            if (stricmp(p, "SHA1") == 0) {
                redirectSignatureMethod = xmlSecTransformRsaSha1Id;
            } else if (stricmp(p, "SHA256") == 0) {
                redirectSignatureMethod = xmlSecTransformRsaSha256Id;
            } else if (stricmp(p, "SHA512") == 0) {
                redirectSignatureMethod = xmlSecTransformRsaSha512Id;
            } else {
                Log("ShibbolethMetadata invalid RedirectSignature: %s", p);
            }
            continue;
        }

        if (stricmp(skipHyphen, "slo") == 0) {
            slo = TRUE;
            continue;
        }

        Log("ShibbolethMetadata unrecognized option: %s", token);
    }

    if (file == NULL || file[0] == 0) {
        Log("Missing -File= parameter.  ShibbolethMetadata configuration line ignored.");
        return;
    }

    if (url) {
        p = url;
        if (strnicmp(p, "http://", 7) == 0) {
            useSsl = 0;
            p += 7;
            defaultPort = 80;
        } else if (strnicmp(p, "https://", 8) == 0) {
            useSsl = 1;
            p += 8;
            defaultPort = 443;
        } else {
            Log("Invalid ShibbolethMetadata URL: %s", url);
            return;
        }

        ParseHost(p, &colon, &endOfHost, 0);

        if (colon) {
            port = atoi(colon + 1);
        } else {
            port = defaultPort;
        }
    }

    for (ss = shibbolethSites, saLast = NULL; ss; ss = ss->next) {
        if (strcmp(file, ss->filename) == 0)
            break;
        saLast = ss;
    }

    if (ss == NULL) {
        if (!(ss = calloc(1, sizeof(*ss))))
            PANIC;
        ss->next = NULL;
        if (!(ss->filename = strdup(file)))
            PANIC;
        UUInitMutex(&ss->mutex, ss->filename);

        if (entityID) {
            if (!(ss->entityID = strdup(entityID)))
                PANIC;
            if (shibbolethAvailable13 == SHIBBOLETHAVAILABLEOFF)
                shibbolethAvailable13 = SHIBBOLETHAVAILABLEON;
            if (shibbolethAvailable20 == SHIBBOLETHAVAILABLEOFF)
                shibbolethAvailable20 = SHIBBOLETHAVAILABLEON;
        } else {
            static BOOL warn1320 = TRUE;
            /*
             * With no Entity ID, this entry represents a Shibboleth 1.2 definition. This preserves
             * classic Shibboleth 1.2 handling if ShibbolethWAYF defined but no ShibbolethProviderID
             * defined
             */
            if (warn1320) {
                Log("To enable Shibboleth 1.3/2.0 support, ShibbolethMetadata must include an "
                    "EntityID.");
                warn1320 = FALSE;
            }
        }

        /* BEGIN signCertList processing */
        /* signCertList is a list of zero-terminated strings which list is terminated with a null
         * string. */
        if (signCertArrayIndex >= SIGNCERT_ASYMPTOTE) {
            signCertArrayIndexAsymptote = SIGNCERT_MAX;
        } else {
            signCertArrayIndexAsymptote = signCertArrayIndex;
        }
        totalSize = 0;
        for (i = 0; i < signCertArrayIndexAsymptote; i++) {
            newStrSize = strlen(signCertArray[i]) + 1;
            totalSize = totalSize + newStrSize;
        }
        totalSize = totalSize + nulStrSize;

        if (!(newSignCertList = malloc(totalSize)))
            PANIC;

        totalSize = 0;
        for (i = 0; i < signCertArrayIndexAsymptote; i++) {
            newStrSize = strlen(signCertArray[i]) + 1;
            memcpy(&(newSignCertList[totalSize]), signCertArray[i], newStrSize); /* copy new str */
            if (debugLevel > 1) {
                Log("Trusted certificate number %d for checking the metadata signature: %s", i,
                    signCertArray[i]);
            }
            totalSize = totalSize + newStrSize;
        }
        memcpy(&(newSignCertList[totalSize]), nulStr, nulStrSize); /* copy null str */
        totalSize = totalSize + nulStrSize;

        ss->signCertList = newSignCertList;
        newSignCertList = NULL;

        if (signCertArrayIndex >= SIGNCERT_ASYMPTOTE) {
            Log("Too many SignCert= arguments given, %d max, ignored %s and  %d subsequent",
                SIGNCERT_MAX, signCertArray[SIGNCERT_MAX], signCertArrayIndex - SIGNCERT_ASYMPTOTE);
        }
        FreeThenNull(signCertArray);
        /* END signCertList processing */

        if (authnContextClassRef) {
            if (!(ss->authnContextClassRef = strdup(authnContextClassRef)))
                PANIC;
        }

        ss->signAuthnRequest = signAuthnRequest;
        ss->slo = slo;
        ss->redirectSignatureMethod = redirectSignatureMethod;

        if (saLast) {
            saLast->next = ss;
        } else {
            shibbolethSites = ss;
        }

        if (allEntityIDsMatch) {
            // A lack of an entity ID does not count in this case
            if (ss->entityID == NULL) {
                shibbolethSitesCommonEntityID = NULL;
                allEntityIDsMatch = FALSE;
            } else {
                if (shibbolethSitesCommonEntityID == NULL) {
                    shibbolethSitesCommonEntityID = ss->entityID;
                } else if (strcmp(shibbolethSitesCommonEntityID, ss->entityID) != 0) {
                    shibbolethSitesCommonEntityID = NULL;
                    allEntityIDsMatch = FALSE;
                }
            }
        }
    }

    ss->certificateCount = 0;

    if (cert) {
        int certificateCount;
        char *str;
        char *token;

        // Start at 2 so there is room for the first certificate plus a final terminating 0
        for (certificateCount = 2, str = cert; (str = strchr(str, ','));
             certificateCount++, str++) {
        }

        if (!(ss->certificates = calloc(certificateCount, sizeof(*ss->certificates))))
            PANIC;
        for (str = NULL, token = StrTok_R(cert, ",", &str); token;
             token = StrTok_R(NULL, WS, &str)) {
            int certNum = atoi(token);
            if (certNum > 0) {
                ss->certificates[ss->certificateCount++] = certNum;
            }
        }
        if (ss->certificateCount == 0) {
            FreeThenNull(ss->certificates);
        }
    }
    ss->signResponse = signResponse;
    ss->signAssertion = signAssertion;
    ss->encryptAssertion = encryptAssertion;

    memcpy(&ss->ipInterface, &ipInterface, sizeof(ss->ipInterface));

    FreeThenNull(ss->url);

    if (url) {
        char save = 0;

        ss->urlSsl = useSsl;

        ReadConfigSetString(&ss->url, url);

        if (colon) {
            *colon = 0;
        }
        if (endOfHost) {
            save = *endOfHost;
            *endOfHost = 0;
        }

        char temp[255];
        TrimHost(p, "[]", temp);
        ReadConfigSetString(&ss->urlHost, temp);

        if (endOfHost)
            *endOfHost = save;

        ss->urlPort = port;
        ss->urlDefaultPort = port == defaultPort;

        if (endOfHost == NULL) {
            ReadConfigSetString(&ss->urlRequest, "/");
        } else {
            FreeThenNull(ss->urlRequest);
            if (!(ss->urlRequest = malloc(strlen(endOfHost) + 2)))
                PANIC;
            if (*endOfHost != '/') {
                *ss->urlRequest = '/';
                strcpy(ss->urlRequest + 1, endOfHost);
            } else {
                strcpy(ss->urlRequest, endOfHost);
            }
        }

        ss->proxyHost = proxyHost;
        ss->proxyPort = proxyPort;
        ss->proxyAuth = proxyAuth;
        ss->proxySslHost = proxySslHost;
        ss->proxySslPort = proxySslPort;
        ss->proxySslAuth = proxySslAuth;
    }

    FreeThenNull(signCertArray);
}

static void ReadConfigShibbolethWAYF(char *arg) {
    if (!ShibbolethAvailable())
        return;

    if (stricmp(arg, "inqueue") == 0) {
        arg = "https://wayf.internet2.edu/InQueue/WAYF";
    }

    ReadConfigSetString(&shibbolethWAYF, arg);
}

static void ReadConfigSPUEdit(char *arg) {
    static struct SPUEDIT *lastSpuEdit = NULL;
    struct SPUEDIT *n;
    char delim;
    char *find, *replace = NULL, *options = NULL, *beyondOptions = NULL;
    char *p;
    char *argCopy = NULL;
    pcre *findPattern = NULL;
    pcre_extra *findPatternExtra = NULL;
    const char *pcreError; /* pcreError will point to a static string, so don't try to free it! */
    int pcreErrorOffset;
    int optionsValues;
    char stop = 0;
    char redirect = 0;
    char global = 0;
    char autoLoginIP = 0;
    char excludeIP = 0;
    char includeIP = 0;
    char any = 0;

    for (;; any = 1, arg = p) {
        arg = SkipWS(arg);
        if (p = StartsIWith(arg, "-AutoLoginIP ")) {
            autoLoginIP = 1;
            continue;
        }
        if (p = StartsIWith(arg, "-ExcludeIP ")) {
            excludeIP = 1;
            continue;
        }
        if (p = StartsIWith(arg, "-IncludeIP ")) {
            includeIP = 1;
            continue;
        }
        break;
    }

    if (any == 0)
        autoLoginIP = excludeIP = includeIP = 1;

    if (*arg == 0)
        goto Finished;

    if (*arg < '!' || *arg > 126 || IsAlpha(*arg) || IsDigit(*arg)) {
        Log("SPUEdit must start with a non-alpha, non-digit character to delimit the find and "
            "replace strings");
        goto Finished;
    }

    if (!(argCopy = strdup(arg)))
        PANIC;

    delim = *arg++;

    find = arg;

    for (p = arg; *p; p++) {
        if (*p == '\\' && *(p + 1) == delim)
            StrCpyOverlap(p, p + 1);
        else if (*p == delim) {
            *p++ = 0;
            if (replace == NULL)
                replace = p;
            else if (options == NULL)
                options = p;
            else
                beyondOptions = p;
        }
    }

    if (replace == NULL || options == NULL || beyondOptions != NULL) {
        Log("Invalid SPUEdit: %s", argCopy);
        goto Finished;
    }

    optionsValues = 0;

    for (; *options; options++) {
        switch (*options) {
        case 'g':
            global = 1;
            break;
        case 'r':
            redirect = 1;
            break;
        case 'i':
            optionsValues |= PCRE_CASELESS;
            break;
        case 's':
            stop = 1;
            break;
        default:
            Log("Unrecognized SPUEdit option: %c", *options);
        }
    }

    /* pcreError will point to a static string, so don't try to free it! */
    findPattern = pcre_compile(find, optionsValues, &pcreError, &pcreErrorOffset, NULL);
    if (findPattern == NULL) {
        Log("SPUEdit find error %s at %d: %s", pcreError, pcreErrorOffset, find);
        goto Finished;
    }
    findPatternExtra = pcre_study(findPattern, 0, &pcreError);
    if (pcreError) {
        Log("SPUEdit find error %s: %s", pcreError, find);
        pcre_free(findPattern);
        findPattern = NULL;
        goto Finished;
    }

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;

    if (!(n->find = strdup(find)))
        PANIC;
    if (!(n->replace = strdup(replace)))
        PANIC;
    n->findPattern = findPattern;
    n->findPatternExtra = findPatternExtra;
    n->stop = stop;
    n->redirect = redirect;
    n->global = global;
    n->cIpTypes = any ? cIpTypes : -1;
    n->autoLoginIP = autoLoginIP;
    n->excludeIP = excludeIP;
    n->includeIP = includeIP;

    if (lastSpuEdit == NULL) {
        spuEdits = n;
    } else {
        lastSpuEdit->next = n;
    }
    lastSpuEdit = n;

Finished:
    FreeThenNull(argCopy);
    /* pcreError will point to a static string, so don't try to free it! */
}

static void ReadConfigSPUEditVar(char *arg) {
    static struct SPUEDITVAR *last = NULL;
    struct SPUEDITVAR *n;
    char *equals;

    if (!(equals = strchr(arg, '='))) {
        Log("SPUEditVar missing =: %s", arg);
        return;
    }

    *equals++ = 0;

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;

    if (!(n->var = strdup(arg)))
        PANIC;
    if (!(n->val = strdup(equals)))
        PANIC;
    n->varLen = strlen(n->var);

    if (last)
        last->next = n;
    else
        spuEditVars = n;
    last = n;
}

static void ReadConfigSSO(char *arg) {
    char *arg2;
    char *p;
    char *url = NULL;
    char *secret = NULL;
    struct SSOSITE *ssos, *last;
    enum SSOHASH ssoHash = SSOHASHSHA1;
    BOOL anonymous = FALSE;

    for (;;) {
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (*arg != '-')
            break;
        if (p = StartsIWith(arg, "-URL="))
            url = p;
        else if (stricmp(arg, "-Anonymous") == 0) {
            anonymous = 1;
        } else if (p = StartsIWith(arg, "-Secret="))
            secret = p;
        else if (p = StartsIWith(arg, "-Hash=")) {
            if (stricmp(p, "SHA1") == 0)
                ssoHash = SSOHASHSHA1;
            else if (stricmp(p, "MD5") == 0)
                ssoHash = SSOHASHMD5;
            else if (stricmp(p, "SHA256") == 0)
                ssoHash = SSOHASHSHA256;
            else {
                Log("Unrecognized SSO -Hash: %s", p);
                return;
            }
        } else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized SSO option: %s", arg);
        }
        arg = arg2;
    }

    if (*arg == 0) {
        Log("SSO missing site name");
        return;
    }

    if (url == NULL) {
        Log("SSO %s missing URL", arg);
        return;
    }

    if (secret == NULL) {
        Log("SSO %s missing shared secret", arg);
        return;
    }

    if (strlen(secret) != 24) {
        Log("SSO %s secret is %d characters long; 24 required", arg, strlen(secret));
        return;
    }

    for (last = NULL, ssos = ssoSites; ssos; last = ssos, ssos = ssos->next) {
        if (stricmp(ssos->site, arg) == 0) {
            Log("Redefinition of SSO %s ignored", arg);
            return;
        }
    }

    if (!(ssos = calloc(1, sizeof(*ssos))))
        PANIC;
    if (!(ssos->site = strdup(arg)))
        PANIC;
    if (!(ssos->url = strdup(url)))
        PANIC;
    if (!(ssos->secret = strdup(secret)))
        PANIC;
    ssos->gm = GroupMaskCopy(NULL, gm);
    ssos->ssoHash = ssoHash;
    ssos->cIpTypes = cIpTypes;
    ssos->anonymous = anonymous;
    ssos->next = NULL;

    if (last)
        last->next = ssos;
    else
        ssoSites = ssos;

    usageLogUser |= ULUSSO;
}

static void ReadConfigSSOUsername(char *arg) {
    char *expr;
    char *p;
    char *url = NULL;
    char *secret = NULL;
    struct SSOSITE *ssos, *last;
    enum SSOHASH ssoHash = SSOHASHSHA1;
    BOOL anonymous = FALSE;

    expr = SkipNonWS(arg);
    if (*expr) {
        *expr++ = 0;
        expr = SkipWS(expr);
    }

    if (*arg == 0) {
        Log("SSO missing site name");
        return;
    }

    if (*expr == 0) {
        Log("SSO %s missing expression to generate username to release", arg);
        return;
    }

    for (last = NULL, ssos = ssoSites; ssos && stricmp(ssos->site, arg) != 0;
         last = ssos, ssos = ssos->next) {
    }

    if (ssos == NULL) {
        Log("SSOUsername must follow an SSO directive for the same site");
        return;
    }

    if (ssos->anonymous) {
        Log("SSOUsername not allowed for sites that use SSO -Anonymous");
        return;
    }

    if (!(ssos->expression = strdup(expr)))
        PANIC;
}

static void ReadConfigLoginSocketBacklog(char *arg) {
    int i;

    i = atoi(arg);

    if (i < 5) {
        Log("LoginSocketBacklog may not be set lower than 5");
        return;
    }

    loginSocketBacklog = i;
}

static void ReadConfigHostSocketBacklog(char *arg) {
    int i;

    i = atoi(arg);

    if (i < 5) {
        Log("HostSocketBacklog may not be set lower than 5");
        return;
    }

    hostSocketBacklog = i;
}

static void ReadConfigDNSSocketBacklog(char *arg) {
    int i;

    i = atoi(arg);

    if (i < 5) {
        Log("DNSSocketBacklog may not be set lower than 5");
        return;
    }

    dnsSocketBacklog = i;
}

static void ReadConfigSSLCert(char *arg) {
    if (UUSslEnabled()) {
        if ((sslIdx = atoi(arg))) {
            if (UUSslLoadKey(sslIdx, 1, NULL, 1, NULL) == NULL) {
                Log("SSLCert %d ignored since certificate does not exist", sslIdx);
                sslIdx = 0;
            }
        }
    }
}

static void ReadConfigSSLCipherSuite(char *arg) {
    char *arg2;
    BOOL inbound = FALSE;
    BOOL outbound = FALSE;

    for (;;) {
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (*arg != '-')
            break;
        if (stricmp(arg, "-inbound") == 0) {
            inbound = TRUE;
        } else if (stricmp(arg, "-outbound") == 0) {
            outbound = TRUE;
        } else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized SSLCipherSuite option: %s", arg);
        }
        arg = arg2;
    }

    if (!(inbound || outbound)) {
        Log("Default TLS security was increased in EZproxy 7.2");
        Log("To override SSLCipherSuite, include -inbound and/or -outbound before ciphers");
    }

    if (*arg == 0) {
        Log("WARNING: SSLCipherSuite requires an OpenSSL formatted cipher specification");
        return;
    }

    if (inbound) {
        ReadConfigSetString(&SSLCipherSuiteInbound, arg);
    }

    if (outbound) {
        ReadConfigSetString(&SSLCipherSuiteOutbound, arg);
    }
}

static void ReadConfigSSLOpenSSLConfCmd(char *arg) {
    static SSL_CONF_CTX *cctx = NULL;

    if (arg == NULL) {
        if (cctx) {
            SSL_CONF_CTX_finish(cctx);
            SSL_CONF_CTX_free(cctx);
            cctx = NULL;
        }
        return;
    }

    if (cctx == NULL) {
        if (!(cctx = SSL_CONF_CTX_new()))
            PANIC;
        SSL_CONF_CTX_set_flags(cctx, SSL_CONF_FLAG_FILE | SSL_CONF_FLAG_SERVER |
                                         SSL_CONF_FLAG_CLIENT | SSL_CONF_FLAG_CERTIFICATE |
                                         SSL_CONF_FLAG_SHOW_ERRORS);
    }

    char *arg2;
    char *p;
    int cert = 0;
    int certSpecified = 0;
    int inbound = 0;
    int outbound = 0;
    for (;;) {
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (*arg != '-') {
            break;
        }
        if ((p = StartsIWith(arg, "-cert="))) {
            cert = atoi(p);
            certSpecified = 1;
        } else if (stricmp(arg, "-inbound") == 0) {
            inbound = 1;
            cert = -1;
        } else if (stricmp(arg, "-outbound") == 0) {
            outbound = 1;
        } else {
            Log("Unrecognized SSLOpenSSLConfCmd option: %s", arg);
        }
        arg = arg2;
    }

    if (inbound && outbound) {
        Log("SSLOpenSSLConfCmd -inbound and -outbound may not appear together");
        return;
    }

    if ((inbound || outbound) && certSpecified) {
        Log("SSLOpenSSLConfCmd -cert may not be combined with -inbound or -outbound");
        return;
    }

    int cmdType = SSL_CONF_cmd_value_type(cctx, arg);

    if (cmdType == SSL_CONF_TYPE_UNKNOWN) {
        Log("SSLOpenSSLConfCmd invalid command: %s", arg);
        return;
    }

    // This value is coming in a future release of OpenSSL; prepare for it now
#ifdef SSL_CONF_TYPE_NONE
    if (cmdType == SSL_CONF_TYPE_NONE) {
        if (*arg2) {
            Log("SSLOpenSSLConfCmd ignoring unused argument: %s %s", arg, arg2);
            arg2 = "";
        }
    } else
#endif
    {
        if (*arg2 == 0) {
            Log("SSLOpenSSLConfCmd requires an argument: %s", arg);
        }

        struct stat statBuf;

        if (cmdType == SSL_CONF_TYPE_FILE) {
            if (stat(arg2, &statBuf) != 0) {
                Log("SSLOpenSSLConfCmd %s file not found: %s", arg, arg2);
                return;
            }
            if (!S_ISREG(statBuf.st_mode)) {
                Log("SSLOpenSSLConfCmd %s argument is not a file: %s", arg, arg2);
                return;
            }
        }

        if (cmdType == SSL_CONF_TYPE_DIR) {
            if (stat(arg2, &statBuf) != 0) {
                Log("SSLOpenSSLConfCmd %s directory not found: %s", arg, arg2);
                return;
            }
            if (!S_ISDIR(statBuf.st_mode)) {
                Log("SSLOpenSSLConfCmd %s argument  value is not a directory: %s", arg, arg2);
                return;
            }
        }
    }

    static struct OPENSSLCONFCMD *last = NULL;
    struct OPENSSLCONFCMD *n;

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;
    n->cert = cert;
    if (!(n->cmd = strdup(arg)))
        PANIC;
    if (arg2) {
        if (!(n->value = strdup(arg2)))
            PANIC;
    }
    if (last) {
        last->next = n;
    } else {
        openSSLConfCmds = n;
    }
    last = n;
}

static void ReadConfigSSLHonorCipherOrder(char *arg) {
    if (stricmp(arg, "on") == 0) {
        sslHonorCipherOrder = 1;
    } else if (stricmp(arg, "off") == 0) {
        sslHonorCipherOrder = 0;
    } else {
        Log("SSLHonorCipherOrder invalid value: %s", arg);
    }
}

static void ReadConfigStartingPointUrlRefresh(char *arg) {
    char *arg2;

    arg2 = SkipNonWS(arg);
    if (*arg2) {
        *arg2++ = 0;
        arg2 = SkipWS(arg2);
    }

    if (*arg == '+' || *arg == '-')
        startingPointUrlRefreshes[cStartingPointUrlRefreshes].mode = *arg++;
    else
        startingPointUrlRefreshes[cStartingPointUrlRefreshes].mode = '+';

    if (!(startingPointUrlRefreshes[cStartingPointUrlRefreshes].filter = strdup(arg)))
        PANIC;
    if (!(startingPointUrlRefreshes[cStartingPointUrlRefreshes++].userAgent = strdup(arg2)))
        PANIC;
}

static void ReadConfigClientTimeout(char *arg) {
    int i;

    i = atoi(arg);
    if (i > 0) {
        clientTimeout = i;
    }
}

static void ReadConfigRemoteTimeout(char *arg) {
    int i;

    i = atoi(arg);
    if (i > 0) {
        remoteTimeout = i;
        if (binaryTimeout < i)
            binaryTimeout = i;
    } else {
        Log("Invalid Timeout value: %s", arg);
    }
}

static void ReadConfigConnectWindow(char *arg) {
    int i = atoi(arg);
    if (i > 0) {
        connectWindow = i;
    } else {
        Log("ConnectWindow invalid: %s", arg);
    }
}

void AddDatabaseUsageLimit(struct DATABASEUSAGELIMIT **dbul, struct USAGELIMIT *ul) {
    struct DATABASEUSAGELIMIT *p, *n;

    for (p = *dbul; p; p = p->next) {
        if (p->usageLimit == ul)
            return;
    }
    if (!(n = calloc(1, sizeof(*n))))
        PANIC;
    n->next = *dbul;
    n->usageLimit = ul;
    *dbul = n;
    usageLogUser |= ULUUSAGELIMIT;
}

static void ReadConfigTitle(char *arg) {
    struct DATABASE *b;
    struct USAGELIMIT *ul;

    ReadConfigValidateFree();
    if (arg == NULL || *arg == 0)
        arg = "untitled";

    b = ReadConfigAdvance();

    if (b->hideFromMenu = (strnicmp(arg, "-hide ", 6) == 0))
        arg = SkipWS(arg + 6);

    if (!(b->title = strdup(arg)))
        PANIC;
    b->cIpTypes = cIpTypes;
    b->cAnonymousUrls = cAnonymousUrls;
    b->sessionLife = mSessionLife;
    /* When using referring URL, we copy the total of all groups associated to referring URL
       definitions instead of taking the current collection of groups
    */
    b->gm = GroupMaskCopy(NULL, (referer ? refererGm : gm));
    b->optionLawyeePatch = optionLawyeePatch;
    b->optionNoHttpsHyphens = optionNoHttpsHyphens;
    b->sslIdx = sslIdx;
    optionLawyeePatch = 0;
    memcpy(&b->ipInterface, &ipInterface, sizeof(b->ipInterface));
    memcpy(&b->vars, &dbvars, sizeof(b->vars));

    for (ul = usageLimits; ul; ul = ul->next) {
        if (ul->open) {
            AddDatabaseUsageLimit(&b->databaseUsageLimits, ul);
        }
    }
}

BOOL ReadConfigByteServe(char *arg) {
    const char *pcreError; /* pcreError will point to a static string, so don't try to free it! */
    int pcreErrorOffset;
    struct BYTESERVE *n;

    pcre *re = pcre_compile(arg, PCRE_CASELESS, &pcreError, &pcreErrorOffset, NULL);
    if (re == NULL) {
        Log("ByteServe invalid regular expression URL %s at %d: %s", pcreError, pcreErrorOffset,
            arg);
        return 0;
    }

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;

    n->regexUri = re;
    n->regexUriExtra = pcre_study(n->regexUri, 0, &pcreError);
    if (!(n->uri = strdup(arg)))
        PANIC;
    n->next = byteServes;
    byteServes = n;

    return 1;
}

BOOL ReadConfigMimeFiltersRegex(struct MIMEFILTER *ptr, char *mimePattern, char *uriPattern) {
    const char *pcreError; /* pcreError will point to a static string, so don't try to free it! */
    int pcreErrorOffset;

    ptr->regexMime = pcre_compile(mimePattern, PCRE_CASELESS, &pcreError, &pcreErrorOffset, NULL);
    if (ptr->regexMime == NULL) {
        Log("MimeFilter user defined setup failure: %s at %d: %s", pcreError, pcreErrorOffset,
            mimePattern);
        return 0;
    }

    ptr->regexUri = pcre_compile(uriPattern, PCRE_CASELESS, &pcreError, &pcreErrorOffset, NULL);
    if (ptr->regexUri == NULL) {
        Log("MimeFilter user defined setup failure: %s at %d: %s", pcreError, pcreErrorOffset,
            uriPattern);
        pcre_free(ptr->regexMime);
        ptr->regexMime = NULL;
        return 0;
    }

    return 1;
}

static void ReadConfigMimeFilterAddDefaults() {
    struct DATABASE *b;
    b = databases + cDatabases;
    b->mimeFiltersDefault = mimeFiltersDefault;
}

static void ReadConfigMimeFilter(char *arg) {
    struct DATABASE *b;
    struct MIMEFILTER newMimeFilter;
    memset(&newMimeFilter, 0, sizeof(newMimeFilter));

    b = databases + cDatabases;

    if (b->title == NULL) {
        Log("MimeFilter must be preceded by a Title directive.");
        return;
    }

    /**
     * MIMEFILTER <mime pattern> <uri pattern> [-]<action>
     * <mime pattern> = pcre regular expression, start and end with " is optional
     * <uri pattern>  = pcre regular expression, start and end with " is optional
     * [-] = an optional minus sign to indicate to remove default patterns
     * <action> = an enum of javascript, html, pdf, text
     */

    char *mime = SkipWS(arg);
    char *uri = NextWSArg(mime);
    char *action = NextWSArg(uri);
    int noDefault = (action[0] == '-') ? 1 : 0;

    if (noDefault)
        action++;

    // quick sanity check
    if (strlen(mime) == 0 || strlen(uri) == 0 || strlen(action) == 0) {
        Log("MimeFilter user defined setup failed. Invalid syntax (%s %s %s%s) for %s.", mime, uri,
            (noDefault ? "-" : ""), action, b->title);
        return;
    }

    // get the action
    if (strcmp(action, "javascript") == 0)
        newMimeFilter.action = javascript;
    else if (strcmp(action, "html") == 0)
        newMimeFilter.action = html;
    else if (strcmp(action, "text") == 0)
        newMimeFilter.action = text;
    else if (strcmp(action, "pdf") == 0)
        newMimeFilter.action = pdf;
    else if (strcmp(action, "image") == 0)
        newMimeFilter.action = image;
    else if (strcmp(action, "none") == 0)
        newMimeFilter.action = none;
    else {
        Log("MimeFilter user defined setup failed. Invalid action (%s) for %s.", action, b->title);
        return;
    }

    // add the mime filter
    if (ReadConfigMimeFiltersRegex(&newMimeFilter, mime, uri)) {
        struct MIMEFILTER *ptr;
        // first time here for database, need to initialize it
        if (b->mimeFiltersUser == NULL) {
            // we reset cMimeFilters on each pass before calling this function
            // so it is always 1 ahead
            b->mimeFiltersUser = mimeFiltersUser + cMimeFilters - 1;
            ptr = b->mimeFiltersUser;

            // not first time, traverse the children
        } else {
            for (ptr = b->mimeFiltersUser; ptr->next != NULL; ptr = ptr->next)
                ;
            ptr->next = mimeFiltersUser + cMimeFilters - 1;
            ptr = ptr->next;
        }
        memcpy(ptr, &newMimeFilter, sizeof(*ptr));
        if (!(ptr->mime = strdup(mime)))
            PANIC;
        if (!(ptr->uri = strdup(uri)))
            PANIC;
        ptr->noDefault = noDefault;
    }

    // remove defaults
    if (noDefault)
        b->mimeFiltersDefault = NULL;
}

static void ReadConfigCharset(char *arg) {
    ReadConfigSetString(&defaultCharset, arg);
    if (*defaultCharset == 0) {
        FreeThenNull(defaultCharset);
    }
}

static void ReadConfigTokenKey(BOOL nameOnly, char *arg) {
    struct DATABASE *b;
    unsigned char key[EVP_MAX_KEY_LENGTH];
    unsigned char ivec[EVP_MAX_IV_LENGTH];
    int len;

    if (nameOnly)
        return;

    if (someKey == 0) {
        Log("Due to export restrictions, TokenKey requires an " EZPROXYNAMEPROPER " license");
        return;
    }

    b = ReadConfigCurrentDatabase();

    FreeThenNull(b->tokenKey);

    if (!(b->tokenKey = malloc(24)))
        PANIC;
    len = EVP_BytesToKey(EVP_des_ede3_cbc(), EVP_md5(), NULL, (unsigned char *)arg, strlen(arg), 2,
                         key, ivec);
    memcpy(b->tokenKey, key, 24);

    /* Server Status checks for this to decide whether or not to show token lookup */
    usageLogUser |= ULUTOKENKEY;
}

static void ReadConfigTokenSalt(char *arg) {
    ReadConfigSetString(&tokenSalt, arg);
}

static void ReadConfigTokenSignatureKey(BOOL nameOnly, char *arg) {
    struct DATABASE *b;

    if (nameOnly)
        return;

    if (someKey == 0) {
        Log("Due to export restrictions, TokenSignatureKey requires an " EZPROXYNAMEPROPER
            " license");
        return;
    }

    b = ReadConfigCurrentDatabase();

    FreeThenNull(b->tokenSignatureKey);

    if (!(b->tokenSignatureKey = calloc(1, 25)))
        PANIC;
    StrCpy3((char *)b->tokenSignatureKey, arg, 25);
}

static void ReadConfigRunAs(BOOL nameOnly, char *arg) {
#ifdef WIN32
    Log("RunAs not supported under Windows");
#else
    struct passwd *pw;
    struct group *gr;
    char *colon;

    if (nameOnly && (changedUid || changedGid)) {
        Log("RunAs may only appear once in " EZPROXYCFG);
        AbortFatalConfig();
    }

    if (colon = strchr(arg, ':')) {
        *colon++ = 0;
        Trim(colon, TRIM_LEAD | TRIM_TRAIL);
    }

    Trim(arg, TRIM_LEAD | TRIM_TRAIL);

    if (*arg) {
        if (IsAllDigits(arg))
            runUid = atoi(arg);
        else {
            pw = getpwnam(arg);
            if (pw == NULL) {
                Log("RunAs specifies unknown user %s, aborting", arg);
                AbortFatalConfig();
            }
            runUid = pw->pw_uid;
        }
        changedUid = 1;
    }

    if (colon && *colon) {
        if (IsAllDigits(colon))
            runGid = atoi(arg);
        else {
            gr = getgrnam(colon);
            if (gr == NULL) {
                Log("RunAs specifies unknown group %s, aborting", colon);
                AbortFatalConfig();
            }
            runGid = gr->gr_gid;
        }
        changedGid = 1;
    }
#endif
}

static void ReadConfigURL(char *option, char *arg) {
    struct DATABASE *b, *rb;
    char *arg2;
    char *p;
    char useSsl;
    BOOL getRefresh = 0;
    BOOL getAppend = 0;
    BOOL getAppendEncoded = 0;
    BOOL getRedirect = 0;
    char *formMethod = NULL;
    BOOL rewriteHost = 0;

    b = ReadConfigCurrentDatabase();

    for (;;) {
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (*arg != '-')
            break;
        if (stricmp(arg, "-refresh") == 0)
            getRefresh = getRedirect = 1;
        else if (stricmp(arg, "-redirect") == 0)
            getRedirect = 1;
        else if (stricmp(arg, "-append") == 0)
            getAppend = 1;
        else if (stricmp(arg, "-encoded") == 0)
            getAppendEncoded = 1;
        else if (stricmp(arg, "-form") == 0 || stricmp(arg, "-form=post") == 0) {
            formMethod = "post";
            getRedirect = 1;
        } else if (stricmp(arg, "-form=get") == 0) {
            formMethod = "get";
            getRedirect = 1;
        } else if (stricmp(arg, "-form=dynamic") == 0) {
            formMethod = "dynamic";
            getRedirect = 1;
        } else if (stricmp(arg, "-rewritehost") == 0)
            rewriteHost = 1;
        else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized URL option: %s", arg);
        }
        arg = arg2;
    }

    if (arg2 && *arg2) {
        if (stricmp(arg, "up") == 0) {
            Log("The URL reference \"%s\" is reserved for use by EZproxy", arg);
            return;
        }
        b->getAppend = getAppend || strchr(option, 'A') != NULL;
        b->getAppendEncoded = getAppendEncoded || strchr(option, 'E') != NULL;
        b->getRefresh = getRefresh;
        b->formMethod = formMethod;
        b->rewriteHost = rewriteHost;
        if (getRedirect || strchr(option, 'R') != NULL) {
            if (!(b->getName = strdup(arg)))
                PANIC;
            b->getRedirect = 1;
            if (!(b->getUrl = strdup(arg2)))
                PANIC;
        } else {
            arg2 = SkipHTTPslashesAt(arg2, &useSsl);

            char *beginPort;
            p = arg2;
            ParseHost(p, &beginPort, &p, FALSE);

            // no port detected
            if (beginPort == NULL) {
                b->getPort = (useSsl ? 443 : 80);
            } else {
                *beginPort++ = 0;
                b->getPort = atoi(beginPort);
            }

            if (p == NULL || *p != '/') {
                if (!(b->getUrl = strdup("/")))
                    PANIC;
            } else {
                if (!(b->getUrl = strdup(p)))
                    PANIC;
                *p = 0;
            }

            // trim the hostname
            TrimHost(arg2, "[]", arg2);

            if (!(b->getHost = strdup(arg2)))
                PANIC;
            if (!(b->getName = strdup(arg)))
                PANIC;
            b->getUseSsl = useSsl;
        }
        sprintf(arg, "%s/login/%s", (myUrlHttp ? myUrlHttp : myUrlHttps), b->getName);
        if (!(b->url = strdup(arg)))
            PANIC;
        b->referer = NULL; /* turn off referring URL if it is on */
        for (rb = b - 1; rb >= databases; rb--) {
            if (rb->getName && stricmp(b->getName, rb->getName) == 0) {
                rb->getRelated = b;
                b->optionGroupInReferer = rb->optionGroupInReferer;
                break;
            }
        }
    } else {
        if (!(b->url = strdup(arg)))
            PANIC;
        /* Add host from URL as a default HOST line */
        if (*arg)
            ReadConfigDomain(FALSE, arg, TRUE);
    }
}

static void ReadConfigUsageLimit(char *arg, char *file, int lineNumber) {
    char *arg2;
    struct USAGELIMIT *ul;
    BOOL haveMb = 0, haveTransfers = 0, haveInterval = 0, haveEnd = 0, haveExpires = 0,
         haveLocal = 0, haveEnforce = 0;
    BOOL haveIgnoreNormalLogin = 0, haveIgnoreAutoLoginIP = 0, haveIgnoreRefererLogin = 0;
    unsigned int maxRecvKBytes = 0;
    unsigned int maxTransfers = 0;
    unsigned int interval = 0;
    unsigned int expires = 0;
    int j;
    double mb;

    if (*arg == 0)
        return;

    for (;;) {
        if (*arg != '-')
            break;
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (stricmp(arg, "-enforce") == 0) {
            haveEnforce = 1;
        } else if (stricmp(arg, "-local") == 0) {
            haveLocal = 1;
        } else if (strnicmp(arg, "-mb=", 4) == 0) {
            mb = atof(arg + 4);
            if (mb > 2000000) {
                Log("-mb %s reduced to maximum of 2000000", arg + 4);
                mb = 2000000;
            }
            if (mb > 0) {
                maxRecvKBytes = (unsigned int)(mb * 1000.0);
                haveMb = 1;
            }
        } else if (strnicmp(arg, "-transfers=", 11) == 0) {
            j = atoi(arg + 11);
            haveTransfers = j >= 0;
            maxTransfers = max(0, j);
        } else if (strnicmp(arg, "-interval=", 10) == 0) {
            interval = atoi(arg + 10);
            haveInterval = interval > 0;
        } else if (strnicmp(arg, "-expires=", 9) == 0) {
            expires = atoi(arg + 9);
            haveExpires = expires > 0;
        } else if (stricmp(arg, "-end") == 0) {
            haveEnd = 1;
        } else if (stricmp(arg, "-IgnoreNormalLogin") == 0) {
            haveIgnoreNormalLogin = 1;
        } else if (stricmp(arg, "-IgnoreAutoLoginIP") == 0) {
            haveIgnoreAutoLoginIP = 1;
        } else if (stricmp(arg, "-IgnoreRefererLogin") == 0) {
            haveIgnoreRefererLogin = 1;
        } else if (strcmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized UsageLimit option %s in %s(%d)", arg, file, lineNumber);
        }
        arg = arg2;
    }

    if (*arg == 0) {
        Log("UsageLimit missing name in %s(%d)", file, lineNumber);
        return;
    }

    ul = FindUsageLimit(arg, 1);

    ul->open = 1;

    if (haveLocal)
        AddDatabaseUsageLimit(&localDatabaseUsageLimits, ul);

    if (haveMb)
        ul->maxRecvKBytes = maxRecvKBytes;

    if (haveTransfers)
        ul->maxTransfers = maxTransfers;

    if (haveInterval)
        ul->interval = interval * 60;

    if (haveExpires)
        ul->expires = expires * 60;

    if (haveEnforce)
        ul->enforce = 1;

    if (haveIgnoreNormalLogin)
        ul->ignoreNormalLogin = 1;

    if (haveIgnoreAutoLoginIP)
        ul->ignoreAutoLoginIP = 1;

    if (haveIgnoreRefererLogin)
        ul->ignoreRefererLogin = 1;

    if (haveEnd)
        ul->open = 0;

    ul->granularity = ul->interval / 12;

    if (ul->granularity < 30)
        ul->granularity = 30;

    ul->oldestInterval = ul->interval + ul->granularity;
}

static void ReadConfigValidateFree(void) {
    struct VALIDATE *v, *n;

    /* If the Validate statement wasn't followed by any URL/Host/Domain statements, free
       up what has been allocated until we reach a point that was used or we hit
       the end of the list
    */
    for (v = validate; v && v->referenced == 0; v = n) {
        n = v->next;
        FreeThenNull(v->auth);
        FreeThenNull(v->path);
        FreeThenNull(v);
    }

    validate = NULL;
}

static void ReadConfigValidate(char *arg) {
    struct VALIDATE *v = NULL;
    char *path = NULL;

    if (*arg == 0) {
        ReadConfigValidateFree();
        return;
    }
    /*
    if (validate) {
        free(validate);
        validate = NULL;
    }
    */

    path = NULL;

    if (strnicmp(arg, "path=", 5) == 0) {
        path = arg + 5;
        arg = SkipNonWS(path);
        if (*arg) {
            *arg++ = 0;
            arg = SkipWS(arg);
        }
    }

    if (!(v = (struct VALIDATE *)calloc(1, sizeof(*v))))
        PANIC;
    if (arg && *arg)
        v->auth = Encode64(NULL, arg);
    if (path) {
        if (!(v->path = strdup(path)))
            PANIC;
    }
    v->next = validate;
    validate = v;
}

static void ReadConfigSciFinder(char *arg, BOOL nameOnly) {
    char *z3950Host;
    char *z3950Port;
    struct LOGINPORT *lp;
    char *arg2;
    BOOL socks = 1;

    for (;;) {
        if (*arg != '-')
            break;
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (stricmp(arg, "-socks") == 0)
            socks = 1;
        else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized SciFinder option: %s", arg);
        }
        arg = arg2;
    }

    z3950Host = SkipNonWS(arg);
    if (z3950Host == NULL || *z3950Host == 0)
        goto Invalid;

    *z3950Host++ = 0;
    z3950Host = SkipWS(z3950Host);

    ParseHost(z3950Host, &z3950Port, NULL, TRUE);

    if (z3950Port == NULL)
        goto Invalid;

    *z3950Port++ = 0;
    if (atoi(z3950Port) <= 0)
        goto Invalid;

    TrimHost(z3950Host, "[]", z3950Host);

    Trim(arg, TRIM_LEAD | TRIM_TRAIL);
    if (*arg == 0)
        goto Invalid;

    if (atoi(arg) <= 0)
        goto Invalid;

    lp = ReadConfigLoginPort(arg, nameOnly, USESSLZ3950);

    if (lp && nameOnly == 0) {
        lp->z3950Host = StaticStringCopy(z3950Host);
        lp->z3950Port = (PORT)atoi(z3950Port);
        lp->z3950Socks = socks;
    }

    goto Finished;

Invalid:
    if (nameOnly == 0)
        Log("SciFinder requires EZproxyPort SciFinderHost:SciFinderPort");
    goto Finished;

Finished:;
}

static void ReadConfigUmask(char *arg, BOOL nameOnly) {
    int newUmask = 0;
    char *p;

    /* umask must start with a 0, and be followed by three octal digits */
    if (nameOnly) {
        if (*arg != '0' || strlen(arg) != 4)
            goto Invalid;
        for (p = arg + 1; *p; p++) {
            if (*p < '0' || *p > '7')
                goto Invalid;
            newUmask = (newUmask << 3) | (*p - '0');
        }
        defaultUmask = newUmask;
        umask(newUmask);
    }

    return;

Invalid:
    if (nameOnly)
        Log("Invalid umask: %s", arg);
}

void ReadConfigEnvironment(const char *arg, BOOL nameOnly) {
    if (arg == NULL) {
        arg = "PROD";
    }

    if (nameOnly) {
        if (stricmp(arg, "QA") == 0) {
            environment = 2;
            if (debugLevel) {
                Log("Environment is QA");
            }

            wskeyExpirePeriod = NINETY_DAYS_IN_SECONDS;
            wskeyThreadSleep = ONE_HOUR_IN_SECONDS;
            wskeyRecheckPeriod = THIRTY_DAYS_IN_SECONDS;
            wskeyRecheckFailPeriod = ONE_HOUR_IN_SECONDS;

            wsKeyServiceHost = LICENSE_SERVER_DEVQA;
            wsKeyServiceSsl = LICENSE_SERVER_DEVQA_SSL;
            wsKeyServicePort = LICENSE_SERVER_DEVQA_PORT;

        } else if (stricmp(arg, "DEV") == 0) {
            environment = 1;
            Log("Running as DEV Environment");

            wskeyExpirePeriod = ONE_WEEK_IN_SECONDS;
            wskeyThreadSleep = 60;  // 1 minute
            wskeyRecheckPeriod = 120;
            wskeyRecheckFailPeriod = 90;

            wsKeyServiceHost = LICENSE_SERVER_DEVQA;
            wsKeyServiceSsl = LICENSE_SERVER_DEVQA_SSL;
            wsKeyServicePort = LICENSE_SERVER_DEVQA_PORT;

        } else if (stricmp(arg, "TESTING") == 0) {
            environment = 1;
            Log("Running as TESTING Environment");

            wskeyExpirePeriod = 60;       // seconds
            wskeyThreadSleep = 15;        // seconds
            wskeyRecheckPeriod = 30;      // seconds
            wskeyRecheckFailPeriod = 30;  // seconds

            wsKeyServiceHost = LICENSE_SERVER_DEVQA;
            wsKeyServiceSsl = LICENSE_SERVER_DEVQA_SSL;
            wsKeyServicePort = LICENSE_SERVER_DEVQA_PORT;

        } else if (stricmp(arg, "LOCALHOST") == 0) {
            environment = 1;
            Log("Running as LOCAHOST Environment");

            wskeyExpirePeriod = 60;       // seconds
            wskeyThreadSleep = 15;        // seconds
            wskeyRecheckPeriod = 30;      // seconds
            wskeyRecheckFailPeriod = 30;  // seconds

            wsKeyServiceHost = "localhost";
            wsKeyServiceSsl = FALSE;
            wsKeyServicePort = 9090;

        } else if (stricmp(arg, "BADHOST") == 0) {
            environment = 1;
            Log("Running as BAD Environment");

            time_t wskeyExpirePeriod = ONE_WEEK_IN_SECONDS;
            time_t wskeyThreadSleep = 60;        // seconds
            time_t wskeyRecheckPeriod = 120;     // seconds
            time_t wskeyRecheckFailPeriod = 90;  // seconds

            wsKeyServiceHost = "some.bad.host";
            wsKeyServiceSsl = LICENSE_SERVER_DEVQA_SSL;
            wsKeyServicePort = LICENSE_SERVER_DEVQA_PORT;

        } else if (stricmp(arg, "PROXY") == 0) {
            environment = 1;
            Log("Running as PROXY Environment");

            wskeyExpirePeriod = 120;      // seconds
            wskeyThreadSleep = 15;        // seconds
            wskeyRecheckPeriod = 15;      // seconds
            wskeyRecheckFailPeriod = 10;  // seconds

            wsKeyServiceHost = LICENSE_SERVER_DEVQA;
            wsKeyServiceSsl = LICENSE_SERVER_DEVQA_SSL;
            wsKeyServicePort = LICENSE_SERVER_DEVQA_PORT;

            wsKeyForceProxy = TRUE;

        } else {
            environment = 0;

            if (stricmp(arg, "PROXYPROD") == 0) {
                wsKeyForceProxy = TRUE;
            } else if (stricmp(arg, "PROD")) {
                Log("Environment is invalid %s, assuming PROD", arg);
            }

            wskeyExpirePeriod = 10 * ONE_DAY_IN_SECONDS;
            wskeyThreadSleep = ONE_HOUR_IN_SECONDS;
            wskeyRecheckPeriod = ONE_DAY_IN_SECONDS;
            wskeyRecheckFailPeriod = ONE_HOUR_IN_SECONDS;

            wsKeyServiceHost = LICENSE_SERVER_PROD;
            wsKeyServiceSsl = LICENSE_SERVER_PROD_SSL;
            wsKeyServicePort = LICENSE_SERVER_PROD_PORT;
        }
    }
}

void ValidateReferers(void) {
    int i, j;
    struct DATABASE *b;
    int lastCIpTypes = 0;
    struct REFERER *lastReferer = NULL, *referer;
    struct IPBLOCK *ipb = NULL;

    for (b = databases, i = 0; i < cDatabases; b++, i++) {
        if (b->referer == NULL)
            continue;
        if (b->referer == lastReferer && b->cIpTypes == lastCIpTypes)
            continue;
        lastReferer = b->referer;
        lastCIpTypes = b->cIpTypes;
        /* If the first range is everything, there's no need to verify anything and
           we can just skip around to the next database
        */
        if (is_max_range(&lastReferer->start, &lastReferer->stop))
            continue;
        ipb = NewIPBlock();
        /* Add in all the ExcludeIP lines, subtract out the
           AutoLoginIP and IncludeIP, and then for whatever remains,
           subtract out the referer lines
        */
        for (j = 0; j < b->cIpTypes; j++) {
            if (ipTypes[j].ipAccess == IPALOCAL)
                AddIPBlock(ipb, &ipTypes[j].start, &ipTypes[j].stop);
            else
                DeleteIPBlock(ipb, &ipTypes[j].start, &ipTypes[j].stop);
        }
        for (referer = b->referer; referer; referer = referer->next) {
            DeleteIPBlock(ipb, &referer->start, &referer->stop);
        }
        if (ipb->flink != ipb) {
            struct IPBLOCK *p;
            char ipBuffer1[INET6_ADDRSTRLEN], ipBuffer2[INET6_ADDRSTRLEN];
            Log("No Referer -IP matches database %s for the following ExcludeIP range(s)",
                b->title);
            for (p = ipb->flink; p != ipb; p = p->flink) {
                Log("  %s-%s", ipToStr(&p->start, ipBuffer1, sizeof ipBuffer1),
                    ipToStr(&p->stop, ipBuffer2, sizeof ipBuffer2));
            }
            Log("Access from the preceding range(s) may result error pages");
        }

        FreeIPBlock(ipb);
        ipb = NULL;
    }
}

/* When nameOnly is 2, process just runas for non-Win32 Guardian functionality */
void ReadConfig(BOOL nameOnly) {
#define MAXINCLUDEFILE 64
#define MAXINCLUDEFILELIST 4096
    int result;
    FILE *saveFILE[MAXINCLUDEFILE];
    int saveFilenameListIndex[MAXINCLUDEFILE];
    char *configFilenameList[MAXINCLUDEFILELIST];
    char *arg;
    char *hostAddr = "none";
    int hostAddrLen = 4;
    int configActiveIdx = 0;
    int currentFilenameListIndex = 0;
    int configFilenameListMax = 0;
    int i;
    char *currentFilename = NULL;
    FILE *currentFILE;
    int saveLineNum[MAXINCLUDEFILE];
    char line[8192];
    char *lineNext;
    int lineRemain;
    size_t cmdLen;
    struct hostent *myHostEnt;
    static struct {
        char *oldcmd;
        char *newcmd;
        size_t min;
    } *rp, remapcmd[] = {
               {"DJ", "DOMAINJAVASCRIPT", 2},
               {"HJ", "HOSTJAVASCRIPT", 2},
               {"INTRUDERATTEMPTS", "IntruderIPAttempts", 16},
               {"UA", "URLAPPEND", 2},
               {"UR", "URLREDIRECT", 2},
               {"UAE", "URLAPPENDENCODED", 2},
               {"URA", "URLREDIRECTAPPEND", 3},
               {"UAR", "URLREDIRECTAPPEND", 3},
               {"URAE", "URLREDIRECTAPPENDENCODED", 4},
               {"URLAPPENDREDIRECT", "URLREDIRECTAPPEND", 17},
               {"PHE", "PROXYHOSTNAMEEDIT", 3},
               {"LOCALIP", "EXCLUDEIP", 7},
               {"REMOTEIP", "INCLUDEIP", 8},
 // This is a historic abbreviation and should not be confused with the use of
  // SPUR to mean Starting Point URL Referer
               {"SPUR", "StartingPointURLRefresh", 4},
               {"COOKIENAME", "LoginCookieName", 10},
               {NULL, NULL}
    };
    char *cmd;
    enum { CLUSTERNO, CLUSTERYES, CLUSTERBAD } isCluster;
    static BOOL gotName = 0;
    BOOL stopHere = FALSE;
    BOOL loginportsslCalled = 0;

    MD5_CTX md5Context;
    unsigned char md5Digest[16];

    // lets wsKey know the config.txt has been read.
    wsKeyReadConfig = 1;

    MD5_Init(&md5Context);
    MD5_Update(&md5Context, (unsigned char *)EZPROXYVERSION, strlen(EZPROXYVERSION));

    ReadConfigEnvironment(NULL, nameOnly);
    ReadConfigInterface(NULL, nameOnly);

    ReadConfigHTTPMethod("GET", TRUE);
    ReadConfigHTTPMethod("POST", TRUE);
    ReadConfigHTTPMethod("HEAD", TRUE);
    ReadConfigHTTPMethod("OPTIONS", TRUE);

    isCluster = CLUSTERNO;
    optionCookie = optionSingleSession ? OCNONE : OCALL;
    /* optionCookie = ! optionSingleSession; */
    optionRedirectPatch = 0;
    optionSendNoCache = 0;
    optionHideEZproxy = 0;
    optionXForwardedFor = 0;
    optionProxyFtp = 0;
    optionMetaEZproxyRewriting = 0;
    optionUTF16 = 0;
    optionLawyeePatch = 0;
    proxyHost = NULL;
    proxyPort = 0;
    proxyAuth = NULL;
    proxySslHost = NULL;
    proxySslPort = 0;
    proxySslAuth = NULL;
    sslIdx = 0;
    memset(&dbvars, 0, sizeof(dbvars));
    lastPattern = nextPattern = NULL;
    struct stat statBuf;

    // each pass we want to reset this to 0
    cMimeFilters = 0;

    // default cipher suite
    //  EZproxy 6.2
    //  ReadConfigSetString(&SSLcipherSuite, "HIGH:MEDIUM:!ADH:!aNULL:!LOW:!EXP:!SSLv2:@STRENGTH");
    //  EZproxy 6.3
    //  ReadConfigSetString(&SSLcipherSuite, "ALL:!EXPORT:!LOW:!aNULL:!eNULL:!SSLv2:!RC4");
    //  EZproxy 7.2
    ReadConfigSetString(&SSLCipherSuiteInbound,
                        "ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256");
    ReadConfigSetString(&SSLCipherSuiteOutbound, "ALL:!EXPORT:!LOW:!aNULL:!eNULL:!SSLv2:!RC4");

    ReadConfigSetString(&xffRemoteIPHeader, "X-Forwarded-For");

    if (debugLevel > 0)
        Log("Reading " EZPROXYCFG " nameOnly=%d", nameOnly);

    /* Start the group mask at 1, the value for the default group */

    if (nameOnly)
        /* We count the number of peer entries during the nameOnly pass */
        cPeers = 0;
    else {
        struct DISPATCH *dp;

        /* Create the default group */
        FindGroup("Default", 0, 1);

        /* Initialize groups for dispatch table */
        for (dp = loginDispatchTable; dp->cmd; dp++) {
            if (dp->groups) {
                dp->gm = MakeGroupMask(NULL, dp->groups, 1, '+');
            }
        }
        gmAdminAudit = MakeGroupMask(NULL, GROUP_ADMIN_AUDIT, 1, '+');
        gmAdminDecryptVar = MakeGroupMask(NULL, GROUP_ADMIN_DECRYPTVAR, 1, '+');
        gmAdminFiddler = MakeGroupMask(NULL, GROUP_ADMIN_FIDDLER, 1, '+');
        gmAdminGroups = MakeGroupMask(NULL, GROUP_ADMIN_GROUPS, 1, '+');
        gmAdminIdentifier = MakeGroupMask(NULL, GROUP_ADMIN_IDENTIFIER, 1, '+');
        gmAdminIntrusion = MakeGroupMask(NULL, GROUP_ADMIN_INTRUSION, 1, '+');
        gmAdminLDAP = MakeGroupMask(NULL, GROUP_ADMIN_LDAP, 1, '+');
        gmAdminMessages = MakeGroupMask(NULL, GROUP_ADMIN_MESSAGES, 1, '+');
        gmAdminRestart = MakeGroupMask(NULL, GROUP_ADMIN_RESTART, 1, '+');
        gmAdminSecurity = MakeGroupMask(NULL, GROUP_ADMIN_SECURITY, 1, '+');
        gmAdminShibboleth = MakeGroupMask(NULL, GROUP_ADMIN_SHIBBOLETH, 1, '+');
        gmAdminSSLUpdate = MakeGroupMask(NULL, GROUP_ADMIN_SSL_UPDATE, 1, '+');
        gmAdminSSLView = MakeGroupMask(NULL, GROUP_ADMIN_SSL_VIEW, 1, '+');
        gmAdminStatusUpdate = MakeGroupMask(NULL, GROUP_ADMIN_STATUS_UPDATE, 1, '+');
        gmAdminStatusView = MakeGroupMask(NULL, GROUP_ADMIN_STATUS_VIEW, 1, '+');
        gmAdminToken = MakeGroupMask(NULL, GROUP_ADMIN_TOKEN, 1, '+');
        gmAdminUsage = MakeGroupMask(NULL, GROUP_ADMIN_USAGE, 1, '+');
        gmAdminUsageLimits = MakeGroupMask(NULL, GROUP_ADMIN_USAGELIMITS, 1, '+');
        gmAdminUser = MakeGroupMask(NULL, GROUP_ADMIN_USER, 1, '+');
        gmAdminVariables = MakeGroupMask(NULL, GROUP_ADMIN_VARIABLES, 1, '+');
        gmAdminAny = GroupMaskNew(0);
        struct GROUP *g;
        for (g = groups; g; g = g->next) {
            if (IsAnAdminGroup(g)) {
                GroupMaskOr(gmAdminAny, g->gm);
            }
        }

        LoggedInGroups();

        if (stat(SECURITYDIR, &statBuf) < 0 && errno == ENOENT) {
            SetEUidGid();
            if (!UUmkdir(SECURITYDIR))
                PANIC;
            ResetEUidGid();
        }
        sqlite3_temp_directory = sqlite3_mprintf("%s", sqliteTmpDir ? sqliteTmpDir : SECURITYDIR);
        ReadConfigSetString(&sqliteTmpDir, NULL);
    }

    currentLineNum = 0;

    gm = GroupMaskDefault(NULL);
    refererGm = GroupMaskNew(0);

    if (gotName == 0) {
        memset(myName, 0, sizeof(myName));
        if (result = gethostname(myName, sizeof(myName) - 1) == 0) {
            AllLower(myName);
            if (myHostEnt = gethostbyname(myName)) {
                hostAddr = myHostEnt->h_addr_list[0];
                hostAddrLen = myHostEnt->h_length;
                if (myHostEnt = gethostbyaddr(hostAddr, hostAddrLen, myHostEnt->h_addrtype)) {
                    StrCpy3(myName, myHostEnt->h_name, sizeof(myName));
                    AllLower(myName);
                } else {
                    if (debugLevel > 20) {
                        LogBinaryPacket("Can't gethostbyaddr with address[0]",
                                        (unsigned char *)hostAddr, hostAddrLen);
                    }
                }
            } else {
                if (debugLevel > 20) {
                    Log("Can't gethostbyname with %s", myName);
                }
            }
        } else {
            if (debugLevel > 20) {
                Log("Can't gethostname, result %" PRIdMAX "", (intmax_t)result);
            }
        }
        myNameLen = strlen(myName);
        gotName = 1;
        if (debugLevel > 20) {
            Log("Got system hostname %s", myName);
            LogBinaryPacket("Got system address[0]", (unsigned char *)hostAddr, hostAddrLen);
        }
    }
    if (debugLevel > 20) {
        char ipBuffer[INET6_ADDRSTRLEN];
        ipToStr(&ipInterface, ipBuffer, sizeof ipBuffer);
        Log("hostname %s", myName);
        Log("ipInterface addr %s", ipBuffer);
    }

    configActiveIdx = 0;
    currentFilenameListIndex = 0;
    if (!(configFilenameList[currentFilenameListIndex] = strdup(EZPROXYCFG)))
        PANIC;
    configFilenameListMax = 1;

    currentFilename = configFilenameList[currentFilenameListIndex];
    currentLineNum = 0;
    lineNext = line;
    lineRemain = sizeof(line) - 1;

    /* Happens only at startup, so there will be a file descriptor available to fopen */
    currentFILE = fopen(currentFilename, "r");
    if (currentFILE == NULL) {
        if (nameOnly)
            return;
        Log("Unable to start since unable to open configuration file %s", currentFilename);
        AbortFatalConfig();
    }

    /*  printf("Proceeding\n"); */

    /* This must be reset before each pass over this file to guarantee consistent behavior */
    mSessionLife = DEFSESSIONLIFE;

    if (logSPUs == NULL) {
        if (!(logSPUs = (struct LOGSPU *)calloc(1, sizeof(*logSPUs))))
            PANIC;
        if (!(logSPUs->filename = strdup(LOGFILE)))
            PANIC;
        if (!(logSPUs->format = strdup(LOGFORMAT)))
            PANIC;
        logSPUs->file = NULL;
    }

    for (;;) {
        if (stopHere || currentFILE == NULL ||
            ((fgets(lineNext, lineRemain, currentFILE) == NULL) && (lineNext == line))) {
            if (currentFILE) {
                fclose(currentFILE);
            }
            if (configActiveIdx == 0) {
                break;
            }
            configActiveIdx--;
            currentFILE = saveFILE[configActiveIdx];
            currentLineNum = saveLineNum[configActiveIdx];
            currentFilenameListIndex = saveFilenameListIndex[configActiveIdx];
            currentFilename = configFilenameList[currentFilenameListIndex];
            lineNext = line;
            lineRemain = sizeof(line);
            continue;
        }

        if (ezproxyCfgDigest[0] == 0) {
            MD5_Update(&md5Context, (unsigned char *)line, strlen(line));
        }

        currentLineNum++;

        Trim(lineNext, TRIM_TRAIL);
        if (debugLevel > 0) {
            Log("%2d %10d %s", currentFilenameListIndex + 1, currentLineNum, lineNext);
        }
        Trim(lineNext, TRIM_LEAD);

        // handle continuation lines, aka lines terminated with a '\'
        lineNext = strchr(line, 0);
        if (lineNext > line && *(lineNext - 1) == '\\') {
            lineNext--;
            *lineNext = 0;
            lineRemain = sizeof(line) - (lineNext - line) - 1;
            if (lineRemain > 0)
                continue;
        }

        lineNext = line;
        lineRemain = sizeof(line) - 1;

        if (line[0] == 0 || line[0] == '#')
            continue;
        /*
        if (islower(line[0]))
            line[0] = toupper(line[0]);
        if (islower(line[1]))
            line[1] = toupper(line[1]);
        */
        /*
        if (nameOnly && (line[0] != 'N' && line[0] != 'P' && line[0] != 'M' && line[0] != 'F'))
            continue;
        if (!nameOnly && line[0] == 'M' && line[1] != 'L')
            continue;
        for (arg = line + 1; *arg && !IsWS(*arg); arg++)
            ;
        */

        ParseCommand(line, &cmdLen, &arg);
        cmd = line;

        /* process synonyms */
        for (rp = remapcmd; rp->oldcmd; rp++)
            if (cmdLen == strlen(rp->oldcmd) && cmdLen >= rp->min &&
                strnicmp(cmd, rp->oldcmd, cmdLen) == 0) {
                cmd = rp->newcmd;
                cmdLen = strlen(rp->newcmd);
                break;
            }

        if (MatchCommand(cmd, cmdLen, "STOP", 0) && stricmp(arg, "HERE") == 0) {
            stopHere = TRUE;
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "XDEBUG", 1)) {
            ReadConfigDebug(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "IncludeFile", 8)) {
            if ((configActiveIdx + 1) == MAXINCLUDEFILE) {
                Log("IncludeFile directives may only be nested %d deep; %s not included",
                    configActiveIdx, arg);
                if (!nameOnly) {
                    AbortFatalConfig();
                }
            }
            saveFILE[configActiveIdx] = currentFILE;
            saveLineNum[configActiveIdx] = currentLineNum;
            saveFilenameListIndex[configActiveIdx] = currentFilenameListIndex;
            configActiveIdx++;
            for (currentFilenameListIndex = 0; currentFilenameListIndex < configFilenameListMax;
                 currentFilenameListIndex++) {
                if (strcmp(configFilenameList[currentFilenameListIndex], arg) == 0) {
                    break;
                }
            }
            if (currentFilenameListIndex == MAXINCLUDEFILELIST) {
                Log("Unable to IncludeFile %s because all %d entries are filled.", arg,
                    currentFilenameListIndex);
                if (!nameOnly) {
                    AbortFatalConfig();
                }
            }
            if (currentFilenameListIndex == configFilenameListMax) {
                configFilenameListMax++;
                if (!(configFilenameList[currentFilenameListIndex] = strdup(arg)))
                    PANIC;
            }
            currentFilename = configFilenameList[currentFilenameListIndex];
            currentLineNum = 0;
            lineNext = line;
            lineRemain = sizeof(line) - 1;
            currentFILE = fopen(currentFilename, "r");
            if (currentFILE == NULL) {
                // When we go around the loop, it will notice currentFILE is NULL and back us out
                // to the previous file
                if (!nameOnly) {
                    Log("Unable to open configuration file %s", currentFilename);
                    AbortFatalConfig();
                }
            }
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "RunAs", 0)) {
            ReadConfigRunAs(nameOnly, arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "umask", 0)) {
            ReadConfigUmask(arg, nameOnly);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "environment", 0)) {
            ReadConfigEnvironment(arg, nameOnly);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "MALLOC_CHECK_", 0)) {
            FreeThenNull(mallocCheck);
            if (arg && *arg) {
                if (!(mallocCheck = malloc(20 + strlen(arg))))
                    PANIC;
                sprintf(mallocCheck, "MALLOC_CHECK_=%s", arg);
            }
        }

        /* Check to see if this is special pre-scan for runas; if so, ignore all else */
        if (nameOnly == 2)
            continue;

        if (MatchCommand(cmd, cmdLen, "SQLiteTmpDir", 0)) {
            if (nameOnly) {
                ReadConfigSQLiteTmpDir(arg);
            }
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "PidFile", 0)) {
            ReadConfigPidFile(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "LoginCookieName", 0)) {
            if (nameOnly)
                ReadConfigLoginCookieName(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "LoginCookieSameSite", 0)) {
            if (nameOnly)
                ReadConfigLoginCookieSameSite(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "PrivateComputerCookieName", 0)) {
            if (nameOnly)
                ReadConfigPrivateComputerCookieName(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "LoginCookieDomain", 0)) {
            if (nameOnly)
                ReadConfigLoginCookieDomain(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "NAME", 1)) {
            ReadConfigName(arg, nameOnly);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "HTTPMethod", 0)) {
            ReadConfigHTTPMethod(arg, FALSE);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "HTTPHeader", 0)) {
            ReadConfigHTTPHeader(arg, nameOnly);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "Proxy", 1)) {
            ReadConfigProxy(arg, nameOnly);
            continue;
        }

        /* Note that these appears after PROXY to avoid name conflict */

        if (MatchCommand(cmd, cmdLen, "ProxySSL", 0)) {
            ReadConfigProxySsl(arg, nameOnly);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "ProxyUrlPassword", 0)) {
            ReadConfigProxyUrlPassword(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "PROXYHOSTNAMEEDIT", 1)) {
            ReadConfigProxyHostnameEdit(arg, nameOnly);
            continue;
        }

        if ((result = ReadConfigMax(cmd, cmdLen, arg, nameOnly))) {
            if (result == 2)
                goto BadOption;
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "FirstPort", 1)) {
            ReadConfigFirstPort(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "LoginPortSSL", 10)) {
            loginportsslCalled = 1;
            if (UUSslEnabled()) {
                ReadConfigLoginPort(arg, nameOnly, 1);
            }
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "SSLCipherSuite", 0)) {
            if (loginportsslCalled) {
                Log("WARNING: SSLCipherSuite must appear prior to LoginPortSSL.  Ignoring %s", arg);
                continue;
            }
            ReadConfigSSLCipherSuite(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "SSLOpenSSLConfCmd", 0)) {
            // check if the LoginPortSSL directive is already set
            if (loginportsslCalled) {
                Log("WARNING: SSLOpenSSLConfCmd must appear prior to LoginPortSSL.  Ignoring %s",
                    arg);
                continue;
            }
            ReadConfigSSLOpenSSLConfCmd(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "SSLHonorCipherOrder", 0)) {
            // check if the LoginPortSSL directive is already set
            if (loginportsslCalled) {
                Log("WARNING: SSLHonorCipherOrder must appear prior to LoginPortSSL.");
                continue;
            }
            ReadConfigSSLHonorCipherOrder(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "LoginPort", 1)) {
            ReadConfigLoginPort(arg, nameOnly, 0);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "SkipPort", 0)) {
            ReadConfigLoginPort(arg, nameOnly, USESSLSKIPPORT);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "LogFile", 0)) {
            if (nameOnly)
                ReadConfigLogFile(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "MinProxy", 0)) {
            ReadConfigMinProxy(arg, nameOnly);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "SciFinder", 0)) {
            ReadConfigSciFinder(arg, nameOnly);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "MutexTimeout", 0)) {
            ReadConfigMutexTimeout(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "MinProxyDelay", 0)) {
            ReadConfigMinProxyDelay(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "Interface", 3)) {
            ReadConfigInterface(arg, nameOnly);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "IntrusionAPI", 0)) {
            if (!nameOnly) {
                ReadConfigIntrusionAPI(arg, currentFilename, currentLineNum);
            }
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "IntruderIPAttempts", 0)) {
            ReadConfigIntruderIPAttempts(arg, currentFilename, currentLineNum);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "IntruderLog", 0)) {
            ReadConfigIntruderLog(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "IntruderReject", 0)) {
            ReadConfigIntruderReject(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "IntruderTimeout", 0)) {
            ReadConfigIntruderTimeout(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "IntruderUserAttempts", 0)) {
            ReadConfigIntruderUserAttempts(arg, currentFilename, currentLineNum);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "Cluster", 1)) {
            if (isCluster != CLUSTERNO) {
                if (nameOnly)
                    Log("CLUSTER statement may only appear once in " EZPROXYCFG);
            } else {
                isCluster = CLUSTERYES;
                if (nameOnly)
                    mPeers++;
                else {
                    ReadConfigPeer(arg);
                    if (cPeers == 0)
                        isCluster = CLUSTERBAD;
                }
            }
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "AutoLoginIPBanner", 0)) {
            if (!nameOnly)
                ReadConfigAutoLoginIPBanner(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "ExcludeIPBanner", 0)) {
            if (!nameOnly)
                ReadConfigExcludeIPBanner(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "AutoLoginIP", 1)) {
            if (nameOnly)
                mIpTypes += ReadConfigCommaCountPlus1(arg);
            else
                ReadConfigIpTypes(arg, IPAAUTO, currentFilename, currentLineNum);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "ExcludeIP", 1)) {
            if (nameOnly)
                mIpTypes += ReadConfigCommaCountPlus1(arg);
            else
                ReadConfigIpTypes(arg, IPALOCAL, currentFilename, currentLineNum);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "IncludeIP", 1)) {
            if (nameOnly)
                mIpTypes += ReadConfigCommaCountPlus1(arg);
            else
                ReadConfigIpTypes(arg, IPAREMOTE, currentFilename, currentLineNum);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "RejectIP", 1)) {
            if (nameOnly)
                mRejectIpTypes += ReadConfigCommaCountPlus1(arg);
            else
                ReadConfigIpTypes(arg, IPAREJECT, currentFilename, currentLineNum);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "WhitelistIP", 1) ||
            MatchCommand(cmd, cmdLen, "AllowIP", 0)) {
            if (nameOnly) {
                mWhitelistIPs += ReadConfigCommaCountPlus1(arg);
            } else {
                ReadConfigWhitelistIP(arg, currentFilename, currentLineNum);
            }
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "PDFRefresh", 4)) {
            if (nameOnly)
                mPdfRefreshes++;
            else
                ReadConfigPdfRefresh(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "PDFRefreshPre", 0)) {
            if (!nameOnly)
                ReadConfigPdfRefreshPre(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "PDFRefreshPost", 0)) {
            if (!nameOnly)
                ReadConfigPdfRefreshPost(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "STARTINGPOINTURLREFRESH", 0)) {
            if (nameOnly)
                mStartingPointUrlRefreshes++;
            else
                ReadConfigStartingPointUrlRefresh(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "ANONYMOUSURL", 0)) {
            if (nameOnly)
                mAnonymousUrls++;
            else
                ReadConfigAnonymousUrl(arg, currentFilename, currentLineNum);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "COOKIEFILTER", 0)) {
            ReadConfigCookieFilter(arg, nameOnly, CFPBLOCK);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "LOGFILTER", 9)) {
            if (nameOnly)
                mLogFilters++;
            else
                ReadConfigLogFilter(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "ServerHeader", 0)) {
            ReadConfigServerHeader(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "HANAME", 0)) {
            ReadConfigHAName(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "HAPEER", 0)) {
            ReadConfigHAPeer(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "PEER", 1)) {
            if (isCluster != CLUSTERYES) {
                if (!nameOnly) {
                    Log("PEER statement may not appear before valid CLUSTER statement");
                    Log("Terminating " EZPROXYNAMEPROPER " to avoid possible cluster corruption");
                    AbortFatalConfig();
                }
                continue;
            }
            if (nameOnly)
                mPeers++;
            else
                ReadConfigPeer(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "COOKIE", 0)) {
            if (!nameOnly)
                ReadConfigCookie(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "TITLE", 1)) {
            if (nameOnly) {
                mDatabases++;
                continue;
            }
            ReadConfigTitle(arg);
            continue;
        }

        // all databases get the default mime filters by default
        if (!nameOnly)
            ReadConfigMimeFilterAddDefaults();

        // user defined mime filters
        if (MatchCommand(cmd, cmdLen, "MIMEFILTER", 0)) {
            cMimeFilters++;  // reset to 0 on each pass
            if (nameOnly)
                continue;

            ReadConfigMimeFilter(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "ByteServe", 0)) {
            if (!nameOnly) {
                ReadConfigByteServe(arg);
            }
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "Referer", 0)) {
            ReadConfigReferer(arg, nameOnly, currentFilename, currentLineNum);
            continue;
        }

        /* ReadConfigOption moved to nameOnly processing to allow OPTION SINGLESESSION in first pass
           to mandate OPTION NOCOOKIE in second pass */
        if (MatchCommand(cmd, cmdLen, "OPTION", 1)) {
            ReadConfigOption(arg, nameOnly, configActiveIdx == 0 && currentLineNum == 1);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "RemoteTimeout", 0)) {
            ReadConfigRemoteTimeout(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "ReceiveBufferSize", 0)) {
            ReadConfigReceiveBufferSize(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "SendBufferSize", 0)) {
            ReadConfigSendBufferSize(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "ClientTimeout", 0)) {
            ReadConfigClientTimeout(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "BINARYTIMEOUT", 13)) {
            ReadConfigBinaryTimeout(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "DNS", 0)) {
            ReadConfigDns(arg, nameOnly, currentFilename, currentLineNum);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "RerouteTo", 0)) {
            ReadConfigReroute(nameOnly, arg, "To");
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "RerouteHost", 0)) {
            ReadConfigReroute(nameOnly, arg, "Host");
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "RerouteDomain", 0)) {
            ReadConfigReroute(nameOnly, arg, "Domain");
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "ChargeSetLatency", 0)) {
            ReadConfigChargeSetLatency(arg, nameOnly);
            continue;
        }

        if (nameOnly)
            continue;

        if (MatchCommand(cmd, cmdLen, "CacheControlMaxAge", 0)) {
            ReadConfigCacheControlMaxAge(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "RemoteIPHeader", 0)) {
            ReadConfigRemoteIPHeader(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "RemoteIPInternalProxy", 0)) {
            ReadConfigRemoteIP(arg, TRUE, currentFilename, currentLineNum);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "RemoteIPTrustedProxy", 0)) {
            ReadConfigRemoteIP(arg, FALSE, currentFilename, currentLineNum);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "LBPeer", 0)) {
            ReadConfigLBPeer(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "ShibbolethDisable", 0)) {
            ReadConfigShibbolethDisable(arg);
            continue;
        }

        if (cmdLen <= 6 && strnicmp(cmd, "DbVar", 5) == 0) {
            ReadConfigDbVar(cmd, arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "KeepAliveTimeout", 0)) {
            ReadConfigKeepAliveTimeout(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "SocketBacklog", 0)) {
            Log("SocketBacklog is incorrect; e-mail " EZPROXYHELPEMAIL " for help");
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "Location", 0)) {
            ReadConfigLocation(arg, currentFilename, currentLineNum);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "LoginSocketBacklog", 0)) {
            ReadConfigLoginSocketBacklog(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "HostSocketBacklog", 0)) {
            ReadConfigHostSocketBacklog(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "DNSSocketBacklog", 0)) {
            ReadConfigDNSSocketBacklog(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "DenyIfRequestHeader", 0)) {
            ReadConfigDenyIfRequestHeader(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "ExtraLoginCookie", 0)) {
            ReadConfigExtraLoginCookie(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "AthensDspId", 0)) {
            ReportAthensDiscontinued("AthensDspId");
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "SessionKeySize", 0)) {
            ReadConfigSessionKeySize(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "ShibbolethWAYF", 0)) {
            ReadConfigShibbolethWAYF(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "ShibbolethProviderId", 0)) {
            ReadConfigShibbolethProviderId(arg);
            continue;
        }

        /* Allow ShibbolethMetadata, ShibbolethSite or ShibbolethSites */
        if (MatchCommand(cmd, cmdLen, "ShibbolethMetadata", 0) ||
            MatchCommand(cmd, cmdLen, "ShibbolethSites", 14)) {
            ReadConfigShibbolethMetadata(arg, nameOnly);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "TokenSalt", 0)) {
            ReadConfigTokenSalt(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "Charset", 0)) {
            ReadConfigCharset(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "P3P", 0)) {
            ReadConfigP3P(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "NeverProxy", 0)) {
            ReadConfigRedirectSafeOrNeverProxy(arg, FALSE);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "RedirectSafe", 0)) {
            ReadConfigRedirectSafeOrNeverProxy(arg, TRUE);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "Account", 3)) {
            ReadConfigAccount(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "Audit", 0)) {
            ReadConfigAudit(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "AuditPurge", 0)) {
            ReadConfigAuditPurge(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "BOOKS24X7SITE", 0)) {
            ReadConfigBooks24X7Site(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "CASServiceURL", 0)) {
            ReadConfigCASServiceURL(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "LocalWSKey", 0)) {
            ReadConfigLocalWSKey(arg, gm);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "ChargeForceSeizure", 0)) {
            ReadConfigChargeForceSeizure(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "EBLSECRET", 0)) {
            ReadConfigEBLSecret(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "ebrarySite", 0)) {
            ReadConfigEbrarySite(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "OverDriveSite", 0)) {
            ReadConfigOverDriveSite(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "SPUEdit", 0)) {
            ReadConfigSPUEdit(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "SPUEditVar", 0)) {
            ReadConfigSPUEditVar(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "SSO", 0)) {
            ReadConfigSSO(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "SSOUsername", 0)) {
            ReadConfigSSOUsername(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "EncryptVar", 0)) {
            ReadConfigEncryptVar(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "ConnectWindow", 0)) {
            ReadConfigConnectWindow(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "CPIPSecret", 0)) {
            ReadConfigCPIPSecret(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "DESCRIPTION", 2)) {
            ReadConfigDescription(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "DOMAIN", 1)) {
            ReadConfigDomain(0, arg, 0);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "DOMAINJAVASCRIPT", 1)) {
            ReadConfigDomain(1, arg, 0);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "AddHeader", 0)) {
            ReadConfigAddHeader(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "FormSelect", 0)) {
            ReadConfigFormSelect(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "FormSubmit", 7)) {
            ReadConfigFormSubmit(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "FormVariable", 7)) {
            ReadConfigFormVariable(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "MyiLibrary", 0)) {
            ReadConfigMyiLibrary(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "NetLibrary", 0)) {
            ReadConfigNetLibrary(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "FMG", 0)) {
            ReadConfigFMG(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "FIND", 4)) {
            ReadConfigFind(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "Replace", 1)) {
            ReadConfigReplace(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "Group", 1)) {
            ReadConfigGroup(arg, gm);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "Gartner", 0)) {
            ReadConfigGartner(arg, gm);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "AthensResource", 0)) {
            ReportAthensDiscontinued("AthensResource");
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "Host", 1)) {
            ReadConfigDomain(0, arg, 1);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "HostJavaScript", 1)) {
            ReadConfigDomain(1, arg, 1);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "LoginMenu", 1)) {
            ReadConfigLoginMenu(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "LogFormat", 1)) {
            ReadConfigLogFormat(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "LOGSPU", 0)) {
            ReadConfigLogSPU(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "LogTag", 0)) {
            ReadConfigLogTag(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "MAILFROM", 0)) {
            ReadConfigMailFrom(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "MAILSERVER", 0)) {
            ReadConfigMailServer(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "MAILTO", 0)) {
            ReadConfigMailTo(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "METAFIND", 1)) {
            ReadConfigMetaFind(arg, nameOnly);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "SSLCert", 1)) {
            ReadConfigSSLCert(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "TITLE", 1)) {
            ReadConfigTitle(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "TOKENKEY", 0)) {
            ReadConfigTokenKey(nameOnly, arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "TOKENSIGNATUREKEY", 0)) {
            ReadConfigTokenSignatureKey(nameOnly, arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "URLAPPENDENCODED", 0)) {
            ReadConfigURL("AE", arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "URLREDIRECTAPPENDENCODED", 0)) {
            ReadConfigURL("RAE", arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "URLAPPEND", 4)) {
            ReadConfigURL("A", arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "URLREDIRECTAPPEND", 12)) {
            ReadConfigURL("RA", arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "URLREDIRECT", 4)) {
            ReadConfigURL("R", arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "URL", 1)) {
            ReadConfigURL("", arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "VALIDATE", 1)) {
            ReadConfigValidate(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "RADIUSTIMEOUT", 7)) {
            radiusTimeout = atoi(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "RADIUSRETRY", 0)) {
            radiusRetry = atoi(arg);
            if (radiusRetry <= 0)
                radiusRetry = 1;
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "OpenURLResolver", 0)) {
            ReadConfigOpenURLResolver(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "AllowVars", 0)) {
            ReadConfigAllowVars(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "UsageLimit", 0)) {
            ReadConfigUsageLimit(arg, currentFilename, currentLineNum);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "AddUserHeader", 0)) {
            ReadConfigAddUserHeader(arg);
            continue;
        }

        if (MatchCommand(cmd, cmdLen, "Identifier", 0)) {
            ReadConfigIdentifier(arg, currentFilename, currentLineNum);
            continue;
        }

    BadOption:
        cmd[64] = 0;
        Log("Unrecognized %s(%d): %s", currentFilename, currentLineNum, cmd);
    }

    if (debugLevel > 128) {
        Log("");
        Log("%10s %s", "FILE NUM", "FILE NAME");
        for (i = 0; i < configFilenameListMax; i++) {
            Log("%10d %s", i + 1, configFilenameList[i]);
        }
        Log("%10s %s", "CWD", getcwd(line, sizeof(line) - 1));
    }
    for (i = 0; i < configFilenameListMax; i++) {
        FreeThenNull(configFilenameList[i]);
    }

    if (shibbolethSites && shibbolethWAYF && shibbolethAvailable12 == SHIBBOLETHAVAILABLEOFF) {
        /* Preserve classic Shibboleth 1.2 handling if ShibbolethWAYF defined but
           no ShibbolethProviderID defined
        */
        shibbolethAvailable12 = SHIBBOLETHAVAILABLEON;
        if (shibbolethProviderId == NULL) {
            char *me = (myUrlHttps ? myUrlHttps : myUrlHttp);
            if (!(shibbolethProviderId = malloc(strlen(me) + 16)))
                PANIC;
            sprintf(shibbolethProviderId, "%s/shibboleth", me);
        }
    }

    if (nameOnly == 2)
        return;

    ReadConfigCookieFilter(requireAuthenticateCookieName, nameOnly, CFPBLOCKMANDATORY);
    ReadConfigCookieFilter(refererCookieName, nameOnly, CFPBLOCKMANDATORY);
    ReadConfigCookieFilter(CSRF_TOKEN_NAME, nameOnly, CFPBLOCKMANDATORY);

    /* Set up a default login port if none defined explicitly */
    /* Set up default IP interface if no one else did */
    ReadConfigLoginPort(NULL, nameOnly, 0);
    if (optionProxyType == PTHOST && NumericHost(myName)) {
        optionProxyType = PTPORT;
        if (nameOnly)
            Log("PROXYTYPEHOST cannot be used with a numeric host name");
    }
    if (!nameOnly) {
        ReadConfigAdvance();
        ReadConfigSetProxyHostnames();
        ResizeAllGroupMasks();
        /* Provide compatibility with old format, insuring that if IntruderTimeout comes after
           IntruderAttempts, it will still work as before
        */
        if (intruderAttemptsClassic && intruderIPAttempts && intruderIPAttempts->next == NULL)
            intruderIPAttempts->intruderInterval = intruderIPAttempts->intruderExpires =
                intruderTimeout;
        if (optionProxyType == PTPORT && optionRefererInHostname) {
            Log("Option RefereInHostname has no effect in proxy by port");
            optionRefererInHostname = 0;
        }
    }

    if (ezproxyCfgDigest[0] == 0) {
        MD5_Final(md5Digest, &md5Context);
        MD5DigestToHex(ezproxyCfgDigest, md5Digest, 0);
    }

    if ((pdfRefreshPre == NULL) ^ (pdfRefreshPost == NULL)) {
        Log("%s ignored since %s was omitted", pdfRefreshPre ? "PDFRefreshPre" : "PDFRefreshPost",
            pdfRefreshPre ? "PDFRefreshPost" : "PDFRefreshPre");
        ReadConfigSetString(&pdfRefreshPre, NULL);
        ReadConfigSetString(&pdfRefreshPost, NULL);
    }

    if (lbPeers && myLbPeer == NULL) {
        Log("No LBPeer matched an IP interface on this system; all LBPeer ignored");
        /* Tacky to just through the linked list away, but it doesn't really matter */
        lbPeers = NULL;
    }

    ReadConfigValidateFree();

    ReadConfigSSLOpenSSLConfCmd(NULL);

    GroupMaskFree(&gm);
    GroupMaskFree(&refererGm);

    if (nameOnly == 0 && validateReferersRequired) {
        ValidateReferers();
    }
}
