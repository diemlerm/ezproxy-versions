#define __UUFILE__ "registry.c"

#include "common.h"
#include "uuxml.h"
#include "html.h"
#include "registry.h"

// http://worldcat.org/webservices/registry/content/groups/5683
// http://worldcat.org/webservices/registry/enhancedContent/Institutions/207

xmlParserCtxtPtr RegistryGet(char *base, char *service) {
    xmlParserCtxtPtr ctxt = NULL;
    xmlXPathContextPtr xpathCtx = NULL;
    char *fn = NULL;

    if (!(fn = malloc(strlen(base) + strlen(service) + 1)))
        PANIC;

    sprintf(fn, "%s%s", base, service);

    ctxt = GetHTML(fn, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0);
    if (ctxt == NULL) {
        Log("Registry server did not respond for %s", fn);
        goto Finished;
    }

    xpathCtx = xmlXPathNewContext(ctxt->myDoc);
    if (xpathCtx == NULL) {
        Log("Error: unable to create new XPath context");
        goto Finished;
    }

Finished:
    FreeThenNull(fn);

    if (xpathCtx) {
        xmlXPathFreeContext(xpathCtx);
        xpathCtx = NULL;
    }

    UUxmlFreeParserCtxtAndDoc(&ctxt);

    return NULL;
}
