#ifndef __USRIII_H__
#define __USRIII_H__

#include "common.h"

/* Returns 0 if valid, 1 if not, 3 if unable to connect to iii host */
int FindUserIii(struct TRANSLATE *t,
                struct USERINFO *uip,
                char *file,
                int f,
                struct FILEREADLINEBUFFER *frb,
                struct sockaddr_storage *pInterface);

#endif  // __USRIII_H__
