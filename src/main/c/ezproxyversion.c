#include "ezproxyversion.h"

/* Set this to the date of the build in ISO 8601 form as in POSIX's date +'%Y-%m-%dT%H:%M:%S%z'
 * command.  This */
/* helps identify the build if it gets out into the wild. */
/* This static version is only used by the Visual C++ build process */
#ifndef EZPROXYBUILD
#define EZPROXYBUILD "2012-07-18T07:00:00Z"
#endif

#define EZPROXYVERSIONLONG                                                \
    EZPROXYNAMEPROPER " " EZPROXYVERSIONSHORT " [SOURCE:" EZSOURCEVERSION \
                      "]"                                                 \
                      " [" EZPOS "] [" EZPROXYBUILD "]"

char *EZPROXYVERSION = EZPROXYVERSIONLONG;
