#ifndef __UUSTRING_H__
#define __UUSTRING_H__

#include "common.h"

#define TRIM_LEAD 1
#define TRIM_TRAIL 2
#define TRIM_COMPRESS 4
#define TRIM_COLLAPSE 8
#define TRIM_SEMI 16
#define TRIM_CTRL 32
#define TRIM_CRTOLF 64
#define TRIM_PEM_CLEANUP 128
#define TRIM_DECIMAL 256
#define TRIM_LDAP_DN 512
#define TRIM_TABTOSPACE 1024
#define TRIM_NBSP 2048

#define MAXARRAYOFSTRINGSIDX 31

#define SEPARATE_INT_BUFFER_LEN 32

struct ARRAYOFSTRINGS {
    int count;
    char *value[MAXARRAYOFSTRINGSIDX];
};

void ArrayOfStringsInit(struct ARRAYOFSTRINGS *aos);
int ArrayOfStringsCount(struct ARRAYOFSTRINGS *aos);
void ArrayOfStringsTokenizeString(struct ARRAYOFSTRINGS *aos, char *values, char delim);
char *ArrayOfStringsGetValue(struct ARRAYOFSTRINGS *aos, int idx);
int ArrayOfStringsGetIndex(struct ARRAYOFSTRINGS *aos, char *value);
int ArrayOfStringsGetIIndex(struct ARRAYOFSTRINGS *aos, char *value);
void ArrayOfStringsSetValue(struct ARRAYOFSTRINGS *aos, char *value, int idx);
void ArrayOfStringsInsertValue(struct ARRAYOFSTRINGS *aos, char *value, int idx);
void ArrayOfStringsDeleteValue(struct ARRAYOFSTRINGS *aos, int idx);

BOOL IsWS(char c);
BOOL IsAlpha(char c);
BOOL IsDigit(char c);
BOOL IsAllDigits(const char *s);
BOOL IsOneOrMoreDigits(const char *s);
BOOL IsAllAlphameric(const char *s);
BOOL IsGraphic(char c);

BOOL NextKEV(char *s, char **key, char **value, char **next, BOOL dontUnescapeValue);

char *StartsWith(const char *str, const char *starts);
char *StartsIWith(const char *str, const char *starts);
char *EndsWith(const char *str, const char *ends);
char *EndsIWith(const char *str, const char *ends);
char *StartsWithNumeric(const char *str);
char *AtEnd(const char *str);

void Trim(char *s, int flags);
BOOL TrimBr(char *s);
BOOL TrimTags(char *s);
void UnescapeString(char *s);

char *Decode64(char *dst, char *src);
char *DecodeUU64(char *dst, char *src);
char *Encode64(char *dst, char *src);
char *EncodeUU64(char *dst, char *src);
char *Encode64Binary(char *dst, unsigned char *src, size_t srcLen);
size_t Encode64BytesNeeded(size_t len);

char *SkipWS(const char *s);
char *SkipNonWS(const char *s);

char *NextWSArg(char *s);

void AllLower(char *s);
void AllUpper(char *s);

char *StrTok_R(char *s, char *d, char **p);
void ReplaceString(char *start, int origLen, char *replace);
void InsertAtFront(char *s, size_t smax, char *i);

void EraseMemory(char *p, size_t len);
void EraseString(char *p);

char StripISO88591Diacritics(char c);

char *DigestToHex(char *digestHex, unsigned char *digestBinary, size_t digestBinaryLen, BOOL upper);
char *MD5DigestToHex(char *md5DigestHex, unsigned char *md5DigestBinary, BOOL upper);

char *IndefiniteArticle(char *s, BOOL upper);

char *SIfNotOne(int i);
char *EsIfNotOne(int i);
char *HasHave(int i, BOOL upper);
char *IsAre(int i, BOOL upper);
char *WasWere(int i, BOOL upper);
char *ThisThese(int i, BOOL upper);

char *StaticStringCopy(const char *s);
void StrCCat3(char *dst, char c, size_t max);
void StrCpy3(char *dst, char *src, int max);

char *UUStrCpyToWide(char *wide, char *narrow);
char *UUStrDup(const char *str);
char *UUStrRealloc(char *str);

time_t ParseDate(char *sdate);

char *SeparateInt(int n, char *buffer);

#endif  // __UUSTRING_H__
