/**
 * Core module for admin (and non-admin) URLs provided by EZproxy
 *
 * This module contains core routines used by admin (and non-admin) URLs
 * provided by EZproxy.  All of the web URLs supported by EZproxy for user
 * and administrator interaction are implemented in a file named admin***.c.
 * This module holds the dispatcher that links access to these other modules.
 */

#define __UUFILE__ "admin.c"

#include "ezproxyversion.h"
#include "common.h"
#include "uustring.h"

#include "openssl/des.h"

#ifdef WIN32
/* io.h, fcntl.h and share.h needed for _sopen in Win32 */
#include <io.h>
#include <fcntl.h>
#include <share.h>
#include <lm.h>
#endif

#include <sys/stat.h>

#include "usertoken.h"
#include "obscure.h"

#include "adminabout.h"
#include "adminaccountadd.h"
#include "adminaccountreset.h"
#include "adminadmin.h"
#include "adminaudit.h"
#include "adminauth.h"
#include "admincas.h"
#include "adminconflicts.h"
#include "admincookie.h"
#include "admincpip.h"
#include "adminform.h"
#include "adminebrary.h"
#include "adminfiddler.h"
#include "adminfmg.h"
#include "admingartner.h"
#include "admingroups.h"
#include "adminheaders.h"
#include "adminidentifier.h"
#include "adminintrusion.h"
#include "adminip.h"
#include "adminldap.h"
#include "adminlogin.h"
#include "adminlogout.h"
#include "adminmenu.h"
#include "adminmessages.h"
#include "adminmv.h"
#include "adminmygroups.h"
#include "adminnetlibrary.h"
#include "adminnetwork.h"
#include "adminoverdrive.h"
#include "adminproxyurl.h"
#include "adminreboot.h"
#include "adminrestart.h"
#include "adminrobots.h"
#include "adminscifinder.h"
#include "adminsecurity.h"
#include "adminshibboleth.h"
#include "adminsliptest.h"
#include "adminsortable.h"
#include "adminsso.h"
#include "adminssl.h"
#include "adminstatus.h"
#include "admintime.h"
#include "admintoken.h"
#include "admintop.h"
#include "adminunauthorized.h"
#include "adminunknown.h"
#include "adminusage.h"
#include "adminusagelimits.h"
#include "adminuser.h"
#include "adminwebserver.h"

#include "usr.h"
#include "usrfinduser.h"
#include "usrldap2.h"

#include "uuxml.h"
#include "html.h"

#include "wskeylicense.h"

#ifndef WIN32
#include <dirent.h>
#endif

#include "adminuserobject.h"
#include "saml.h"

static int lastRefererKey = 0;

int RefererKey(void) {
    static time_t lastRefererKeyBucket;
    static int refererKey = 0;
    static time_t refererKeyBucket = 0;
    time_t now, bucket;

    UUtime(&now);
    bucket = now / 60;

    UUAcquireMutex(&mActive);
    if (refererKeyBucket != bucket) {
        if (refererKeyBucket + 1 == bucket) {
            lastRefererKey = refererKey;
            lastRefererKeyBucket = refererKeyBucket;
        } else {
            lastRefererKey = 0;
            lastRefererKeyBucket = 0;
        }

        refererKey = RandNumber(999999999) + 1;
        refererKeyBucket = bucket;
    }
    UUReleaseMutex(&mActive);

    return refererKey;
}

BOOL ValidRefererKey(int checkRefererKey) {
    return checkRefererKey == RefererKey() || checkRefererKey == lastRefererKey;
}

/* If we are returning a copy, SnipField removes only the first instance,
   but if we aren't returning anything, SnipField removes all copies
*/

char *GetSnipField(char *query, char *field, BOOL snipField, BOOL returnCopy) {
    size_t fieldLen = strlen(field);
    char *s;
    char *v;
    char *amp;
    char *value = NULL;
    size_t valueLen;

    if (query == NULL)
        return NULL;

    while (*query == '/' || *query == '?')
        query++;

ReSnip:
    for (s = query; s; s = strchr(s, '&')) {
        for (; *s == '&'; s++)
            ;
        if (strnicmp(s, field, fieldLen) != 0)
            continue;
        v = s + fieldLen;
        if (*v != '=')
            continue;
        v++;
        amp = strchr(v, '&');
        if (amp == NULL)
            valueLen = strlen(v);
        else
            valueLen = amp - v;
        if (returnCopy) {
            if (!(value = malloc(valueLen + 1)))
                PANIC;
            if (valueLen)
                memcpy(value, v, valueLen);
            *(value + valueLen) = 0;
            UnescapeString(value);
        }

        if (!snipField)
            return value;

        if (amp)
            StrCpyOverlap(s, amp + 1);
        else {
            if (s == query)
                *s = 0;
            else
                *(s - 1) = 0;
        }
        if (returnCopy)
            break;
        else
            goto ReSnip;
    }

    return value;
}

void WriteField(struct TRANSLATE *t,
                char *label,
                char *name,
                char *value,
                char *error,
                int size,
                int max,
                char *fieldType) {
    struct UUSOCKET *s = &t->ssocket;

    if (value == NULL)
        value = "";

    if (fieldType == NULL)
        fieldType = "text";

    WriteLine(s, "<tr valign=top><td><label for='%s'>%s:</label> </td><td>", name, label);
    WriteLine(s, "<div><input name='%s' id='%s' type='%s' ", name, name, fieldType);
    if (size)
        WriteLine(s, " size='%d'", size);
    if (max)
        WriteLine(s, " maxlength='%d'", max);
    UUSendZ(s, " value='");
    SendQuotedURL(t, value);
    UUSendZ(s, "' /></div>");
    if (error && *error)
        WriteLine(s, "<div class='failure'>%s</div>", error);
    WriteLine(s, "</td></tr>\n");
}

void WriteCheckbox(struct TRANSLATE *t, char *label, char *name, BOOL checked, char *error) {
    struct UUSOCKET *s = &t->ssocket;

    WriteLine(s, "<tr valign=bottom><td>%s: </td><td>", label);
    if (error && *error)
        WriteLine(s, "<div class='failure'>%s</div>", error);
    WriteLine(s, "<div><input name='%s' id='%s' type='checkbox'%s></div></td></tr>", name, name,
              checked ? " checked" : "");
}

char *AddEncodedFieldLimited(char *s, char *f, size_t limit, char *stop) {
    char *e;
    unsigned char x;

    if (s == NULL || f == NULL)
        return s;

    /* If stopping point specified, move back 1 to insure there is room for final null */
    if (stop)
        stop--;

    for (e = strchr(s, 0); *f && limit > 0 && (stop == NULL || e < stop); f++, limit--) {
        x = (unsigned char)(*f & 0xff);
        if (x < ' ')
            continue;
        if (x == ' ')
            *e++ = '+';
        else if (IsDigit(x) || IsAlpha(x) || x == '.' || x == '$' || x == '_' || x == '-')
            *e++ = x;
        else {
            /* If the encoded version won't fit, stop here */
            if (stop && e + 4 > stop)
                break;
            /* Encode characters that aren't reserved per RFC 1738 */
            sprintf(e, "%%%02x", x);
            e = strchr(e, 0);
        }
    }
    *e = 0;
    return e;
}

char *AddEncodedField(char *s, char *f, char *stop) {
    return AddEncodedFieldLimited(s, f, 65535, stop);
}

char *AddHTMLEncodedField(char *s, char *f, char *stop) {
    s = strchr(s, 0);

    for (; *f; f++) {
        if (*f == '<' || *f == '>' || *f == '"' || *f == '\'' || *f == '&' || *f < ' ') {
            char enc[7];
            sprintf(enc, "&#%d;", (int)(*f & 0xff));
            if (stop && s + strlen(enc) >= stop)
                break;
            strcpy(s, enc);
            s = strchr(s, 0);
        } else {
            if (stop && s + 1 >= stop)
                break;
            *s++ = *f;
        }
    }
    *s = 0;

    return s;
}

char *AddDoubleEncodedField(char *s, char *f, char *stop) {
    char *e;
    char unenc[2];
    char enc[4];
    int x;

    if (s == NULL || f == NULL)
        return s;

    /* If stopping point specified, move back 1 to insure there is room for final null */
    if (stop)
        stop--;

    unenc[1] = 0;
    for (e = strchr(s, 0); *f && (stop == NULL || e < stop); f++) {
        x = *f & 0xff;
        if (x < ' ')
            continue;
        unenc[0] = x;
        enc[0] = 0;
        AddEncodedField(enc, unenc, NULL);
        e = AddEncodedField(e, enc, stop);
    }
    return e;
}

void AdminForceHttpsAdmin(struct TRANSLATE *t) {
    GenericRedirect(t, myHttpsName, primaryLoginPortSsl, t->url, 1, 301);
}

void AdminRelogin(struct TRANSLATE *t) {
    sprintf(t->buffer, "/login?url=%s%s",
            primaryLoginPortSsl && (t->useSsl || primaryLoginPort == 0) ? myUrlHttps : myUrlHttp,
            t->url);
    MyNameRedirect(t, t->buffer, 0);
}

BOOL AdminReloginIfRequired(struct TRANSLATE *t) {
    if (ReloginRequired(t->session)) {
        AdminRelogin(t);
        return 1;
    }

    return 0;
}

void MergeSessionVariables(struct TRANSLATE *t, struct SESSION *e) {
    if (t && e) {
        if (VariablesMerge(e->sessionVariables, t->localVariables, "session:*")) {
            e->cookiesChanged = 1;
        }
    }
}

void AdminStarted(struct UUSOCKET *s) {
    char da[MAXASCIIDATE];

    WriteLine(s, "%s started at %s\n", EZPROXYVERSION, ASCIIDate(&startupTime, da));
}

void SendObfuscated(struct UUSOCKET *s, char *p, char *stop) {
    if (p == NULL)
        return;

    if (stop == NULL)
        stop = strchr(p, 0);

    for (; p < stop; p++) {
        if (optionSuppressObfuscation) {
            char t[2];
            t[0] = *p;
            t[1] = 0;
            SendHTMLEncoded(s, t);
        } else {
            WriteLine(s, "&#%d;", *p);
        }
    }
}

char *RefererURL(char *url, char *referer, int useSsl) {
    if (optionRefererInHostname == 0) {
        strcpy(url, useSsl && myUrlHttps ? myUrlHttps : myUrlHttp);
    } else {
        sprintf(url, "http%s://%s", useSsl ? "s" : "", referer);
        PeriodsToHyphens(url);
        AllLower(url);
        sprintf(AtEnd(url), ".%s", myName);
    }
    return url;
}

void AdminLoginReferer(struct TRANSLATE *t, struct DATABASE *b, char *url, char *accessType) {
    struct UUSOCKET *s = &t->ssocket;
    struct REFERER *referer;
    char *myUrl = (myUrlHttps && StartsIWith(url, "https")) ? myUrlHttps : myUrlHttp;
    char *signedUrl;
    char *ur = NULL;
    struct SESSION *e = t->session;
    BOOL local = stricmp(accessType, "local") == 0;

    /* Note that url is likely to point to t->buffer on entry, so encoding into a temp
       variable up-front takes care of any issue from the subsequent destruction
       of t->buffer
    */

    referer = b->referer;

    if (local) {
        for (; referer; referer = referer->next) {
            // make sure t->sin_addr between start and stop bounds
            if (compare_ip(&t->sin_addr, &referer->start) >= 0 &&
                compare_ip(&referer->stop, &t->sin_addr) >= 0)
                break;
        }
    } else {
        if (e == NULL)
            referer = NULL;
        else {
            for (; referer; referer = referer->next) {
                if (GroupMaskOverlap(e->gm, referer->gm))
                    break;
            }
        }
    }

    if (referer == NULL) {
        if (local) {
            char ipBuffer[INET6_ADDRSTRLEN];
            HTMLHeader(t, htmlHeaderOmitCache);
            WriteLine(s,
                      "IP address %s is excluded from proxying but does not correspond to a "
                      "Referer -IP directive for this URL.\n",
                      ipToStr(&t->sin_addr, ipBuffer, sizeof ipBuffer));
            t->finalStatus = 994;
        } else {
            AdminLogup(t, FALSE, url, b, b->gm, FALSE);
        }
        goto Finished;
    }

    if (e && e->anonymous == 0) {
        FreeThenNull(e->refererDestUrl);
        if (!(e->refererDestUrl = strdup(url)))
            PANIC;
    } else {
        if (!(signedUrl = malloc(strlen(url) + 20)))
            PANIC;
        sprintf(signedUrl, "%d|%s", RefererKey(), url);
        /* Logging happens here, so there is no need to add the tag to this reference
           and actually adding it is an issue since /referer does not expect it */
        ur = URLReference(signedUrl, NULL);
        FreeThenNull(signedUrl);
    }

    RefererURL(t->buffer, referer->tag, StartsIWith(url, "https") != NULL);
    sprintf(strchr(t->buffer, 0), "/referer/%s", referer->tag);

    HTTPCode(t, 302, 0);
    NoCacheHeaders(s);
    if (ur)
        WriteLine(s, "Set-Cookie: %s=%s; path=/\r\n", refererCookieName, ur);
    WriteLine(s, "Location: %s\r\n", t->buffer);

    HeaderToBody(t);

    WriteLine(s, "To access this site, go <a href='");
    SendHTMLEncoded(s, t->buffer);
    WriteLine(s, "'>here</a>\r\n");
    LogSPUs(t, t->buffer, accessType, b);

Finished:
    FreeThenNull(ur);
}

void AdminRedirect(struct TRANSLATE *t) {
    char *host;
    char *slash;
    struct UUSOCKET *s;
    size_t len;
    struct DATABASE *b;
    BOOL redirectPatch;
    BOOL forcePDF = 0;
    char *redirSlash;
    BOOL urlChanged;
    char *token = NULL;
    char *tokenSignature = NULL;
    int isMe;

    s = &t->ssocket;
    /* The ReverseEditURL checks to see if an EZproxy URL has been passed in, and if so, rearranges
       it to the non-EZproxy form needed below, which will end up changing it back again
    */
    ReverseEditURL(t, t->buffer, &urlChanged);

    host = t->buffer;
    if (strnicmp(host, "http://", 7) == 0) {
        host += 7;
    } else if (strnicmp(host, "https://", 8) == 0) {
        host += 8;
    } else if (strnicmp(host, "ftp://", 6) == 0) {
        host += 6;
    }

    slash = strchr(host, '/');

    redirSlash = (slash ? "" : "/");

    /* Moved ReverseEditURL up to make it globally part of this before slash computed */
    if (VerifyHost2(t->buffer, NULL, &b, NULL, &isMe) /* &&
        (ReverseEditURL(t, t->buffer, &urlChanged), urlChanged == 0) */ ) {
        AdminUnknown(t, t->buffer);
        return;
    }

    if (b && b->referer) {
        AdminLoginReferer(t, b, t->buffer, "proxy");
        return;
    }

    redirectPatch = b && b->redirectPatch;

    if (strnicmp(t->urlCopy, "/login/", 7) == 0 && b != NULL && b->getName != NULL) {
        /*   LogSPU handled in AdminLoginGet */
        AdminLoginGet(t, t->buffer, "proxy");
        return;
    }

    if (slash && *slash != '/') {
        *slash = '/';
    }

    len = strlen(t->buffer);
    if (len > 11 && stricmp(t->buffer + len - 11, "/ezproxypdf") == 0) {
        forcePDF = 1;
        *(t->buffer + len - 11) = 0;
    }

    /* LogSPU */
    /* Since URL is OK and in bare form here */
    LogSPUs(t, t->buffer, "proxy", b);

    EditURL(t, t->buffer);
    slash = strchr(host, '/');

    len = strlen(t->buffer);

    /*
    if (*t->version) {
        if (RefreshStartingPointUrl(t, t->buffer) ||
            (t->needPdfRefresh && (forcePDF || IsIERedirectBrainDead(t->buffer)))) {
            HTTPCode(t, 200, 0);
            NoCacheHeaders(s);
            HTTPContentType(t, NULL, 0);
            UUSendZ(s, "Refresh: 0; URL=");
        } else  {
            HTTPCode(t, 302, 0);
            NoCacheHeaders(s);
            UUSendZ(s, "Location: ");
        }
        SendTokenizedURL(t, b, t->buffer, &token, &tokenSignature, 0);
        if (redirectPatch)
            WriteLine(s, "%s%s", redirSlash, REDIRECTPATCHSTRING);
        UUSendZ(s, "\r\n\r\n");
    }
    */

    if (*t->version) {
        if (RefreshStartingPointUrl(t, t->buffer) ||
            (t->needPdfRefresh && (forcePDF || IsIERedirectBrainDead(t->buffer)))) {
            HTTPCode(t, 200, 0);
            /* If you include NoCacheHeaders, then the page will be forced to reload,
               which will break the back button in this case
            */
            /* NoCacheHeaders(s); */
            HTTPContentType(t, NULL, 0);
        } else {
            HTTPCode(t, 302, 0);
            NoCacheHeaders(s);
            UUSendZ(s, "Location: ");
            SendTokenizedURL(t, b, t->buffer, &token, &tokenSignature, 0);
            if (redirectPatch)
                WriteLine(s, "%s%s", redirSlash, REDIRECTPATCHSTRING);
            UUSendCRLF(s);
        }
        HeaderToBody(t);
    }

    UUSendZ(s, TAG_DOCTYPE_HTML_HEAD "</head>\n");

    if (t->finalStatus == 200) {
        WriteLine(s,
                  "<body onload='EZproxyCheckBack()'>\r\n\
<form name='EZproxyTrack'><input type='hidden' name='back' value='1'></form>\r\n\
<script language='JavaScript'>\r\n\
<!--\r\n\
function EZproxyCheckBack() {\r\n\
  var goForward = (document.EZproxyTrack.back.value==1);\r\n\
  document.EZproxyTrack.back.value=2;\r\n\
  document.EZproxyTrack.back.defaultValue=2;\r\n\
  if (goForward) { location.href = '");
        SendTokenizedURL(t, b, t->buffer, &token, &tokenSignature, 0);
        if (redirectPatch)
            WriteLine(s, "%s%s", redirSlash, REDIRECTPATCHSTRING);
        UUSendZ(s,
                "'; }\r\n\
}\r\n\
-->\r\n\
</script>\r\n");
    } else {
        UUSendZ(s, "<body>\n");
    }
    /*
    if (t->finalStatus == 200) {
        UUSendZ(s, "<meta http-equiv='Refresh' content=\"0; URL=");
        SendTokenizedURL(t, b, t->buffer, &token, &tokenSignature, 0);
        if (redirectPatch)
            WriteLine(s, "%s%s", redirSlash, REDIRECTPATCHSTRING);
        UUSendZ(s, "\">");
    }
    */
    if (pdfRefreshPre && t->finalStatus == 200)
        UUSendZ(s, pdfRefreshPre);
    else
        UUSendZ(s, "To access this document, wait a moment or click <a href='");

    SendTokenizedURL(t, b, t->buffer, &token, &tokenSignature, 0);
    if (redirectPatch)
        WriteLine(s, "%s%s", redirSlash, REDIRECTPATCHSTRING);

    if (pdfRefreshPost && t->finalStatus == 200)
        UUSendZ(s, pdfRefreshPost);
    else
        UUSendZ(s, "'>here</a> to continue\n");

    UUSendZ(s, "</body></html>\n");

    FreeThenNull(token);
    FreeThenNull(tokenSignature);
}

void AdminBaseHeader2(struct TRANSLATE *t, int options, char *title, char *extra, char *extraHead) {
    struct UUSOCKET *s = &t->ssocket;
    char *style1 =
        "\
<style type='text/css'>\n\
<!--\n\
a, body, font, p, td, th, blockquote, h1, h2, caption {\n\
font-family: Verdana, Arial, Helvetica, sans-serif;\n\
font-size: 10pt;\n\
}\n\
:focus {\n\
  background-color: yellow;\n\
}\n\
.small {\n\
font-family: Verdana, Arial, Helvetica, sans-serif;\n\
font-size: 8pt;\n\
}\n\
.success {\n\
color: green;\n\
}\n\
.warning, .failure {\n\
color: red;\n\
}\n\
.nomargin {\n\
margin-top: 0;\n\
margin-bottom: 0;\n\
margin-left: 0;\n\
margin-right: 0;\n\
}\n\
.prespace {\n\
margin-top: 1em;\n\
}\n\
.double, dd {\n\
margin-bottom: 10pt}\n\
tt {\n\
font-size: 10pt;\n\
font-family: 'Lucida Console', 'Lucida Sans Typewriter', 'Courier', monospace;\n\
}\n\
b {\n\
font-weight: bold;\n\
}\n\
font.small {\n\
font-size: 8pt;\n\
}\n\
h1, p.heading, bordered-table {\n\
font-weight: bold;\n\
font-size: 16pt;\n\
text-align: center;\n\
margin-top: 6px;\n\
}\n\
h2, p.subheading {\n\
font-weight: bold;\n\
}\n\
\n/* this caption equivalent to h2 */\n\
table.bordered-table caption, table.loose-table caption {\n\
font-weight: bold;\n\
margin-top: 0.83em;\n\
margin-bottom: 0.83em;\n\
text-align: left\n\
}\n\
a:active, a:link, a:visited {\n\
text-decoration: none;\n\
color: #0033CC;\n\
background-color: transparent;\n\
}\n\
a:hover {\n\
text-decoration: underline;\n\
}\n\
a.small:active, a.small:link, a.small:visited {\n\
font-size: 8pt;\n\
}\n\
table.bordered-table {\n\
border: 1px outset black;\n\
border-spacing: 0;\n\
}\n\
table.bordered-table td, table.bordered-table th {\n\
border: 1px inset black;\n\
padding: 1px 0.5em;\n\
}\n\
table.tight-table {\n\
border-spacing: 0;\n\
}\n\
table.loose-table {\n\
border-spacing: 0;\n\
}\n\
table.tight-table, table.tight-table td, table.tight-table th,\n\
table.loose-table, table.loose-table td, table.loose-table th {\n\
border: 0 none;\n\
padding: 0;\n\
margin: 0;\n\
}\n\
table.loose-table td, table.loose-table th {\n\
padding-top: 2px;\
padding-bottom: 2px;\
padding-right: 1em;\n\
}\n\
.top-bottom-padding {\n\
padding-top: 0.5em;\n\
padding-bottom: 0.5em;\n\
}\n\
th[scope=row] {\n\
text-align: left;\n\
}\n\
table.th-row-nobold th[scope=row] {\n\
font-weight: normal;\n\
}\n";
    char *style2 =
        "\
-->\n\
</style>\n";

    char *sorttable = "<script src='/sortable.js'></script>\n";

    if (title == NULL)
        title = "";

    if (extra == NULL)
        extra = "";

    if (optionDisableSortable || t->isMacMSIE)
        options &= ~AHSORTABLE;

    if ((options & AHNOHTTPHEADER) == 0)
        HTMLHeader(t, (options & AHNOEXPIRES) ? htmlHeaderMaxAgeCache : htmlHeaderNoCache);

    if ((options & AHNOHTMLHEADER) == 0) {
        UUSendZ(s, TAG_DOCTYPE_HTML_HEAD);
        if (*title) {
            UUSendZ(s, "<title>");
            SendHTMLEncoded(s, title);
            UUSendZ(s, "</title>\n");
        }
        if ((options & AHNOCSS) == 0) {
            UUSendZ(s, style1);
            UUSendZ(s, style2);
        }
        if (options & AHSORTABLE) {
            UUSendZ(s, sorttable);
        }

        if (extraHead && *extraHead)
            UUSendZ(s, extraHead);

        WriteLine(s, "</head>\n<body%s>\n",
                  (options & AHSORTABLE ? " onload='sortable_init()'" : ""));
    }

    if ((options & AHNOTOPMENU) == 0)
        WriteLine(s, "<a href='/admin'>Administration</a>%s<hr>\n", extra);

    if (*title && (options & AHNOH1TITLE) == 0) {
        UUSendZ(s, "<h1>");
        SendHTMLEncoded(s, title);
        UUSendZ(s, "</h1>\n");
    }
}

void AdminBaseHeader(struct TRANSLATE *t, int options, char *title, char *extra) {
    AdminBaseHeader2(t, options, title, extra, NULL);
}

void AdminHeader(struct TRANSLATE *t, int options, char *title) {
    AdminBaseHeader(t, options, title, NULL);
}

void AdminFooter(struct TRANSLATE *t, BOOL suppressCopyright) {
    struct UUSOCKET *s = &t->ssocket;

    if (suppressCopyright == 0)
        UUSendZ(s, "<p><a class='small' href='" EZPROXYURLROOT "'>" EZPROXYCOPYRIGHTSTATEMENT
                   ".</a></p>\n");
    UUSendZ(s, "</body>\n</html>\n");
}

void AdminMinimalHeader(struct TRANSLATE *t, char *title) {
    AdminHeader(t, AHNOTOPMENU, title);
}

void AdminMinimalFooter(struct TRANSLATE *t) {
    AdminFooter(t, TRUE);
}

BOOL AdminNotAllowed(struct TRANSLATE *t, GROUPMASK gm) {
    struct SESSION *e = t->session;

    if (e == NULL) {
        AdminRelogin(t);
        return 1;
    }

    if (AdminReloginIfRequired(t))
        return 1;

    if (e && e->priv == 0 && e->autoLoginBy != autoLoginByNone) {
        AdminRelogin(t);
        return 1;
    }

    if (e->priv == 0 && (gm == NULL || GroupMaskOverlap(gm, e->gm) == 0)) {
        AdminUnauthorized(t);
        return 1;
    }

    return 0;
}

void AdminTokenKeyField(struct TRANSLATE *t, int type, struct DATABASE *b, BOOL hidden, BOOL first);

char *DecryptVar(char *input, unsigned char *key, unsigned char *ivec) {
    unsigned char *hold = NULL;
    size_t len;
    EVP_CIPHER_CTX *ctx = NULL;
    int updateLen, finalLen;
    unsigned char *output = NULL;

    if (!(hold = malloc(strlen(input) + 1)))
        PANIC;
    Decode64Binary(hold, &len, input);
    if (len == 0 || len % 8 != 0)
        goto Invalid;

    if (!(ctx = EVP_CIPHER_CTX_new()))
        PANIC;
    EVP_CIPHER_CTX_init(ctx);
    EVP_DecryptInit(ctx, EVP_des_ede3_cbc(), key, ivec);

    /* The decrypted version cannot be larger than the encrypted version */
    if (!(output = malloc(len)))
        PANIC;

    if (!EVP_DecryptUpdate(ctx, output, &updateLen, hold, (int)len))
        goto Invalid;

    if (!EVP_DecryptFinal(ctx, output + updateLen, &finalLen))
        goto Invalid;

    EVP_CIPHER_CTX_cleanup(ctx);
    len = updateLen + finalLen;
    if (output[len - 1] == 0)
        goto Cleanup;

Invalid:
    FreeThenNull(output);

Cleanup:
    if (ctx) {
        EVP_CIPHER_CTX_free(ctx);
        ctx = NULL;
    }
    FreeThenNull(hold);

    return (char *)output;
}

void AdminDecryptVar(struct TRANSLATE *t, char *query, char *post) {
    struct DATABASE *b, *firstToken = NULL;
    struct UUSOCKET *s = &t->ssocket;
    char *decryptedVar;
    const int ADVKEY = 0;
    const int ADVVAR = 1;
    const int ADVVAL = 2;
    struct FORMFIELD ffs[] = {
        {"key", NULL, 0, 0, 6  },
        {"var", NULL, 0, 0, 1  },
        {"val", NULL, 0, 0, 128},
        {NULL,  NULL, 0, 0, 0  }
    };
    BOOL anySent = 0;
    int i;
    char *key, *var, *val;
    struct ENCRYPTVAR *ev;

    FindFormFields(query, post, ffs);

    key = ffs[ADVKEY].value;
    var = ffs[ADVVAR].value;
    val = ffs[ADVVAL].value;
    if (var) {
        Trim(var, TRIM_COLLAPSE);
        AllLower(var);
    }
    AdminHeader(t, 0, "Decrypt Variable");

    if (key && *key && var && *var && val && *val) {
        i = atoi(key);
        b = NULL;
        if (i >= 0 && i < cDatabases) {
            b = databases + i;
            for (ev = b->ev; ev; ev = ev->next) {
                if (strchr(ev->var, *var))
                    break;
            }

            if (ev == NULL) {
                UUSendZ(s, "Variable ");
                SendHTMLEncoded(s, var);
                UUSendZ(s, " is not encrypted in ");
                SendHTMLEncoded(s, b->title);
                UUSendCRLF(s);
                return;
            }
            SendHTMLEncoded(s, val);
            UnescapeString(val);
            decryptedVar = DecryptVar(val, ev->key, ev->ivec);
            UUSendZ(s, " is ");
            if (decryptedVar == NULL) {
                UUSendZ(s, "not a valid value");
            } else {
                SendHTMLEncoded(s, decryptedVar);
            }
            FreeThenNull(decryptedVar);
            return;
        }
    }

    firstToken = NULL;
    for (i = 0, b = databases; i < cDatabases; i++, b++) {
        if (b->ev == NULL)
            continue;
        if (firstToken == NULL) {
            firstToken = b;
            UUSendZ(s, "<form action='/decryptvar' method='post'>\n");
            continue;
        }
        if (anySent == 0) {
            AdminTokenKeyField(t, 0, firstToken, 0, 1);
            anySent = 1;
        }
        AdminTokenKeyField(t, 0, b, 0, 0);
    }

    if (anySent == 0) {
        if (firstToken == NULL) {
            UUSendZ(s, EZPROXYCFG " does not contain any EncryptVar statements\n");
            AdminFooter(t, 0);
            return;
        }
        UUSendZ(s, "<div>Decrypt variable from database ");
        SendHTMLEncoded(s, firstToken->title);
        UUSendZ(s, "</div>\n");
        AdminTokenKeyField(t, 0, firstToken, 1, 0);
    }

    UUSendZ(s,
            "<div><label for='var'>Enter variable letter or number: </label><input type='text' "
            "name='var' id='var' size='1' maxlength='1'");
    if (var) {
        UUSendZ(s, " value='");
        SendQuotedURL(t, var);
        UUSendZ(s, "'");
    }
    UUSendZ(s, "></div>\n");

    UUSendZ(s,
            "<div><label for='val'>Enter value to decrypt: </label><input type='text' name='val' "
            "id='val'");
    if (val) {
        UUSendZ(s, " value='");
        SendQuotedURL(t, val);
        UUSendZ(s, "'");
    }
    UUSendZ(s, "></div>\n");

    UUSendZ(s, "<input type='submit' value='Decrypt'>\n</form>\n");

    AdminFooter(t, 0);
}

char *FirstSlashQuestion(char *s) {
    for (;; s++) {
        if (*s == 0 || *s == '/' || *s == '?')
            return s;
    }
}

void AdminNoToken(struct TRANSLATE *t) {
    HTMLHeader(t, htmlHeaderOmitCache);
    UUSendZ(&t->ssocket,
            "<p>Unable to generate user token. <a href='/logout'>Log out</a> of " EZPROXYNAMEPROPER
            " then log back in.</p>\n");
}

void AdminEncodeURL(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    const int urlIdx = 0;
    struct FORMFIELD ffs[] = {
        {"url", NULL, 0, 0, 0},
        {NULL,  NULL, 0, 0, 0}
    };
    char *url;

    FindFormFields(query, post, ffs);
    url = ffs[urlIdx].value;

    AdminMinimalHeader(t, "Encode URL");
    if (url) {
        UUSendZ(s, "<div>");
        SendQuotedURL(t, url);
        UUSendZ(s, "</div><div>encodes to</div><div>");
        SendUrlEncoded(s, url);
        UUSendZ(s, "</div>\n");
    } else {
        UUSendZ(s,
                "<form action='/encodeurl'><label for='url'>Enter URL to encode then press enter "
                "</label><input type='text' name='url' id='url' size='50'></form>");
    }
    AdminMinimalFooter(t);
}

void AdminMyiLibrary(struct TRANSLATE *t, char *query, char *post) {
    struct DATABASE *b;
    struct SESSION *e = t->session;
    char *userToken = NULL;
    struct MYILIBRARYCONFIG *milc = NULL;
    time_t now;
    char nows[11];
    EVP_MD_CTX *mdctx = NULL;
    unsigned char md_value[EVP_MAX_MD_SIZE];
    unsigned int md_len;
    GROUPMASK requiredGm = NULL;

    if (t->host == NULL || (b = t->host->myDb) == NULL || (milc = b->myiLibraryConfigs) == NULL) {
        HTTPCode(t, 404, 1);
        goto Finished;
    }

    /* If we need to redirect without change, use the following cod
    {
        GenericRedirect(t, t->host->hostname, t->host->remport, t->urlCopy, t->host->useSsl, 0);
        goto Finished;
    }
    */

    if (e = t->session) {
        for (;; milc = milc->next) {
            if (milc == NULL) {
                AdminLogup2(t, "");
                goto Finished;
            }
            if (GroupMaskOverlap(e->gm, milc->gm))
                break;

            if (requiredGm)
                GroupMaskOr(requiredGm, milc->gm);
            else
                requiredGm = GroupMaskCopy(NULL, milc->gm);
        }
    }

    /* We do allow the anonymous case for MyiLibrary, so only bail on unknown */
    if (e == NULL) {
        AdminRelogin(t);
        goto Finished;
    }

    if (e->autoLoginBy == autoLoginByNone) {
        userToken = UserToken2("mil", e->logUserBrief, 1);
        if (userToken == NULL) {
            AdminNoToken(t);
            goto Finished;
        }
    }

    time(&now);
    sprintf(nows, "%" PRIdMAX "", (intmax_t)now);

    if (!(mdctx = EVP_MD_CTX_new()))
        PANIC;
    if (milc->hash == SSOHASHSHA1) {
    } else if (milc->hash == SSOHASHSHA256) {
        EVP_DigestInit(mdctx, EVP_sha256());
    } else {
        EVP_DigestInit(mdctx, EVP_md5());
    }
    EVP_DigestUpdate(mdctx, (unsigned char *)milc->key, strlen(milc->key));
    EVP_DigestUpdate(mdctx, (unsigned char *)"|", 1);
    EVP_DigestUpdate(mdctx, (unsigned char *)milc->inst, strlen(milc->inst));
    EVP_DigestUpdate(mdctx, (unsigned char *)"|", 1);
    EVP_DigestUpdate(mdctx, (unsigned char *)nows, strlen(nows));
    EVP_DigestUpdate(mdctx, (unsigned char *)"|", 1);
    if (userToken)
        EVP_DigestUpdate(mdctx, (unsigned char *)userToken, strlen(userToken));
    EVP_DigestFinal(mdctx, md_value, &md_len);
    EVP_MD_CTX_free(mdctx);
    mdctx = NULL;

    StrCpy3(t->buffer, t->url, sizeof(t->buffer));

    query = strchr(t->buffer, '?');
    if (query) {
        char *eol;
        GetSnipField(query, "ver", 1, 0);
        GetSnipField(query, "inst", 1, 0);
        GetSnipField(query, "ts", 1, 0);
        GetSnipField(query, "user", 1, 0);
        GetSnipField(query, "sig", 1, 0);
        for (eol = strchr(query, 0) - 1; eol >= query && (*eol == '?' || *eol == '&'); eol--)
            *eol = 0;
    }

    strcat(t->buffer, strchr(t->buffer, '?') ? "&" : "?");
    sprintf(strchr(t->buffer, 0), "ver=1&inst=");
    AddEncodedField(t->buffer, milc->inst, NULL);
    sprintf(strchr(t->buffer, 0), "&ts=%s&user=", nows);
    if (userToken)
        AddEncodedField(t->buffer, userToken, NULL);
    strcat(t->buffer, "&sig=");
    DigestToHex(strchr(t->buffer, 0), md_value, md_len, 1);

    GenericRedirect(t, t->host->hostname, t->host->remport, t->buffer, t->useSsl, 0);

    if (debugLevel) {
        Log("MyiLibrary URL generated with user token %s inst %s", userToken, milc->inst);
    }

Finished:
    FreeThenNull(userToken);
    GroupMaskFree(&requiredGm);
}

void AdminHA(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;

    const int AHSESSION = 0;

    struct FORMFIELD ffs[] = {
        {"session", NULL, 0, 0, MAXKEY},
        {NULL, NULL, 0, 0}
    };
    char *key;
    struct SESSION *e;

    FindFormFields(query, post, ffs);

    key = ffs[AHSESSION].value;

    if (key == NULL) {
        HTTPCode(t, 404, 1);
        return;
    }

    e = FindSessionByKey(key);

    HTMLHeader(t, htmlHeaderOmitCache);

    WriteLine(s, "SESSION=%s\n", (e == NULL || e->active != 1) ? "0" : "1");
}

void LocalRedirect(struct TRANSLATE *t,
                   char *url,
                   int status,
                   BOOL useRefresh,
                   struct DATABASE *b) {
    struct UUSOCKET *s = &t->ssocket;
    char *token = NULL;
    char *tokenSignature = NULL;

    if (t->excludeIPBanner && excludeIPBanner) {
        HTTPCode(t, 200, 0);
        if (t->session == NULL)
            SendEZproxyCookie(t, NULL);
        HTTPContentType(t, NULL, 1);
        SendEditedFile(t, NULL, excludeIPBanner, 1, url, 0, NULL);
        return;
    }

    if (b && b->referer) {
        AdminLoginReferer(t, b, url, "local");
        return;
    }

    if (status == 0)
        status = 302;

    if (useRefresh)
        status = 200;

    HTTPCode(t, status, 0);
    NoCacheHeaders(s);
    if (t->version) {
        if (useRefresh) {
            UUSendZ(s, "Refresh: 0; URL=");
        } else {
            UUSendZ(s, "Location: ");
        }
        SendTokenizedURL(t, b, url, &token, &tokenSignature, 0);
        UUSendCRLF(s);
        HeaderToBody(t);
    }

    UUSendZ(s, TAG_DOCTYPE_HTML_HEAD);
    if (useRefresh) {
        UUSendZ(s, "<meta http-equiv='Refresh' content='0; URL=");
        SendTokenizedURL(t, b, url, &token, &tokenSignature, 0);
        UUSendZ(s, "'>");
    }
    UUSendZ(s, "</head>\n<body>\n");
    UUSendZ(s, "To access this document, wait a moment or click <a href='");
    SendTokenizedURL(t, b, url, &token, &tokenSignature, 0);
    UUSendZ(s, "'>here</a> to continue\n");
    UUSendZ(s, "</body></html>\n");

    FreeThenNull(token);
    FreeThenNull(tokenSignature);
}

/* This variant of AdminLogup is called from things that note group mismatch during the login
 * process */
void AdminLogup(struct TRANSLATE *t,
                BOOL makeUrl,
                char *url,
                struct DATABASE *b,
                GROUPMASK requiredGm,
                BOOL isIntruder) {
    struct USERINFO ui;
    int userLimit;
    char *title;
    struct SESSION *e = t->session;
    struct GROUP *g;
    char *denyFile;
    char *myUrl = NULL;
    GROUPMASK gm;
    static int logupId = 0;

    if (makeUrl) {
        if (t->host == hosts) {
            if (!(myUrl = malloc(strlen(myName) + strlen(t->urlCopy) + 128)))
                PANIC;
            sprintf(myUrl, "%s%s",
                    primaryLoginPortSsl && (t->useSsl || primaryLoginPort == 0) ? myUrlHttps
                                                                                : myUrlHttp,
                    t->urlCopy);
            url = myUrl;
        } else {
            struct HOST *h = t->host;
            if (!(myUrl = malloc(strlen(h->hostname) + strlen(t->urlCopy) + 128)))
                PANIC;
            sprintf(myUrl, "http%s://%s", h->useSsl ? "s" : "", h->hostname);
            if (h->remport != (h->useSsl ? 443 : 80)) {
                sprintf(AtEnd(myUrl), ":%d", h->remport);
            }
            strcat(myUrl, t->urlCopy);
        }
    }

    gm = GroupMaskNew(0);
    InitUserInfo(t, &ui, NULL, NULL, url, gm);
    ui.logup = 1;
    ui.requiredGm = requiredGm;
    FindUser(t, &ui, NULL, NULL, NULL, &userLimit, isIntruder);
    GroupMaskFree(&gm);

    if (ui.deniedNotified) {
        goto Finished;
    }

    /* For the final value, we are only truly doing a logup if there is an existing
    session, and that session did not come into existence by AutoLoginIP and/or Referring URL */
    if (AdminCGILogin(t, &ui, url,
                      (t->session != NULL && t->session->autoLoginBy == autoLoginByNone)))
        goto Finished;

    if (e == NULL || e->autoLoginBy != autoLoginByNone)
        denyFile = ui.login;
    else {
        denyFile = LOGUPHTM;
        for (g = groups; g; g = g->next) {
            if (GroupMaskOverlap(requiredGm, g->gm) && GroupMaskOverlap(e->gm, g->gm) == 0 &&
                g->denyFile) {
                denyFile = g->denyFile;
                break;
            }
        }
    }

    if (e != NULL && t->useSsl == 0 && optionAllowHTTPLogin == 0) {
        UUAcquireMutex(&mActive);
        e->logupId = ++logupId;
        FreeThenNull(e->logupDenyFile);
        if (!(e->logupDenyFile = strdup(denyFile)))
            PANIC;
        FreeThenNull(e->logupUrl);
        if (url) {
            if (!(e->logupUrl = strdup(url)))
                PANIC;
        }
        e->logupDb = b;
        sprintf(t->buffer, "/login?logup=%d", e->logupId);
        UUReleaseMutex(&mActive);
        GenericRedirect(t, myHttpsName, primaryLoginPortSsl, t->buffer, 1, 0);
    } else {
        title = (b != NULL && b->title != NULL ? b->title : "");
        CSRFHeader(t);
        SendEditedFile(t, NULL, denyFile, 1, url, 1, title, NULL);
    }

Finished:
    FreeThenNull(myUrl);
}

/* This variant of AdminLogup is called when the host is being proxied but group access isn't
 * allowed */
void AdminLogup2(struct TRANSLATE *t, char *up) {
    struct UUSOCKET *s = &t->ssocket;
    char *q;
    char *myUrl;

    HTTPCode(t, 302, 0);
    NoCacheHeaders(s);
    /* StripScripts(t->url); */
    EncodeAngleBrackets(t->url, sizeof(t->url));
    q = PoundFix(t->url, sizeof(t->url), 0);

    myUrl = myUrlHttp;
    if (myUrlHttps && (t->useSsl || optionAllowHTTPLogin == 0))
        myUrl = myUrlHttps;

    if (*t->version) {
        WriteLine(s, "Location: %s/login%s?%surl=http%s://", myUrl, up, q,
                  ((t->host)->useSsl ? "s" : ""));
        UUSendZ(s, (t->host)->hostname);
        SendIfNonDefaultPort(s, (t->host)->remport, (t->host)->useSsl);
        UUSendZCRLF(s, t->url);
        HeaderToBody(t);
    }

    WriteLine(s, "To access this site, go <a href='%s/login%s?%surl=http%s://", myUrl, up, q,
              ((t->host)->useSsl ? "s" : ""));
    UUSendZ(s, (t->host)->hostname);
    SendIfNonDefaultPort(s, (t->host)->remport, (t->host)->useSsl);
    UUSendZ(s, t->url);
    UUSendZ(s, "'>here</a>\n");
}

void AdminOvidHtml(struct TRANSLATE *t, char *query, char *post) {
    MyNameRedirect(t, "/login?url=http://gateway.ovid.com/autologin.html", 0);
}

void AdminReferer(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    char *url = NULL;
    struct DATABASE *b;
    char *key, *val;
    char *bar;
    char *cookie;
    char *rc, *p;
    struct SESSION *e = t->session;

    if (e && (url = e->refererDestUrl))
        goto Valid;

    for (cookie = t->requestHeaders; (cookie = FindField("cookie", cookie));) {
        LinkField(cookie);
        for (rc = cookie; rc = strstr(rc, refererCookieName); rc++) {
            if (rc == cookie || IsWS(*(rc - 1)) || *(rc - 1) == ';') {
                p = rc + strlen(refererCookieName);
                if (strncmp(p, "=ezp.2", 6) == 0) {
                    char *semi;
                    size_t len;
                    p++;
                    if (semi = strchr(p, ';'))
                        len = semi - p;
                    else
                        len = strlen(p);
                    if (len >= sizeof(t->buffer))
                        len = sizeof(t->buffer) - 1;
                    memcpy(t->buffer, p, len);
                    t->buffer[len] = 0;
                    url = t->buffer;
                    if (!DecodeURLReference(url))
                        goto Invalid;
                    break;
                }
            }
        }
    }

    if (url == NULL) {
        HTMLHeader(t, htmlHeaderOmitCache);
        if (FileExists(REFERERCOOKIEHTM)) {
            SendFile(t, REFERERCOOKIEHTM, SFREPORTIFMISSING);
            goto Finished;
        } else {
            WriteLine(s,
                      "<p>Destination URL missing; either your browser rejected the cookie %s or "
                      "you refreshed this page.</p>\n",
                      refererCookieName);
        }
        goto Finished;
    }

    if ((bar = strchr(url, '|')) == NULL || ValidRefererKey(atoi(url)) == 0) {
        if (bar) {
            char *urlCopy;
            if (!(urlCopy = strdup(bar + 1)))
                PANIC;
            sprintf(t->buffer, "/login?qurl=");
            AddEncodedField(t->buffer, urlCopy, t->buffer + sizeof(t->buffer));
            FreeThenNull(urlCopy);
            MyNameRedirect(t, t->buffer, 0);
            goto Finished;
        }
        goto Invalid;
    }

    url = bar + 1;

Valid:

    VerifyHost2(url, NULL, &b, NULL, NULL);

    if (query = strchr(url, '?'))
        *query++ = 0;
    else
        query = strchr(url, 0);

    HTTPCode(t, 200, 0);
    WriteLine(s, "Set-Cookie: %s=0; path=/; expires=Fri, 01-Jan-1999 00:00:00 GMT\r\n",
              refererCookieName);
    HeaderToBody(t);
    UUSendZ(s, TAG_DOCTYPE_HTML_HEAD "<title>");
    if (b)
        SendHTMLEncoded(s, b->title);
    WriteLine(s,
              "</title></head>\
<body onload='EZproxyCheckBack()'>\
<form name='EZproxyForm' method='GET' action='");
    SendQuotedURL(t, url);
    UUSendZ(s, "'>");

    while (NextKEV(query, &key, &val, &query, 0)) {
        UUSendZ(s, "<input type='hidden' name='");
        SendObfuscated(s, key, NULL);
        UUSendZ(s, "' value='");
        SendObfuscated(s, val, NULL);
        UUSendZ(s, "'>");
    }
    if (b->formSubmit)
        UUSendZ(s, b->formSubmit);
    else
        UUSendZ(s,
                "If your browser does not continue automatically, click <input type='submit' "
                "value='here'>");
    UUSendZ(s,
            "\r\n\
</form>\
<form name='EZproxyTrack'><input type='hidden' name='back' value='1'>\
<script language='JavaScript'>\r\n\
<!--\r\n\
function EZproxyCheckBack() {\
  var goForward = (document.EZproxyTrack.back.value==1);\
  document.EZproxyTrack.back.value=2;\
  document.EZproxyTrack.back.defaultValue=2;\
  if (goForward) { document.EZproxyForm.submit(); }\
}\r\n\
-->\r\n\
</script></body></html>\r\n");
    goto Finished;

Invalid:
    HTTPCode(t, 404, 1);

Finished:;
}

void AdminSave(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;

    AdminHeader(t, 0, "Save Hosts and Usage");

    UUSendZ(s, "Saving hosts and usage\n");
    AdminFooter(t, 0);
    NeedSave();
    NeedSaveUsage();
}

int UUCopyFile(char *newFile, char *oldFile) {
    int fo = -1;
    int fn = -1;
    int result;
    char buffer[1024];
    int len;

    fo = SOPEN(oldFile);
    if (fo < 0)
        goto Error;

    fn = UUopenCreateFD(newFile, O_CREAT | O_WRONLY | O_TRUNC, defaultFileMode);
    if (fn < 0)
        goto Error;

    while ((len = UUreadFD(fo, buffer, sizeof(buffer))) > 0) {
        if (write(fn, buffer, len) != len)
            goto Error;
    }

    result = 0;
    goto Finished;

Error:
    result = errno;

Finished:
    if (fo >= 0)
        close(fo);
    if (fn >= 0)
        close(fn);

    return result;
}

char *ValidRefererHostname(char *host, size_t len, char *specific) {
    size_t refererLen;
    struct DATABASE *b;
    struct REFERER *referer;
    struct REFERER *lastReferer = NULL;
    int i;
    static struct VRN {
        struct VRN *next;
        char *name;
        size_t len;
    } *vrn = NULL;
    struct VRN *n;
    char *p;

    if (optionProxyType == PTPORT)
        return NULL;

    if (len == 0)
        len = strlen(host);

    refererLen = len - myNameLen - 1;
    if (refererLen <= 0)
        return NULL;

    p = host + refererLen;

    if (*p != '.' || strnicmp(p + 1, myName, myNameLen) != 0)
        return NULL;

    if (specific) {
        if (strnicmp(host, specific, refererLen) != 0 || refererLen != strlen(specific))
            return NULL;
    }

    if (vrn == NULL) {
        if (optionRefererInHostname) {
            for (i = 0, b = databases; i < cDatabases; i++, b++) {
                if (lastReferer != b->referer) {
                    lastReferer = b->referer;
                    for (referer = b->referer; referer; referer = referer->next) {
                        for (n = vrn; n; n = n->next) {
                            if (stricmp(n->name, referer->tag) == 0)
                                break;
                        }
                        if (n == NULL) {
                            if (!(n = calloc(1, sizeof(*n))))
                                PANIC;
                            n->name = referer->tag;
                            n->len = strlen(n->name);
                            n->next = vrn;
                            vrn = n;
                        }
                    }
                }
            }
            if (anyOptionGroupInReferer) {
                struct GROUP *g;
                char transformedName[256];
                for (g = groups; g; g = g->next) {
                    StrCpy3(transformedName, g->name, sizeof(transformedName));
                    PeriodsToHyphens(transformedName);
                    for (n = vrn; n; n = n->next) {
                        if (stricmp(n->name, transformedName) == 0)
                            break;
                    }
                    if (n == NULL) {
                        if (!(n = calloc(1, sizeof(*n))))
                            PANIC;
                        if (strcmp(g->name, transformedName) == 0) {
                            n->name = g->name;
                        } else {
                            if (!(n->name = strdup(transformedName)))
                                PANIC;
                        }
                        n->len = strlen(n->name);
                        n->next = vrn;
                        vrn = n;
                    }
                }
            }
        }
        if (!(n = calloc(1, sizeof(*n))))
            PANIC;
        n->name = "login";
        n->len = strlen(n->name);
        n->next = vrn;
        vrn = n;
    }

    for (n = vrn; n; n = n->next) {
        if ((size_t)refererLen == n->len && strnicmp(host, n->name, n->len) == 0)
            return n->name;
    }

    return NULL;
}

void AdminStatusCookies(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s;
    int i;
    struct SESSION *e;
    struct COOKIE *k;
    char de[MAXASCIIDATE];
    s = &t->ssocket;

    AdminHeader(t, 0, "Cookies");

    UUSendZ(s, "<table class='bordered-table'>\n");
    UUSendZ(s,
            "<tr><th scope='col'>Domain</th><th scope='col'>Path</th><th "
            "scope='col'>Expires</th><th scope='col'>Cookie</th></tr>\n");
    for (e = sessions, i = 0; i < cSessions; e++, i++) {
        if (e->active != 1)
            continue;

        WriteLine(s, "<tr><td colspan=4>%s</td></tr>\n", e->key);
        for (k = e->cookies; k; k = k->next) {
            UUSendZ(s, "<tr><td>");
            SendHTMLEncoded(s, k->domain);
            UUSendZ(s, "<td>");
            SendHTMLEncoded(s, k->path);
            ASCIIDate(&k->expires, de);
            WriteLine(s, "<td>%s<td>", de);
            SendHTMLEncoded(s, k->name);
            if (k->val) {
                UUSendZ(s, "=");
                SendHTMLEncoded(s, k->val);
            }
            UUSendZ(s, "</tr>\n");
        }
    }
    UUSendZ(s, "</table>\n");

    AdminFooter(t, 0);
}

void AdminTranslates(struct TRANSLATE *t, char *query, char *post) {
    int i;
    struct UUSOCKET *s;
    struct TRANSLATE *t1;
    int count;
    char da[MAXASCIIDATE];

    s = &t->ssocket;
    HTMLHeader(t, htmlHeaderOmitCache);
    UUSendZ(s, "<p>Translates</p>\n");
    for (count = 0, i = 0, t1 = translates; i < cTranslates; i++, t1++) {
        if (t1->active == 0)
            continue;
        count++;
        ASCIIDate(&t1->activated, da);
        WriteLine(s, "<div>%d %d %d %s</div>\n", count, i, t1->raw, da);
    }
}

void AdminUserLimitExceeded(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    char limitString[10];
    struct SESSION *e;

    if (t == NULL || (e = t->session) == NULL)
        return;

    HTMLHeader(t, htmlHeaderNoCache);

    if (e->lifetime > 300)
        e->lifetime = 300;

    sprintf(limitString, "%d", e->userLimitExceeded);
    if (SendEditedFile(t, NULL, LIMITHTM, 0, "", 0, limitString, SIfNotOne(e->userLimitExceeded),
                       IsAre((t->session)->userLimitExceeded, 0), NULL)) {
        WriteLine(s, "Your account is limited to %d session%s. ", e->userLimitExceeded,
                  SIfNotOne(e->userLimitExceeded));
        UUSendZ(s, "This limit has been exceeded.<p>\n");
        WriteLine(s, "Click <a href='%s/logout'>logout</a> to end this session.\n", t->myUrl);
    }
}

BOOL AdminLicenseInvalid(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket;
    static BOOL warn = TRUE;
    static BOOL warnInvalid = TRUE;
    static BOOL warnValid = FALSE;

    if (licenseValid || WskeyIsBeta()) {
        if (warnValid) {
            Log("The problem with the EZproxy license has been resolved.");
            warnValid = FALSE;
            warnInvalid = TRUE;
        }
        warn = TRUE;
        return FALSE;
    }

    if (warnInvalid) {
        Log("There is a problem with the EZproxy license. Please contact " SUPPORTEMAIL
            " for assistance.");
        warnValid = TRUE;
        warnInvalid = FALSE;
    }

    HTMLErrorPage(t, NULL,
                  "EZproxy is currently unavailable. Please contact your system administrator for "
                  "assistance.");
    t->finalStatus = 903;

    return TRUE;
}

#define AdminShibbolethSSOSAML_URL_STEM "/Shibboleth.sso/SAML"
#define AdminShibbolethSSOSAML_POST_DATA_SIZE_LIMIT (4 * 64 * 1024)
#define TYPICAL_POST_DATA_SIZE_LIMIT (64 * 1024)

// clang-format off
struct DISPATCH loginDispatchTable[] = {
        { "/",                        AdminTop,                   ADMIN_UNAUTH      },
        { "/about",                   AdminAbout,                 ADMIN_PRIV        },
        { "/account-add",             AdminAccountAdd,            ADMIN_PRIV        },
        { "/account-reset",           AdminAccountReset,          ADMIN_PRIV        },
        { "/admin",                   AdminAdmin,                 ADMIN_HTTPS       },
        { "/audit",                   AdminAudit,                 ADMIN_PRIV, GROUP_ADMIN_AUDIT },
        { "/auth",                    AdminAuth,                  ADMIN_UNAUTH      },
        { "/cas/admin",               AdminCASAdmin,              ADMIN_PRIV        },
        { "/cas/login",               AdminCASLogin,              ADMIN_UNAUTH | ADMIN_LICENSED },
        { "/cas/logout",              AdminCASLogout,             ADMIN_UNAUTH | ADMIN_LICENSED },
        { "/cas/validate",            AdminCASValidate,           ADMIN_UNAUTH | ADMIN_LICENSED      },
        { "/cas/serviceValidate",     AdminCASServiceValidate,    ADMIN_UNAUTH | ADMIN_LICENSED      },
        { "/cas/p3/serviceValidate",  AdminCASP3ServiceValidate,  ADMIN_UNAUTH | ADMIN_LICENSED      },
        { "/conflicts",               AdminConflicts,             ADMIN_PRIV        },
        { "/connect",                 AdminLogin,                 ADMIN_UNAUTH      },
        { "/cpip/getconfigversion",   AdminCPIPGetConfigVersion2, ADMIN_UNAUTH      },
        { "/cpip/getconfigversion2",  AdminCPIPGetConfigVersion2, ADMIN_UNAUTH      },
        { "/cpip/identify",           AdminCPIPIdentify,          ADMIN_UNAUTH      },
        { "/cpip/pickup",             AdminCPIPPickup,            ADMIN_UNAUTH      },
        { "/cpip/lastactive",         AdminCPIPLastActive,        ADMIN_UNAUTH      },
        { "/cpip/deauthenticate",     AdminCPIPDeauthenticate,    ADMIN_UNAUTH      },
        { "/cookie",                  AdminCookie,                ADMIN_UNAUTH      },
        { "/decryptvar",              AdminDecryptVar,            ADMIN_PRIV, GROUP_ADMIN_DECRYPTVAR },
        { "/ebrary",                  AdminEbrary,                ADMIN_LICENSED    },
        { "/encodeurl",               AdminEncodeURL,             ADMIN_UNAUTH      },
        { "/fiddler",                 AdminFiddler,               ADMIN_PRIV, GROUP_ADMIN_FIDDLER },
        { "/form",                    AdminForm,                  ADMIN_UNAUTH      },
        { "/gartner",                 AdminGartner,               ADMIN_LICENSED    },
        { "/gartnerkey",              AdminGartnerKey,            ADMIN_PRIV        },
        { "/groups",                  AdminGroups,                ADMIN_PRIV, GROUP_ADMIN_GROUPS },
        { "/ha",                      AdminHA,                    ADMIN_UNAUTH      },
        { "/headers",                 AdminHeaders,               ADMIN_UNAUTH      },
        { "/identifier/scopes",       AdminIdentifierScopes,      ADMIN_PRIV, GROUP_ADMIN_IDENTIFIER },
        { "/identifier",              AdminIdentifier,            ADMIN_PRIV, GROUP_ADMIN_IDENTIFIER },
        { "/intrusion",               AdminIntrusion,             ADMIN_PRIV, GROUP_ADMIN_INTRUSION },
        { "/ip",                      AdminIP,                    ADMIN_UNAUTH | ADMIN_LICENSED },
        { WSLIMITED,                  AdminWebServer,             ADMIN_UNAUTH | ADMIN_LICENSED },
        { WSLOGGEDIN,                 AdminWebServer,             ADMIN_LICENSED                },
        { "/ldap",                    AdminLDAP,                  ADMIN_PRIV | ADMIN_LICENSED, GROUP_ADMIN_LDAP },
        { "/login",                   AdminLogin,                 ADMIN_UNAUTH      },
        { "/logout",                  AdminLogout,                ADMIN_UNAUTH      },
        { "/menu",                    AdminMenu,                  ADMIN_UNAUTH | ADMIN_LICENSED },
        { "/messages",                AdminMessages,              ADMIN_PRIV, GROUP_ADMIN_MESSAGES },
        { "/mygroups",                AdminMygroups,              0                 },
        { "/network",                 AdminNetwork,               ADMIN_PRIV        },
        { "/ovid.html",               AdminOvidHtml,              ADMIN_UNAUTH      },
        { "/overdrive",               AdminOverDrive,             ADMIN_LICENSED    },
        { "/password",                AdminLogin,                 0                 },
        { "/proxy_url",               AdminProxyUrl,              ADMIN_UNAUTH      },
        { WSPUBLIC,                   AdminWebServer,             ADMIN_UNAUTH | ADMIN_LICENSED },
        { "/reboot",                  AdminReboot,                ADMIN_UNAUTH      },
        { "/referer",                 AdminReferer,               ADMIN_UNAUTH | ADMIN_LICENSED },
        { "/restart",                 AdminRestart,               ADMIN_PRIV, GROUP_ADMIN_RESTART },
        { "/robots.txt",              AdminRobots,                ADMIN_UNAUTH      },
        { "/save",                    AdminSave,                  ADMIN_PRIV        },
        { "/scifinder",               AdminSciFinder,             ADMIN_LICENSED    },
        { "/security/exemptions",     AdminSecurityExemptions,    ADMIN_PRIV, GROUP_ADMIN_SECURITY },
        { "/security/notifications",  AdminSecurityNotifications, ADMIN_PRIV, GROUP_ADMIN_SECURITY },
        { "/security/purge",          AdminSecurityPurge,         ADMIN_PRIV, GROUP_ADMIN_SECURITY },
        { "/security/rules",          AdminSecurityRules,         ADMIN_PRIV, GROUP_ADMIN_SECURITY },
        { "/security/tripped",        AdminSecurityTripped,       ADMIN_PRIV, GROUP_ADMIN_SECURITY },
        { "/security/vacuum",         AdminSecurityVacuum,        ADMIN_PRIV, GROUP_ADMIN_SECURITY },
        { "/security",                AdminSecurity,              ADMIN_PRIV, GROUP_ADMIN_SECURITY },
        { "/shibboleth",              AdminShibboleth,            ADMIN_PRIV, GROUP_ADMIN_SHIBBOLETH },
        { "/Shibboleth.sso/DS",       AdminShibbolethSSODS,       ADMIN_UNAUTH      },
        { "/Shibboleth.sso/Metadata", AdminShibbolethSSOMetadata, ADMIN_UNAUTH      },
        { AdminShibbolethSSOSAML_URL_STEM,     AdminShibbolethSSOSAML,     ADMIN_UNAUTH      },
        { AdminShibbolethSSOSAML_URL_STEM "2",    AdminShibbolethSSOSAML2,    ADMIN_UNAUTH      },
        // { "/sliptest",                AdminSlipTest,              ADMIN_PRIV        },
        { "/sortable.js",             AdminSortable,              ADMIN_UNAUTH      },
        { SHIBBOLETHSHIREURL,         AdminShibbolethShire,       ADMIN_UNAUTH      },
        { "/ssl",                     AdminSsl,                   ADMIN_PRIV, GROUP_ADMIN_SSL_VIEW "+" GROUP_ADMIN_SSL_UPDATE },
        { "/ssl-import",              AdminSslImport,             ADMIN_PRIV, GROUP_ADMIN_SSL_UPDATE },
        { "/ssl-new",                 AdminSslNew,                ADMIN_PRIV, GROUP_ADMIN_SSL_UPDATE },
        { "/sso",                     AdminSSO,                   ADMIN_MAYBE_LOGIN | ADMIN_LICENSED },
        { "/status",                  AdminStatus,                ADMIN_PRIV, GROUP_ADMIN_STATUS_UPDATE "+" GROUP_ADMIN_STATUS_VIEW },
        { "/status-reset",            AdminStatusReset,           ADMIN_PRIV        },
        { "/status-cookies",          AdminStatusCookies,         ADMIN_PRIV        },
        { "/time",                    AdminTime,                  ADMIN_UNAUTH      },
        { "/token",                   AdminToken,                 ADMIN_PRIV, GROUP_ADMIN_TOKEN },
        { "/translates",              AdminTranslates,            ADMIN_PRIV        },
        { "/usage",                   AdminUsage,                 ADMIN_PRIV, GROUP_ADMIN_USAGE },
        { "/usagelimits",             AdminUsageLimits,           ADMIN_PRIV, GROUP_ADMIN_USAGELIMITS },
        { "/userobject",              AdminUserObject,            ADMIN_UNAUTH | ADMIN_LICENSED },
        { "/user",                    AdminUser,                  ADMIN_PRIV | ADMIN_LICENSED, GROUP_ADMIN_USER },
        /* Maintain historic /usr URL for those who may have it bookmarked */
        { "/usr",                     AdminUser,                  ADMIN_PRIV | ADMIN_LICENSED },
        { "/variables",               AdminVariables,             ADMIN_PRIV, GROUP_ADMIN_VARIABLES },
        { WSWELLKNOWN,                AdminWebServer,             ADMIN_UNAUTH | ADMIN_LICENSED },
        { NULL,                       NULL,                       0                 },
    };
// clang-format off

struct DISPATCH *FindLoginDispatch(char *url) {
    struct DISPATCH *dp;
    size_t len;
    char *slash;

    for (dp = loginDispatchTable; dp->cmd; dp++) {
        len = strlen(dp->cmd);
        if (strnicmp(url, dp->cmd, len) != 0)
            continue;
        slash = url + len;
        if (*slash != 0 && *slash != '/' && *slash != '?')
            continue;
        return dp;
    }
    return NULL;
}

BOOL LoadPostData(struct UUSOCKET *s, int contentLength, char **post) {
    char *p;

    if (!(*post = malloc(contentLength + 1024)))
        PANIC;
    p = *post;
    *p = 0;
    while (contentLength > 0) {
        int len = UURecv(s, p, contentLength, 0);
        if (len <= 0) {
            return FALSE;
        }
        p += len;
        *p = 0;
        contentLength -= len;
    }

    return TRUE;
}

void DoAdmin(struct TRANSLATE *t) {
    struct UUSOCKET *s;
    struct SESSION *e;
    char *query;
    struct DISPATCH *dp;
    intmax_t contentLength;
    char *post = NULL;
    char *field;
    char *p;
    intmax_t maxContentLength;

    t->finalStatus = 950;
    /* StripScripts(t->url); */
    EncodeAngleBrackets(t->url, sizeof(t->url));
    s = &t->ssocket;

    if (DenyIfGoogleWebAccelerator(t))
        goto Finished;

    t->ifModifiedSince = 0;
    if (p = FindField("If-Modified-Since", t->requestHeaders)) {
        LinkFields(p);
        t->ifModifiedSince = ParseHttpDate(p, 0);
    }

    e = t->session;

    if (e != NULL && strnicmp(t->url, "/http://", 8) == 0) {
        strcpy(t->buffer, t->url + 1);
        AdminRedirect(t);
        goto Finished;
    }

    contentLength =
        (field = FindField("Content-Length", t->requestHeaders)) != NULL ? atoll(field) : 0;

    if (contentLength > 0 && strcmp(t->method, "HEAD") != 0) {
        if (strncmp(t->url, AdminShibbolethSSOSAML_URL_STEM,
                    strlen(AdminShibbolethSSOSAML_URL_STEM)) == 0) {
            maxContentLength = AdminShibbolethSSOSAML_POST_DATA_SIZE_LIMIT;
        } else {
            maxContentLength = TYPICAL_POST_DATA_SIZE_LIMIT;
        }
        if (contentLength > maxContentLength) {
            Log("Content-Length stated %" PRIdMAX " bytes which is more than the limit of %" PRIdMAX
                ", HTTP transaction ignored",
                contentLength, maxContentLength);
            goto Finished;
        }
        if (debugLevel >= 8000) {
            Log("Content-Length stated %" PRIdMAX ", limit %" PRIdMAX, contentLength,
                maxContentLength);
        }
        if (!LoadPostData(s, contentLength, &post)) {
            if (debugLevel >= 8000) {
                Log("Content read error, HTTP transaction ignored");
            }
            goto Finished;
        }
    }

    if (t->host && t->host->myDb && t->host->myDb->netLibraryConfigs) {
        if (query = strchr(t->url, '?'))
            query++;
        AdminNetLibrary(t, query, post);
        goto Finished;
    }

    if (t->host && t->host->myDb && t->host->myDb->fmgConfigs) {
        if (query = strchr(t->url, '?'))
            query++;
        AdminFMG(t, query, post);
        goto Finished;
    }

    if (t->host && t->host->myDb && t->host->myDb->myiLibraryConfigs) {
        if (query = strchr(t->url, '?'))
            query++;
        AdminMyiLibrary(t, query, post);
        goto Finished;
    }

    /* Transform /login/? to /login? and /login/up to just /login */
    if (strnicmp(t->url, "/login/", 7) == 0) {
        char *l = t->url + 7;
        /* Find and adjust the /login/? case */
        if (*l == 0 || *l == '?')
            StrCpyOverlap(l - 1, l);
        else if (strnicmp(l, "up", 2) == 0 && (*(l + 2) == 0 || *(l + 2) == '?'))
            StrCpyOverlap(t->url + 6, t->url + 9);
    }

    /* Parameter to /login/(ref)/ are not part of the query string, they are parameters to the URL
     */
    if (strnicmp(t->url, "/login/", 7) == 0) {
        query = NULL;
    } else {
        query = strchr(t->url, '?');
    }

    if (t->host == mvHost || t->host == mvHostSsl) {
        AdminMv(t, query, post);
        goto Finished;
    }

    if (e && e->userLimitExceeded) {
        if (strnicmp(t->url, "/logout", 7) != 0) {
            AdminUserLimitExceeded(t, query, post);
            goto Finished;
        }
    }

    if (e && UsageLimitExceeded(t, e->usageLimitExceeded, ULEVOLUME)) {
        if (strnicmp(t->url, "/logout", 7) != 0) {
            AdminUsageLimitExceeded(t, query, post);
            goto Finished;
        }
    }

    if (dp = FindLoginDispatch(t->url)) {
        BOOL mustLogin = TRUE;
        if ((dp->flags & ADMIN_LICENSED) && AdminLicenseInvalid(t)) {
            goto Finished;
        }
        if ((dp->flags & ADMIN_MAYBE_LOGIN)) {
            mustLogin = AdminMaybeLogin(t, dp, query, post);
        }
        if ((dp->flags & ADMIN_UNAUTH) == 0 && mustLogin) {
            if (e == NULL) {
                AdminRelogin(t);
                goto Finished;
            }
            if (AdminReloginIfRequired(t))
                goto Finished;
        }

        if ((dp->flags & ADMIN_PRIV) != 0) {
            if (e && e->priv == 0 && e->autoLoginBy != autoLoginByNone) {
                AdminRelogin(t);
                goto Finished;
            }
            if (e == NULL ||
                (e->priv == 0 && (dp->gm == NULL || GroupMaskOverlap(e->gm, dp->gm) == 0))) {
                AdminUnauthorized(t);
                goto Finished;
            }
        }

        if ((dp->flags & (ADMIN_PRIV | ADMIN_HTTPS)) != 0 && optionForceHttpsAdmin &&
            t->useSsl == 0 && myUrlHttps) {
            AdminForceHttpsAdmin(t);
            goto Finished;
        }

        if (query)
            query++;
        t->finalStatus = 200;
        (*dp->func)(t, query, post);
        MergeSessionVariables(t, e);
        goto Finished;
    }

    HTTPCode(t, 404, 1);

Finished:
    FreeThenNull(post);
    /* AdminGet can leave the socket in raw transfer mode, so close down only if that's not the case
     */
    if (t->raw == 0) {
        EndTranslate(t, 1);
    }
}
