/**
 * SirsiDynix HIP (Horizon Information Portal) 3.x user authentication
 *
 * This module implements user authentication against the SirsiDynix
 * HIP (Horizon Information Portal) 3.x.  It is not compatible with
 * HIP 4.x, which was a limited release of SirsiDynix's next-generation
 * platform that was killed before it went into broad distribution.
 *
 * This authentication method supports common conditions and actions.
 */

#define __UUFILE__ "usrhip.c"

#include "common.h"

#include "usr.h"
#include "usrhip.h"

#include "uuxml.h"

struct HIPCTX {
    char *overviewSubmenu;
    char *profileSubmenu;
    char *profile;
    xmlChar *itemsOverdue;
    xmlChar *itemsLost;
    xmlChar *balance;
    xmlChar *location;
};

void UsrHIPFreeXML(xmlParserCtxtPtr *ctxt, xmlXPathContextPtr *xpathCtx) {
    if (xpathCtx && *xpathCtx) {
        xmlXPathFreeContext(*xpathCtx);
        *xpathCtx = NULL;
    }

    if (ctxt && *ctxt) {
        xmlDocPtr doc = (*ctxt)->myDoc;
        xmlFreeParserCtxt(*ctxt);
        *ctxt = NULL;
        /* Even though the doc is initially part of the parser context, the call above
           to xmlFreeParserCtxt does not free the document.
        */
        if (doc) {
            xmlFreeDoc(doc);
            doc = NULL;
        }
    }
}

static char *UsrHIPAuthGetValue(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                void *context,
                                const char *var,
                                const char *index) {
    struct HIPCTX *hipCtx = (struct HIPCTX *)context;
    char *val = NULL;

    if (strcmp(var, "ItemsOverdue") == 0)
        val = (char *)hipCtx->itemsOverdue;
    else if (strcmp(var, "ItemsLost") == 0)
        val = (char *)hipCtx->itemsLost;
    else if (strcmp(var, "Balance") == 0)
        val = (char *)hipCtx->balance;
    else if (strcmp(var, "Location") == 0)
        val = (char *)hipCtx->location;
    else
        UsrLog(uip, "Unknown HIP auth variable: %s", var);

    if (val)
        if (!(val = strdup(val)))
            PANIC;

    return val;
}

int UsrHIPIfTest(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct HIPCTX *hipCtx = (struct HIPCTX *)context;
    char *arg2;
    char compareOperator;
    char compareType;
    xmlChar *val;
    int result = 1;

    arg2 = SkipNonWS(arg);
    if (*arg2) {
        *arg2++ = 0;
        arg2 = SkipWS(arg2);
    }

    arg2 = UsrTestComparisonOperatorType(arg2, &compareOperator, &compareType);

    if (stricmp(arg, "ItemsOverdue") == 0)
        val = hipCtx->itemsOverdue;
    else if (stricmp(arg, "ItemsLost") == 0)
        val = hipCtx->itemsLost;
    else if (stricmp(arg, "Balance") == 0)
        val = hipCtx->balance;
    else if (stricmp(arg, "Location") == 0)
        val = hipCtx->location;
    else {
        UsrLog(uip, "HIP unknown test variable: %s", arg);
        return 1;
    }

    if (val == NULL)
        val = BAD_CAST "";

    result = UsrTestCompare((char *)val, arg2, compareOperator, compareType) ? 0 : 1;

    if (uip->debug)
        UsrLog(uip, "HIP test %s %s against %s result %s", arg, val, arg2,
               result ? "FALSE" : "TRUE");

    return result;
}

int UsrHIPOverviewSubmenu(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct HIPCTX *hipCtx = (struct HIPCTX *)context;

    ReadConfigSetString(&hipCtx->overviewSubmenu, arg);
    return 0;
}

int UsrHIPProfile(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct HIPCTX *hipCtx = (struct HIPCTX *)context;

    ReadConfigSetString(&hipCtx->profile, arg);
    return 0;
}

int UsrHIPProfileSubmenu(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct HIPCTX *hipCtx = (struct HIPCTX *)context;

    ReadConfigSetString(&hipCtx->profileSubmenu, arg);
    return 0;
}

int UsrHIPURL(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    int result = 1;
    char *user, *pass;
    struct UUSOCKET rr, *r = &rr;
    char line[512];
    BOOL header;
    char save;
    struct PROTOCOLDETAIL *protocolDetail;
    char *host;
    char *colon;
    char *endOfHost, *endOfHostPort;
    PORT port;
    xmlChar *session = NULL;
    char *redirect = NULL;
    int phase;
    struct HIPCTX *hipCtx = (struct HIPCTX *)context;
    xmlParserCtxtPtr ctxt = NULL;
    xmlXPathContextPtr xpathCtx = NULL;

    UUInitSocket(r, INVALID_SOCKET);

    user = uip->user ? uip->user : "";
    pass = uip->pass ? uip->pass : "";

    if (*user == 0 || strlen(user) > 32 || strlen(pass) > 32)
        goto Finished;

    if (!(ParseProtocolHostPort(arg, "http://", &protocolDetail, &host, &endOfHost, &colon, &port,
                                NULL, &endOfHostPort)))
        goto Finished;

    if (protocolDetail->protocol > PROTOCOLHTTPS)
        goto Finished;

    uip->result = resultRefused;

    for (phase = 0; phase < 4; phase++) {
        UsrHIPFreeXML(&ctxt, &xpathCtx);

        if (phase > 0 && session == NULL)
            break;

        if (save = *endOfHost)
            *endOfHost = 0;

        if (UUConnectWithSource2(r, host, port, "FindUserHIP", uip->pInterface,
                                 (char)protocolDetail->protocol))
            goto Finished;

        if (save)
            *endOfHost = save;

        if (phase == 0)
            uip->result = resultInvalid;

        strcpy(line, *endOfHostPort ? endOfHostPort : "/ipac20/ipac.jsp");

        if (phase == 0) {
            WriteLine(r, "GET %s?GetXML=true&auth=true", line);
        } else if (phase == 1) {
            WriteLine(r, "POST %s", line);
        } else {
            strcat(line, "?session=");
            AddEncodedField(line, (char *)session, NULL);
            strcat(line, "&profile=");
            AddEncodedField(line, hipCtx->profile, NULL);
            strcat(line, "&menu=account&submenu=");
            if (phase == 2)
                AddEncodedField(
                    line, hipCtx->overviewSubmenu ? hipCtx->overviewSubmenu : "overview", NULL);
            else
                AddEncodedField(line, hipCtx->profileSubmenu ? hipCtx->profileSubmenu : "info",
                                NULL);
            strcat(line, "&GetXML=true");
            WriteLine(r, "GET %s", line);
        }

        UUSendZ(r, " HTTP/1.0\r\n");

        save = *endOfHostPort;
        *endOfHostPort = 0;
        WriteLine(r, "Host: %s\r\n", host);
        *endOfHostPort = save;
        UUSendZ(r, "Connection: close\r\n");

        if (phase == 1) {
            strcpy(line, "GetXML=true&menu=home&subtab=subtab11&sec1=");
            AddEncodedField(line, user, NULL);
            if (pass && *pass) {
                strcat(line, "&sec2=");
                AddEncodedField(line, pass, NULL);
            }
            strcat(line, "&button=Login&login_prompt=true&session=");
            AddEncodedField(line, (char *)session, NULL);
            UUSendZ(r, "Content-Type: application/x-www-form-urlencoded\r\n");
            WriteLine(r, "Content-Length: %d\r\n\r\n", strlen(line));
            UUSendZCRLF(r, line);
        } else {
            UUSendCRLF(r);
        }

        header = 1;
        while (ReadLine(r, line, sizeof(line))) {
            if (debugLevel > 0 && uip->debug)
                UsrLog(uip, "HIP response %s", line);

            if (header) {
                if (line[0] == 0) {
                    header = 0;
                    break;
                }
                continue;
            }
        }

        if (header == 0) {
            size_t len;
            while ((len = UURecv(r, line, sizeof(line) - 1, 0)) > 0) {
                line[len] = 0;
                if (debugLevel > 0 && uip->debug)
                    UsrLog(uip, "HIP response %s", line);
                if (ctxt == NULL) {
                    ctxt = xmlCreatePushParserCtxt(NULL, NULL, line, len, NULL);
                    if (ctxt == NULL) {
                        UsrLog(uip, "Non-XML response from HIP");
                        goto Finished;
                    }
                } else {
                    xmlParseChunk(ctxt, line, len, 0);
                }
            }
        }

        UUStopSocket(r, 0);

        if (ctxt == NULL) {
            UsrLog(uip, "HIP server did not respond\n");
            goto Finished;
        }

        xmlParseChunk(ctxt, line, 0, 1);
        xpathCtx = xmlXPathNewContext(ctxt->myDoc);
        if (xpathCtx == NULL) {
            UsrLog(uip, "Error: unable to create new XPath context");
            goto Finished;
        }

        if (phase == 0) {
            session = UUxmlSimpleGetContent(NULL, xpathCtx, "//session");
            if (session == NULL)
                break;
            if (hipCtx->profile == NULL) {
                xmlChar *profile = UUxmlSimpleGetContent(NULL, xpathCtx, "//profile");
                if (profile == NULL) {
                    UsrLog(uip, "Error: HIP unable to locate default profile");
                    break;
                }
                if (!(hipCtx->profile = strdup((char *)profile)))
                    PANIC;
                xmlFree(profile);
                profile = NULL;
            }
        }

        if (phase == 1) {
            xmlChar *auth = UUxmlSimpleGetContent(NULL, xpathCtx, "//security/auth");
            BOOL authTrue = FALSE;
            if (auth) {
                if (stricmp((char *)auth, "true") == 0)
                    authTrue = TRUE;
                xmlFree(auth);
                auth = NULL;
            }
            if (authTrue == 0)
                break;
            uip->result = resultValid;
        }

        if (phase == 2) {
            hipCtx->itemsOverdue = UUxmlSimpleGetContent(NULL, xpathCtx, "//overview/itemsoverdue");
            hipCtx->itemsLost = UUxmlSimpleGetContent(NULL, xpathCtx, "//overview/itemslost");
            hipCtx->balance = UUxmlSimpleGetContent(NULL, xpathCtx, "//overview/balance");
        }

        if (phase == 3) {
            hipCtx->location = UUxmlSimpleGetContent(NULL, xpathCtx, "//patroninfo/location");
        }
    }

    if (uip->debug) {
        UsrLog(uip, "HIP results for %s", user);
        if (uip->result == resultValid) {
            if (hipCtx->location == NULL)
                UsrLog(uip, "Location blank, may need to override ProfileSubmenu");
            else
                UsrLog(uip, "Location: %s",
                       (char *)hipCtx->location ? (char *)hipCtx->location : "");
            UsrLog(uip, "ItemsOverdue: %s",
                   (char *)hipCtx->itemsOverdue ? (char *)hipCtx->itemsOverdue : "");
            UsrLog(uip, "ItemsLost: %s",
                   (char *)hipCtx->itemsLost ? (char *)hipCtx->itemsLost : "");
            UsrLog(uip, "Balance: %s", (char *)hipCtx->balance ? (char *)hipCtx->balance : "");
            if (hipCtx->itemsOverdue == NULL || hipCtx->itemsLost == NULL ||
                hipCtx->balance == NULL)
                UsrLog(uip, "May need to override OverviewSubmenu");
        } else {
            UsrLog(uip, "User did not authenticate");
        }
    }

Finished:

    UsrHIPFreeXML(&ctxt, &xpathCtx);

    FreeThenNull(redirect);
    xmlFreeThenNull(session);
    UUStopSocket(r, 0);

    return result;
}

enum FINDUSERRESULT FindUserHIP(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                char *file,
                                int f,
                                struct FILEREADLINEBUFFER *frb,
                                struct sockaddr_storage *pInterface) {
    struct USRDIRECTIVE udc[] = {
        {"IfTest",          UsrHIPIfTest,          0},
        {"OverviewSubmenu", UsrHIPOverviewSubmenu, 0},
        {"Profile",         UsrHIPProfile,         0},
        {"ProfileSubmenu",  UsrHIPProfileSubmenu,  0},
        {"URL",             UsrHIPURL,             0},
        {NULL,              NULL,                  0}
    };
    enum FINDUSERRESULT result;
    struct HIPCTX hipCtx;

    memset(&hipCtx, 0, sizeof(hipCtx));

    uip->flags = 0;
    uip->authGetValue = UsrHIPAuthGetValue;
    result = UsrHandler(t, "HIP", udc, &hipCtx, uip, file, f, frb, pInterface, NULL);

    xmlFreeThenNull(hipCtx.balance);
    xmlFreeThenNull(hipCtx.itemsLost);
    xmlFreeThenNull(hipCtx.itemsOverdue);
    xmlFreeThenNull(hipCtx.location);
    xmlFreeThenNull(hipCtx.profile);

    return result;
}
