#ifndef __SAML_H__
#define __SAML_H__

#include "common.h"

// xmlsec requires XMLSEC_NO_XSLT to exist
#ifndef XMLSEC_NO_XSLT
#define XMLSEC_NO_XSLT
#endif

#include <xmlsec/xmlsec.h>
#include <xmlsec/crypto.h>

#define METADATADOC_VALID (0)
#define METADATADOC_INVALID_SIGNATURE (1)
#define METADATADOC_INVALID_XML (2)
#define METADATADOC_INVALID_FETCH (3)
#define METADATADOC_FETCHING (4)

#define SESSION_VARIABLE_SAML_ISSUER "saml:issuer"
#define SESSION_VARIABLE_SAML_NAMEID "saml:nameID"
#define SESSION_VARIABLE_SAML_NAMEID_FORMAT "saml:nameIDFormat"
#define SESSION_VARIABLE_SAML_NAMEID_NAME_QUALIFIER "saml:nameIDNameQualifier"
#define SESSION_VARIABLE_SAML_NAMEID_SP_NAME_QUALIFIER "saml:nameIDSPNameQualifier"
#define SESSION_VARIABLE_SAML_SESSION_INDEX "saml:sessionIndex"

struct SHIBBOLETHSITE {
    /*
     * The filename is a unique key for the items in this list.
     */
    struct SHIBBOLETHSITE *next;
    UUMUTEX mutex;

    /*
     * The Entity ID for the execution instance when using this metadata
     * to participate in the federation represented by the metadata.  It
     * is the unique identifier of the Shibboleth Identity Provider (IDP)
     * or Service Provider (SP). This execution instance <strong>may</strong> be an
     * IDP and <strong>will</strong> be an SP.  If this is NULL, this metadata
     * will only be useful for Shibboleth 1.2 operation.
     */
    char *entityID;

    /*
     * This is the name of the file in which the metadata will be stored.
     * This is the only field required to be defined in the configuration file.
     */
    char *filename;
    int fileErrno;

    /*
     * This is the file change time, (struct stat).st_mtime, of the file.
     * It is recorded here only upon successful load of the metadata into the
     * metadata XML document.
     */
    time_t fileMTimeAsLoaded;

    /*
     * The time of the last attempt to retrieve the metadata from the
     * metadata retrieval end point URL.  You can set this to zero
     * without a lock to cause the file to be refreshed from the URL.
     */
    time_t urlAttempted;

    /*
     * The time of the last successful retrieval the metadata from the
     * metadata retrieval end point URL into the file.
     */
    time_t urlRetrieved;

    /*
     * This is the metadata XML document.  If it's not NULL the
     * document passed all tests for validity.
     */
    xmlDocPtr doc;
    xmlXPathContextPtr xPathCtx;
    int docFetchValid;

    /*
     * This is the private certificate(s) used to decrypt assertions that are delivered
     * to this execution instance by IDPs.  It is not used in making SSL
     * connections to servers.  The active certificate is used for that.
     */
    int certificateCount;
    int *certificates;

    /*
     * This is a list of public certificates that are explicitly trusted for the purpose
     * of validating metadata.  The XML Encryption Syntax and Processing Version 1.1
     * specification is being used.  The certs are loaded into the keysMngrForEncryption.
     */
    char *signCertList;
    int keyForSignatureCount;
    int keyLoadedForSignatureCount;
    xmlSecKeysMngrPtr keysMngrForSignature;

    /*
     * This is the interface thru which this execution instance will make out-bound
     * HTTP connections to retrieve metadata and to retrieve Assertion Consumer
     * Service assertions.
     */
    struct sockaddr_storage ipInterface;

    /*
     * This is the metadata retrieval endpoint URL.  If it is NULL, the
     * content of the file named herein is static, or managed manually.
     */
    char *url;
    int urlErrno;
    int urlUUErrno;
    int urlHTTPCode;

    /*
     * The data for an optional samlp:RequestedAuthnContext/saml:AuthnContextClassRef
     * data element.
     */
    char *authnContextClassRef;

    char *proxyHost;
    char *proxyAuth;
    char *proxySslHost;
    char *proxySslAuth;
    char urlSsl;
    char *urlHost;
    char *urlRequest;
    xmlSecTransformId redirectSignatureMethod;
    PORT urlPort;
    BOOL urlDefaultPort;
    PORT proxyPort;
    PORT proxySslPort;
    BOOL signAuthnRequest;
    BOOL slo;
    BOOL signResponse;
    BOOL signAssertion;
    BOOL encryptAssertion;
};

struct SAMLATTRIBUTE {
    struct SAMLATTRIBUTE *next;
    char *name;
    char *friendlyName;
    char *scope;
    char *value;
    char *path;
    BOOL valid;
    BOOL signedValue;
    BOOL encryptedValue;
    BOOL httpsGetMethod;
    BOOL rejected;
};

struct SAMLARTIFACT {
    struct SHIBBOLETHSITE *shibbolethSite;
    xmlChar *entityID;
    unsigned char sourceId[20];
    unsigned char messageHandle[20];
    BOOL valid;
    size_t sourceIdLen;
    size_t messageHandleLen;
    unsigned short typeCode;
    unsigned short endPointIndex;
};

enum SHIBBOLETHAVAILABLE {
    SHIBBOLETHAVAILABLEOFF,
    SHIBBOLETHAVAILABLEDISABLED,
    SHIBBOLETHAVAILABLEON
};

extern enum SHIBBOLETHAVAILABLE shibbolethAvailable12, shibbolethAvailable13, shibbolethAvailable20;

enum SHIBBOLETHVERSION { SHIBVERSION13 = 13, SHIBVERSION20 = 20, SHIBVERSION13OR20 = 33 };

#define SHIBBOLETHSITEREFRESH (ONE_DAY_IN_SECONDS)

#define SHIBOLDUSR "shib.usr"
#define SHIBNEWUSR "shibuser.txt"

extern struct SHIBBOLETHSITE *shibbolethSites;

extern char *shibbolethSitesCommonEntityID;

BOOL ShibbolethAvailable(void);
char *SAMLGetOrgnizationName(struct SHIBBOLETHSITE *ss, char *entityID);
BOOL RetrieveShibbolethMetadata(struct SHIBBOLETHSITE *ss, BOOL holdMutex);
BOOL LoadUpdateShibbolethMetadata(struct SHIBBOLETHSITE *ss, BOOL holdMutex);
void RetrieveLoadUpdateShibbolethMetadatas(void);
xmlChar *SAMLShibbolethMetadataValue(char *xPathMain,
                                     char *subValue,
                                     char *subValue2,
                                     struct SHIBBOLETHSITE **ssMatch,
                                     BOOL holdMutex);
xmlChar *SAMLSSOLocation(char *entityID,
                         struct SHIBBOLETHSITE **ss,
                         enum SHIBBOLETHVERSION shibVersion,
                         BOOL *redirect,
                         enum SHIBBOLETHVERSION *foundVersion);
void SAMLAuthnRequest(struct TRANSLATE *t,
                      xmlChar *location,
                      char *relayState,
                      struct SHIBBOLETHSITE *ss,
                      BOOL redirect);
BOOL SAMLLogoutRequest(struct TRANSLATE *t,
                       char *issuer,
                       char *nameID,
                       char *nameIDFormat,
                       char *nameIDNameQualifier,
                       char *nameIDSPNameQualifier,
                       char *sessionIndex);
void SAMLSendURLforDS(struct UUSOCKET *s, char *url, char *wayf, char *entityID, char *tag);
void SAMLSendShibbolethMetadata(struct TRANSLATE *t,
                                int certificate,
                                struct SHIBBOLETHSITE *ssSpecific,
                                BOOL forceSLO);

BOOL AdminShibbolethUnavailable(struct TRANSLATE *t,
                                enum SHIBBOLETHAVAILABLE shibbolethAvailableValue,
                                char *version);
void AdminShibbolethSSODS(struct TRANSLATE *t, char *query, char *post);
void AdminShibbolethSSOLogin(struct TRANSLATE *t, char *query, char *post);
void AdminShibbolethSSOMetadata(struct TRANSLATE *t, char *query, char *post);
void AdminShibbolethSSOSAML(struct TRANSLATE *t, char *query, char *post);
void AdminShibbolethSSOSAML2(struct TRANSLATE *t, char *query, char *post);
void SendShibFailure(struct TRANSLATE *t);

#endif  // __SAML_H__
