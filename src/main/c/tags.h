#ifndef __TAGS_H__
#define __TAGS_H__

#define TAG_DOCTYPE_HTML "<!DOCTYPE html>\n<html lang='en'>\n"

// Incorporating the meta viewport tag by default should improve admin access on
// small screens. However, should that prove to be problematic, they can be separated
// by using the following instead.
// #define TAG_DOCTYPE_HTML_HEAD   TAG_DOCTYPE_HTML "<head>\n"
// #define TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT   TAG_DOCTYPE_HTML_HEAD "<meta name='viewport'
// content='width=device-width, initial-scale=1.0'>\n"

#define TAG_DOCTYPE_HTML_HEAD \
    TAG_DOCTYPE_HTML          \
        "<head>\n<meta name='viewport' content='width=device-width, initial-scale=1.0'>\n"
#define TAG_DOCTYPE_HTML_HEAD_META_VIEWPORT TAG_DOCTYPE_HTML_HEAD

#endif  // __TAGS_H__
