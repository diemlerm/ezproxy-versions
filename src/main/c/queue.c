#define __UUFILE__ "queue.c"

#include "common.h"
#include "queue.h"

struct QUEUENODE {
    struct QUEUENODE *next;
    void *element;
};

struct QUEUE {
    UUMUTEX lock;
    struct QUEUENODE *front, *rear;
    char *name;
    int count;
    BOOL allowNullElement;
};

struct QUEUE *QueueNew(char *name, BOOL allowNullElement) {
    struct QUEUE *q;

    if (!(q = calloc(1, sizeof(*q))))
        PANIC;
    if (!(q->name = strdup(name)))
        PANIC;
    q->allowNullElement = allowNullElement;
    UUInitMutex(&q->lock, name);
    return q;
}

BOOL QueueIsEmpty(struct QUEUE *q) {
    BOOL empty;

    UUAcquireMutex(&q->lock);
    empty = q->front == NULL;
    UUReleaseMutex(&q->lock);
    return empty;
}

int QueueSize(struct QUEUE *q) {
    int count;

    UUAcquireMutex(&q->lock);
    count = q->count;
    UUReleaseMutex(&q->lock);
    return count;
}

void QueueEnqueue(struct QUEUE *q, void *element) {
    struct QUEUENODE *n;

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;
    n->element = element;
    UUAcquireMutex(&q->lock);
    if (element == NULL && !q->allowNullElement)
        PANIC;
    if (q->rear == NULL) {
        q->front = q->rear = n;
    } else {
        q->rear->next = n;
        q->rear = n;
    }
    q->count++;
    UUReleaseMutex(&q->lock);
}

void *QueueDequeue(struct QUEUE *q) {
    struct QUEUENODE *n;
    void *element = NULL;

    UUAcquireMutex(&q->lock);
    if (q->front) {
        n = q->front;
        q->front = q->front->next;
        if (q->front == NULL) {
            q->rear = NULL;
        }
        element = n->element;
        free(n);
        q->count--;
    }
    UUReleaseMutex(&q->lock);

    return element;
}

void *QueuePeek(struct QUEUE *q) {
    struct QUEUENODE *n;
    void *element = NULL;

    UUAcquireMutex(&q->lock);
    if (q->front) {
        element = q->front->element;
    }
    UUReleaseMutex(&q->lock);

    return element;
}

void QueueDestroy(struct QUEUE **q) {
    if (*q) {
        while (!QueueIsEmpty(*q)) {
            QueueDequeue(*q);
        }
        FreeThenNull(*q);
    }
}
