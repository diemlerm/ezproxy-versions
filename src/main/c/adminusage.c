/**
 * /usage admin URL to view ezproxy.log.
 */

#define __UUFILE__ "adminusage.c"

#include "common.h"
#include "uustring.h"

#include "adminusage.h"

void AdminUsage(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    const int AULAST = 0;
    const int AUFIND = 1;
    struct FORMFIELD ffs[] = {
        {"last", NULL, 0, 0, 64},
        {"find", NULL, 0, 0, 0 },
        {NULL,   NULL, 0, 0, 0 }
    };
    char *fn;
    int last;
    char lastBuffer[SEPARATE_INT_BUFFER_LEN];
    char title[256];

    fn = LogSPUActiveFilename(NULL, 0);
    FindFormFields(query, post, ffs);
    last = atoi(ffs[AULAST].value ? ffs[AULAST].value : "0");
    strcpy(title, "View " EZPROXYNAMEPROPER " Log File");
    if (last >= 0) {
        if (last > 0) {
            sprintf(AtEnd(title), " - Last %s", SeparateInt(last, lastBuffer));
        } else {
            strcat(title, " - All");
        }
    }
    AdminHeader(t, 0, title);
    UUSendZ(s, "<pre>\n");
    if (ffs[AUFIND].value)
        SendFileMatches(t, fn, SFREPORTIFMISSING | SFQUOTEDURL, ffs[AUFIND].value, 0);
    else
        SendFileTail(t, fn, SFREPORTIFMISSING | SFQUOTEDURL,
                     atoi(ffs[AULAST].value ? ffs[AULAST].value : "0"));
    UUSendZ(s, "</pre>\n");
    AdminFooter(t, 1);
}
