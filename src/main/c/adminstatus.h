#ifndef __ADMINSTATUS_H__
#define __ADMINSTATUS_H__

#include "common.h"

void AdminStatus(struct TRANSLATE *t, char *query, char *post);
void AdminStatusReset(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINSTATUS_H__
