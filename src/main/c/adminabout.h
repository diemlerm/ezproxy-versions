#ifndef __ADMINABOUT_H__
#define __ADMINABOUT_H__

#include "common.h"

void AdminAbout(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINABOUT_H__
