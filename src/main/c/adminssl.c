/**
 * /ssl admin URL to manage SSL certificates.
 */

#define __UUFILE__ "adminssl.c"

#include "common.h"
#include "uustring.h"

#include "adminssl.h"
#include "saml.h"

extern BIO *bio_err;

void AdminSslHeader(struct TRANSLATE *t, int options, char *title) {
    AdminBaseHeader(t, options, title, " | <a href='/ssl'>Manage SSL (https) certificates</a>");
}

BOOL AdminSslDisabled(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket;

    if (UUSslEnabled())
        return 0;

    HTMLHeader(t, htmlHeaderNoCache);

    UUSendZ(s, "<p>Due to export restrictions, a key is required to activate SSL features.</p>\n");
    UUSendZ(s, "<p>For a demo key, contact " EZPROXYHELPEMAIL ".</p>\n");
    return 1;
}

void AdminSslList(struct TRANSLATE *t, BOOL update) {
    struct UUSOCKET *s = &t->ssocket;
    struct UUSSLFILES *head, *p;
    SSL_CTX *ctx;
    BOOL anyValid = 0;
    char created[MAXASCIIDATE];

    if (AdminSslDisabled(t))
        return;

    AdminHeader(t, 0, "Manage SSL (https) Certificates");

    UUFlush(s);

    head = UUSslFiles(NULL, 1);

    if (head != head->flink) {
        UUSendZ(s, "<table class='bordered-table'>\n");
        UUSendZ(s,
                "<tr valign=bottom align=center><th scope='col'>ID</th><th "
                "scope='col'>Created</th><th scope='col'>Status</th></tr>");

        for (p = head->flink; p != head; p = p->flink) {
            UUSslKeyCreated(p->serial, created, sizeof(created));
            WriteLine(s, "<tr><td><a href='/ssl?entry=%d'>%d</a></td><td>%s</td><td>", p->serial,
                      p->serial, created);
            if (p->serial == sslActiveIndex)
                UUSendZ(s, "<strong>Active Certificate</strong>");
            else if (p->flags & UUSSLFILESCRT) {
                ctx = UUSslLoadKey(p->serial, 0, NULL, 0, NULL);
                if (ctx) {
                    SSL_CTX_free(ctx);
                    UUSendZ(s, "Certificate");
                    anyValid = 1;
                } else
                    UUSendZ(s, "Damaged Certificate");
            } else if (p->flags & UUSSLFILESCSR) {
                UUSendZ(s, "Certificate Signing Request (CSR)");
            } else
                UUSendZ(s, "Damaged");
            UUSendZ(s, "</td></tr>\n");
        }

        UUSendZ(s, "</table>\n");

        if (sslActiveIndex == -1 && anyValid) {
            UUSendZ(s,
                    "<p>None of your certificates are active on this server.  Please click on one "
                    "to receive the option to activate it.</p>\n");
        }
    }

    if (update) {
        UUSendZ(s, "<p><a href='/ssl-new'>Create New SSL Certificate</a>  \n");
        UUSendZ(s, "<br /><a href='/ssl-import'>Import Existing SSL Certificate</a></p>\n");
    }

    AdminFooter(t, 0);
}

BOOL AdminSslShowCert(struct TRANSLATE *t,
                      char *header,
                      BIO *mem,
                      BOOL full,
                      char *subjectAltNames) {
    struct UUSOCKET *s = &t->ssocket;
    char notBefore[64], notAfter[64];
    char line[256];
    char *colon;
    BOOL valid = 0;
    char *p;

    if (full)
        UUSendZ(s, "<pre>\n");
    notBefore[0] = notAfter[0] = 0;
    while (BIO_gets(mem, line, sizeof(line)) > 0) {
        if (full) {
            SendHTMLEncoded(s, line);
            continue;
        }
        Trim(line, TRIM_LEAD | TRIM_TRAIL);
        if (!(colon = strchr(line, ':')))
            continue;

        *colon++ = 0;
        Trim(line, TRIM_TRAIL);
        colon = SkipWS(colon);

        /* Remove () around RSA key size */
        if (*colon == '(' && (p = strchr(colon, 0) - 1) && *p == ')') {
            *p = 0;
            StrCpyOverlap(colon, colon + 1);
        }

        if (strcmp(line, "Not Before") == 0) {
            StrCpy3(notBefore, colon, sizeof(notBefore));
        }
        if (strcmp(line, "Not After") == 0) {
            StrCpy3(notAfter, colon, sizeof(notAfter));
        }
        if (strcmp(line, "Issuer") == 0 || strcmp(line, "Subject") == 0 ||
            strcmp(line, "RSA Public Key") == 0) {
            if (valid == 0) {
                WriteLine(s, "<strong>%s</strong>\n<p><table class='tight-table th-row-nobold'>\n",
                          header);
                valid = 1;
            }
            WriteLine(s, "<tr valign='top'><th scope='row'>%s:&nbsp;</th><td>", line);
            if (*colon) {
                FindReplace(colon, sizeof(line) - (colon - line),
                            "/2.5.4.9=", ", streetAddress=", FINDREPLACE_GLOBAL);
                FindReplace(colon, sizeof(line) - (colon - line),
                            "2.5.4.9=", "streetAddress=", FINDREPLACE_GLOBAL);
                FindReplace(colon, sizeof(line) - (colon - line),
                            "/2.5.4.17=", ", postalCode=", FINDREPLACE_GLOBAL);
                FindReplace(colon, sizeof(line) - (colon - line),
                            "2.5.4.17=", "postalCode=", FINDREPLACE_GLOBAL);
                SendQuotedURL(t, colon);
            }
            UUSendZ(s, "</td></tr>\n");
        }
    }
    if ((!full) && subjectAltNames && *subjectAltNames) {
        UUSendZ(s, "<tr><th scope='row'>Subject Alternate Name: </th><td>");
        char *p;
        BOOL first = 1;
        for (p = subjectAltNames; *p; p = strchr(p, 0) + 1) {
            if (first) {
                first = 0;
            } else {
                UUSendZ(s, ", ");
            }
            SendQuotedURL(t, p);
        }
        UUSendZ(s, "</td></tr>\n");
    }
    if (valid) {
        if (notBefore[0])
            WriteLine(s, "<tr><th scope='row'>Valid:&nbsp;</th><td>%s to %s</td></tr>\n", notBefore,
                      notAfter);
        UUSendZ(s, "</table>\n");
    }
    if (full)
        UUSendZ(s, "</pre>\n");
    return valid;
}

BOOL AdminLooksLikeACertificate(char *s) {
    char *e;

    /* This check used to be more thorough, but people seemed to be leaving things around
       the certificates that OpenSSL doesn't care about but EZproxy does, so be more
       tolerant now.
    */

    e = StrIStr(s, "-----BEGIN CERTIFICATE-----");

    if (e == NULL)
        return 0;

    if (StrIStr(e, "-----END CERTIFICATE-----") == NULL)
        return 0;

    return 1;

    /*
    if (strncmp(s, "-----BEGIN CERTIFICATE-----\n", 28) != 0)
        return 0;

    e = strchr(s, 0) - 25;
    if (e < s || strcmp(e, "-----END CERTIFICATE-----") != 0)
        return 0;

    return 1;
    */
}

BOOL AdminLooksLikeAKey(char *s) {
    char *e;

    e = StrIStr(s, "-----BEGIN RSA PRIVATE KEY-----");

    if (e == NULL)
        return 0;

    if (StrIStr(e, "-----END RSA PRIVATE KEY-----") == NULL)
        return 0;

    return 1;
}

BOOL AdminSslMatchesMyWildcardName(const char *s) {
    if (s == NULL) {
        return 0;
    }
    const char *p = StartsWith(s, "*.");
    return p != NULL && stricmp(p, myName) == 0;
}

BOOL AdminSslMatchesMyWildcardDomain(const char *s) {
    if (s == NULL || myDomain == NULL) {
        return 0;
    }
    const char *p = StartsWith(s, "*.");
    if (p == NULL) {
        return 0;
    }
    // If the domain starts with a period, back up one so that p points
    // at the period after the * so it will match.
    if (*myDomain == '.') {
        p--;
    }
    return stricmp(p, myDomain) == 0;
}

static void AdminSSLCheckWildcards(const char *cn,
                                   const char *subjectAltNames,
                                   BOOL *retValidPBHWildcard,
                                   BOOL *retAnyWildcard,
                                   BOOL *retDomainWildcard) {
    int i;
    BOOL validPBHWildcard = 0;
    BOOL anyWildcard = 0;
    BOOL domainWildcard = 0;

    if (cn != NULL && *cn == '*') {
        anyWildcard = 1;
        if (AdminSslMatchesMyWildcardName(cn)) {
            validPBHWildcard = 1;
        }
        if (AdminSslMatchesMyWildcardDomain(cn)) {
            domainWildcard = 1;
        }
    }

    if (subjectAltNames != NULL) {
        const char *p;
        for (p = subjectAltNames; *p; p = strchr(p, 0) + 1) {
            if (*p == '*') {
                anyWildcard = 1;
                if (AdminSslMatchesMyWildcardName(p)) {
                    validPBHWildcard = 1;
                }
                if (AdminSslMatchesMyWildcardDomain(p)) {
                    domainWildcard = 1;
                }
            }
        }
    }

    if (retValidPBHWildcard) {
        *retValidPBHWildcard = validPBHWildcard;
    }
    if (retAnyWildcard) {
        *retAnyWildcard = anyWildcard;
    }
    if (retDomainWildcard) {
        *retDomainWildcard = domainWildcard;
    }
}

BOOL AdminSslCheckWildcardsHttps(BOOL running) {
    if (optionProxyType != PTHOST || primaryLoginPortSsl == 0)
        return 0;

    BOOL newOptionWildcardHTTPS = 0;
    const char *cn = UUActiveCertCN();
    char *subjectAltNames = UUActiveCertSubjectAltNames();
    if (optionForceWildcardCertificate) {
        newOptionWildcardHTTPS = 1;
    } else if (!optionIgnoreWildcardCertificate) {
        AdminSSLCheckWildcards(cn, subjectAltNames, &newOptionWildcardHTTPS, NULL, NULL);
    }
    FreeThenNull(subjectAltNames);

    if (newOptionWildcardHTTPS == optionWildcardHTTPS) {
        return 0;
    }

    optionWildcardHTTPS = newOptionWildcardHTTPS;

    if (running) {
        SetMyUrlHttps();
        SetMyHttpsName();
        int i;
        struct HOST *h;
        for (i = 1, h = hosts + 1; i < cHosts; i++, h++) {
            if (h->useSsl)
                SetProxyHostname(h);
        }
    }

    return 1;
}

BOOL AdminSslCreateFile(char *filename, char *contents, BOOL addFinalNewline) {
    int len;
    int f;

    if (filename == NULL)
        return FALSE;

    f = UUopenCreateFD(filename, O_CREAT | O_WRONLY | O_TRUNC, defaultFileMode);
    if (f < 0) {
        Log("Unable to create %s: %d", filename, errno);
        unlink(filename); /* in case file actually got created */
        return FALSE;
    }

    if (contents && (len = strlen(contents))) {
        if (write(f, contents, len) != len) {
            Log("Failed writing to %s: %d", filename, errno);
            close(f);
            unlink(filename);
            return FALSE;
        }
    }

    if (addFinalNewline)
        write(f, "\n", 1);

    close(f);

    return TRUE;
}

void AdminSslImport(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    char fnKey[64] = {0};
    char fnCrt[64] = {0};
    char fnCnf[64] = {0};
    int serial;

    const int SNCRT = 0;
    const int SNKEY = 1;

    struct FORMFIELD ffs[] = {
        {"crt", NULL, 0, 0, 0, FORMFIELDNOTRIMCTRL},
        {"key", NULL, 0, 0, 0, FORMFIELDNOTRIMCTRL},
        {NULL, NULL, 0, 0, 0}
    };

    char *crt, *key;
    char newUrl[32];
    BOOL crtKnownInvalid = FALSE;
    BOOL keyKnownInvalid = FALSE;
    BOOL keyCrtKnownMismatch = FALSE;
    SSL_CTX *ctx = NULL;
    enum SSLLOADKEYERROR slke = SLKENONE;
    char asciiDate[MAXASCIIDATE];
    int f = -1;
    int provided;
    BOOL crtProvided = FALSE;
    BOOL keyProvided = FALSE;

    char created[256];
    time_t now;

    if (AdminSslDisabled(t))
        return;

    FindFormFields(query, post, ffs);
    crt = ffs[SNCRT].value;
    key = ffs[SNKEY].value;

    Trim(crt, TRIM_PEM_CLEANUP);
    Trim(key, TRIM_PEM_CLEANUP);

    provided = 0;
    if (((crt != NULL) && (*crt != 0))) {
        provided++;
        crtProvided = TRUE;
    }
    if (((key != NULL) && (*key != 0))) {
        provided++;
        keyProvided = TRUE;
    }

    if (provided == 2) {
        UUAcquireMutex(&mAdminSsl);

        UUSslFiles(&serial, 0);
        serial++;

        if (serial == -1) {
            UUReleaseMutex(&mAdminSsl);
            HTMLErrorPage(t, "Unable to determine new certificate number", NULL);
            return;
        }

        sprintf(fnCrt, "%s%08d.crt", SSLDIR, serial);
        sprintf(fnKey, "%s%08d.key", SSLDIR, serial);
        sprintf(fnCnf, "%s%08d.cnf", SSLDIR, serial);

        if (!AdminSslCreateFile(fnCrt, crt, TRUE)) {
            UUReleaseMutex(&mAdminSsl);
            sprintf(t->buffer, "Unable to create file %s", fnCrt);
            HTMLErrorPage(t, t->buffer, NULL);
            return;
        }

        if (!AdminSslCreateFile(fnKey, key, TRUE)) {
            unlink(fnCrt);
            UUReleaseMutex(&mAdminSsl);
            sprintf(t->buffer, "Unable to create file %s", fnKey);
            HTMLErrorPage(t, t->buffer, NULL);
            return;
        }

        f = UUopenCreateFD(fnCnf, O_CREAT | O_EXCL | O_WRONLY, defaultFileMode);
        UUtime(&now);
        sprintf(created, "#created %s\n#imported\n", ASCIIDate(&now, asciiDate));
        write(f, created, strlen(created));
        close(f);
        f = -1;

        UUReleaseMutex(&mAdminSsl);

        ctx = UUSslLoadKey(serial, 1, NULL, 0, &slke);
        if (ctx) {
            f = UUopenCreateFD(fnCnf, O_APPEND | O_CREAT | O_WRONLY, defaultFileMode);
            sprintf(created, "#load succeeded\n");
            write(f, created, strlen(created));
            close(f);
            f = -1;
            SSL_CTX_free(ctx);
            ctx = NULL;
            sprintf(newUrl, "/ssl?entry=%d", serial);
            MyNameRedirect(t, newUrl, 303);
            return;
        }

        switch (slke) {
        case SLKENONE:
            sprintf(created, "SLKENONE\n");
            break;
        case SLKECRTKEYMISMATCH:
            keyCrtKnownMismatch = TRUE;
            sprintf(created, "SLKECRTKEYMISMATCH\n");
            break;
        case SLKEOTHER:
            sprintf(created, "SLKEOTHER\n");
            break;
        case SLKEBADCRT:
            crtKnownInvalid = TRUE;
            sprintf(created, "SLKEBADCRT\n");
            break;
        case SLKEBADKEY:
            keyKnownInvalid = TRUE;
            sprintf(created, "SLKEBADKEY\n");
            break;
        }

        if (debugLevel < 80) {
            unlink(fnKey);
            unlink(fnCrt);
            unlink(fnCnf);
        } else {
            f = UUopenCreateFD(fnCnf, O_APPEND | O_CREAT | O_WRONLY, defaultFileMode);
            UUfputsFD("#load failed: ", f);
            UUfputsFD(created, f);
            UUfputsFD("#files kept for debugging\n", f);
            close(f);
            f = -1;
        }
    }

    AdminSslHeader(t, 0, "Import Existing SSL Certificate");

    UUSendZ(s,
            "<p>This page is only intended for use by advanced users who have created certificates "
            "outside of " EZPROXYNAMEPROPER " that need to be imported into " EZPROXYNAMEPROPER
            ".</p>\n");
    UUSendZ(s, "<p>If you are trying to create a new certificate for use with " EZPROXYNAMEPROPER
               ", use <a href='/ssl-new'>Create New SSL Certificate</a> instead.</p>\n");

    UUSendZ(s, "<form action='/ssl-import' method='post'>\n");

    UUSendZ(s, "<h2>Certificate</h2>\n");

    if ((provided > 0) && (provided < 2)) {
        UUSendZ(s,
                "<p class='failure'>This input is invalid because both a certificate and its "
                "unencrypted private key are required.</p>\n");
    }
    if (keyCrtKnownMismatch) {
        UUSendZ(s, "<p class='failure'>This certificate and key do not match each other.</p>\n");
    }

    UUSendZ(s,
            "<p>Insert the text of your certificate and its private key below, each in their "
            "designated box.</p>\n");

    UUSendZ(s,
            "<p>Insert the text of your certificate below, including the BEGIN CERTIFICATE and END "
            "CERTIFICATE lines.</p>\n");

    UUSendZ(s,
            "<p>If you have a certificate authority chain (intermediate certificates), do not "
            "include it here. You will have the opportunity to provides this information on a "
            "separate page once you import your certificate.</p>\n");

    if ((provided > 0) && (!crtProvided)) {
        UUSendZ(s, "<p class='failure'>A certificate is required.</p>\n");
    }

    if (crtProvided && crtKnownInvalid) {
        UUSendZ(s, "<p class='failure'>This certificate is invalid.</p>\n");
    }

    UUSendZ(s, "<textarea name='crt' rows='16' cols='85' style='font-family:courier' >");
    SendQuotedURL(t, crt);
    WriteLine(&t->ssocket, "&#%d;",
              (unsigned char)'\n'); /* TRIM_PEM_CLEANUP removed the final newline. */
    UUSendZ(s, "</textarea>\n");

    UUSendZ(s, "<h2>Key</h2>\n");

    UUSendZ(s,
            "<p>Insert the text of your unencrypted private key below, including the BEGIN RSA "
            "PRIVATE KEY and END RSA PRIVATE KEY lines.</p>\n");

    if ((provided > 0) && (!keyProvided)) {
        UUSendZ(s, "<p class='failure'>An unencrypted private key is required.</p>\n");
    }

    if (keyProvided && keyKnownInvalid) {
        UUSendZ(s, "<p class='failure'>This private key is invalid.</p>\n");
    }

    UUSendZ(s, "<textarea name='key' rows='16' cols='85' style='font-family:courier'>");
    SendQuotedURL(t, key);
    WriteLine(&t->ssocket, "&#%d;",
              (unsigned char)'\n'); /* TRIM_PEM_CLEANUP removed the final newline. */
    UUSendZ(s, "</textarea>\n");

    UUSendZ(s, "<p><input type='submit' value='Import Certificate'>\n");

    AdminFooter(t, 0);
}

void AdminSslNew(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    char fnCnf[64], fnKey[64], fnCsr[64], fnCrt[64], fnErr[64], fnCa[64];
    char *argv[30];
    char **p;
    int req_main(int argc, char **argv);
    int status;
    int serial;
    char algorithmParam[10];
    char keysizeParam[32];
    char newUrl[32];
    BOOL override;
    BOOL anyError = 0;

    const int SNCOUNTRY = 0;
    const int SNSTATE = 1;
    const int SNLOCALITY = 2;
    const int SNORG = 3;
    const int SNUNIT = 4;
    const int SNEMAIL = 5;
    const int SNACTION = 6;
    const int SNKEYSIZE = 7;
    const int SNCERTNAME = 8;
    const int SNYEARS = 9;
    const int SNOVERRIDE = 10;
    const int SNSHIBBOLETH = 11;
    const int SNALGORITHM = 12;
    const int SNALTBASE = 13;
    const int SNALTWILD = 14;

    struct FORMFIELD ffs[] = {
        {"country",    NULL, 0, 0, 3 },
        {"state",      NULL, 0, 0, 64},
        {"locality",   NULL, 0, 0, 64},
        {"org",        NULL, 0, 0, 64},
        {"unit",       NULL, 0, 0, 64},
        {"email",      NULL, 0, 0, 64},
        {"action",     NULL, 0, 0, 10},
        {"keysize",    NULL, 0, 0, 4 },
        {"certname",   NULL, 0, 0, 5 },
        {"years",      NULL, 0, 0, 2 },
        {"override",   NULL, 0, 0, 3 },
        {"shibboleth", NULL, 0, 0, 2 },
        {"algorithm",  NULL, 0, 0, 7 },
        {"altbase",    NULL, 0, 0, 1 },
        {"altwild",    NULL, 0, 0, 1 },
        {NULL,         NULL, 0, 0, 0 }
    };

    char *country, *state, *locality, *org, *unit, *email, *action, *algorithm;
    char *keysize;
    char *countryError, *stateError, *localityError, *orgError, *unitError, *emailError,
        *subjectAltNameError;
    char *certName = NULL;
    char *prefix;
    int years;
    char days[9];
    int i;
    BOOL shibCert;
    BOOL altBase;
    BOOL altWild;

    if (AdminSslDisabled(t))
        return;

    FindFormFields(query, post, ffs);
    override = ffs[SNOVERRIDE].value != NULL;
    shibCert = optionProxyType == PTHOST && optionWildcardHTTPS && ffs[SNSHIBBOLETH].value != NULL;

    prefix = "";
    if (ffs[SNCERTNAME].value) {
        if (strcmp(ffs[SNCERTNAME].value, "wild") == 0)
            prefix = "*.";
    }

    if (shibCert)
        prefix = "login.";

    if (ffs[SNYEARS].value == NULL)
        years = 1;
    else {
        years = atoi(ffs[SNYEARS].value);
        if (years < 1 || years > 10)
            years = 1;
    }

    if (!(keysize = ffs[SNKEYSIZE].value))
        keysize = "2048";

    if (!(algorithm = ffs[SNALGORITHM].value))
        algorithm = "sha256";

    if (!(country = ffs[SNCOUNTRY].value))
        country = "";
    else
        AllUpper(country);

    countryError = strlen(country) != 2 ? "Country code must be exactly two characters long" : "";

    if (!(state = ffs[SNSTATE].value))
        state = "";

    if (stricmp(country, "US") == 0 && strlen(state) == 2)
        stateError = "State abbreviations are not allowed";
    else
        stateError = "";

    if (!(locality = ffs[SNLOCALITY].value))
        locality = "";

    localityError = "";

    if (!(org = ffs[SNORG].value))
        org = "";

    orgError = *org == 0 ? "Organization must be specified" : "";

    if (!(unit = ffs[SNUNIT].value))
        unit = "";

    unitError = "";

    if (!(email = ffs[SNEMAIL].value))
        email = "";

    emailError = *email == 0 ? "Email must be specified" : "";

    altBase = ffs[SNALTBASE].value && *(ffs[SNALTBASE].value) == '1';
    altWild = ffs[SNALTWILD].value && *(ffs[SNALTWILD].value) == '1';

    subjectAltNameError =
        "When using Subject Alternate Name, recommended practice is to also check the name that "
        "matches your certificate name.";
    if (!(altBase || altWild)) {
        subjectAltNameError = "";
    } else {
        if (*prefix == '*') {
            if (altWild) {
                subjectAltNameError = "";
            }
        } else {
            if (altBase) {
                subjectAltNameError = "";
            }
        }
    }

    if (override) {
        countryError = stateError = localityError = orgError = unitError = emailError =
            subjectAltNameError = "";
    }

    if (!(action = ffs[SNACTION].value)) {
        action = "";
    }

    if (action[0] == 0) {
        countryError = stateError = localityError = orgError = unitError = emailError =
            subjectAltNameError = "";
    } else {
        if (*countryError == 0 && *stateError == 0 && *localityError == 0 && *orgError == 0 &&
            *unitError == 0 && *emailError == 0 && *subjectAltNameError == 0)
            goto Action;
        anyError = 1;
    }

    AdminSslHeader(t, 0,
                   shibCert ? "Create New SSL Certificate for Shibboleth Communication"
                            : "Create New SSL Certificate");

    UUSendZ(s, "<form action='/ssl-new' method='post'>\n");

    UUSendZ(s, "<table>\n");
    UUSendZ(s, "<tr><th scope='row'>Server name: </th><td>");
    SendQuotedURL(t, myName);
    UUSendZ(s, " (can only be changed in " EZPROXYCFG ")</td></tr>\n");

    WriteLine(s, "<tr><th scope='row'>Digest: </th><td><select name='%s'>", ffs[SNALGORITHM].name);
    printSelectBoxOptions(s, algorithm, 4, "sha256", "SHA-256", "sha1", "SHA-1");
    WriteLine(s, "</select></td></tr>");

    WriteLine(s, "<tr><th scope='row'>Key size: </th><td><select name='keysize'>");
    printSelectBoxOptions(s, keysize, 6, "1024", "1024", "2048", "2048", "4096", "4096");
    WriteLine(s, "</select></td></tr>\n");

    WriteField(t, "Country", "country", country, countryError, 2, 2, NULL);
    WriteField(t, "State or Province (optional)", "state", state, stateError, 40, 64, NULL);
    WriteField(t, "City or Locality (optional)", "locality", locality, localityError, 40, 64, NULL);
    WriteField(t, "Organization", "org", org, orgError, 40, 64, NULL);
    WriteField(t, "Organization Unit (optional)", "unit", unit, unitError, 40, 64, NULL);
    WriteField(t, "Administrator email", "email", email, emailError, 40, 64, NULL);
    if (optionProxyType == PTHOST) {
        if (!shibCert) {
            UUSendZ(s, "<tr><td colspan='2'>&nbsp;</td></tr>\n");
        }

        UUSendZ(s, "<tr><td>Certificate name:</td><td>");
        if (shibCert) {
            UUSendZ(s, "<input type='hidden' name='shibboleth' value='on'>\nlogin.");
            SendHTMLEncoded(s, myName);
        } else {
            WriteLine(s, "<input name='certname' type='radio' value='base'%s> ",
                      (*prefix == 0 ? " checked='checked'" : ""));
            SendHTMLEncoded(s, myName);
            UUSendZ(s, " (browser warnings proxying https web sites; less expensive)<br />\n");

            WriteLine(s, "<input name='certname' type='radio' value='wild'%s> *.",
                      (*prefix == '*' ? " checked='checked'" : ""));
            SendHTMLEncoded(s, myName);
            UUSendZ(s,
                    " (fewest to no browser warnings proxying https web sites; more expensive)<br "
                    "/>\n");
        }

        UUSendZ(s, "</td></tr>\n");

        if (!shibCert) {
            UUSendZ(s, "<tr><td colspan='2'>&nbsp;</td></tr>\n");

            UUSendZ(s, "<tr><th scope='row'>Subject Alternate Name:</th><td>");
            if (*subjectAltNameError) {
                WriteLine(s, "<div class='failure'>%s</div>", subjectAltNameError);
            }

            WriteLine(s, "<input name='altbase' type='checkbox' value='1'%s> ",
                      (altBase ? " checked='checked'" : ""));
            SendHTMLEncoded(s, myName);
            UUSendZ(s, "<br />\n");

            WriteLine(s, "<input name='altwild' type='checkbox' value='1'%s> *.",
                      (altWild ? " checked='checked'" : ""));
            SendHTMLEncoded(s, myName);
            UUSendZ(s, "</td></tr>\n");

            UUSendZ(s, "<tr><td colspan='2'>&nbsp;</td></tr>\n");
        }
    }

    UUSendZ(
        s, "<tr><th scope='row'>Expiration (for self-signed only)</th><td><select name='years'>\n");
    for (i = 1; i <= 10; i++)
        WriteLine(s, "<option value='%d'%s>%d year%s</option>\n", i,
                  (i == years ? " selected='selected'" : ""), i, SIfNotOne(i));
    UUSendZ(s, "</select></td></tr>\n");

    if (anyError)
        UUSendZ(s,
                "<tr><th scope='row'>Override:</th><td><input type='checkbox' name='override'> "
                "check here to ignore the identified errors</td></tr>\n");

    UUSendZ(
        s,
        "<tr><th scope='row'>Create: </th><td><input type='submit' name='action' value='Self-Signed Certificate'> or \
<input type='submit' name='action' value='Certificate Signing Request'></td></tr>\n\
</table>\
<p>You can generate self-signed certificates for no additional cost.  These certificates will generate warnings\n\
in remote web browsers when used, but they are an excellent choice when you are first testing SSL features.\n\
If you find the browser warnings acceptable, you can use them for production use.</p>\n\
<p>Certificate Signing Requests are used when you purchase a certificate from a certificate authority.\n\
By purchasing a certificate, you avoid warnings in remote web browsers.  If you are using or plan to use\n\
proxy by hostname, you should consider purchasing a wildcard certificate.  These certificates are more\n\
expensive to purchase, but suppress all warnings in remote web browsers.  If you use a regular certificate\n\
with proxy by hostname, users will receive a browser warning whenever they access a proxied https web site.</p>\n\
<p>If you are unclear on the advantages and disadvantages of any of these certificate options, contact\n\
<a href='mailto:" EZPROXYHELPEMAIL "'>" EZPROXYHELPEMAIL "</a> for more information.</p>\n");

    AdminFooter(t, 0);

    return;

Action:

    p = argv;

    *p++ = "req"; /* arg 0 is always the name of the program, but we're just faking it. */
    sprintf(algorithmParam, "-%s", algorithm);
    *p++ = algorithmParam;
    *p++ = "-new";
    *p++ = "-config";
    *p++ = fnCnf;
    *p++ = "-newkey";
    sprintf(keysizeParam, "rsa:%s", keysize);
    *p++ = keysizeParam;
    *p++ = "-nodes";
    *p++ = "-keyout";
    *p++ = fnKey;
    *p++ = "-out";
    if (*action == 'S') {
        *p++ = fnCrt;
        *p++ = "-x509";
        *p++ = "-verify";
        *p++ = "-days";
        sprintf(days, "%d", (int)((float)years * 365.25));
        *p++ = days;
    } else {
        *p++ = fnCsr;
    }

    UUAcquireMutex(&mAdminSsl);
    if (!(certName = malloc(strlen(myName) + strlen(prefix) + 1)))
        PANIC;
    sprintf(certName, "%s%s", prefix, myName);
    char *alt1 = NULL;
    char *alt2 = NULL;
    if (altBase) {
        alt1 = myName;
    }
    if (altWild) {
        if (!(alt2 = malloc(strlen(myName) + 3)))
            PANIC;
        sprintf(alt2, "*.%s", myName);
    }
    serial = UUSslCnf(atoi(keysize), algorithm, country, state, locality, org, unit, certName,
                      email, alt1, alt2);
    FreeThenNull(certName);
    // alt1 points straight at myName, do not attempt to free
    FreeThenNull(alt2);

    if (serial == -1)
        goto Failed;

    sprintf(fnCnf, "%s%08d.cnf", SSLDIR, serial);
    sprintf(fnCsr, "%s%08d.csr", SSLDIR, serial);
    sprintf(fnCrt, "%s%08d.crt", SSLDIR, serial);
    sprintf(fnErr, "%s%08d.err", SSLDIR, serial);
    sprintf(fnKey, "%s%08d.key", SSLDIR, serial);
    sprintf(fnCa, "%s%08d.ca", SSLDIR, serial);

    bio_err = BIO_new_file(fnErr, "w");

    if (bio_err == NULL) {
        Log("AdminSSL unable to create %s", fnErr);
        goto Failed;
    }

    status = req_main(p - argv, argv);

    if (status) {
        Log("Certificate creation failed, see %s for details", fnErr);
        goto Failed;
    }

    BIO_free(bio_err);
    bio_err = NULL;

    UUReleaseMutex(&mAdminSsl);

    sprintf(newUrl, "/ssl?entry=%d", serial);
    MyNameRedirect(t, newUrl, 303);

    return;

Failed:
    if (bio_err) {
        BIO_free(bio_err);
        bio_err = NULL;
    }
    UUReleaseMutex(&mAdminSsl);
    HTMLHeader(t, htmlHeaderNoCache);
    WriteLine(s, "Operation failed, see %s for more information", EZPROXYMSG);
}

void AdminSslViewCrt(struct TRANSLATE *t, X509 *x509) {
    struct UUSOCKET *s = &t->ssocket;
    BIO *mem = NULL;
    char line[256];

    if (!(mem = BIO_new(BIO_s_mem())))
        PANIC;

    PEM_write_bio_X509(mem, x509);
    UUSendZ(s, "<p><pre>\n");
    while (BIO_gets(mem, line, sizeof(line)) > 0) {
        Trim(line, TRIM_TRAIL);
        SendHTMLEncoded(s, line);
        UUSendZ(s, "\n");
    }
    UUSendZ(s, "</pre></p>\n");
    BIO_free(mem);
}

void AdminSsl(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    int i;
    X509 *x509 = NULL;
    X509_REQ *x509_req = NULL;
    char filename[MAX_PATH], filename2[MAX_PATH];
    char line[256];
    BIO *in = NULL, *mem = NULL;
    SSL_CTX *ctx;
    BOOL copyError;
    char *exts[] = {".cnf", ".err", ".key", ".csr", ".crt", ".ca", NULL};
    char **ep;
    int f;
    int result;
    char *entityType = "";
    char saveError = 0;
    BOOL caChanged;
    BOOL caEdit;
    int serial;
    char *subjectAltNames = NULL;
    char action[16], confirm[10], *crt, *ca;

    const int ASENTRY = 0;
    const int ASACTION = 1;
    const int ASCONFIRM = 2;
    const int ASCRT = 3;
    const int ASCA = 4;
    const int ASFULL = 5;
    const int ASVIEWCRT = 6;

    struct FORMFIELD ffs[] = {
        {"entry", NULL, 0, 0, 10},
        {"action", NULL, 0, 0, sizeof(action) - 1},
        {"confirm", NULL, 0, 0, sizeof(confirm) - 1},
        {"crt", NULL, 0, 0, 0, FORMFIELDNOTRIMCTRL},
        {"ca", NULL, 0, 0, 0, FORMFIELDNOTRIMCTRL},
        {"full", NULL, 0, 0, 0},
        {"viewcrt", NULL, 0, 0, 0},
        {NULL, NULL, 0, 0, 0}
    };

    BOOL update = t->session->priv || GroupMaskOverlap(gmAdminSSLUpdate, t->session->gm);

    if (AdminSslDisabled(t))
        return;

    FindFormFields(query, post, ffs);

    i = ffs[ASENTRY].value ? atoi(ffs[ASENTRY].value) : 0;
    StrCpy3(action, ffs[ASACTION].value, sizeof(action));
    StrCpy3(confirm, ffs[ASCONFIRM].value, sizeof(confirm));

    if (!(crt = ffs[ASCRT].value))
        crt = "";
    if (!(ca = ffs[ASCA].value))
        ca = "";

    if (i <= 0) {
        AdminSslList(t, update);
        return;
    }

    if (stricmp(action, "metadata") == 0) {
        SAMLSendShibbolethMetadata(t, i, NULL, 0);
        return;
    }

    if (stricmp(action, "metadata-slo") == 0) {
        SAMLSendShibbolethMetadata(t, i, NULL, 1);
        return;
    }

    if (!update) {
        strcpy(action, "");
    }

    char title[256];
    sprintf(title, "Manage SSL Certificate %d", i);
    AdminSslHeader(t, 0, title);

    if (stricmp(action, "copy") == 0) {
        sprintf(filename, "%s%08d.key", SSLDIR, i);
        if (!FileExists(filename)) {
            goto Bad;
        }

        UUSslFiles(&serial, 0);
        serial++;

        copyError = 0;
        for (ep = exts; *ep; ep++) {
            if (stricmp(*ep, ".crt") == 0)
                continue;
            sprintf(filename, "%s%08d%s", SSLDIR, i, *ep);
            if (!FileExists(filename))
                continue;
            sprintf(filename2, "%s%08d%s", SSLDIR, serial, *ep);
            result = UUCopyFile(filename2, filename);
            if (result != 0) {
                copyError = 1;
                break;
            }
        }

        if (copyError) {
            for (ep = exts; *ep; ep++) {
                sprintf(filename2, "%s%08d%s", SSLDIR, serial, *ep);
                unlink(filename2);
            }
            UUSendZ(s, "Fatal error while trying to duplicate certificate");
            AdminFooter(t, 0);
            return;
        }

        i = serial;
    }

    if (strcmp(action, "crt") == 0 && *crt != 0) {
        Trim(crt, TRIM_PEM_CLEANUP);
        if (!AdminLooksLikeACertificate(crt))
            saveError = 1;
        else {
            sprintf(filename, "%s%08d.crt", SSLDIR, i);
            if (FileExists(filename)) {
                UUSendZ(s, "Tried to overwrite existing key\n");
                AdminFooter(t, 0);
                return;
            }
            f = UUopenCreateFD(filename, O_CREAT | O_WRONLY | O_TRUNC, defaultFileMode);
            if (f < 0) {
                Log("Unable to create %s: %d", filename, errno);
                unlink(filename); /* in case file actually got created */
                UUSendZ(s, "Unable to create certificate file");
                AdminFooter(t, 0);
                return;
            }

            if (write(f, crt, strlen(crt)) != (int)strlen(crt)) {
                Log("Failed writing to %s: %d", filename, errno);
                close(f);
                unlink(filename);
                AdminFooter(t, 0);
                return;
            } else {
                write(f, "\n", 1);
                close(f);
                ctx = UUSslLoadKey(i, 1, NULL, 0, NULL);
                if (ctx == NULL) {
                    saveError = 1;
                    unlink(filename);
                } else
                    SSL_CTX_free(ctx);
            }
        }
    }

    caChanged = caEdit = 0;

    if (strcmp(action, "ca") == 0) {
        Trim(ca, TRIM_PEM_CLEANUP);
        sprintf(filename, "%s%08d.ca", SSLDIR, i);
        if (*ca == 0) {
            if (FileExists(filename)) {
                caChanged = 1;
                if (unlink(filename) != 0 && errno != ENOENT) {
                    Log("Unable to delete %s: %d", filename, errno);
                    UUSendZ(s, "Unable to remove certificate chain authority file");
                    AdminFooter(t, 0);
                    return;
                }
                UUSendZ(s, "<strong>Certificate authority chain deleted.</strong><p>");
            }
        } else {
            if (!AdminLooksLikeACertificate(ca)) {
                caEdit = 1;
            } else {
                caChanged = 1;
                f = UUopenCreateFD(filename, O_CREAT | O_WRONLY | O_TRUNC, defaultFileMode);
                if (f < 0) {
                    Log("Unable to create %s: %d", filename, errno);
                    unlink(filename); /* in case file actually got created */
                    UUSendZ(s, "Unable to create certificate chain authority file");
                    AdminFooter(t, 0);
                    return;
                }

                if (write(f, ca, strlen(ca)) != (int)strlen(ca)) {
                    Log("Failed writing to %s: %d", filename, errno);
                    close(f);
                    unlink(filename);
                    AdminFooter(t, 0);
                    return;
                } else {
                    close(f);
                    ctx = UUSslLoadKey(i, 0, NULL, 0, NULL);
                    if (ctx) {
                        // Do not free othercerts!
                        STACK_OF(X509) * othercerts;
                        if (SSL_CTX_get_extra_chain_certs_only(ctx, &othercerts) == 0 ||
                            othercerts == NULL) {
                            SSL_CTX_free(ctx);
                            ctx = NULL;
                            UUSendZ(s,
                                    "<strong>The certificate authority chain you entered does not "
                                    "contain any valid certificates.</strong><p>\n");
                            saveError = 1;
                            unlink(filename);
                            caEdit = 1;
                        } else {
                            SSL_CTX_free(ctx);
                            ctx = NULL;
                            UUSendZ(s, "<strong>Certificate authority chain saved.</strong><p>");
                        }
                    }
                }
            }
        }
    }

    if (caChanged && i == sslActiveIndex)
        UUInitSslKey(i, 1);

    if (strcmp(action, "active") == 0) {
        if (stricmp(confirm, "ACTIVE") == 0) {
            UUInitSslKey(i, 0);
            if (i != sslActiveIndex) {
                UUSendZ(s, "Error changing active certificate.\n");
                AdminFooter(t, 0);
                return;
            }

            f = UUopenCreateFD(SSLDIR "active", O_CREAT | O_WRONLY | O_TRUNC, defaultFileMode);
            if (f < 0) {
                Log("Unable to create %s", SSLDIR "active");
            } else {
                sprintf(line, "%d\n", i);
                write(f, line, strlen(line));
                close(f);
            }
            if (AdminSslCheckWildcardsHttps(1)) {
                UUSendZ(s,
                        "<p>Some databases may not work properly until you <a "
                        "href='/restart'>restart</a> EZproxy.</p>\n");
            }
        }
    }

    if (strcmp(action, "delete") == 0) {
        if (stricmp(confirm, "DELETE") == 0) {
            if (i == sslActiveIndex) {
                UUSendZ(s, "You cannot delete the active certificate.\n");
                AdminFooter(t, 0);
                return;
            }
            for (ep = exts; *ep; ep++) {
                sprintf(filename, "%s%08d%s", SSLDIR, i, *ep);
                unlink(filename);
            }
            UUSendZ(s, "Deletion complete\n");
            AdminFooter(t, 0);
            return;
        }
    }

    in = mem = NULL;
    x509 = NULL;
    sprintf(filename, "%s%08d.crt", SSLDIR, i);
    in = BIO_new(BIO_s_file());
    if (in && BIO_read_filename(in, filename) > 0 &&
        (x509 = PEM_read_bio_X509(in, NULL, NULL, NULL))) {
        if (mem = BIO_new(BIO_s_mem())) {
            X509_print(mem, x509);
            subjectAltNames = UUSslSubjectAltNamesFromCRT(x509);
            if (AdminSslShowCert(t, "Certificate Details", mem, ffs[ASFULL].value != NULL,
                                 subjectAltNames)) {
                ctx = UUSslLoadKey(i, 0, NULL, 0, NULL);
                if (ctx) {
                    SSL_CTX_free(ctx);

                    if (ffs[ASVIEWCRT].value != NULL)
                        AdminSslViewCrt(t, x509);
                    else {
                        WriteLine(s,
                                  "<div class='prespace'><a href='/ssl?entry=%d&viewcrt=on'>View "
                                  "PEM version of this certificate</a></div>\n",
                                  i);
                        WriteLine(
                            s,
                            "<div><a href='/ssl?entry=%d&action=metadata'>View Shibboleth metadata "
                            "for this certificate without Single Logout enabled</a></div>\n",
                            i);
                        WriteLine(
                            s,
                            "<div><a href='/ssl?entry=%d&action=metadata-slo'>View Shibboleth "
                            "metadata for this certificate with Single Logout enabled</a></div>\n",
                            i);
                    }

                    entityType = "certificate";
                    if (i != sslActiveIndex) {
                        if (update) {
                            WriteLine(
                                s, "<form action='/ssl?entry=%d&action=active' method='post'>", i);
                            UUSendZ(s,
                                    "<p>To make this the active certificate for this server, type "
                                    "ACTIVE in this box <input type='text' size='7' maxlength='7' "
                                    "name='confirm'> then click <input type='submit' "
                                    "value='activate'>.</p></form>\n");
                        }
                    } else {
                        UUSendZ(s, "<p>This is the current SSL certificate for this server.</p>\n");
                        if (optionProxyType == PTHOST && optionIgnoreWildcardCertificate == 0 &&
                            optionForceWildcardCertificate == 0) {
                            BOOL validPBHWildcard;
                            BOOL anyWildcard;
                            BOOL domainWildcard;
                            char commonName[256];

                            commonName[0] = 0;
                            X509_NAME_get_text_by_NID(X509_get_subject_name(x509), NID_commonName,
                                                      commonName, sizeof(commonName) - 1);
                            char *subjectAltNames = UUSslSubjectAltNamesFromCRT(x509);
                            AdminSSLCheckWildcards(commonName, subjectAltNames, &validPBHWildcard,
                                                   &anyWildcard, &domainWildcard);
                            FreeThenNull(subjectAltNames);

                            if (anyWildcard && !validPBHWildcard) {
                                UUSendZ(s,
                                        "<h2>Wildcard Certificate Problem</h2>\n<p>In proxy by "
                                        "hostname, the preferred wildcard form of name or subject "
                                        "alternate name for this server to avoid browser warnings "
                                        "is *.");
                                SendHTMLEncoded(s, myName);
                                UUSendZ(s,
                                        ". This wildcard certificate does not match the preferred "
                                        "wildcard form which will lead to server name mismatch "
                                        "browser warnings when proxying https web sites.</p>\n");
                                if (domainWildcard) {
                                    UUSendZ(s,
                                            "<p>The wildcard certificate contains the name or "
                                            "subject alternate name *");
                                    UUSendZ(s, myDomain);
                                    UUSendZ(s,
                                            " which will allow EZproxy to use it for login "
                                            "purposes without generating server name mismatch "
                                            "browser warnings.</p>\n");
                                }
                            }
                        }
                    }

                    sprintf(filename, "%s%08d.csr", SSLDIR, i);
                    if (update && FileExists(filename)) {
                        WriteLine(s, "<p><form action='/ssl?entry=%d&action=copy' method='post'>",
                                  i);
                        UUSendZ(s,
                                "If your certificate authority has sent you an updated certificate "
                                "for this key, ");
                        UUSendZ(s,
                                "type COPY in this box <input type='text' size='5' maxlength='5' "
                                "name='confirm'> then click <input type='submit' value='copy'> ");
                        UUSendZ(s,
                                "to create a copy for use with the updated certificate.</form>\n");
                    }

                    UUSendZ(s,
                            "<h2>Certificate Authority Chain (Intermediate Certificates)</h2>\n");
                    sprintf(filename, "%s%08d.ca", SSLDIR, i);
                    if (update) {
                        UUSendZ(
                            s,
                            "<p>If your certificate requires a certificate authority chain, enter "
                            "it below including the BEGIN CERTIFICATE and END CERTIFICATE lines "
                            "then click Save Certificate Authority Chain.</p>\n");
                        UUSendZ(s,
                                "<p>If the chain contains more than one certificate, include each "
                                "certificate one after the other, each with its own BEGIN "
                                "CERTIFICATE and END CERTIFICATE lines.</p>\n");
                        WriteLine(s, "<form action='/ssl?entry=%d&action=ca' method='post'>", i);
                        if (caEdit)
                            UUSendZ(s,
                                    "<p class='failure'>The certificate authority chain specified "
                                    "is invalid.</p>\n");
                    }
                    WriteLine(s, "<p><textarea name='ca' rows='8' cols='72'%s>",
                              update ? "" : " readonly");

                    if (caEdit)
                        SendQuotedURL(t, ca);
                    else {
                        SendFile(t, filename, SFQUOTEDURL);
                    }

                    UUSendZ(s, "</textarea></p>\n");

                    if (update) {
                        UUSendZ(s,
                                "<p><input type='submit' value='Save Certificate Authority "
                                "Chain'></p>\n");
                        UUSendZ(s, "</form>\n");
                    }
                } else {
                    entityType = "damaged certificate";
                    UUSendZ(s, "<p>This certificate is damaged.</p>\n");
                }
            }
        }
    }
    if (x509)
        X509_free(x509);
    if (mem != NULL)
        BIO_free(mem);
    if (in != NULL)
        BIO_free(in);

    if (*entityType == 0) {
        in = mem = NULL;
        x509_req = NULL;
        sprintf(filename, "%s%08d.csr", SSLDIR, i);
        in = BIO_new(BIO_s_file());
        if (in && BIO_read_filename(in, filename) > 0 &&
            (x509_req = PEM_read_bio_X509_REQ(in, NULL, NULL, NULL))) {
            if ((mem = BIO_new(BIO_s_mem()))) {
                X509_REQ_print(mem, x509_req);
                subjectAltNames = UUSslSubjectAltNamesFromCSR(x509_req);
                if (AdminSslShowCert(t, "Certificate Signing Request (CSR) Details", mem,
                                     ffs[ASFULL].value != NULL, subjectAltNames)) {
                    UUSendZ(s,
                            "<p>To obtain a certificate, copy the lines below, including the BEGIN "
                            "CERTIFICATE REQUEST and END CERTIFICATE REQUEST lines,\n");
                    UUSendZ(s, "then submit them to your certificate authority.</p><p><pre>\n");
                    SendFile(t, filename, SFREPORTIFMISSING | SFQUOTEDURL);
                    UUSendZ(s, "</pre></p>\n");

                    UUSendZ(s,
                            "<p>If your certificate authority asks what type of web server you are "
                            "using, specify Apache or Other.</p>\n");

                    if (update) {
                        UUSendHR(s);

                        UUSendZ(
                            s,
                            "<p>When you receive the certificate from your certificate authority, "
                            "copy all of it including the BEGIN CERTIFICATE and END CERTIFICATE "
                            "lines into the box below then click Save Certificate.</p>\n");
                        UUSendZ(s,
                                "<p>If you receive a certificate authority chain (intermediate "
                                "certificates), do not include it here. You will have the "
                                "opportunity to provides this information on a separate page once "
                                "you install your certificate.</p>\n");

                        WriteLine(s, "<form action='/ssl?entry=%d&action=crt' method='post'>\n", i);

                        if (saveError)
                            UUSendZ(s,
                                    "<strong>The certificate specified is either invalid or not "
                                    "valid for this Certificate Signing Request.</strong><p>\n");

                        UUSendZ(s, "<textarea name='crt' rows='8' cols='72'>");
                        SendQuotedURL(t, crt);
                        UUSendZ(s, "</textarea>\n");
                        if (update) {
                            UUSendZ(s, "<p><input type='submit' value='Save Certificate'></p>\n");
                        }
                        UUSendZ(s, "</form>\n");
                    }
                    entityType = "certificate signing request";
                }
            }
        }
        if (x509_req)
            X509_REQ_free(x509_req);
        if (mem != NULL)
            BIO_free(mem);
        if (in != NULL)
            BIO_free(in);
    }

Bad:
    if (*entityType == 0) {
        UUSendZ(s,
                "<p>This is either damaged residue of a failed certificate creation, or the file "
                "permissions are invalid on files in the SSL subdirectory.\n");
        entityType = "entry";
    }

    if (update && i != sslActiveIndex) {
        WriteLine(s, "<p><form action='/ssl?entry=%d&action=delete' method='post'>\n", i);
        WriteLine(s,
                  "To delete this %s, type DELETE in this box <input type='text' name='confirm' "
                  "size='7' maxlength='7'> then click <input type='submit' value='delete'>\n",
                  entityType);
        UUSendZ(s, "</form>\n");
    }

    FreeThenNull(subjectAltNames);

    AdminFooter(t, 0);
}
