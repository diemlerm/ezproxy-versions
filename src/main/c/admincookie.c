#define __UUFILE__ "admincookie.c"

#include "common.h"
#include "uustring.h"

#include "admincookie.h"

void AdminCookie(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s;

    s = &t->ssocket;

    HTMLHeader(t, htmlHeaderOmitCache);
    SendFile(t, COOKIEHTM, SFREPORTIFMISSING);
}
