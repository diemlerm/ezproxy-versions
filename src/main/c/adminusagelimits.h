#ifndef __ADMINUSAGELIMITS_H__
#define __ADMINUSAGELIMITS_H__

#include "common.h"

void AdminUsageLimits(struct TRANSLATE *t, char *query, char *post);
void AdminUsageLimitExceeded(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINUSAGELIMITS_H__
