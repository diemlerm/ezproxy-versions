/**
 * External user authentication
 *
 * This module implements external user authentication.  This method connects
 * to a specified URL and looks for a specified string in the response to
 * indicate a user is authenticated.  Refer to the documentation page
 * for additional details.
 *
 * This authentication method DOES NOT SUPPORT common conditions and actions.
 */

#define __UUFILE__ "usrexternal.c"

#include "common.h"
#include "uustring.h"

#include "usr.h"
#include "usrexternal.h"

/* Returns 0 if valid, 1 if not, 3 if unable to connect to host */
enum FINDUSERRESULT FindUserExternal(struct TRANSLATE *t,
                                     struct USERINFO *uip,
                                     char *externalUrl,
                                     char *post,
                                     char *valid,
                                     struct sockaddr_storage *pInterface) {
    struct UUSOCKET rr, *r = &rr;
    enum FINDUSERRESULT result = resultRefused;
    char query[4096];
    char postFields[1024];
    char *postFieldsEnd = postFields + sizeof(postFields) - 1;
    char *slash;
    char *colon;
    char useSsl;
    char *p;
    BOOL skipping = 0;
    BOOL inHeader;
    BOOL checkHeader, checkBody;

    UUInitSocket(r, INVALID_SOCKET);

    if (externalUrl == NULL || uip->user == NULL || uip->pass == NULL || strlen(uip->user) > 64 ||
        strlen(uip->pass) > 64)
        goto Finished;

    if (p = StartsIWith(valid, "header:")) {
        valid = p;
        checkHeader = 1;
        checkBody = 0;
    } else if (p = StartsIWith(valid, "body:")) {
        valid = p;
        checkHeader = 0;
        checkBody = 1;
    } else {
        checkHeader = checkBody = 1;
    }

    externalUrl = SkipHTTPslashesAt(externalUrl, &useSsl);

    if (useSsl && !UUSslEnabled()) {
        static char report = 1;
        if (report) {
            report = 0;
            UsrLog(uip, "Due to export restriction, ::External=https requires an " EZPROXYNAMEPROPER
                        " license.");
        }
        goto Finished;
    }

    ParseHost(externalUrl, &colon, &slash, TRUE);

    if (slash)
        *slash = 0;

    if (colon)
        *colon = 0;

    if (UUConnectWithSource2(r, externalUrl, (PORT)(colon ? atoi(colon + 1) : (useSsl ? 443 : 80)),
                             "External", pInterface, useSsl))
        goto Finished;

    if (colon)
        *colon = ':';

    if (slash)
        *slash = '/';

    if (post && *post) {
        strcpy(query, "POST ");
        EncodeFieldsToBuffer(postFields, postFieldsEnd, post, t, uip);
    } else {
        strcpy(query, "GET ");
        postFields[0] = 0;
    }

    strcpy(query, post ? "POST " : "GET ");
    if (post == NULL && (slash == NULL || strchr(slash, '^') == NULL)) {
        sprintf(strchr(query, 0), "%s0=", (slash ? slash : "/"));
        AddEncodedField(query, uip->user, postFieldsEnd);
        strcat(query, "&2=");
        AddEncodedField(query, uip->pass, postFieldsEnd);
    } else {
        EncodeFieldsToBuffer(strchr(query, 0), postFieldsEnd, (slash ? slash : "/"), t, uip);
    }

    strcat(query, " HTTP/1.0\r\n");
    UUSendZ(r, query);

    if (post)
        UUSendZ(r, "Content-type: application/x-www-form-urlencoded\r\n");

    UUSendZ(r, "User-Agent: Mozilla/4.0 (compatible; EZproxy)\r\n");

    if (slash)
        *slash = 0;
    WriteLine(r, "Host: %s\r\n", externalUrl);

    if (post)
        WriteLine(r, "Content-length: %d\r\n", strlen(postFields));

    UUSendZ(r, "Connection: close\r\n");
    UUSendZ(r, "Cache-Control: no-cache\r\n");

    UUSendCRLF(r);

    if (post) {
        UUFlush(r);
        UUSendZ(r, postFields);
    }

    result = 1;
    inHeader = 1;
    while (ReadLine(r, query, sizeof(query))) {
        Trim(query, TRIM_LEAD | TRIM_TRAIL);
        if (uip->debug)
            UsrLog(uip, "::external received text: %s", query);

        if (inHeader) {
            if (query[0] == 0) {
                inHeader = 0;
            }
        }

        if (strncmp(query, "ezproxy_eof", 11) == 0) {
            break;
        }

        if (skipping)
            continue;

        if ((inHeader && checkHeader) || ((!inHeader) && checkBody)) {
            if (valid && *valid) {
                if (StrIStr(query, valid) || WildCompare(query, valid))
                    result = resultValid;
            } else {
                if (strstr(query, "webchkpass") != NULL || StrIStr(query, "+VALID") != NULL) {
                    if (uip->debug)
                        UsrLog(uip, "::external received valid indicator");
                    result = resultValid;
                }
            }
        }

        if (p = StartsWith(query, "ezproxy_group=")) {
            if (uip->debug) {
                UsrLog(uip, "::external received group: %s", p);
            }
            uip->gm = MakeGroupMask(uip->gm, p, 0, '=');
        }

        if ((p = StartsWith(query, "ezproxy_deny")) && (*p == 0 || *p == '=')) {
            /*
            char denyFile[MAXMENU];

            strcpy(denyFile, DENYHTM);
            if (*p) {
                p++;
                if (ValidMenu(p))
                    sprintf(denyFile, "%s%s", DOCSDIR, p);
            }
            HTMLHeader(t, htmlHeaderOmitCache);
            SendEditedFile(t, uip, denyFile, 1, uip->url, 1, NULL);
            */
            if (*p)
                p++;
            UsrDeny(t, uip, NULL, p);
            result = resultNotified;
            skipping = 1;
            break;
        }

        if (p = StartsWith(query, "ezproxy_menu=")) {
            if (ValidMenu(p)) {
                if (uip->debug)
                    UsrLog(uip, "::external received menu: %s", p);
                sprintf(uip->menu, "%s%s", DOCSDIR, p);
            } else
                UsrLog(uip, "::external references invalid menu %s", p);
        }
    }
    UUStopSocket(r, 0);

Finished:
    return result;
}
