#define __UUFILE__ "sql.c"

#include "common.h"
#include "sql.h"

void SQLPanic(const char *file, int line, int sqlErrno, const char *errMsg) {
    char buffer[256];

    while (panicing) {
        exit(1);
    };
    panicing = TRUE;

    if (errMsg == NULL) {
        errMsg = sqlite3_errstr(sqlErrno);
        if (errMsg == NULL) {
            errMsg = "";
        }
    }

    safe_portable_snprintf(buffer, sizeof(buffer), "SQL PANIC %s:%d error %d %s\n", file, line,
                           sqlErrno, errMsg);
    WriteTrace(1, 0, NULL, buffer);
    fflush(stdout);

    exit(1);
}

void SQLBeginTransaction(sqlite3 *db) {
    SQLEXEC(db, "Begin Transaction", NULL, NULL);
}

void SQLCommitTransaction(sqlite3 *db) {
    SQLEXEC(db, "Commit Transaction", NULL, NULL);
}

void SQLRollbackTransaction(sqlite3 *db) {
    SQLEXEC(db, "Rollback Transaction", NULL, NULL);
}

int SQLBindTextByName(sqlite3_stmt *stmt,
                      const char *name,
                      const char *str,
                      void (*destructor)(void *)) {
    int col = sqlite3_bind_parameter_index(stmt, name);
    if (col <= 0) {
        return SQLITE_NOTFOUND;
    }
    if (str == NULL) {
        return sqlite3_bind_null(stmt, col);
    } else {
        return sqlite3_bind_text(stmt, col, str, -1, destructor);
    }
}

int SQLBindIntByName(sqlite3_stmt *stmt, const char *name, int val) {
    int col = sqlite3_bind_parameter_index(stmt, name);
    if (col <= 0) {
        return SQLITE_NOTFOUND;
    }
    return sqlite3_bind_int(stmt, col, val);
}

int SQLBindInt64ByName(sqlite3_stmt *stmt, const char *name, sqlite3_int64 val) {
    int col = sqlite3_bind_parameter_index(stmt, name);
    if (col <= 0) {
        return SQLITE_NOTFOUND;
    }
    return sqlite3_bind_int64(stmt, col, val);
}

int SQLBindNullByName(sqlite3_stmt *stmt, const char *name) {
    int col = sqlite3_bind_parameter_index(stmt, name);
    if (col <= 0) {
        return SQLITE_NOTFOUND;
    }
    return sqlite3_bind_null(stmt, col);
}

sqlite3_int64 SQLMaxAutoIncrement(sqlite3 *db, char *table) {
    sqlite3_int64 max = 0;
    const char *sql = "SELECT seq FROM sqlite_sequence WHERE name = :table";
    sqlite3_stmt *stmt = NULL;
    SQLPrepare(db, sql, &stmt);
    SQLOK(SQLBindTextByName(stmt, ":table", table, SQLITE_STATIC));
    int result = sqlite3_step(stmt);
    if (result == SQLITE_ROW) {
        max = sqlite3_column_int64(stmt, 0);
    } else if (result != SQLITE_DONE) {
        SQLPANIC(result);
    }
    sqlite3_finalize(stmt);
    return max;
}

void SQLPrepare(sqlite3 *db, const char *sql, sqlite3_stmt **stmt) {
    const char *unused;

    int result = sqlite3_prepare_v3(db, sql, -1, 0, stmt, &unused);
    if (result != SQLITE_OK) {
        Log("SQL prepare failed %d: %s %s", result, unused, sql);
        SQLOK(result);
    }
}

void SQLFreeTable(struct SQLNode **table) {
    struct SQLNode *curr, *next;

    for (curr = *table; curr; curr = next) {
        next = curr->next;
        FreeThenNull(curr->element);
        FreeThenNull(curr);
    }
    *table = NULL;
}

static struct SQLNode SQLAddNode(struct SQLNode **head,
                                 struct SQLNode **last,
                                 const unsigned char *element) {
    struct SQLNode *next;

    if (!(next = calloc(1, sizeof(*next))))
        PANIC;
    if (element != NULL && !(next->element = (unsigned char *)strdup(element)))
        PANIC;
    if (*last) {
        (*last)->next = next;
    } else {
        *head = next;
    }
    *last = next;
}

struct SQLNode *SQLGetTable(sqlite3_stmt *stmt, int *rows, int *cols) {
    int result;
    struct SQLNode *head = NULL, *last = NULL;
    int i;

    *rows = 0;
    *cols = sqlite3_column_count(stmt);

    while ((result = sqlite3_step(stmt)) == SQLITE_ROW) {
        if (*rows == 0) {
            for (i = 0; i < *cols; i++) {
                SQLAddNode(&head, &last, sqlite3_column_name(stmt, i));
            }
            *rows = *rows + 1;
        }
        for (i = 0; i < *cols; i++) {
            SQLAddNode(&head, &last, sqlite3_column_text(stmt, i));
        }
        *rows = *rows + 1;
    }
    SQLDONE(result);
    return head;
}
