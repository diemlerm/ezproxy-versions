#define __UUFILE__ "adminlogout.c"

#include "common.h"
#include "uustring.h"

#include "adminlogout.h"
#include "saml.h"

void AdminLogout(struct TRANSLATE *t, char *query, char *post) {
    struct SESSION *e = t->session;
    const int FFURL = 0;
    const int FFQURL = 1;
    struct FORMFIELD ffs[] = {
        {"url",  NULL, 0, 0, 0},
        {"qurl", NULL, 0, 0, 0},
        {NULL,   NULL, 0, 0, 0},
    };
    char *url = NULL;
    char *issuer = NULL;
    char *nameID = NULL;
    char *nameIDFormat = NULL;
    char *nameIDNameQualifier = NULL;
    char *nameIDSPNameQualifier = NULL;
    char *sessionIndex = NULL;
    BOOL handled = FALSE;

    /* If this is a GET method, we want to pull the query string out as unharmed as possible */
    if (query != NULL) {
        for (url = query; url = StrIStr(url, "url="); url++)
            if (url == query || *(url - 1) == '&')
                break;
        if (url) {
            ffs[FFURL].value = url + 4; /* Skip url= */
            if (url == query)
                query = NULL;
            else
                *(url - 1) = 0;
        }
    }

    FindFormFields(query, post, ffs);

    if (ffs[FFQURL].value)
        ffs[FFURL].value = ffs[FFQURL].value;

    if (ffs[FFURL].value && *ffs[FFURL].value) {
        /* Make sure session is stopped before we redirect in case someone is trying
           to redirect back to the /login page (odds of browser making there before
           we reach StopSession slim, but why take chances?
        */
        StopSession(t->session, "/logout");
        /* Always allow /logout?url=/ as a way to redirect back to another EZproxy
           URL, but be sure to block /logout?url=//somehost/ possibility
        */

        struct REDIRECTSAFEORNEVERPROXYURLDATABASE *RedirectSafeOrNeverProxyURLDatabase;
        if (!(RedirectSafeOrNeverProxyURLDatabase =
                  calloc(1, sizeof(struct REDIRECTSAFEORNEVERPROXYURLDATABASE))))
            PANIC;
        RedirectSafeOrNeverProxyURLDatabase =
            RedirectSafeOrNeverProxyURL(ffs[FFURL].value, 1, RedirectSafeOrNeverProxyURLDatabase);
        if ((ffs[FFURL].value[0] == '/' &&
             (ffs[FFURL].value[1] == 0 || ffs[FFURL].value[1] != '/')) ||
            RedirectSafeOrNeverProxyURLDatabase->result)
            SimpleRedirect(t, ffs[FFURL].value, 0);
        else {
            HTMLErrorPage(t, "Unauthorized URL",
                          "<p>The hostnames of URLs specified with /logout?url= must be explicitly "
                          "authorized using <a "
                          "href='http://www.oclc.org/support/documentation/ezproxy/cfg/"
                          "redirectsafe/'>RedirectSafe</a>.</p>\n");
        }
        FreeThenNull(RedirectSafeOrNeverProxyURLDatabase);
    } else {
        if (e) {
            issuer = VariablesGetValue(e->sessionVariables, SESSION_VARIABLE_SAML_ISSUER);
            nameID = VariablesGetValue(e->sessionVariables, SESSION_VARIABLE_SAML_NAMEID);
            nameIDFormat =
                VariablesGetValue(e->sessionVariables, SESSION_VARIABLE_SAML_NAMEID_FORMAT);
            nameIDNameQualifier =
                VariablesGetValue(e->sessionVariables, SESSION_VARIABLE_SAML_NAMEID_NAME_QUALIFIER);
            nameIDSPNameQualifier = VariablesGetValue(
                e->sessionVariables, SESSION_VARIABLE_SAML_NAMEID_SP_NAME_QUALIFIER);
            sessionIndex =
                VariablesGetValue(e->sessionVariables, SESSION_VARIABLE_SAML_SESSION_INDEX);
            if (SAMLLogoutRequest(t, issuer, nameID, nameIDFormat, nameIDNameQualifier,
                                  nameIDSPNameQualifier, sessionIndex)) {
                handled = TRUE;
            }
        }

        if (!handled) {
            HTMLHeader(t, htmlHeaderNoCache);
            SendFile(t, LOGOUTHTM, SFREPORTIFMISSING);
        }

        StopSession(t->session, "/logout");
    }

    FreeThenNull(issuer);
    FreeThenNull(nameID);
    FreeThenNull(nameIDFormat);
    FreeThenNull(nameIDNameQualifier);
    FreeThenNull(nameIDSPNameQualifier);
    FreeThenNull(sessionIndex);
}
