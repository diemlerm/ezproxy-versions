#ifndef __USRTICKET_H__
#define __USRTICKET_H__

#include "common.h"

int FindUserTicket(struct TRANSLATE *t,
                   struct USERINFO *uip,
                   char *file,
                   int f,
                   struct FILEREADLINEBUFFER *frb,
                   struct sockaddr_storage *pInterface);

#endif  // __USRTICKET_H__
