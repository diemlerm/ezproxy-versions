#define __UUFILE__ "adminheaders.c"

#include "common.h"
#include "uustring.h"

#include "adminheaders.h"

void AdminHeaders(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    char *p;

    if (!optionHeaders) {
        HTTPCode(t, 404, 1);
        return;
    }

    HTMLHeader(t, htmlHeaderOmitCache);

    UUSendZ(s, "<tt>\r\n");
    for (p = t->requestHeaders; *p; p = strchr(p, 0) + 1) {
        SendHTMLEncoded(s, p);
        UUSendZ(s, "<br>\r\n");
    }
    UUSendZ(s, "</tt>\r\n");
}
