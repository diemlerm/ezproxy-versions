#define __UUFILE__ "win32.c"
#ifdef WIN32

#include "common.h"
#include "win32.h"
#include <aclapi.h>
#include "service.h"
#include "ezproxyversion.h"
#include <dbghelp.h>
#include <inttypes.h>
#pragma comment(lib, "dbghelp.lib")

CRITICAL_SECTION csException;

BOOL cancleAck = 0;

/* EINTR doesn't apply on Windows, so on this platform, the value is what the value is */
long int interrupted_neg_int(long int rc) {
    return rc;
}

/* EINTR doesn't apply on Windows, so on this platform, the value is what the value is */
FILE *interrupted_null_file(FILE *f) {
    return f;
}

void ackCancel() {
    cancleAck = 1;
}

void EnablePrivilege(void) {
    return;
}

void SuspendPrivilege(void) {}

void AbdicatePrivilege(void) {}

void StartWinsock(void) {
    static BOOL wsaStarted = FALSE;
    WORD wVersionRequested;
    WSADATA wsaData;

    if (!wsaStarted) {
        wVersionRequested = MAKEWORD(2, 0);

        wsaStarted = WSAStartup(wVersionRequested, &wsaData) == 0;
        if (!wsaStarted) {
            Log("No usable winsock.dll found");
            exit(1);
        }

        if (LOBYTE(wsaData.wVersion) < 2 ||
            (LOBYTE(wsaData.wVersion) == 2 && HIBYTE(wsaData.wVersion) < 0)) {
            Log("No version compatible winsock.dll found");
            exit(1);
        }
    }
}

int UUInitMutexOS(UUMUTEX *uumutex) {
#ifdef UUWIN32DEADLOCKCHECK
    if (!(uumutex->mutex = CreateMutex(NULL, FALSE, NULL)))
        PANIC;
#else
    InitializeCriticalSection(&uumutex->mutex);
#endif
    return 0;
}

int UUAcquireMutexOS(UUMUTEX *uumutex, const char *file, int line) {
#ifdef UUWIN32DEADLOCKCHECK
    DWORD result;
    /* If we don't get the mutex within the timeout,
       something is seriously wrong and we declare deadlock
    */
    result = WaitForSingleObject(uumutex->mutex, dMutexTimeout);
    if (result != WAIT_OBJECT_0)
        UUDeadlockMutex(uumutex, file, line);
#else
    EnterCriticalSection(&uumutex->mutex);
#endif
    return 0;
}

int UUTryAcquireMutexOS(UUMUTEX *uumutex) {
#ifdef UUWIN32DEADLOCKCHECK
    DWORD result;
    result = WaitForSingleObject(uumutex->mutex, 0);
    if (result == WAIT_TIMEOUT)
        return 0;
    if (result != WAIT_OBJECT_0)
        PANIC;
    return 1;
#else
    return TryEnterCriticalSection(&uumutex->mutex);
#endif
}

int UUReleaseMutexOS(UUMUTEX *uumutex) {
#ifdef UUWIN32DEADLOCKCHECK
    ReleaseMutex(uumutex->mutex);
#else
    LeaveCriticalSection(&uumutex->mutex);
#endif
    return 0;
}

static void StackTrace64(int f, PCONTEXT pCtx) {
    CONTEXT context;
    KNONVOLATILE_CONTEXT_POINTERS nvContext;
    PRUNTIME_FUNCTION runtimeFunction;
    PVOID handlerData;
    ULONG64 establisherFrame;
    ULONG64 imageBase;
    char msg[1024];

    memcpy(&context, pCtx, sizeof(context));

    while (context.Rip) {
        snprintf(msg, sizeof(msg), "0x%p\n", context.Rip);
        write(f, msg, strlen(msg));

        runtimeFunction = RtlLookupFunctionEntry(context.Rip, &imageBase,
                                                 NULL  // &unwindHistoryTable
        );

        RtlZeroMemory(&nvContext, sizeof(KNONVOLATILE_CONTEXT_POINTERS));

        if (!runtimeFunction) {
            // If we don't have a RUNTIME_FUNCTION, then we've encountered
            // a leaf function.  Adjust the stack approprately.
            context.Rip = (ULONG64)(*(PULONG64)context.Rsp);
            context.Rsp += 8;
        } else {
            // Otherwise, call upon RtlVirtualUnwind to execute the unwind for us.
            RtlVirtualUnwind(UNW_FLAG_NHANDLER, imageBase, context.Rip, runtimeFunction, &context,
                             &handlerData, &establisherFrame, &nvContext);
        }
    }
}

/*

// This function does not walk the stack correctly and could never figure out just why.

static void StackTrace(int f, PCONTEXT pCtx) {
    DWORD machineType;
    CONTEXT context;
    STACKFRAME64 stackFrame;
    HANDLE currentProcess = GetCurrentProcess();
    HANDLE currentThread = GetCurrentThread();

    char error[1024];

    memcpy(&context, pCtx, sizeof(context));
    memset(&stackFrame, 0, sizeof(stackFrame));
#ifdef _M_IX86
    machineType = IMAGE_FILE_MACHINE_I386;
    stackFrame.AddrPC.Offset = context.Eip;
    stackFrame.AddrPC.Mode = AddrModeFlat;
    stackFrame.AddrFrame.Offset = context.Ebp;
    stackFrame.AddrFrame.Mode = AddrModeFlat;
    stackFrame.AddrStack.Offset = context.Esp;
    stackFrame.AddrStack.Mode = AddrModeFlat;
#elif _M_X64
    machineType = IMAGE_FILE_MACHINE_AMD64;
    stackFrame.AddrPC.Offset = context.Rip;
    stackFrame.AddrPC.Mode = AddrModeFlat;
    stackFrame.AddrFrame.Offset = context.Rbp;
    stackFrame.AddrFrame.Mode = AddrModeFlat;
    stackFrame.AddrStack.Offset = context.Rsp;
    stackFrame.AddrStack.Mode = AddrModeFlat;
#elif _M_IA64
    machineType = IMAGE_FILE_MACHINE_IA64;
    stackFrame.AddrPC.Offset = context.StIIP;
    stackFrame.AddrPC.Mode = AddrModeFlat;
    stackFrame.AddrFrame.Offset = context.IntSp;
    stackFrame.AddrFrame.Mode = AddrModeFlat;
    stackFrame.AddrBStore.Offset = context.RsBSP;
    stackFrame.AddrBStore.Mode = AddrModeFlat;
    stackFrame.AddrStack.Offset = context.IntSp;
    stackFrame.AddrStack.Mode = AddrModeFlat;
#else
#error "Unrecognized machine type"
#endif

    for (;;) {
        if (!StackWalk64(
            machineType,
            currentProcess,
            currentThread,
            &stackFrame,
            pCtx,
            NULL,
            SymFunctionTableAccess64,
            SymGetModuleBase64,
            NULL)) {
            break;
        }


        // Reached base of stack
        if (stackFrame.AddrPC.Offset == 0) {
            break;
        }

        sprintf(error, "%016llX\n", (ULONG64) stackFrame.AddrPC.Offset);
        _write(f, error, (unsigned int) strlen(error));
    }
}
*/

LONG __stdcall StackTraceExceptionFilter(LPEXCEPTION_POINTERS e) {
    static char first = 1;
    int f;
    char error[512];
    time_t now;
    PEXCEPTION_RECORD pExceptionRecord = e->ExceptionRecord;
    PCONTEXT pCtx = e->ContextRecord;
    DWORD section, offset;
    TCHAR szFaultingModule[MAX_PATH];
    struct tm *tm;

    EnterCriticalSection(&csException);
    if (first == 0)
        ExitProcess(0);
    first = 0;
    LeaveCriticalSection(&csException);

    f = open("ezproxy.ftl", O_APPEND | O_CREAT | O_WRONLY, defaultFileMode);

    UUtime(&now);
    ASCIIDate(&now, error);

    UUwriteFD(f, EZPROXYVERSION, strlen(EZPROXYVERSION));
    UUwriteFD(f, "\n", 1);

    time(&now);
    tm = localtime(&now);
    strftime(error, sizeof(error), "%Y-%m-%d %H:%M:%S critical exception stack trace\n", tm);
    UUwriteFD(f, error, strlen(error));
    StackTrace64(f, pCtx);
    close(f);
    Log("Terminating critical exception");
    CloseMsgFile();
    ExitProcess(0);
    return 0;
}

void WriteTrace(int f, int sig, void *faultAddr, char *message) {
    char buffer[128];
    time_t now;
    CONTEXT ctx;
    BOOL rc;

    UUtime(&now);
    ASCIIDate(&now, buffer);
    UUwriteFD(f, buffer, strlen(buffer));

    sprintf(buffer, " PID %d", getpid());
    UUwriteFD(f, buffer, strlen(buffer));

    if (sig != 0) {
        sprintf(buffer, " caught signal %d", sig);
        UUwriteFD(f, buffer, strlen(buffer));
    }

    if (faultAddr != NULL) {
        sprintf(buffer, " at address %p", faultAddr);
        UUwriteFD(f, buffer, strlen(buffer));
    }

    if ((message != NULL) && (*message != 0)) {
        UUwriteFD(f, ", ", 2);
        UUwriteFD(f, message, strlen(message));
    }

    UUwriteFD(f, "\n", 1);

    RtlCaptureContext(&ctx);
    StackTrace64(f, &ctx);
}

void DisableDebugger(void) {
    InitializeCriticalSection(&csException);
    SetUnhandledExceptionFilter(StackTraceExceptionFilter);
}

void MapGuardian(BOOL allowFailure) {
    HANDLE mh;
    char *errorMsg;

    guardian = NULL;

    if (guardianSharedMemoryID == NULL) {
        if (allowFailure)
            return;
        printf("Where to map memory?\n");
        exit(1);
    }

    mh = OpenFileMapping(FILE_MAP_ALL_ACCESS, 0, guardianSharedMemoryID);
    if (mh == NULL) {
        errorMsg = ErrorMessage(GetLastError());
        Log("OpenFileMapping failed: %s", errorMsg);
        if (allowFailure)
            return;
        /* should free errorMsg, but we're terminating, so who cares? */
        exit(1);
    }

    guardian =
        (struct GUARDIAN *)MapViewOfFile(mh, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(struct GUARDIAN));

    if (guardian == NULL) {
        if (allowFailure)
            return;
        errorMsg = ErrorMessage(GetLastError());
        Log("MapViewOfFile failed: %s", errorMsg);
        /* should free errorMsg, but we're terminating, so who cares? */
        exit(1);
    }
}

PTOKEN_USER MySid(void) {
    HANDLE htok = NULL;
    PTOKEN_USER pTokenUser = NULL;
    DWORD tokenUserLength;
    SID_IDENTIFIER_AUTHORITY sia = SECURITY_NT_AUTHORITY;

    pTokenUser = NULL;

    OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &htok);

    if (GetTokenInformation(htok, TokenUser, NULL, 0, &tokenUserLength) == 0 &&
        GetLastError() != ERROR_INSUFFICIENT_BUFFER) {
        Log("GetTokenInformation 1 error %d", GetLastError());
        goto Failed;
    }

    if (!(pTokenUser = (PTOKEN_USER)malloc(tokenUserLength)))
        PANIC;

    if (GetTokenInformation(htok, TokenUser, pTokenUser, tokenUserLength, &tokenUserLength) == 0) {
        Log("GetTokenInformation 2 error %d", GetLastError());
        FreeThenNull(pTokenUser);
        goto Failed;
    }

Failed:
    if (htok)
        CloseHandle(htok);

    return pTokenUser;
}

void AdminACL(SECURITY_ATTRIBUTES *psa) {
    DWORD dwRes;
    PSID pEveryoneSID = NULL, pAdminSID = NULL, pSystemSID;
    PACL pACL = NULL;
    PSECURITY_DESCRIPTOR pSD = NULL;
    EXPLICIT_ACCESS ea[4];
    SID_IDENTIFIER_AUTHORITY SIDAuthWorld = SECURITY_WORLD_SID_AUTHORITY;
    SID_IDENTIFIER_AUTHORITY SIDAuthNT = SECURITY_NT_AUTHORITY;
    PTOKEN_USER mySid;

    mySid = MySid();

    // Create a SID for the BUILTIN\Administrators group.
    if (!AllocateAndInitializeSid(&SIDAuthNT, 2, SECURITY_BUILTIN_DOMAIN_RID,
                                  DOMAIN_ALIAS_RID_ADMINS, 0, 0, 0, 0, 0, 0, &pAdminSID)) {
        Log("AllocateAndInitializeSid Error %u\n", GetLastError());
        exit(1);
    }

    // Create a SID for the local system account
    if (!AllocateAndInitializeSid(&SIDAuthNT, 1, SECURITY_LOCAL_SYSTEM_RID, 0, 0, 0, 0, 0, 0, 0,
                                  &pSystemSID)) {
        Log("AllocateAndInitializeSid Error %u\n", GetLastError());
        exit(1);
    }

    memset(&ea, 0, sizeof(ea));

    // Initialize an EXPLICIT_ACCESS structure for an ACE.
    // The ACE will allow the Administrators group full access to the key.
    ea[0].grfAccessPermissions = SECTION_ALL_ACCESS;
    ea[0].grfAccessMode = SET_ACCESS;
    ea[0].grfInheritance = NO_INHERITANCE;
    ea[0].Trustee.pMultipleTrustee = NULL;
    ea[0].Trustee.MultipleTrusteeOperation = NO_MULTIPLE_TRUSTEE;
    ea[0].Trustee.TrusteeForm = TRUSTEE_IS_SID;
    ea[0].Trustee.TrusteeType = TRUSTEE_IS_GROUP;
    ea[0].Trustee.ptstrName = (LPTSTR)pAdminSID;

    // Initialize an EXPLICIT_ACCESS structure for an ACE.
    // The ACE will allow the local system account full access to the key.
    ea[1].grfAccessPermissions = SECTION_ALL_ACCESS;
    ea[1].grfAccessMode = SET_ACCESS;
    ea[1].grfInheritance = NO_INHERITANCE;
    ea[1].Trustee.pMultipleTrustee = NULL;
    ea[1].Trustee.MultipleTrusteeOperation = NO_MULTIPLE_TRUSTEE;
    ea[1].Trustee.TrusteeForm = TRUSTEE_IS_SID;
    ea[1].Trustee.TrusteeType = TRUSTEE_IS_GROUP;
    ea[1].Trustee.ptstrName = (LPTSTR)pSystemSID;

    // Also allow my process control, if I run from an unprivileged account
    /*
    ea[2].grfAccessPermissions = GENERIC_ALL;
    ea[2].grfAccessMode = GRANT_ACCESS;
    ea[2].grfInheritance= NO_INHERITANCE;
    ea[2].Trustee.MultipleTrusteeOperation = NO_MULTIPLE_TRUSTEE;
    ea[2].Trustee.TrusteeForm = TRUSTEE_IS_SID;
    ea[2].Trustee.TrusteeType = TRUSTEE_IS_USER;
    ea[2].Trustee.ptstrName  = (LPTSTR) mySid->User.Sid;
    */

    ea[2].grfAccessPermissions = SECTION_ALL_ACCESS;
    ea[2].grfAccessMode = SET_ACCESS;
    ea[2].grfInheritance = NO_INHERITANCE;
    ea[2].Trustee.pMultipleTrustee = NULL;
    ea[2].Trustee.MultipleTrusteeOperation = NO_MULTIPLE_TRUSTEE;
    ea[2].Trustee.TrusteeForm = TRUSTEE_IS_NAME;
    ea[2].Trustee.TrusteeType = TRUSTEE_IS_USER;
    ea[2].Trustee.ptstrName = "CURRENT_USER";

    // Create a new ACL that contains the new ACEs.
    dwRes = SetEntriesInAcl(3, ea, NULL, &pACL);
    if (ERROR_SUCCESS != dwRes) {
        printf("SetEntriesInAcl Error %u\n", GetLastError());
        exit(1);
    }

    // Initialize a security descriptor.
    pSD = (PSECURITY_DESCRIPTOR)LocalAlloc(LPTR, SECURITY_DESCRIPTOR_MIN_LENGTH);
    if (NULL == pSD) {
        Log("LocalAlloc Error %u\n", GetLastError());
        exit(1);
    }

    if (!InitializeSecurityDescriptor(pSD, SECURITY_DESCRIPTOR_REVISION)) {
        Log("InitializeSecurityDescriptor Error %u\n", GetLastError());
        exit(1);
    }

    // Add the ACL to the security descriptor.
    if (!SetSecurityDescriptorDacl(pSD,
                                   TRUE,  // bDaclPresent flag
                                   pACL,
                                   FALSE))  // not a default DACL
    {
        Log("SetSecurityDescriptorDacl Error %u\n", GetLastError());
        exit(1);
    }

    // Initialize a security attributes structure.
    psa->nLength = sizeof(SECURITY_ATTRIBUTES);
    psa->lpSecurityDescriptor = pSD;
    psa->bInheritHandle = FALSE;

    /*
    if (mySid)
        free(mySid);
        mySid = NULL;
    }
    if (pAdminSID)
        FreeSid(pAdminSID);
    if (pACL)
        LocalFree(pACL);
    if (pSD)
        LocalFree(pSD);
    */
}

int Guardian(char *guardianMap, int argc, char **argv) {
    HANDLE mh;
    static char mapName[256];
    int seized, seizedCharge, seizedMainLocation, seizedRawLocation, seizedStopSocketsLocation;
    BOOL childStopped = FALSE;
    BOOL childStoppedEver = FALSE;
    int seizedCount = 0;
    int chargeStartsFailed;
    time_t lastStarted;
    char *myCmd;
    size_t myCmdLen;
    int i;
    char *q;
    char *p;
    char myApp[MAX_PATH];
    STARTUPINFO startupInfo;
    PROCESS_INFORMATION child;
    DWORD waitStatus;
    int grantPid = 0;
    SECURITY_ATTRIBUTES sa;
    int loginPortsFailed;
    time_t now;
    DWORD endTicks;

    if (guardianMap != NULL) {
        guardianSharedMemoryID = strdup(guardianMap);
        if (stricmp(guardianMap, "none") != 0)
            MapGuardian(0);
        else {
            ResetGuardian(0);
            SaveIPC();
            if (optionLogThreadStartup) {
                Log(EZPROXYNAMEPROPER " without guardian.");
            }
        }
        guardian->chargeStatus = chargeStarting;
        return IAmTheCharge;
    }

    if (optionLogThreadStartup) {
        Log("Thread starting: guardian.");
    }

    sprintf(mapName, "Global\\EZproxy.%d%s", getpid(), GUARDIANGUID);
    guardianMap = mapName;
    guardianSharedMemoryID = guardianMap;

    AdminACL(&sa);

    mh = CreateFileMapping(INVALID_HANDLE_VALUE, &sa, PAGE_READWRITE, 0, sizeof(struct GUARDIAN),
                           mapName);
    if (mh == NULL) {
        /* Older versions of Windows don't understand the Global\ prefix, so retry without */
        char *bslash = strchr(mapName, '\\');
        if (bslash == NULL)
            PANIC;
        StrCpyOverlap(mapName, bslash + 1);
        mh = CreateFileMapping(INVALID_HANDLE_VALUE, &sa, PAGE_READWRITE, 0,
                               sizeof(struct GUARDIAN), mapName);
        if (mh == NULL)
            PANIC;
    }

    guardian =
        (struct GUARDIAN *)MapViewOfFile(mh, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(struct GUARDIAN));
    if (guardian == NULL)
        PANIC;

    myCmdLen = strlen(GUARDIANMAPOPTION) + strlen(mapName) +
               32; /* a little extra space in case we missed anything */

    /* The initial +1 will cover the null at the end of the string; all others cover space
     * separators */
    for (i = 0; i < argc; i++) {
        myCmdLen += 1 + strlen(argv[i]);
        for (q = strchr(argv[i], '"'); q; q = strchr(q + 1, '"')) {
            myCmdLen++;
        }
    }

    if (!(p = myCmd = malloc(myCmdLen)))
        PANIC;

    for (i = 0; i < argc; i++) {
        if (i > 0)
            *p++ = ' ';
        *p++ = '"';
        for (q = argv[i]; *q; q++) {
            if (i > 0 && *q == '"')
                *p++ = '\\';
            *p++ = *q;
        }
        *p++ = '"';
        if (i == 0) {
            sprintf(p, " %s %s", GUARDIANMAPOPTION, mapName);
            p = strchr(p, 0);
        }
    }
    *p = 0;

    GetModuleFileName(NULL, myApp, sizeof(myApp));
    GetStartupInfo(&startupInfo);

    WaitForSingleObject(mStopPending, INFINITE);

    loginPortsFailed = 0;
    Log("Guardian initialized");
    SaveIPC();

    chargeStartsFailed = 0;
    for (; !guardian->guardianStop;) {
        OpenMsgFile();
        Log("Guardian starting EZproxy");
        printf("\n");
        UUtime(&lastStarted);
        ResetGuardian(1);

        /* start suspended so we can log if there was an error, but otherwise hand-off the file */
        if (CreateProcess(myApp, myCmd, NULL, NULL, 0, CREATE_SUSPENDED, NULL, NULL, &startupInfo,
                          &child) == 0) {
            Log("CreateProcess failed %s", ErrorMessage(GetLastError()));
            PANIC;
        }
        guardian->cpid = child.dwProcessId;
        SaveIPC();
        CloseMsgFile();

        if (ResumeThread(child.hThread) == -1) {
            OpenMsgFile();
            Log("ResumeThread failed %d", GetLastError());
        }

        /* We are done with the handle to the starting thread, so close it */
        CloseHandle(child.hThread);

        /* We are still using the handle to the process, so it remains open until
           the following loop completes
        */

        seized = seizedCharge = seizedMainLocation = seizedRawLocation = seizedStopSocketsLocation =
            -1;
        for (;;) {
            if (grantPid) {
                if (!ProcessAlive(grantPid) || guardian->serverGrantPid != grantPid) {
                    if (guardian->clientRequestPid == grantPid)
                        guardian->clientRequestPid = 0;
                    grantPid = guardian->serverGrantPid = 0;
                }
            }

            if (grantPid == 0 && (grantPid = guardian->clientRequestPid)) {
                if (ProcessAlive(grantPid))
                    guardian->serverGrantPid = grantPid;
                else
                    grantPid = guardian->clientRequestPid = 0;
            }

            waitStatus = WaitForSingleObject(child.hProcess, 1000);

            if (waitStatus == WAIT_OBJECT_0)
                break;
            if (waitStatus != WAIT_TIMEOUT)
                PANIC;

            if (guardian->guardianStop)
                guardian->chargeStop = 1;
            seized = CheckCharge();
            if (seized < 0)
                continue;

            seizedMainLocation = guardian->mainLocation;
            seizedRawLocation = guardian->rawLocation;
            seizedStopSocketsLocation = guardian->stopSocketsLocation;
            seizedCharge = seized;

            /* Charge has stopped updating flags, time to force a restart */
            TerminateProcess(child.hProcess, 0);
            WaitForSingleObject(child.hProcess, 10000);
            /* chargeFroze is not a concept in unix.c Guardian and serves no additional purpose,
               while actually causing EZproxy to shut down if EZproxy freezes more than 5 times.
               Disable this logic to bring parity to the unix.c code as eliminate this failure to
               restart.
            */
            /*
            if (guardian->chargeStatus == chargeRunning)
                guardian->chargeStatus = chargeFroze;
            */
            break;
        }

        CloseHandle(child.hProcess);

        OpenMsgFile();

        UUtime(&now);
        endTicks = GetTickCount();

        if (seizedCharge >= 0) {
            Log("Guardian terminated " EZPROXYNAMEPROPER
                " process because its %s thread's latency is greater than %f seconds (main "
                "location %d, raw location %d, stopSockets location %d).",
                guardianChargeName[seizedCharge], guardianChargeMaximumLatency[seizedCharge],
                seizedMainLocation, seizedRawLocation, seizedStopSocketsLocation);
            if (childStoppedEver) {
                Log("The " EZPROXYNAMEPROPER
                    " process had been stopped by a signal at least once.");
                if (!childStopped) {
                    Log("The " EZPROXYNAMEPROPER
                        " process was runnable at the time the charge seized.");
                }
#ifndef WIFCONTINUED
                Log("There is no way on this platform to detect that a stopped process has been "
                    "restarted except for inferring this from the fact that the process has "
                    "completed some work after it was stopped.");
#endif
            }
        }
        if ((seizedCharge >= 0) || optionLogThreadStartup) {
            seizedCount = 0;
            for (i = 0; i <= GCLAST; i++) {
                Log("The %s thread had%s seized.", guardianChargeName[i],
                    (seized = ChargeHasSeized(i, endTicks)) ? "" : " not");
                if (seized) {
                    seizedCount++;
                }
            }
            if (seizedCount > GCLAST) {
                Log("Because all " EZPROXYNAMEPROPER
                    " threads had seized, it's probable that the process in which they ran had "
                    "seized.");
            }
        }

        if (guardian->chargeStatus == chargeRunning) {
            /* Abnormal termination of the child AKA charge. Try to restart unless guardianStop.*/
            chargeStartsFailed = 0;
        } else if (guardian->chargeStatus == chargeStopping) {
            /* Normal termination of the child AKA charge. */
            guardian->guardianStop = TRUE;

            chargeStartsFailed = 0;
        } else {
            /* Abnormal termination try to restart unless guardianStop. */
            if (guardian->chargeStatus == chargeBeingStarted) {
                Log(EZPROXYNAMEPROPER " process has failed to start.");
            } else if (seizedCharge < 0) {
                Log(EZPROXYNAMEPROPER " %s thread has failed.", guardianChargeName[0]);
            }
            chargeStartsFailed++;
        }

        if (guardian->portOpenFailure) {
            loginPortsFailed++;
            SubSleep(5, 0);
        } else {
            loginPortsFailed = 0;
        }

        if (guardian->portOpenFailure) {
            Log(EZPROXYNAMEPROPER " can't listen on the configured host name and port number.");
        }
        if ((guardian->portOpenFailure & GCPRIVPORTFAILED) || (chargeStartsFailed > 6) ||
            (loginPortsFailed > 6)) {
            guardian->guardianStop = TRUE;
            Log("Guardian is unable to start " EZPROXYNAMEPROPER ".");
            break;
        }

        /* This logic doesn't apply on Windows
        if (((seizedCharge >= 0) || (chargeStartsFailed > 5)) && (loginPortsFailed == 0) &&
        (debugLevel > 2)) { killSignal = SIGABRT; /* maybe get a core dump */ /*
        }
        */

        SaveIPC();
    }

    ReleaseMutex(mStopPending);

    if (optionLogThreadStartup) {
        Log("Thread exiting: guardian.");
    }

    return IAmTheGuardian;
}

UUTHREADID UUThreadId(void) {
    return GetCurrentThreadId();
}
#endif

void SetEUidGid(void) {}

void ResetEUidGid(void) {}

void SetUidGid(void) {}
