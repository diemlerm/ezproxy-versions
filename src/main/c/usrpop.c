/**
 * POP (email protocol) user authentication
 *
 * This authentication method DOES NOT SUPPORT common conditions and actions.
 */

#define __UUFILE__ "usrpop.c"

#include "common.h"
#include "uustring.h"

#include "usr.h"
#include "usrpop.h"

/* Returns 0 if valid, 1 if not, 3 if unable to connect to pop host */
int FindUserPop(char *popHost,
                char *user,
                char *pass,
                struct sockaddr_storage *pInterface,
                char useSsl,
                BOOL debug,
                BOOL noAPOP) {
    struct UUSOCKET rr, *r = &rr;
    int result;
    int state;
    char line[256];
    char *langle, *rangle;
    MD5_CTX md5Context;
    unsigned char md5Signature[16];
    char md5HexSignature[33];

    UUInitSocket(r, INVALID_SOCKET);

    result = 3;

    if (popHost == NULL || user == NULL || pass == NULL)
        goto Finished;

    if (UUConnectWithSource2(r, popHost, (PORT)(useSsl ? 995 : 110), "POP", pInterface, useSsl))
        goto Finished;

    if (debug)
        Log("POP connect to %s", popHost);

    result = 1;
    for (state = (noAPOP ? 0 : -2);;) {
        if (ReadLine(r, line, sizeof(line)) == 0) {
            result = 3;
            break;
        }
        if (debug)
            Log("POP got %s", line);
        if (strnicmp(line, "+OK", 3) != 0) {
            if (state == -1 || state == 2) {
                /* Check to see if it could be a mailbox already locked, and if so, allow it */
                /* But make sure no one is spoofing us with a username of lock */
                if (StrIStr(line, " lock") != NULL && StrIStr(user, "lock") == NULL)
                    result = 0;
                /* If this is VMS, we get an error if they've never read mail, but assume they did
                 * authenticate */
                if (StrIStr(line, "Could not open VMS mail box") != NULL &&
                    StrIStr(user, "VMS") == NULL)
                    result = 0;
            }
            if (state >= 0 || result == 0)
                break;
            state = 0;
        }

        if (state == -1) {
            result = 0;
            break;
        }

        if (state == -2) {
            if ((langle = strchr(line, '<')) && (rangle = strchr(langle, '>'))) {
                *(rangle + 1) = 0;
                MD5_Init(&md5Context);
                MD5_Update(&md5Context, (unsigned char *)langle, (unsigned int)strlen(langle));
                MD5_Update(&md5Context, (unsigned char *)pass, (unsigned int)strlen(pass));
                MD5_Final(md5Signature, &md5Context);
                MD5DigestToHex(md5HexSignature, md5Signature, 0);
                UUSendZ(r, "APOP ");
                UUSendZ(r, user);
                WriteLine(r, " %s\r\n", md5HexSignature);
                state = -1;
                if (debug)
                    Log("POP sent APOP %s %s", user, md5HexSignature);
                continue;
            }
            state = 0;
        }

        if (state == 0) {
            UUSendZ(r, "USER ");
            UUSendZCRLF(r, user);
            if (debug)
                Log("POP sent USER %s", user);
        } else if (state == 1) {
            UUSendZ(r, "PASS ");
            UUSendZCRLF(r, pass);
            if (debug)
                Log("POP sent password");
        } else {
            result = 0;
            break;
        }
        state++;
    }
    if (result != 3) {
        UUSendZ(r, "QUIT\r\n");
        UUFlush(r);
    }
    if (debug)
        Log("POP result %d, disconnect", result);
    UUStopSocket(r, 0);

Finished:
    return result;
}
