/**
 * SIP user authentication
 *
 * This module implements 3M SIP (Standard Interchange Protocol) user
 * authentication.  SIP is a library standard for interacting with an ILS
 * for circulation and authentication.
 *
 * This authentication method supports common conditions and actions.
 */

#define __UUFILE__ "usrsip.c"

#include "common.h"
#include "usr.h"
#include "usrsip.h"
#include "telnet.h"

char *sipPatronStatus[] = {
    /*  0 */ "charge privileges denied",
    /*  1 */ "renewal privileges denied",
    /*  2 */ "recall privileges denied",
    /*  3 */ "hold privileges denied",
    /*  4 */ "card reported lost",
    /*  5 */ "too many items charged",
    /*  6 */ "too many items overdue",
    /*  7 */ "too many renewals",
    /*  8 */ "too many claims of items returned",
    /*  9 */ "too many items lost",
    /* 10 */ "excessive outstanding fines",
    /* 11 */ "excessive outstanding fees",
    /* 12 */ "recall overdue",
    /* 13 */ "too many items billed",
    NULL};

struct SIPPATRONFIELDS {
    char *field;
    size_t fixedLength;
    char *desc;
};

struct SIPPATRONFIELDS sipPatronFields[] = {
    {"AO", 0, "institution id"        },
    {"AA", 0, "patron identifier"     },
    {"AE", 0, "personal name"         },
    {"BZ", 0, "hold items limit"      },
    {"CA", 0, "overdue items limit"   },
    {"CB", 0, "charged items limit"   },
    {"BL", 0, "valid patron"          },
    {"CQ", 0, "valid patron password" },
    {"BH", 0, "currency type"         },
    {"BV", 0, "fee amount"            },
    {"CC", 0, "fee limit"             },
    {"AS", 0, "hold items"            },
    {"AT", 0, "overdue items"         },
    {"AU", 0, "charged items"         },
    {"AV", 0, "fine items"            },
    {"BU", 0, "recall items"          },
    {"CD", 0, "unavailable hold items"},
    {"BD", 0, "home address"          },
    {"BE", 0, "e-mail address"        },
    {"BF", 0, "home phone number"     },
    {"AF", 0, "screen message"        },
    {"AG", 0, "print line"            },
    {"AY", 1, "sequence"              },
    {"AZ", 4, "checksum"              },
    {NULL, 0, NULL                    }
};

struct SIPPATRONFIXEDFIELDS {
    char *field;
    size_t offset;
    size_t fixedLength;
    char *desc;
};

struct SIPPATRONFIXEDFIELDS sipPatronFixedFields[] = {
    {"HIC", 37, 4, "hold items count"       },
    {"CIC", 41, 4, "overdue items count"    },
    {"FIC", 45, 4, "charged items count"    },
    {"RIC", 49, 4, "recalled items count"   },
    {"UHC", 53, 4, "unavailable holds count"},
    {NULL,  0,  0, NULL                     }
};

struct SIPCTX {
    char *eol;
    char *sipUser;
    char *sipPass;
    BOOL noPatronPassword;
    BOOL seenHost;
    BOOL suppressNewline;
    struct UUSOCKET rsocket;

    struct TELNETSTATE ts;

    char loginUsername[80];
    char loginPassword[80];
    char loginLocation[80];
    char terminalInstitution[80];
    char terminalPassword[80];
    char line[4096];
    char response[4096];
};

void AddSipSequenceChecksum(char *line, int sequence, BOOL suppressNewline) {
    int sum = 0;

    sprintf(strchr(line, 0), "AY%dAZ", sequence % 10);
    while (*line)
        sum += (unsigned char)*line++;
    sum = -sum & 0xffff;
    sprintf(line, "%4.4X\r%s", sum, suppressNewline ? "" : "\n");
}

char *EncodeCtrl(char *s) {
    char u;
    char *p;

    for (p = s; *p; p++) {
        if (*p == '\\') {
            StrCpyOverlap(p, p + 1);
            if (*p == 0)
                break;
            u = *p;
            if (islower(u))
                u = toupper(u);
            switch (u) {
            case 'R':
                *p = 13;
                break;
            case 'N':
                *p = 10;
                break;
            case 'T':
                *p = 9;
                break;
            case 'S':
                *p = ' ';
                break;
            }
        }
    }
    return s;
}

BOOL InvalidSIP(char *s) {
    int len;

    len = 0;

    for (len = 0; *s; len++, s++) {
        if (len > 32)
            return 1;
        if (*s < ' ' || *s > '~' || *s == '|')
            return 1;
    }

    return 0;
}

char *UsrSipVariableStart(const char *line) {
    size_t offset = 0;

    if (strnicmp(line, "24", 2) == 0)
        offset = 37;
    else if (strnicmp(line, "64", 2) == 0)
        offset = 61;
    else
        return NULL;

    if (strlen(line) <= offset)
        return NULL;

    return (char *)line + offset;
}

size_t UsrSipField(char *field, struct SIPPATRONFIELDS **spf, char **vNext) {
    struct SIPPATRONFIELDS *spfs;
    size_t fieldLen;
    char *vbar;
    char *value;

    if (spf)
        *spf = NULL;

    if (vNext)
        *vNext = NULL;

    if (strlen(field) < 2)
        return 0;

    value = field + 2;

    for (spfs = sipPatronFields; spfs->field; spfs++) {
        if (strncmp(field, spfs->field, 2) == 0) {
            if (spf)
                *spf = spfs;
            break;
        }
    }

    /* If nothing matched, we will be at final NULL case where fixedLength is 0,
       causing unknown fields to be treated as variable length
    */
    if (spfs->fixedLength)
        fieldLen = UUMIN(spfs->fixedLength, strlen(value));
    else {
        if (vbar = strchr(value, '|'))
            fieldLen = vbar - value;
        else
            fieldLen = strlen(value);
    }

    if (vNext) {
        *vNext = value + fieldLen;
        while (**vNext == '|')
            *vNext = *vNext + 1;
    }

    return fieldLen;
}

char *UsrSipFindFieldValue(const char *response,
                           const char *field,
                           size_t *retFieldLen,
                           BOOL *numericField) {
    size_t fieldLen;
    char *v, *vNext;
    struct SIPPATRONFIXEDFIELDS *spff;
    int iField;

    if (numericField)
        *numericField = 0;

    if (strlen(field) == 3 && strncmp(response, "64", 2) == 0) {
        for (spff = sipPatronFixedFields; spff->field; spff++) {
            if (strcmp(field, spff->field) == 0) {
                v = (char *)response + spff->offset;
                if (retFieldLen)
                    *retFieldLen = spff->fixedLength;
                return v;
            }
        }
    }

    iField = atoi(field);
    if (strcmp(field, "0") == 0 || (iField > 0 && iField <= 13)) {
        v = (char *)response + iField + 2;
        if (retFieldLen)
            *retFieldLen = 1;
        if (numericField)
            *numericField = 1;
        return v;
    }

    if (v = UsrSipVariableStart(response)) {
        for (; v && *v; v = vNext) {
            fieldLen = UsrSipField(v, NULL, &vNext);
            if (strncmp(v, field, 2) == 0) {
                if (retFieldLen)
                    *retFieldLen = fieldLen;
                return v + 2;
            }
        }
    }

    if (retFieldLen)
        *retFieldLen = 0;

    return NULL;
}

/* Assumes that line isn't static and can be briefly altered in place */

void UsrSipStatus(struct TRANSLATE *t, struct USERINFO *uip, char *response) {
    int i;
    char **sp;
    struct SIPPATRONFIELDS *spf;
    struct SIPPATRONFIXEDFIELDS *spff;
    char *v, *vNext;
    size_t fieldLen;
    char save;
    char c;

    v = UsrSipVariableStart(response);
    if (v == NULL)
        return;

    for (i = 0, sp = sipPatronStatus; *sp; i++, sp++) {
        c = response[i + 2];
        if (c == ' ')
            c = 'N';
        UsrLog(uip, "SIP %d%s (%s) %c", i, (i < 10 ? " " : ""), *sp, c);
    }

    for (; v && *v; v = vNext) {
        fieldLen = UsrSipField(v, &spf, &vNext);
        save = *(v + 2 + fieldLen);
        *(v + 2 + fieldLen) = 0;
        if (spf == NULL) {
            UsrLog(uip, "SIP %c%c (custom) %s", *v, *(v + 1), v + 2);
        } else {
            UsrLog(uip, "SIP %s (%s) %s", spf->field, spf->desc, v + 2);
        }
        *(v + 2 + fieldLen) = save;
    }

    if (strncmp(response, "64", 2) == 0) {
        for (spff = sipPatronFixedFields; spff->field; spff++) {
            v = response + spff->offset;
            save = *(v + spff->fixedLength);
            *(v + spff->fixedLength) = 0;
            UsrLog(uip, "SIP %s (%s) %s", spff->field, spff->desc, v);
            *(v + spff->fixedLength) = save;
        }
    }
}

char *FindUserSipAuthGetValue(struct TRANSLATE *t,
                              struct USERINFO *uip,
                              void *context,
                              const char *var,
                              const char *index) {
    struct SIPCTX *sipCtx = (struct SIPCTX *)context;
    char *val = NULL;
    BOOL numericField;
    char *v;
    size_t fieldLen;

    if (sipCtx->response[0] == 0)
        return NULL;

    if (sipCtx->response[0] &&
        (v = UsrSipFindFieldValue(sipCtx->response, var, &fieldLen, &numericField))) {
        if (!(val = (char *)malloc(fieldLen + 1)))
            PANIC;
        if (fieldLen)
            memcpy(val, v, fieldLen);
        *(val + fieldLen) = 0;
        if (numericField && *val == ' ')
            *val = 'N';
    }

    return val;
}

static int UsrSIPCmd(struct TRANSLATE *t, struct USERINFO *uip, void *context, int sipCmd) {
    struct SIPCTX *sipCtx = (struct SIPCTX *)context;
    struct UUSOCKET *r = &sipCtx->rsocket;
    int tries = 0;
    struct tm *tm, rtm;
    time_t now;
    int sequence = 0;

    if (uip->debug)
        UsrLog(uip, "SIP negotiate");
    UUFlush(r);

    if (sipCtx->loginUsername[0]) {
        sprintf(sipCtx->line, "9300CN%s|CO%s|", sipCtx->loginUsername, sipCtx->loginPassword);
        if (sipCtx->loginLocation[0])
            sprintf(strchr(sipCtx->line, 0), "CP%s|", sipCtx->loginLocation);
        AddSipSequenceChecksum(sipCtx->line, sequence++, sipCtx->suppressNewline);
        if (uip->debug)
            UsrLog(uip, "SIP send %s", sipCtx->line);
        TelnetSendString(&sipCtx->ts, r, sipCtx->line);
        for (;;) {
            if (TelnetRecvLine(&sipCtx->ts, r, sipCtx->line, sizeof(sipCtx->line)) == 0) {
                if (uip->debug)
                    UsrLog(uip, "SIP timeout");
                uip->skipping = TRUE;
                return 1;
            }
            if (uip->debug)
                UsrLog(uip, "SIP receive %s", sipCtx->line);
            if (strnicmp(sipCtx->line, "94", 2) == 0)
                break;
        }
        if (sipCtx->line[2] == '0') {
            UsrLog(uip, "SIP login denied");
            uip->skipping = TRUE;
            return 1;
        }
    }

    sprintf(sipCtx->line, "9900802.00");
    AddSipSequenceChecksum(sipCtx->line, sequence++, sipCtx->suppressNewline);
    if (uip->debug)
        UsrLog(uip, "SIP send %s", sipCtx->line);
    TelnetSendString(&sipCtx->ts, r, sipCtx->line);
    for (;;) {
        if (TelnetRecvLine(&sipCtx->ts, r, sipCtx->line, sizeof(sipCtx->line)) == 0) {
            if (uip->debug)
                UsrLog(uip, "SIP timeout");
            uip->skipping = TRUE;
            return 1;
        }
        if (uip->debug)
            UsrLog(uip, "SIP receive %s", sipCtx->line);
        if (strnicmp(sipCtx->line, "98", 2) == 0)
            break;
    }

    tries = 0;
Retry:
    if (tries > 2) {
        return 1;
    }

    UUtime(&now);
    tm = UUlocaltime_r(&now, &rtm);
    sprintf(sipCtx->line, "%s001%04d%02d%02d    %02d%02d%02d%sAO%s|AA%s|AC%s|",
            (sipCmd == 2 ? "63" : "23"), tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
            tm->tm_hour, tm->tm_min, tm->tm_sec, (sipCmd == 2 ? "          " : ""),
            sipCtx->terminalInstitution, sipCtx->sipUser, sipCtx->terminalPassword);
    if (sipCtx->noPatronPassword == 0) {
        sprintf(strchr(sipCtx->line, 0), "AD%s|", sipCtx->sipPass);
    }

    AddSipSequenceChecksum(sipCtx->line, sequence++, sipCtx->suppressNewline);
    if (uip->debug)
        UsrLog(uip, "SIP send %s", sipCtx->line);
    TelnetSendString(&sipCtx->ts, r, sipCtx->line);

    for (;;) {
        if (TelnetRecvLine(&sipCtx->ts, r, sipCtx->line, sizeof(sipCtx->line)) == 0) {
            if (uip->debug)
                UsrLog(uip, "SIP timeout");
            uip->skipping = TRUE;
            return 1;
        }

        if (uip->debug) {
            UsrLog(uip, "SIP receive %s", sipCtx->line);
            UsrSipStatus(t, uip, sipCtx->line);
        }

        if (sipCtx->line[0])
            break;
    }

    if (strnicmp(sipCtx->line, "96", 2) == 0) {
        tries++;
        goto Retry;
    }

    if (strnicmp(sipCtx->line, "24", 2) != 0 && strnicmp(sipCtx->line, "64", 2) != 0) {
        UsrLog(uip, "SIP unexpected response %s", sipCtx->line);
        uip->skipping = TRUE;
        return 1;
    }

    memset(sipCtx->response, 0, sizeof(sipCtx->response));
    StrCpy3(sipCtx->response, sipCtx->line, sizeof(sipCtx->response));
    if (sipCtx->noPatronPassword) {
        char *bl;
        if (sipCmd != 2 ||
            ((bl = UsrSipFindFieldValue(sipCtx->response, "BL", NULL, NULL)) && *bl == 'Y')) {
            uip->result = resultValid;
        }
    } else {
        if (sipCmd == 1) {
            if (sipCtx->response[2] != 'Y')
                uip->result = resultValid;
        } else {
            char *cq;
            if ((cq = UsrSipFindFieldValue(sipCtx->response, "CQ", NULL, NULL)) && *cq == 'Y')
                uip->result = resultValid;
        }
    }
    return 0;
}

static int UsrSIPExpect(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct SIPCTX *sipCtx = (struct SIPCTX *)context;
    struct UUSOCKET *r = &sipCtx->rsocket;
    size_t lArg, lLine;
    char *match;
    char *bol;
    int len;

    if (uip->debug)
        UsrLog(uip, "SIP expect %s", arg);
    EncodeCtrl(arg);
    lArg = strlen(arg);
    for (;;) {
        if (match = strstr(sipCtx->line, arg)) {
            StrCpyOverlap(sipCtx->line, match + strlen(arg));
            bol = strchr(sipCtx->line, 0);
            break;
        }
        /* No need to preserve more than we could possibly match; creates rolling window */
        lLine = strlen(sipCtx->line);
        if (lLine > lArg)
            StrCpyOverlap(sipCtx->line, sipCtx->line + lLine - lArg);
        bol = strchr(sipCtx->line, 0);
        len = TelnetRecv(&sipCtx->ts, r, bol, sipCtx->eol - bol);
        if (len <= 0) {
            uip->skipping = TRUE;
            return 1;
        }
        if (len > 0) {
            *(bol + len) = 0;
            if (uip->debug)
                UsrLog(uip, "SIP received %s", bol);
        }
    }
    if (uip->debug)
        UsrLog(uip, "SIP got %s", arg);
    return 0;
}

static int UsrSIPHost(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct SIPCTX *sipCtx = (struct SIPCTX *)context;
    struct UUSOCKET *r = &sipCtx->rsocket;
    char *colon;

    if (sipCtx->seenHost) {
        UsrLog(uip, "For SIP, Host may only appear once");
        return 1;
    }
    if (uip->debug)
        UsrLog(uip, "SIP HOST %s", arg);

    ParseHost(arg, &colon, NULL, TRUE);
    if (colon)
        *colon = 0;

    if (UUConnectWithSource(r, arg, (PORT)(colon ? atoi(colon + 1) : 23), "SIP", uip->pInterface)) {
        uip->result = resultRefused;
        return 0;
    }
    r->timeout = 30;
    uip->result = resultInvalid;
    sipCtx->seenHost = 1;
    return 0;
}

static int UsrSIPIfTest(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct SIPCTX *sipCtx = (struct SIPCTX *)context;
    BOOL anyMatch;
    char *arg2, *nextArg;
    char *v;
    char val[256];
    size_t fieldLen;
    char *str;

    char compareOperator;
    char compareType;

    arg2 = SkipNonWS(arg);
    if (*arg2) {
        *arg2++ = 0;
        arg2 = SkipWS(arg2);
    }

    arg2 = UsrTestComparisonOperatorType(arg2, &compareOperator, &compareType);

    if (stricmp(arg, "user") == 0) {
        StrCpy3(val, sipCtx->sipUser, sizeof(val));
    } else if (stricmp(arg, "pass") == 0) {
        /* StrCpy3 deals with the possibility that pass is NULL automatically */
        StrCpy3(val, sipCtx->sipPass, sizeof(val));
    } else {
        char save;
        BOOL numericField;

        v = UsrSipFindFieldValue(sipCtx->response, arg, &fieldLen, &numericField);
        if (v == NULL) {
            return 1;
        }
        save = *(v + fieldLen);
        *(v + fieldLen) = 0;
        StrCpy3(val, v, sizeof(val));
        *(v + fieldLen) = save;
        if (numericField && val[0] == ' ')
            val[0] = 'N';
    }

    for (anyMatch = 0, str = NULL, nextArg = StrTok_R(arg2, ",", &str); nextArg;
         nextArg = StrTok_R(NULL, ",", &str)) {
        if (UsrTestCompare(val, nextArg, compareOperator, compareType)) {
            anyMatch = 1;
            break;
        }
    }
    if (anyMatch)
        return 0;

    return 1;
}

static int UsrSIPLoginLocation(struct TRANSLATE *t,
                               struct USERINFO *uip,
                               void *context,
                               char *arg) {
    struct SIPCTX *sipCtx = (struct SIPCTX *)context;

    StrCpy3(sipCtx->loginLocation, arg, sizeof(sipCtx->loginLocation));
    return 0;
}

static int UsrSIPLoginPassword(struct TRANSLATE *t,
                               struct USERINFO *uip,
                               void *context,
                               char *arg) {
    struct SIPCTX *sipCtx = (struct SIPCTX *)context;

    StrCpy3(sipCtx->loginPassword, arg, sizeof(sipCtx->loginPassword));
    return 0;
}

static int UsrSIPLoginUsername(struct TRANSLATE *t,
                               struct USERINFO *uip,
                               void *context,
                               char *arg) {
    struct SIPCTX *sipCtx = (struct SIPCTX *)context;

    StrCpy3(sipCtx->loginUsername, arg, sizeof(sipCtx->loginUsername));
    return 0;
}

static int UsrSIPMsgAuth(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct SIPCTX *sipCtx = (struct SIPCTX *)context;
    int i;
    char **sp;
    struct SIPPATRONFIELDS *spf;
    struct SIPPATRONFIXEDFIELDS *spff;
    char *v, *vNext;
    size_t fieldLen;
    char save;
    char c;

    UsrLog(uip, "SIP auth variables");

    v = UsrSipVariableStart(sipCtx->response);
    if (v == NULL)
        return 0;

    for (i = 0, sp = sipPatronStatus; *sp; i++, sp++) {
        c = sipCtx->response[i + 2];
        if (c == ' ')
            c = 'N';
        UsrLog(uip, "%d=%c (%s)", i, c, *sp);
    }

    for (; v && *v; v = vNext) {
        fieldLen = UsrSipField(v, &spf, &vNext);
        save = *(v + 2 + fieldLen);
        *(v + 2 + fieldLen) = 0;
        if (spf == NULL) {
            UsrLog(uip, "%c%c=%s (custom)", *v, *(v + 1), v + 2);
        } else {
            UsrLog(uip, "%s=%s (%s)", spf->field, v + 2, spf->desc);
        }
        *(v + 2 + fieldLen) = save;
    }

    if (strncmp(sipCtx->response, "64", 2) == 0) {
        for (spff = sipPatronFixedFields; spff->field; spff++) {
            v = sipCtx->response + spff->offset;
            save = *(v + spff->fixedLength);
            *(v + spff->fixedLength) = 0;
            UsrLog(uip, "%s=%s (%s)", spff->field, v, spff->desc);
            *(v + spff->fixedLength) = save;
        }
    }
    return 0;
}

static int UsrSIPNoPatronPassword(struct TRANSLATE *t,
                                  struct USERINFO *uip,
                                  void *context,
                                  char *arg) {
    struct SIPCTX *sipCtx = (struct SIPCTX *)context;

    sipCtx->noPatronPassword = 1;
    return 0;
}

static int UsrSIPSend(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct SIPCTX *sipCtx = (struct SIPCTX *)context;
    struct UUSOCKET *r = &sipCtx->rsocket;

    if (uip->debug)
        UsrLog(uip, "SIP send %s", arg);
    EncodeCtrl(arg);
    TelnetSendString(&sipCtx->ts, r, arg);
    return 0;
}

static int UsrSIPSIP(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return UsrSIPCmd(t, uip, context, 0);
}

static int UsrSIPSIP1(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return UsrSIPCmd(t, uip, context, 1);
}

static int UsrSIPSIP2(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return UsrSIPCmd(t, uip, context, 2);
}

static int UsrSIPSuppressNewline(struct TRANSLATE *t,
                                 struct USERINFO *uip,
                                 void *context,
                                 char *arg) {
    struct SIPCTX *sipCtx = (struct SIPCTX *)context;

    sipCtx->suppressNewline = TRUE;
    return 0;
}

static int UsrSIPTerminalInstitution(struct TRANSLATE *t,
                                     struct USERINFO *uip,
                                     void *context,
                                     char *arg) {
    struct SIPCTX *sipCtx = (struct SIPCTX *)context;

    StrCpy3(sipCtx->terminalInstitution, arg, sizeof(sipCtx->terminalInstitution));
    return 0;
}

static int UsrSIPTerminalPassword(struct TRANSLATE *t,
                                  struct USERINFO *uip,
                                  void *context,
                                  char *arg) {
    struct SIPCTX *sipCtx = (struct SIPCTX *)context;

    StrCpy3(sipCtx->terminalPassword, arg, sizeof(sipCtx->terminalPassword));
    return 0;
}

static int UsrSIPTimeout(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct SIPCTX *sipCtx = (struct SIPCTX *)context;
    struct UUSOCKET *r = &sipCtx->rsocket;

    if (atoi(arg)) {
        r->timeout = atoi(arg);
        if (uip->debug)
            UsrLog(uip, "SIP timeout %d", r->timeout);
    }
    return 0;
}

static int UsrSIPWait(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct SIPCTX *sipCtx = (struct SIPCTX *)context;
    struct UUSOCKET *r = &sipCtx->rsocket;

    int secs = atoi(arg);
    int millisecs = (int)((atof(arg) - secs) * 1000.0);
    if (r->socketType != INVALID_SOCKET)
        UUFlush(r);
    if (secs > 0 || millisecs > 0) {
        SubSleep(secs, millisecs);
    }
    return 0;
}

/* Returns 0 if valid, 1 if not, 3 if unable to connect to SIP host */
enum FINDUSERRESULT FindUserSip(struct TRANSLATE *t,
                                char *sipHost,
                                struct USERINFO *uip,
                                char *file,
                                int f,
                                struct FILEREADLINEBUFFER *frb,
                                struct sockaddr_storage *pInterface) {
    struct SIPCTX sipCtxBuffer, *sipCtx;
    struct UUSOCKET *r = &sipCtxBuffer.rsocket;
    struct USRDIRECTIVE udc[] = {
        {"Expect",              UsrSIPExpect,              0},
        {"Host",                UsrSIPHost,                0},
        {"IfTest",              UsrSIPIfTest,              0},
        {"LoginLocation",       UsrSIPLoginLocation,       0},
        {"LoginPassword",       UsrSIPLoginPassword,       0},
        {"LoginUsername",       UsrSIPLoginUsername,       0},
        {"MsgAuth",             UsrSIPMsgAuth,             0},
        {"NoPatronPassword",    UsrSIPNoPatronPassword,    0},
        {"Send",                UsrSIPSend,                0},
        {"SIP",                 UsrSIPSIP,                 0},
        {"SIP1",                UsrSIPSIP1,                0},
        {"SIP2",                UsrSIPSIP2,                0},
 // The typo-version SupressNewline was the only available
  // version through 5.6, so keep it but add correct spelling
  // as well
        {"SupressNewline",      UsrSIPSuppressNewline,     0},
        {"SuppressNewline",     UsrSIPSuppressNewline,     0},
        {"TerminalInstitution", UsrSIPTerminalInstitution, 0},
        {"TerminalPassword",    UsrSIPTerminalPassword,    0},
        {"Test",                UsrSIPIfTest,              0},
        {"Timeout",             UsrSIPTimeout,             0},
        {"Wait",                UsrSIPWait,                0},
        {NULL,                  NULL,                      0},
    };

    memset(&sipCtxBuffer, 0, sizeof(sipCtxBuffer));
    sipCtx = &sipCtxBuffer;

    strcpy(sipCtx->terminalInstitution, "EZproxy");

    uip->result = resultInvalid;

    UUInitSocket(r, INVALID_SOCKET);

    TelnetInit(&sipCtx->ts);

    sipCtx->eol = sipCtx->line + sizeof(sipCtx->line) - 1;

    sipCtx->sipUser = uip->user;
    sipCtx->sipPass = uip->pass ? uip->pass : "";

    if (InvalidSIP(sipCtx->sipUser) || InvalidSIP(sipCtx->sipPass))
        sipCtx->sipUser = NULL;

    uip->skipping = sipCtx->sipUser == NULL || *sipCtx->sipUser == 0;

    uip->authGetValue = FindUserSipAuthGetValue;
    uip->result = UsrHandler(t, "SIP", udc, sipCtx, uip, file, f, frb, pInterface, NULL);

    UUStopSocket(r, 0);

    return uip->result;
}
