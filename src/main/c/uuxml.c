#define __UUFILE__ "uuxml.c"

#include "common.h"
#include <openssl/conf.h>
#include "uuxml.h"
#include "uustring.h"

// xmlsec needs this?
#ifndef XMLSEC_NO_XSLT
#define XMLSEC_NO_XSLT
#endif

#include <xmlsec/xmlsec.h>
#include <xmlsec/crypto.h>
#include <xmlsec/errors.h>
#include <sys/stat.h>

#define NULLValue ""
#define PNS(x) ((char *)(x) ? (char *)(x) : (char *)NULLValue)

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#ifdef DEBUG_COMPILATION_MODE
#define AT __UUFILE__ ":" TOSTRING(__LINE__) " "
#else
#define AT "XML "
#endif

/**
 *    Report the XPath return type.
 *
 * @cur:  the object to inspect
 */
#define UsrXMLXPathTypeToStringSIZE 128
char *UUxmlXPathTypeToString(xmlXPathObjectType type) {
    char *result;

    if (!(result = malloc(UsrXMLXPathTypeToStringSIZE)))
        PANIC;

    _SNPRINTF_BEGIN(result, UsrXMLXPathTypeToStringSIZE);
    switch (type) {
    case XPATH_UNDEFINED:
        _snprintf(result, UsrXMLXPathTypeToStringSIZE, "an uninitialized value");
        break;
    case XPATH_NODESET:
        _snprintf(result, UsrXMLXPathTypeToStringSIZE, "a nodeset");
        break;
    case XPATH_XSLT_TREE:
        _snprintf(result, UsrXMLXPathTypeToStringSIZE, "an XSLT tree");
        break;
    case XPATH_BOOLEAN:
        _snprintf(result, UsrXMLXPathTypeToStringSIZE, "a boolean value");
        break;
    case XPATH_NUMBER:
        _snprintf(result, UsrXMLXPathTypeToStringSIZE, "a numeric value");
        break;
    case XPATH_STRING:
        _snprintf(result, UsrXMLXPathTypeToStringSIZE, "a string value");
        break;
    case XPATH_POINT:
        _snprintf(result, UsrXMLXPathTypeToStringSIZE, "an XPointer point");
        break;
    case XPATH_RANGE:
        _snprintf(result, UsrXMLXPathTypeToStringSIZE, "an XPointer range");
        break;
    case XPATH_LOCATIONSET:
        _snprintf(result, UsrXMLXPathTypeToStringSIZE, "an XPointer location set");
        break;
    case XPATH_USERS:
        _snprintf(result, UsrXMLXPathTypeToStringSIZE, "a user defined value");
        break;
    }
    _SNPRINTF_PANIC_OVERFLOW(result, UsrXMLXPathTypeToStringSIZE);
    _SNPRINTF_END(result, UsrXMLXPathTypeToStringSIZE);

    return result;
}

#define UsrXMLLogNodeInfoSIZE 1024
char *UUxmlNodeName(xmlNodePtr node) {
    char *tmpTxt = NULL;
    xmlNodePtr cur;
    xmlNsPtr ns;
    char *result;

    if (!(result = malloc(UsrXMLLogNodeInfoSIZE)))
        PANIC;

    _SNPRINTF_BEGIN(result, UsrXMLLogNodeInfoSIZE);
    if (node == NULL) {
        _snprintf(result, UsrXMLLogNodeInfoSIZE, "= (null)");
    } else {
        if (node->type == XML_NAMESPACE_DECL) {
            ns = (xmlNsPtr)node;
            cur = (xmlNodePtr)ns->next;
            if (cur->ns) {
                _snprintf(result, UsrXMLLogNodeInfoSIZE, "= namespace \"%s\"=\"%s\" for node %s:%s",
                          ns->prefix, ns->href, cur->ns->href, cur->name);
            } else {
                _snprintf(result, UsrXMLLogNodeInfoSIZE, "= namespace \"%s\"=\"%s\" for node %s",
                          ns->prefix, ns->href, cur->name);
            }
        } else if (node->type == XML_ELEMENT_NODE) {
            cur = node;
            if (cur->ns) {
                _snprintf(result, UsrXMLLogNodeInfoSIZE, "= element node \"%s:%s\"", cur->ns->href,
                          cur->name);
            } else {
                _snprintf(result, UsrXMLLogNodeInfoSIZE, "= element node \"%s\"", cur->name);
            }
        } else {
            cur = node;
            _snprintf(result, UsrXMLLogNodeInfoSIZE, "= node \"%s\" is %s", cur->name,
                      tmpTxt = UUxmlXPathTypeToString(cur->type));
            FreeThenNull(tmpTxt);
        }
    }
    _SNPRINTF_PANIC_OVERFLOW(result, UsrXMLLogNodeInfoSIZE);
    _SNPRINTF_END(result, UsrXMLLogNodeInfoSIZE);

    return result;
}

xmlNodePtr UUxmlFindChild(xmlNodePtr parent, char *childName) {
    xmlNodePtr child;
    xmlNodePtr grandChild;

    for (child = parent->children; child; child = child->next) {
        if (child->type != XML_ELEMENT_NODE)
            continue;
        if (child->name != NULL && strcmp(childName, (char *)child->name) == 0)
            return child;
        if (child->children) {
            if (grandChild = UUxmlFindChild(child, childName))
                return grandChild;
        }
    }
    return NULL;
}

xmlChar *UUxmlSimpleGetContent(xmlDocPtr doc, xmlXPathContextPtr xpathCtx, char *tag) {
    xmlXPathObjectPtr xpathObj = NULL;
    xmlXPathCompExprPtr compExprPtr = NULL;
    xmlChar *value = NULL;
    xmlNodeSetPtr nodes = NULL;
    BOOL localXpathCtx = 0;

    if (xpathCtx == NULL) {
        if (doc == NULL)
            goto Finished;
        localXpathCtx = 1;
        xpathCtx = xmlXPathNewContext(doc);
        if (xpathCtx == NULL) {
            Log(AT "can't create an XPath context");
            goto Finished;
        }
    }

    compExprPtr = xmlXPathCtxtCompile(xpathCtx, BAD_CAST tag);
    if (compExprPtr == NULL) {
        Log(AT "xpath %s is not syntactically correct", tag);
        goto Finished;
    }
    xpathObj = xmlXPathCompiledEval(compExprPtr, xpathCtx);
    if (xpathObj == NULL || (nodes = xpathObj->nodesetval) == NULL || (nodes->nodeNr == 0)) {
        if (debugLevel >= 8000) {
            Log(AT "xpath %s has no value", tag);
        }
        goto Finished;
    }
    value = xmlNodeGetContent(nodes->nodeTab[0]);
    if (debugLevel >= 8000) {
        Log(AT "xpath %s (first value only) has value \"%s\"", tag, PNS(value));
    }

Finished:
    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    if (compExprPtr) {
        xmlXPathFreeCompExpr(compExprPtr);
        compExprPtr = NULL;
    }

    if (localXpathCtx) {
        xmlXPathFreeContext(xpathCtx);
        xpathCtx = NULL;
    }

    return value;
}

#define UUxmlSimpleGetContentSnprintfVarSize (128)
xmlChar *UUxmlSimpleGetContentSnprintf(xmlDocPtr doc,
                                       xmlXPathContextPtr xpathCtx,
                                       char *buffer,
                                       int bufferSize,
                                       char *format,
                                       ...) {
    va_list ap;
    xmlXPathObjectPtr xpathObj = NULL;
    xmlXPathCompExprPtr compExprPtr = NULL;
    xmlChar *value = NULL;
    char *errStr = NULL;
    xmlNodeSetPtr nodes = NULL;
    BOOL localXpathCtx = 0;

    if (xpathCtx == NULL) {
        if (doc == NULL) {
            if (debugLevel > 0) {
                Log(AT "XPath with no document.");
            }
            goto Finished;
        }
        localXpathCtx = 1;
        xpathCtx = xmlXPathNewContext(doc);
        if (xpathCtx == NULL) {
            Log(AT "XPath can't create a context.");
            goto Finished;
        }
    }

    _SNPRINTF_BEGIN(buffer, bufferSize);
    va_start(ap, format);
    _vsnprintf(buffer, bufferSize, format, ap);
    va_end(ap);
    if (_SNPRINTF_IS_OVERFLOW(buffer, bufferSize)) {
        Log(AT
            "XPath %s has been truncated because it is larger than the supplied buffer which is %d "
            "long.",
            buffer, bufferSize);
    }
    _SNPRINTF_END(buffer, bufferSize);

    compExprPtr = xmlXPathCtxtCompile(xpathCtx, BAD_CAST buffer);
    if (compExprPtr != NULL) {
        xpathObj = xmlXPathCompiledEval(compExprPtr, xpathCtx);
    }

    if (compExprPtr == NULL) {
        Log(AT "XPath %s is not syntactically correct.", buffer);
    } else if (xpathObj == NULL) {
        if (debugLevel > 0) {
            Log(AT "XPath %s selects no nodes.", buffer);
        }
    } else if (xpathObj->type == XPATH_NODESET) {
        if (((nodes = xpathObj->nodesetval) == NULL) || ((nodes->nodeNr == 0))) {
            if (debugLevel > 0) {
                Log(AT "XPath %s selects no nodes.", buffer);
            }
        } else {
            value = xmlNodeGetContent(nodes->nodeTab[0]);
            if (debugLevel > 0) {
                Log(AT "XPath %s has %d nodes.", buffer, nodes->nodeNr);
            }
        }
    } else {
        if (!(value = (xmlChar *)malloc(UUxmlSimpleGetContentSnprintfVarSize)))
            PANIC;
        _SNPRINTF_BEGIN(value, UUxmlSimpleGetContentSnprintfVarSize);
        if (xpathObj->type == XPATH_BOOLEAN) {
            _snprintf((char *)value, UUxmlSimpleGetContentSnprintfVarSize, "%d",
                      (xpathObj->boolval == 0) ? 0 : 1);
        } else if (xpathObj->type == XPATH_NUMBER) {
            _snprintf((char *)value, UUxmlSimpleGetContentSnprintfVarSize, "%f",
                      xpathObj->floatval);
        } else if (xpathObj->type == XPATH_STRING) {
            _snprintf((char *)value, UUxmlSimpleGetContentSnprintfVarSize, "%s",
                      xpathObj->stringval);
        } else {
            Log(AT
                "XPath expression %s resulted in data type %s that is not useful in this context.",
                buffer, errStr = UUxmlXPathTypeToString(xpathObj->type));
            FreeThenNull(errStr);
            FreeThenNull(value);
        }
        _SNPRINTF_PANIC_OVERFLOW(value, UUxmlSimpleGetContentSnprintfVarSize);
        _SNPRINTF_END(value, UUxmlSimpleGetContentSnprintfVarSize);
    }

    if (debugLevel > 0) {
        Log(AT "XPath %s (first value only) has value \"%s\".", buffer, PNS(value));
    }

Finished:
    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    if (compExprPtr) {
        xmlXPathFreeCompExpr(compExprPtr);
        compExprPtr = NULL;
    }

    if (localXpathCtx && xpathCtx) {
        xmlXPathFreeContext(xpathCtx);
        xpathCtx = NULL;
    }

    return value;
}

xmlChar *UUxmlSimpleGetAttribute(xmlDocPtr doc,
                                 xmlXPathContextPtr xpathCtx,
                                 char *tag,
                                 char *attribute) {
    xmlXPathObjectPtr xpathObj = NULL;
    xmlXPathCompExprPtr compExprPtr = NULL;
    xmlChar *value = NULL;
    xmlNodeSetPtr nodes = NULL;
    BOOL localXpathCtx = 0;

    if (xpathCtx == NULL) {
        if (doc == NULL)
            goto Finished;
        localXpathCtx = 1;
        xpathCtx = xmlXPathNewContext(doc);
        if (xpathCtx == NULL) {
            Log(AT "can't create an XPath context");
            goto Finished;
        }
    }

    compExprPtr = xmlXPathCtxtCompile(xpathCtx, BAD_CAST tag);
    if (compExprPtr == NULL) {
        Log(AT "xpath %s is not syntactically correct", tag);
        goto Finished;
    }
    xpathObj = xmlXPathCompiledEval(compExprPtr, xpathCtx);
    if (xpathObj == NULL || (nodes = xpathObj->nodesetval) == NULL || (nodes->nodeNr == 0)) {
        if (debugLevel >= 8000) {
            Log(AT "xpath %s has no value", tag);
        }
        goto Finished;
    }
    value = xmlGetProp(nodes->nodeTab[0], BAD_CAST attribute);
    if (debugLevel >= 8000) {
        Log(AT "xpath %s for attribute %s (first value only) has value \"%s\"", tag, attribute,
            PNS(value));
    }

Finished:
    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    if (compExprPtr) {
        xmlXPathFreeCompExpr(compExprPtr);
        compExprPtr = NULL;
    }

    if (localXpathCtx) {
        xmlXPathFreeContext(xpathCtx);
        xpathCtx = NULL;
    }

    return value;
}

xmlChar *UUxmlSimpleGetAttributeSnprintf(xmlDocPtr doc,
                                         xmlXPathContextPtr xpathCtx,
                                         char *attribute,
                                         char *buffer,
                                         int bufferSize,
                                         char *format,
                                         ...) {
    va_list ap;
    xmlXPathObjectPtr xpathObj = NULL;
    xmlXPathCompExprPtr compExprPtr = NULL;
    xmlChar *value = NULL;
    xmlNodeSetPtr nodes = NULL;
    BOOL localXpathCtx = 0;

    if (xpathCtx == NULL) {
        if (doc == NULL)
            goto Finished;
        localXpathCtx = 1;
        xpathCtx = xmlXPathNewContext(doc);
        if (xpathCtx == NULL) {
            Log(AT "can't create an XPath context");
            goto Finished;
        }
    }

    buffer[bufferSize - 1] = 1;
    va_start(ap, format);
    _vsnprintf(buffer, bufferSize, format, ap);
    va_end(ap);
    if (buffer[bufferSize - 1] != 1) {
        Log(AT "warning: XPath may have been truncated");
    }

    compExprPtr = xmlXPathCtxtCompile(xpathCtx, BAD_CAST buffer);
    if (compExprPtr == NULL) {
        Log(AT "xpath %s is not syntactically correct", buffer);
        goto Finished;
    }
    xpathObj = xmlXPathCompiledEval(compExprPtr, xpathCtx);
    if (xpathObj == NULL || (nodes = xpathObj->nodesetval) == NULL || (nodes->nodeNr == 0)) {
        if (debugLevel >= 8000) {
            Log(AT "xpath %s has no value", buffer);
        }
        goto Finished;
    }
    value = xmlGetProp(nodes->nodeTab[0], BAD_CAST attribute);
    if (debugLevel >= 8000) {
        Log(AT "xpath %s for attribute %s (first value only) has value \"%s\"", buffer, attribute,
            PNS(value));
    }

Finished:
    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    if (compExprPtr) {
        xmlXPathFreeCompExpr(compExprPtr);
        compExprPtr = NULL;
    }

    if (localXpathCtx) {
        xmlXPathFreeContext(xpathCtx);
        xpathCtx = NULL;
    }

    return value;
}

void UUxmlFreeParserCtxtAndDoc(xmlParserCtxtPtr *ctxt) {
    xmlDocPtr doc;

    if (*ctxt) {
        doc = (*ctxt)->myDoc;
        xmlFreeParserCtxt(*ctxt);
        *ctxt = NULL;
        if (doc) {
            xmlFreeDoc(doc);
            doc = NULL;
        }
    }
}

static void UUxmlSecErrorsCallback(const char *file,
                                   int line,
                                   const char *func,
                                   const char *errorObject,
                                   const char *errorSubject,
                                   int reason,
                                   const char *msg) {
    const char *error_msg = NULL;
    xmlSecSize i;
    BOOL ignore;

    if (debugLevel > 1) {
        for (i = 0; (i < XMLSEC_ERRORS_MAX_NUMBER) && (xmlSecErrorsGetMsg(i) != NULL); ++i) {
            if (xmlSecErrorsGetCode(i) == reason) {
                error_msg = xmlSecErrorsGetMsg(i);
                break;
            }
        }
        ignore = (strstr(msg, "err=18;msg=self signed certificate") != NULL) && (debugLevel < 7000);
        if (!ignore) {
            Log(AT "%s |file=%s:line=%d:obj=%s:subj=%s:reason=(%d,%s):%s|",
                (func != NULL) ? func : "unknown", (file != NULL) ? file : "unknown", line,
                (errorObject != NULL) ? errorObject : "unknown",
                (errorSubject != NULL) ? errorSubject : "unknown", reason,
                (error_msg != NULL) ? error_msg : "", (msg != NULL) ? msg : "");
        }
    }
}

void UUxmlSecInit(void) {
    static BOOL initialized = FALSE;

    if (initialized)
        return;

    UUAcquireMutex(&mActive);

    if (!initialized) {
        if (xmlSecInit() < 0) {
            Log(AT "xmlsec initialization failed");
            PANIC;
        }

        /* Check loaded library version */
        if (xmlSecCheckVersion() != 1) {
            Log(AT "Loaded xmlsec library version is not compatible");
            PANIC;
        }

        /* xmlSecCryptoAppInit will call OPENSSL_config(NULL) which makes
           OpenSSL try to initialize from openssl.cnf, which may not exist, and
           if it doesn't exist, the program will terminate.  By pre-emptively
           calling OPENSSL_no_config(), we indicate that the config file should
           not be used, overriding the xmlsec1 behavior
        */
        OPENSSL_no_config();
        /* Init crypto library */
        if (xmlSecCryptoAppInit(NULL) < 0) {
            Log(AT "xmlsec crypto app initialization failed");
            PANIC;
        }

        /* Init xmlsec-crypto library */
        if (xmlSecCryptoInit() < 0) {
            Log(AT "xmlsec-crypto initialization failed");
            PANIC;
        }

        /* Set up our on error logging */
        xmlSecErrorsSetCallback(UUxmlSecErrorsCallback);

        initialized = TRUE;
    }

    UUReleaseMutex(&mActive);
}

FILE *LogXMLDocGetFileName(char **rfn) {
    static int nextLogFileIdx = 1;
    int tries = 0;
    int rc;
    char *fn;
    FILE *f = NULL;
    struct stat statBuf;

    if (!(fn = malloc(MAX_PATH + 1)))
        PANIC;

    UUAcquireMutex(&mActive);
    for (;;) {
        sprintf(fn, "xl%04d.xml", nextLogFileIdx++);
        rc = stat(fn, &statBuf);
        if (rc == 0)
            continue; /* file exists */
        if ((errno == ENOENT) && (f = fopen(fn, "w")))
            break; /* file created */
        f = NULL;
        tries++;
        if (tries < 11) {
            Log(AT "unable to create %s, error %d, retrying", fn, errno);
        } else {
            Log(AT "unable to create %s, error %d, after %d tries, quitting", fn, errno, tries);
            break;
        }
    }
    UUReleaseMutex(&mActive);

    if (rfn) {
        *rfn = fn;
    } else {
        FreeThenNull(fn);
    }

    return f;
}

void LogXMLDoc(xmlDocPtr doc, xmlChar *textDoc) {
    char *fn;
    FILE *f = NULL;

    if ((doc == NULL) && (textDoc == NULL)) {
        return;
    }

    f = LogXMLDocGetFileName(&fn);

    if (f) {
        Log(AT "document logged to file '%s'", fn);
        if (doc != NULL) {
            xmlDocFormatDump(f, doc, xmlIndentTreeOutput);
        } else {
            fwrite((char *)textDoc, 1, strlen((char *)textDoc), f);
        }
        fclose(f);
        f = NULL;
    }

    if (debugLevel >= 8000) {
        LogXMLDocLogOnly(doc, textDoc);
    }

    FreeThenNull(fn);
}

void LogXMLDocLogOnly(xmlDocPtr doc, xmlChar *textDoc) {
    int len;
    xmlChar *buffer = NULL;
    char *str;
    char *line;

    if (doc != NULL) {
        xmlDocDumpFormatMemory(doc, &buffer, &len, xmlIndentTreeOutput);
        if (buffer != NULL) {
            for (str = NULL, line = StrTok_R((char *)buffer, "\r\n", &str); line;
                 line = StrTok_R(NULL, "\r\n", &str)) {
                if (*line) {
                    Log("%s", line);
                }
            }
            xmlFreeThenNull(buffer);
        }
    } else if (textDoc != NULL) {
        for (str = NULL, line = StrTok_R((char *)textDoc, "\r\n", &str); line;
             line = StrTok_R(NULL, "\r\n", &str)) {
            if (*line) {
                Log("%s", line);
            }
        }
    }
}
void WriteXMLDocAsHTML(struct UUSOCKET *s, xmlDocPtr doc) {
    int len;
    xmlChar *buffer = NULL;
    char *str;
    char *line;

    if (doc && s) {
        xmlDocDumpFormatMemory(doc, &buffer, &len, xmlIndentTreeOutput);
        if (buffer != NULL) {
            for (str = NULL, line = StrTok_R((char *)buffer, "\r\n", &str); line;
                 line = StrTok_R(NULL, "\r\n", &str)) {
                if (*line) {
                    SendHTMLEncoded(s, line);
                    UUSendZ(s, "\n");
                }
            }
            xmlFreeThenNull(buffer);
        }
    }
}
