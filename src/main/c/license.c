/**
 * Old-style used crc, custhi, custmi, date, custlo, crc where date was 0 for permanent or number of
 *  weeks since epoch 8/4/1999
 *
 *  New-style: remains same if date == 0
 *             implies cluster-license if date == 1
 *             implies new-style encoding if date == 2
 *             anything else in date considered to be old style
 *
 * When date == 2, true date stored in custmi and custlo, with custhi for salt
 */

/* #define ENCODING */

#include "uustring.h"
#include "wskeylicense.h"

#define __UUFILE__ "license.c"

/* For version, 0 always means newest */
unsigned char *Obscurity(int version) {
    if (version == 1)
        return (unsigned char *)"\002\003\005\007\011\013\017\023\031\037";
    if (version == 2)
        return (unsigned char *) "\003\005\007\011\013\017\023\031\037\041\043\047\053\061\067\071\073\101\103\107\113\127\131\137";
    return (unsigned char *) "\xf8\x13\x05\xd2\x15\x05\xa0\x17\x25\x57\x92\xd1\x3e\x11\x5f\x2c\xed\x33\x4e\x1c\xdd\xee\x7f\x6b\x51\x0d\x1a\x8c\xc4\x04\x1c\x77";
}

int ValidateTag(int c) {
    int icc[] = {7234994, /* clemson.edu */
                 100201,  /* University of Iran inadvertent sale */
                 0},
        *picc;

    for (picc = icc; *picc; picc++) {
        if (*picc == c)
            return 1;
    }
    return 0;
}

// #define ENCODING
#ifdef ENCODING

int someKey;
unsigned int exportCustomer;

#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#include <inttypes.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#define stricmp strcasecmp
#define strnicmp strncasecmp
typedef unsigned char U48[6];
#endif
#ifndef PRIdMAX
#define PRIdMAX "ld"
#endif
#ifndef PRIuMAX
#define PRIuMAX "lu"
#endif
#ifndef PRIxMAX
#define PRIxMAX "lx"
#endif
#else
#include "common.h"
#include "usr.h"
#include "usrfinduser.h"
#endif

#ifdef ENCODING

#define UUtime time
struct tm *UUlocaltime_r(const time_t *clock, struct tm *result) {
    struct tm *tm;

    if (tm = localtime(clock))
        memcpy(result, tm, sizeof(*result));
    return (tm ? result : NULL);
}
#endif

void div48(U48 d1, unsigned char d2, U48 q, unsigned char *r) {
    unsigned short t;
    int i;

    t = 0;
    for (i = 0; i < sizeof(U48); i++) {
        t = (t << 8) + *d1++;
        *q++ = t / d2;
        t = t % d2;
    }
    *r = t & 0xff;
}

void mul48(U48 m1, unsigned char m2, unsigned char c, U48 p) {
    unsigned short t;
    int i;

    t = c;
    m1 += sizeof(U48) - 1;
    p += sizeof(U48) - 1;
    for (i = 0; i < sizeof(U48); i++) {
        t = t + *m1-- * m2;
        *p-- = t & 0xff;
        t = t >> 8;
    }
}

unsigned short crc16(unsigned short crc, unsigned char data) {
    int i;
    char carry;

    for (i = 0; i < 8; i++) {
        carry = (crc & 0x8000) != 0;
        crc <<= 1;
        if ((data & 0x80) != 0)
            crc |= 1;
        if (carry)
            crc ^= 0x1021;
        data = (data << 1) & 0xff;
    }
    return crc;
}

#ifdef ENCODING
unsigned char btchar(unsigned char b) {
    if (b < 14)
        return b + 'm';
    if (b < 22)
        return b - 14 + '2';
    if (b < 24)
        return b - 22 + 'j';
    if (b < 33)
        return b - 24 + 'a';
    return 255;
}
#endif

unsigned char tbc(unsigned char t) {
    if (isupper(t))
        t = tolower(t);
    if (t >= 'm' && t <= 'z')
        return t - 'm';
    if (t >= '2' && t <= '9')
        return t - '2' + 14;
    if (t >= 'j' && t <= 'k')
        return t - 'j' + 22;
    if (t >= 'a' && t <= 'i')
        return t - 'a' + 24;
    return 255;
}

#ifdef ENCODING
unsigned char *encode(unsigned int customer, unsigned int date) {
    unsigned char custhi, custmi, custlo;
    unsigned short crc;
    U48 input;
    static unsigned char output[11];
    unsigned char r;
    unsigned char obyte;

    static int key[6];

    memset(key, 0, sizeof(key));

    if (date == 0 || date == 1) {
        custhi = (customer >> 16) & 0xff;
        custmi = (customer >> 8) & 0xff;
        custlo = customer & 0xff;
    } else {
        custhi = (customer >> 16) & 0xff;
        custmi = (date >> 8) & 0xff;
        custlo = date & 0xff;
        date = 2;
    }

    crc = crc16(0, custhi);
    crc = crc16(crc, custmi);
    crc = crc16(crc, (unsigned char)(date & 0xff));
    crc = crc16(crc, custlo);
    crc = crc16(crc, 0);
    crc = crc16(crc, 0);

    input[0] = crc >> 8;
    input[1] = custhi;
    input[2] = custmi;
    input[3] = date;
    input[4] = custlo;
    input[5] = crc & 0xff;

    for (obyte = 0; obyte < 10; obyte++) {
        div48(input, 31, input, &r);
        output[obyte] = btchar(r);
    }
    output[obyte] = 0;
    return output;

    /* original encode code */
#if 0
    unsigned char custhi, custmi, custlo;
    unsigned short crc;
    U48 input;
    static unsigned char output[11];
    unsigned char r;
    unsigned char obyte;

    static key[6];

    memset(key, 0, sizeof(key));

    custhi = (customer >> 16) & 0xff;
    custmi = (customer >> 8) & 0xff;
    custlo = customer & 0xff;

    crc = crc16(0,   custhi);
    crc = crc16(crc, custmi);
    crc = crc16(crc, date);
    crc = crc16(crc, custlo);
    crc = crc16(crc, 0);
    crc = crc16(crc, 0);

    input[0] = crc >> 8;
    input[1] = custhi;
    input[2] = custmi;
    input[3] = date;
    input[4] = custlo;
    input[5] = crc & 0xff;

    for (obyte = 0; obyte < 10; obyte++) {
        div48(input, 31, input, &r);
        output[obyte] = btchar(r);
    }
    output[obyte] = 0;
    return output;
#endif
}
#endif
