#ifndef __ADMINEBRARY_H__
#define __ADMINEBRARY_H__

#include "common.h"

void AdminEbrary(struct TRANSLATE *t, char *query, char *post);
struct EBRARYSITE *AdminEbrarySite(char *site, size_t siteLen);

#endif  // __ADMINEBRARY_H__
