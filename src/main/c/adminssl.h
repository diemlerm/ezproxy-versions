#ifndef __ADMINSSL_H__
#define __ADMINSSL_H__

#include "common.h"

void AdminSsl(struct TRANSLATE *t, char *query, char *post);
void AdminSslImport(struct TRANSLATE *t, char *query, char *post);
void AdminSslNew(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINSSL_H__
