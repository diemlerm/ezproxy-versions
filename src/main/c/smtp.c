#define __UUFILE__ "smtp.c"

#include "common.h"

#ifndef WIN32
#include <dirent.h>
#endif

int SendMail(char *fn) {
    struct UUSOCKET rr, *r = &rr;
    int result;
    int state;
    char line[100];
    char multi[4];
    time_t now, gmnow;
    int diff;
    int ohour, omin;
    struct tm rtm;
    char dt[64];

    UUInitSocket(r, INVALID_SOCKET);

    result = 3;

    multi[0] = 0;

    if (mailServer == NULL) {
        Log("MailServer undefined");
        return 1;
    }

    if (mailFrom == NULL) {
        if (!(mailFrom = malloc(strlen(myName) + 16)))
            PANIC;
        sprintf(mailFrom, "ezproxy@%s", myName);
    }

    if (mailTo == NULL) {
        Log("MailTo undefined");
        return 2;
    }

    if (UUConnectWithSource(r, mailServer, 25, "SendMail", NULL))
        goto Finished;

    if (logMail)
        Log("SMTP connect to %s", mailServer);

    result = 1;
    for (state = 0;;) {
        if (ReadLine(r, line, sizeof(line)) == 0) {
            result = 3;
            break;
        }
        if (logMail)
            Log("Received %s", line);

        /* If we are receiving a multi-line response, skip all but final line */
        if (strlen(line) >= 4) {
            /* Multi-line responses end when the original code is received, followed by a space */
            if (multi[0]) {
                if (strncmp(line, multi, 3) == 0 && line[3] == ' ')
                    multi[0] = 0;
                /* Multi-line responses start when there is a - after the 3 digit code */
            } else if (line[3] == '-') {
                StrCpy3(multi, line, 4);
            }
        }
        if (multi[0])
            continue;

        if (state == 0) {
            if (strncmp(line, "220", 3) != 0)
                break;
            UUSendZ(r, "HELO ");
            UUSendZCRLF(r, myName);
            if (logMail)
                Log("SMTP send HELO %s", myName);
        } else if (state == 1) {
            if (strncmp(line, "250", 3) != 0)
                break;
            UUSendZ(r, "MAIL FROM:<");
            UUSend2(r, mailFrom, strlen(mailFrom), ">\r\n", 3, 0);
            if (logMail)
                Log("SMTP send MAIL FROM:<%s>", mailFrom);
        } else if (state == 2) {
            if (strncmp(line, "250", 3) != 0)
                break;
            UUSendZ(r, "RCPT TO:<");
            UUSend2(r, mailTo, strlen(mailTo), ">\r\n", 3, 0);
            if (logMail)
                Log("SMTP send RCPT TO:<%s>", mailTo);
        } else if (state == 3) {
            if (strncmp(line, "250", 3) != 0)
                break;
            UUSendZ(r, "DATA\r\n");
            UUSendZ(r, "From: ");
            UUSendZCRLF(r, mailFrom);
            UUSendZ(r, "To: ");
            UUSendZCRLF(r, mailTo);
            UUtime(&now);
            strftime(dt, sizeof(dt) - 1, "%a, %d %b %Y %H:%M:%S", UUlocaltime_r(&now, &rtm));
            gmnow = mktime(UUgmtime_r(&now, &rtm));
            diff = (now - gmnow) / 60;
            ohour = diff / 60;
            omin = diff % 60;
            sprintf(strchr(dt, 0), " %+03d%02d", ohour, omin);
            WriteLine(r, "Date: %s\r\n", dt);
            UUSendZ(r, "Subject: Something or another\r\n\r\n");
            UUSendZ(r, "Howdy\r\n.\r\n");
        } else if (state == 4) {
            break;
        }
        state++;
    }
    UUSendZ(r, "QUIT\r\n");
    UUFlush(r);
    UUStopSocket(r, 0);

Finished:
    return result;
}

void RunMailQueue(void) {
    char *fn;
    char fullName[MAX_PATH];
#ifdef WIN32
    WIN32_FIND_DATA fileData;
    HANDLE hSearch;
#else
    DIR *dir;
    struct dirent *dirent;
#endif

    UUmkdir(MAILDIR);
#ifdef WIN32
    hSearch = FindFirstFile(MAILDIR "*.*", &fileData);
    if (hSearch == INVALID_HANDLE_VALUE)
        return;
    do {
        fn = fileData.cFileName;
#else
    dir = opendir(MAILDIR);
    if (dir == NULL) {
        return;
    }

    while (dirent = readdir(dir)) {
        fn = dirent->d_name;
#endif
        if (strcmp(fn, ".") == 0 || strcmp(fn, "..") == 0)
            continue;
        sprintf(fullName, "%s%s", MAILDIR, fn);
        if (!CheckIfFile(fullName))
            continue;
        if (SendMail(fullName)) {
            unlink(fullName);
        }
#ifdef WIN32
    } while (FindNextFile(hSearch, &fileData));
    FindClose(hSearch);
#else
    }
    closedir(dir);
#endif
}
