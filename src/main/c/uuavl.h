#ifndef __UUAVL_H__
#define __UUAVL_H__

/*
 * Copyright 1998-2003 The OpenLDAP Foundation, Redwood City, California, USA
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted only as authorized by the OpenLDAP
 * Public License.  A copy of this license is available at
 * http://www.OpenLDAP.org/license.html or in file LICENSE in the
 * top-level directory of the distribution.
 */
/* Portions
 * Copyright (c) 1993 Regents of the University of Michigan.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that this notice is preserved and that due credit is given
 * to the University of Michigan at Ann Arbor. The name of the University
 * may not be used to endorse or promote products derived from this
 * software without specific prior written permission. This software
 * is provided ``as is'' without express or implied warranty.
 */
/* uuavl.h - uuavl tree definitions */

#ifndef _UUAVL
#define _UUAVL

#include "common.h"

#define LDAP_NEEDS_PROTOTYPES

#include <ldap_cdefs.h>

/*
 * this structure represents a generic uuavl tree node.
 */

LDAP_BEGIN_DECL

#ifdef UUAVL_INTERNAL

struct uuavlnode {
    void *uuavl_data;
    signed int uuavl_bf;
    struct uuavlnode *uuavl_left;
    struct uuavlnode *uuavl_right;
    struct uuavlnode *uuavl_prev;
    struct uuavlnode *uuavl_next;
};

/* balance factor values */
#define LH (-1)
#define EH 0
#define RH 1

/* uuavl routines */
#define uuavl_getone(x) ((x) == 0 ? 0 : (x)->uuavl_data)
#define uuavl_onenode(x) ((x) == 0 || ((x)->uuavl_left == 0 && (x)->uuavl_right == 0))

#endif /* UUAVL_INTERNALS */

typedef struct uuavlnode *UUAVLTREEITERCTX;

typedef int(*UUAVL_APPLY) LDAP_P((void *, void *));
typedef int(*UUAVL_CMP) LDAP_P((const void *, const void *));
typedef int(*UUAVL_DUP) LDAP_P((char *name, void *, void *));
typedef void(*UUAVL_FREE) LDAP_P((void *));

#define LDAP_UUAVL_F(type) extern type
#define LDAP_UUAVL_V(type) extern type

struct UUAVLTREE {
    struct uuavlnode *root;
    struct uuavlnode *first;
    char *name;
    UUAVL_FREE avlFree;
    UUAVL_CMP avlCmp;
    UUAVL_DUP avlDup;
};

#ifdef UUAVL_INTERNAL

LDAP_UUAVL_F(int)
uuavl_free LDAP_P((struct uuavlnode * root, UUAVL_FREE dfree));

LDAP_UUAVL_F(int)
uuavl_insert LDAP_P(
    (struct uuavlnode **, char *name, struct uuavlnode *closest, void *, UUAVL_CMP, UUAVL_DUP));

LDAP_UUAVL_F(void *)
uuavl_delete LDAP_P((struct uuavlnode **, void *, UUAVL_CMP));

LDAP_UUAVL_F(struct uuavlnode *)
uuavl_find LDAP_P((struct UUAVLTREE *, const void *));

LDAP_UUAVL_F(void *)
uuavl_find_lin LDAP_P((struct uuavlnode *, const void *, UUAVL_CMP));

#ifdef UUAVL_NONREENTRANT
LDAP_UUAVL_F(void *)
uuavl_getfirst LDAP_P((struct uuavlnode *));

LDAP_UUAVL_F(void *)
uuavl_getnext LDAP_P((void));
#endif

LDAP_UUAVL_F(int)
uuavl_dup_error LDAP_P((void *, void *));

LDAP_UUAVL_F(int)
uuavl_dup_ok LDAP_P((void *, void *));

LDAP_UUAVL_F(int)
uuavl_apply LDAP_P((struct uuavlnode *, UUAVL_APPLY, void *, int, int));

LDAP_UUAVL_F(int)
uuavl_prefixapply LDAP_P((struct uuavlnode *, void *, UUAVL_CMP, void *, UUAVL_CMP, void *, int));

#endif /* UUAVL_INTERNAL */

LDAP_UUAVL_F(int)
UUAvlTreeFree LDAP_P((struct UUAVLTREE * tree));

LDAP_UUAVL_F(int)
UUAvlTreeInsert LDAP_P((struct UUAVLTREE * tree, void *));

LDAP_UUAVL_F(void *)
UUAvlTreeDelete LDAP_P((struct UUAVLTREE * tree, void *));

LDAP_UUAVL_F(void *)
UUAvlTreeFind LDAP_P((struct UUAVLTREE * tree, const void *));

LDAP_UUAVL_F(void *)
UUAvlTreeFindGE LDAP_P((struct UUAVLTREE * tree, const void *));

void UUAvlTreeInit(struct UUAVLTREE *tree,
                   char *name,
                   UUAVL_CMP avlCmp,
                   UUAVL_DUP avlDup,
                   UUAVL_FREE avlFree);

void *UUAvlTreeIterFirst(struct UUAVLTREE *tree, UUAVLTREEITERCTX *uatic);
void *UUAvlTreeIterNext(struct UUAVLTREE *tree, UUAVLTREEITERCTX *uatic);
void *UUAvlTreeIterLast(struct UUAVLTREE *tree, UUAVLTREEITERCTX *uatic);
void *UUAvlTreeIterPrev(struct UUAVLTREE *tree, UUAVLTREEITERCTX *uatic);
void *UUAvlTreeIterFind(struct UUAVLTREE *tree, UUAVLTREEITERCTX *uatic, void *data);
void *UUAvlTreeIterFindGE(struct UUAVLTREE *tree, UUAVLTREEITERCTX *uatic, const void *data);

/* apply traversal types */
#define UUAVL_PREORDER 1
#define UUAVL_INORDER 2
#define UUAVL_POSTORDER 3
/* what apply returns if it ran out of nodes */
#define UUAVL_NOMORE (-6)

LDAP_END_DECL

#endif /* _UUAVL */

#endif  // __UUAVL_H__
