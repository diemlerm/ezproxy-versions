/**
 * posix_report.c
 *
 * Discover the POSIXyness the program environment.
 *
 *  Created on: May 25, 2010
 *      Author: Steven Eastman
 */

#ifdef WIN32
#include <windows.h>
#define intmax_t int
#define INTMAX_C(x) x
#else
#include <unistd.h>
#include <inttypes.h>
#endif
#include <stdio.h>
#include <errno.h>
#include <string.h>

#define buffer1_MAX 4096
#define buffer2_MAX 4096

#define QUOTEME(x) #x
#define QUOTEME_MY_VALUE(x) QUOTEME(x)

#define notrequired(v)                   \
    do {                                 \
        printf("%-38s undefined\n", #v); \
        errno = 0;                       \
    } while (0)

#define required(v)                      \
    do {                                 \
        printf("%-38s undefined\n", #v); \
        errno = 0;                       \
    } while (0)

#define proconf(v)                                                                \
    do {                                                                          \
        printf("%-38s %-45s\n", #v,                                               \
               interpoconf(QUOTEME_MY_VALUE(v), (long int)(v + 1 - 1), buffer2)); \
    } while (0)

#define prsconf(v)                                                               \
    do {                                                                         \
        printf("%-38s %-45s\n", #v, interpsconf((long int)sysconf(v), buffer2)); \
        errno = 0;                                                               \
    } while (0)

#define prpconf(p, v)                                                         \
    do {                                                                      \
        printf("%-38s %-45s\n", #v, interppconf(pathconf(p, v), p, buffer2)); \
        errno = 0;                                                            \
    } while (0)

#define prconfs(v)                                                        \
    do {                                                                  \
        rc = confstr(v, buffer1, buffer1_MAX);                            \
        printf("%-38s %s '%s'\n", #v, interpconfs(rc, buffer2), buffer1); \
        errno = 0;                                                        \
    } while (0)

static char *interpsconf(long int n, char *buffer2) {
    if (n == -1 && errno == 0) {
        return "no limit defined, may be unsupported";
    }
    if (errno == 0) {
        sprintf(buffer2, "supported, value %ld", (long int)n);
        return buffer2;
    }
    sprintf(buffer2, "unsupported, %s", strerror(errno));
    return buffer2;
}

static char *interpoconf(char *v, long int n, char *buffer2) {
    if (n == -1) {
        return "not supported";
    }
    if (n > 0) {
        sprintf(buffer2, "supported, value %ld", (long int)n);
        return buffer2;
    } else if (*v == 0) {
        sprintf(buffer2, "supported");
        return buffer2;
    }
    return "may be supported, call sysconf()";
}

static char *interppconf(long int n, char *p, char *buffer2) {
    if (n == -1 && errno == 0) {
        sprintf(buffer2, "supported for '%s', no limit", p);
        return buffer2;
    }
    if (n >= 0 && errno == 0) {
        sprintf(buffer2, "value for '%s' is %ld", p, (long int)n);
        return buffer2;
    }
    sprintf(buffer2, "unsupported for '%s', %s", p, strerror(errno));
    return buffer2;
}

static char *interpconfs(long int n, char *buffer2) {
    if (n > 0 && errno == 0) {
        return "value defined";
    }
    if (n == 0 && errno == 0) {
        return "no defined value";
    }
    sprintf(buffer2, "unsupported, %s", strerror(errno));
    return buffer2;
}

int main(int argc, char **argv, char **env) {
    char buffer1[buffer1_MAX];
    char buffer2[buffer2_MAX];
    size_t rc;

    errno = 0;

    /////////////////////// CONFIGSTR //////////////////////////

#ifdef _CS_PATH
    prconfs(_CS_PATH);
#else
    required(_CS_PATH);
#endif
#ifdef _CS_POSIX_V6_ILP32_OFF32_CFLAGS
    prconfs(_CS_POSIX_V6_ILP32_OFF32_CFLAGS);
#else
    required(_CS_POSIX_V6_ILP32_OFF32_CFLAGS);
#endif
#ifdef _CS_POSIX_V6_ILP32_OFF32_LDFLAGS
    prconfs(_CS_POSIX_V6_ILP32_OFF32_LDFLAGS);
#else
    required(_CS_POSIX_V6_ILP32_OFF32_LDFLAGS);
#endif
#ifdef _CS_POSIX_V6_ILP32_OFF32_LIBS
    prconfs(_CS_POSIX_V6_ILP32_OFF32_LIBS);
#else
    required(_CS_POSIX_V6_ILP32_OFF32_LIBS);
#endif
#ifdef _CS_POSIX_V6_ILP32_OFFBIG_CFLAGS
    prconfs(_CS_POSIX_V6_ILP32_OFFBIG_CFLAGS);
#else
    required(_CS_POSIX_V6_ILP32_OFFBIG_CFLAGS);
#endif
#ifdef _CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS
    prconfs(_CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS);
#else
    required(_CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS);
#endif
#ifdef _CS_POSIX_V6_ILP32_OFFBIG_LIBS
    prconfs(_CS_POSIX_V6_ILP32_OFFBIG_LIBS);
#else
    required(_CS_POSIX_V6_ILP32_OFFBIG_LIBS);
#endif
#ifdef _CS_POSIX_V6_LP64_OFF64_CFLAGS
    prconfs(_CS_POSIX_V6_LP64_OFF64_CFLAGS);
#else
    required(_CS_POSIX_V6_LP64_OFF64_CFLAGS);
#endif
#ifdef _CS_POSIX_V6_LP64_OFF64_LDFLAGS
    prconfs(_CS_POSIX_V6_LP64_OFF64_LDFLAGS);
#else
    required(_CS_POSIX_V6_LP64_OFF64_LDFLAGS);
#endif
#ifdef _CS_POSIX_V6_LP64_OFF64_LIBS
    prconfs(_CS_POSIX_V6_LP64_OFF64_LIBS);
#else
    required(_CS_POSIX_V6_LP64_OFF64_LIBS);
#endif
#ifdef _CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS
    prconfs(_CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS);
#else
    required(_CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS);
#endif
#ifdef _CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS
    prconfs(_CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS);
#else
    required(_CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS);
#endif
#ifdef _CS_POSIX_V6_LPBIG_OFFBIG_LIBS
    prconfs(_CS_POSIX_V6_LPBIG_OFFBIG_LIBS);
#else
    required(_CS_POSIX_V6_LPBIG_OFFBIG_LIBS);
#endif
#ifdef _CS_POSIX_V6_WIDTH_RESTRICTED_ENVS
    prconfs(_CS_POSIX_V6_WIDTH_RESTRICTED_ENVS);
#else
    required(_CS_POSIX_V6_WIDTH_RESTRICTED_ENVS);
#endif

    /* LEGACY */
#ifdef _CS_XBS5_ILP32_OFF32_CFLAGS
    prconfs(_CS_XBS5_ILP32_OFF32_CFLAGS);
#else
    notrequired(_CS_XBS5_ILP32_OFF32_CFLAGS);
#endif
#ifdef _CS_XBS5_ILP32_OFF32_LDFLAGS
    prconfs(_CS_XBS5_ILP32_OFF32_LDFLAGS);
#else
    notrequired(_CS_XBS5_ILP32_OFF32_LDFLAGS);
#endif
#ifdef _CS_XBS5_ILP32_OFF32_LIBS
    prconfs(_CS_XBS5_ILP32_OFF32_LIBS);
#else
    notrequired(_CS_XBS5_ILP32_OFF32_LIBS);
#endif
#ifdef _CS_XBS5_ILP32_OFF32_LINTFLAGS
    prconfs(_CS_XBS5_ILP32_OFF32_LINTFLAGS);
#else
    notrequired(_CS_XBS5_ILP32_OFF32_LINTFLAGS);
#endif
#ifdef _CS_XBS5_ILP32_OFFBIG_CFLAGS
    prconfs(_CS_XBS5_ILP32_OFFBIG_CFLAGS);
#else
    notrequired(_CS_XBS5_ILP32_OFFBIG_CFLAGS);
#endif
#ifdef _CS_XBS5_ILP32_OFFBIG_LDFLAGS
    prconfs(_CS_XBS5_ILP32_OFFBIG_LDFLAGS);
#else
    notrequired(_CS_XBS5_ILP32_OFFBIG_LDFLAGS);
#endif
#ifdef _CS_XBS5_ILP32_OFFBIG_LIBS
    prconfs(_CS_XBS5_ILP32_OFFBIG_LIBS);
#else
    notrequired(_CS_XBS5_ILP32_OFFBIG_LIBS);
#endif
#ifdef _CS_XBS5_ILP32_OFFBIG_LINTFLAGS
    prconfs(_CS_XBS5_ILP32_OFFBIG_LINTFLAGS);
#else
    notrequired(_CS_XBS5_ILP32_OFFBIG_LINTFLAGS);
#endif
#ifdef _CS_XBS5_LP64_OFF64_CFLAGS
    prconfs(_CS_XBS5_LP64_OFF64_CFLAGS);
#else
    notrequired(_CS_XBS5_LP64_OFF64_CFLAGS);
#endif
#ifdef _CS_XBS5_LP64_OFF64_LDFLAGS
    prconfs(_CS_XBS5_LP64_OFF64_LDFLAGS);
#else
    notrequired(_CS_XBS5_LP64_OFF64_LDFLAGS);
#endif
#ifdef _CS_XBS5_LP64_OFF64_LIBS
    prconfs(_CS_XBS5_LP64_OFF64_LIBS);
#else
    notrequired(_CS_XBS5_LP64_OFF64_LIBS);
#endif
#ifdef _CS_XBS5_LP64_OFF64_LINTFLAGS
    prconfs(_CS_XBS5_LP64_OFF64_LINTFLAGS);
#else
    notrequired(_CS_XBS5_LP64_OFF64_LINTFLAGS);
#endif
#ifdef _CS_XBS5_LPBIG_OFFBIG_CFLAGS
    prconfs(_CS_XBS5_LPBIG_OFFBIG_CFLAGS);
#else
    notrequired(_CS_XBS5_LPBIG_OFFBIG_CFLAGS);
#endif
#ifdef _CS_XBS5_LPBIG_OFFBIG_LDFLAGS
    prconfs(_CS_XBS5_LPBIG_OFFBIG_LDFLAGS);
#else
    notrequired(_CS_XBS5_LPBIG_OFFBIG_LDFLAGS);
#endif
#ifdef _CS_XBS5_LPBIG_OFFBIG_LIBS
    prconfs(_CS_XBS5_LPBIG_OFFBIG_LIBS);
#else
    notrequired(_CS_XBS5_LPBIG_OFFBIG_LIBS);
#endif
#ifdef _CS_XBS5_LPBIG_OFFBIG_LINTFLAGS
    prconfs(_CS_XBS5_LPBIG_OFFBIG_LINTFLAGS);
#else
    notrequired(_CS_XBS5_LPBIG_OFFBIG_LINTFLAGS);
#endif

    /////////////////////// PATHCONF //////////////////////////

#ifdef _PC_2_SYMLINKS
    prpconf("/bin", _PC_2_SYMLINKS);
#else
    required(_PC_2_SYMLINKS);
#endif
#ifdef _PC_ALLOC_SIZE_MIN
    prpconf("/bin", _PC_ALLOC_SIZE_MIN);
#else
    required(_PC_ALLOC_SIZE_MIN);
#endif
#ifdef _PC_ASYNC_IO
    prpconf("/bin", _PC_ASYNC_IO);
#else
    required(_PC_ASYNC_IO);
#endif
#ifdef _PC_AUTH_OPAQUE_NP
    prpconf("/bin", _PC_AUTH_OPAQUE_NP);
#else
    notrequired(_PC_AUTH_OPAQUE_NP);
#endif
#ifdef _PC_CASE_PRESERVING
    prpconf("/bin", _PC_CASE_PRESERVING);
#else
    notrequired(_PC_CASE_PRESERVING);
#endif
#ifdef _PC_CASE_SENSITIVE
    prpconf("/bin", _PC_CASE_SENSITIVE);
#else
    notrequired(_PC_CASE_SENSITIVE);
#endif
#ifdef _PC_CHOWN_RESTRICTED
    prpconf("/bin", _PC_CHOWN_RESTRICTED);
#else
    required(_PC_CHOWN_RESTRICTED);
#endif
#ifdef _PC_EXTENDED_SECURITY_NP
    prpconf("/bin", _PC_EXTENDED_SECURITY_NP);
#else
    notrequired(_PC_EXTENDED_SECURITY_NP);
#endif
#ifdef _PC_FILESIZEBITS
    prpconf("/bin", _PC_FILESIZEBITS);
#else
    required(_PC_FILESIZEBITS);
#endif
#ifdef _PC_LINK_MAX
    prpconf("/bin", _PC_LINK_MAX);
#else
    required(_PC_LINK_MAX);
#endif
#ifdef _PC_MAX_CANON
    prpconf("/dev/tty", _PC_MAX_CANON);
#else
    required(_PC_MAX_CANON);
#endif
#ifdef _PC_MAX_INPUT
    prpconf("/dev/tty", _PC_MAX_INPUT);
#else
    required(_PC_MAX_INPUT);
#endif
#ifdef _PC_NAME_CHARS_MAX
    prpconf("/bin", _PC_NAME_CHARS_MAX);
#else
    notrequired(_PC_NAME_CHARS_MAX);
#endif
#ifdef _PC_NAME_MAX
    prpconf("/bin", _PC_NAME_MAX);
#else
    required(_PC_NAME_MAX);
#endif
#ifdef _PC_NO_TRUNC
    prpconf("/bin", _PC_NO_TRUNC);
#else
    required(_PC_NO_TRUNC);
#endif
#ifdef _PC_PATH_MAX
    prpconf("/bin", _PC_PATH_MAX);
#else
    required(_PC_PATH_MAX);
#endif
#ifdef _PC_PIPE_BUF
    prpconf("/bin", _PC_PIPE_BUF);
#else
    required(_PC_PIPE_BUF);
#endif
#ifdef _PC_PRIO_IO
    prpconf("/bin", _PC_PRIO_IO);
#else
    required(_PC_PRIO_IO);
#endif
#ifdef _PC_REC_INCR_XFER_SIZE
    prpconf("/bin", _PC_REC_INCR_XFER_SIZE);
#else
    required(_PC_REC_INCR_XFER_SIZE);
#endif
#ifdef _PC_REC_MAX_XFER_SIZE
    prpconf("/bin", _PC_REC_MAX_XFER_SIZE);
#else
    required(_PC_REC_MAX_XFER_SIZE);
#endif
#ifdef _PC_REC_MIN_XFER_SIZE
    prpconf("/bin", _PC_REC_MIN_XFER_SIZE);
#else
    required(_PC_REC_MIN_XFER_SIZE);
#endif
#ifdef _PC_REC_XFER_ALIGN
    prpconf("/bin", _PC_REC_XFER_ALIGN);
#else
    required(_PC_REC_XFER_ALIGN);
#endif
#ifdef _PC_SYMLINK_MAX
    prpconf("/bin", _PC_SYMLINK_MAX);
#else
    required(_PC_SYMLINK_MAX);
#endif
#ifdef _PC_SYNC_IO
    prpconf("/bin", _PC_SYNC_IO);
#else
    required(_PC_SYNC_IO);
#endif
#ifdef _PC_VDISABLE
    prpconf("/dev/tty", _PC_VDISABLE);
#else
    required(_PC_VDISABLE);
#endif

    /////////////////////// SYSCONF //////////////////////////

#ifdef _SC_ARG_MAX
    prsconf(_SC_ARG_MAX);
#else
    required(_SC_ARG_MAX);
#endif
#ifdef _SC_CHILD_MAX
    prsconf(_SC_CHILD_MAX);
#else
    required(_SC_CHILD_MAX);
#endif
#ifdef _SC_CLK_TCK
    prsconf(_SC_CLK_TCK);
#else
    required(_SC_CLK_TCK);
#endif
#ifdef _SC_NGROUPS_MAX
    prsconf(_SC_NGROUPS_MAX);
#else
    required(_SC_NGROUPS_MAX);
#endif
#ifdef _SC_OPEN_MAX
    prsconf(_SC_OPEN_MAX);
#else
    required(_SC_OPEN_MAX);
#endif
#ifdef _SC_STREAM_MAX
    prsconf(_SC_STREAM_MAX);
#else
    required(_SC_STREAM_MAX);
#endif
#ifdef _SC_TZNAME_MAX
    prsconf(_SC_TZNAME_MAX);
#else
    required(_SC_TZNAME_MAX);
#endif
#ifdef _SC_JOB_CONTROL
    prsconf(_SC_JOB_CONTROL);
#else
    required(_SC_JOB_CONTROL);
#endif
#ifdef _SC_SAVED_IDS
    prsconf(_SC_SAVED_IDS);
#else
    required(_SC_SAVED_IDS);
#endif
#ifdef _SC_REALTIME_SIGNALS
    prsconf(_SC_REALTIME_SIGNALS);
#else
    required(_SC_REALTIME_SIGNALS);
#endif
#ifdef _SC_PRIORITY_SCHEDULING
    prsconf(_SC_PRIORITY_SCHEDULING);
#else
    required(_SC_PRIORITY_SCHEDULING);
#endif
#ifdef _SC_TIMERS
    prsconf(_SC_TIMERS);
#else
    required(_SC_TIMERS);
#endif
#ifdef _SC_ASYNCHRONOUS_IO
    prsconf(_SC_ASYNCHRONOUS_IO);
#else
    required(_SC_ASYNCHRONOUS_IO);
#endif
#ifdef _SC_PRIORITIZED_IO
    prsconf(_SC_PRIORITIZED_IO);
#else
    required(_SC_PRIORITIZED_IO);
#endif
#ifdef _SC_SYNCHRONIZED_IO
    prsconf(_SC_SYNCHRONIZED_IO);
#else
    required(_SC_SYNCHRONIZED_IO);
#endif
#ifdef _SC_FSYNC
    prsconf(_SC_FSYNC);
#else
    required(_SC_FSYNC);
#endif
#ifdef _SC_MAPPED_FILES
    prsconf(_SC_MAPPED_FILES);
#else
    required(_SC_MAPPED_FILES);
#endif
#ifdef _SC_MEMLOCK
    prsconf(_SC_MEMLOCK);
#else
    required(_SC_MEMLOCK);
#endif
#ifdef _SC_MEMLOCK_RANGE
    prsconf(_SC_MEMLOCK_RANGE);
#else
    required(_SC_MEMLOCK);
#endif
#ifdef _SC_MEMORY_PROTECTION
    prsconf(_SC_MEMORY_PROTECTION);
#else
    required(_SC_MEMLOCK);
#endif
#ifdef _SC_MESSAGE_PASSING
    prsconf(_SC_MESSAGE_PASSING);
#else
    required(_SC_MESSAGE_PASSING);
#endif
#ifdef _SC_SEMAPHORES
    prsconf(_SC_SEMAPHORES);
#else
    required(_SC_SEMAPHORES);
#endif
#ifdef _SC_SHARED_MEMORY_OBJECTS
    prsconf(_SC_SHARED_MEMORY_OBJECTS);
#else
    required(_SC_SHARED_MEMORY_OBJECTS);
#endif
#ifdef _SC_AIO_LISTIO_MAX
    prsconf(_SC_AIO_LISTIO_MAX);
#else
    required(_SC_AIO_LISTIO_MAX);
#endif
#ifdef _SC_AIO_MAX
    prsconf(_SC_AIO_MAX);
#else
    required(_SC_AIO_MAX);
#endif
#ifdef _SC_AIO_PRIO_DELTA_MAX
    prsconf(_SC_AIO_PRIO_DELTA_MAX);
#else
    required(_SC_AIO_PRIO_DELTA_MAX);
#endif
#ifdef _SC_DELAYTIMER_MAX
    prsconf(_SC_DELAYTIMER_MAX);
#else
    required(_SC_DELAYTIMER_MAX);
#endif
#ifdef _SC_MQ_OPEN_MAX
    prsconf(_SC_MQ_OPEN_MAX);
#else
    required(_SC_MQ_OPEN_MAX);
#endif
#ifdef _SC_MQ_PRIO_MAX
    prsconf(_SC_MQ_PRIO_MAX);
#else
    required(_SC_MQ_PRIO_MAX);
#endif
#ifdef _SC_VERSION
    prsconf(_SC_VERSION);
#else
    required(_SC_VERSION);
#endif
#ifdef _SC_PAGESIZE
    prsconf(_SC_PAGESIZE);
#else
    required(_SC_PAGESIZE);
#endif
#ifdef _SC_PAGE_SIZE
    prsconf(_SC_PAGE_SIZE);
#else
    required(_SC_PAGE_SIZE);
#endif
#ifdef _SC_RTSIG_MAX
    prsconf(_SC_RTSIG_MAX);
#else
    required(_SC_RTSIG_MAX);
#endif
#ifdef _SC_SEM_NSEMS_MAX
    prsconf(_SC_SEM_NSEMS_MAX);
#else
    required(_SC_SEM_NSEMS_MAX);
#endif
#ifdef _SC_SEM_VALUE_MAX
    prsconf(_SC_SEM_VALUE_MAX);
#else
    required(_SC_SEM_VALUE_MAX);
#endif
#ifdef _SC_SIGQUEUE_MAX
    prsconf(_SC_SIGQUEUE_MAX);
#else
    required(_SC_SIGQUEUE_MAX);
#endif
#ifdef _SC_TIMER_MAX
    prsconf(_SC_TIMER_MAX);
#else
    required(_SC_TIMER_MAX);
#endif
#ifdef _SC_BC_BASE_MAX
    prsconf(_SC_BC_BASE_MAX);
#else
    required(_SC_BC_BASE_MAX);
#endif
#ifdef _SC_BC_DIM_MAX
    prsconf(_SC_BC_DIM_MAX);
#else
    required(_SC_BC_DIM_MAX);
#endif
#ifdef _SC_BC_SCALE_MAX
    prsconf(_SC_BC_SCALE_MAX);
#else
    required(_SC_BC_SCALE_MAX);
#endif
#ifdef _SC_BC_STRING_MAX
    prsconf(_SC_BC_STRING_MAX);
#else
    required(_SC_BC_STRING_MAX);
#endif
#ifdef _SC_COLL_WEIGHTS_MAX
    prsconf(_SC_COLL_WEIGHTS_MAX);
#else
    required(_SC_COLL_WEIGHTS_MAX);
#endif
#ifdef _SC_EXPR_NEST_MAX
    prsconf(_SC_EXPR_NEST_MAX);
#else
    required(_SC_EXPR_NEST_MAX);
#endif
#ifdef _SC_LINE_MAX
    prsconf(_SC_LINE_MAX);
#else
    required(_SC_LINE_MAX);
#endif
#ifdef _SC_RE_DUP_MAX
    prsconf(_SC_RE_DUP_MAX);
#else
    required(_SC_RE_DUP_MAX);
#endif
#ifdef _SC_2_VERSION
    prsconf(_SC_2_VERSION);
#else
    required(_SC_2_VERSION);
#endif
#ifdef _SC_2_C_BIND
    prsconf(_SC_2_C_BIND);
#else
    required(_SC_2_C_BIND);
#endif
#ifdef _SC_2_C_DEV
    prsconf(_SC_2_C_DEV);
#else
    required(_SC_2_C_DEV);
#endif
#ifdef _SC_2_FORT_DEV
    prsconf(_SC_2_FORT_DEV);
#else
    required(_SC_2_FORT_DEV);
#endif
#ifdef _SC_2_FORT_RUN
    prsconf(_SC_2_FORT_RUN);
#else
    required(_SC_2_FORT_RUN);
#endif
#ifdef _SC_2_SW_DEV
    prsconf(_SC_2_SW_DEV);
#else
    required(_SC_2_SW_DEV);
#endif
#ifdef _SC_2_LOCALEDEF
    prsconf(_SC_2_LOCALEDEF);
#else
    required(_SC_2_LOCALEDEF);
#endif
#ifdef _SC_IOV_MAX
    prsconf(_SC_IOV_MAX);
#else
    required(_SC_IOV_MAX);
#endif
#ifdef _SC_THREADS
    prsconf(_SC_THREADS);
#else
    required(_SC_THREADS);
#endif
#ifdef _SC_THREAD_SAFE_FUNCTIONS
    prsconf(_SC_THREAD_SAFE_FUNCTIONS);
#else
    required(_SC_THREAD_SAFE_FUNCTIONS);
#endif
#ifdef _SC_GETGR_R_SIZE_MAX
    prsconf(_SC_GETGR_R_SIZE_MAX);
#else
    required(_SC_THREAD_SAFE_FUNCTIONS);
#endif
#ifdef _SC_GETPW_R_SIZE_MAX
    prsconf(_SC_GETPW_R_SIZE_MAX);
#else
    required(_SC_GETPW_R_SIZE_MAX);
#endif
#ifdef _SC_LOGIN_NAME_MAX
    prsconf(_SC_LOGIN_NAME_MAX);
#else
    required(_SC_LOGIN_NAME_MAX);
#endif
#ifdef _SC_TTY_NAME_MAX
    prsconf(_SC_TTY_NAME_MAX);
#else
    required(_SC_TTY_NAME_MAX);
#endif
#ifdef _SC_THREAD_DESTRUCTOR_ITERATIONS
    prsconf(_SC_THREAD_DESTRUCTOR_ITERATIONS);
#else
    required(_SC_THREAD_DESTRUCTOR_ITERATIONS);
#endif
#ifdef _SC_THREAD_KEYS_MAX
    prsconf(_SC_THREAD_KEYS_MAX);
#else
    required(_SC_THREAD_KEYS_MAX);
#endif
#ifdef _SC_THREAD_STACK_MIN
    prsconf(_SC_THREAD_STACK_MIN);
#else
    required(_SC_THREAD_STACK_MIN);
#endif
#ifdef _SC_THREAD_THREADS_MAX
    prsconf(_SC_THREAD_THREADS_MAX);
#else
    required(_SC_THREAD_THREADS_MAX);
#endif
#ifdef _SC_THREAD_ATTR_STACKADDR
    prsconf(_SC_THREAD_ATTR_STACKADDR);
#else
    required(_SC_THREAD_ATTR_STACKADDR);
#endif
#ifdef _SC_THREAD_ATTR_STACKSIZE
    prsconf(_SC_THREAD_ATTR_STACKSIZE);
#else
    required(_SC_THREAD_ATTR_STACKSIZE);
#endif
#ifdef _SC_THREAD_PRIORITY_SCHEDULING
    prsconf(_SC_THREAD_PRIORITY_SCHEDULING);
#else
    required(_SC_THREAD_PRIORITY_SCHEDULING);
#endif
#ifdef _SC_THREAD_PRIO_INHERIT
    prsconf(_SC_THREAD_PRIO_INHERIT);
#else
    required(_SC_THREAD_PRIO_INHERIT);
#endif
#ifdef _SC_THREAD_PRIO_PROTECT
    prsconf(_SC_THREAD_PRIO_PROTECT);
#else
    required(_SC_THREAD_PRIO_PROTECT);
#endif
#ifdef _SC_THREAD_PROCESS_SHARED
    prsconf(_SC_THREAD_PROCESS_SHARED);
#else
    required(_SC_THREAD_PRIO_PROTECT);
#endif
#ifdef _SC_NPROCESSORS_CONF
    prsconf(_SC_NPROCESSORS_CONF);
#else
    required(_SC_NPROCESSORS_CONF);
#endif
#ifdef _SC_NPROCESSORS_ONLN
    prsconf(_SC_NPROCESSORS_ONLN);
#else
    required(_SC_NPROCESSORS_ONLN);
#endif
#ifdef _SC_ATEXIT_MAX
    prsconf(_SC_ATEXIT_MAX);
#else
    required(_SC_ATEXIT_MAX);
#endif
#ifdef _SC_PASS_MAX
    prsconf(_SC_PASS_MAX);
#else
    required(_SC_PASS_MAX);
#endif
#ifdef _SC_XOPEN_VERSION
    prsconf(_SC_XOPEN_VERSION);
#else
    required(_SC_XOPEN_VERSION);
#endif
#ifdef _SC_XOPEN_XCU_VERSION
    prsconf(_SC_XOPEN_XCU_VERSION);
#else
    required(_SC_XOPEN_XCU_VERSION);
#endif
#ifdef _SC_XOPEN_UNIX
    prsconf(_SC_XOPEN_UNIX);
#else
    required(_SC_XOPEN_UNIX);
#endif
#ifdef _SC_XOPEN_CRYPT
    prsconf(_SC_XOPEN_CRYPT);
#else
    required(_SC_XOPEN_CRYPT);
#endif
#ifdef _SC_XOPEN_ENH_I18N
    prsconf(_SC_XOPEN_ENH_I18N);
#else
    required(_SC_XOPEN_ENH_I18N);
#endif
#ifdef _SC_XOPEN_SHM
    prsconf(_SC_XOPEN_SHM);
#else
    required(_SC_XOPEN_SHM);
#endif
#ifdef _SC_2_CHAR_TERM
    prsconf(_SC_2_CHAR_TERM);
#else
    required(_SC_2_CHAR_TERM);
#endif
#ifdef _SC_2_UPE
    prsconf(_SC_2_UPE);
#else
    required(_SC_2_UPE);
#endif
#ifdef _SC_XBS5_ILP32_OFF32
    prsconf(_SC_XBS5_ILP32_OFF32);
#else
    required(_SC_XBS5_ILP32_OFF32);
#endif
#ifdef _SC_XBS5_ILP32_OFFBIG
    prsconf(_SC_XBS5_ILP32_OFFBIG);
#else
    required(_SC_XBS5_ILP32_OFFBIG);
#endif
#ifdef _SC_XBS5_LP64_OFF64
    prsconf(_SC_XBS5_LP64_OFF64);
#else
    required(_SC_XBS5_LP64_OFF64);
#endif
#ifdef _SC_XBS5_LPBIG_OFFBIG
    prsconf(_SC_XBS5_LPBIG_OFFBIG);
#else
    required(_SC_XBS5_LPBIG_OFFBIG);
#endif
#ifdef _SC_XOPEN_LEGACY
    prsconf(_SC_XOPEN_LEGACY);
#else
    required(_SC_XOPEN_LEGACY);
#endif
#ifdef _SC_XOPEN_REALTIME
    prsconf(_SC_XOPEN_REALTIME);
#else
    required(_SC_XOPEN_REALTIME);
#endif
#ifdef _SC_XOPEN_REALTIME_THREADS
    prsconf(_SC_XOPEN_REALTIME_THREADS);
#else
    required(_SC_XOPEN_REALTIME_THREADS);
#endif
#ifdef _SC_ADVISORY_INFO
    prsconf(_SC_ADVISORY_INFO);
#else
    required(_SC_ADVISORY_INFO);
#endif
#ifdef _SC_BARRIERS
    prsconf(_SC_BARRIERS);
#else
    required(_SC_BARRIERS);
#endif
#ifdef _SC_CLOCK_SELECTION
    prsconf(_SC_CLOCK_SELECTION);
#else
    required(_SC_CLOCK_SELECTION);
#endif
#ifdef _SC_CPUTIME
    prsconf(_SC_CPUTIME);
#else
    required(_SC_CPUTIME);
#endif
#ifdef _SC_THREAD_CPUTIME
    prsconf(_SC_THREAD_CPUTIME);
#else
    required(_SC_THREAD_CPUTIME);
#endif
#ifdef _SC_FILE_LOCKING
    prsconf(_SC_FILE_LOCKING);
#else
    required(_SC_FILE_LOCKING);
#endif
#ifdef _SC_MONOTONIC_CLOCK
    prsconf(_SC_MONOTONIC_CLOCK);
#else
    required(_SC_MONOTONIC_CLOCK);
#endif
#ifdef _SC_READER_WRITER_LOCKS
    prsconf(_SC_READER_WRITER_LOCKS);
#else
    required(_SC_READER_WRITER_LOCKS);
#endif
#ifdef _SC_SPIN_LOCKS
    prsconf(_SC_SPIN_LOCKS);
#else
    required(_SC_SPIN_LOCKS);
#endif
#ifdef _SC_REGEXP
    prsconf(_SC_REGEXP);
#else
    required(_SC_REGEXP);
#endif
#ifdef _SC_SHELL
    prsconf(_SC_SHELL);
#else
    required(_SC_SHELL);
#endif
#ifdef _SC_SPAWN
    prsconf(_SC_SPAWN);
#else
    required(_SC_SPAWN);
#endif
#ifdef _SC_SPORADIC_SERVER
    prsconf(_SC_SPORADIC_SERVER);
#else
    required(_SC_SPORADIC_SERVER);
#endif
#ifdef _SC_THREAD_SPORADIC_SERVER
    prsconf(_SC_THREAD_SPORADIC_SERVER);
#else
    required(_SC_THREAD_SPORADIC_SERVER);
#endif
#ifdef _SC_TIMEOUTS
    prsconf(_SC_TIMEOUTS);
#else
    required(_SC_TIMEOUTS);
#endif
#ifdef _SC_TYPED_MEMORY_OBJECTS
    prsconf(_SC_TYPED_MEMORY_OBJECTS);
#else
    required(_SC_TYPED_MEMORY_OBJECTS);
#endif
#ifdef _SC_2_PBS
    prsconf(_SC_2_PBS);
#else
    required(_SC_2_PBS);
#endif
#ifdef _SC_2_PBS_ACCOUNTING
    prsconf(_SC_2_PBS_ACCOUNTING);
#else
    required(_SC_2_PBS_ACCOUNTING);
#endif
#ifdef _SC_2_PBS_LOCATE
    prsconf(_SC_2_PBS_LOCATE);
#else
    required(_SC_2_PBS_LOCATE);
#endif
#ifdef _SC_2_PBS_MESSAGE
    prsconf(_SC_2_PBS_MESSAGE);
#else
    required(_SC_2_PBS_MESSAGE);
#endif
#ifdef _SC_2_PBS_TRACK
    prsconf(_SC_2_PBS_TRACK);
#else
    required(_SC_2_PBS_TRACK);
#endif
#ifdef _SC_SYMLOOP_MAX
    prsconf(_SC_SYMLOOP_MAX);
#else
    required(_SC_SYMLOOP_MAX);
#endif
#ifdef _SC_2_PBS_CHECKPOINT
    prsconf(_SC_2_PBS_CHECKPOINT);
#else
    required(_SC_2_PBS_CHECKPOINT);
#endif
#ifdef _SC_V6_ILP32_OFF32
    prsconf(_SC_V6_ILP32_OFF32);
#else
    required(_SC_V6_ILP32_OFF32);
#endif
#ifdef _SC_V6_ILP32_OFFBIG
    prsconf(_SC_V6_ILP32_OFFBIG);
#else
    required(_SC_V6_ILP32_OFFBIG);
#endif
#ifdef _SC_V6_LP64_OFF64
    prsconf(_SC_V6_LP64_OFF64);
#else
    required(_SC_V6_LP64_OFF64);
#endif
#ifdef _SC_V6_LPBIG_OFFBIG
    prsconf(_SC_V6_LPBIG_OFFBIG);
#else
    required(_SC_V6_LPBIG_OFFBIG);
#endif
#ifdef _SC_HOST_NAME_MAX
    prsconf(_SC_HOST_NAME_MAX);
#else
    required(_SC_HOST_NAME_MAX);
#endif
#ifdef _SC_TRACE
    prsconf(_SC_TRACE);
#else
    required(_SC_TRACE);
#endif
#ifdef _SC_TRACE_EVENT_FILTER
    prsconf(_SC_TRACE_EVENT_FILTER);
#else
    required(_SC_TRACE_EVENT_FILTER);
#endif
#ifdef _SC_TRACE_INHERIT
    prsconf(_SC_TRACE_INHERIT);
#else
    required(_SC_TRACE_INHERIT);
#endif
#ifdef _SC_TRACE_LOG
    prsconf(_SC_TRACE_LOG);
#else
    required(_SC_TRACE_LOG);
#endif

    /////////////////////// POSIX //////////////////////////

#ifdef _POSIX2_CHAR_TERM
    proconf(_POSIX2_CHAR_TERM);
#else
    notrequired(_POSIX2_CHAR_TERM);
#endif
#ifdef _POSIX2_C_BIND
    proconf(_POSIX2_C_BIND);
#else
    notrequired(_POSIX2_C_BIND);
#endif
#ifdef _POSIX2_C_DEV
    proconf(_POSIX2_C_DEV);
#else
    notrequired(_POSIX2_C_DEV);
#endif
#ifdef _POSIX2_FORT_DEV
    proconf(_POSIX2_FORT_DEV);
#else
    notrequired(_POSIX2_FORT_DEV);
#endif
#ifdef _POSIX2_FORT_RUN
    proconf(_POSIX2_FORT_RUN);
#else
    notrequired(_POSIX2_FORT_RUN);
#endif
#ifdef _POSIX2_LOCALEDEF
    proconf(_POSIX2_LOCALEDEF);
#else
    notrequired(_POSIX2_LOCALEDEF);
#endif
#ifdef _POSIX2_PBS
    proconf(_POSIX2_PBS);
#else
    notrequired(_POSIX2_PBS);
#endif
#ifdef _POSIX2_PBS_ACCOUNTING
    proconf(_POSIX2_PBS_ACCOUNTING);
#else
    notrequired(_POSIX2_PBS_ACCOUNTING);
#endif
#ifdef _POSIX2_PBS_CHECKPOINT
    proconf(_POSIX2_PBS_CHECKPOINT);
#else
    notrequired(_POSIX2_PBS_CHECKPOINT);
#endif
#ifdef _POSIX2_PBS_LOCATE
    proconf(_POSIX2_PBS_LOCATE);
#else
    notrequired(_POSIX2_PBS_LOCATE);
#endif
#ifdef _POSIX2_PBS_MESSAGE
    proconf(_POSIX2_PBS_MESSAGE);
#else
    notrequired(_POSIX2_PBS_MESSAGE);
#endif
#ifdef _POSIX2_PBS_TRACK
    proconf(_POSIX2_PBS_TRACK);
#else
    notrequired(_POSIX2_PBS_TRACK);
#endif
#ifdef _POSIX2_SW_DEV
    proconf(_POSIX2_SW_DEV);
#else
    notrequired(_POSIX2_SW_DEV);
#endif
#ifdef _POSIX2_UPE
    proconf(_POSIX2_UPE);
#else
    notrequired(_POSIX2_UPE);
#endif
#ifdef _POSIX2_VERSION
    proconf(_POSIX2_VERSION);
#else
    required(_POSIX2_VERSION);
#endif
#ifdef _POSIX_ADVISORY_INFO
    proconf(_POSIX_ADVISORY_INFO);
#else
    notrequired(_POSIX_ADVISORY_INFO);
#endif
#ifdef _POSIX_ASYNCHRONOUS_IO
    proconf(_POSIX_ASYNCHRONOUS_IO);
#else
    notrequired(_POSIX_ASYNCHRONOUS_IO);
#endif
#ifdef _POSIX_ASYNC_IO
    proconf(_POSIX_ASYNC_IO);
#else
    notrequired(_POSIX_ASYNC_IO);
#endif
#ifdef _POSIX_BARRIERS
    proconf(_POSIX_BARRIERS);
#else
    notrequired(_POSIX_BARRIERS);
#endif
#ifdef _POSIX_CHOWN_RESTRICTED
    proconf(_POSIX_CHOWN_RESTRICTED);
#else
    notrequired(_POSIX_CHOWN_RESTRICTED);
#endif
#ifdef _POSIX_CLOCK_SELECTION
    proconf(_POSIX_CLOCK_SELECTION);
#else
    notrequired(_POSIX_CLOCK_SELECTION);
#endif
#ifdef _POSIX_CPUTIME
    proconf(_POSIX_CPUTIME);
#else
    notrequired(_POSIX_CPUTIME);
#endif
#ifdef _POSIX_FSYNC
    proconf(_POSIX_FSYNC);
#else
    notrequired(_POSIX_FSYNC);
#endif
#ifdef _POSIX_IPV6
    proconf(_POSIX_IPV6);
#else
    notrequired(_POSIX_IPV6);
#endif
#ifdef _POSIX_JOB_CONTROL
    proconf(_POSIX_JOB_CONTROL);
#else
    notrequired(_POSIX_JOB_CONTROL);
#endif
#ifdef _POSIX_MAPPED_FILES
    proconf(_POSIX_MAPPED_FILES);
#else
    notrequired(_POSIX_MAPPED_FILES);
#endif
#ifdef _POSIX_MEMLOCK
    proconf(_POSIX_MEMLOCK);
#else
    notrequired(_POSIX_MEMLOCK);
#endif
#ifdef _POSIX_MEMLOCK_RANGE
    proconf(_POSIX_MEMLOCK_RANGE);
#else
    notrequired(_POSIX_MEMLOCK_RANGE);
#endif
#ifdef _POSIX_MEMORY_PROTECTION
    proconf(_POSIX_MEMORY_PROTECTION);
#else
    notrequired(_POSIX_MEMORY_PROTECTION);
#endif
#ifdef _POSIX_MESSAGE_PASSING
    proconf(_POSIX_MESSAGE_PASSING);
#else
    notrequired(_POSIX_MESSAGE_PASSING);
#endif
#ifdef _POSIX_MONOTONIC_CLOCK
    proconf(_POSIX_MONOTONIC_CLOCK);
#else
    notrequired(_POSIX_MONOTONIC_CLOCK);
#endif
#ifdef _POSIX_NO_TRUNC
    proconf(_POSIX_NO_TRUNC);
#else
    notrequired(_POSIX_NO_TRUNC);
#endif
#ifdef _POSIX_PRIORITIZED_IO
    proconf(_POSIX_PRIORITIZED_IO);
#else
    notrequired(_POSIX_PRIORITIZED_IO);
#endif
#ifdef _POSIX_PRIO_IO
    proconf(_POSIX_PRIO_IO);
#else
    notrequired(_POSIX_PRIO_IO);
#endif
#ifdef _POSIX_PRIORITY_SCHEDULING
    proconf(_POSIX_PRIORITY_SCHEDULING);
#else
    notrequired(_POSIX_PRIORITY_SCHEDULING);
#endif
#ifdef _POSIX_RAW_SOCKETS
    proconf(_POSIX_RAW_SOCKETS);
#else
    notrequired(_POSIX_RAW_SOCKETS);
#endif
#ifdef _POSIX_READER_WRITER_LOCKS
    proconf(_POSIX_READER_WRITER_LOCKS);
#else
    notrequired(_POSIX_READER_WRITER_LOCKS);
#endif
#ifdef _POSIX_REALTIME_SIGNALS
    proconf(_POSIX_REALTIME_SIGNALS);
#else
    notrequired(_POSIX_REALTIME_SIGNALS);
#endif
#ifdef _POSIX_REGEXP
    proconf(_POSIX_REGEXP);
#else
    notrequired(_POSIX_REGEXP);
#endif
#ifdef _POSIX_SAVED_IDS
    proconf(_POSIX_SAVED_IDS);
#else
    notrequired(_POSIX_SAVED_IDS);
#endif
#ifdef _POSIX_SEMAPHORES
    proconf(_POSIX_SEMAPHORES);
#else
    notrequired(_POSIX_SEMAPHORES);
#endif
#ifdef _POSIX_SHARED_MEMORY_OBJECTS
    proconf(_POSIX_SHARED_MEMORY_OBJECTS);
#else
    notrequired(_POSIX_SHARED_MEMORY_OBJECTS);
#endif
#ifdef _POSIX_SHELL
    proconf(_POSIX_SHELL);
#else
    notrequired(_POSIX_SHELL);
#endif
#ifdef _POSIX_SPAWN
    proconf(_POSIX_SPAWN);
#else
    notrequired(_POSIX_SPAWN);
#endif
#ifdef _POSIX_SPIN_LOCKS
    proconf(_POSIX_SPIN_LOCKS);
#else
    notrequired(_POSIX_SPIN_LOCKS);
#endif
#ifdef _POSIX_SPORADIC_SERVER
    proconf(_POSIX_SPORADIC_SERVER);
#else
    notrequired(_POSIX_SPORADIC_SERVER);
#endif
#ifdef _POSIX_SYNCHRONIZED_IO
    proconf(_POSIX_SYNCHRONIZED_IO);
#else
    notrequired(_POSIX_SYNCHRONIZED_IO);
#endif
#ifdef _POSIX_SYNC_IO
    proconf(_POSIX_SYNC_IO);
#else
    notrequired(_POSIX_SYNC_IO);
#endif
#ifdef _POSIX_THREADS
    proconf(_POSIX_THREADS);
#else
    notrequired(_POSIX_THREADS);
#endif
#ifdef _POSIX_THREAD_ATTR_STACKADDR
    proconf(_POSIX_THREAD_ATTR_STACKADDR);
#else
    notrequired(_POSIX_THREAD_ATTR_STACKADDR);
#endif
#ifdef _POSIX_THREAD_ATTR_STACKSIZE
    proconf(_POSIX_THREAD_ATTR_STACKSIZE);
#else
    notrequired(_POSIX_THREAD_ATTR_STACKSIZE);
#endif
#ifdef _POSIX_THREAD_CPUTIME
    proconf(_POSIX_THREAD_CPUTIME);
#else
    notrequired(_POSIX_THREAD_CPUTIME);
#endif
#ifdef _POSIX_THREAD_KEYS_MAX
    proconf(_POSIX_THREAD_KEYS_MAX);
#else
    notrequired(_POSIX_THREAD_KEYS_MAX);
#endif
#ifdef _POSIX_THREAD_PRIORITY_SCHEDULING
    proconf(_POSIX_THREAD_PRIORITY_SCHEDULING);
#else
    notrequired(_POSIX_THREAD_PRIORITY_SCHEDULING);
#endif
#ifdef _POSIX_THREAD_PRIO_INHERIT
    proconf(_POSIX_THREAD_PRIO_INHERIT);
#else
    notrequired(_POSIX_THREAD_PRIO_INHERIT);
#endif
#ifdef _POSIX_THREAD_PRIO_PROTECT
    proconf(_POSIX_THREAD_PRIO_PROTECT);
#else
    notrequired(_POSIX_THREAD_PRIO_PROTECT);
#endif
#ifdef _POSIX_THREAD_PROCESS_SHARED
    proconf(_POSIX_THREAD_PROCESS_SHARED);
#else
    notrequired(_POSIX_THREAD_PROCESS_SHARED);
#endif
#ifdef _POSIX_THREAD_SAFE_FUNCTIONS
    proconf(_POSIX_THREAD_SAFE_FUNCTIONS);
#else
    notrequired(_POSIX_THREAD_SAFE_FUNCTIONS);
#endif
#ifdef _POSIX_THREAD_SPORADIC_SERVER
    proconf(_POSIX_THREAD_SPORADIC_SERVER);
#else
    notrequired(_POSIX_THREAD_SPORADIC_SERVER);
#endif
#ifdef _POSIX_TIMEOUTS
    proconf(_POSIX_TIMEOUTS);
#else
    notrequired(_POSIX_TIMEOUTS);
#endif
#ifdef _POSIX_TIMERS
    proconf(_POSIX_TIMERS);
#else
    notrequired(_POSIX_TIMERS);
#endif
#ifdef _POSIX_TRACE
    proconf(_POSIX_TRACE);
#else
    notrequired(_POSIX_TRACE);
#endif
#ifdef _POSIX_TRACE_EVENT_FILTER
    proconf(_POSIX_TRACE_EVENT_FILTER);
#else
    notrequired(_POSIX_TRACE_EVENT_FILTER);
#endif
#ifdef _POSIX_TRACE_INHERIT
    proconf(_POSIX_TRACE_INHERIT);
#else
    notrequired(_POSIX_TRACE_INHERIT);
#endif
#ifdef _POSIX_TRACE_LOG
    proconf(_POSIX_TRACE_LOG);
#else
    notrequired(_POSIX_TRACE_LOG);
#endif
#ifdef _POSIX_TYPED_MEMORY_OBJECTS
    proconf(_POSIX_TYPED_MEMORY_OBJECTS);
#else
    notrequired(_POSIX_TYPED_MEMORY_OBJECTS);
#endif
#ifdef _POSIX_VDISABLE
    proconf(_POSIX_VDISABLE);
#else
    notrequired(_POSIX_VDISABLE);
#endif
#ifdef _POSIX_VERSION
    proconf(_POSIX_VERSION);
#else
    required(_POSIX_VERSION);
#endif
#ifdef _XOPEN_CRYPT
    proconf(_XOPEN_CRYPT);
#else
    required(_XOPEN_CRYPT);
#endif
#ifdef _XOPEN_ENH_I18N
    proconf(_XOPEN_ENH_I18N);
#else
    required(_XOPEN_ENH_I18N);
#endif
#ifdef _XOPEN_LEGACY
    proconf(_XOPEN_LEGACY);
#else
    required(_XOPEN_LEGACY);
#endif
#ifdef _XOPEN_REALTIME
    proconf(_XOPEN_REALTIME);
#else
    required(_XOPEN_REALTIME);
#endif
#ifdef _XOPEN_REALTIME_THREADS
    proconf(_XOPEN_REALTIME_THREADS);
#else
    required(_XOPEN_REALTIME_THREADS);
#endif
#ifdef _XOPEN_SHM
    proconf(_XOPEN_SHM);
#else
    required(_XOPEN_SHM);
#endif
#ifdef _XOPEN_STREAMS
    proconf(_XOPEN_STREAMS);
#else
    required(_XOPEN_STREAMS);
#endif
#ifdef _XOPEN_UNIX
    proconf(_XOPEN_UNIX);
#else
    required(_XOPEN_UNIX);
#endif
#ifdef _XOPEN_XCU_VERSION
    proconf(_XOPEN_XCU_VERSION);
#else
    required(_XOPEN_XCU_VERSION);
#endif
#ifdef _XOPEN_VERSION
    proconf(_XOPEN_VERSION);
#else
    required(_XOPEN_VERSION);
#endif

    return 0;
}
