#define __UUFILE__ "adminebrary.c"

#include "common.h"
#include "uustring.h"
#include "usertoken.h"

#include "adminebrary.h"

struct EBRARYSITE *AdminEbrarySite(char *site, size_t siteLen) {
    struct EBRARYSITE *ebs;

    if (siteLen == 0)
        siteLen = FirstSlashQuestion(site) - site;

    for (ebs = ebrarySites; ebs; ebs = ebs->next)
        if (strncmp(ebs->site, site, siteLen) == 0 && strlen(ebs->site) == siteLen)
            break;

    return ebs;
}

static int AdminEbraryConnect(struct TRANSLATE *t,
                              struct EBRARYSITE *ebs,
                              struct UUSOCKET *r,
                              char *method) {
    int rc;
    int result = 1;
    char *psa = NULL;
    BOOL throughProxy = 0;
    BOOL needConnect = 0;
    char line[512];
    char *arg;

    UUInitSocket(r, INVALID_SOCKET);

    if (ebs->proxyPort != 0 && ebs->useSsl == 0) {
        rc = UUConnectWithSource2(r, ebs->proxyHost, ebs->proxyPort, "AdminEbraryConnect-p",
                                  &ebs->ipInterface, 0);
        psa = ebs->proxyAuth;
        throughProxy = 1;
    } else if (ebs->proxySslPort != 0 && ebs->useSsl) {
        /* Connecting through a proxy for https still starts with normal socket */
        rc = UUConnectWithSource2(r, ebs->proxySslHost, ebs->proxySslPort, "AdminEbraryConnect-ps",
                                  &ebs->ipInterface, 0);
        needConnect = 1;
    } else {
        rc = UUConnectWithSource2(r, ebs->host, ebs->port, "AdminEbraryConnect", &ebs->ipInterface,
                                  ebs->useSsl);
    }

    if (rc != 0)
        goto Finished;

    /* r->timeout = remoteTimeout; */

    if (needConnect) {
        UUSendZ(r, "CONNECT ");
        UUSendZ(r, ebs->host);
        WriteLine(r, ":%d HTTP/1.0\r\nHost: ", ebs->port);
        UUSendZ(r, ebs->host);
        WriteLine(r, ":%d\r\n", ebs->port);
        if (ebs->proxySslAuth) {
            UUSendZ(r, "Proxy-Authorization: basic ");
            UUSendZCRLF(r, ebs->proxySslAuth);
        }
        UUSendCRLF(r);
        result = 0;
        while (ReadLine(r, line, sizeof(line))) {
            if (line[0] == 0)
                break;
            if (strnicmp(line, "HTTP/", 5) == 0) {
                arg = SkipNonWS(SkipWS(line));
                result = atoi(arg);
                if (result < 200 || result > 299) {
                    Log("ebrary ProxySSL connection refused: %s", line);
                    goto Finished;
                }
            }
        }
        UUInitSocketSsl(r, r->o, UUSSLCONNECT, 0, ebs->host);
        if (r->ssl == NULL) {
            Log("ebrary unable to negotiate secure connection through proxy");
            goto Finished;
        }
    }

    WriteLine(r, "%s ", method);
    if (throughProxy) {
        WriteLine(r, "http%s://", (ebs->useSsl ? "s" : ""));
        UUSendZ(r, ebs->host);
        if (ebs->port != (ebs->useSsl ? 443 : 80)) {
            WriteLine(r, ":%d", ebs->port);
        }
    }
    result = 0;

Finished:
    return result;
}

static int AdminEbrarySignInRequest(struct TRANSLATE *t,
                                    struct EBRARYSITE *ebs,
                                    char *userToken,
                                    xmlChar **errorMessage) {
    struct UUSOCKET rr, *r = &rr;
    char *p;
    time_t partnerKey;
    static time_t nextPartnerKey = 0;
    char line[2048];
    char *lineEnd;
    BOOL inHeader;
    xmlParserCtxtPtr ctxt = NULL;
    xmlDocPtr doc = NULL; /* the resulting document tree */
    int res;
    xmlXPathContextPtr xpathCtx = NULL;
    xmlXPathObjectPtr xpathObj = NULL;
    xmlNodeSetPtr nodes = NULL;
    char *status = NULL;
    char *errorCode = NULL;
    int result = 1;

    if (errorMessage)
        *errorMessage = NULL;

    UUAcquireMutex(&mActive);
    if (nextPartnerKey == 0)
        UUtime(&nextPartnerKey);
    partnerKey = nextPartnerKey++;
    UUReleaseMutex(&mActive);

    UUInitSocket(r, INVALID_SOCKET);

    if (AdminEbraryConnect(t, ebs, r, "GET") != 0)
        goto Finished;

    lineEnd = line + sizeof(line) - 64;

    sprintf(line, "/lib/%s/SignInRequest?ebrary_username=", ebs->site);
    p = AddEncodedField(line, ebs->site, lineEnd);
    p = AddEncodedField(p, "_", lineEnd);
    p = AddEncodedField(p, userToken, lineEnd);
    sprintf(p, "&partner_key=%" PRIdMAX "", (intmax_t)partnerKey);
    UUSendZ(r, line);
    WriteLine(r, " HTTP/1.0\r\nUser-Agent: EZproxy\r\nHost: %s\r\n\r\n", ebs->host);

    inHeader = 1;
    while (ReadLine(r, line, sizeof(line))) {
        if (inHeader) {
            if (line[0] == 0) {
                inHeader = 0;
            } else {
            }
            continue;
        }
        if (debugLevel > 5) {
            Log("ebrary SignInRequest %s", line);
        }
        if (ctxt == NULL) {
            ctxt = xmlCreatePushParserCtxt(NULL, NULL, line, strlen(line), NULL);
            if (ctxt == NULL) {
                Log("Non-XML response from ebrary");
                goto Finished;
            }
        } else {
            xmlParseChunk(ctxt, line, strlen(line), 0);
        }
    }

    if (ctxt == NULL) {
        Log("ebrary SignInRequest server did not respond");
        goto Finished;
    }

    xmlParseChunk(ctxt, line, 0, 1);

    doc = ctxt->myDoc;
    res = ctxt->wellFormed;

    xpathCtx = xmlXPathNewContext(doc);
    if (xpathCtx == NULL) {
        Log("Error: unable to create new XPath context");
        goto Finished;
    }

    /* xmlXPathRegisterNs(xpathCtx, BAD_CAST "cas", BAD_CAST "http://www.yale.edu/tp/cas"); */

    xpathObj = xmlXPathEvalExpression(BAD_CAST "/sign-in-response", xpathCtx);
    if (xpathObj == NULL || (nodes = xpathObj->nodesetval) == NULL || (nodes->nodeNr == 0)) {
        goto Finished;
    }

    status = (char *)xmlGetProp(nodes->nodeTab[0], BAD_CAST "status");

    if (strcmp(status, "success") == 0) {
        sprintf(line, "/lib/%s/SignInPartnerUser?ebrary_username=%s_", ebs->site, ebs->site);
        p = AddEncodedField(line, userToken, NULL);
        sprintf(p, "&partner_key=%" PRIdMAX "", (intmax_t)partnerKey);
        GenericRedirect(t, ebs->host, (PORT)(ebs->redirectHttp ? 80 : ebs->port), line,
                        (char)(ebs->redirectHttp ? 0 : ebs->useSsl), 0);
        result = 0;
        goto Finished;
    }

    errorCode = (char *)xmlGetProp(nodes->nodeTab[0], BAD_CAST "error-code");
    if (errorCode && atoi(errorCode))
        result = atoi(errorCode);

    if (errorMessage)
        *errorMessage = xmlGetProp(nodes->nodeTab[0], BAD_CAST "error-message");

Finished:
    UUStopSocket(r, UUSSTOPNICE);

    if (errorCode) {
        xmlFree(errorCode);
        errorCode = NULL;
    }

    if (status) {
        xmlFree(status);
        status = NULL;
    }

    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    if (xpathCtx) {
        xmlXPathFreeContext(xpathCtx);
        xpathCtx = NULL;
    }

    if (ctxt) {
        xmlFreeParserCtxt(ctxt);
        ctxt = NULL;
    }

    if (doc) {
        xmlFreeDoc(doc);
        doc = NULL;
    }

    return result;
}

static int AdminEbraryUserRequest(struct TRANSLATE *t,
                                  struct EBRARYSITE *ebs,
                                  char *userToken,
                                  xmlChar **errorMessage) {
    struct UUSOCKET rr, *r = &rr;
    char line[2048];
    BOOL inHeader;
    xmlParserCtxtPtr ctxt = NULL;
    xmlDocPtr doc = NULL; /* the resulting document tree */
    int res;
    xmlXPathContextPtr xpathCtx = NULL;
    xmlXPathObjectPtr xpathObj = NULL;
    xmlNodeSetPtr nodes = NULL;
    char *status = NULL;
    int result = 1;
    int contentLength;
    char content[1024];
    char *contentEnd;
    char *p;
    char *errorCode = NULL;
    int i;

    if (errorMessage)
        *errorMessage = NULL;

    UUInitSocket(r, INVALID_SOCKET);

    if (AdminEbraryConnect(t, ebs, r, "POST") != 0)
        goto Finished;

    contentEnd = content + sizeof(content) - 10;

    p = content;
    *p = 0;

    strcat(p, "customer_id=");
    p = AddEncodedField(p, userToken, contentEnd);
    strcat(p, "&first_name=first&last_name=last&email=none@ebrary.com&ebrary_password=");
    p = strchr(p, 0);
    for (i = 0; i < 8; i++) {
        *p++ = RandChar();
    }
    *p = 0;

    contentLength = strlen(content);

    WriteLine(r,
              "/lib/%s/UserRequest HTTP/1.0\r\nHost: %s\r\nUser-Agent: EZproxy\r\nContent-Type: "
              "application/x-www-form-urlencoded\r\nContent-length: %d\r\n\r\n",
              ebs->site, ebs->host, contentLength);

    UUSend(r, content, contentLength, 0);

    inHeader = 1;
    while (ReadLine(r, line, sizeof(line))) {
        if (inHeader) {
            if (line[0] == 0) {
                inHeader = 0;
            } else {
            }
            continue;
        }
        if (debugLevel > 5) {
            Log("ebrary UserRequest %s", line);
        }
        if (ctxt == NULL) {
            ctxt = xmlCreatePushParserCtxt(NULL, NULL, line, strlen(line), NULL);
            if (ctxt == NULL) {
                Log("Non-XML response from CAS");
                goto Finished;
            }
        } else {
            xmlParseChunk(ctxt, line, strlen(line), 0);
        }
    }

    if (ctxt == NULL) {
        Log("ebrary UserRequest server did not respond");
        goto Finished;
    }

    xmlParseChunk(ctxt, line, 0, 1);

    doc = ctxt->myDoc;
    res = ctxt->wellFormed;

    if (debugLevel > 5)
        printf("\n\n");

    xpathCtx = xmlXPathNewContext(doc);
    if (xpathCtx == NULL) {
        Log("Error: unable to create new XPath context");
        goto Finished;
    }

    /* xmlXPathRegisterNs(xpathCtx, BAD_CAST "cas", BAD_CAST "http://www.yale.edu/tp/cas"); */

    xpathObj = xmlXPathEvalExpression(BAD_CAST "/ebrary-user-response", xpathCtx);
    if (xpathObj == NULL || (nodes = xpathObj->nodesetval) == NULL || (nodes->nodeNr == 0)) {
        goto Finished;
    }

    status = (char *)xmlGetProp(nodes->nodeTab[0], BAD_CAST "status");

    if (strcmp(status, "success") == 0) {
        result = 0;
    }

    errorCode = (char *)xmlGetProp(nodes->nodeTab[0], BAD_CAST "error-code");
    if (errorCode && atoi(errorCode))
        result = atoi(errorCode);

    if (errorMessage)
        *errorMessage = xmlGetProp(nodes->nodeTab[0], BAD_CAST "error-message");

Finished:
    UUStopSocket(r, UUSSTOPNICE);

    if (errorCode) {
        xmlFree(errorCode);
        errorCode = NULL;
    }

    if (status) {
        xmlFree(status);
        status = NULL;
    }

    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    if (xpathCtx) {
        xmlXPathFreeContext(xpathCtx);
        xpathCtx = NULL;
    }

    if (ctxt) {
        xmlFreeParserCtxt(ctxt);
        ctxt = NULL;
    }

    if (doc) {
        xmlFreeDoc(doc);
        doc = NULL;
    }

    return result;
}

void AdminEbrary(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    char *site;
    char *slash;
    int result;
    struct EBRARYSITE *ebs;
    char *userToken = NULL;
    xmlChar *errorMessage = NULL;
    struct SESSION *e;

    if (strcmp(t->urlCopy, "/ebrary/unauthorized") == 0) {
        strcpy(t->urlCopy, "/ebrary/emcc/unauthorized");
    }

    if ((site = StartsIWith(t->urlCopy, "/ebrary/")) == NULL ||
        (slash = strchr(site, '/')) == NULL || site == slash) {
        HTTPCode(t, 404, 1);
        goto Finished;
    }

    ebs = AdminEbrarySite(site, slash - site);

    if (ebs == NULL) {
        HTMLHeader(t, htmlHeaderOmitCache);
        UUSendZ(s, "EZproxy is not configured for this ebrary site");
        goto Finished;
    }

    if ((e = t->session) == NULL) {
        AdminRelogin(t);
        goto Finished;
    }

    if (GroupMaskOverlap(e->gm, ebs->gm) == 0 || e->autoLoginBy != autoLoginByNone) {
        AdminLogup(t, TRUE, NULL, NULL, ebs->gm, FALSE);
        goto Finished;
    }

    userToken = UserToken2("ebr", (t->session)->logUserBrief, 1);

    if (userToken == NULL) {
        AdminNoToken(t);
        goto Finished;
    }

    result = AdminEbrarySignInRequest(t, ebs, userToken, &errorMessage);
    if (result == 0)
        goto Finished;

    if (result == 4008 || result == 4202) {
        if ((result = AdminEbraryUserRequest(t, ebs, userToken, &errorMessage)) == 0 &&
            (result = AdminEbrarySignInRequest(t, ebs, userToken, &errorMessage)) == 0)
            goto Finished;
    }

    HTMLHeader(t, htmlHeaderOmitCache);

    UUSendZ(s, "ebrary request for site ");
    SendHTMLEncoded(s, ebs->site);
    UUSendZ(s, " to host ");
    SendHTMLEncoded(s, ebs->host);
    WriteLine(s, " failed: %d ", result);
    if (errorMessage)
        SendHTMLEncoded(s, (char *)errorMessage);

Finished:
    if (errorMessage) {
        xmlFree(errorMessage);
        errorMessage = NULL;
    }

    FreeThenNull(userToken);
}
