#define __UUFILE__ "win32rwl.c"
#ifdef WIN32

#include "common.h"

/* #include "errors.h" */
/* #include "win32rwl.h" */

int RWLInit(struct RWL *rwl, char *name) {
    InitializeCriticalSection(&rwl->cs);

    /* Create condRead so it continues to signal until reset
       which allows a group of readers to build at once
    */
    rwl->condRead = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (rwl->condRead == NULL)
        PANIC;

    /* Create condWrite to auto-reset since we can only have
       one writer at a time
    */
    rwl->condWrite = CreateEvent(NULL, FALSE, FALSE, NULL);

    if (rwl->condWrite == NULL)
        PANIC;

    rwl->waitRead = rwl->waitWrite = rwl->activeRead = rwl->activeWrite = 0;
    if (!(rwl->name = strdup(name)))
        PANIC;

    return 0;
}

int RWLDestroy(struct RWL *rwl) {
    DeleteCriticalSection(&rwl->cs);
    CloseHandle(rwl->condRead);
    CloseHandle(rwl->condWrite);
    return 0;
}

int RWLSignal(struct RWL *rwl) {
    if (rwl->waitWrite != 0 && rwl->activeRead == 0) {
        if (SetEvent(rwl->condWrite) == 0)
            PANIC;
    } else if (rwl->waitRead != 0 && rwl->activeWrite == 0) {
        if (SetEvent(rwl->condRead) == 0)
            PANIC;
    }
    LeaveCriticalSection(&rwl->cs);
    return 0;
}

int RWLAcquireRead(struct RWL *rwl) {
    EnterCriticalSection(&rwl->cs);
    for (;;) {
        if (rwl->activeWrite == 0 && rwl->waitWrite == 0) {
            rwl->activeRead++;
            LeaveCriticalSection(&rwl->cs);
            return 0;
        }
        if (ResetEvent(rwl->condRead) == 0)
            PANIC;
        rwl->waitRead++;
        LeaveCriticalSection(&rwl->cs);
        if (WaitForSingleObject(rwl->condRead, INFINITE) == WAIT_FAILED)
            PANIC;
        EnterCriticalSection(&rwl->cs);
        rwl->waitRead--;
    }
    return 0;
}

int RWLAcquireWrite(struct RWL *rwl) {
    DWORD myThread = GetCurrentThreadId();
    EnterCriticalSection(&rwl->cs);
    for (;;) {
        if ((rwl->activeWrite == 0 && rwl->activeRead == 0) ||
            (rwl->activeWrite > 0 && rwl->writeThread == myThread)) {
            rwl->activeWrite++;
            rwl->writeThread = myThread;
            LeaveCriticalSection(&rwl->cs);
            return 0;
        }
        rwl->waitWrite++;
        LeaveCriticalSection(&rwl->cs);
        if (WaitForSingleObject(rwl->condWrite, INFINITE) == WAIT_FAILED)
            PANIC;
        EnterCriticalSection(&rwl->cs);
        rwl->waitWrite--;
    }
    return 0;
}

int RWLReleaseRead(struct RWL *rwl) {
    EnterCriticalSection(&rwl->cs);
    rwl->activeRead--;
    return RWLSignal(rwl);
}

int RWLReleaseWrite(struct RWL *rwl) {
    EnterCriticalSection(&rwl->cs);
    rwl->activeWrite--;
    return RWLSignal(rwl);
}
#endif
