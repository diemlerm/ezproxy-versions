#ifndef __ADMINLOGIN_H__
#define __ADMINLOGIN_H__

#include "common.h"

void AdminLogin(struct TRANSLATE *t, char *query, char *post);
BOOL AdminCGILogin(struct TRANSLATE *t, struct USERINFO *uip, char *url, BOOL logup);
void AdminLoginGet(struct TRANSLATE *t, char *url, char *accessType);
BOOL AdminMaybeLogin(struct TRANSLATE *t, struct DISPATCH *dp, char *query, char *post);

#endif  // __ADMINLOGIN_H__
