#ifndef __WSKEYLICENSE_H__
#define __WSKEYLICENSE_H__

#include <openssl/rand.h>
#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/sha.h>

#include <time.h>

#include "ezproxyversion.h"
#include "common.h"
// #include "thread.c"

#define WSKEY_KEYSIZE 512
#define WSKEY_SIGSIZE 512
#define WSKEY_TOKENSIZE 40
#define WSKEY_HASHSIZE 1024
// 33 is for 2 timestamps as strings and extra space for dashes
#define WSKEY_NONCESIZE (WSKEY_KEYSIZE + WSKEY_TOKENSIZE + 33)

/**
 * Various states which wiskey can exist in.
 */
enum WSKEY_STATES {

    // good states
    WSKEY_AUTHORIZED,
    WSKEY_UNAUTHORIZED,

    // starting state, should never end up here
    WSKEY_INIT,

    // signature has been set on the key
    // also means we talked to wskey, signature was good,
    // and the key was authorized
    WSKEY_SIG_SET,

    // error states
    WSKEY_BADXML,
    WSKEY_BADXPATH,
    WSKEY_SERVERDOWN,
    WSKEY_NOT200,
    WSKEY_SOCKETERR,
    WSKEY_NOSIG,
    WSKEY_BADSIG,
    WSKEY_NOPUBKEY,
    WSKEY_CLOCK_ERROR,

    WSKEY_EXPIRED,

    // catchall
    WSKEY_CATCH_ALL
};

enum WSKEY_LICENSE_STATES {
    WSKEY_L_VALID,
    WSKEY_L_EXPIRED_GRACE_PAST,
    WSKEY_L_EXPIRED_GRACE_IN,
    WSKEY_L_BAD_READ,
    WSKEY_L_BAD_SIG
};

/**
 * Holds a representation of wskey license.
 * If any fields made into pointers, a lot of logic must
 * change as current code assumes a pointer to these can just
 * be freed and also that one license can be memcpy'd to another.
 */
struct WSKEYLICENSE {
    int keySize;
    char key[WSKEY_KEYSIZE];

    int sigSize;
    char signature[WSKEY_SIGSIZE];

    int tokenSize;
    char token[WSKEY_TOKENSIZE];

    int nonceSize;
    char nonce[WSKEY_NONCESIZE];

    time_t expiry;
    time_t revalidate;
    time_t nextRevalidateAttempt;
    time_t lastRevalidateAttempt;
    time_t lastRevalidateSuccess;

    BOOL warned;
};

BOOL WskeyReadLicense(struct WSKEYLICENSE *license, BOOL showFileError);

/**
 * Initialize a wskey license.  This will perform
 * an initial validation of the license key.
 */
void WskeyInitialize(struct WSKEYLICENSE *license);

/**
 * Creates a default wskey license, but no key.  Sets dates
 * and times assuming license is a valid license.
 */
struct WSKEYLICENSE *WskeyLicenseCreate(const char *key);

void WskeyBeginThread(void *v);

void GetWskeyKey(char **key);

void WskeyInfo();

void WskeyBetaInfo();
BOOL WskeyIsBeta();
BOOL WskeyIsBetaExpired();

struct WSKEYLICENSE *WskeyActiveLicense();

BOOL isLicenseExpired(struct WSKEYLICENSE *license);

#endif /* WSKEYLICENSE_H_ */
