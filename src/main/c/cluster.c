#define __UUFILE__ "cluster.c"

#include "common.h"
#include <stdarg.h>

static enum CLUSTERSTATE {
    CLUSTERIDLE,
    CLUSTERACQUIRING,
    CLUSTERACQUIRED,
    CLUSTERVOTING,
    CLUSTERDENIED
} clusterState,
    lastState;

int peerTTL = 10;
SOCKET clusterSocket = INVALID_SOCKET;
struct PEER *clusterMutexHolder = NULL;
BOOL clusterHaveMutex = 0;
int peerHello = 3;
int peerStale = 8;
int peerDown = 20;
time_t clusterStartedAcquiring;
time_t clusterLastTriedAcquiring;
time_t clusterStartedVoting;
time_t clusterLastTriedVoting;
time_t clusterLastDenied;
int clusterMsgid = 0;
struct CLUSTERREQUEST *crCurrent = NULL, crLoadRequest;
struct PEERMSG msgLoadRequest;
struct PEER *peerLoadRequest = NULL;

void ClusterRequestRelease(void);

/* EncodeFields return 0 on success, 1 if ran out of space */
int EncodeFields(void *vbuffer, int *remain, ...) {
    va_list ap;
    enum ENCODETYPE et;
    unsigned char *buffer = (unsigned char *)vbuffer;
    unsigned char c;
    unsigned short s;
    unsigned int i;
    unsigned char *p = NULL;
    int len = 0;
    BOOL status = TRUE;

    va_start(ap, remain);
    for (;;) {
        et = va_arg(ap, enum ENCODETYPE);
        if (et == EDSTOP)
            break;
        if (*remain <= 0) {
            status = FALSE;
            break;
        }

        switch (et) {
        case EDCHAR:
            i = va_arg(ap, unsigned int);
            c = i;
            p = &c;
            len = 1;
            break;
        case EDSHORT:
            i = va_arg(ap, unsigned int);
            s = htons((short)i);
            p = (unsigned char *)&s;
            len = 2;
            break;
        case EDINT:
            i = va_arg(ap, unsigned int);
            i = htonl(i);
            p = (unsigned char *)&i;
            len = 4;
            break;
        case EDACHAR:
            p = va_arg(ap, unsigned char *);
            /* Encode null strings as blank strings */
            if (p == NULL)
                p = (unsigned char *)"";
            len = strlen((char *)p) + 1;
            break;
        case EDPCHAR:
            p = va_arg(ap, unsigned char *);
            if (p == NULL) {
                len = 0;
                *buffer++ = 0;
            } else {
                *buffer++ = 1;
                len = strlen((char *)p) + 1;
            }
            *remain -= 1;
            break;
        default:
            break;
        }

        if (len == 0)
            continue;

        if (*remain < len) {
            status = FALSE;
            break;
        }

        memcpy(buffer, p, len);
        buffer += len;
        *remain -= len;
    }
    va_end(ap);

    return status;
}

/* EncodeFields return 0 on success, 1 if ran out of space */
int DecodeFields(void *vbuffer, int *remain, ...) {
    va_list ap;
    enum ENCODETYPE et;
    unsigned char *buffer = (unsigned char *)vbuffer;
    unsigned char *c;
    unsigned short *s = NULL;
    unsigned int *i = NULL;
    unsigned char **pp;
    unsigned char *p = NULL;
    int len = 0;
    BOOL status = TRUE;
    size_t max;

    va_start(ap, remain);
    for (;;) {
        et = va_arg(ap, enum ENCODETYPE);
        if (et == EDSTOP)
            break;
        if (*remain <= 0) {
            status = FALSE;
            break;
        }

        switch (et) {
        case EDCHAR:
            c = va_arg(ap, unsigned char *);
            p = c;
            len = 1;
            break;
        case EDSHORT:
            s = va_arg(ap, unsigned short *);
            p = (unsigned char *)s;
            len = 2;
            break;
        case EDINT:
            i = va_arg(ap, unsigned int *);
            p = (unsigned char *)i;
            len = 4;
            break;
        case EDACHAR:
            p = va_arg(ap, unsigned char *);
            len = strlen((char *)buffer) + 1;
            max = va_arg(ap, size_t);
            if ((int)max < len) {
                StrCpy3((char *)buffer, (char *)p, max);
                goto SkipCopy;
            }
            break;
        case EDPCHAR:
            pp = va_arg(ap, unsigned char **);
            if (*buffer++ == 0) {
                *pp = NULL;
                len = 0;
            } else {
                len = strlen((char *)buffer) + 1;
                if (!(p = *pp = malloc(len)))
                    PANIC;
            }
            *remain -= 1;
            break;
        case EDRCHAR:
            pp = va_arg(ap, unsigned char **);
            *pp = buffer;
            len = strlen((char *)buffer) + 1;
            goto SkipCopy;
            break;
        default:
            break;
        }

        if (len == 0)
            continue;

        if (*remain < len) {
            status = FALSE;
            break;
        }

        memcpy(p, buffer, len);
        if (et == EDSHORT)
            *s = ntohs(*s);
        if (et == EDINT)
            *i = ntohl(*i);

    SkipCopy:
        buffer += len;
        *remain -= len;
        if (*remain < 0) {
            status = FALSE;
            break;
        }
    }
    va_end(ap);

    return status;
}

/*
struct COOKIE {
    struct COOKIE *next;
    char *name;
    char *val;
    // domain used for domain-based cookies, host used for non-domain-based cookies (so one or other
not NULL) char *domain; struct HOST *host; char *path; time_t expires;
*/

BOOL EncodeCookie(struct PEERMSG *msg, struct SESSION *s, struct COOKIE *k) {
    int buflen = MAXCLUSTERDATA;
    int rc;

    rc = EncodeFields(msg->data, &buflen, EDACHAR, s->key, EDPCHAR, k->name, EDPCHAR, k->domain,
                      EDPCHAR, k->path, EDINT, k->expires, EDPCHAR, k->val, EDSTOP);
    msg->datalen = MAXCLUSTERDATA - buflen;
    return rc;
}

void ClusterFreeCookieFields(struct COOKIE *k) {
    FreeThenNull(k->name);
    FreeThenNull(k->domain);
    FreeThenNull(k->path);
    FreeThenNull(k->val);
    memset(k, 0, sizeof(*k));
}

struct COOKIE *DecodeCookie(struct PEERMSG *msg, struct SESSION *s, struct COOKIE *k) {
    /*  struct COOKIE *k; */
    char tempKey[MAXKEY];
    int buflen;

    /*  if (!(k = calloc(1, sizeof(struct COOKIE)))) PANIC; */

    buflen = msg->datalen;
    if (!DecodeFields(msg->data, &buflen, EDACHAR, tempKey, sizeof(tempKey), EDPCHAR, &k->name,
                      EDPCHAR, &k->domain, EDPCHAR, &k->path, EDINT, &k->expires, EDPCHAR, &k->val,
                      EDSTOP)) {
        ClusterFreeCookieFields(k);
        return NULL;
    }

    return k;
}

/*
struct SESSION {
    struct COOKIE *cookies; //
    char *genericUser;
    char *logUser;
    struct in_addr sin_addr; // RO
    time_t created; // RO
    time_t accessed; // UA
    GROUPMASK gm;
    int lifetime;
    char key[MAXKEY];
    char active;
    char priv;
    char expirable;
    char notify;
*/

BOOL EncodeSession(struct PEERMSG *msg, struct SESSION *s) {
    int buflen = MAXCLUSTERDATA;
    int rc;

    rc = EncodeFields(msg->data, &buflen, EDPCHAR, s->genericUser, EDPCHAR, s->logUserFull, EDINT,
                      s->created, EDINT, s->accessed, EDINT, s->gm, EDINT, s->lifetime, EDACHAR,
                      s->key, EDCHAR, (int)s->priv, EDCHAR, (int)s->expirable, EDCHAR,
                      (int)s->notify, EDSTOP);
    msg->datalen = MAXCLUSTERDATA - buflen;
    return rc;
}

void FreeSessionFields(struct SESSION *s) {
    FreeThenNull(s->genericUser);
    FreeThenNull(s->logUserFull);
    s->logUserFull = s->logUserBrief = NULL;
    FreeThenNull(s->loginBanner);
    memset(s, 0, sizeof(*s));
}

struct SESSION *DecodeSession(struct PEERMSG *msg, struct SESSION *s) {
    /*  struct SESSION *s; */
    int buflen;

    /* if (!(s = calloc(1, sizeof(struct SESSION)))) PANIC; */
    buflen = msg->datalen;

    if (!DecodeFields(msg->data, &buflen, EDPCHAR, &s->genericUser, EDPCHAR, &s->logUserFull, EDINT,
                      &s->created, EDINT, &s->accessed, EDINT, &s->gm, EDINT, &s->lifetime, EDACHAR,
                      s->key, sizeof(s->key), EDCHAR, &s->priv, EDCHAR, &s->expirable, EDCHAR,
                      &s->notify, EDSTOP)) {
        FreeSessionFields(s);
        return NULL;
    }

    s->active = 1;
    return s;
}

BOOL EncodeHost(struct PEERMSG *msg, struct HOST *h) {
    int buflen = MAXCLUSTERDATA;
    int rc;

    rc = EncodeFields(msg->data, &buflen, EDPCHAR, h->hostname, EDPCHAR,
                      "", /* was h->validate, but that has a totally different appearance now */
                      EDINT, h->created, EDINT, h->referenced, EDINT, h->accessed, EDSHORT,
                      (int)h->remport, EDSHORT, (int)h->myPort, EDINT, h->javaScript, EDSTOP);
    msg->datalen = MAXCLUSTERDATA - buflen;
    return rc;
}

void ClusterFreeHostFields(struct HOST *h) {
    FreeThenNull(h->hostname);
    /* If this was for real, a lot more work would be needed */
    FreeThenNull(h->validate);
    memset(h, 0, sizeof(*h));
}

struct HOST *DecodeHost(struct PEERMSG *msg, struct HOST *h) {
    /*  struct HOST *h; */
    /*  if (!(h = calloc(1, sizeof(struct HOST)))) PANIC; */
    int buflen;
    char *fakeValidate;

    buflen = msg->datalen;

    memset(h, 0, sizeof(*h));
    if (!DecodeFields(msg->data, &buflen, EDPCHAR, &h->hostname, EDPCHAR,
                      &fakeValidate, /* &h->validate, */
                      EDINT, &h->created, EDINT, &h->referenced, EDINT, &h->accessed, EDSHORT,
                      &h->remport, EDSHORT, &h->myPort, EDINT, &h->javaScript, EDSTOP)) {
        ClusterFreeHostFields(h);
        return NULL;
    }

    return h;
}

struct CLUSTERREQUEST *crHead = NULL, *crTail = NULL;

int StartUdpSocket(SOCKET *s, PORT port) {
    int rc;
    int one = 1;
    struct sockaddr_storage sl;

    *s = MoveFileDescriptorHigh(socket(UUDEFAULT_FAMILY, SOCK_DGRAM, 0));
    if (*s == INVALID_SOCKET) {
        goto Error;
    }
    if (debugLevel > 5)
        Log("%" SOCKET_FORMAT " Opened socket, errno=%d", *s, (*s == INVALID_SOCKET) ? errno : 0);

    if (setsockopt(*s, SOL_SOCKET, SO_REUSEADDR, (char *)&one, sizeof(one)))
        goto Error;

    init_storage(&sl, UUDEFAULT_FAMILY, TRUE, port);
    if (bind(*s, (struct sockaddr *)&sl, get_size(&sl)))
        goto Error;

    return 0;

Error:
    rc = socket_errno;
    if (*s != INVALID_SOCKET) {
        closesocket(*s);
        *s = INVALID_SOCKET;
    }
    return rc;
}

void ClusterBad(char *rtn, struct PEER *c, struct PEERMSG *msg) {
    int len;

    if (debugLevel == 0)
        return;

    if (msg->datalen > 10)
        len = 10;
    else
        len = msg->datalen;

    msg->data[len] = 0;
    Log("Invalid cluster msg from %s:%d of %s: %c %c %d: %s", c->peerName,
        get_port(&c->peerAddress), rtn, msg->type, msg->cmd, msg->msgid, msg->data);
}

int ClusterSendRawMsg(struct PEER *c, struct PEERMSG *msg) {
    int msglen;
    int rc;
    short datalen;

    datalen = msg->datalen;
    if (datalen > MAXCLUSTERDATA)
        datalen = MAXCLUSTERDATA;
    msglen = PEERMSGHDRSIZE + datalen;
    msg->datalen = htons(datalen);
    msg->version = htons(CLUSTERVERSION);
    if (msg->msgid == 0)
        msg->msgid = htonl(++clusterMsgid);
    rc = sendto(clusterSocket, (char *)msg, msglen, 0, (struct sockaddr *)&c->peerAddress,
                sizeof(c->peerAddress));
    if (rc == SOCKET_ERROR) {
        Log("ClusterSendRawMsg sendto failed: %d", socket_errno);
    }
    msg->datalen = datalen;
    return rc;
}

void ClusterSendMsg(struct PEER *c, struct PEERMSG *msg) {
    int msglen;

    /*  if (msg->cmd != 'T')
            printf("Send msg to %s: %c %c %c\n", c->peerName, msg->type, msg->cmd, msg->data[0]); */
    ClusterSendRawMsg(c, msg);
    if (msg->type == 'A') {
        /* If we are not retransmitting the same message, spruce it up */
        if (msg != &c->smsg) {
            msglen = PEERMSGHDRSIZE + msg->datalen;
            if (msglen >= sizeof(c->smsg))
                msglen = sizeof(c->smsg);
            memcpy((char *)&c->smsg, (char *)msg, msglen);
            c->sAck = PAWAITING;
        }
        UUtime(&c->lastSentTo);
    }
}

/* This isn't normally needed, since CheckPeers retransmits every second */
void ClusterResendUnacked(void) {
    int i;
    struct PEER *c;

    for (i = 1, c = peers + 1; i < cPeers; i++, c++)
        if (c->sAck == PAWAITING)
            if (c->peerStatus >= PEERUP)
                ClusterSendRawMsg(c, &c->smsg);
            else
                c->sAck = PADIED;
}

void ClusterSendResetAck(void) {
    int i;
    struct PEER *c;

    for (i = 1, c = peers + 1; i < cPeers; i++, c++) {
        c->sAck = PANOTWAITING;
        c->smsg.msgid = 0;
    }
}

BOOL ClusterAllAcked(void) {
    struct PEER *c;
    int i;

    for (i = 1, c = peers + 1; i < cPeers; i++, c++) {
        if (c->sAck == PAWAITING) {
            if (debugLevel > 8)
                printf("still waiting on at least %s\n", c->peerName);
            return FALSE;
        }
    }
    return TRUE;
}

void ClusterSendMsgAll(struct PEERMSG *msg) {
    int i;
    struct PEER *c;

    for (i = 1, c = peers + 1; i < cPeers; i++, c++)
        ClusterSendMsg(c, msg);
}

void ClusterSendSimpleMsg(struct PEER *c, char *simple, int msgid) {
    struct PEERMSG msg;
    short len;

    msg.type = *simple;
    msg.cmd = *(simple + 1);
    msg.msgid = msgid;
    len = strlen(simple) + 1;
    if (len <= 2)
        msg.datalen = 0;
    else {
        len -= 2;
        if (len > MAXCLUSTERDATA)
            len = MAXCLUSTERDATA;
        msg.datalen = len;
        StrCpy3(msg.data, simple + 2, len);
    }
    if (c == NULL)
        ClusterSendMsgAll(&msg);
    else
        ClusterSendMsg(c, &msg);
}

void ClusterMsgCookie(struct SESSION *e,
                      char *name,
                      char *domain,
                      char *path,
                      time_t expires,
                      struct PEERMSG *msg) {
    int remain;

    ClusterInitMsg(msg, 'A', 'C', 0);
    remain = sizeof(msg->data);
    EncodeFields(msg->data, &remain, EDACHAR, e->key, EDACHAR, name, EDACHAR, domain, EDACHAR, path,
                 EDINT, expires, EDSTOP);
    msg->datalen = sizeof(msg->data) - remain;
}

void ClusterMsgSession(struct SESSION *e, struct PEERMSG *msg) {
    int remain;

    ClusterInitMsg(msg, 'A', 'S', 0);
    remain = sizeof(msg->data);
    EncodeFields(msg->data, &remain, EDCHAR, (int)'S', EDACHAR, e->key, EDACHAR, e->genericUser,
                 EDACHAR, e->logUserFull, EDINT, e->created, EDINT, e->accessed, EDINT, e->gm,
                 EDINT, e->lifetime, EDCHAR, (int)e->priv, EDCHAR, (int)e->expirable, EDCHAR,
                 (int)e->notify, EDSTOP);
    msg->datalen = sizeof(msg->data) - remain;
}

void ClusterUpdateSession(struct SESSION *e) {
    struct PEERMSG msg;

    if (cPeers == 0)
        return;

    ClusterMsgSession(e, &msg);
    ClusterRequest(&msg);
}

BOOL ClusterMutexAcquired(void) {
    int i;
    struct PEER *c;

    for (i = 1, c = peers + 1; i < cPeers; i++, c++) {
        if (c->peerStatus >= PEERUP)
            continue;
        if (c->pmstate == 'Y')
            return TRUE;
        if (c->pmstate == 'D')
            return FALSE;
        if (c->pmstate == 0) {
            if (debugLevel > 3)
                printf("still waiting on at least %s\n", c->peerName);
            return FALSE;
        }
    }
    return TRUE;
}

BOOL ClusterMutexVoted(void) {
    int i;
    struct PEER *c;

    for (i = 1, c = peers + 1; i < cPeers; i++, c++) {
        if (c->peerStatus >= PEERUP)
            continue;
        if (c->pmstate != 'Y')
            return FALSE;
    }
    return TRUE;
}

void ClusterMutexAcquire(void) {
    int i;
    struct PEER *c;
    BOOL any;

    clusterState = CLUSTERACQUIRED;
    if (clusterHaveMutex)
        return;

    for (any = 0, i = 1, c = peers + 1; i < cPeers; i++, c++) {
        c->pmstate = 0;
        if (c->peerStatus >= PEERUP)
            clusterState = CLUSTERACQUIRING;
    }

    if (clusterState == CLUSTERACQUIRED)
        return;

    ClusterSendSimpleMsg(NULL, "AMS", 0);
    UUtime(&clusterStartedAcquiring);
    clusterLastTriedAcquiring = clusterStartedAcquiring;
}

void ClusterMutexRelease(void) {
    ClusterSendSimpleMsg(NULL, "IMA", 0);
    clusterState = CLUSTERIDLE;
}

void ClusterRequest(struct PEERMSG *msg) {
    struct CLUSTERREQUEST cr;

    if (cPeers == 0)
        return;

    cr.msg = msg;
    cr.processed = 0;
    cr.next = 0;
    cr.phase = 0;

#ifdef WIN32
    cr.condVar = CreateSemaphore(NULL, 0, 1, NULL);
    if (cr.condVar == NULL)
        PANIC;
#else
    if (pthread_cond_init(&cr.condVar, NULL) != 0)
        PANIC;
#endif

    UUAcquireMutex(&mCluster);

    if (crHead == NULL) {
        crHead = crTail = &cr;
    } else {
        crTail->next = &cr;
        crTail = &cr;
    }
    /* Send a NULL message looped back to ourselves to flag the cluster thread
       to look for work.
    */
    if (clusterSocket != INVALID_SOCKET)
        sendto(clusterSocket, "", 0, 0, (struct sockaddr *)&peers->peerAddress,
               sizeof(peers->peerAddress));

#ifdef WIN32
    UUReleaseMutex(&mCluster);
    if (WaitForSingleObject(cr.condVar, INFINITE) == WAIT_FAILED)
        PANIC;
    CloseHandle(cr.condVar);
#else
    for (;;) {
        if (pthread_cond_wait(&cr.condVar, &mCluster.mutex) != 0)
            PANIC;
        if (cr.processed)
            break;
    }
    UUReleaseMutex(&mCluster);
    pthread_cond_destroy(&cr.condVar);
#endif
    /* printf("done\n"); */
}

struct PEER *FindPeerByAddress(struct sockaddr_storage *addr) {
    int i;
    struct PEER *c;

    for (i = 0, c = peers; i < cPeers; i++, c++)
        if (is_ip_equal(addr, &c->peerAddress) && get_port(addr) == get_port(&c->peerAddress))
            return c;
    return NULL;
}

void ClusterInitMsg(struct PEERMSG *msg, char type, char cmd, int msgid) {
    memset(msg, 0, sizeof(*msg));
    msg->type = type;
    msg->cmd = cmd;
    msg->msgid = msgid;
}

void ClusterHello(struct PEER *c) {
    struct PEERMSG msg;

    ClusterInitMsg(&msg, 'I', 'T', 0);
    msg.datalen = 2;
    msg.data[0] = (peers->peerStatus == PEERUP ? 'U' : 'B');
    msg.data[1] = 0;
    ClusterSendMsg(c, &msg);
}

void ClusterHellos(void) {
    static time_t nextHello = 0;
    time_t now;
    int i;
    struct PEER *c;

    UUtime(&now);
    if (nextHello > now)
        return;
    for (i = 1, c = peers + 1; i < cPeers; i++, c++)
        ClusterHello(c);
    nextHello = now + peerHello;
}

void ClusterCheckPeers(void) {
    static time_t startup = 0;
    time_t now, stale, down;
    int i;
    struct PEER *c;
    enum PEERSTATUS saveStatus;
    struct PEER *up;
    BOOL unknowns;
    static BOOL first = 1;

    UUtime(&now);
    if (startup == 0)
        startup = now;
    stale = now - peerStale;
    down = now - peerDown;
    up = NULL;
    unknowns = 0;

    for (i = 1, c = peers + 1; i < cPeers; i++, c++) {
        saveStatus = c->peerStatus;
        switch (c->peerStatus) {
        case PEERDOWN:
            unknowns++;
            break;

        case PEERSTARTING:
            if (c->lastHeardFrom < down) {
                c->peerStatus = PEERDOWN;
                break;
            }
            break;

        case PEERUP:
            if (c->lastHeardFrom < stale) {
                c->peerStatus = PEERSTALE;
                break;
            }
            if (peers->peerStatus < PEERUP)
                ClusterSendSimpleMsg(c, "RL", 0);
            if (up == NULL) {
                up = c;
            }
            break;

        case PEERSTALE:
            unknowns++;
            if (c->lastHeardFrom < down)
                c->peerStatus = PEERDOWN;
            break;

        default:
            break;
        }

        if (saveStatus != c->peerStatus)
            printf("%s:%d changed to %s\n", c->peerName, get_port(&c->peerAddress),
                   peerStatusName[c->peerStatus]);

        if (c->smsg.type == 'A' && c->sAck == PAWAITING)
            if (c->peerStatus >= PEERUP)
                ClusterSendMsg(c, &c->smsg);
            else
                c->sAck = PADIED;
    }

    if (peers->peerStatus != PEERSTARTING)
        return;
    if (up != NULL) {
        /*      peers->peerStatus = PEERUP; */
        ClusterHellos();
        /*      ClusterSendSimpleMsg(up, "AL", 0); */
        return;
    }
    if (unknowns > 0 && startup + 10 > now)
        return;
    if (first) {
        printf("I must now propose to become the master\n");
        peers->peerStatus = PEERUP;
        ClusterHellos();
        first = 0;
    }
}

void ClusterCmdCookie(struct PEER *c, struct PEERMSG *msg) {
    int remain;
    char *key;
    char *name = NULL;
    char *domain = NULL;
    char *path = NULL;
    time_t expires = 0;
    struct SESSION *e;

    if (msg->type != 'A')
        return;

    remain = msg->datalen;
    if (!DecodeFields(msg->data, &remain, EDRCHAR, &key, EDRCHAR, &name, EDRCHAR, &domain, EDRCHAR,
                      &path, EDINT, &expires, EDSTOP)) {
        if (c) {
            ClusterSendSimpleMsg(c, "NC", msg->msgid);
            ClusterBad("ClusterCmdCookie", c, msg);
        }
        return;
    }

    e = FindSessionByKey(key);
    if (e == NULL) {
        Log("Received cookie request for non-existent session %s", key);
        if (c)
            ClusterSendSimpleMsg(c, "NC", msg->msgid);
    } else {
        ClusterAddCookie(e, name, domain, path, expires);
        if (c)
            ClusterSendSimpleMsg(c, "YC", msg->msgid);
    }
}

void ClusterCmdHost(struct PEER *c, struct PEERMSG *msg) {
    int remain;
    char *host;
    PORT remport;
    PORT myPort;
    char useSsl;
    struct HOST *h;

    if (msg->type == 'A') {
        remain = msg->datalen;
        if (!DecodeFields(msg->data, &remain, EDRCHAR, &host, EDSHORT, &remport, EDSHORT, &myPort,
                          EDCHAR, &useSsl, EDSTOP)) {
            ClusterSendSimpleMsg(c, "YH", msg->msgid);
            ClusterBad("ClusterCmdHostDecode", c, msg);
            return;
        }
        h = ClusterFindHost(host, remport, useSsl, 0, 1);
        if (h == NULL || h->myPort != myPort) {
            Log("Failed to assign same port for %s:%d (got %d, mine %d)", host, remport, myPort,
                (h == NULL ? 0 : h->myPort));
        }
        ClusterSendSimpleMsg(c, "YH", msg->msgid);
        return;
    }
}

void ClusterCmdLoad(struct PEER *c, struct PEERMSG *msg) {
    if (msg->type == 'R') {
        if (peerLoadRequest == NULL)
            peerLoadRequest = c;
        else if (peerLoadRequest != c) {
            ClusterSendSimpleMsg(c, "DL", msg->msgid);
            return;
        }
        if (c->peerStatus == PEERDOWN)
            c->peerStatus = PEERSTARTING;
        ClusterSendSimpleMsg(c, "QL", msg->msgid);
    }

    if (msg->type == 'E') {
        peers->peerStatus = PEERUP;
        ClusterHellos();
        return;
    }

    if (msg->type == 'N') {
        if (peerLoadRequest == c) {
            peerLoadRequest = NULL;
            if (crCurrent == &crLoadRequest)
                ClusterRequestRelease();
        }
    }
}

void ClusterCmdMutexAvailable(struct PEER *c, struct PEERMSG *msg) {
    if (clusterState == CLUSTERDENIED) {
        clusterState = CLUSTERIDLE;
        printf("out of denial\n");
    }
}

void ClusterCmdMutexStart(struct PEER *c, struct PEERMSG *msg) {
    int i;
    struct PEER *pc;

    /* Types can be A for want to acquire, Y for yes you can have it, N for no you can't, or
       U for unknown as to who has it */

    if (msg->type == 'A') {
        if (clusterHaveMutex) {
            if (clusterState == CLUSTERIDLE) {
                ClusterSendSimpleMsg(c, "YMS", msg->msgid);
                clusterHaveMutex = 0;
                /* printf("Relinquish mutex to %s\n", c->peerName);  */
                return;
            }
            ClusterSendSimpleMsg(c, "NMS", msg->msgid);
            return;
        }

        /* Indicate that we don't have the mutex */
        ClusterSendSimpleMsg(c, "UMS", msg->msgid);
        return;
    }

    if (clusterState != CLUSTERACQUIRING || msg->msgid != c->smsg.msgid ||
        (msg->type != 'Y' && msg->type != 'N' && msg->type != 'U')) {
        if (msg->msgid != c->smsg.msgid)
            printf("Expected id %d, got %d\n", ntohl(c->smsg.msgid), ntohl(msg->msgid));
        ClusterBad("ClusterCmdMutexStart", c, msg);
        return;
    }

    c->pmstate = msg->type;

    if (msg->type == 'Y' && clusterState == CLUSTERACQUIRING) {
        /* printf("%s grants mutex\n", c->peerName); */
        ClusterSendResetAck();
        clusterHaveMutex = 1;
        if (clusterState == CLUSTERACQUIRING)
            clusterState = CLUSTERACQUIRED;
        return;
    }

    if (msg->type == 'N') {
        printf("%s denies mutex %d\n", c->peerName, ntohl(msg->msgid));
        ClusterSendResetAck();
        clusterState = CLUSTERDENIED;
        UUtime(&clusterLastDenied);
        return;
    }

    for (i = 1, pc = peers + 1; i < cPeers; i++, pc++)
        if (pc->peerStatus >= PEERUP)
            if (pc->pmstate == 0)
                return;

    /* If we reach here, no one claims to have the mutex, so try to get it */
    printf("Vote ");
    for (i = 1, pc = peers + 1; i < cPeers; i++, pc++)
        pc->pmstate = 0;

    clusterState = CLUSTERVOTING;
    ClusterSendSimpleMsg(NULL, "AMV", 0);
    UUtime(&clusterStartedVoting);
    clusterLastTriedVoting = clusterStartedVoting;
}

void CluserCmdMutexVoting(struct PEER *c, struct PEERMSG *msg) {
    int i;
    struct PEER *pc;

    if (msg->type == 'A') {
        if (clusterState != CLUSTERVOTING) {
            ClusterSendSimpleMsg(c, (clusterHaveMutex ? "NMV" : "YMV"), msg->msgid);
            return;
        }
        /* We're running for election, if we have the lower address, deny the other */
        if (compare_ip(&peers->peerAddress, &c->peerAddress) > 0 &&
            get_port(&peers->peerAddress) > get_port(&c->peerAddress)) {
            ClusterSendSimpleMsg(c, "NMV", msg->msgid);
            return;
        }
        printf("Concede :(\n");
        /* We're the higher address, so throw in the towel */
        ClusterSendSimpleMsg(c, "YMV", msg->msgid);
        ClusterMutexAcquire();
    }

    if (clusterState != CLUSTERVOTING)
        return;

    if (msg->msgid != c->smsg.msgid || (msg->type != 'Y' && msg->type != 'N')) {
        ClusterBad("ClusterCmdMutexVoting", c, msg);
        return;
    }

    if (msg->type == 'N') {
        printf("Lose ");
        ClusterMutexAcquire();
        return;
    }

    c->pmstate = 'Y';
    for (i = 1, pc = peers + 1; i < cPeers; i++, pc++)
        if (pc->peerStatus >= PEERUP)
            if (pc->pmstate != 'Y')
                return;

    printf("Win ");
    ClusterSendResetAck();
    clusterHaveMutex = 1;
    clusterState = CLUSTERACQUIRED;
}

void ClusterCmdMutex(struct PEER *c, struct PEERMSG *msg) {
    if (msg->data[0] == 'A')
        ClusterCmdMutexAvailable(c, msg);
    else if (msg->data[0] == 'S')
        ClusterCmdMutexStart(c, msg);
    else if (msg->data[0] == 'V')
        CluserCmdMutexVoting(c, msg);
    else
        ClusterBad("ClusterCmdMutex", c, msg);
}

void ClusterCmdSessionEnd(struct PEER *c, struct PEERMSG *msg) {
    struct SESSION *e;
    int remain;
    char *key;
    char dummy;

    if (msg->type == 'Y')
        return;
    if (msg->type != 'A') {
        ClusterBad("ClusterCmdSessionEnd", c, msg);
        return;
    }

    remain = msg->datalen;
    if (!DecodeFields(msg->data, &remain, EDCHAR, &dummy, EDRCHAR, &key, EDSTOP)) {
        ClusterSendSimpleMsg(c, "NSE", msg->msgid);
        ClusterBad("ClusterCmdSessionStartDecode", c, msg);
        return;
    }
    if (e = FindSessionByKey(key))
        ClusterStopSession(e, "Cluster", 1);
    ClusterSendSimpleMsg(c, "YSE", msg->msgid);
}

int strnullcmp(const char *s1, const char *s2) {
    if (s1 == NULL)
        s1 = "";
    if (s2 == NULL)
        s2 = "";
    return strcmp(s1, s2);
}

int strinullcmp(const char *s1, const char *s2) {
    if (s1 == NULL)
        s1 = "";
    if (s2 == NULL)
        s2 = "";
    return stricmp(s1, s2);
}

void ClusterCmdSessionStart(struct PEER *c, struct PEERMSG *msg) {
    int remain;
    char dummy;
    char *key;
    char *genericUser;
    char *logUserFull;
    time_t created, accessed;
    GROUPMASK gm;
    int lifetime;
    char priv, expirable, notify;
    struct SESSION *e;

    if (msg->type == 'Y' || msg->type == 'N')
        return;

    if (msg->type != 'A') {
        ClusterBad("ClusterCmdSessionStart", c, msg);
        return;
    }

    remain = sizeof(msg->data);
    if (!DecodeFields(msg->data, &remain, EDCHAR, &dummy, EDRCHAR, &key, EDRCHAR, &genericUser,
                      EDRCHAR, &logUserFull, EDINT, &created, EDINT, &accessed, EDINT, &gm, EDINT,
                      &lifetime, EDCHAR, &priv, EDCHAR, &expirable, EDCHAR, &notify, EDSTOP)) {
        ClusterSendSimpleMsg(c, "NSS", msg->msgid);
        ClusterBad("ClusterCmdSessionStartDecode", c, msg);
        return;
    }
    e = ClusterStartSession((*logUserFull == 0 ? NULL : logUserFull), 0, key, 1);
    if (e == NULL) {
        Log("Couldn't create cluster matching session");
        return;
    }
    if (strnullcmp(e->genericUser, genericUser) != 0) {
        FreeThenNull(e->genericUser);
        if (genericUser && *genericUser) {
            if (!(e->genericUser = strdup(genericUser)))
                PANIC;
        }
    }

    if (strnullcmp(e->logUserFull, logUserFull) != 0) {
        StoreLogUser(e, logUserFull);
    }

    if (e->created != created)
        e->created = created;

    if (e->accessed < accessed)
        e->accessed = accessed;

    e->lifetime = lifetime;
    GroupMaskOr(e->gm, gm);
    e->priv = priv;
    e->sessionFlags = 0;
    e->expirable = expirable;
    e->notify = notify;

    NeedSave();

    ClusterSendSimpleMsg(c, "YSS", msg->msgid);
}

void ClusterCmdSession(struct PEER *c, struct PEERMSG *msg) {
    if (msg->data[0] == 'E')
        ClusterCmdSessionEnd(c, msg);
    else if (msg->data[0] == 'S')
        ClusterCmdSessionStart(c, msg);
    else
        ClusterBad("ClusterCmdSession", c, msg);
}

void ClusterCmdTransition(struct PEER *c, struct PEERMSG *msg) {
    enum PEERSTATUS newStatus;

    newStatus = c->peerStatus;
    if (msg->data[0] == 'B')
        newStatus = PEERSTARTING;
    if (msg->data[0] == 'U')
        newStatus = PEERUP;
    if (c->peerStatus == newStatus)
        return;

    c->peerStatus = newStatus;
    printf("Hey! %s:%d is changing to %s\n", c->peerName, get_port(&c->peerAddress),
           peerStatusName[c->peerStatus]);
    ClusterHello(c);
}

void ClusterRequestStart(void) {
    static int requests = 0;

    if (peerLoadRequest != NULL) {
        ClusterInitMsg(&msgLoadRequest, 'A', 'L', 0);
        crLoadRequest.msg = &msgLoadRequest;
        crLoadRequest.processed = 0;
        crLoadRequest.next = 0;
        crLoadRequest.phase = 0;
        crCurrent = &crLoadRequest;
        return;
    }

    UUAcquireMutex(&mCluster);
    if (crHead == NULL) {
        UUReleaseMutex(&mCluster);
        ClusterMutexRelease();
        return;
    }
    /* printf("Handle request\n"); */
    crCurrent = crHead;
    UUReleaseMutex(&mCluster);
    if (debugLevel > 3) {
        printf("*");
        fflush(stdout);
    }
    requests++;
    if (requests % 100 == 0)
        printf("%d ", requests);
}

void ClusterRequestRelease(void) {
    struct CLUSTERREQUEST *crSave;

    if (crCurrent == &crLoadRequest) {
        crCurrent = NULL;
        peerLoadRequest = NULL;
        ClusterMutexRelease();
        return;
    }

    crCurrent = NULL;

    UUAcquireMutex(&mCluster);
    if (crHead == NULL) {
        UUReleaseMutex(&mCluster);
        ClusterMutexRelease();
        return;
    }

    if (debugLevel > 3)
        printf("Releasing request %c\n", (crHead->msg)->cmd);
    crSave = crHead;
    crHead = crHead->next;
    if (crHead == NULL)
        crTail = NULL;
    crSave->processed = 1;
#ifdef WIN32
    if (ReleaseSemaphore(crSave->condVar, 1, NULL) == 0)
        PANIC;
#else
    if (pthread_cond_signal(&crSave->condVar) != 0)
        PANIC;
#endif
    /* We release then reacquire in this loop so the Posix version will be able to acquire
       the mutex and be able to proceed
    */
    UUReleaseMutex(&mCluster);
    ClusterMutexRelease();
}

void ClusterRequestCookie(struct PEERMSG *msg) {
    if (crCurrent->phase == 0) {
        ClusterCmdCookie(NULL, msg);
        ClusterSendResetAck();
        ClusterSendMsgAll(msg);
        crCurrent->phase = 1;
    }
    if (ClusterAllAcked()) {
        ClusterSendResetAck();
        ClusterRequestRelease();
    }
}

void ClusterRequestHost(struct PEERMSG *msg) {
    struct HOST *h;
    char *host;
    PORT remport;
    char useSsl;
    int remain;

    if (crCurrent->phase == 0) {
        remain = msg->datalen;
        DecodeFields(msg->data, &remain, EDRCHAR, &host, EDSHORT, &remport, EDCHAR, &useSsl,
                     EDSTOP);
        h = ClusterFindHost(host, remport, useSsl, 0, 1);
        if (h == NULL) {
            ClusterRequestRelease();
            return;
        }
        /* Add the assigned port number into the request */
        remain = sizeof(msg->data) - msg->datalen;
        EncodeFields(msg->data + msg->datalen, &remain, EDSHORT, (int)h->myPort, EDSTOP);
        msg->datalen = sizeof(msg->data) - remain;
        ClusterSendResetAck();
        ClusterSendMsgAll(msg);
        crCurrent->phase = 1;
    } else {
        if (ClusterAllAcked()) {
            ClusterSendResetAck();
            ClusterRequestRelease();
        }
    }
}

void ClusterRequestLoad(struct PEERMSG *msg) {
    static struct HOST *h;
    static struct SESSION *e;
    static struct COOKIE *k;
    static int i;
    struct PEERMSG smsg;
    int remain;

    /* If we stop hearing from the load host, give up on it and end this request */
    if (peerLoadRequest->peerStatus == PEERDOWN) {
        ClusterRequestRelease();
        return;
    }

    if (crCurrent->phase == 0) {
        printf("Phase 1 load to %s\n", peerLoadRequest->peerName);
        ClusterSendResetAck();
        ClusterSendSimpleMsg(peerLoadRequest, "AL", 0);
        crCurrent->phase = 1;
    }

    if (crCurrent->phase == 1) {
        if (peerLoadRequest->sAck != PAWAITING) {
            printf("Phase 2 load to %s\n", peerLoadRequest->peerName);
            crCurrent->phase = 2;
            h = hosts + 1;
            i = 1;
        }
    }

Phase2:
    if (crCurrent->phase == 2) {
        if (i == cHosts) {
            e = sessions;
            i = 0;
            crCurrent->phase = 4;
            goto Phase4;
        }

        ClusterInitMsg(&smsg, 'A', 'H', 0);
        remain = sizeof(smsg.data);
        EncodeFields(smsg.data, &remain, EDACHAR, h->hostname, EDSHORT, (int)h->remport, EDSHORT,
                     (int)h->myPort, EDSTOP);
        smsg.datalen = sizeof(smsg.data) - remain;
        ClusterSendMsg(peerLoadRequest, &smsg);
        crCurrent->phase = 3;
    }

    if (crCurrent->phase == 3) {
        if (peerLoadRequest->sAck != PAWAITING) {
            h++;
            i++;
            crCurrent->phase = 2;
            goto Phase2;
        }
    }

Phase4:
    if (crCurrent->phase == 4) {
        if (i == cSessions) {
            ClusterSendResetAck();
            ClusterSendSimpleMsg(peerLoadRequest, "EL", 0);
            crCurrent->phase = 8;
            return;
        }

        ClusterMsgSession(e, &smsg);
        ClusterSendResetAck();
        ClusterSendMsg(peerLoadRequest, &smsg);
        crCurrent->phase = 5;
        return;
    }

    if (crCurrent->phase == 5) {
        if (peerLoadRequest->sAck != PAWAITING) {
            k = e->cookies;
            crCurrent->phase = 6;
        }
    }

Phase6:
    if (crCurrent->phase == 6) {
        if (k == NULL) {
            e++;
            i++;
            crCurrent->phase = 4;
            goto Phase4;
        }
        ClusterMsgCookie(e, k->name, k->domain, k->path, k->expires, &smsg);
        ClusterSendResetAck();
        ClusterSendMsg(peerLoadRequest, &smsg);
        crCurrent->phase = 7;
        return;
    }

    if (crCurrent->phase == 7) {
        if (peerLoadRequest->sAck != PAWAITING) {
            k = k->next;
            crCurrent->phase = 6;
            goto Phase6;
        }
    }

    if (crCurrent->phase == 8) {
        if (peerLoadRequest->sAck != PAWAITING) {
            ClusterRequestRelease();
        }
    }
}

void ClusterRequestSession(struct PEERMSG *msg) {
    if (crCurrent->phase == 0) {
        ClusterSendResetAck();
        ClusterSendMsgAll(msg);
        crCurrent->phase = 1;
    } else {
        if (ClusterAllAcked()) {
            ClusterSendResetAck();
            ClusterRequestRelease();
        }
    }
}

void ClusterRequestProcess(void) {
    struct PEERMSG *msg;

    if (crCurrent == NULL) {
        ClusterMutexRelease();
        return;
    }

    msg = crCurrent->msg;
    if (msg == NULL) {
        Log("Null cluster request ignored");
        ClusterRequestRelease();
        return;
    }

    switch (msg->cmd) {
    case 'C':
        ClusterRequestCookie(msg);
        break;
    case 'H':
        ClusterRequestHost(msg);
        break;
    case 'L':
        ClusterRequestLoad(msg);
        break;
    case 'S':
        ClusterRequestSession(msg);
        break;
    default:
        Log("Unknown cluster request ignored: %c", msg->cmd);
        ClusterRequestRelease();
        break;
    }
}

void DoCluster(void *v) {
    int rc;
    struct sockaddr_storage sf;
    socklen_t_M sfl;
    struct PEERMSG msg;
    int msglen;
#ifdef USEPOLL
    struct pollfd pfds;
#else
    fd_set sfds;
    struct timeval tv;
#endif
    struct PEER *c;
    time_t now, last;
    char ipBuffer[INET6_ADDRSTRLEN];

    ChargeAlive(GCCLUSTER, &now);
    if (rc = StartUdpSocket(&clusterSocket, get_port(&peers->peerAddress))) {
        Log("Unable to create cluster socket: %d", rc);
        ChargeStopped(GCCLUSTER, NULL);
        exit(1);
    }
    peers->peerStatus = PEERSTARTING;
    SocketNonBlocking(clusterSocket);
    ClusterHellos();
    clusterState = CLUSTERIDLE;
    last = 0;
    for (;;) {
        ChargeAlive(GCCLUSTER, &now);
        ticksCluster++;
        /*
        if (lastHaveMutex != clusterHaveMutex) {
            printf(clusterHaveMutex ? "+" : "-");
            lastHaveMutex = clusterHaveMutex;
        }
        if (lastState != clusterState) {
            printf("**%d%d ", clusterState, clusterHaveMutex);
            lastState = clusterState;
        }
        */

        /* Technically, it's possible to go from idle, to acquiring, perhaps a stop in
           voting, then to acquired right in a row if we are the only node up, so it's good
           to keep these checks in this sequence
        */
        if (clusterState == CLUSTERIDLE) {
            if ((crHead != NULL || peerLoadRequest != NULL) && peers->peerStatus == PEERUP) {
                /* printf("Somebody's waiting, acquire mutex\n"); */
                ClusterMutexAcquire();
            }
        }

        if (clusterState == CLUSTERACQUIRING) {
            if (clusterLastTriedAcquiring != now) {
                /* This routine checks to see if the peers we are waiting on died */
                if (ClusterAllAcked()) {
                    ClusterSendResetAck();
                    clusterState = CLUSTERACQUIRED;
                } else {
                    /* We don't need to resend, since CheckPeers does that automatically */
                    /* ClusterResendUnacked(); */
                    clusterLastTriedAcquiring = now;
                }
            }
        }

        if (clusterState == CLUSTERVOTING) {
            if (clusterLastTriedVoting != now) {
                /* If all have acknowledge, then no one denied us, but someone must have gone down
                   during the election, so assume we may now take control */
                if (ClusterAllAcked()) {
                    ClusterSendResetAck();
                    clusterState = CLUSTERACQUIRED;
                } else
                    clusterLastTriedVoting = now;
            }
        }

        if (clusterState == CLUSTERDENIED) {
            if (now != clusterLastDenied)
                clusterState = CLUSTERIDLE;
        }

        if (clusterState == CLUSTERACQUIRED) {
            if (crCurrent == NULL)
                ClusterRequestStart();
            ClusterRequestProcess();
        }

        if (now != last) {
            ClusterHellos();
            ClusterCheckPeers();
            last = now;
            /*
            printf("s%d%d ", clusterState, clusterHaveMutex);
            fflush(stdout);
            */
        }

#ifdef USEPOLL
        pfds.fd = clusterSocket;
        pfds.events = POLLIN;
        rc = poll(&pfds, 1, 1000);
#else
        FD_ZERO(&sfds);
        FD_SET(clusterSocket, &sfds);
        tv.tv_sec = 1;
        tv.tv_usec = 0;
        rc = select(clusterSocket + 1, &sfds, NULL, NULL, &tv);
#endif
        if (rc <= 0) {
#ifndef WIN32
            if ((errno == ECANCELED)) {
                break;
            }
#endif
            continue;
        }
        sfl = sizeof(sf);
        msglen = recvfrom(clusterSocket, (char *)&msg, (size_t_M)sizeof(msg), 0,
                          (struct sockaddr *)&sf, &sfl);
        if (msglen < 0) {
            /* Linux indicates that there is data on the socket, but then returns this error? */
#ifdef ECONNREFUSED
            if (socket_errno != ECONNREFUSED)
#endif
                Log("Error receiving: %d", socket_errno);
            continue;
        }
        c = FindPeerByAddress(&sf);
        /* If this message is "from me", it is probably telling me to get on with checking status */
        if (c == peers)
            continue;
        if (c == NULL || msglen < PEERMSGHDRSIZE ||
            msglen != PEERMSGHDRSIZE + (msg.datalen = ntohs(msg.datalen))) {
            Log("Invalid cluster message from %s on port %d",
                ipToStr(&sf, ipBuffer, sizeof ipBuffer), ntohs(get_port(&sf)));
            continue;
        }
        msg.version = ntohs(msg.version);
        if (msg.version > CLUSTERVERSION) {
            Log("I use %04x clustering, %s:%d uses %04x clustering, packet ignored", CLUSTERVERSION,
                msg.version, c->peerName, get_port(&c->peerAddress));
            continue;
        }
        msg.msgid = msg.msgid;
        UUtime(&c->lastHeardFrom);
        msg.data[msg.datalen] = 0;
        if (0 && msg.cmd != 'T') /* Filter out all the hellos */
            Log("%s:%d sends type %c command %c id %d len %d data %s", c->peerName,
                get_port(&c->peerAddress), msg.type, msg.cmd, ntohl(msg.msgid), msg.datalen,
                msg.data);

        if (msg.msgid == c->smsg.msgid && msg.type != 'A' && msg.type != 'I')
            c->sAck = PAACKED;

        switch (msg.cmd) {
        case 'C':
            ClusterCmdCookie(c, &msg);
            break;

        case 'H':
            ClusterCmdHost(c, &msg);
            break;

        case 'L':
            ClusterCmdLoad(c, &msg);
            break;

        case 'M':
            ClusterCmdMutex(c, &msg);
            break;

        case 'S':
            ClusterCmdSession(c, &msg);
            break;

        case 'T':
            ClusterCmdTransition(c, &msg);
            break;

        default:
            ClusterBad("DoCluster", c, &msg);
        }
    }
    ChargeStopped(GCCLUSTER, NULL);
}
