#define __UUFILE__ "identifier.c"

#include "common.h"
#include "identifier.h"
#include "sql.h"

static sqlite3 *db = NULL;
static sqlite3 *sharedDb = NULL;
static int secretId = 0;
static char *secretText = NULL;
static time_t lastPurged = 0;

struct UUAVLTREE avlScope;
struct RWL rwlScope;

BOOL enabledDefault = TRUE;
int retentionDays = 60;
const int ENABLED_PENDING = -1;

struct IDENTIFIERSCOPE *otherScope = NULL;

static int UUAvlTreeCmpScope(const void *v1, const void *v2) {
    struct IDENTIFIERSCOPE *s1 = (struct IDENTIFIERSCOPE *)v1;
    struct IDENTIFIERSCOPE *s2 = (struct IDENTIFIERSCOPE *)v2;

    return strcmp(s1->name, s2->name);
}

// The user field should be forced to lowercase.
// Could use COLLATE NOCASE but avoiding to sidestep any performance penalty.
static const char *startupCommands[] = {"PRAGMA foreign_keys=ON",

                                        "CREATE TABLE IF NOT EXISTS secret("
                                        "id INTEGER PRIMARY KEY,"
                                        "text TEXT NOT NULL"
                                        ")",

                                        "CREATE TABLE IF NOT EXISTS scope("
                                        "id INTEGER PRIMARY KEY,"
                                        "name TEXT NOT NULL"
                                        ")",

                                        "CREATE TABLE IF NOT EXISTS identifier("
                                        "user TEXT NOT NULL,"
                                        "scopeid INTEGER NOT NULL,"
                                        "secretid INTEGER NOT NULL,"
                                        "created_at INT NOT NULL DEFAULT (strftime('%s', 'now')),"
                                        "from_at INTEGER NOT NULL,"
                                        "to_at INTEGER NOT NULL,"
                                        "pairwise TEXT NOT NULL,"
                                        "FOREIGN KEY(scopeid)"
                                        " REFERENCES scope(id)"
                                        " ON DELETE CASCADE"
                                        " ON UPDATE CASCADE,"
                                        "FOREIGN KEY(secretid)"
                                        " REFERENCES secret(id)"
                                        " ON DELETE CASCADE"
                                        " ON UPDATE CASCADE"
                                        ")",

                                        "CREATE INDEX IF NOT EXISTS"
                                        " idx_identifier_user_scopeid"
                                        " ON identifier(user, scopeid)",

                                        NULL};

int IdentifierSecret(char *text) {
    const char *sql;
    static sqlite3_stmt *selectStmt = NULL;
    static sqlite3_stmt *insertStmt = NULL;
    int result;

    if (selectStmt == NULL) {
        sql = "SELECT id FROM secret WHERE text = :text";
        SQLPrepare(db, sql, &selectStmt);
        sql = "INSERT INTO secret(text) VALUES (:text)";
        SQLPrepare(db, sql, &insertStmt);
    }

    for (;;) {
        SQLOK(SQLBindTextByName(selectStmt, ":text", text, SQLITE_STATIC));
        result = sqlite3_step(selectStmt);
        if (result == SQLITE_ROW) {
            secretId = sqlite3_column_int(selectStmt, 0);
            FreeThenNull(secretText);
            if (!(secretText = strdup(text)))
                PANIC;
            sqlite3_reset(selectStmt);
            return secretId;
        } else if (result != SQLITE_DONE) {
            SQLPANIC(result);
        } else {
            sqlite3_reset(selectStmt);
            SQLOK(SQLBindTextByName(insertStmt, ":text", text, SQLITE_STATIC));
            SQLDONE(sqlite3_step(insertStmt));
            sqlite3_reset(insertStmt);
        }
    }
}

sqlite3 *IdentifierGetSharedDatabase() {
    return sharedDb;
}

struct UUAVLTREE IdentifierGetScopeTree() {
    return avlScope;
}

static struct IDENTIFIERSCOPE *IdentifierCreateScope(const char *name, int id) {
    struct IDENTIFIERSCOPE *n;

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;
    n->name = UUStrDup(name);
    n->id = id;
    n->enabled = ENABLED_PENDING;
}

struct IDENTIFIERSCOPE *IdentifierScope(const char *name, BOOL create) {
    static sqlite3_stmt *selectStmt = NULL;
    static sqlite3_stmt *insertStmt = NULL;
    char *sql;
    int id;
    int result;
    struct IDENTIFIERSCOPE s = {name}, *n;

    RWLAcquireWrite(&rwlScope);
    n = UUAvlTreeFind(&avlScope, &s);
    if (n || !create) {
        RWLReleaseWrite(&rwlScope);
        return n;
    }

    if (selectStmt == NULL) {
        // clang-format off
        sql =
            "SELECT"
                " id"
            " FROM"
                " scope"
            " WHERE"
                " name = lower(:name)";
        // clang-format on

        SQLPrepare(db, sql, &selectStmt);
        sql = "INSERT INTO scope(name) VALUES (lower(:name))";
        SQLPrepare(db, sql, &insertStmt);
    }

    for (;;) {
        SQLOK(SQLBindTextByName(selectStmt, ":name", name, SQLITE_STATIC));
        result = sqlite3_step(selectStmt);
        if (result == SQLITE_ROW) {
            n = IdentifierCreateScope(name, sqlite3_column_int(selectStmt, 0));
            UUAvlTreeInsert(&avlScope, n);
            RWLReleaseWrite(&rwlScope);
            sqlite3_reset(selectStmt);
            return n;
        } else if (result != SQLITE_DONE) {
            SQLPANIC(result);
        } else {
            sqlite3_reset(selectStmt);
            SQLOK(SQLBindTextByName(insertStmt, ":name", name, SQLITE_STATIC));
            SQLDONE(sqlite3_step(insertStmt));
            sqlite3_reset(insertStmt);
        }
    }
}

void IdentifierScopeHeader(const char *name, const char *header) {
    struct IDENTIFIERSCOPE *p;

    p = IdentifierScope(name, TRUE);
    FreeThenNull(p->header);
    p->header = UUStrDup(header);
}

int IdentifierScopeId(char *name) {
    return IdentifierScope(name, TRUE)->id;
}

struct IDENTIFIERSCOPE *IdentifierScopeByHostname(const char *hostname) {
    struct IDENTIFIERSCOPE *p, *next;
    char *str;
    char *token;
    char *end;
    UUAVLTREEITERCTX uatic;
    int match = 0;

    if (db) {
        RWLAcquireRead(&rwlScope);

        for (p = (struct IDENTIFIERSCOPE *)UUAvlTreeIterFirst(&avlScope, &uatic); p;
             p = UUAvlTreeIterNext(&avlScope, &uatic)) {
            if (p->domains) {
                char domains[strlen(p->domains) + 1];
                strcpy(domains, p->domains);
                for (str = NULL, token = StrTok_R(domains, "|", &str); token;
                     token = StrTok_R(NULL, "|", &str)) {
                    if ((end = EndsIWith(hostname, token)) &&
                        (end == hostname || *(end - 1) == '.')) {
                        match = 1;
                        break;
                    }
                }
                if (match) {
                    break;
                }
            }
        }

        RWLReleaseRead(&rwlScope);
    }

    return match ? p : otherScope;
}

const char *IdentifierScopeName(int scopeid) {
    struct IDENTIFIERSCOPE *p, *next;
    UUAVLTREEITERCTX uatic;

    RWLAcquireRead(&rwlScope);

    for (p = (struct IDENTIFIERSCOPE *)UUAvlTreeIterFirst(&avlScope, &uatic); p;
         p = UUAvlTreeIterNext(&avlScope, &uatic)) {
        if (p->id == scopeid) {
            break;
        }
    }

    RWLReleaseRead(&rwlScope);
    return p ? p->name : NULL;
}

void IdentifierScopeAddDomain(const char *name, char *domain) {
    struct IDENTIFIERSCOPE *s = IdentifierScope(name, TRUE);
    size_t len;

    if (s->domains == NULL) {
        if (!(s->domains = strdup(domain)))
            PANIC;
    } else {
        if (!(s->domains = realloc(s->domains, strlen(s->domains) + strlen(domain) + 2)))
            PANIC;
        strcat(s->domains, "|");
        strcat(s->domains, domain);
    }
}

char *IdentifierCreateText(const char *user, int scopeid, time_t from_at, time_t to_at) {
    char *newSalt = NULL;
    char *output = NULL;
    unsigned char binaryOutput[EVP_MAX_MD_SIZE];
    char *p;
    int i;
    EVP_MD_CTX *mdctx = NULL;
    unsigned int mdLen;
    char *dollar;
    size_t saltLen;
    const char *name = IdentifierScopeName(scopeid);
    char times[32];
    char *text;
    char *sep = "|";

    if (!(mdctx = EVP_MD_CTX_new()))
        PANIC;
    EVP_DigestInit(mdctx, EVP_sha512());

    if (haName && *haName) {
        EVP_DigestUpdate(mdctx, haName, strlen(haName));
    } else {
        EVP_DigestUpdate(mdctx, myName, strlen(myName));
    }
    EVP_DigestUpdate(mdctx, sep, strlen(sep));
    EVP_DigestUpdate(mdctx, user, strlen(user));
    EVP_DigestUpdate(mdctx, sep, strlen(sep));
    EVP_DigestUpdate(mdctx, name, strlen(name));
    EVP_DigestUpdate(mdctx, sep, strlen(sep));
    snprintf(times, sizeof(times), "%" PRIdMAX "%s%" PRIdMAX "%s", from_at, sep, to_at, sep);
    EVP_DigestUpdate(mdctx, secretText, strlen(secretText));
    EVP_DigestUpdate(mdctx, times, strlen(times));
    EVP_DigestFinal(mdctx, binaryOutput, &mdLen);

    text = Encode64Binary(NULL, binaryOutput, mdLen);

    if (mdctx) {
        EVP_MD_CTX_free(mdctx);
        mdctx = NULL;
    }

    return text;
}

// Since timegm is not available in LSB, need to provide similar functionality.
// Adapted from days_from_civil at http://howardhinnant.github.io/date_algorithms.html
// which includes the author statement "Consider these donated to the public domain."
static int IdentifierSecondsSinceUnixEpoch(int year, int month, int day) {
    year -= month <= 2;
    int era = (year >= 0 ? year : year - 399) / 400;
    int yearOfEra = (year - era * 400);
    int dayOfYear = (153 * (month + (month > 2 ? -3 : 9)) + 2) / 5 + day - 1;
    int dayOfEra = yearOfEra * 365 + yearOfEra / 4 - yearOfEra / 100 + dayOfYear;
    return (era * 146097 + dayOfEra - 719468) * ONE_DAY_IN_SECONDS;
}

char *IdentifierTextByScopeId(const char *user, int scopeid, time_t when) {
    char *sql;
    static sqlite3_stmt *selectStmt = NULL;
    static sqlite3_stmt *insertStmt = NULL;
    int result;
    struct tm tm;
    time_t from_at, to_at;
    char *pairwise;

    if (scopeid == 0 || secretId == 0) {
        return NULL;
    }

    if (when == 0) {
        UUtime(&when);
    }

    if (selectStmt == NULL) {
        // clang-format off
        sql =
            "SELECT"
                " pairwise"
            " FROM"
                " identifier"
            " WHERE"
                " user = lower(:user)"
                " AND scopeid = :scopeid"
                " AND secretid = :secretid"
                " AND :when >= from_at"
                " AND :when < to_at"
            " ORDER BY"
                " from_at DESC"
            " LIMIT 1";
        // clang-format on

        SQLPrepare(db, sql, &selectStmt);
        sql =
            "INSERT INTO identifier(user, scopeid, secretid, from_at, to_at, pairwise) VALUES "
            "(lower(:user), :scopeid, :secretid, :from_at, :to_at, :pairwise)";
        SQLPrepare(db, sql, &insertStmt);
    }

    for (;;) {
        SQLOK(SQLBindTextByName(selectStmt, ":user", user, SQLITE_STATIC));
        SQLOK(SQLBindIntByName(selectStmt, ":scopeid", scopeid));
        SQLOK(SQLBindIntByName(selectStmt, ":secretid", secretId));
        SQLOK(SQLBindInt64ByName(selectStmt, ":when", when));
        result = sqlite3_step(selectStmt);
        if (result == SQLITE_ROW) {
            if (!(pairwise = strdup(sqlite3_column_text(selectStmt, 0))))
                PANIC;
            sqlite3_reset(selectStmt);
            return pairwise;
        } else if (result != SQLITE_DONE) {
            SQLPANIC(result);
        } else {
            UUgmtime_r(&when, &tm);
            int year = tm.tm_year + 1900;
            int month = tm.tm_mon + 1;
            int day = 1;
            from_at = IdentifierSecondsSinceUnixEpoch(year, month, day);
            if (month == 12) {
                month = 1;
                year++;
            } else {
                month++;
            }
            to_at = IdentifierSecondsSinceUnixEpoch(year, month, day);

            sqlite3_reset(selectStmt);
            pairwise = IdentifierCreateText(user, scopeid, from_at, to_at);
            SQLOK(SQLBindTextByName(insertStmt, ":user", user, SQLITE_STATIC));
            SQLOK(SQLBindIntByName(insertStmt, ":scopeid", scopeid));
            SQLOK(SQLBindIntByName(insertStmt, ":secretid", secretId));
            SQLOK(SQLBindInt64ByName(insertStmt, ":from_at", from_at));
            SQLOK(SQLBindInt64ByName(insertStmt, ":to_at", to_at));
            SQLOK(SQLBindTextByName(insertStmt, ":pairwise", pairwise, SQLITE_STATIC));
            SQLDONE(sqlite3_step(insertStmt));
            sqlite3_reset(insertStmt);
            return pairwise;
        }
    }
}

char *IdentifierTextByHost(char *user, struct HOST *h, time_t when) {
    if (h->identifierScope == NULL) {
        h->identifierScope = IdentifierScopeByHostname(h->hostname);
    }

    if (h->identifierScope->enabled) {
        return IdentifierTextByScopeId(user, h->identifierScope->id, when);
    } else {
        return NULL;
    }
}

struct IDENTIFIERSCOPE *IdentifierScopeByHost(struct HOST *h) {
    if (h->identifierScope == NULL) {
        h->identifierScope = IdentifierScopeByHostname(h->hostname);
    }

    return h->identifierScope;
}

void IdentifierPurge() {
    DWORD startTick, endTick;
    int idx = 0;

    // clang-format off
    const char *deleteCommands[] = {
        "DELETE"
        " FROM"
            " identifier"
        " WHERE"
            " to_at < :earliest",

        NULL
    };
    // clang-format on

    sqlite3_stmt *deleteStmt = NULL;
    int deleted;
    time_t now;
    time_t earliest;
    const char **sql;

    // If identifiers are not in use, return without attempting purge
    if (db == NULL) {
        return;
    }

    UUtime(&now);
    earliest = now - retentionDays * ONE_DAY_IN_SECONDS;

    Log("Identifier database purge");

    for (sql = deleteCommands; *sql; sql++, idx++) {
        if (optionSQLStats) {
            startTick = GetTickCount();
        }

        ChargeAlive(GCSECURITY, NULL);
        SQLPrepare(db, *sql, &deleteStmt);
        SQLOPTIONAL(SQLBindInt64ByName(deleteStmt, ":now", now));
        SQLOPTIONAL(SQLBindInt64ByName(deleteStmt, ":earliest", earliest));
        SQLDONE(sqlite3_step(deleteStmt));
        deleted = sqlite3_changes(db);
        sqlite3_reset(deleteStmt);
        sqlite3_clear_bindings(deleteStmt);

        if (optionSQLStats) {
            endTick = GetTickCount();
            Log("SQL IdentifierPurge %d deleted %d in %d", idx, deleted,
                GetTickDiff(endTick, startTick));
        }
    }

    UUtime(&lastPurged);
}

void IdentifierVacuum() {
    const char *sql = "VACUUM";
    sqlite3_stmt *vacuumStmt = NULL;

    // If identifiers are not in use, return without attempting vacuum
    if (db == NULL) {
        return;
    }

    ChargeAlive(GCIDENTIFIER, NULL);
    SQLPrepare(db, "VACUUM", &vacuumStmt);
    ChargeStopped(GCIDENTIFIER, NULL);
    Log("Identifier database vacuum starting");
    SQLDONE(sqlite3_step(vacuumStmt));
    sqlite3_finalize(vacuumStmt);
    Log("Identifier database vacuum finished");
    ChargeAlive(GCIDENTIFIER, NULL);
}

static void DoIdentifier(void *v) {
    struct SECURITY_NODE *n;
    DWORD currTick, lastTick;
    DWORD interval = 60 * 1000;
    time_t now;
    struct tm tm;

    lastTick = 0;

    for (;;) {
        ChargeAlive(GCIDENTIFIER, &now);
        // Nothing really needs to be done here for now
        SubSleep(1, 0);
    }
}

void IdentifierOpenDatabase(sqlite3 **db) {
    SQLOK(sqlite3_open_v2(IDENTIFIERDB, db,
                          SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX,
                          NULL));
    SQLOK(sqlite3_busy_timeout(*db, 60000));
}

static void IdentifierInit() {
    const char **sql;
    struct IDENTIFIERSCOPE *s;
    const char *acs = "acs.org";
    const char *osa = "osapublishing.org";
    const char *sd = "sciencedirect.com";
    const char *tf = "taylorandfrancis.com";
    const char *wiley = "wiley.com";

    UUAvlTreeInit(&avlScope, "avlScope", UUAvlTreeCmpScope, NULL, NULL);
    RWLInit(&rwlScope, "rwlScope");

    IdentifierOpenDatabase(&db);
    IdentifierOpenDatabase(&sharedDb);

    SQLOK(sqlite3_open_v2(IDENTIFIERDB, &db,
                          SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX,
                          NULL));
    SQLOK(sqlite3_busy_timeout(db, 15000));

    for (sql = startupCommands; *sql; sql++) {
        SQLEXEC(db, *sql, NULL, NULL);
    }

    IdentifierScopeHeader(acs, IDENTIFIERHISTORICHEADER);
    IdentifierScopeAddDomain(acs, "pubs.acs.org");

    IdentifierScopeHeader(osa, IDENTIFIERHISTORICHEADER);
    IdentifierScopeAddDomain(osa, "opg.optica.org");
    IdentifierScopeAddDomain(osa, "opticsinfobase.org");
    IdentifierScopeAddDomain(osa, "osa.org");
    IdentifierScopeAddDomain(osa, "osapublishing.org");

    IdentifierScopeHeader(sd, IDENTIFIERHISTORICHEADER);
    IdentifierScopeAddDomain(sd, "sciencedirect.com");

    IdentifierScopeHeader(tf, IDENTIFIERHEADER);
    // Additional domains provided 2022-10-14
    IdentifierScopeAddDomain(tf, "taylorfrancis.com");
    IdentifierScopeAddDomain(tf, "taylorfrancis.net");
    // Europa
    IdentifierScopeAddDomain(tf, "europaworld.com");
    IdentifierScopeAddDomain(tf, "worldoflearning.com");
    IdentifierScopeAddDomain(tf, "worldwhoswho.com");
    // F1000 sites
    IdentifierScopeAddDomain(tf, "aasopenresearch.org");
    IdentifierScopeAddDomain(tf, "amrcopenresearch.org");
    IdentifierScopeAddDomain(tf, "f1000research.com");
    IdentifierScopeAddDomain(tf, "gatesopenresearch.org");
    IdentifierScopeAddDomain(tf, "hrbopenresearch.org");
    IdentifierScopeAddDomain(tf, "isfopenresearch.org");
    IdentifierScopeAddDomain(tf, "mededpublish.org");
    IdentifierScopeAddDomain(tf, "mniopenresearch.org");
    IdentifierScopeAddDomain(tf, "open-research-europe.ec.europa.eu");
    // branded with Routledge/ portals
    IdentifierScopeAddDomain(tf, "19thcenturyart-facos.com");
    IdentifierScopeAddDomain(tf, "balkema.nl");
    IdentifierScopeAddDomain(tf, "crcpress.com");
    IdentifierScopeAddDomain(tf, "drugcalcsnurses.co.uk");
    IdentifierScopeAddDomain(tf, "pricebooks.co.uk");
    IdentifierScopeAddDomain(tf, "reframingphotography.com");
    IdentifierScopeAddDomain(tf, "routledge.com");
    IdentifierScopeAddDomain(tf, "routledgehandbooks.com");
    IdentifierScopeAddDomain(tf, "routledgehistoricalresources.com");
    IdentifierScopeAddDomain(tf, "routledgeperformancearchive.com");
    IdentifierScopeAddDomain(tf, "routledgeresearchencyclopedias.com");
    IdentifierScopeAddDomain(tf, "routledgesoc.com");
    IdentifierScopeAddDomain(tf, "routledgesw.com");
    IdentifierScopeAddDomain(tf, "routledgeteachertraininghub.com");
    IdentifierScopeAddDomain(tf, "routledgetextbooks.com");
    // T&F, TFO, UBX (redirects exist on some)
    IdentifierScopeAddDomain(tf, "taylorandfrancis.com");
    IdentifierScopeAddDomain(tf, "taylorandfrancisgroup.com");
    IdentifierScopeAddDomain(tf, "taylorandfrancis-renewals.informa.com");
    IdentifierScopeAddDomain(tf, "baileyandlove.com");
    IdentifierScopeAddDomain(tf, "chemnetbase.com");
    IdentifierScopeAddDomain(tf, "giftedscales.com");
    IdentifierScopeAddDomain(tf, "renaldrugdatabase.co.uk");
    IdentifierScopeAddDomain(tf, "renaldrugdatabase.com");
    IdentifierScopeAddDomain(tf, "catalogingandclassificationquarterly.com");
    IdentifierScopeAddDomain(tf, "tandf.co.uk");
    IdentifierScopeAddDomain(tf, "tandfeditingservices.cn");
    IdentifierScopeAddDomain(tf, "tandfeditingservices.com");
    IdentifierScopeAddDomain(tf, "tandfonline.com");
    // The following were provided by the vendor but are omitted in code since they are covered by
    // tandfonline.com IdentifierScopeAddDomain(tf, "rp.tandfonline.com");
    // IdentifierScopeAddDomain(tf, "rp-dev.tandfonline.com");
    // IdentifierScopeAddDomain(tf, "rp-uat.tandfonline.com");
    // Dove Press
    IdentifierScopeAddDomain(tf, "dovepress.com");

    IdentifierScopeHeader(wiley, IDENTIFIERHEADER);
    IdentifierScopeAddDomain(wiley, "onlinelibrary.wiley.com");
    IdentifierScopeAddDomain(wiley, "pericles-stag.literatumonline.com");
    IdentifierScopeAddDomain(wiley, "pericles-test.literatumonline.com");
    IdentifierScopeAddDomain(wiley, "www.embopress.org");
    IdentifierScopeAddDomain(wiley, "www.birpublications.org");
}

void IdentifierStart() {
    struct IDENTIFIERSCOPE *s;
    struct IDENTIFIERSCOPE *p;
    UUAVLTREEITERCTX uatic;

    otherScope = IdentifierCreateScope(IDENTIFIEROTHERSCOPE, 0);
    otherScope->enabled = FALSE;
    otherScope->hidden = TRUE;

    if (db != NULL) {
        for (p = (struct IDENTIFIERSCOPE *)UUAvlTreeIterFirst(&avlScope, &uatic); p;
             p = (struct IDENTIFIERSCOPE *)UUAvlTreeIterNext(&avlScope, &uatic)) {
            if (p->enabled == ENABLED_PENDING) {
                p->enabled = enabledDefault;
            }
        }
        Log("Identifiers enabled");
        if (UUBeginThread(DoIdentifier, NULL, guardianChargeName[GCIDENTIFIER]))
            PANIC;
    }
}

void IdentifierConfig(char *arg, char *currentFilename, int currentLineNum) {
    char *cmd;
    struct IDENTIFIERSCOPE *s;
    int days;

    cmd = arg;
    arg = SkipNonWS(arg);
    if (*arg) {
        *arg = 0;
        arg = SkipWS(arg + 1);
    }

    if (stricmp(cmd, "Secret") == 0) {
        if (*arg == 0 || StrIStr(arg, "a-value-you-set-here") || StrIStr(arg, "astringyoupick")) {
            Log("Invalid Identifier Secret");
        } else {
            if (db == NULL) {
                IdentifierInit();
            }
            IdentifierSecret(arg);
        }
    } else if (db == NULL) {
        Log("Identifier Secret must appear before any other Identifier directives");
    } else {
        if (stricmp(cmd, "Retention") == 0) {
            days = atoi(arg);
            if (days <= 0) {
                Log("Identifier invalid retention: %s", arg);
            } else {
                retentionDays = days;
            }
        } else if (stricmp(cmd, "Enable") == 0) {
            if (*arg == 0) {
                enabledDefault = TRUE;
            } else {
                AllLower(arg);
                s = IdentifierScope(arg, FALSE);
                if (s) {
                    s->enabled = TRUE;
                } else {
                    Log("Identifier invalid scope: %s", arg);
                }
            }
        } else if (stricmp(cmd, "Disable") == 0) {
            if (*arg == 0) {
                enabledDefault = FALSE;
            } else {
                s = IdentifierScope(arg, FALSE);
                if (s) {
                    s->enabled = FALSE;
                } else {
                    Log("Identifier invalid scope: %s", arg);
                }
            }
        } else if (stricmp(cmd, "Development") == 0) {
            char *uu = "usefulutilities.com";
            s = IdentifierScope(uu, TRUE);
            // IdentifierScopeHeader(uu, IDENTIFIERHISTORICHEADER);
            s->enabled = TRUE;
            IdentifierScopeAddDomain(uu, uu);
        } else {
            Log("Identifier invalid directive in %s:%d: %s", currentFilename, currentLineNum, cmd);
        }
    }
}
