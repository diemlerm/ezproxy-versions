#ifndef __ADMINAUDIT_H__
#define __ADMINAUDIT_H__

#include "common.h"

/* These AUDIT* definitions must be synchronized with AUDITNAMES entries. */
#define AUDITLOGINSUCCESS 0
#define AUDITLOGINSUCCESSGROUPS 1
#define AUDITLOGINSUCCESSPASSWORD 2
#define AUDITLOGINFAILURE 3
#define AUDITLOGINFAILUREPASSWORD 4
#define AUDITLOGININTRUDERIP 5
#define AUDITLOGININTRUDERUSER 6
#define AUDITLOGOUT 7
#define AUDITUSAGELIMIT 8
#define AUDITUNAUTHORIZED 9
#define AUDITSYSTEM 10
#define AUDITZ3950CONNECT 11
#define AUDITZ3950DISCONNECT 12
#define AUDITZ3950DENIED 13
#define AUDITINFOUSR 14
#define AUDITLOGINDENIED 15
#define AUDITBLOCKCOUNTRYCHANGE 16
#define AUDITUSEROBJECTGETTOKEN 17
#define AUDITUSEROBJECTGETUSEROBJECT 18
#define AUDITUSEROBJECTPUTUSEROBJECT 19
#define AUDITSESSIONIPCHANGE 20
#define AUDITSESSIONRECONNECTBLOCKED 21
#define AUDITLOGINSUCCESSRELOGIN 22
#define AUDITMINPROXYSUCCESSCONNECT 23
#define AUDITMINPROXYSUCCESSBLOCKED 24
#define AUDITMINPROXYFAILURE 25
#define AUDITINTRUSIONAPIBADIP 26
#define AUDITINTRUSIONAPIERROR 27
#define AUDITINTRUSIONAPINONE 28
#define AUDITINTRUSIONAPIWHITELISTED 29
#define AUDITSECURITY 30
#define AUDITSECURITYEXEMPT 31
#define AUDITSECURITYADMIN 32
#define AUDITMAX 33

#define AUDITLOGINSUCCESSMASK (1 << AUDITLOGINSUCCESS)
#define AUDITLOGINSUCCESSGROUPSMASK (1 << AUDITLOGINSUCCESSGROUPS)
#define AUDITLOGINSUCCESSPASSWORDMASK (1 << AUDITLOGINSUCCESSPASSWORD)
#define AUDITLOGINFAILUREMASK (1 << AUDITLOGINFAILURE)
#define AUDITLOGINFAILUREPASSWORDMASK (1 << AUDITLOGINFAILUREPASSWORD)
#define AUDITLOGININTRUDERIPMASK (1 << AUDITLOGININTRUDERIP)
#define AUDITLOGININTRUDERUSERMASK (1 << AUDITLOGININTRUDERUSER)
#define AUDITLOGOUTMASK (1 << AUDITLOGOUT)
#define AUDITUSAGELIMITMASK (1 << AUDITUSAGELIMIT)
#define AUDITUNAUTHORIZEDMASK (1 << AUDITUNAUTHORIZED)
#define AUDITSYSTEMMASK (1 << AUDITSYSTEM)
#define AUDITZ3950CONNECTMASK (1 << AUDITZ3950CONNECT)
#define AUDITZ3950DISCONNECTMASK (1 << AUDITZ3950DISCONNECT)
#define AUDITZ3950DENIEDMASK (1 << AUDITZ3950DENIED)
#define AUDITINFOUSRMASK (1 << AUDITINFOUSR)
#define AUDITLOGINDENIEDMASK (1 << AUDITLOGINDENIED)
#define AUDITBLOCKCOUNTRYCHANGEMASK (1 << AUDITBLOCKCOUNTRYCHANGE)
#define AUDITUSEROBJECTGETTOKENMASK (1 << AUDITUSEROBJECTGETTOKEN)
#define AUDITUSEROBJECTGETUSEROBJECTMASK (1 << AUDITUSEROBJECTGETUSEROBJECTMASK)
#define AUDITSESSIONIPCHANGEMASK (1 << AUDITSESSIONIPCHANGE)
#define AUDITSESSIONRECONNECTBLOCKEDMASK (1 << AUDITSESSIONRECONNECTBLOCKED)
#define AUDITLOGINSUCCESSRELOGINMASK (1 << AUDITLOGINSUCCESSRELOGIN)
#define AUDITMINPROXYSUCCESSCONNECTMASK (1 << AUDITMINPROXYSUCCESSCONNECT)
#define AUDITMINPROXYSUCCESSBLOCKEDMASK (1 << AUDITMINPROXYSUCCESSBLOCKED)
#define AUDITMINPROXYFAILUREMASK (1 << AUDITMINPROXYFAILURE)
#define AUDITINTRUSIONAPIBADIPMASK (1 << AUDITINTRUSIONAPIBADIP)
#define AUDITINTRUSIONAPIERRORMASK (1 << AUDITINTRUSIONAPIERROR)
#define AUDITINTRUSIONAPINONEMASK (1 << AUDITINTRUSIONAPINONE)
#define AUDITINTRUSIONAPIWHITELISTEDMASK (1 << AUDITINTRUSIONAPIWHITELISTED)
#define AUDITSECURITYMASK (1 << AUDITSECURITY)
#define AUDITSECURITYEXEMPTMASK (1 << AUDITSECURITYEXEMPT)
#define AUDITSECURITYADMINMASK (1 << ADMINSECURITYADMIN)

#define AUDITMOSTMASK                                                                            \
    (~(AUDITINTRUSIONAPINONEMASK | AUDITLOGINSUCCESSGROUPSMASK | AUDITLOGINSUCCESSPASSWORDMASK | \
       AUDITLOGINFAILUREPASSWORDMASK | AUDITSESSIONIPCHANGEMASK))
#define AUDITREMOVEDMASK (AUDITLOGINSUCCESSPASSWORDMASK | AUDITLOGINFAILUREPASSWORDMASK)
extern uint64_t auditEventsMask;

void AdminAudit(struct TRANSLATE *t, char *query, char *post);
char *AuditEventNameSelect(int event);
void AuditEvent(struct TRANSLATE *t,
                int event,
                const char *user,
                struct SESSION *e,
                char *other,
                ...);
void AuditEventLogin(struct TRANSLATE *t,
                     int event,
                     char *user,
                     struct SESSION *e,
                     char *password,
                     char *comment);
BOOL AdminLicenseInvalid(struct TRANSLATE *t);
void AuditPurge(void);

#endif  // __ADMINAUDIT_H__
