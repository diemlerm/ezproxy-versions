#ifndef __USRLDAP_H__
#define __USRLDAP_H__

#include "common.h"

/* Returns 0 if valid, 1 if not, 3 if unable to connect to ldap host */
int FindUserLdap(char *ldapHost,
                 char *user,
                 char *pass,
                 struct sockaddr_storage *pInterface,
                 char useSsl,
                 int sslIdx);

#endif  // __USRLDAP_H__
