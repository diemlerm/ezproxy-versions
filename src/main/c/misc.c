#define __UUFILE__ "misc.c"

#include "ezproxyversion.h"
#include "common.h"
#include "expression.h"
#include "usr.h"
#include "openssl/err.h"
#include "openssl/rand.h"
#include "openssl/des.h"
#include "url_parser.h"
#include "spur.h"

#include <stdarg.h>

#ifdef WIN32
/* io.h, fcntl.h, share.h and sys/stat.h needed for _sopen in Win32 */
#include <io.h>
#include <fcntl.h>
#include <share.h>
#include <sys/locking.h>
#include <direct.h>
#else
#include <sys/uio.h>
#include <dirent.h>
#include "unix.h"
#endif

#include <sys/stat.h>

#include "adminfmg.h"
#include "adminnetlibrary.h"

BOOL deadlocked = 0;
UUTHREADID deadlockReporter;

char *UUErrorMessage() {
#ifdef WIN32
    return ErrorMessage(GetLastError());
#else
    return ErrorMessage(errno);
#endif
}

BOOL RefreshStartingPointUrl(struct TRANSLATE *t, char *url) {
    int i;
    struct STARTINGPOINTURLREFRESH *startingPointUrlRefresh;
    BOOL refresh = 0;
    char *userAgent;

    if (cStartingPointUrlRefreshes == 0)
        return 0;

    userAgent = FindField("User-Agent", t->requestHeaders);
    if (userAgent == NULL)
        userAgent = "(unknown)";
    else
        LinkFields(userAgent);

    for (startingPointUrlRefresh = startingPointUrlRefreshes, i = 0; i < cStartingPointUrlRefreshes;
         i++) {
        if (WildCompare(url, startingPointUrlRefresh->filter)) {
            if (*startingPointUrlRefresh->userAgent &&
                WildCompare(userAgent, startingPointUrlRefresh->userAgent) == 0)
                continue;
            refresh = startingPointUrlRefresh->mode == '+';
        }
    }

    return refresh;
}

BOOL IsIERedirectBrainDead(char *url) {
    char *query;
    int tries;

    if (url == NULL)
        return 0;

    query = strchr(url, '?');
    if (query == NULL)
        query = strchr(url, 0);

    for (tries = 0; tries < 2; tries++) {
        if (query - 4 >= url && strnicmp(query - 4, ".pdf", 4) == 0)
            return 1;

        if (query - 5 >= url && strnicmp(query - 5, ".djvu", 5) == 0)
            return 1;

        if (*query == 0)
            break;

        query = strchr(query, 0);
    }

    return 0;
}

void CheckUserAgent(struct TRANSLATE *t) {
    int i;
    char *userAgent;
    BOOL minus;
    char *pastMinus;
    char *p;

    t->needPdfRefresh = 0;

    userAgent = FindField("User-Agent", t->requestHeaders);
    if (userAgent == NULL)
        userAgent = "(unknown)";
    else
        LinkFields(userAgent);

    if (p = StrIStr(userAgent, "Mozilla/")) {
        p = strchr(p, '/');
        t->mozillaVersion = atof(p + 1);
    } else {
        t->mozillaVersion = 0;
    }

    t->safari = strstr(userAgent, "AppleWebKit/") != NULL;
    t->isMSIE = WildCompare(userAgent, "*MSIE*");
    t->isMacMSIE =
        t->isMSIE & WildCompare(userAgent, "*Mac*") && WildCompare(userAgent, "*Opera*") == 0;

    if (cPdfRefreshes == 0) {
        t->needPdfRefresh = 1;
        return;
    }

    for (i = 0; i < cPdfRefreshes; i++) {
        pastMinus = pdfRefreshes[i].userAgent;
        if (minus = (*pastMinus == '-'))
            pastMinus++;
        else if (*pastMinus == '+')
            pastMinus++;
        if (WildCompare(userAgent, pastMinus)) {
            if (minus == 0)
                t->needPdfRefresh = 1;
            return;
        }
    }
}

void CheckRequireAuthenticate(struct TRANSLATE *t) {
    char *cookie;
    char *rac;

    t->requireAuthenticate = REQUIREAUTHENTICATEMAX;

    if (optionRequireAuthenticate) {
        for (cookie = t->requestHeaders; (cookie = FindField("cookie", cookie));) {
            LinkField(cookie);
            for (rac = cookie; rac = strstr(rac, requireAuthenticateCookieName);) {
                rac += strlen(requireAuthenticateCookieName);
                if (*rac == '=') {
                    rac++;
                    if (*rac >= '0' && *rac < '0' + REQUIREAUTHENTICATEMAX) {
                        t->requireAuthenticate = *rac - '0';
                    }
                    break;
                }
            }
        }
    }

    if (t->requireAuthenticate == REQUIREAUTHENTICATEMAX)
        t->requireAuthenticate = REQUIREAUTHENTICATENEVER;
}

char *URLReference(char *url, char *tag) {
    char *me = (myUrlHttps ? myUrlHttps : myUrlHttp);
    char *ur = NULL;
    char *uu64 = NULL;
    char *menu = NULL;

    if (url == NULL || *url == 0) {
        if (!(menu = malloc(strlen(me) + 10)))
            PANIC;
        sprintf(menu, "%s/menu", me);
        url = menu;
    }

    if (tag == NULL)
        tag = "";

    /* the +16 allows for adding in the ezp.2, maybe a $ between tag and URL, and still a bit to
     * spare */
    if (!(ur = malloc(Encode64BytesNeeded(strlen(tag) + strlen(url)) + 16)))
        PANIC;

    /* URL References must always start ezp. so external scripts may safely count on this */
    strcpy(ur, "ezp.2");

    if (*tag) {
        char *temp;
        if (!(temp = malloc(strlen(tag) + strlen(url) + 2)))
            PANIC;
        sprintf(temp, "%s$%s", tag, url);
        uu64 = EncodeUU64(strchr(ur, 0), temp);
        FreeThenNull(temp);
    } else {
        EncodeUU64(strchr(ur, 0), url);
    }

    FreeThenNull(menu);

    return ur;
}

void SendURLReference(struct UUSOCKET *s, char *url, char *tag) {
    char *ur = NULL;

    ur = URLReference(url, tag);
    UUSendZ(s, ur);
    FreeThenNull(ur);
}

/* The ezp.1 version is now not considered to be a true reference,
   whereas ezp.2 is; the difference is that the "alturl" mechanism
   will always create ezp.1, which EZproxy now considers no different
   than a real URL, which won't be used for loop detection, whereas
   EZproxy will create ezp.2 for its own purposes, which does result
   in loop detection
*/
BOOL DecodeURLReference(char *url) {
    char *s = NULL;
    char *test = url;
    BOOL result = 0;
    char *ref;

    if (url) {
        /* Avoid creating a duplicate copy unless it could be a reference */
        if (*url == '%' || *url == 'e') {
            /* A reference is all unreserved characters, but they could have been
               percent-escaped by a remote CGI, so in case that did happen, unencode
               a copy for checking
            */
            if (strchr(url, '%')) {
                if (!(s = strdup(url)))
                    PANIC;
                UnescapeString(s);
                test = s;
            }
            if (ref = StartsWith(test, "ezp.")) {
                if (*ref == '1' || *ref == '2') {
                    char *newUrl;
                    if (newUrl = DecodeUU64(NULL, ref + 1)) {
                        if (*ref == '2')
                            result = 1;
                        strcpy(url, newUrl);
                        FreeThenNull(newUrl);
                    }
                }
            }
        }
    }

    FreeThenNull(s);

    return result;
}

char *RemoveLogTag(char *url) {
    char *p;
    size_t len;
    char *logTag;

    if (url == NULL)
        return NULL;

    for (p = url, len = 0;; p++, len++) {
        if (*p == '$') {
            if (len == 0)
                return NULL;
            if (!(logTag = malloc(len + 1)))
                PANIC;
            memcpy(logTag, url, len);
            *(logTag + len) = 0;
            StrCpyOverlap(url, p + 1);
            return logTag;
        }
        if (*p == 0 || len >= MAXLOGTAGLEN)
            return NULL;
        if (IsAlpha(*p) || IsDigit(*p) || *p == '.')
            continue;
        return NULL;
    }

    return NULL;
}

BOOL IsValidLogTag(char *logTag) {
    char *p;
    size_t len;

    if (logTag == NULL)
        return 0;

    for (p = logTag, len = 0; *p; p++, len++) {
        if (len >= MAXLOGTAGLEN)
            return 0;
        if (IsAlpha(*p) || IsDigit(*p) || *p == '.')
            continue;
        return 0;
    }
    return len > 0;
}

char *SkipAt(char *s) {
    char *host, *p;

    for (host = p = s; *p; p++) {
        if (*p == '/' || *p == '?')
            break;
        if (*p == '@')
            host = p + 1;
    }
    return host;
}

char *SkipHTTPslashesAt(char *s, char *useSsl) {
    /* Assume no use of SSL until proven otherwise */
    if (useSsl)
        *useSsl = 0;

    if (strnicmp(s, "http://", 7) == 0) {
        s += 7;
    } else if (strnicmp(s, "https://", 8) == 0) {
        s += 8;
        if (useSsl)
            *useSsl = 1;
    } else if (strnicmp(s, "//", 2) == 0) {
        s += 2;
    }
    return SkipAt(s);
}

char *SkipFTPHTTPslashesAt(char *s, char *useSsl, char *ftpgw) {
    if (useSsl)
        *useSsl = 0;

    if (ftpgw)
        *ftpgw = 0;

    if (strnicmp(s, "ftp://", 6) == 0) {
        if (ftpgw)
            *ftpgw = 1;
        s += 6;
        return SkipAt(s);
    }

    return SkipHTTPslashesAt(s, useSsl);
}

/**
 * Basically just call inet_ntop, but has the restriction
 * of not allowing ip wrapped with "[]".
 *
 * Handles NULL pointer
 *
 * @return 1 on success, 0 on failure
 */
static BOOL ValidIp6(const char *ip) {
    if (!ip)
        return FALSE;

    struct sockaddr_in6 sa;

    if (inet_pton(AF_INET6, ip, &(sa.sin6_addr)) != 1)
        return FALSE;

    // dont want to let through bracketed addresses either
    if (ip && ip[0] == '[')
        return FALSE;

    return TRUE;
}

/**
 * Verfiy that this is a valid ipv6 address.  Allows for
 * address wrapped with "[]" or without.  Does not allow
 * range syntax such as ::1/128
 *
 * Handles NULL pointer
 */
BOOL NumericHostIp6(const char *host) {
    if (host && host[0] == '[') {
        int len = strlen(host);
        if (host[len - 1] != ']')
            return FALSE;

        char substr[len];
        strncpy(substr, host + 1, len - 2);
        substr[len - 2] = '\0';

        return ValidIp6(substr);
    }
    return ValidIp6(host);
}

BOOL NumericHostIp4(const char *host) {
    static char *re = "^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$";
    static pcre *pattern = NULL;
    static pcre_extra *patternExtra = NULL;
    const char *pcreError;
    int pcreErrorOffset;

    /* The main routine calls here while we are still single-threaded with
       NULL to let us initialize our patterns
    */
    if (pattern == NULL) {
        pattern = pcre_compile(re, 0, &pcreError, &pcreErrorOffset, NULL);
        if (pattern == NULL)
            PANIC;
        patternExtra = pcre_study(pattern, 0, &pcreError);
    }

    if (host == NULL)
        return FALSE;

    return pcre_exec(pattern, patternExtra, host, strlen(host), 0, 0, 0, 0) >= 0;
}

/* Check a domain to see if it is totally numeric (digits and periods only) */
/* Returns 1 if totally numeric, 0 otherwise */
BOOL NumericHost(const char *host) {
    if (NumericHostIp6(host))
        return TRUE;

    return NumericHostIp4(host);
}

#define LOGVAR_FMT "%-40s %s"
#define LOGVAR_HEADING1_FMT "%s"
#define LOGVAR_HEADING2_FMT "%-40s %s"
void LogVariables2(struct TRANSLATE *t,
                   struct VARIABLES *v,
                   void *context,
                   const char *name,
                   const char *value,
                   int count) {
    Log(LOGVAR_FMT, name, value);
}

void LogVariables(struct VARIABLES *v, char *type) {
    int count = 0;

    Log(LOGVAR_HEADING1_FMT, type);
    Log(LOGVAR_HEADING2_FMT, "VARIABLE", "VALUE");
    count = VariablesForAll(NULL, v, NULL, LogVariables2);
    if (count == 0) {
        Log("  NONE");
    }
}

char *ASCIIKMB(unsigned int x, int decimals, char *buffer) {
    if (decimals == -1)
        decimals = 8;

    sprintf(buffer, "%0.*f", decimals, (double)(x / 1000.0));
    Trim(buffer, TRIM_DECIMAL);
    return buffer;
}

char *ASCIIDHM(int seconds, char *buffer) {
    int days = seconds / ONE_DAY_IN_SECONDS;
    seconds -= days * ONE_DAY_IN_SECONDS;
    int hours = seconds / ONE_HOUR_IN_SECONDS;
    seconds -= hours * ONE_HOUR_IN_SECONDS;
    int minutes = seconds / 60;

    *buffer = 0;

    if (days > 0) {
        sprintf(AtEnd(buffer), "%d day%s ", days, SIfNotOne(days));
    }

    if (hours > 0) {
        sprintf(AtEnd(buffer), "%d hour%s ", hours, SIfNotOne(hours));
    }

    if (minutes > 0) {
        sprintf(AtEnd(buffer), "%d minute%s ", minutes, SIfNotOne(minutes));
    }

    Trim(buffer, TRIM_TRAIL);

    return buffer;
}

char *ASCIIDate(time_t *time, char *buffer) {
    struct tm *tm, rtm;

    if (*time == 0) {
        strcpy(buffer, "(never)");
    } else {
        tm = UUlocaltime_r(time, &rtm);
        sprintf(buffer, "%04d-%02d-%02d %02d:%02d:%02d", tm->tm_year + 1900, tm->tm_mon + 1,
                tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
    }
    return buffer;
}

int print_hex(char *buffer, size_t size, unsigned char *value, size_t valueLen) {
    static char digits[16] = "0123456789ABCDEF";
    size_t i, j;

    for (i = 0, j = 0; i < valueLen; i++) {
        if (j < size) {
            buffer[j] = digits[(value[i] >> 4) & 0xF];
            j++;
        }
        if (j < size) {
            buffer[j] = digits[(value[i]) & 0xF];
            j++;
        }
    }
    if (j < size) {
        buffer[j] = 0;
    } else if (size > 0) {
        buffer[size - 1] = 0;
    }
    return j;
}

void NextLine(char *suffixLine, char **endOfLinePtr, char **nextLinePtr) {
    *endOfLinePtr = strchr(suffixLine, '\r');
    if (*endOfLinePtr == NULL) {
        *endOfLinePtr = strchr(suffixLine, '\n');
        if (*endOfLinePtr == NULL) {
            *endOfLinePtr = strchr(suffixLine, 0);
        }
    }
    *nextLinePtr = *endOfLinePtr;
    if (((*nextLinePtr)[0] == '\r') && ((*nextLinePtr)[1] == '\n')) {
        *nextLinePtr = *nextLinePtr + 2;
    } else if ((*nextLinePtr)[0] == '\n') {
        *nextLinePtr = (*nextLinePtr) + 1;
    }
}

void LogLine(char *prefix, char *suffix, char *terminator) {
    if (msgFile != -1) {
        UUAcquireMutex(&mMsgFile);
        write(msgFile, prefix, strlen(prefix));
        write(msgFile, suffix, strlen(suffix));
        write(msgFile, terminator, strlen(terminator));
        UUReleaseMutex(&mMsgFile);
    }
    if (!ttyClosed) {
        UUAcquireMutex(&mMsgFile);
        if (debugLevel > 0) {
            write(1, prefix, strlen(prefix));
        }
        write(1, suffix, strlen(suffix));
        write(1, terminator, strlen(terminator));
        UUReleaseMutex(&mMsgFile);
    }
}

#define LINE_WRAP_LENGTH 132
#define TRUNCATION_STATEMENT_LENGTH 132
void LogVA(const char *format, va_list *ap) {
#ifdef WIN32
    int pid;
    DWORD tid;
#else
    pid_t pid;
    pthread_t tid;
#endif
#ifdef WIN32
#define xGetCurrentThreadId GetCurrentThreadId
#else
#define xGetCurrentThreadId pthread_self
#endif
    time_t now;
    char dn[MAXASCIIDATE];
    char *suffixLine, *nextSuffixLine, *suffixLineEnd;
    char c;
    char prefix[120];
    char suffix[32000];
    BOOL overflowLine = FALSE;
    BOOL firstLine;
    char *p = prefix;
    int l;
    int m;

    UUtime(&now);
    ASCIIDate(&now, dn);
    _SNPRINTF_BEGIN(prefix, sizeof prefix);
    _snprintf(prefix, sizeof prefix, "%s ", dn);
    _SNPRINTF_END(prefix, sizeof prefix);

    if (debugLevel > 0) {
        tid = xGetCurrentThreadId();
        pid = getpid();

        p = prefix;
        m = sizeof prefix;

        l = strlen(p);
        m = m - l;
        p = p + l;
        _SNPRINTF_BEGIN(p, m);
        print_hex(p, m, (unsigned char *)(&(pid)), sizeof pid);
        _SNPRINTF_END(p, m);

        l = strlen(p);
        m = m - l;
        p = p + l;
        _SNPRINTF_BEGIN(p, m);
        _snprintf(p, m, " ");
        _SNPRINTF_END(p, m);

        l = strlen(p);
        m = m - l;
        p = p + l;
        _SNPRINTF_BEGIN(p, m);
        print_hex(p, m, (unsigned char *)(&(tid)), sizeof tid);
        _SNPRINTF_END(p, m);

        l = strlen(p);
        m = m - l;
        p = p + l;
        _SNPRINTF_BEGIN(p, m);
        _snprintf(p, m, " ");
        _SNPRINTF_END(p, m);
    }

    _SNPRINTF_BEGIN(suffix, (sizeof suffix) - TRUNCATION_STATEMENT_LENGTH);
    _vsnprintf(suffix, (sizeof suffix) - TRUNCATION_STATEMENT_LENGTH, format, *ap);
    overflowLine = _SNPRINTF_IS_OVERFLOW(suffix, (sizeof suffix) - TRUNCATION_STATEMENT_LENGTH);
    _SNPRINTF_END(suffix, (sizeof suffix) - TRUNCATION_STATEMENT_LENGTH);
    if (overflowLine) {
        p = suffix;
        m = sizeof suffix;

        l = strlen(p);
        m = m - l;
        p = p + l;

        _SNPRINTF_BEGIN(p, m);
        _snprintf(p, m,
                  "...  The logging of this text was truncated because it is longer than %d bytes.",
                  (unsigned int)((sizeof suffix) - TRUNCATION_STATEMENT_LENGTH - 1));
        _SNPRINTF_END(p, m);
    }

    firstLine = TRUE;
    for (suffixLine = suffix; *suffixLine || firstLine; suffixLine = nextSuffixLine) {
        NextLine(suffixLine, &suffixLineEnd, &nextSuffixLine);
        suffixLineEnd[0] = 0;
        l = suffixLineEnd - suffixLine;
        for (; l > -1;) {
            if (l > LINE_WRAP_LENGTH) {
                c = suffixLine[LINE_WRAP_LENGTH + 1];
                suffixLine[LINE_WRAP_LENGTH + 1] = 0;
                LogLine(prefix, suffixLine, "\n");
                suffixLine[LINE_WRAP_LENGTH + 1] = c;
                suffixLine = suffixLine + LINE_WRAP_LENGTH + 1;
                l = suffixLineEnd - suffixLine;
            } else {
                LogLine(prefix, suffixLine, "\n");
                l = -1;
            }
        }
        firstLine = FALSE;
    }
}

int Log(const char *format, ...) {
    va_list ap;

    va_start(ap, format);
    LogVA(format, &ap);
    va_end(ap);

    return 0;
}

#define MAXLEN 4096
#define LINE (32 + 1)
void LogBinaryPacket(char *prefix, unsigned char *c, int len) {
    /* Each byte takes 3 characters plus the prefix of 200 plus 1 for the colon, so that's 96 plus
     * 200 plus 1. */
    char buffer[300];
    int j;
    BOOL truncated = FALSE;

    if (len > MAXLEN) {
        len = MAXLEN;
        truncated = TRUE;
    }
    Log("%s: length = %d%s", prefix, len, (truncated) ? " the hex dump will be truncated" : "");

    /* Avoid something that is just ridicuously long */
    StrCpy3(buffer, prefix, 200);
    strcat(buffer, ": ");

    for (j = 1; len > 0; j++) {
        if ((j % LINE) == 0) {
            Log("%s", buffer);
            /* Avoid something that is just ridicuously long */
            StrCpy3(buffer, prefix, 200);
            strcat(buffer, ": ");
        } else {
            sprintf(strchr(buffer, 0), " %02x", (unsigned int)(*((unsigned char *)c)));
            len--;
            c++;
        }
    }
    if (((j - 1) % LINE) != 0) {
        Log("%s", buffer);
    }
}

/* Returns TRUE if the request is valid; otherwise FALSE. If 'f' is unchanged, no attempt was made
 * to open the file. */
BOOL PushIncludeFileStack(struct USERINFO *uip,
                          struct FILEREADLINEBUFFER *frb,
                          char *fileName,
                          char *pseudoFileContent,
                          struct FILEINCLUSION *fileNodeTopOfStack,
                          struct FILEINCLUSION *fileNodeNewTopOfStack,
                          int *f) {
    int i;
    struct FILEINCLUSIONNAME *fileNameNode;
    struct FILEINCLUSIONNAME *fileNameNodeNext;
    struct FILEINCLUSIONNAME *fileNameNodePrevious;

    fileNodeNewTopOfStack->previous = fileNodeTopOfStack;
    fileNodeNewTopOfStack->fileName = fileName;
    fileNodeNewTopOfStack->lineNumber = 0;
    if (fileNodeTopOfStack != NULL) {
        fileNodeNewTopOfStack->depth = fileNodeTopOfStack->depth + 1;
        if (fileNodeNewTopOfStack->depth > 1000) {
            UsrLog(uip, "Possible infinite recursion, depth is %d, aborting.",
                   fileNodeTopOfStack->depth);
            return FALSE;
        }
        fileNameNode = fileNodeTopOfStack->fileNameListHead;
        fileNodeNewTopOfStack->fileNameListHead = fileNodeTopOfStack->fileNameListHead;
    } else {
        fileNodeNewTopOfStack->depth = 1;
        fileNameNode = NULL;
    }
    for (i = 1, fileNameNodePrevious = NULL; fileNameNode != NULL;
         i++, fileNameNodePrevious = fileNameNode, fileNameNode = fileNameNodeNext) {
        if (strcmp(fileName, fileNameNode->fileName) == 0) {
            break;
        }
        fileNameNodeNext = fileNameNode->next;
    }
    fileNodeNewTopOfStack->fileNameIndex = i;
    if (fileNameNode == NULL) {
        if (!(fileNameNode = calloc(1, sizeof(*fileNameNode))))
            PANIC;
        if (!(fileNameNode->fileName = strdup(fileName)))
            PANIC;
        fileNameNode->fileNameIndex = i;
        if (fileNameNodePrevious != NULL) {
            fileNameNodePrevious->next = fileNameNode;
        } else {
            /* The only way fileNameNodePrevious == NULL is if fileNodeTopOfStack == NULL; so,
             * fileNodeNewTopOfStack is also the bottom of the stack. */
            fileNodeNewTopOfStack->fileNameListHead = fileNameNode;
        }
    }

    InitializeFileReadLineBuffer(frb);
    frb->continuationChar = '\\';
    if (pseudoFileContent) {
        *f = -1;
        frb->pseudoFile = pseudoFileContent;
    } else {
        *f = SOPEN(fileName);
        if (*f < 0) {
            *f = -1;
            if ((uip != NULL) && (uip->debug) || (debugLevel > 1)) {
                int e = GetLastError();
                UsrLog(uip, "Unable to open %s: %d", fileName, e);
                SetErrno(e);
            }
            return FALSE;
        }
    }

    return TRUE;
}

#ifndef PATH_MAX
#define PATH_MAX 1024
#endif
void PopIncludeFileStack(struct USERINFO *uip,
                         struct FILEINCLUSION *fileNodeTopOfStack,
                         struct FILEINCLUSION *fileNodePreviousTopOfStack) {
    char cdw[PATH_MAX];
    struct FILEINCLUSIONNAME *fileNameNode;
    struct FILEINCLUSIONNAME *fileNameNodeNext;

    if ((fileNodePreviousTopOfStack == NULL) && (fileNodeTopOfStack != NULL)) {
        /* The last POP, report the fileNameList. */
        if (((uip != NULL) && (uip->debug) && (debugLevel >= 8000))) {
            UsrLog(uip, "");
            UsrLog(uip, "%" FMT_FILE_INDEX "s %" FMT_LINE_NUMBER "s", "FILE", "SOURCE");
            for (fileNameNode = fileNodeTopOfStack->fileNameListHead; fileNameNode != NULL;
                 fileNameNode = fileNameNodeNext) {
                UsrLog(uip, "%" FMT_FILE_INDEX "d %" FMT_LINE_NUMBER "s",
                       fileNameNode->fileNameIndex, fileNameNode->fileName);
                fileNameNodeNext = fileNameNode->next;
            }
            UsrLog(uip, "%" FMT_FILE_INDEX "s %" FMT_LINE_NUMBER "s", "CWD",
                   getcwd(cdw, sizeof(cdw) - 1) == NULL ? "**path too long**" : cdw);
        }
        /* The last POP, discard the fileNameList. */
        for (fileNameNode = fileNodeTopOfStack->fileNameListHead; fileNameNode != NULL;
             fileNameNode = fileNameNodeNext) {
            FreeThenNull(fileNameNode->fileName);
            fileNameNodeNext = fileNameNode->next;
            FreeThenNull(fileNameNode);
        }
    }
}

#ifndef LOG_FILE_NAME
/* Log file index number on each line. */
char *IncludeFileDisplayName(struct FILEINCLUSION *fileNode, char *buffer, size_t sizeofBuffer) {
    _SNPRINTF_BEGIN(buffer, sizeof(buffer));
    _snprintf(buffer + strlen(buffer), sizeofBuffer - strlen(buffer),
              "%" FMT_FILE_INDEX "d %" FMT_LINE_NUMBER "d", fileNode->fileNameIndex,
              fileNode->lineNumber);
    _SNPRINTF_END(buffer, sizeof(buffer));
    return buffer;
}
#else
/* Log file names on each line. */
char *IncludeFileDisplayName(struct FILEINCLUSION *fileNode, char *buffer, size_t sizeofBuffer) {
    if (fileNode->previous != NULL) {
        IncludeFileDisplayName(fileNode->previous, indicator, buffer, sizeofBuffer);
        _snprintf(buffer + strlen(buffer), sizeofBuffer - strlen(buffer), "in ");
    } else {
        _snprintf(buffer + strlen(buffer), sizeofBuffer - strlen(buffer), "In ");
    }
    _snprintf(buffer + strlen(buffer), sizeofBuffer - strlen(buffer), "%s at line %d ",
              fileNode->fileName, fileNode->lineNumber);
    return buffer;
}
#endif

void InitializeFileReadLineBuffer(struct FILEREADLINEBUFFER *frb) {
    frb->next = NULL;
    frb->lastCr = 0;
    frb->continuationChar = 0;
    frb->pseudoFile = NULL;
}

BOOL FileReadLine(int f, char *buffer, int max, struct FILEREADLINEBUFFER *frb) {
    /* This routine must not call Log() */
    int rc;
    BOOL any;
    char c;
    BOOL eatLf;
    char lastChar = 0;

    any = 0;
    /* Leave room for the final null by decrementing upfront */
    max--;

    if (frb && frb->pseudoFile) {
        if (*frb->pseudoFile == 0) {
            *buffer = 0;
            return 0;
        }

        for (; *frb->pseudoFile;) {
            if (*frb->pseudoFile == 13) {
                frb->pseudoFile++;
                if (*frb->pseudoFile == 10)
                    frb->pseudoFile++;
                if (frb->continuationChar == lastChar) {
                    buffer--;
                    max++;
                    lastChar = 0;
                    continue;
                } else {
                    break;
                }
            }

            if (*frb->pseudoFile == 10) {
                frb->pseudoFile++;
                if (frb->continuationChar == lastChar) {
                    buffer--;
                    max++;
                    lastChar = 0;
                    continue;
                } else {
                    break;
                }
            }

            lastChar = *(frb->pseudoFile);
            if (max > 0) {
                *buffer++ = lastChar;
                max--;
            }
            frb->pseudoFile++;
        }
        *buffer = 0;
        return 1;
    }

Continuation:
    /* The lastCr logic allows us to read files that use a CR only as a line separator,
       such as Mac files being used for text authentication on NT machines with a shared
       file system.
    */
    if (eatLf = (frb && frb->lastCr)) {
        frb->lastCr = 0;
    }

    for (;; eatLf = 0) {
        if (frb == NULL) {
            rc = UUreadFD(f, &c, 1);
            if (rc != 1)
                break;
        } else {
            if (frb->next == NULL || frb->next > frb->last) {
                rc = UUreadFD(f, frb->buffer, sizeof(frb->buffer));
                if (rc <= 0)
                    break;
                frb->next = frb->buffer;
                frb->last = frb->buffer + rc - 1;
            }
            c = *(frb->next++);
        }

        any = 1;
        if (c == 13 && frb) {
            frb->lastCr = 1;
            break;
        }
        if (c == 10) {
            if (eatLf)
                continue;
            break;
        }

        lastChar = c;
        if (max > 0) {
            *buffer++ = c;
            max--;
        }
    }
    *buffer = 0;
    if (frb && lastChar && frb->continuationChar == lastChar) {
        buffer--;
        max++;
        goto Continuation;
    }

    return any;
}

/**
 * Read until a new line detected on socket read
 * @param struct UUSOCKET *s The socket to read from
 * @param char *buffer A buffer to read characters into
 * @param int max Maximum characters to read
 * @see UURecv
 */
int ReadLine(struct UUSOCKET *s, char *buffer, int max) {
    int rc;
    BOOL any;
    char c;

    any = 0;
    /* Leave room for the final null by decrementing upfront */
    max--;
    for (;;) {
        // UURecv must handle character encodings, since we use ASCII
        rc = UURecv(s, &c, 1, 0);
        if (rc != 1)
            break;
        /*      printf("%c", c); */
        /*      fflush(stdout); */
        any = 1;

        // 13 is chariage return
        if (c == 13)
            continue;

        // 10 is a Line Feed
        if (c == 10)
            break;

        /* Since readline was called, we assume the caller intended us to read all the way to
         * newline */
        /* But, only store incoming chars if there is still buffer space left */
        if (max > 0) {
            *buffer++ = c;
            max--;
        }
    }
    *buffer = 0;

    return any;
}

int SendQuotedString(struct UUSOCKET *s, char *string) {
    int len;

    len = 2; /* Start off counting the opening and closing quotes */

    UUSend(s, "\"", 1, 0);

    for (; *string; string++) {
        if (*string == '"' || *string == '\\') {
            UUSend(s, "\\", 1, 0);
            len++;
        }
        UUSend(s, string, 1, 0);
        len++;
    }

    UUSend(s, "\"", 1, 0);

    return len;
}

int WriteLine(struct UUSOCKET *s, char *format, ...) {
    va_list ap;
    char buffer[2048];

    va_start(ap, format);
    _SNPRINTF_BEGIN(buffer, sizeof(buffer));
    _vsnprintf(buffer, sizeof(buffer), format, ap);
    _SNPRINTF_PANIC_OVERFLOW(buffer, sizeof(buffer));
    _SNPRINTF_END(buffer, sizeof(buffer));
    va_end(ap);

    return UUSendZ(s, buffer);
}

/**
 * write the <option/> tags for a selct box.  Example:
 *
 *     printSelectBoxOptions(s, "foo", "hello", "world", "foo", "bar");
 *
 * This would print: <option value="hello">world</option><option value="foo" selected>bar</option>
 *
 * @param s The socket to write option tags to
 * @param selected The value of option tag that should have "selected" attribtue added to.
 * @param argcount The number of key/value pairs
 * @param ... Key, Value pairs to be used for an <option/> display value (value) and actual value
 * (key).
 */
void printSelectBoxOptions(struct UUSOCKET *s, const char *selected, const int argcount, ...) {
    va_list args;
    char *key;
    char *val;
    int i;

    va_start(args, argcount);

    for (i = 0; i < argcount; i = i + 2) {
        key = va_arg(args, char *);
        val = va_arg(args, char *);
        char *temp = (strcmp(selected, key) == 0) ? "selected" : "";
        WriteLine(s, "<option value='%s' %s>%s</option>", key, temp, val);
    }

    va_end(args);
}

int SendHTMLEncoded(struct UUSOCKET *s, const char *str) {
    int len = 0;

    if (str == NULL)
        return 0;

    for (; *str; str++) {
        if (strchr("<>&\"'", *str) != NULL) {
            len += WriteLine(s, "&#%d;", *str);
        } else {
            UUSend(s, str, 1, 0);
            len++;
        }
    }
    return len;
}

int SendUrlEncoded(struct UUSOCKET *s, char *f) {
    int len = 0;

    if (f) {
        /* Encode characters that aren't reserved per RFC 1738 */
        for (; *f; f++) {
            if (*f == ' ') {
                UUSend(s, "+", 1, 0);
                len++;
            } else if (IsDigit(*f) || IsAlpha(*f) || *f == '.' || *f == '$' || *f == '_' ||
                       *f == '-') {
                UUSend(s, f, 1, 0);
                len++;
            } else {
                len += WriteLine(s, "%%%02x", (unsigned int)(*f & 0xff));
            }
        }
    }
    return len;
}

int SendField(struct UUSOCKET *s,
              char *name,
              char *value,
              char *label,
              char *type,
              int size,
              int maxlength,
              int flags) {
    int len = 0;

    if (type == NULL) {
        type = "text";
    }
    if (label) {
        len += WriteLine(s, "<tr><td><label for='%s'>%s</label>: </td><td>", name, label);
    }
    WriteLine(s, "<input name='%s' id='%s' type='%s' value='", name, name, type);
    if (value) {
        len += SendHTMLEncoded(s, value);
    }
    len += WriteLine(s, "'");
    if (label) {
        len += WriteLine(s, " id='%s'", name);
    }
    if (size) {
        len += WriteLine(s, " size='%d'", size);
    }
    if (maxlength) {
        len += WriteLine(s, " maxlength='%d'", maxlength);
    }
    len += WriteLine(s, ">");
    if (label) {
        len += WriteLine(s, "</td></tr>\n");
    }

    return len;
}

int SendHiddenField(struct UUSOCKET *s, char *name, char *value) {
    return SendField(s, name, value, NULL, "hidden", 0, 0, 0);
}

int SendTextField(struct UUSOCKET *s,
                  char *name,
                  char *value,
                  char *label,
                  int size,
                  int maxlength,
                  int flags) {
    return SendField(s, name, value, label, "text", size, maxlength, flags);
}

int SendPasswordField(struct UUSOCKET *s,
                      char *name,
                      char *value,
                      char *label,
                      int size,
                      int maxlength,
                      int flags) {
    return SendField(s, name, value, label, "password", size, maxlength, flags);
}

char *EncryptVar(char *f, struct DATABASE *b, char c) {
    struct ENCRYPTVAR *ev;
    char *input = NULL;
    char *output = NULL;
    unsigned char *hold = NULL;
    EVP_CIPHER_CTX *ctx = NULL;
    int updateLen, finalLen;

    for (ev = b->ev;; ev = ev->next) {
        if (ev == NULL)
            return NULL;
        if (strchr(ev->var, c))
            break;
    }

    if (!(input = strdup(f)))
        PANIC;
    Trim(input, TRIM_LEAD | TRIM_TRAIL);
    AllLower(input);

    /* Can't grow by more than 9 (1 for null, 8 for the possible pad */
    if (!(hold = malloc(strlen(input) + 9)))
        PANIC;

    if (!(ctx = EVP_CIPHER_CTX_new()))
        PANIC;
    EVP_CIPHER_CTX_init(ctx);
    EVP_EncryptInit(ctx, EVP_des_ede3_cbc(), ev->key, ev->ivec);

    /* Send the NULL as well; we check on receive to see if it is there */
    if (!EVP_EncryptUpdate(ctx, hold, &updateLen, (unsigned char *)input, strlen(input) + 1)) {
        goto Cleanup;
    }

    if (!EVP_EncryptFinal(ctx, hold + updateLen, &finalLen)) {
        goto Cleanup;
    }

    output = Encode64Binary(NULL, hold, updateLen + finalLen);

Cleanup:
    if (ctx) {
        EVP_CIPHER_CTX_free(ctx);
        ctx = NULL;
    }

    FreeThenNull(input);
    FreeThenNull(hold);

    return output;
}

void SendUrlEncodedEncrypted(struct UUSOCKET *s, char *f, struct DATABASE *b, char c) {
    char *output = NULL;

    output = EncryptVar(f, b, c);

    if (output) {
        SendUrlEncoded(s, output);
        FreeThenNull(output);
    } else {
        SendUrlEncoded(s, f);
    }

    return;
}

void SendObfuscatedEncrypted(struct UUSOCKET *s, char *f, struct DATABASE *b, char c) {
    if (f == NULL)
        return;

    if (b) {
        char *output = EncryptVar(f, b, c);

        if (output) {
            SendObfuscated(s, output, NULL);
            FreeThenNull(output);
            return;
        }
    }

    SendObfuscated(s, f, NULL);
}

char *PeriodsToHyphens(char *str) {
    if (str) {
        for (; str = strchr(str, '.');)
            *str = '-';
    }

    return str;
}

char *ASCIITrueOrFalse(BOOL state) {
    return state ? "true" : "false";
}

void HTTPCode(struct TRANSLATE *t, int code, BOOL body) {
    struct UUSOCKET *s = &t->ssocket;
    struct MSGS {
        int code;
        char *msg;
    };
    struct MSGS *msgp;
    static struct MSGS msgs[] = {200, "OK",
                                 301, "Moved permanently",
                                 302, "Moved temporarily",
                                 303, "See other",
                                 304, "Not modified",
                                 307, "Temporary redirect",
                                 400, "Bad request",
                                 403, "Forbidden",
                                 404, "Not found",
                                 405, "Method not allowed",
                                 413, "Request entity too large",
                                 414, "Request-URI Too Long",
                                 417, "Expectation failed",
                                 501, "Method not implemented",
                                 503, "Service unavailable",
                                 0,   NULL};
    char *msg;
    char httpDate[HTTPDATELENGTH];
    time_t now;

    /*
    if (t->clientVersionX1000 < 1100 && code == 303)
        code = 302;
    */

    /* The see other 303 code just doesn't seem to work right, so replace with 302 always */
    if (code == 303)
        code = 302;

    msg = NULL;

    for (msgp = msgs; msgp->code; msgp++) {
        if (code == msgp->code) {
            msg = msgp->msg;
            break;
        }
    }

    if (msg == NULL)
        msg = "unknown error";

    if (*t->version) {
        WriteLine(s, "%s %d %s\r\n", HTTPVERSION11, code, msg);
        WriteLine(s, "Date: %s\r\n", FormatHttpDate(httpDate, sizeof(httpDate), UUtime(&now)));
        if (serverHeader && *serverHeader)
            WriteLine(s, "Server: %s\r\n", serverHeader);
    }

    t->bodylessStatus = BodylessStatus(t, code);

    if (body) {
        if (t->bodylessStatus) {
            HeaderToBody(t);
        } else {
            if (*t->version)
                HTTPContentType(t, NULL, 1);
            WriteLine(s, "<H1>%d %s</H1>\r\n", code, msg);
        }
    }
    t->finalStatus = code;
}

void HTTPContentType(struct TRANSLATE *t, char *ct, BOOL endHeaders) {
    if (ct == NULL)
        ct = "text/html";

    /* HeaderToBody will look at t->html as a reason to enable gzip processing */
    if (strcmp(ct, "text/html") == 0)
        t->html = 1;

    if (t->version) {
        WriteLine(&t->ssocket, "Content-Type: %s%s%s\r\n", ct, (defaultCharset ? "; charset=" : ""),
                  (defaultCharset ? defaultCharset : ""));
        if (endHeaders)
            HeaderToBody(t);
    }
}

void SimpleRedirect(struct TRANSLATE *t, char *url, int redirectCode) {
    struct UUSOCKET *s = &t->ssocket;

    if (redirectCode == 0)
        redirectCode = 302;

    HTTPCode(t, redirectCode, 0);
    NoCacheHeaders(s);
    if (t->version) {
        UUSendZ(s, "Location: ");
        UUSendZCRLF(s, url);
        HeaderToBody(t);
    }

    UUSendZ(s, "To access this site, go <a href='");
    UUSendZ(s, url);
    UUSendZ(s, "'>here</a>\r\n");
}

void SendURL(struct UUSOCKET *s, char *host, PORT port, char *url, char useSsl, char htmlEncoded) {
    int len;

    len = WriteLine(s, "http%s://", (useSsl ? "s" : ""));
    UUSendZ(s, host);
    SendIfNonDefaultPort(s, port, useSsl);
    /* The next two lines seem silly; should be able to just get rid of sending slash and not
    look further, but I'm not sure if there is some darker secret here, so leave it alone */
    UUSendZ(s, "/");
    if (*url == '/')
        url++;
    if (*url) {
        if (htmlEncoded) {
            SendHTMLEncoded(s, url);
        } else {
            UUSendZ(s, url);
        }
    }
}

void GenericRedirect(struct TRANSLATE *t,
                     char *host,
                     PORT port,
                     char *url,
                     char useSsl,
                     int redirectCode) {
    struct UUSOCKET *s;

    s = &t->ssocket;

    if (redirectCode == 0)
        redirectCode = 302;

    HTTPCode(t, redirectCode, 0);
    NoCacheHeaders(s);
    if (t->version) {
        UUSendZ(s, "Location: ");
        SendURL(s, host, port, url, useSsl, 0);
        UUSendCRLF(s);
        HeaderToBody(t);
    }

    UUSendZ(s, "To access this site, go <a href='");
    SendURL(s, host, port, url, useSsl, 1);
    UUSendZ(s, "'>here</a>\r\n");
}

void MyNameRedirect(struct TRANSLATE *t, char *url, int redirectCode) {
    if (primaryLoginPortSsl && (t->useSsl || primaryLoginPort == 0))
        GenericRedirect(t, myHttpsName, primaryLoginPortSsl, url, 1, redirectCode);
    else
        GenericRedirect(t, myHttpName, primaryLoginPort, url, 0, redirectCode);
}

void CheckReopenAccessFile(struct LOGSPU *logSPU, BOOL holdingMAccessFileMutex) {
    char *fn;

    if (logSPU == NULL) {
        if (logSPUs == NULL)
            return;
        logSPU = logSPUs;
    }

    if (logSPU == logSPUs && logSPU->strftimeFilename == 0 && guardian->dontReopenAccessFile)
        return;

    guardian->mainLocation = 2000;

    /* Everything before here uses return to go back, but after must use goto Finished
       to release mutex if we acquired it
    */

    if (holdingMAccessFileMutex == 0)
        UUAcquireMutex(&mAccessFile);

    guardian->mainLocation = 2001;

    if (logSPU == logSPUs && guardian->dontReopenAccessFile == 0) {
        struct LOGSPU *p;
        for (p = logSPUs; p; p = p->next) {
            if (p->file) {
                fclose(p->file);
                p->file = NULL;
            }
        }
    }

    guardian->mainLocation = 2002;

    fn = LogSPUActiveFilename(logSPU, 1);

    guardian->mainLocation = 2003;

    if (logSPU->file != NULL && (logSPU != logSPUs || guardian->dontReopenAccessFile) &&
        logSPU->activeFilenameChanged == 0)
        goto Finished;

    guardian->mainLocation = 2004;

    logSPU->activeFilenameChanged = 0;

    if (logSPU->file) {
        fclose(logSPU->file);
        logSPU->file = NULL;
    }

    guardian->mainLocation = 2005;

    if (logSPU == logSPUs) {
        guardian->renameStatus = 0;
        if (guardian->renameLog[0]) {
            /* don't allow an existing file to be destroyed */
            if (FileExists(guardian->renameLog))
                guardian->renameStatus = EEXIST;
            else {
                if (rename(fn, guardian->renameLog))
                    guardian->renameStatus = errno;
            }
            guardian->renameLog[0] = 0;
        }
    }

    guardian->mainLocation = 2006;

    logSPU->file = fopen(fn, "a"); /* O_CREAT | O_WRONLY | O_APPEND, defaultFileMode); */
    if (logSPU->file == NULL)
        Log("Unable to append %s error %d", fn, errno);

    guardian->mainLocation = 2007;

    if (logSPU == logSPUs)
        guardian->dontReopenAccessFile = 1;

Finished:
    guardian->mainLocation = 2008;
    if (holdingMAccessFileMutex == 0)
        UUReleaseMutex(&mAccessFile);
}

int qfputc(char c, FILE *f) {
    switch (c) {
    case '\b':
        fputs("\\b", f);
        break;
    case '\n':
        fputs("\\n", f);
        break;
    case '\r':
        fputs("\\r", f);
        break;
    case '\t':
        fputs("\\t", f);
        break;
    case '\v':
        fputs("\\v", f);
        break;
    case '\\':
    case '"':
        fputc('\\', f);
        fputc(c, f);
        break;
    default:
        if (c >= ' ' && c <= '~') {
            fputc(c, f);
        } else {
            /* Consider this as %% %02x x, so initial %, two letter hex of c, then x */
            fprintf(f, "%%%02xx", (unsigned int)(c & 0xff));
        }
    }
    return 0;
}

int qfputs(char *s, FILE *f) {
    for (; *s; s++) {
        qfputc(*s, f);
    }
    return 0;
}

void LogTagApprove(struct TRANSLATE *t, char *url) {
    struct LOGTAGMAP *ltm;
    char *urlCS = NULL;
    char *urlLower = NULL;
    char *tagLower = NULL;
    PORT defaultPort;

    if (t->logTagApproved) {
        return;
    }

    if (optionCaptureSpur && t->logTag && strlen(t->logTag) > 1 && *(t->logTag) == 'r' &&
        strlen(t->logTag) < 20 && IsAllDigits(t->logTag + 1)) {
        t->logTagSpur = 1;
        goto Approved;
    }

    if (logTagMaps == NULL) {
        return;
    }

    if (url) {
        urlCS = url;
    } else {
        if (t->host == NULL || (t->host)->myDb == NULL)
            goto Unapproved;

        /* The 20 account for up to 8 in https://, the : for the port, 5 for the port, the null, and
         * 5 to spare */
        if (!(urlCS = malloc(strlen((t->host)->hostname) + strlen(t->urlCopy) + 20)))
            PANIC;

        if (t->host->ftpgw) {
            strcpy(urlCS, "ftp");
            defaultPort = 21;
        } else if (t->host->useSsl) {
            strcpy(urlCS, "https");
            defaultPort = 443;
        } else {
            strcpy(urlCS, "http");
            defaultPort = 80;
        }

        sprintf(strchr(urlCS, 0), "://%s", t->host->hostname);
        if (t->host->remport != defaultPort)
            sprintf(strchr(urlCS, 0), ":%d", t->host->remport);
        strcat(urlCS, t->urlCopy);
    }

    for (ltm = logTagMaps; ltm; ltm = ltm->next) {
        /* If this is -Authorize, then the comparison URL is optional */
        if (ltm->url && *ltm->url) {
            if (ltm->urlRE) {
                char *compUrl;
                if (ltm->caseSensitive) {
                    compUrl = urlCS;
                } else {
                    if (urlLower == NULL) {
                        if (!(urlLower = strdup(urlCS)))
                            PANIC;
                        AllLower(urlLower);
                    }
                    compUrl = urlLower;
                }
                if (xmlRegexpExec(ltm->urlRE, BAD_CAST compUrl) != 1)
                    continue;
            } else {
                if (!(ltm->caseSensitive ? WildCompareCaseSensitive(urlCS, ltm->url)
                                         : WildCompare(urlCS, ltm->url)))
                    continue;
            }
        }

        if (ltm->approve) {
            if (t->logTag == NULL)
                continue;
            if (ltm->tagRE) {
                if (xmlRegexpExec(ltm->tagRE, BAD_CAST t->logTag) != 1) {
                    continue;
                }
            } else {
                if (!WildCompareCaseSensitive(t->logTag, ltm->logTag)) {
                    continue;
                }
            }
            /* We authorized the existing tag, so no need to drop/duplicate;
               just move on to approval.
            */
            goto Approved;
        }

        FreeThenNull(t->logTag);
        if (!(t->logTag = strdup(ltm->logTag)))
            PANIC;
        goto Approved;
    }

Unapproved:
    FreeThenNull(t->logTag);
    t->logTagApproved = 2;
    goto Done;

Approved:
    t->logTagApproved = 1;
    goto Done;

Done:
    FreeThenNull(urlLower);
    FreeThenNull(tagLower);
    /* urlCS can point to url, which is static, or to a newly formed string.
       We only free if urlCS is not pointing to the passed in "url"
    */
    if (urlCS != url)
        FreeThenNull(urlCS);
}

char *TimeZoneOffset(char *s, time_t *when) {
    struct tm gtm, ltm;
    int offset;
    char gymd[16];
    char lymd[16];
    char sign;

    UUgmtime_r(when, &gtm);
    UUlocaltime_r(when, &ltm);

    offset = (ltm.tm_hour * 60 + ltm.tm_min) - (gtm.tm_hour * 60 + gtm.tm_min);
    sprintf(gymd, "%04d%02d%02d", gtm.tm_year + 1900, gtm.tm_mon + 1, gtm.tm_mday);
    sprintf(lymd, "%04d%02d%02d", ltm.tm_year + 1900, ltm.tm_mon + 1, ltm.tm_mday);
    // This adds or subtracts a day if necessary based on how the UTC version
    // of the date and the local version of the date vary.
    offset = offset + strcmp(lymd, gymd) * 24 * 60;
    // Can't compute the proper minutes unless the offset is positive
    // so store the appropriate sign and force value positive if appropriate.
    if (offset < 0) {
        sign = '-';
        offset = -offset;
    } else {
        sign = '+';
    }
    sprintf(s, "%c%02d%02d", sign, offset / 60, offset % 60);
    return (s);
}

void LogRequest(struct TRANSLATE *t,
                struct LOGSPU *logSPU,
                char *url,
                char *accessType,
                struct DATABASE *b) {
    struct tm *tm;
    time_t now;
    char *p;
    char *braceStart, *braceEnd;
    char braceVal[256];
    int braceLen;
    char *timeFormat;
    char outTimeFormat[128];
    char *field;
    char *uField;
    int i;
    struct LOGFILTER *lf;
    struct tm rtm;
    char sport[6];
    const char *hostArray[20], **a;
    char ipBuffer[INET6_ADDRSTRLEN];
    char *protocol = "http";
    char *format;
    char *refererUrl = NULL;

    if (accessType == NULL || *accessType == 0)
        accessType = "-";

    format = logSPU->format;
    if (format == NULL)
        format = LOGFORMAT;

    if (format == NULL || *format == 0)
        goto SkipLogging;

    if (*(t->method) == 0)
        goto SkipLogging;

    if (url) {
        if (strnicmp(url, "http://", 7) == 0)
            protocol = "http";
        else if (strnicmp(url, "https://", 8) == 0)
            protocol = "https";
        else if (strnicmp(url, "ftp://", 6) == 0)
            protocol = "ftp";
        else
            protocol = "";
    }

    if (t->host && url == NULL) {
        b = (t->host)->myDb;
        if ((t->host)->ftpgw)
            protocol = "ftp";
        else if ((t->host != hosts && (t->host)->useSsl) || (t->host == hosts && t->useSsl))
            protocol = "https";

        sprintf(sport, "%d", (t->host)->remport);
        a = hostArray;
        *a++ = protocol;
        *a++ = "://";
        *a++ = (t->host)->hostname;
        *a++ = ":";
        *a++ = sport;
        *a++ = t->urlCopy;
        *a++ = NULL;
        for (i = 0, lf = logFilters; i < cLogFilters; i++, lf++) {
            if (WildCompareArray(hostArray, lf->filter, 0))
                goto SkipLogging;
        }
    }

    UUtime(&now);
    char tzo[16], tzs[20];
    TimeZoneOffset(tzo, &now);
    sprintf(tzs, " %s]", tzo);

    tm = UUlocaltime_r(&now, &rtm);
    UUAcquireMutex(&mAccessFile);

    CheckReopenAccessFile(logSPU, 1);

    if (logSPU->file == NULL)
        goto NoLogFile;

    for (p = format; *p; p++) {
        if (*p == '\\') {
            if (*(p + 1) == 'n') {
                fputc('\n', logSPU->file);
                p++;
                continue;
            }
            if (*(p + 1) == 't') {
                fputc('\t', logSPU->file);
                p++;
                continue;
            }
        }
        if (*p != '%') {
            fputc(*p, logSPU->file);
            continue;
        }
        p++;
        if (*p == 0)
            break;

        if (*p != '{') {
            braceStart = braceEnd = NULL;
            braceVal[0] = 0;
            braceLen = 0;
        } else {
            braceStart = p + 1;
            braceEnd = strchr(braceStart, '}');

            /* If no end of brace, assume misformed and stop logging */
            if (braceEnd == NULL) {
                break;
            }
            p = braceEnd + 1;
            if (*p == 0)
                break;
            braceLen = (braceEnd - braceStart) + 1;
            if (braceLen <= 1)
                braceVal[0] = 0;
            else {
                if (braceLen > sizeof(braceVal))
                    braceLen = sizeof(braceVal);
                StrCpy3(braceVal, braceStart, braceLen);
                braceVal[braceLen - 1] = 0;
                braceLen = braceEnd - braceStart;
            }
        }

        if (*p == '%') {
            fputc(*p, logSPU->file);
            continue;
        }

        if (*p == 'l') { /* Username got by identd (which we don't get) */
            fputs("-", logSPU->file);
            continue;
        }

        if (*p == 'T') { /* time to serve request */
            fprintf(logSPU->file, "%" PRIdMAX "", (intmax_t)(now - t->activated));
            continue;
        }

        if (*p == 'P') {
            fprintf(logSPU->file, "%d", t->sequence);
            continue;
        }

        if (*p == 'b') { /* Bytes sent */
            fprintf(logSPU->file, "%" PRIuMAX, (t->ssocket).sendCount);
            continue;
        }

        if (*p == 'm') {
            qfputs(t->method, logSPU->file);
            continue;
        }

        if (*p == 'v') {
            if (url) {
                char *v, *ve;
                if (*protocol == 0) {
                    fputc('-', logSPU->file);
                } else {
                    v = SkipFTPHTTPslashesAt(url, NULL, NULL);
                    ParseHost(v, NULL, &ve, 0);
                    if (v == ve)
                        fputc('-', logSPU->file);
                    else {
                        for (; v < ve; v++) {
                            qfputc(*v, logSPU->file);
                        }
                    }
                }
            } else {
                qfputs((t->host)->hostname, logSPU->file);
                fprintf(logSPU->file, ":%d", t->logPort);
            }
            continue;
        }

        if (*p == 'r' || *p == 'U') { /* 'r' first line of request OR 'U' URL */
            if (*p == 'r') {
                qfputs(t->method, logSPU->file);
                fputs(" ", logSPU->file);
            }

            if (url) {
                qfputs(url, logSPU->file);
            } else {
                qfputs(protocol, logSPU->file);
                fputs("://", logSPU->file);
                qfputs((t->host)->hostname, logSPU->file);
                fprintf(logSPU->file, ":%d", t->logPort);
                qfputs(t->urlCopy, logSPU->file);
            }

            if (*p == 'r') {
                fputs(" ", logSPU->file);
                qfputs(t->version, logSPU->file);
            }

            continue;
        }

        if (*p == 'u') { /* Remote user*/
            uField = NULL;
            if (usageLogSession && t->session != NULL)
                uField = (t->session)->key;
            else if ((usageLogUser & ULULOGUSER) != 0 && t->session != NULL &&
                     (t->session)->logUserBrief != NULL)
                uField = (t->session)->logUserBrief;

            if (uField == NULL || *uField == 0)
                uField = "-";

            qfputs(uField, logSPU->file);
            continue;
        }

        if (*p == 's') { /* Status */
            fprintf(logSPU->file, "%d", t->finalStatus);
            continue;
        }

        if (*p == 'h' || *p == 'a') { /* Address */
            ipToStr(&t->sin_addr, ipBuffer, sizeof ipBuffer);
            qfputs(ipBuffer, logSPU->file);
            continue;
        }

        if (*p == 'e') {
            char *val = ExpressionValue(t, NULL, NULL, braceVal);
            if (val) {
                qfputs(val, logSPU->file);
                FreeThenNull(val);
            }
            continue;
        }

        if (*p == 'i') {
            char urlPart[128];

            field = NULL;

            if (stricmp(braceVal, "ezproxy-groups") == 0) {
                struct GROUP *g;
                BOOL any = 0;
                if (t->session) {
                    for (g = groups; g; g = g->next) {
                        if (GroupMaskOverlap((t->session)->gm, g->gm)) {
                            if (any)
                                fputc('+', logSPU->file);
                            else
                                any = 1;
                            qfputs(g->name, logSPU->file);
                        }
                    }
                }
                if (any == 0)
                    qfputc('-', logSPU->file);
                continue;
            }

            if (strnicmp(braceVal, "ezproxy-url", 11) == 0) {
                int part = atoi(braceVal + 11);
                char *p, *q, *e;

                urlPart[0] = 0;
                if (part) {
                    if (url) {
                        p = SkipFTPHTTPslashesAt(url, NULL, NULL);
                        p = strchr(p, '/');
                        if (p == NULL)
                            p = "";
                    } else
                        p = t->urlCopy;

                    for (; *p; p++) {
                        if (*p == '?') {
                            break;
                        }
                        if (*p == '/') {
                            part--;
                            if (part == 0) {
                                p++;
                                break;
                            }
                        }
                    }

                    for (q = urlPart, e = urlPart + sizeof(urlPart) - 1;
                         *p && *p != '/' && *p != '?' && q < e;) {
                        *q++ = *p++;
                    }
                    *q = 0;
                }
                qfputs(urlPart, logSPU->file);
                continue;
            } else if (stricmp(braceVal, "ezproxy-session") == 0) {
                if (t->session != NULL)
                    field = (t->session)->key;
            } else if (stricmp(braceVal, "ezproxy-spuaccess") == 0) {
                field = accessType;
            } else if (stricmp(braceVal, "ezproxy-tag") == 0) {
                LogTagApprove(t, url);
                field = t->logTag;
            } else if (strnicmp(braceVal, "ezproxy-usrvar", 14) == 0) {
                if (t->session && strlen(braceVal) == 15 && IsDigit(*(braceVal + 14)))
                    field = (t->session)->vars[*(braceVal + 14) - '0'];
            } else if (strnicmp(braceVal, "ezproxy-dbvar", 13) == 0) {
                if (b && strlen(braceVal) == 14 && IsDigit(*(braceVal + 13)))
                    field = b->vars[*(braceVal + 13) - '0'];
            } else if (stricmp(braceVal, "ezproxy-protocol") == 0) {
                field = protocol;
            } else if (stricmp(braceVal, "ezproxy-spur") == 0 && optionCaptureSpur) {
                refererUrl = SpurGetRefererUrl(t->logTag);
                field = refererUrl;
            } else {
                if ((field = FindField(braceVal, t->requestHeaders)))
                    LinkFields(field);
            }
            qfputs(field && *field ? field : "-", logSPU->file);
            FreeThenNull(refererUrl);
            continue;
        }

        if (*p == 't') {
            if (braceVal[0] == 0)
                timeFormat = "[%d/%b/%Y:%H:%M:%S";
            else
                timeFormat = braceVal;
            strftime(outTimeFormat, sizeof(outTimeFormat) - 1, timeFormat, tm);
            qfputs(outTimeFormat, logSPU->file);
            if (braceVal[0] == 0) {
                fputs(tzs, logSPU->file);
            }
            continue;
        }

        fputc('%', logSPU->file);
        fputc(*p, logSPU->file);
    }
    if (*format)
        fputs("\n", logSPU->file);

    fflush(logSPU->file);

NoLogFile:
    UUReleaseMutex(&mAccessFile);

SkipLogging:;
}

void LogSPUs(struct TRANSLATE *t, char *url, char *accessType, struct DATABASE *b) {
    struct LOGSPU *p;

    if (url == NULL)
        return;

    for (p = logSPUs->next; p; p = p->next) {
        /*
        if (p->file < 0) {
            p->file = UUopenCreateFD(p->filename, O_CREAT | O_WRONLY | O_APPEND, defaultFileMode);
            if (p->file < 0) {
                Log("LogSPU unable to open %d: %s", errno, p->filename);
                continue;
            }
        }
        */
        LogRequest(t, p, url, accessType, b);
    }
}

char *NextField(char *buffer) {
    buffer = strchr(buffer, 0) + 1;
    return buffer;
}

// Link all continuation lines into one long line.
char *LinkField(char *buffer) {
    char *p;

    if (buffer == NULL || *buffer == 0)
        return buffer;

    for (p = buffer; p = NextField(p), *p && IsWS(*p); *(p - 1) = ' ')
        ;  // replace a zero byte with a blank if whitespace follows the zero byte.

    return p;
}

void LinkFields(char *buffer) {
    char *p;

    for (p = buffer; *p; p = LinkField(p))
        ;
}

char *FindField(const char *field, char *buffer) {
    size_t len = strlen(field);
    char *p;

    if (buffer == NULL)
        return NULL;

    for (p = buffer; *p; p = NextField(p)) {
        if (IsWS(*p))
            continue;
        if (strlen(p) < len + 1)
            continue;
        if (strnicmp(p, field, len) != 0)
            continue;
        p = p + len;
        if (*p++ != ':')
            continue;
        while (IsWS(*p))
            p++;
        return p;
    }
    return NULL;
}

/*
    From the HTTP RFC:

    Multiple message-header fields with the same field-name MAY
    be present in a message if and only if the entire
    field-value for that header field is defined as a
    comma-separated list [i.e., #(values)].  It MUST be possible
    to combine the multiple header fields into one "field-name:
    field-value" pair, without changing the semantics of the
    message, by appending each subsequent field-value to the
    first, each separated by a comma. The order in which header
    fields with the same field-name are received is therefore
    significant to the interpretation of the combined field
    value, and thus a proxy MUST NOT change the order of these
    field values when a message is forwarded.

    We start numbering them at 0.  indexVal = 0 is the first one.
 */
char *FindFieldN(const char *field, char *buffer, int indexVal) {
    int indexCurrent = 0;
    size_t len = strlen(field);
    char *p;

    if (buffer == NULL)
        return NULL;

    for (p = buffer; *p; p = NextField(p)) {
        if (IsWS(*p))
            continue;
        if (strlen(p) < len + 1)
            continue;
        if (strnicmp(p, field, len) != 0)
            continue;
        p = p + len;
        if (*p++ != ':')
            continue;
        while (IsWS(*p))
            p++;
        if (indexCurrent++ == indexVal) {
            return p;
        }
    }
    return NULL;
}

/* Raw transfer from socket src to socket dst using t->buffer for intermediate storage */
/* If max is 0, transfer until eof */
int RawTransfer(struct TRANSLATE *t,
                struct UUSOCKET *dst,
                struct UUSOCKET *src,
                intmax_t max,
                BOOL addCrLf) {
    int len;
    time_t now, lastChange;
    char *recvPtr;
    char *sendPtr;
    char *endPtr;

    intmax_t recvMax;
    intmax_t sendMax;
    intmax_t remain;

    struct UUPOLLFD pfds[2];
    int pfdc;

    recvMax = sendMax = max;

    recvPtr = sendPtr = t->buffer;
    /* Hold back two bytes for the CR/LF */
    endPtr = t->buffer + sizeof(t->buffer) - 2;

    UUtime(&lastChange);
    for (;;) {
        if (!UUValidSocketSend(dst)) {
            if (debugLevel >= 50)
                Log("RawTransfer dst went invalid");
            return 0;
        }

        UUtime(&now);
        if (now - lastChange > 60)
            return 0;

        if (recvMax > 0) {
            if (!UUValidSocketRecv(src)) {
                if (debugLevel >= 50)
                    Log("RawTransfer src went invalid");
                return 0;
            }
            remain = endPtr - recvPtr;
            if (remain > 0) {
                /* If there is anything pending to send, don't block here */
                len = UURecv(src, recvPtr, remain, UUSNOWAIT);
                if (len > 0) {
                    if (debugLevel >= 50)
                        Log("RawTransfer receive %d", len);
                    if (len > recvMax) {
                        if (debugLevel >= 50)
                            Log("RawTransfer receive adjusted to %d", len);
                        len = recvMax;
                    }
                    UUtime(&lastChange);
                    recvPtr += len;
                    recvMax -= len;
                    if (recvMax == 0 && addCrLf) {
                        /* Use those two bytes we saved up above */
                        endPtr += 2;
                        *recvPtr++ = 13;
                        *recvPtr++ = 10;
                        sendMax += 2;
                    }
                }
            }
        }

        if (recvPtr != sendPtr || UUSendDataWaiting(dst)) {
            /* If more to receive and room to receive it, don't block here */
            len = recvPtr - sendPtr;
            len = UUSend(dst, sendPtr, len, UUSNOWAIT);
            if (len > 0) {
                if (debugLevel >= 50)
                    Log("RawTransfer send %d", len);
                UUtime(&lastChange);
                sendMax -= len;
                if (sendMax <= 0) {
                    if (debugLevel >= 50)
                        Log("RawTransfer complete");
                    UUFlush(dst);
                    break;
                }
                remain = (recvPtr - sendPtr) - len;
                if (remain > 0)
                    memmove(sendPtr, sendPtr + len, remain);
                recvPtr -= len;
                /* Since things have shifted around, loop back around again
                   without a poll to see if there is more to do immediately
                */
                continue;
            }
        }

        memset(&pfds, 0, sizeof(pfds));
        pfdc = 0;
        if (recvMax > 0 && recvPtr != endPtr) {
            pfds[pfdc].fd = src->o;
            if (src->socketType != STSSL || src->lastRecvWant != UUWANTSEND) {
                pfds[pfdc].events = UUPOLLIN;
            } else {
                pfds[pfdc].events = UUPOLLOUT;
            }
            pfdc++;
        }
        if (recvPtr != sendPtr) {
            pfds[pfdc].fd = dst->o;
            if (dst->socketType != STSSL || dst->lastSendWant != UUWANTRECV) {
                pfds[pfdc].events = UUPOLLOUT;
            } else {
                pfds[pfdc].events = UUPOLLIN;
            }
            pfdc++;
        }
        UUPoll(&pfds[0], pfdc, 5000);
    }

    return 1;
}

/*
Prior to standardization there was ioctl(...FIONBIO...) and fcntl(...O_NDELAY...), but these behaved
inconsistently between systems, and even within the same system. For example, it was common for
FIONBIO to work on sockets and O_NDELAY to work on ttys, with a lot of inconsistency for things like
pipes, fifos, and devices. And if you didn't know what kind of file descriptor you had, you'd have
to set both to be sure. But in addition, a non-blocking read with no data available was also
indicated inconsistently; depending on the OS and the type of file descriptor the read may return 0,
or -1 with errno EAGAIN, or -1 with errno EWOULDBLOCK. Even today, setting FIONBIO or O_NDELAY on
Solaris causes a read with no data to return 0 on a tty or pipe, or -1 with errno EAGAIN on a
socket. However 0 is ambiguous since it is also returned for EOF.

POSIX addressed this with the introduction of O_NONBLOCK, which has standardized behavior across
different systems and file descriptor types. Because existing systems usually want to avoid any
changes to behavior which might break backward compatibility, POSIX defined a new flag rather than
mandating specific behavior for one of the others. Some systems like Linux treat all 3 the same, and
also define EAGAIN and EWOULDBLOCK to the same value, but systems wishing to maintain some other
legacy behavior for backward compatibility can do so when the older mechanisms are used.

New programs should use fcntl(...O_NONBLOCK...), as standardized by POSIX.
 */
void SocketNonBlocking(SOCKET s) {
#ifdef FIONBIO
#ifdef WIN32
    u_long arg;
#else
    int arg;
#endif

    arg = 1;
    /* The Windows/BSD way. */
    ioctlsocket(s, FIONBIO, &arg);
#else
    /* The POSIX/LSB way. */
    fcntl(s, F_SETFL, O_NONBLOCK);  // set to non-blocking
#endif
}

void SocketBlocking(SOCKET s) {
#ifdef FIONBIO
#ifdef WIN32
    u_long arg;
#else
    int arg;
#endif

    arg = 0;
    /* The Windows/BSD way. */
    ioctlsocket(s, FIONBIO, &arg);
#else
    /* The POSIX/LSB way. */
    fcntl(s, F_SETFL, O_ASYNC);  // set to asynchronous I/O
#endif
}

void SocketSetBufferSize(SOCKET s) {
    if (receiveBufferSize) {
        if (setsockopt(s, SOL_SOCKET, SO_RCVBUF, (void *)&receiveBufferSize,
                       sizeof(receiveBufferSize)) == SOCKET_ERROR) {
            Log("setsockopt SO_RCVBUF error %d", socket_errno);
        }
    }

    if (sendBufferSize) {
        if (setsockopt(s, SOL_SOCKET, SO_SNDBUF, (void *)&sendBufferSize, sizeof(sendBufferSize)) ==
            SOCKET_ERROR) {
            Log("setsockopt SO_SNDBUF error %d", socket_errno);
        }
    }
}

void SocketDisableNagle(SOCKET s) {
    int disable = 1;
    setsockopt(s, IPPROTO_TCP, TCP_NODELAY, (char *)&disable, sizeof(disable));
}

void FlushSocket(SOCKET s) {
    int len;
    int got;
    char dummy[64];

    SocketNonBlocking(s);

#ifdef WIN32
    unsigned long remaining;
    ioctlsocket(s, FIONREAD, &remaining);
#else
    int remaining;
    ioctl(s, FIONREAD, &remaining);
#endif

    for (;;) {
        if (remaining == 0)
            return;
        while (remaining > 0) {
            len = sizeof(dummy);
            if (len > remaining)
                len = remaining;
            /* Don't use UUrecv here since the need to flush the input is somewhat custom */
            got = recv(s, dummy, len, 0);
            if (got <= 0)
                return;
            remaining -= got;
        }
    }
}

#ifdef USEPOLL
void RawRecv(struct TRANSLATE *t,
             int *maxRecv,
             int *maxSend,
             struct pollfd *currPfds,
             unsigned int *currPfdc)
#else
void RawRecv(struct TRANSLATE *t, int *maxRecv, int *maxSend, fd_set *currRfds, fd_set *currWfds)
#endif
{
    struct UUSOCKET *w = t->wsocketPtr;
    int len1, len2, len;

    if (t->rawReadEof)
        return;

    if (!UUValidSocketRecv(w) || *maxRecv == 0) {
        if (debugLevel > 500) {
            Log("RawRecv 1");
        }
        return;
    }

    if (UURecvDataWaiting(w))
        goto Recv;

    if (t->wsocketPtr->recvChunking == 99) {
        t->rawReadEof = 1;
        return;
    }

    if (w->socketType != STSSL) {
        if (
#ifdef USEPOLL
            (PollRevents(currPfds, *currPfdc, w->o) & (POLLIN | POLLERR | POLLHUP | POLLNVAL)) != 0
#else
            FD_ISSET(w->o, currRfds)
#endif
        )
            goto Recv;
        if (debugLevel > 500) {
            Log("RawRecv 2");
        }

        return;
    }

    if (w->sslLast != SSLLASTRECV)
        goto Recv;

    if (w->sslLastStatus == SSL_ERROR_WANT_READ) {
        if (
#ifdef USEPOLL
            (PollRevents(currPfds, *currPfdc, w->o) & (POLLIN | POLLERR | POLLHUP | POLLNVAL)) != 0
#else
            FD_ISSET(w->o, currRfds)
#endif
        )
            goto Recv;
        if (debugLevel > 500) {
            Log("RawRecv 3");
        }

        return;
    }

    if (w->sslLastStatus == SSL_ERROR_WANT_WRITE) {
        if (
#ifdef USEPOLL
            (PollRevents(currPfds, *currPfdc, w->o) & (POLLOUT | POLLERR | POLLHUP | POLLNVAL)) != 0
#else
            FD_ISSET(w->o, currWfds)
#endif
        )
            goto Recv;
        if (debugLevel > 500) {
            Log("RawRecv 4");
        }

        return;
    }

    /* Now falling into Recv based on SSL connection with unknown error status */

Recv:

    len1 = MAXBUFFER - t->rawReadOfs;
    if (len1 > *maxRecv) {
        len1 = *maxRecv;
        len2 = 0;
    } else {
        len2 = *maxRecv - len1;
    }

    if (debugLevel > 5)
        printf("Try receive for %d (%d/%d/%d) get ", *maxRecv, len1, len2, w->o);
    len = UURecv2(w, t->buffer + t->rawReadOfs, len1, t->buffer, len2, UUSNOWAIT);
    if (debugLevel > 5)
        printf("len %d err %d\n", len, socket_errno);
    if (len > 0) {
        UUtime(&t->rawLast);
        t->rawReadOfs += len;
        if (t->rawReadOfs >= MAXBUFFER) {
            t->rawReadOfs -= MAXBUFFER;
            t->rawReadPage++;
        }
        /* eof is now track globally based on recvChunkRemain
        if (t->rawReadRemain > 0) {
            t->rawReadRemain -= len;
            if (t->rawReadRemain == 0) {
                t->rawReadEof = 1;
            }
        }
        */
        /* Note changes to maxRecv and maxSend as a result of this read */

        *maxRecv -= len;
        *maxSend += len;
    } else if (len == 0) {
        t->rawReadEof = 1;
    } else if (socket_errno != WSAEWOULDBLOCK) {
        t->rawReadEof = 1;
        UUStopSocket(w, 0);
    }
}

#ifdef USEPOLL
void RawSend(struct TRANSLATE *t,
             int *maxRecv,
             int *maxSend,
             struct pollfd *currPfds,
             unsigned int *currPfdc)
#else
void RawSend(struct TRANSLATE *t, int *maxRecv, int *maxSend, fd_set *currRfds, fd_set *currWfds)
#endif
{
    struct UUSOCKET *s = &t->ssocket;
    int len1, len2, len;

    if (!UUValidSocketSend(s) || *maxSend == 0)
        return;

    if (s->socketType != STSSL) {
        if (
#ifdef USEPOLL
            (PollRevents(currPfds, *currPfdc, s->o) & (POLLOUT | POLLERR | POLLHUP | POLLNVAL)) != 0
#else
            FD_ISSET(s->o, currWfds)
#endif
        )
            goto Send;
        return;
    }

    if (s->sslLast != SSLLASTSEND)
        goto Send;

    if (s->sslLastStatus == SSL_ERROR_WANT_READ) {
        if (
#ifdef USEPOLL
            (PollRevents(currPfds, *currPfdc, s->o) & (POLLIN | POLLERR | POLLHUP | POLLNVAL)) != 0
#else
            FD_ISSET(s->o, currRfds)
#endif
        )
            goto Send;
        return;
    }

    if (s->sslLastStatus == SSL_ERROR_WANT_WRITE) {
        if (
#ifdef USEPOLL
            (PollRevents(currPfds, *currPfdc, s->o) & (POLLOUT | POLLERR | POLLHUP | POLLNVAL)) != 0
#else
            FD_ISSET(s->o, currWfds)
#endif
        )
            goto Send;
        return;
    }

    /* Now falling into Send based on SSL connection with unknown error status */

Send:

    len1 = MAXBUFFER - t->rawWriteOfs;
    if (len1 > *maxSend) {
        len1 = *maxSend;
        len2 = 0;
    } else {
        len2 = *maxSend - len1;
    }

    if (debugLevel > 5)
        Log("Try send for %d (%d/%d/%d) get ", *maxSend, len1, len2, s->o);
    len = UUSend2(s, t->buffer + t->rawWriteOfs, len1, t->buffer, len2, UUSNOWAIT);
    if (debugLevel > 5)
        Log("len %d err %d", len, socket_errno);
    if (len >= 0) {
        if (len > 0)
            UUtime(&t->rawLast);
        t->rawWriteOfs += len;
        if (t->rawWriteOfs >= MAXBUFFER) {
            t->rawWriteOfs -= MAXBUFFER;
            t->rawWritePage++;
        }
        *maxSend -= len;
        *maxRecv += len;
        /*
        if (t->rawReadRemain > 0 && *maxRecv > t->rawReadRemain)
            *maxRecv = t->rawReadRemain;
        */
    } else {
        if (socket_errno != WSAEWOULDBLOCK)
            UUStopSocket(&(t->ssocket), UUSSTOPNICE);
    }
}

#ifdef USEPOLL
void RawFlagRecv(struct TRANSLATE *t,
                 int maxRecv,
                 SOCKET *maxSock,
                 int *timeout,
                 struct pollfd *nextPfds,
                 unsigned int *nextPfdc)
#else
void RawFlagRecv(struct TRANSLATE *t,
                 int maxRecv,
                 SOCKET *maxSock,
                 int *timeout,
                 fd_set *nextRfds,
                 fd_set *nextWfds)
#endif
{
    struct UUSOCKET *w = t->wsocketPtr;

    if (t->rawReadEof)
        return;

    if (!UUValidSocketRecv(w) || maxRecv == 0)
        return;

    if (w->socketType != STSSL) {
#ifdef USEPOLL
        nextPfds[*nextPfdc].fd = w->o;
        nextPfds[(*nextPfdc)++].events = POLLIN;
#else
        FD_SET(w->o, nextRfds);
#endif
        goto CheckIfMax;
    }

    if (w->sslLast != SSLLASTRECV) {
        *timeout = 0;
        return;
    }

    if (w->sslLastStatus == SSL_ERROR_WANT_READ) {
#ifdef USEPOLL
        nextPfds[*nextPfdc].fd = w->o;
        nextPfds[(*nextPfdc)++].events = POLLIN;
#else
        FD_SET(w->o, nextRfds);
#endif
        goto CheckIfMax;
    }

    if (w->sslLastStatus == SSL_ERROR_WANT_WRITE) {
#ifdef USEPOLL
        nextPfds[*nextPfdc].fd = w->o;
        nextPfds[(*nextPfdc)++].events = POLLOUT;
#else
        FD_SET(w->o, nextWfds);
#endif
        goto CheckIfMax;
    }

    *timeout = 0;

    return;

CheckIfMax:
    if (w->o > *maxSock)
        *maxSock = w->o;
}

#ifdef USEPOLL
void RawFlagSend(struct TRANSLATE *t,
                 int maxSend,
                 SOCKET *maxSock,
                 int *timeout,
                 struct pollfd *nextPfds,
                 unsigned int *nextPfdc)
#else
void RawFlagSend(struct TRANSLATE *t,
                 int maxSend,
                 SOCKET *maxSock,
                 int *timeout,
                 fd_set *nextRfds,
                 fd_set *nextWfds)
#endif
{
    struct UUSOCKET *s = &t->ssocket;

    if (!UUValidSocketSend(s) || (UUValidSocketRecv(t->wsocketPtr) && maxSend == 0))
        return;

    if (s->socketType != STSSL) {
#ifdef USEPOLL
        nextPfds[*nextPfdc].fd = s->o;
        nextPfds[(*nextPfdc)++].events = POLLOUT;
#else
        FD_SET(s->o, nextWfds);
#endif
        goto CheckIfMax;
    }

    if (s->sslLast != SSLLASTSEND) {
        *timeout = 0;
        return;
    }

    if (s->sslLastStatus == SSL_ERROR_WANT_READ) {
#ifdef USEPOLL
        nextPfds[*nextPfdc].fd = s->o;
        nextPfds[(*nextPfdc)++].events = POLLIN;
#else
        FD_SET(s->o, nextRfds);
#endif
        goto CheckIfMax;
    }

    if (s->sslLastStatus == SSL_ERROR_WANT_WRITE) {
#ifdef USEPOLL
        nextPfds[*nextPfdc].fd = s->o;
        nextPfds[(*nextPfdc)++].events = POLLOUT;
#else
        FD_SET(s->o, nextWfds);
#endif
        goto CheckIfMax;
    }

    *timeout = 0;

    return;

CheckIfMax:
    if (s->o > *maxSock)
        *maxSock = s->o;
}

/**
 * Starts as its own thread in main.  All translations that get their
 * t->raw=2 will be handled by this thread.  Took me a loooonnnnnggggg
 * time to figure this out :(
 *
 * The only place I find where this happens is for documents which are
 * t->javascript=0 and t->html=0 and optionUseRaw=1.  This will happend
 * for ContentType: text/plain
 */
void DoHandleRaw(void *v) {
    int i;
    int rc;
    struct UUSOCKET *s, *w;
    struct TRANSLATE *t;
    time_t now;
    int maxRecv, maxSend;
    int timeout;
    SOCKET maxSock;
    int currIdx;
    BOOL clearErrors = 0;
#ifdef USEPOLL
    struct pollfd *pfds[2], *currPfds, *nextPfds;
    unsigned int pfdc[2], *currPfdc, *nextPfdc;
#else
    fd_set rfds[2], wfds[2], *currRfds, *nextRfds, *currWfds, *nextWfds;
    struct timeval pollInterval;
#endif

    currIdx = 0;
#ifdef USEPOLL
    if (!(pfds[0] = (struct pollfd *)calloc(mTranslates * 2, sizeof(struct pollfd))))
        PANIC;
    if (!(pfds[1] = (struct pollfd *)calloc(mTranslates * 2, sizeof(struct pollfd))))
        PANIC;
    pfdc[0] = pfdc[1] = 0;
#else
    FD_ZERO(&rfds[0]);
    FD_ZERO(&rfds[1]);
    FD_ZERO(&wfds[0]);
    FD_ZERO(&wfds[1]);
#endif

Again:
    guardian->rawLocation = 1;
    ChargeAlive(GCHANDLERAW, &now);

    if (clearErrors) {
        ERR_clear_error();
        clearErrors = 0;
    }

    guardian->rawLocation = 2;

    /* timeout starts out at 100 milliseconds.  It will be reduced to zero if there are any
       SSL sockets in an "unknown pending" state, so they can be scanned to determine just
       what to wait on */
    timeout = 100;
    maxSock = 0;

#ifdef USEPOLL
    nextPfds = pfds[currIdx];
    nextPfdc = &pfdc[currIdx];
    *nextPfdc = 0;
    currIdx = 1 - currIdx;
    currPfds = pfds[currIdx];
    currPfdc = &pfdc[currIdx];
#else
    nextRfds = &rfds[currIdx];
    nextWfds = &wfds[currIdx];
    FD_ZERO(nextRfds);
    FD_ZERO(nextWfds);
    currIdx = 1 - currIdx;
    currRfds = &rfds[currIdx];
    currWfds = &wfds[currIdx];
#endif

    for (i = 0, t = translates; i < cTranslates; i++, t++) {
        if (t->active == 0 || t->raw != 2)
            continue;

        guardian->rawLocation = 3;

        s = &t->ssocket;
        w = t->wsocketPtr;

        if (s->socketType == STSSL || w->socketType == STSSL)
            clearErrors = 1;

        ticksRaw++;

        // this is where the length error for send happens
        maxSend =
            (t->rawReadPage != t->rawWritePage ? MAXBUFFER : 0) + t->rawReadOfs - t->rawWriteOfs;
        maxRecv = MAXBUFFER - maxSend;
        /*
        if (t->rawReadRemain > 0 && maxRecv > t->rawReadRemain)
            maxRecv = t->rawReadRemain;
        */
        guardian->rawLocation = 4;

#ifdef USEPOLL
        RawRecv(t, &maxRecv, &maxSend, currPfds, currPfdc);
#else
        RawRecv(t, &maxRecv, &maxSend, currRfds, currWfds);
#endif

        guardian->rawLocation = 5;

        if (maxSend == 0 && UUValidSocketSend(s)) {
            /* If there is nothing more to send and if we hit valid eof from remote, try to reuse */
            if (t->sentKeepAlive && t->rawReadEof && UUValidSocketRecv(w)) {
                dprintf("raw trying to give back %d\n", t->ssocket.o);
                EndTranslate(t, 1);
                t->raw = 0;
                if (StartTranslate(t) == 0) {
                    dprintf("raw sent back for reuse %d\n", t->ssocket.o);
                    /* If we dispatched, then there is no more work to do on this translate
                       and we move on to the next
                    */
                    continue;
                }
                /* Something went wrong, so we don't get to try to keep alive and should just shut
                 * down */
                t->raw = 2;
                UUStopSocket(&(t->ssocket), UUSSTOPNICE);
            } else if (t->rawReadEof || !UUValidSocketRecv(w)) {
                UUStopSocket(&(t->ssocket), UUSSTOPNICE);
            }
        }

        guardian->rawLocation = 6;

#ifdef USEPOLL
        RawSend(t, &maxRecv, &maxSend, currPfds, currPfdc);
#else
        RawSend(t, &maxRecv, &maxSend, currRfds, currWfds);
#endif

        guardian->rawLocation = 7;

        if ((!UUValidSocketSend(s)) || (now - t->rawLast) > binaryTimeout) {
            guardian->rawLocation = 8;
            UUStopSocket(s, UUSSTOPNICE);
            guardian->rawLocation = 9;
            /* If no FTP socket in use, this won't hurt as it will be set to INVALID_SOCKET */
            UUStopSocket(&t->ftpsocket, 0);
            guardian->rawLocation = 10;
            UUStopSocket(w, 0);
            guardian->rawLocation = 11;
            t->raw = 0;
            EndTranslate(t, 1);
            ReleaseTranslate(t);
            guardian->rawLocation = 12;

            /* We won't be waiting on this next go round, so just continue instead of the
               "what to check" logic below */
            continue;
        }

        guardian->rawLocation = 13;

#ifdef USEPOLL
        RawFlagRecv(t, maxRecv, &maxSock, &timeout, nextPfds, nextPfdc);
        guardian->rawLocation = 14;
        RawFlagSend(t, maxSend, &maxSock, &timeout, nextPfds, nextPfdc);
#else
        RawFlagRecv(t, maxRecv, &maxSock, &timeout, nextRfds, nextWfds);
        guardian->rawLocation = 14;
        RawFlagSend(t, maxSend, &maxSock, &timeout, nextRfds, nextWfds);
#endif
    }

    guardian->rawLocation = 15;

    if (maxSock == 0) {
        /* No explicit file descriptors to monitor, so just delay */
        /* (you'd think that'd be OK below, since select and poll understand such things, but Win32
           doesn't like do the "use select as a delay" thing */
        guardian->rawLocation = 16;

        if (timeout > 0) {
            SetErrno(0);
            SubSleep(0, timeout);
#ifndef WIN32
            if ((errno == ECANCELED)) {
                ChargeStopped(GCHANDLERAW, NULL);
                return;
            }
#endif
        }
        goto Again;
    }

    guardian->rawLocation = 17;

#ifdef USEPOLL
    PollSort(nextPfds, *nextPfdc);
    guardian->rawLocation = 18;
    TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(poll(nextPfds, *nextPfdc, timeout)));
    guardian->rawLocation = 19;
    if (rc < 0) {
        guardian->rawLocation = 20;
        Log("poll returned %d", socket_errno);
        PANIC;
    }
#else
    /* Note that Linux select destroys the timeout field, so it must be reset each time */
    pollInterval.tv_sec = 0;
    pollInterval.tv_usec = timeout * 1000;
    TEMP_FAILURE_RETRY_INT(
        rc = interrupted_neg_int(select(maxSock + 1, nextRfds, nextWfds, NULL, &pollInterval)));
    guardian->rawLocation = 21;
    if (rc < 0) {
        guardian->rawLocation = 22;
        Log("select returned %d", socket_errno);
        PANIC;
    }
#endif

    goto Again;
}

void ProcessNonHTML(struct TRANSLATE *t) {
    int rc;
    struct UUSOCKET *s = &t->ssocket;
    struct UUSOCKET *w = t->wsocketPtr;
    time_t now;
    int maxRecv, maxSend;
    int timeout;
    SOCKET maxSock;
    int currIdx;
#ifdef USEPOLL
    struct pollfd *pfds[2], *currPfds, *nextPfds;
    unsigned int pfdc[2], *currPfdc, *nextPfdc;
#else
    fd_set rfds[2], wfds[2], *currRfds, *nextRfds, *currWfds, *nextWfds;
    struct timeval pollInterval;
#endif

    /* Flush sockets before transition to raw */
    UUFlush(t->wsocketPtr);
    UUFlush(&t->ssocket);
    /* eof is now track globally based on recvChunkRemain
    t->rawReadRemain = t->contentLength == -1 ? 0 : t->contentLength;
    */
    t->rawReadPage = t->rawWritePage = 0;
    t->rawReadOfs = t->rawWriteOfs = 0;
    t->rawReadEof = 0;
    UUtime(&t->rawLast);

    currIdx = 0;
#ifdef USEPOLL
    if (!(pfds[0] = (struct pollfd *)calloc(2, sizeof(struct pollfd))))
        PANIC;
    if (!(pfds[1] = (struct pollfd *)calloc(2, sizeof(struct pollfd))))
        PANIC;
    pfdc[0] = pfdc[1] = 0;
#else
    FD_ZERO(&rfds[0]);
    FD_ZERO(&rfds[1]);
    FD_ZERO(&wfds[0]);
    FD_ZERO(&wfds[1]);
#endif

Again:
    UUtime(&now);
    /* timeout starts out at 100 milliseconds.  It will be reduced to zero if there are any
       SSL sockets in an "unknown pending" state, so they can be scanned to determine just
       what to wait on */
    timeout = 100;
    maxSock = 0;

#ifdef USEPOLL
    nextPfds = pfds[currIdx];
    nextPfdc = &pfdc[currIdx];
    *nextPfdc = 0;
    currIdx = 1 - currIdx;
    currPfds = pfds[currIdx];
    currPfdc = &pfdc[currIdx];
#else
    nextRfds = &rfds[currIdx];
    nextWfds = &wfds[currIdx];
    FD_ZERO(nextRfds);
    FD_ZERO(nextWfds);
    currIdx = 1 - currIdx;
    currRfds = &rfds[currIdx];
    currWfds = &wfds[currIdx];
#endif

    maxSend = (t->rawReadPage != t->rawWritePage ? MAXBUFFER : 0) + t->rawReadOfs - t->rawWriteOfs;
    maxRecv = MAXBUFFER - maxSend;
    /*
    if (t->rawReadRemain > 0 && maxRecv > t->rawReadRemain)
        maxRecv = t->rawReadRemain;
    */

#ifdef USEPOLL
    RawRecv(t, &maxRecv, &maxSend, currPfds, currPfdc);
#else
    RawRecv(t, &maxRecv, &maxSend, currRfds, currWfds);
#endif

    if (maxSend == 0) {
        if (t->rawReadEof)
            return;
        if (!UUValidSocketRecv(w)) {
            UUStopSocket(&t->ssocket, UUSSTOPNICE);
            return;
        }
    }

#ifdef USEPOLL
    RawSend(t, &maxRecv, &maxSend, currPfds, currPfdc);
#else
    RawSend(t, &maxRecv, &maxSend, currRfds, currWfds);
#endif

    if ((!UUValidSocketSend(s)) || (now - t->rawLast) > binaryTimeout) {
        return;
        /*
        UUStopSocket(s, UUSSTOPNICE);
        */
        /* If no FTP socket in use, this won't hurt as it will be set to INVALID_SOCKET */
        /*
        UUStopSocket(&t->ftpsocket, 0);
        UUStopSocket(r, 0);
        ReleaseTranslate(t, 1);
        return;
        */
    }

#ifdef USEPOLL
    RawFlagRecv(t, maxRecv, &maxSock, &timeout, nextPfds, nextPfdc);
    RawFlagSend(t, maxSend, &maxSock, &timeout, nextPfds, nextPfdc);
#else
    RawFlagRecv(t, maxRecv, &maxSock, &timeout, nextRfds, nextWfds);
    RawFlagSend(t, maxSend, &maxSock, &timeout, nextRfds, nextWfds);
#endif

    if (maxSock == 0) {
        /* This should mean that we dumped everything out and we're all done */
        goto Again;
    }

#ifdef USEPOLL
    PollSort(nextPfds, *nextPfdc);
    TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(poll(nextPfds, *nextPfdc, timeout)));
    if (rc < 0) {
        Log("poll returned %d", socket_errno);
        PANIC;
    }
#else
    /* Note that Linux select destroys the timeout field, so it must be reset each time */
    pollInterval.tv_sec = 0;
    pollInterval.tv_usec = timeout * 1000;
    TEMP_FAILURE_RETRY_INT(
        rc = interrupted_neg_int(select(maxSock + 1, nextRfds, nextWfds, NULL, &pollInterval)));
    if (rc < 0) {
        Log("select returned %d", socket_errno);
        PANIC;
    }
#endif

    goto Again;
}

void StartRaw(struct TRANSLATE *t, intmax_t len, BOOL defer) {
    /* Remap the Content-Length unknown (-1) case to the
       0 case that DoHandleRaw expects
    */
    if (len == -1)
        len = 0;
    /* Flush sockets before transition to raw */
    UUFlush(t->wsocketPtr);
    UUFlush(&t->ssocket);

    /* Turn off blocking on these sockets to keep them from freezing up the */
    /* shared raw transfer processing */
    /*
    SocketNonBlocking(t->ssocket);
    SocketNonBlocking(t->rsocket);
    SocketDisableNagle(t->ssocket);
    SocketDisableNagle(t->rsocket);
    */
    /*  t->rawReadPtr = t->buffer; */

    /* eof is now track globally based on recvChunkRemain
    t->rawReadRemain = len;
    */

    /*  t->rawReadBytes = 0; */
    /*  t->rawReadMax = len == 0 || len > MAXRAWBUFF ? MAXRAWBUFF : len; */
    /*  t->rawWriteBytes = 0; */
    /*  t->rawWriteMax = 0; */
    /* strategy 2 */
    t->rawReadPage = t->rawWritePage = 0;
    t->rawReadOfs = t->rawWriteOfs = 0;
    t->rawReadEof = 0;
    /* end stragey 2 */
    UUtime(&t->rawLast);

    if (defer == 0)
        t->raw = 1;
}

void NoCacheHeaders(struct UUSOCKET *s) {
    time_t now;
    char httpDate[HTTPDATELENGTH];

    UUtime(&now);

    WriteLine(s,
              "\
Expires: Mon, 02 Aug 1999 00:00:00 GMT\r\n\
Last-Modified: %s\r\n\
Cache-Control: no-store, no-cache, must-revalidate\r\n\
Cache-Control: post-check=0, pre-check=0\r\n\
Pragma: no-cache\r\n",
              FormatHttpDate(httpDate, sizeof(httpDate), now));

    /* WriteLine(s, "Expires: Mon, 02 Aug 1999 00:00:00 GMT\r\nPragma: no-cache\r\nCache-control:
     * no-cache\r\nCache-control: no-store\r\n"); */
}

void MaxAgeCacheHeader(struct UUSOCKET *s) {
    if (cacheControlMaxAge) {
        WriteLine(s, "Cache-Control: private, max-age=%d\r\n", cacheControlMaxAge);
    }
}

void ExpireCacheHeaders(struct UUSOCKET *s) {}
/**
 * Sends final header "Connection: (Keep-Alive | Close)
 */
void HeaderToBody(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket;

    /* Compress the result if it is a 200 from EZproxy or remote,
       or if there is a remote site being proxied and it chose to compress
       Compressing the other status codes doesn't generally make sense as there
       is so little content
    */
    if (t->sendZipAllowed) {
        if (!(t->finalStatus == 200 || (t->zip && UUValidSocketRecv(t->wsocketPtr))))
            t->sendZipAllowed = 0;
        else
            /* Old verisons of Mozilla have compression issues, so just avoid them */
            /* But, MSIE also calls itself Mozilla/4, so don't block it out */
            if (t->isMSIE == 0 && t->mozillaVersion < 5)
                t->sendZipAllowed = 0;
            else if (t->html == 0 && t->javaScript == 0)
                t->sendZipAllowed = 0;
    }

    if (t->keepAlivesRemaining > 0) {
        t->keepAlivesRemaining--;
    } else {
        t->clientKeepAlive = 0;
    }

    if (t->denyClientKeepAlive)
        t->clientKeepAlive = 0;

    t->sentKeepAlive = t->clientKeepAlive;

    if (t->bodylessStatus) {
        t->sendZipAllowed = 0;
        t->sendChunkingAllowed = 0;
    } else {
        if (t->sendChunkingAllowed) {
            UUSendZ(s, "Transfer-Encoding: chunked\r\n");
        }
        if (t->sendZipAllowed) {
            UUSendZ(s, "Content-Encoding: gzip\r\n");
        } else {
            if (t->sendContentLengthSent == 0)
                t->sentKeepAlive = 0;
        }
    }

    if (t->sentKeepAlive) {
        WriteLine(s, "Connection: keep-alive\r\nKeep-Alive: timeout=%d, max=%d\r\n",
                  keepAliveTimeout, maxKeepAliveRequests);
    } else {
        UUSendZ(s, "Connection: close\r\n");
    }

    // send cookie stuff and other variables
    VariablesSendHttpHeaders(t);
    if (t->host == hosts) {
        ProcessHttpHeadersList(t, serverHttpHeaders, s, HHDRESPONSE, NULL, NULL);
    }

    // this actually marks the end of the header
    // RFC that you should send a line with just \r\n as end of header
    UUSendCRLF(s);

    if (t->sendChunkingAllowed || t->sendZipAllowed) {
        UUFlush(s);
        if (t->sendChunkingAllowed) {
            s->sendChunking = 1;
            s->sendChunkRemain = 0;
        }
        if (t->sendZipAllowed)
            s->sendZip = 1;
    }
}

void HTMLHeader2(struct TRANSLATE *t, enum HTMLHEADERCACHE hhc, BOOL endHeaders) {
    struct UUSOCKET *s = &t->ssocket;

    HTTPCode(t, 200, 0);
    if (t->version) {
        switch (hhc) {
        case htmlHeaderNoCache:
            NoCacheHeaders(s);
            break;
        case htmlHeaderMaxAgeCache:
            MaxAgeCacheHeader(s);
            break;
        }
    }
    HTTPContentType(t, NULL, endHeaders);
}

void HTMLHeader(struct TRANSLATE *t, enum HTMLHEADERCACHE hhc) {
    HTMLHeader2(t, hhc, 1);
}

void CSRFHeader(struct TRANSLATE *t) {
    HTMLHeader2(t, htmlHeaderMaxAgeCache, 0);
    if (optionCSRFToken) {
        char *p;
        int i;

        if (p3pHeader) {
            UUSendZ(&t->ssocket, "P3P: ");
            UUSendZCRLF(&t->ssocket, p3pHeader);
        }

        for (p = t->csrfToken, i = 0; i < CSRF_TOKEN_LEN - 1; p++, i++) {
            *p = RandChar();
        }
        *p = 0;
        WriteLine(&t->ssocket, "Set-Cookie: %s=%s; path=/; HttpOnly\r\n", CSRF_TOKEN_NAME,
                  t->csrfToken);
    }
    HeaderToBody(t);
}

void HTMLErrorPageHeader(struct TRANSLATE *t, char *title) {
    struct UUSOCKET *s = &t->ssocket;

    HTMLHeader(t, htmlHeaderNoCache);
    UUSendZ(s, TAG_DOCTYPE_HTML_HEAD "<title>");
    UUSendZ(s, title ? title : "Error");
    UUSendZ(s, "</title></head>\n<body>\n");
}

void HTMLErrorPageFooter(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket;

    UUSendZ(s, "</body></html>\n");
}

void HTMLErrorPage(struct TRANSLATE *t, char *title, char *error) {
    struct UUSOCKET *s = &t->ssocket;

    if (title == NULL)
        title = "Error";

    if (error == NULL)
        error = title;

    HTMLErrorPageHeader(t, title);
    UUSendZ(s, error);
    HTMLErrorPageFooter(t);
}

void RandTest(void) {
#define RANDTESTMAX 32768
    char used[RANDTESTMAX];
    int count = 0;
    int tries = 0;
    int i;

    memset(&used, 0, sizeof(used));

    for (; count < RANDTESTMAX; tries++) {
        i = RandNumber(RANDTESTMAX);
        if (used[i] == 0)
            count++;
        used[i]++;
    }

    printf("%d/%d\n", count, tries);
}

char RandChar(void) {
    int c;

    c = RandNumber(62);
    if (c < 26)
        return c + 'A';
    c -= 26;
    if (c < 26)
        return c + 'a';
    c -= 26;
    return c + '0';
}

void SendIfNonDefaultPort(struct UUSOCKET *s, PORT port, char useSsl) {
    if (port == 0 || port == (useSsl ? 443 : 80))
        return;
    WriteLine(s, ":%d", port);
}

time_t ParseLocalDate(char *s, int offset) {
    int year, month, day;
    struct tm tm;

    if (sscanf(s, "%d-%d-%d", &year, &month, &day) != 3)
        return 0;

    memset(&tm, 0, sizeof(tm));
    tm.tm_year = year - 1900;
    tm.tm_mon = month - 1;
    tm.tm_mday = day + offset;
    tm.tm_isdst = -1;
    return mktime(&tm);
}

time_t ParseRelativeDate(char *s) {
    time_t now, when;
    struct tm rtm;
    int i;

    if (s == NULL || *s == 0)
        return 0;

    UUtime(&now);
    UUlocaltime_r(&now, &rtm);
    rtm.tm_sec = rtm.tm_min = rtm.tm_hour = 0;
    /* set dst as unknown in case time interval moves between daylight savings time and not */
    rtm.tm_isdst = -1;

    when = 0;

    if (stricmp(s, "now") == 0) {
        when = now;
    } else if (stricmp(s, "today") == 0) {
        when = mktime(&rtm);
    } else if (stricmp(s, "yesterday") == 0) {
        /* mktime is defined as addressing when fields are out of bounds, so it's OK if
           mday goes to 0 as that's automatically last day of previous month and if
           necessary year
        */
        rtm.tm_mday--;
        when = mktime(&rtm);
    } else {
        i = atoi(s + 1);
        if (i || *(s + 1) == '0') {
            switch (*s) {
            case 's':
                when = now - i;
                break;
            case 'm':
                when = now - i * 60;
                break;
            case 'h':
                when = now - i * 3600;
                break;
            case 'd':
                rtm.tm_mday -= i;
                when = mktime(&rtm);
                break;
            case 'w':
                rtm.tm_mday -= i * 7;
                when = mktime(&rtm);
                break;
            }
        }
    }

    if (when == 0 && debugLevel > 0) {
        Log("Unable to ParseRelativeDate %s", s);
    }

    return when;
}

int SendFileMatches(struct TRANSLATE *t, char *file, int flags, char *matches, time_t earliest) {
    struct UUSOCKET *s;
    int f;
    int len;
    int result;
    int save;
    struct FILEREADLINEBUFFER frb;
    struct tm rtm;
    time_t when;
    BOOL seekingEarliest = 1;

    s = &t->ssocket;

    f = SBOPEN(file);
    if (f < 0) {
        save = errno;
        if (flags & SFREPORTIFMISSING)
            WriteLine(s, "File missing: %s\n", file);
        return errno;
    }

    result = 0;

    InitializeFileReadLineBuffer(&frb);
    while (FileReadLine(f, t->buffer, sizeof(t->buffer), &frb)) {
        if (earliest && seekingEarliest) {
            memset(&rtm, 0, sizeof(rtm));
            if (t->buffer[4] != '-' || t->buffer[7] != '-' || t->buffer[10] != ' ' ||
                t->buffer[13] != ':' || t->buffer[16] != ':' || t->buffer[19] != ' ')
                continue;
            if (sscanf(t->buffer, "%d-%d-%d %d:%d:%d", &rtm.tm_year, &rtm.tm_mon, &rtm.tm_mday,
                       &rtm.tm_hour, &rtm.tm_min, &rtm.tm_sec) < 6 ||
                rtm.tm_year < 1999)
                continue;
            rtm.tm_year -= 1900;
            rtm.tm_mon--;
            rtm.tm_isdst = -1;
            when = mktime(&rtm);
            if (when < earliest)
                continue;
            seekingEarliest = 0;
        }

        if (matches && StrIStr(t->buffer, matches) == NULL)
            continue;

        if (flags & SFQUOTEDURL)
            SendQuotedURL(t, t->buffer);
        else {
            len = strlen(t->buffer);
            if (UUSend(s, t->buffer, len, 0) != len) {
                result = -1;
                break;
            }
        }
        UUSendCRLF(s);
    }

    close(f);
    return result;
}

int OpenSendFile(struct TRANSLATE *t, char *file, BOOL binary) {
    int f;
    char *customFile = NULL;
    char *p;

    if (t && t->docsCustomDir && *t->docsCustomDir && (p = StartsIWith(file, DOCSDIR)) &&
        strchr(p, '/') == NULL && strchr(p, '\\') == NULL) {
        if (!(customFile = malloc(strlen(file) + strlen(t->docsCustomDir) + 32)))
            PANIC;
        sprintf(customFile, "%scustom%s%s%s%s", DOCSDIR, DIRSEP, t->docsCustomDir, DIRSEP, p);
        if (CheckIfFile(customFile)) {
            f = (binary ? SBOPEN(customFile) : SOPEN(customFile));
        } else {
            f = -1;
        }
        FreeThenNull(customFile);
        if (f >= 0)
            return f;
    }

    if (CheckIfFile(file))
        f = (binary ? SBOPEN(file) : SOPEN(file));
    else {
        f = -1;
        errno = ENOENT;
    }

    return f;
}

int SendFile(struct TRANSLATE *t, char *file, int flags) {
    struct UUSOCKET *s;
    int f;
    int len;
    int result;
    int save;
    struct FILEREADLINEBUFFER frb;

    s = &t->ssocket;

    f = OpenSendFile(t, file, 1);
    if (f < 0) {
        save = errno;
        if (flags & SFREPORTIFMISSING)
            WriteLine(s, "File missing: %s\n", file);
        return errno;
    }

    result = 0;
    if (flags & SFQUOTEDURL) {
        InitializeFileReadLineBuffer(&frb);
        while (FileReadLine(f, t->buffer, sizeof(t->buffer), &frb)) {
            SendQuotedURL(t, t->buffer);
            UUSendZ(s, "\n");
        }
    } else {
        for (;;) {
            len = UUreadFD(f, t->buffer, sizeof(t->buffer));
            if (len <= 0)
                break;
            if (UUSend(s, t->buffer, len, 0) != len) {
                result = -1;
                break;
            }
        }
    }

    close(f);
    return result;
}

int SendFileTail(struct TRANSLATE *t, char *file, int flags, int tail) {
    struct UUSOCKET *s;
    int f;
    int len;
    int result;
    int save;
    struct FILEREADLINEBUFFER frb;

    s = &t->ssocket;

    f = SBOPEN(file);
    if (f < 0) {
        save = errno;
        if (flags & SFREPORTIFMISSING)
            WriteLine(s, "File missing: %s\n", file);
        return errno;
    }

    if (tail)
        TailSeek(f, tail);

    result = 0;
    if (flags & SFQUOTEDURL) {
        InitializeFileReadLineBuffer(&frb);
        while (FileReadLine(f, t->buffer, sizeof(t->buffer), &frb)) {
            SendQuotedURL(t, t->buffer);
            UUSendZ(s, "\n");
        }
    } else {
        for (;;) {
            len = UUreadFD(f, t->buffer, sizeof(t->buffer));
            if (len <= 0)
                break;
            if (UUSend(s, t->buffer, len, 0) != len) {
                result = -1;
                break;
            }
        }
    }

    close(f);
    return result;
}

void SendQuotedURL(struct TRANSLATE *t, char *url) {
    if (url == NULL)
        return;

    for (; *url; url++)
        if (*url == '&' || *url == '"' || *url == '\'' || *url == '<' || *url == '>' ||
            (*url < ' ')) {
            WriteLine(&t->ssocket, "&#%d;", (unsigned char)*url);
        } else
            UUSend(&t->ssocket, url, 1, 0);
}

void SendEscapedURL(struct UUSOCKET *s, char *url) {
    char hex[4];

    if (url == NULL)
        return;

    for (; *url; url++) {
        if (IsDigit(*url) || IsAlpha(*url) || *url == '.')
            UUSend(s, url, 1, 0);
        else if (*url == ' ')
            UUSendZ(s, "+");
        else {
            sprintf(hex, "%%%02x", (unsigned int)(*url & 0xff));
            UUSend(s, hex, 3, 0);
        }
    }
}

BOOL FileExists(char *file) {
    int f;

    f = SOPEN(file);
    if (f < 0)
        return 0;
    close(f);
    return 1;
}

void SendLoginURL(struct TRANSLATE *t, char *url) {
    struct UUSOCKET *s = &t->ssocket;
    char *myUrl;

    if (t->excludeIPBanner) {
        if (url == NULL || *url == 0 || stricmp(url, "menu") == 0) {
            UUSendZ(s, t->myUrl);
            UUSendZ(s, "/menu");
        } else {
            UUSendZ(s, url);
        }
        return;
    }

    if (url == NULL || *url == 0 || stricmp(url, "menu") == 0) {
        myUrl = t->myUrl;
    } else if (strnicmp(url, "https://", 8) != 0 || myUrlHttps == NULL || *myUrlHttps == 0)
        myUrl = myUrlHttp;
    else
        myUrl = myUrlHttps;

    UUSendZ(s, myUrl);
    UUSendZ(s, "/login");

    if (url == NULL || *url == 0)
        return;

    UUSendZ(s, "?url=");
    UUSendZ(s, url);
}

int SendEditedFile(struct TRANSLATE *t,
                   struct USERINFO *uip,
                   char *file,
                   BOOL reportIfMissing,
                   char *url,
                   BOOL quoteURL,
                   ...) {
    struct UUSOCKET *s;
    int f;
    off_t dbstart = 0;
    BOOL quote;
    struct DATABASE *b = NULL, *lastB = NULL;
    int i = 0;
    char c;
    BOOL skipping;
    char myPort[16];
    int save;
    char *ph;
    int rc;
    char *params[10];
    int pidx;
    char *title;
    va_list ap;
    BOOL groupScan = 0;
    char exprBuffer[1024], *expr = NULL, *exprEnd;
    char caretVar[16];

    exprEnd = exprBuffer + sizeof(exprBuffer) - 1;

    title = NULL;
    va_start(ap, quoteURL);
    for (pidx = 0; pidx < 10; pidx++) {
        sprintf(caretVar, "login:%d", pidx);
        params[pidx] = va_arg(ap, char *);
        if (params[pidx] == NULL)
            break;
        if (pidx == 0)
            title = params[pidx];
        VariablesSetOrDeleteValue(t->localVariables, caretVar, params[pidx]);
    }
    va_end(ap);

    while (pidx < 10) {
        params[pidx++] = "";
        sprintf(caretVar, "login:%d", pidx);
        VariablesDelete(t->localVariables, caretVar);
    }

    sprintf(myPort, "%d", primaryLoginPort);

    s = &t->ssocket;

    f = OpenSendFile(t, file, 0);
    if (f < 0) {
        save = errno;
        if (reportIfMissing)
            WriteLine(s, "File missing: %s\n", file);
        return save;
    }

    b = NULL;
    skipping = 0;
    quote = 0;

    VariablesSetOrDeleteValue(t->localVariables, "login:title", title);
    VariablesSetOrDeleteValue(t->localVariables, "login:url", url);

    for (;;) {
        if (groupScan) {
            groupScan = 0;
            if (b) {
                for (;; i++, b++) {
                    if (i >= cDatabases) {
                        b = NULL;
                        i = -1;
                        skipping = 1;
                        break;
                    }
                    if (b->hideFromMenu)
                        continue;
                    if (optionMenuByGroups && t->session &&
                        GroupMaskOverlap(b->gm, (t->session)->gm) == 0)
                        continue;
                    if (b->getName == NULL && b->dbdomains == NULL) {
                        if (b->desc)
                            UUSendZ(&t->ssocket, b->desc);
                        continue;
                    }
                    break;
                }
            }
        }

        if (b != lastB) {
            if (b) {
                VariablesSetOrDeleteValue(t->localVariables, "db:title", b->title);
                VariablesSetOrDeleteValue(t->localVariables, "db:url", b->url);
                VariablesSetOrDeleteValue(t->localVariables, "db:description", b->desc);
            } else {
                VariablesDelete(t->localVariables, "db:title");
                VariablesDelete(t->localVariables, "db:url");
                VariablesDelete(t->localVariables, "db:description");
            }
            lastB = b;
        }

        rc = UUreadFD(f, &c, sizeof(c));
        if (rc <= 0)
            break;

        if (expr) {
            if (c == '}') {
                char *val;
                *expr = 0;
                val = ExpressionValue(t, uip, NULL, exprBuffer);
                if (val)
                    UUSendZ(s, val);
                FreeThenNull(val);
                expr = NULL;
                continue;
            }
            if (expr < exprEnd) {
                *expr++ = c;
                continue;
            }
        }

        if (c == '^') {
            quote = !quote;
            if (quote)
                continue;
        }

        if (quote == 0) {
            if (!skipping)
                UUSend(&t->ssocket, &c, 1, 0);
            continue;
        }

        quote = 0;

        if (c == '{') {
            expr = exprBuffer;
            continue;
        }

        if (c == 'i' || c == 'I') {
            char ipBuffer[INET6_ADDRSTRLEN];
            WriteLine(s, "%s", ipToStr(&t->sin_addr, ipBuffer, sizeof ipBuffer));
            continue;
        }

        if (c == 'f' || c == 'F') {
            if (t->csrfToken[0]) {
                WriteLine(s, "<input type='hidden' name='%s' value='%s'>", CSRF_TOKEN_NAME,
                          t->csrfToken);
            }
            continue;
        }

        if (c == 'b' || c == 'B') {
            dbstart = lseek(f, 0, SEEK_CUR);
            if (cDatabases == 0) {
                i = -1;
                skipping = -1;
            } else {
                i = 0;
                b = databases;
                groupScan = 1;
            }
            continue;
        }

        if (c == 'e' || c == 'E') {
            if (b != NULL) {
                i++;
                b++;
                if (i < cDatabases) {
                    lseek(f, dbstart, SEEK_SET);
                    groupScan = 1;
                } else
                    b = NULL;
            }
            skipping = 0;
            continue;
        }

        if (skipping)
            continue;

        if (IsDigit(c)) {
            SendQuotedURL(t, params[c - '0']);
            continue;
        }

        if (c == 'l' || c == 'L') {
            char *myUrl = (myUrlHttps ? myUrlHttps : myUrlHttp);
            /*
            if (myUrlHttps && (t->useSsl || optionAllowHTTPLogin == 0))
                myUrl = myUrlHttps;
            */
            WriteLine(s, "%s/login", myUrl);
            continue;
        }

        if (url != NULL && *url != 0 && (c == 'u' || c == 'U')) {
            /* Note that all tags are URL-safe so no quoting is required */
            if (t->logTag)
                WriteLine(s, "%s$", t->logTag);
            if (quoteURL)
                SendQuotedURL(t, url);
            else
                UUSendZ(s, url);
            continue;
        }

        if (url != NULL && *url != 0 && (c == 'h' || c == 'H')) {
            ph = SkipHTTPslashesAt(url, NULL);
            for (; *ph && *ph != '/'; ph++)
                UUSend(s, ph, 1, 0);
            continue;
        }

        if (url != NULL && *url != 0 && (c == 'v' || c == 'V')) {
            if (t->logTag)
                WriteLine(s, "%s$", t->logTag);
            UUSendZ(s, url);
            continue;
        }

        if (url != NULL && *url != 0 && (c == 'r' || c == 'R')) {
            SendURLReference(s, url, t->logTag);
            continue;
        }

        if (c == 'w' || c == 'W') {
            if (shibbolethWAYF == NULL) {
                static char reportError = 1;

                if (reportError) {
                    reportError = 0;
                    Log("Use of ^W in %s not allowed unless ShibbolethWAYF defined in " EZPROXYCFG,
                        file);
                }
            } else {
                ShibbolethSendURLwithWAYF(s, url, NULL, NULL, t->logTag);
            }
            continue;
        }

        if (c == 'n' || c == 'N') {
            if (optionUsernameCaretN) {
                struct SESSION *e = t->session;
                if (e && e->logUserBrief)
                    SendHTMLEncoded(s, e->logUserBrief);
            }
            continue;
        }

        if (title != NULL && (c == 't' || c == 'T')) {
            UUSendZ(s, title);
            continue;
        }

        if (c == 'p' || c == 'P') {
            WriteLine(s, "http://%s:%s", myName, myPort);
            continue;
        }

        /* We need to handle this a bit different since ^C should always produce a
           URL in any context
        */
        if (c == 'c' || c == 'C') {
            if (b)
                SendLoginURL(t, b->url);
            else
                SendLoginURL(t, url);
            continue;
        }

        if (b != NULL) {
            if (c == 'u' || c == 'U') {
                if (b->url)
                    if (quoteURL)
                        SendQuotedURL(t, b->url);
                    else
                        UUSendZ(s, b->url);
                continue;
            }
            if (c == 'v' || c == 'V') {
                if (b->url) {
                    UUSendZ(s, b->url);
                }
                continue;
            }
            if (c == 'r' || c == 'R') {
                if (b->url)
                    SendURLReference(s, b->url, t->logTag);
                continue;
            }

            if (c == 't' || c == 'T') {
                if (b->title)
                    UUSendZ(s, b->title);
                continue;
            }

            if (c == 'd' || c == 'D') {
                if (b->desc)
                    UUSendZ(s, b->desc);
                continue;
            }
        }

        WriteLine(s, "^%c", c);
    }

    close(f);

    return 0;
}

void FindFormFields(char *query, char *post, struct FORMFIELD *ffs) {
    char *val;
    char *p;
    struct FORMFIELD *ffp;
    BOOL rest;
    int i;
    char *buffer;

    rest = 0;
    for (i = 0; i < 2; i++) {
        buffer = (i == 0 ? post : query);
        for (; buffer && *buffer; buffer = p) {
            val = NULL;
            for (p = buffer;; p++) {
                if (*p == 0) {
                    p = NULL;
                    break;
                }
                if (*p == '&') {
                    *p++ = 0;
                    break;
                }
                if (*p == '=' && val == NULL) {
                    *p = 0;
                    val = p + 1;
                    for (ffp = ffs; ffp->name; ffp++)
                        if (strcmp(buffer, ffp->name) == 0 && ffp->rest != 0) {
                            p = strchr(p, 0);
                            rest = 1;
                            break;
                        }
                    if (rest)
                        break;
                }
            }
            for (ffp = ffs; ffp->name; ffp++) {
                if (stricmp(buffer, ffp->name) == 0) {
                    if (val != NULL && ffp->dontUnescape == 0)
                        UnescapeString(val);
                    if (val != NULL && (ffp->flags & FORMFIELDNOTRIMCTRL) == 0) {
                        Trim(val, TRIM_CTRL);
                    }
                    if (val != NULL && strlen(val) > ffp->maxLen && ffp->maxLen > 0)
                        *(val + ffp->maxLen) = 0;

                    if (i == 1)
                        ffp->flags |= FORMFIELDQUERYSTRING;

                    if ((ffp->flags & FORMFIELDALLOWMULTIVALUES) == 0 ||
                        (ffp->flags & FORMFIELDTAKELASTVALUE) != 0 || ffp->value == NULL)
                        ffp->value = val;
                    else {
                        struct FORMFIELDMULTIVALUE *fflv = NULL, *ffcv, *ffnv;

                        if (!(ffnv = calloc(1, sizeof(*ffnv))))
                            PANIC;
                        if (!(ffnv->value = strdup(val)))
                            PANIC;

                        for (fflv = NULL, ffcv = ffp->nextMultiValue; ffcv;
                             fflv = ffcv, ffcv = ffcv->nextMultiValue)
                            ;

                        if (fflv == NULL)
                            ffp->nextMultiValue = ffnv;
                        else
                            fflv->nextMultiValue = ffnv;
                    }
                }
            }
        }
    }
}

void FindFormFieldsFreeMultiValues(struct FORMFIELD *ffs) {
    struct FORMFIELD *ffp;
    struct FORMFIELDMULTIVALUE *ffcv, *ffnv;

    for (ffp = ffs; ffp->name; ffp++) {
        if (ffp->nextMultiValue) {
            for (ffcv = ffp->nextMultiValue; ffcv; ffcv = ffnv) {
                ffnv = ffcv->nextMultiValue;
                FreeThenNull(ffcv->value);
                FreeThenNull(ffcv);
            }
            ffp->nextMultiValue = NULL;
            ffp->flags = ffp->flags & ~FORMFIELDALLOWMULTIVALUES;
        }
    }
}

/* Validate a modulus 10 check digit, ignorning non numerics in the string in case its a barcode
 * surrounded */
/* by stop/start characters */

BOOL ValidateCheckDigit(char *digits) {
    char *p;
    int mult;
    int sum;
    int n;
    int check;

    if (digits == NULL || *digits == 0)
        return 0;

    check = -1;
    mult = 2;
    sum = 0;

    for (p = strchr(digits, 0) - 1; p >= digits; p--) {
        if (!IsDigit(*p))
            continue;
        if (check == -1) {
            check = *p - '0';
            continue;
        }
        n = (*p - '0') * mult;
        sum += n / 10 + n % 10;
        mult ^= 3; /* use exclusive or to efficiently toggle between 1 and 2 */
    }

    sum = (10 - sum % 10) % 10;

    return sum == check;
}

enum IPACCESS URLIpAccess(struct TRANSLATE *t,
                          char *url,
                          int *sessionLife,
                          GROUPMASK gmAuto,
                          BOOL *anyExcluded,
                          char **autoUser) {
    int i, j, max, stop;
    enum IPACCESS ipAccess, ipAccessBlock;
    struct DATABASE *b = NULL;
    BOOL isMyLoggedIn = 1;
    int isMe;
    GROUPMASK gmBlock = NULL;
    BOOL resetAtEnd = 0;
    BOOL netlib = FALSE;

    if (gmAuto)
        GroupMaskClear(gmAuto);

    if (anyExcluded)
        *anyExcluded = 0;

    if (autoUser)
        *autoUser = NULL;

    ipAccess = IPAREMOTE;
    i = 0;
    max = cIpTypes;
    if (url) {
        if (VerifyHost2(url, NULL, &b, &isMyLoggedIn, &isMe) == 0 && b != NULL) {
            /* Short-circuit FMG for special processing where if inst= included,
               require authentication, but if inst= not included, treat as local
               and force redirect
            */
            if (b && b->fmgConfigs) {
                return FMGHasInst(url) ? IPAREMOTE : IPALOCAL;
            }

            netlib = b != NULL && b->netLibraryConfigs != NULL;
            /* When NLReturn is present, we always have to require the user to log in */
            if (netlib && NetLibraryHasNLReturn(url))
                return IPAREMOTE;

            max = b->cIpTypes;
            if (sessionLife && *sessionLife != mSessionLife)
                *sessionLife = b->sessionLife;
        } else {
            if (isMe == 0) {
                if (optionRedirectUnknown)
                    resetAtEnd = 1;
                else {
                    struct REDIRECTSAFEORNEVERPROXYURLDATABASE *RedirectSafeOrNeverProxyURLDatabase;
                    if (!(RedirectSafeOrNeverProxyURLDatabase =
                              calloc(1, sizeof(struct REDIRECTSAFEORNEVERPROXYURLDATABASE))))
                        PANIC;
                    RedirectSafeOrNeverProxyURLDatabase =
                        RedirectSafeOrNeverProxyURL(url, 1, RedirectSafeOrNeverProxyURLDatabase);
                    if (RedirectSafeOrNeverProxyURLDatabase->result) {
                        t->redirectSafeOrNeverProxyCache = redirectSafeOrNeverProxyCacheTrue;
                        resetAtEnd = 1;
                    } else {
                        t->redirectSafeOrNeverProxyCache = redirectSafeOrNeverProxyCacheFalse;
                    }
                    FreeThenNull(RedirectSafeOrNeverProxyURLDatabase);
                }
            }
        }

        if (isMyLoggedIn)
            return IPAREMOTE;
    }

    for (b = databases, j = 0; j <= cDatabases; j++, b++) {
        gmBlock = NULL;
        if (j < cDatabases) {
            stop = b->cIpTypes;
        } else {
            stop = cIpTypes;
            if (resetAtEnd)
                ipAccess = IPALOCAL;
        }
        /* The following logic is duplicated in SPUEdit, so keep in sync */
        ipAccessBlock = IPAREMOTE;
        for (; i < stop; i++) {
            if (compare_ip(&t->sin_addr, &ipTypes[i].start) >= 0 &&
                compare_ip(&ipTypes[i].stop, &t->sin_addr) >= 0) {
                ipAccessBlock = ipTypes[i].ipAccess;
                /* for ipAccess, only consider entries up to "max", which is the point where
                   the destination database appears in EZPROXYCFG
                */
                if (i < max)
                    ipAccess = ipTypes[i].ipAccess;
                /* but, for exclusion and gmAuto, consider everything in the file */
                if (anyExcluded && ipAccess == IPALOCAL)
                    *anyExcluded = 1;
                /* In 3.0f and earlier, gmAuto was based on the last match; now, we or together
                    everything to maximize everything to minimize confusion
                */
                if (ipTypes[i].ipAccess == IPAAUTO) {
                    if (gmAuto)
                        gmBlock = ipTypes[i].gmAuto;
                    if (autoUser && ipTypes[i].autoUser)
                        *autoUser = ipTypes[i].autoUser;
                }
            }
        }
        if (gmAuto && gmBlock && ipAccessBlock == IPAAUTO)
            GroupMaskOr(gmAuto, gmBlock);
    }

    if (t->requireAuthenticate && (ipAccess == IPALOCAL || ipAccess == IPAAUTO))
        ipAccess = IPAREMOTE;

    if (ipAccess == IPAAUTO && netlib)
        ipAccess = IPALOCAL;

    if (autoUser) {
        if (ipAccess != IPAAUTO)
            *autoUser = NULL;
        else if (*autoUser == NULL)
            *autoUser = "auto";
    }

    return ipAccess;
}

BOOL IsRejectedIP(struct TRANSLATE *t) {
    int i;

    for (i = 0; i < mRejectIpTypes; i++) {
        if (compare_ip(&t->sin_addr, &rejectIpTypes[i].start) >= 0 &&
            compare_ip(&rejectIpTypes[i].stop, &t->sin_addr) >= 0) {
            return TRUE;
        }
    }
    return FALSE;
}

void CloseMsgFile(void) {
    if (msgFile != -1) {
        UUcloseFD(msgFile);
        msgFile = -1;
    }
    /* If we're in control of the STDIN and STDOUT FDs, close them, but reserve their FDs. */
    if (ttyDetach) {
        NullFD(1);
        NullFD(2);
        ttyClosed = TRUE;
    }
}

/* When calling this, the caller should hold the mMsgFile mutex */
BOOL MsgFilenameChanged(time_t now) {
    /* This routine must not call Log() */
    char tempFilename[MAX_PATH];
    struct tm tm;

    if (msgFilenameStrftime) {
        if (now == 0)
            UUtime(&now);

        UUlocaltime_r(&now, &tm);

        tempFilename[sizeof(tempFilename) - 1] = 0;
        strftime(tempFilename, sizeof(tempFilename) - 1, msgFilenameTemplate, &tm);

        if (strcmp(msgFilename, tempFilename) != 0) {
            StrCpy3(msgFilename, tempFilename, sizeof(msgFilename));
            msgFilenameChanged = TRUE;
        }
    }

    return msgFilenameChanged;
}

void NullFD(int fd) {
    int i;
#ifdef WIN32
    const char *nullFile = "nul";
#else
    const char *nullFile = "/dev/null";
#endif

    TEMP_FAILURE_RETRY_INT(i = interrupted_neg_int(open(nullFile, O_RDWR, defaultFileMode)));
    if (dup2(i, fd) != fd)
        PANIC;
    TEMP_FAILURE_RETRY_INT(interrupted_neg_int(close(i)));
}

int UUopenCreateFDSilent(const char *path, int flags, int mode);
int UUopenExistantFDSilent(const char *path, int flags);
int OpenMsgFile(void) {
    if (msgFile != -1) {
        if (!MsgFilenameChanged(0))
            return 0;
        CloseMsgFile();
    } else {
        msgFile = UUopenExistantFDSilent(EZPROXYMSG, O_RDONLY);
        if (msgFile >= 0) {
            char line[2048];
            char *p, *q;
            FileReadLine(msgFile, line, sizeof(line), NULL);
            Trim(line, TRIM_LEAD | TRIM_TRAIL);
            if (p = StartsIWith(line, "MessagesFile ")) {
                p = SkipWS(p);
                msgFilenameStrftime = FALSE;
                if (q = StartsIWith(p, "-strftime ")) {
                    msgFilenameStrftime = TRUE;
                    p = SkipWS(q);
                }
                if (*p) {
                    msgFilenameTemplate = StaticStringCopy(p);
                    if (!msgFilenameStrftime)
                        StrCpy3(msgFilename, p, sizeof(msgFilename));
                } else {
                    msgFilenameTemplate = EZPROXYMSG;
                    msgFilenameStrftime = FALSE;
                }
            }
            UUcloseFD(msgFile);
        }

        MsgFilenameChanged(0);
    }

    msgFilenameChanged = FALSE;

    msgFile = UUopenCreateFDSilent(msgFilename, O_APPEND | O_CREAT | O_WRONLY, defaultFileMode);
    if (msgFile == -1) {
        char *errorMsg = ErrorMessage(errno);
        fprintf(stderr, "Unable to open %s: %s\n", msgFilename, errorMsg);
        FreeThenNull(errorMsg);
        return 1;
    }
    if (ttyClosed) {
        dup2(msgFile, 1); /* Closes 1 and copies msgFile to 1. */
        dup2(msgFile, 2); /* Closes 2 and copies msgFile to 2. */
    }

    return 0;
}

#ifdef USEPOLL
int PollCompare(const void *v1, const void *v2) {
    struct pollfd *p1 = (struct pollfd *)v1;
    struct pollfd *p2 = (struct pollfd *)v2;

    return p1->fd - p2->fd;
}

void PollShow(struct pollfd *pfds, unsigned int pfdc) {
    unsigned int i;

    for (i = 0; i < pfdc; i++)
        Log("%d=%d ", i, pfds[i].fd);
}

void PollSort(struct pollfd *pfds, unsigned int pfdc) {
    qsort(pfds, pfdc, sizeof(*pfds), PollCompare);
}

short PollRevents(struct pollfd *pfds, unsigned int pfdc, SOCKET s) {
    int lo, hi, mid;

    lo = 0;
    hi = pfdc - 1;

    while (lo <= hi) {
        mid = (lo + hi) / 2;
        if (pfds[mid].fd == s)
            return pfds[mid].revents;
        if (pfds[mid].fd < s)
            lo = mid + 1;
        else
            hi = mid - 1;
    }
    if (debugLevel > 50) {
        Log("no match? %d ", s);
        PollShow(pfds, pfdc);
    }
    return 0;
}
#endif

void safe_portable_checked_vsnprintf(char *buffer,
                                     const size_t size,
                                     const char *format,
                                     va_list ap) {
    _SNPRINTF_BEGIN(buffer, size);
    _vsnprintf(buffer, size, format, ap);
    _SNPRINTF_PANIC_OVERFLOW(buffer, size);
    _SNPRINTF_END(buffer, size);
}

void safe_portable_checked_snprintf(char *buffer, const size_t size, const char *format, ...) {
    va_list ap;

    va_start(ap, format);
    safe_portable_checked_vsnprintf(buffer, size, format, ap);
    va_end(ap);
}

void safe_portable_vsnprintf(char *buffer, const size_t size, const char *format, va_list ap) {
    _SNPRINTF_BEGIN(buffer, size);
    _vsnprintf(buffer, size, format, ap);
    _SNPRINTF_END(buffer, size);
}

void safe_portable_snprintf(char *buffer, const size_t size, const char *format, ...) {
    va_list ap;

    va_start(ap, format);
    safe_portable_vsnprintf(buffer, size, format, ap);
    va_end(ap);
}

void reportFileLineCode(char *file, int line, void *code) {
    char buffer[128];

    safe_portable_snprintf(buffer, 128, "In %s at %d code %08X.\n", file, line, code);
    WriteTrace(1, 0, NULL, buffer);
    fflush(stdout);
}

volatile BOOL panicing = FALSE;

void panic(const char *file, int line, int saveErrno) {
    char buffer[128];

    if (!panicing) {
        panicing = TRUE;
        int f = UUopenSimpleFD(FAILFILE, O_APPEND | O_CREAT | O_WRONLY, defaultFileMode);
        safe_portable_snprintf(buffer, 128, "PANIC in %s at %d error %d\n", file, line, saveErrno);
        WriteTrace(f >= 0 ? f : 1, 0, NULL, buffer);
        if (f >= 0) {
            close(f);
        }
    }

    exit(1);
}

char *panicstr(const char *file, int line, int saveErrno, const char *txt) {
    char buffer[128];

    while (panicing) {
        exit(1);
    };
    panicing = TRUE;

    safe_portable_snprintf(buffer, 128, "PANIC in %s at %d error %s\n", file, line, txt);
    WriteTrace(1, 0, NULL, buffer);
    fflush(stdout);

    exit(1);
    return "";
}

int panic0(const char *file, int line, int saveErrno, const char *txt) {
    char buffer[128];

    while (panicing) {
        exit(1);
    };
    panicing = TRUE;

    safe_portable_snprintf(buffer, 128, "PANIC in %s at %d error %s\n", file, line, txt);
    WriteTrace(1, 0, NULL, buffer);
    fflush(stdout);

    exit(1);
    return 0;
}

void SubSleep(int sec, int millisec) {
#ifdef WIN32
    Sleep(sec * 1000 + millisec);
#else
#ifdef USEPOLL
    int rc, part;
    time_t t1, t2;

    UUtime(&t1);

    for (; sec > 0 || millisec > 0; sec -= part, millisec = 0) {
        part = UUMIN(sec, 3600);
        rc = poll(NULL, 0, part * 1000 + millisec);

        if (rc != 0) {
            if (errno != EINTR) {
                UUtime(&t2);
                Log("Poll() returned %d, errno %d, time %d.", rc, errno, t2 - t1);
            }
        }
    }
#else
    struct timeval tv;

    tv.tv_sec = sec;
    tv.tv_usec = millisec * 1000;
    select(0, NULL, NULL, NULL, &tv);
#endif
#endif
}

void ParseCommand(char *cmd, size_t *cmdLen, char **arg) {
    char *p;

    for (p = cmd; *p && !IsWS(*p); p++)
        if (islower(*p))
            *p = toupper(*p);
    *cmdLen = p - cmd;
    p = SkipWS(p);
    *arg = p;
}
/**
 * check if the cmd is a substring of match starting at position 0.
 *
 * If minMatch = 0, then test cmd == match
 */
BOOL MatchCommand(char *cmd, size_t cmdLen, char *match, size_t minMatch) {
    size_t matchlen;

    if (minMatch == 0)
        minMatch = strlen(match);

    if (cmdLen < minMatch)
        return 0;

    matchlen = strlen(match);
    if (matchlen < cmdLen)
        return 0;
    if (matchlen > cmdLen)
        matchlen = cmdLen;
    return strnicmp(cmd, match, matchlen) == 0;
}

char *SkipNonName(char *s, char *l, BOOL utf8, BOOL *sawNonAlpha) {
    char c;
    unsigned char uc;
    unsigned char *us = (unsigned char *)s;

    if (sawNonAlpha)
        *sawNonAlpha = 0;
    for (;; us++) {
        if (utf8 && (*us & 224) == 192 && (*(us + 1) & 192) == 128) {
            uc = (*us & 31) << 6;
            us++;
            uc = uc | (*us & 63);
            c = (char)uc;
        } else {
            c = (char)*us;
        }
        c = StripISO88591Diacritics(c);
        if (c == 0 || IsAlpha(c))
            break;
        if (sawNonAlpha)
            *sawNonAlpha = 1;
    }
    *l = isupper(c) ? tolower(c) : c;
    return (char *)us;
}

/* Assumes n1 is in UTF-8 and n2 is in ISO-8859 raw */

BOOL NameMatch(char *n1, char *n2, BOOL partialNameMatch) {
    char l1, l2;
    BOOL sawNonAlpha;
    BOOL matchedAny = 0;

    for (;; n1++, n2++) {
        n1 = SkipNonName(n1, &l1, 1, &sawNonAlpha);
        n2 = SkipNonName(n2, &l2, 0, NULL);
        if (l1 != l2) {
            if (partialNameMatch && l1 != 0 && l2 == 0 && sawNonAlpha)
                return matchedAny;
            return 0;
        }
        if (l1 == 0)
            return 1;
        matchedAny = 1;
    }
}

char *NameParts(char *s, BOOL utf8, size_t *longestPart) {
    BOOL sawNonAlpha;
    char l;
    char *parts, *p, *q;
    char *dropWords[] = {"jr", "sr", NULL};
    char **dw;
    char *e;

    if (!(parts = p = q = malloc(strlen(s) + 2)))
        PANIC;
    *longestPart = 0;

    for (; *s; s++) {
        if (*s == '\'') {
            continue;
        }
        s = SkipNonName(s, &l, utf8, &sawNonAlpha);
        if (l == 0)
            break;
        if (sawNonAlpha && p != parts) {
            if ((size_t)(p - q) > *longestPart)
                *longestPart = p - q;
            *p++ = 0;
            q = p;
        }
        *p++ = l;
    }

    if ((size_t)(p - q) > *longestPart)
        *longestPart = p - q;

    *p++ = 0;
    *p++ = 0;
    e = p;

    /* Remove single characters unless all single characters and remove stop words */
    if (*longestPart > 1) {
        for (p = parts; *p; p = q) {
            q = strchr(p, 0) + 1;
            for (dw = dropWords; *dw; dw++) {
                if (stricmp(p, *dw) == 0)
                    break;
            }
            if (*dw) {
                memmove(p, q, e - q);
                e -= (q - p);
                q = p;
            }
        }
    }

    return parts;
}

BOOL CompareNameParts(char *n1, BOOL utf1, char *n2, BOOL utf2) {
    char *p1, *p2;
    char *q1, *q2;
    size_t len1, len2;
    BOOL match = 0;

    p1 = NameParts(n1, utf1, &len1);
    p2 = NameParts(n2, utf2, &len2);

    /* Otherwise, compare each part of III to each part of user string */
    for (q1 = p1; match == 0 && *q1; q1 = strchr(q1, 0) + 1) {
        /* If we have any III name part of more than 1 character, ignore any single character parts
         */
        if (len1 > 1 && strlen(q1) < 2)
            continue;
        for (q2 = p2; *q2; q2 = strchr(q2, 0) + 1) {
            if (strcmp(q1, q2) == 0) {
                match = 1;
                goto Done;
            }
        }
    }

Done:
    FreeThenNull(p1);
    FreeThenNull(p2);

    return match;
}

BOOL WildCompareArray1(const char **a, const char *s, const char *w, BOOL caseSensitive) {
    char ls, lw;

    if (s == NULL)
        s = "";

    for (;; s++, w++) {
        if (*w == '*')
            break;
        while (*s == 0) {
            if (*a == NULL)
                break;
            s = *a++;
        }
        if (caseSensitive) {
            ls = *s;
            lw = *w;
        } else {
            ls = (isupper(*s) ? tolower(*s) : *s);
            lw = (isupper(*w) ? tolower(*w) : *w);
        }
        if (ls == lw || (ls != 0 && lw == '?')) {
            if (ls == 0)
                return 1;
        } else {
            return 0;
        }
    }

    /* skip multiple * in a row */
    for (; w++, *w == '*';)
        ;

    if (*w == 0)
        return 1;

    for (;; s++) {
        while (*s == 0) {
            if (*a == NULL)
                return 0;
            s = *a++;
        }
        if (WildCompareArray1(a, s, w, caseSensitive))
            return 1;
    }
}

BOOL WildCompareArray(const char **a, const char *w, BOOL caseSensitive) {
    return WildCompareArray1(a, NULL, w, caseSensitive);
}

BOOL WildCompare(const char *s, const char *w) {
    const char *a[2];

    a[0] = s;
    a[1] = NULL;
    return WildCompareArray(a, w, 0);
}

BOOL WildCompareCaseSensitive(const char *s, const char *w) {
    const char *a[2];

    a[0] = s;
    a[1] = NULL;
    return WildCompareArray(a, w, 1);
}

BOOL WildCompareOld(char *s, char *w) {
    char ls, lw;

    for (;; s++, w++) {
        if (*w == '*')
            break;
        ls = (isupper(*s) ? tolower(*s) : *s);
        lw = (isupper(*w) ? tolower(*w) : *w);
        if (ls != lw)
            return 0;
        if (ls == 0)
            return 1;
    }

    /* skip multiple * in a row */
    for (; w++, *w == '*';)
        ;

    if (*w == 0)
        return 1;

    for (; *s; s++) {
        if (WildCompare(s, w))
            return 1;
    }
    return 0;
}

int UUFILEFD(FILE *file) {
#ifdef WIN32
    return _fileno(file);
#else
    return fileno(file);
#endif
}

int UUopenSimpleFD(const char *path, int flags, int mode) {
    int rc;

    TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(open(path, flags, mode)));
    if (debugLevel > 8000)
        Log("%" SOCKET_FORMAT " Opened %s, errno=%d", rc, path, (rc < 0) ? errno : 0);
    return rc;
}

int UUopenCreateSimpleFD(const char *path, int flags, int mode) {
    int rc;

    TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(open(path, flags | O_CREAT, mode)));
    if (debugLevel > 8000)
        Log("%" SOCKET_FORMAT " Opened %s, errno=%d", rc, path, (rc < 0) ? errno : 0);
    return rc;
}

int UUopenCreateFD(const char *path, int flags, int mode) {
    int rc;

    TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(open(path, flags | O_CREAT, mode)));
    if (debugLevel > 8000)
        Log("%" SOCKET_FORMAT " Opened %s, errno=%d", rc, path, (rc < 0) ? errno : 0);
    TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(MoveFileDescriptorHigh(rc)));
    return rc;
}

int UUopenCreateFDSilent(const char *path, int flags, int mode) {
    /* This routine must not call Log() */
    int rc;

    TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(open(path, flags | O_CREAT, mode)));
    return rc;
}

int UUopenExistantSimpleFD(char *path, int flags) {
    int rc;

    TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(open(path, flags)));
    if (debugLevel > 8000)
        Log("%" SOCKET_FORMAT " Opened %s, errno=%d", rc, path, (rc < 0) ? errno : 0);
    return rc;
}

int UUopenExistantFD(const char *path, int flags) {
    int rc;

    TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(open(path, flags)));
    if (debugLevel > 8000)
        Log("%" SOCKET_FORMAT " Opened %s, errno=%d", rc, path, (rc < 0) ? errno : 0);
    TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(MoveFileDescriptorHigh(rc)));
    return rc;
}

int UUopenExistantFDSilent(const char *path, int flags) {
    /* This routine must not call Log() */
    int rc;

    TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(open(path, flags)));
    return rc;
}

int UUreadFD(int f, void *buf, size_t len) {
    int rc;

    TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(read(f, buf, len)));
    return rc;
}

int UUwriteFD(int f, void *vbuf, size_t len) {
    int rc;
    int sent;
    char *buf = (char *)vbuf;

    sent = 0;
    for (; len > 0;) {
        TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(write(f, buf, len)));
        if (rc >= 0) {
            sent += rc;
            buf += rc;
            len -= rc;
            continue;
        }
        return rc;
    }
    return sent;
}

int UUfputsFD(char *s, int f) {
    return UUwriteFD(f, s, strlen(s));
}

int UUfputiFD(int i, int f) {
    char s[16];

    sprintf(s, "%d", i);
    return UUwriteFD(f, s, strlen(s));
}

int UUfputdFD(double i, int f) {
    char s[56];

    sprintf(s, "%f", i);
    return UUwriteFD(f, s, strlen(s));
}

int UUfputcFD(char c, int f) {
    return UUwriteFD(f, &c, 1);
}

int UUcloseFD(int s) {
    int rc;

    if (s < 0)
        return s;

    TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(close(s)));
    if (debugLevel > 8000) {
        Log("%" SOCKET_FORMAT " Closed errno=%d", s, rc);
    }
    return rc;
}

void ApplyProxyHostnameEdits(char *proxyHostname) {
    struct PROXYHOSTNAMEEDIT *phe;
    char *p;
    size_t findLen, replaceLen;
    int i;

    for (phe = proxyHostnameEdits, i = 0; i < cProxyHostnameEdits; i++, phe++) {
        findLen = strlen(phe->find);

        if (*(phe->find) == '^') {
            /* Adjust findLen to ignore ^ */
            findLen--;
            if (strnicmp(proxyHostname, phe->find + 1, findLen) != 0)
                continue;
            p = proxyHostname;
        } else if (findLen > 1 && *(phe->find + findLen - 1) == '$') {
            /* Adjust findLen to ignore $ */
            findLen--;
            p = strchr(proxyHostname, 0) - findLen;
            if (p < proxyHostname || strnicmp(p, phe->find, findLen) != 0)
                continue;
        } else {
            p = StrIStr(proxyHostname, phe->find);
            if (p == NULL)
                continue;
        }

        replaceLen = strlen(phe->replace);

        /* Don't forget to move the NULL as well */
        if (replaceLen != findLen)
            memmove(p + replaceLen, p + findLen, strlen(p + findLen) + 1);
        memmove(p, phe->replace, replaceLen);
        break;
    }
}

char *ProxyHostname(char *hostname,
                    PORT remport,
                    PORT forceMyPort,
                    char useSsl,
                    char ftpgw,
                    BOOL optionNoHttpsHyphens) {
    char *proxyHostname;

    if (!(proxyHostname = (char *)malloc(strlen(hostname) + strlen(myName) + 32 +
                                         maxIncreaseProxyHostnameEdit)))
        PANIC;

    if (forceMyPort) {
        sprintf(proxyHostname, "%s:%d", myName, forceMyPort);
    } else {
        if (ftpgw == 0 && ((useSsl == 0 && remport == 80) ||
                           (useSsl != 0 && remport == 443 && optionForceSsl == 0))) {
            strcpy(proxyHostname, hostname);
        } else {
            sprintf(proxyHostname, "%s%d-%s", (ftpgw ? "f" : (useSsl ? "s" : "p")), remport,
                    hostname);
        }

        ApplyProxyHostnameEdits(proxyHostname);

        if (optionNoHttpsHyphens == 0 && optionWildcardHTTPS && (useSsl || optionForceSsl)) {
            PeriodsToHyphens(proxyHostname);
        }
        strcat(proxyHostname, ".");
        strcat(proxyHostname, myName);

        if (useSsl || optionForceSsl) {
            if (primaryLoginPortSsl != 443)
                sprintf(strchr(proxyHostname, 0), ":%d", primaryLoginPortSsl);
        } else {
            if (primaryLoginPort != 80)
                sprintf(strchr(proxyHostname, 0), ":%d", primaryLoginPort);
        }
    }

    if (!(proxyHostname = (char *)realloc(proxyHostname, strlen(proxyHostname) + 1)))
        PANIC;

    return proxyHostname;
}

void SetProxyHostname(struct HOST *h) {
    char *newName;
    char *hostUrl;

    /* This routine can cause an old version of h->proxyHostname to get dereferenced
       whenever the current certificate goes from wildcard to non-wildcard, but I'm
       worried about the alternative of freeing something that some other part of
       EZproxy may now have in use, so I'm willing to accept a few dangling pointers
       on those rare occassions when we switch from wildcard to non-wildcard in a
       single instance
    */
    newName = ProxyHostname(h->hostname, h->remport, h->forceMyPort, h->useSsl, h->ftpgw,
                            (h->myDb ? (h->myDb)->optionNoHttpsHyphens : 0));
    if (h->proxyHostname == NULL || strcmp(h->proxyHostname, newName) != 0)
        h->proxyHostname = newName;
    else {
        FreeThenNull(newName);
    }

    if (h->hostUrl == NULL) {
        PORT defaultPort;
        /* Extra space for worst case of https://, :12345, and the null */
        if (!(hostUrl = malloc(strlen(h->hostname) + 15)))
            PANIC;
        if (h->ftpgw) {
            defaultPort = 21;
            strcpy(hostUrl, "ftp");
        } else {
            if (h->useSsl) {
                defaultPort = 443;
                strcpy(hostUrl, "https");
            } else {
                defaultPort = 80;
                strcpy(hostUrl, "http");
            }
        }
        strcat(hostUrl, "://");
        strcat(hostUrl, h->hostname);
        if (h->remport != defaultPort) {
            sprintf(strchr(hostUrl, 0), ":%d", h->remport);
        }
        AllLower(hostUrl);
        h->hostUrl = hostUrl;
    }

    UUtime(&dnsSerial);
}

int CompareProxyHostname(char *host, char *proxyHost, size_t len) {
    char *ph, *pph;

    for (ph = host, pph = proxyHost; len > 0; ph++, pph++, len--) {
        if (*ph != *pph) {
            /* allow old entries where . in provided (e.g. bookmark) but _ in the new to still work
             */
            if (*ph == '.' && *pph == '-')
                continue;
            return *ph - *pph;
        }
    }

    return 0;
}

// isPDF can also be set up by checking byteServes.
// In that scenario, the URL is not cached since it will be rediscovered every time by
// the check of byteServes.
struct PDFURL *FindPdfUrl(struct TRANSLATE *t, BOOL add) {
    struct PDFURL *h, *p, *n;
    int count;

    /* Short-circuit test to avoid using mutex
       if there are no URLs to check and this isn't an add
    */

    if (t->session == NULL || ((t->session)->pdfUrls == NULL && add == 0))
        return NULL;

    UUAcquireMutex(&mPdfUrls);
    h = (t->session)->pdfUrls;
    if (h == NULL) {
        if (!(h = (struct PDFURL *)calloc(1, sizeof(struct PDFURL))))
            PANIC;
        (t->session)->pdfUrls = h->flink = h->blink = h;
    }

    for (p = h->flink, count = 0; p != h; p = p->flink, count++) {
        if (strcmp(t->urlCopy, p->url) == 0) {
            n = p;
            goto MoveToFront;
        }
    }

    if (add == 0) {
        UUReleaseMutex(&mPdfUrls);
        return NULL;
    }

    if (count == MAXPDFURLS) {
        n = h->blink;
        if (!(n->url = realloc(n->url, strlen(t->urlCopy) + 1)))
            PANIC;
        strcpy(n->url, t->urlCopy);
    } else {
        if (!(n = (struct PDFURL *)calloc(1, sizeof(struct PDFURL))))
            PANIC;
        n->flink = n->blink = n;
        if (!(n->url = strdup(t->urlCopy)))
            PANIC;
    }

MoveToFront:
    if (h->flink != n) {
        (n->blink)->flink = n->flink;
        (n->flink)->blink = n->blink;

        n->blink = h;
        n->flink = h->flink;
        (n->blink)->flink = n;
        (n->flink)->blink = n;
    }

    t->isPDF = 1;
    UUReleaseMutex(&mPdfUrls);
    return n;
}

void CheckIsPdfUrl(struct TRANSLATE *t) {
    FindPdfUrl(t, 0);
}

#define errorStringSize 1024
char *ErrorMessage(int errorCode) {
    char *errorMessage;
    char errorCodeString[16];
    size_t errorCodeStringLen;
    char *errorText = NULL;
#ifdef HAVE_STRERROR_R
    char errorString[errorStringSize];
#if !defined(STRERROR_R_CHAR_P)
    int rc;
#endif
#endif
#ifdef WIN32
    char *errorTextWin32 = NULL;
#endif

    sprintf(errorCodeString, "(%d)", errorCode);
    errorCodeStringLen = strlen(errorCodeString) + 1;

#ifdef WIN32
    if (errorCode < _sys_nerr)
        errorText = strerror(errorCode);
#else
#ifdef HAVE_STRERROR_R
#if !defined(STRERROR_R_CHAR_P)
    /* The POSIX version. */
    errorString[0] = 0;
    rc = strerror_r(errorCode, errorString, sizeof(errorString));
    errorText = errorString;
#elif defined(STRERROR_R_CHAR_P)
    /* The GLIBC version. */
    errorString[0] = 0;
    errorText = strerror_r(errorCode, errorString, sizeof(errorString));
#else
    /* The ANSI "C" version. */
    errorText = strerror(errorCode); /* Assume thread-safe */
#endif
#else
    /* The ANSI "C" version. */
    errorText = strerror(errorCode); /* Assume thread-safe */
#endif
#endif

    if (errorText) {
        if (!(errorText = strdup(errorText)))
            PANIC;
    }
#ifdef WIN32
    else {
        if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_IGNORE_INSERTS |
                              FORMAT_MESSAGE_FROM_SYSTEM,
                          NULL, /* module to get message from (NULL == system) */
                          errorCode,
                          MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), /* default language */
                          (LPSTR)&errorTextWin32, 0, NULL) == 0 &&
            errorTextWin32 != NULL) {
            LocalFree(errorTextWin32);
            errorTextWin32 = NULL;
        } else {
            errorText = errorTextWin32;
        }
    }
#endif

    if (errorText == NULL) {
        if (!(errorMessage = malloc(errorCodeStringLen)))
            PANIC;
        strcpy(errorMessage, errorCodeString);
    } else {
        Trim(errorText, TRIM_COMPRESS);
        if (!(errorMessage = malloc(errorCodeStringLen + strlen(errorText) + 1)))
            PANIC; /* +1 for space between */
        if (errorMessage == NULL)
            PANIC;
        sprintf(errorMessage, "%s %s", errorCodeString, errorText);
    }

#ifdef WIN32
    if (errorTextWin32) {
        LocalFree(errorText);
        errorText = errorTextWin32 = NULL;
    }
#endif

    FreeThenNull(errorText);

    return errorMessage;
}

char *PoundFix(char *url, size_t max, BOOL forceQuote) {
    char *p;
    size_t len;
    char *result = "";
    char hex[5];

    // To force internally generated bodies to be secure,
    // always force quoting to occur.
    forceQuote = 1;

    if (forceQuote == 0 && strchr(url, '#') == NULL)
        return "";

    len = strlen(url);
    /* Assume this may be a sizeof() max, so take away one to keep room for NULL */
    max--;

    for (p = url; *p; p++) {
        if (IsDigit(*p) || IsAlpha(*p) || strchr("~.-_", *p) != NULL) {
            continue;
        }
        /* Really shouldn't be spaces here, but in case, encode them efficiently */
        if (*p == ' ') {
            *p = '+';
            continue;
        }
        /* If there's not enough room to expand, stop processing */
        if (len + 2 > max)
            break;

        /* Insert two characters, moving strlen(p) so we move the NULL as well */
        memmove(p + 3, p + 1, strlen(p));
        /* Note that string has grown by 2 */
        len += 2;
        sprintf(hex, "%02x", (unsigned int)(*p & 0xff));
        *p++ = '%';
        *p++ = hex[0];
        *p = hex[1]; /* the for will advance the pointer beyond this char */
        result = "q";
    }
    return result;
}

time_t ParseHttpDate(char *s, time_t dt) {
    struct tm tm, rtm;
    char *comma;
    char *space;
    int hour, min, sec;
    int diff;
    time_t newDtLocal, newDtGm;

    memset(&tm, 0, sizeof(tm));
    if (s == NULL)
        return dt;

    if (comma = strchr(s, ','))
        s = SkipWS(comma + 1);

    if (IsDigit(*s)) {
        /* Parsing these formats (already skipped to beyond comma):
           Sun, 06 Nov 1994 08:49:37 GMT  ; RFC 822, updated by RFC 1123
           Sunday, 06-Nov-94 08:49:37 GMT ; RFC 850, obsoleted by RFC 1036
        */

        tm.tm_mday = atoi(s);

        s += 2;
        if (*s != ' ' && *s != '-')
            return dt;
        s++;

        for (tm.tm_mon = 0;; tm.tm_mon++)
            if (tm.tm_mon == 12)
                return dt;
            else if (strnicmp(s, monthNames[tm.tm_mon], 3) == 0)
                break;

        s += 3;
        if (*s != ' ' && *s != '-')
            return dt;
        s++;

        space = strchr(s, ' ');
        if (space == NULL || space - s > 4)
            return dt;
        tm.tm_year = atoi(s);

        s = SkipWS(space + 1);
        sscanf(s, "%d:%d:%d", &hour, &min, &sec);
        tm.tm_hour = hour;
        tm.tm_min = min;
        tm.tm_sec = sec;
        s = SkipWS(SkipNonWS(s));
        if (strnicmp(s, "GMT", 3) != 0)
            return dt;
        s = SkipWS(s + 3);
        /* enough checked, consider it OK
        if (*s != 0)
            return dt;
        */
    } else {
        /* Try Sun Nov  6 08:49:37 1994 format */
        /* First, try to skip day of week */
        s = SkipWS(SkipNonWS(s));

        for (tm.tm_mon = 0;; tm.tm_mon++)
            if (tm.tm_mon == 12)
                return dt;
            else if (strnicmp(s, monthNames[tm.tm_mon], 3) == 0)
                break;

        s += 3;
        if (*s != ' ')
            return dt;
        s = SkipWS(s);

        if (!IsDigit(*s))
            return dt;

        tm.tm_mday = atoi(s);
        if (tm.tm_mday < 1 || tm.tm_mday > 31)
            return dt;

        s = SkipWS(SkipNonWS(s));

        sscanf(s, "%d:%d:%d", &hour, &min, &sec);
        tm.tm_hour = hour;
        tm.tm_min = min;
        tm.tm_sec = sec;

        s = SkipWS(SkipNonWS(s));

        if (!IsDigit(*s))
            return dt;

        tm.tm_year = atoi(s);
    }

    /* We are working with GMT where DST isn't observed */
    tm.tm_isdst = 0;

    if (tm.tm_year < 0)
        return dt;

    if (tm.tm_mday < 1 || tm.tm_mday > 31)
        return dt;

    if (tm.tm_hour < 0 || tm.tm_hour > 23 || tm.tm_min < 0 || tm.tm_min > 59 || tm.tm_sec < 0 ||
        tm.tm_sec > 59)
        return dt;

    /* Handle 2-digit years as 50-99 is 1950-1999, 00-49 is 2000 - 2049 */
    if (tm.tm_year >= 1900)
        tm.tm_year -= 1900;
    else if (tm.tm_year < 50)
        tm.tm_year += 100;

    /* If set before the first year of the epoch, treat it like the epoch */
    if (tm.tm_year < 70)
        return 0;

    /* Consider the entire first day of the epoch to be the epoch.
       This avoids a glitch with mktime dealing with the epoch, since mktime
       tries to represent in local time first, and 1 Jan 1970 at midnight wasn't
       possible in time zones with positive offsets from UTC
    */
    if (tm.tm_year == 70 && tm.tm_mon == 0 && tm.tm_mday == 1)
        return 0;

    newDtLocal = mktime(&tm);
    if (newDtLocal == (time_t)-1)
        return dt;
    if (UUgmtime_r(&newDtLocal, &rtm) == NULL)
        return dt;

    newDtGm = mktime(&rtm);
    if (newDtGm == (time_t)-1)
        return dt;

    /* Now convert to GMT by computing time zone difference and applying */
    diff = newDtGm - newDtLocal;
    if (diff > newDtLocal)
        return dt;

    return newDtLocal - diff;
}

char *FormatHttpDate(char *s, size_t len, time_t dt) {
    struct tm rtm;

    strftime(s, len, "%a, %d %b %Y %H:%M:%S GMT", UUgmtime_r(&dt, &rtm));
    *(s + len - 1) = 0;
    return s;
}

BOOL CheckIfDevice(char *filename) {
#ifndef WIN32
    return 0;
#else
    BOOL device;
    HANDLE h;

    h = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                   FILE_ATTRIBUTE_NORMAL, NULL);

    if (h == INVALID_HANDLE_VALUE)
        device = 0;
    else {
        device = GetFileType(h) != FILE_TYPE_DISK;
        CloseHandle(h);
    }

    return device;
#endif
}

BOOL CheckIfFile(char *fileName) {
    struct stat fileStat;

    if (stat(fileName, &fileStat) < 0 || *fileName == 0)
        return 0;

    if ((fileStat.st_mode & S_IFREG) == 0)
        return 0;

    return 1;
}

void FindMIMEType(char *filename, char *mimeType, size_t mimeTypeLen, char *defaultMimeType) {
    int f;
    char line[256];
    char *text;
    char *str;
    char *token;
    char *ext;
    struct FILEREADLINEBUFFER frb;

    *mimeType = 0;
    ext = strrchr(filename, '.');
    if (ext != NULL) {
        ext++;
        f = SOPEN(MIMETYPE);
        InitializeFileReadLineBuffer(&frb);
        while (*mimeType == 0 && FileReadLine(f, line, sizeof(line), &frb)) {
            text = NULL;
            for (str = NULL, token = StrTok_R(line, WS, &str); *mimeType == 0 && token;
                 token = StrTok_R(NULL, WS, &str)) {
                if (*token == 0)
                    continue;
                if (text == NULL) {
                    text = token;
                    continue;
                }
                if (stricmp(token, ext) == 0) {
                    StrCpy3(mimeType, text, mimeTypeLen);
                    break;
                }
            }
        }

        close(f);
    }

    if (*mimeType == 0 && defaultMimeType != NULL)
        StrCpy3(mimeType, defaultMimeType, mimeTypeLen);
}

int UUmkdir(char *filename) {
    int rc;
#ifdef WIN32
    rc = mkdir(filename);
#else
    rc = mkdir(filename, 0777);
#endif
    if (debugLevel > 5)
        Log("Mkdir %s rc=%d errno=%d", filename, rc, errno);
    return rc;
}

struct tm *UUgmtime_r(const time_t *clock, struct tm *result) {
#if defined(UUTIME_R2) || defined(UUTIME_R3)
    return gmtime_r(clock, result);
#else
    struct tm *tm;

#ifdef UUTIME_R0
    UUAcquireMutex(&mTimeFuncs);
#endif

    if (tm = gmtime(clock))
        memcpy(result, tm, sizeof(*result));

#ifdef UUTIME_R0
    UUReleaseMutex(&mTimeFuncs);
#endif

    return (tm ? result : NULL);
#endif
}

struct tm *UUlocaltime_r(const time_t *clock, struct tm *result) {
#if defined(UUTIME_R2) || defined(UUTIME_R3)
    return localtime_r(clock, result);
#else
    struct tm *tm;

#ifdef UUTIME_R0
    UUAcquireMutex(&mTimeFuncs);
#endif

    if (tm = localtime(clock))
        memcpy(result, tm, sizeof(*result));

#ifdef UUTIME_R0
    UUReleaseMutex(&mTimeFuncs);
#endif

    return (tm ? result : NULL);
#endif
}

char *UUctime_r(const time_t *clock, char *buf, int buflen) {
#if (!(defined(UUTIME_R2) || defined(UUTIME_R3)))
    char *c;
#endif

    if (buflen < 26)
        PANIC; /* POSIX and GNU require this */

#if defined(UUTIME_R2)
    return ctime_r(clock, buf);
#elif defined(UUTIME_R3)
    return ctime_r(clock, buf, buflen);
#else

#ifdef UUTIME_R0
    UUAcquireMutex(&mTimeFuncs);
#endif

    if (c = ctime(clock))
        StrCpy3(buf, c, buflen);
    else
        *buf = 0;

#ifdef UUTIME_R0
    UUReleaseMutex(&mTimeFuncs);
#endif

    return (c ? buf : NULL);
#endif
}

/* UUtime gets the time in seconds from January 1, 1970, not counting leap seconds. */
#ifdef UUPROFILETIME
time_t UUtimex(time_t *tloc, char *file, int line)
#else
time_t UUtime(time_t *tloc)
#endif
{
    /* UUtime gets the time in seconds from January 1, 1970, not counting leap seconds. */
#ifdef UUPROFILETIME
    struct WHERE {
        struct WHERE *next;
        char *file;
        int line;
        int count;
    };

    static struct WHERE *list = NULL;
    struct WHERE *p;

    for (p = list; p; p = p->next) {
        if (strcmp(p->file, file) == 0 && p->line == line)
            break;
    }

    if (p == NULL) {
        if (!(p = calloc(1, sizeof(*p))))
            PANIC;
        p->file = strdup(file);
        p->line = line;
        p->next = list;
        list = p;
    }

    p->count = p->count + 1;

    if (p->count % 100 == 0) {
        printf("%s %d %d\n", p->file, p->line, p->count);
    }
#endif

#ifdef UUTIMEGETHRTIME
    {
        static time_t global = 0;
        static hrtime_t delta = 200000000; /* .1 of a second */
        static hrtime_t oldTime = 0;
        hrtime_t newTime;

        newTime = gethrtime();
        if (newTime - oldTime > delta) {
            global = time(tloc);
            oldTime = newTime;
        } else {
            if (tloc)
                *tloc = global;
        }
        return global;
    }
#else
#ifdef UUTIMEGETTIMEOFDAY
    {
        /* We want a resolution of one second. */
        static time_t global = 0;
        static struct timeval oldTv = {0, 0};
        struct timeval tv;

        gettimeofday(&tv, NULL);
        if (tv.tv_sec != oldTv.tv_sec) {
            global = time(tloc);
            oldTv.tv_sec = tv.tv_sec;
        } else {
            if (tloc)
                *tloc = global;
        }
        return global;
    }
#else
    {
        return time(tloc);
    }
#endif
#endif
}

void EncodeAngleBrackets(char *s, int max) {
    int remain;
    char *p;
    char hex[4];

    /* extra -1 to account for final null */
    remain = max - strlen(s) - 1;

    for (p = strchr(s, 0) - 1; p >= s; p--) {
        if (*p == '<' || *p == '>') {
            /* if no room remains to fix up, take safer path and eliminate */
            if (remain < 2) {
                StrCpyOverlap(p, p + 1);
                remain++;
            } else {
                /* although strlen(p) includes the angle bracket, use it to get a +1 effect to copy
                 * null */
                memmove(p + 3, p + 1, strlen(p));
                sprintf(hex, "%%%02x", (unsigned int)(*p & 0xff));
                memmove(p, hex, 3);
                remain -= 2;
            }
        }
    }
}

void StripScripts(char *url) {
    if (url == NULL)
        return;

    while (*url) {
        if (*url == '<' || *url == '>')
            StrCpyOverlap(url, url + 1);
        else
            url++;
    }
}

int GetChargeIndexFromName(char *name) {
    int i;

    for (i = 0; i <= GCLAST; i++) {
        if (stricmp(guardianChargeName[i], name) == 0) {
            return i;
        }
    }

    return -1;
}

void ChargeAlive(int i, time_t *now) {
    time_t now2;
    static time_t cache[GCLAST + 1];

    now2 = UUtime(now);
    /* try not to hit IPC memory except when something changes */
    if (cache[i] != now2) {
        if (i == guardianChargeForceSeizureIndex) {
            if (--guardianChargeForceSeizureDelay > 0) {
                Log("%s will be forced to seize for testing in %d second%s", guardianChargeName[i],
                    guardianChargeForceSeizureDelay, SIfNotOne(guardianChargeForceSeizureDelay));
            } else {
                Log("%s is being forced to seize for testing", guardianChargeName[i]);
                Log("Guardian should force restart in %d seconds",
                    guardianChargeMaximumLatency[i] / LATENCY_FACTOR);
                for (;;) {
                    SubSleep(1, 0);
                }
            }
        }
        guardian->chargesCheckin[i] = GetTickCount();
        guardian->chargesRunning[i] = TRUE;
        cache[i] = now2;
    }
}

void ChargeStopped(int i, time_t *now) {
    ChargeAlive(i, now);
    guardian->chargesRunning[i] = FALSE;
}

// It's crucial to compute in this manner to handle tick wrap around correctly
DWORD GetTickDiff(DWORD endTick, DWORD startTick) {
    DWORD diff = endTick - startTick;
    return diff;
}

DWORD ChargeLatency(int i, DWORD endTick) {
    return GetTickDiff(endTick, guardian->chargesCheckin[i]);
}

BOOL ChargeHasSeized(int i, DWORD endTick) {
    return guardian->chargesRunning[i] &&
           (ChargeLatency(i, endTick) > guardianChargeMaximumLatency[i]) &&
           (guardianChargeMaximumLatency[i] != 0);
}

int CheckCharge(void) {
    int seized = -1;
    int i;
    DWORD endTick;

    if (guardian->chargeStop)
        return -1;

    if (guardian->chargeStatus != chargeRunning)
        return -1;

    endTick = GetTickCount();

    for (i = 0; i <= GCLAST; i++) {
        if (ChargeHasSeized(i, endTick)) {
            seized = i;
        }
    }

    return seized;
}

void SeizeMe(void) {
#ifndef WIN32
    struct sigaction act, oact;
#endif
    printf("execution is being seized\n");
    fflush(stdout);
#ifndef WIN32
    act.sa_flags = 0;
    act.sa_handler = SIG_IGN;
    sigaction(SIGTERM, &act, &oact);
#endif
    for (;;) {
        SubSleep(120, 0);
    }
}

void CrashMe(void) {
#ifndef WIN32
    struct sigaction act, oact;
#endif
    printf("simulating an illegal instruction\n");
    fflush(stdout);
#ifndef WIN32
    act.sa_flags = 0;
    act.sa_handler = SIG_IGN;
    sigaction(SIGTERM, &act, &oact);
#endif
    raise(SIGILL);
}

void FindReplace(char *s, size_t maxLen, char *find, char *replace, int flags) {
    size_t flen, rlen;
    int diff;
    char *found, *right;

    flen = strlen(find);
    rlen = strlen(replace);

    diff = rlen - flen;

    for (found = s;;) {
        if (flags & FINDREPLACE_IGNORECASE)
            found = StrIStr(found, find);
        else
            found = strstr(found, find);
        if (found == NULL)
            return;

        /* If there's not enough room for change, don't do it */

        if (strlen(s) + diff >= maxLen)
            return;

        right = found + flen;
        if (diff)
            memmove(right + diff, right, strlen(right) + 1); /* don't forget the null */

        memmove(found, replace, rlen);

        if ((flags & FINDREPLACE_GLOBAL) == 0)
            return;

        found += rlen;
    }
}

BOOL ProcessAlive(int pid) {
#ifdef WIN32
    HANDLE h;

    if (pid == 0)
        return FALSE;

    h = OpenProcess(SYNCHRONIZE, 0, pid);

    if (h == NULL) {
        if (GetLastError() == ERROR_ACCESS_DENIED)
            return TRUE;
        return FALSE;
    }
    CloseHandle(h);
    return TRUE;
#else
    int status;

    if (pid == 0)
        return FALSE;

    status = kill(pid, 0);
    if (status == 0 || (status < 0 && errno == EPERM))
        return TRUE;

    return FALSE;
#endif /* #ifdef WIN32 */
}

int ProcessKill(int pid) {
#ifdef WIN32
    HANDLE h;

    if (pid == 0)
        return 0;

    h = OpenProcess(PROCESS_TERMINATE, 0, pid);
    if (h) {
        TerminateProcess(h, 1);
        CloseHandle(h);
    }

    return 0;
#else
    if (pid == 0)
        return 0;

    if (kill(pid, SIGKILL) == 0 || errno == ESRCH)
        return 0;

    return errno;
#endif
}

void DirName(char *s) {
    char *slash;

    if (s == NULL)
        return;

    for (slash = strchr(s, 0) - 1;; slash--) {
        if (slash < s) {
            if (*s)
                strcpy(s, ".");
            break;
        }
        if (*slash == '/' || *slash == '\\') {
            if (slash - 2 == s && *(slash - 1) == ':')
                *(slash + 1) = 0;
            else
                *slash = 0;
            break;
        }
    }
}

void ResetGuardian(BOOL real) {
    if (guardian == NULL) {
        if (real)
            return;
        if (!(guardian = malloc(sizeof(struct GUARDIAN))))
            PANIC;
    }

    memset(guardian, 0, sizeof(*guardian));
    StrCpy3(guardian->version, GUARDIANVERSION, sizeof(guardian->version));
    guardian->chargeStatus = chargeBeingStarted;
    if (real)
        guardian->gpid = getpid();
    else
        guardian->cpid = getpid();
    guardian->chargeStop = 0;
    guardian->killSession = 0;
}

struct PROTOCOLDETAIL *ParseProtocol(char *url, char **endOfProtocol) {
    struct PROTOCOLDETAIL *p;

    for (p = protocolDetails; p->name; p++) {
        if (strnicmp(url, p->name, p->nameLen) == 0) {
            if (endOfProtocol)
                *endOfProtocol = url + p->nameLen;
            return p;
        }
    }

    if (endOfProtocol)
        *endOfProtocol = url;

    return NULL;
}

/**
 * This should probably go into the generic uustring.c file.  Anyway, if I
 * find myself needing this elsewhere I will create a generic trim function.
 *
 * Trim leading and trailing characters from host given the charList.  This
 * potentially shorter string is copied into the result.
 *
 * Assumes host is not null and is null terminated.  Uses strcpy to initialize
 * result.
 *
 * If host is NULL, this will atleast return the empty string.
 *
 * @return A pointer to result.
 */
char *TrimHost(char *host, const char *charList, char *result) {
    char *start, *end;

    // handle empty string and null
    if (!host || host[0] == '\0') {
        if (result)
            result[0] = '\0';
        return result;
    }

    StrCpyOverlap(result, host);

    if (!charList || charList[0] == '\0')
        return result;

    int len = strlen(result);
    start = result;
    end = result + len - 1;

    while (strchr(charList, *start)) {
        start++;
        len--;
    }

    while (strchr(charList, *end)) {
        end--;
        len--;
    }

    memmove(result, start, len);
    result[len] = '\0';

    return result;
}

void ParseHost(char *host, char **colon, char **endOfHostPort, BOOL allowAsterisk) {
    BOOL firstColon = TRUE;
    BOOL bracket = FALSE;
    char *saveHost = host;

    if (colon)
        *colon = NULL;

    for (; *host; host++) {
        // check for ipv6 notation, nested brackets would do weird things
        if (*host == '[') {
            bracket = TRUE;
            continue;
        }
        if (*host == ']' && bracket) {
            bracket = FALSE;
            continue;
        }
        if (bracket)
            continue;

        // If @ is present, invalidate the entire parse by resetting back to
        // the start of the host string, changing to be functionally similar to https:///
        if (*host == '@') {
            if (colon) {
                *colon = NULL;
            }
            host = saveHost;
            break;
        }

        if (*host == ':') {
            if (firstColon) {
                firstColon = FALSE;
                if (colon)
                    *colon = host;
            }
            continue;
        }
        if (IsWS(*host))
            continue;
        if (IsAlpha(*host) || IsDigit(*host) || strchr(".-_", *host))
            continue;
        if (allowAsterisk && *host == '*')
            continue;
        break;
    }
    if (endOfHostPort)
        *endOfHostPort = host;
}

BOOL ParseProtocolHostPort(char *url,
                           char *defaultProtocol,
                           struct PROTOCOLDETAIL **protocol,
                           char **host,
                           char **endOfHost,
                           char **colon,
                           PORT *port,
                           BOOL *isDefaultPort,
                           char **endOfHostPort) {
    struct PROTOCOLDETAIL *localProtocol;
    char *localHost, *localColon, *localEndOfHostPort;
    PORT localPort, defaultPort;

    if (url == NULL) {
        if (protocol)
            *protocol = NULL;
        if (host)
            *host = NULL;
        if (endOfHost)
            *endOfHost = NULL;
        if (colon)
            *colon = NULL;
        if (port)
            *port = 0;
        if (isDefaultPort)
            *isDefaultPort = 0;
        if (endOfHostPort)
            *endOfHostPort = NULL;
        return 0;
    }

    /* If no protocol found, localHost will point to start of url, which will
       stick even if we go to a defaultProtocol
    */
    localProtocol = ParseProtocol(url, &localHost);
    if (localProtocol == NULL && defaultProtocol) {
        localProtocol = ParseProtocol(defaultProtocol, NULL);
    }

    if (protocol)
        *protocol = localProtocol;

    if (host)
        *host = localHost;

    ParseHost(localHost, &localColon, &localEndOfHostPort, 0);

    if (endOfHost)
        *endOfHost = localColon ? localColon : localEndOfHostPort;

    if (colon)
        *colon = localColon;

    defaultPort = localProtocol ? localProtocol->defaultPort : 0;

    localPort = localColon ? atoi(localColon + 1) : defaultPort;

    if (port)
        *port = localPort;

    if (isDefaultPort)
        *isDefaultPort = localPort == defaultPort;

    if (endOfHostPort)
        *endOfHostPort = localEndOfHostPort;

    // localHost == localEndOfHostPort if @ present or field is omitted,
    // both of which should be treated equivalently
    return localHost != NULL && localHost != localEndOfHostPort && localPort != 0;
}

void CheckProxyByHostnameWildcardDNS(void) {
    char *testName = NULL;
    struct sockaddr_storage sa;
    int i;
    char *p;

    if (optionProxyType == PTPORT)
        return;

    if (!(testName = malloc(strlen(myName) + 64)))
        PANIC;

    p = testName;
    *p++ = 'r';

    for (i = 0; i < 7; i++)
        *p++ = RandChar();

    /* When using our own DNS server, making the check something that can come back valid */
    if (dnsPorts) {
        strcpy(testName, "login");
        p = strchr(testName, 0);
    }

    *p++ = '.';
    strcpy(p, myName);

    if (UUGetHostByName(testName, &sa,
                        (PORT)(loginPorts && loginPorts->port ? loginPorts->port : 80), 0)) {
        Log("Warning: wildcard DNS entry *.%s not detected by EZproxy; proxy by hostname may fail",
            myName);
    }
}

#define TAILSEEKBLOCK 1024

BOOL TailSeek(int file, int lines) {
    off_t where;
    char buffer[TAILSEEKBLOCK];
    off_t len;
    char *p;
    BOOL lastNewline = 0;

    where = lseek(file, 0, SEEK_END);
    if (where == -1)
        return 0;

    lines++;

    for (;;) {
        if (where == 0) {
            where = lseek(file, where, SEEK_SET);
            return where != -1;
        }

        len = (where > TAILSEEKBLOCK ? TAILSEEKBLOCK : where);
        where -= len;
        where = lseek(file, where, SEEK_SET);
        if (where == -1)
            return 0;
        if (UUreadFD(file, buffer, (size_t)len) != len)
            return 0;
        for (p = buffer + len - 1; lines > 0 && p >= buffer; p--) {
            if (*p == 10) {
                lines--;
                lastNewline = 1;
            } else {
                if (*p == 13 && lastNewline == 0)
                    lines--;
                lastNewline = 0;
            }
        }
        if (lines <= 0) {
            where = lseek(file, where + (p - buffer), SEEK_SET);
            return where != -1;
        }
    }
}

char *LogSPUActiveFilename(struct LOGSPU *logSPU, BOOL haveMutex) {
    char tempFilename[MAX_PATH];
    time_t now;
    struct tm tm;

    if (guardian)
        guardian->mainLocation = 3000;

    if (logSPU == NULL) {
        if (logSPUs == NULL)
            return NULL;
        logSPU = logSPUs;
    }

    if (guardian)
        guardian->mainLocation = 3001;

    if (haveMutex == 0)
        UUAcquireMutex(&mAccessFile);

    if (guardian)
        guardian->mainLocation = 3002;

    if (logSPU->strftimeFilename == 0) {
        if (guardian)
            guardian->mainLocation = 3003;
        if (logSPU->activeFilename[0] == 0) {
            if (guardian)
                guardian->mainLocation = 3004;
            StrCpy3(logSPU->activeFilename, logSPU->filename, sizeof(logSPU->activeFilename));
            logSPU->activeFilenameChanged = 1;
        }
    } else {
        if (guardian)
            guardian->mainLocation = 3005;
        UUtime(&now);
        if (guardian)
            guardian->mainLocation = 3006;
        UUlocaltime_r(&now, &tm);
        if (guardian)
            guardian->mainLocation = 3007;
        tempFilename[sizeof(tempFilename) - 1] = 0;
        strftime(tempFilename, sizeof(tempFilename) - 1, logSPU->filename, &tm);
        if (guardian)
            guardian->mainLocation = 3008;
        if (strcmp(logSPU->activeFilename, tempFilename) != 0) {
            if (guardian)
                guardian->mainLocation = 3009;
            strcpy(logSPU->activeFilename, tempFilename);
            logSPU->activeFilenameChanged = 1;
        }
    }

    if (guardian)
        guardian->mainLocation = 3010;

    if (haveMutex == 0)
        UUReleaseMutex(&mAccessFile);

    return logSPU->activeFilename;
}

int UUInitMutexBase(UUMUTEX *uumutex, const char *name) {
    struct TRACKMUTEX *tm;

    memset(uumutex, 0, sizeof(*uumutex));
    if (!(uumutex->name = strdup(name)))
        PANIC;
    uumutex->logDuplicateAttempts = 1;

    if (!(tm = (struct TRACKMUTEX *)calloc(1, sizeof(*tm))))
        PANIC;
    tm->mutex = uumutex;
    tm->next = trackMutexes;
    trackMutexes = tm;

    return UUInitMutexOS(uumutex);
}

BOOL UUDeadlockOverride(UUTHREADID threadid) {
    if (deadlocked) {
        if (deadlockReporter == threadid)
            return 1;
        for (;;) {
            SubSleep(60, 0);
        }
    }
    return 0;
}

int UUAcquireMutexBase(UUMUTEX *uumutex, const char *file, int line) {
    int result;
    UUTHREADID threadid = UUThreadId();

    /* If the holder of uumutex isn't our thread, the threadid may be in flux if
       other threads are acquiring or releasing, but it won't be our thread, so
       the equality test will always fail if it isn't our thread and always
       succeed if it is our thread
    */

    /* If the same thread asks twice, lie since the thread already has the mutex
       but also note it as well */

    if (UUDeadlockOverride(threadid))
        return 0;

    if (uumutex->threadid == threadid) {
        if (uumutex->logDuplicateAttempts) {
            Log("UUAcquireMutex %s from %s:%d and %s:%d", uumutex->name, uumutex->file,
                uumutex->line, file, line);
        }
        uumutex->multi = 1;
        result = 0;
    } else {
        result = UUAcquireMutexOS(uumutex, file, line);
        uumutex->threadid = threadid;
        uumutex->file = file;
        uumutex->line = line;
    }
    uumutex->count++;
    return result;
}

int UUTryAcquireMutexBase(UUMUTEX *uumutex, const char *file, int line) {
    int result;
    UUTHREADID threadid = UUThreadId();

    /* See comments in UUAcquireMutexBase */

    if (UUDeadlockOverride(threadid))
        return 0;

    if (uumutex->threadid == threadid) {
        if (uumutex->logDuplicateAttempts) {
            Log("UUTryAcquireMutex %s from %s:%d and %s:%d", uumutex->name, uumutex->file,
                uumutex->line, file, line);
        }
        uumutex->multi = 1;
        result = 1;
    } else {
        result = UUTryAcquireMutexOS(uumutex);
        if (result == 0)
            return 0;
        uumutex->threadid = threadid;
        uumutex->file = file;
        uumutex->line = line;
    }
    uumutex->count++;
    return result;
}

int UUReleaseMutexBase(UUMUTEX *uumutex, const char *file, int line) {
    int result = 0;

    if (UUDeadlockOverride(UUThreadId()))
        return result;

    if (uumutex->count == 0) {
        if (uumutex->logDuplicateAttempts) {
            Log("UUReleaseMutex %s not held from %s:%d", uumutex->name, file, line);
        }
    } else {
        if (uumutex->multi && uumutex->logDuplicateAttempts) {
            Log("UUReleaseMutex %s from %s:%d (%d)", uumutex->name, file, line, uumutex->count);
        }
        uumutex->count--;
        if (uumutex->count > 0) {
            result = 0;
        } else {
            uumutex->threadid = 0;
            uumutex->file = NULL;
            uumutex->line = 0;
            uumutex->multi = 0;
            result = UUReleaseMutexOS(uumutex);
        }
    }

    return result;
}

void UUDeadlockMutex(UUMUTEX *uumutex, char *file, int line) {
    struct TRACKMUTEX *tm;

    deadlockReporter = UUThreadId();
    deadlocked = 1;

    Log("Deadlock trying to acquire %s from %s:%d", uumutex->name, file, line);
    for (tm = trackMutexes; tm; tm = tm->next) {
        if (tm->mutex->count == 0)
            continue;
        Log("%s held by %s:%d", tm->mutex->name, tm->mutex->file, tm->mutex->line);
    }
    exit(1);
}

struct sockaddr_storage *DatabaseOrSessionIpInterface(struct TRANSLATE *t, struct DATABASE *b) {
    struct SESSION *e;

    if (t == NULL || (e = t->session) == NULL) {
        if (b == NULL)
            return NULL;
        return &b->ipInterface;
    }

    /* We are guaranteed to have a session from here  */

    if (b == NULL && t->host)
        b = t->host->myDb;

    if (b == NULL)
        return (is_inet(&e->ipInterface) ? &e->ipInterface : NULL);

    /* We are guaranteed to have a session and a database from here */

    // the sin_addr has not been set yet, but the session looks like it has
    if (is_unspecified(&b->ipInterface) && is_inet(&e->ipInterface))
        return &e->ipInterface;

    return &b->ipInterface;
}

#define SHA512SALTLEN 8

char *SHA512String(char *input, char *salt) {
    char *newSalt = NULL;
    char *output = NULL;
    unsigned char binaryOutput[EVP_MAX_MD_SIZE];
    char *p;
    int i;
    EVP_MD_CTX *mdctx = NULL;
    unsigned int mdLen;
    char *dollar;
    size_t saltLen;

    if (input == NULL)
        return NULL;

    if (salt == NULL) {
        if (!(newSalt = malloc(SHA512SALTLEN + 1)))
            PANIC;
        for (p = newSalt, i = 0; i < SHA512SALTLEN; i++) {
            *p++ = RandChar();
        }
        *p++ = 0;
        salt = newSalt;
    } else if (*salt == '$')
        salt++;

    if (dollar = strchr(salt, '$'))
        saltLen = dollar - salt;
    else
        saltLen = strlen(salt);

    if (!(mdctx = EVP_MD_CTX_new()))
        PANIC;
    EVP_DigestInit(mdctx, EVP_sha512());

    if (saltLen)
        EVP_DigestUpdate(mdctx, salt, saltLen);
    if (*input)
        EVP_DigestUpdate(mdctx, input, strlen(input));
    EVP_DigestFinal(mdctx, binaryOutput, &mdLen);

    if (!(output = malloc(saltLen + mdLen * 2 + 5)))
        PANIC;

    p = output;
    *p++ = '$';
    if (saltLen) {
        memcpy(p, salt, saltLen);
        p += saltLen;
    }
    *p++ = '$';
    Encode64Binary(p, binaryOutput, mdLen);

    for (p = strchr(output, 0) - 1; p >= output; p--) {
        if (*p == '=')
            StrCpyOverlap(p, p + 1);
    }

    if (mdctx) {
        EVP_MD_CTX_free(mdctx);
        mdctx = NULL;
    }
    FreeThenNull(newSalt);

    return output;
}

char *PetersonsMangleString(char *mangledOut, char *unmangledIn, char *key) {
    char *op;
    unsigned char *ip, *kp;

    if (key == NULL || *key == 0 || unmangledIn == NULL)
        return NULL;

    if (mangledOut == NULL) {
        if (!(mangledOut = malloc(strlen(unmangledIn) * 2 + 1)))
            PANIC;
    }

    for (op = mangledOut, ip = (unsigned char *)unmangledIn, kp = (unsigned char *)key; *ip; ip++) {
        sprintf(op, "%02x", (*ip + *kp) % 127);
        op = AtEnd(op);
        kp++;
        if (*kp == 0)
            kp = (unsigned char *)key;
    }
    return mangledOut;
}

char *PetersonsUnmangleString(char *unmangledOut, char *mangledIn, char *key) {
    char *ip;
    unsigned char *op, *kp;
    unsigned int code1;
    int code;

    if (key == NULL || *key == 0 || mangledIn == NULL)
        return NULL;

    if (unmangledOut == NULL) {
        if (!(unmangledOut = malloc(strlen(mangledIn) + 1)))
            PANIC;
    }

    for (op = (unsigned char *)unmangledOut, ip = mangledIn, kp = (unsigned char *)key; *ip;) {
        sscanf(ip, "%02x", &code1);
        code = (int)(code1) - (int)(*kp);
        if (code < 0)
            code += 127;
        *op++ = code;

        ip++;
        if (*ip == 0)
            break;
        ip++;

        kp++;
        if (*kp == 0)
            kp = (unsigned char *)key;
    }
    *op = 0;

    return unmangledOut;
}

/*
void PetersonsTest(void)
{
    char *key = "auj67yu6te245!@knpyo";
    char *test = PetersonsMangleString(NULL, "<data><student-id>234569</student-id><full-name>Tom
Weiss</full-name><email>tom.weiss@petersons.com</email><password>i8urfood4lunch</password><sponsor-id>10133</sponsor-id><product-id></product-id></data>",
        key);
    char *test2 = PetersonsUnmangleString(NULL, test, key);
    printf("%s\n", test);
    printf("%s\n", test2);
}
*/

void EncodeFieldsToBuffer(char *buffer,
                          char *bufferEnd,
                          char *template,
                          struct TRANSLATE *t,
                          struct USERINFO *uip) {
    char *p, *q;
    char ipBuffer[INET6_ADDRSTRLEN];

    for (p = template, q = buffer; *p; p++) {
        if (*p != '^') {
            *q++ = *p;
            continue;
        }

        p++;
        if (*p == 0)
            break;

        switch (*p) {
        case '^':
            *q++ = '^';
            break;

        case 'u':
        case 'U':
            *q = 0;
            AddEncodedField(q, uip->user, NULL);
            q = strchr(q, 0);
            break;

        case 'p':
        case 'P':
            *q = 0;
            AddEncodedField(q, uip->pass, NULL);
            q = strchr(q, 0);
            break;

        case 'a':
        case 'A':
            *q = 0;
            AddEncodedField(q, uip->auth, NULL);
            q = strchr(q, 0);
            break;

        case 'i':
        case 'I':
            *q = 0;
            ipToStr(&t->sin_addr, ipBuffer, sizeof ipBuffer);
            AddEncodedField(q, ipBuffer, NULL);
            q = strchr(q, 0);
            break;
        }
    }

    *q = 0;
}

char *ParseRegularExpression(const char *pattern,
                             pcre **findPattern,
                             pcre_extra **findPatternExtra,
                             unsigned int *optionsMask,
                             char **findReturn,
                             char **replaceReturn) {
    char *error = NULL;
    char delim;
    char *p;
    const char *pcreError; /* pcreError will point to a static string, so don't try to free it! */
    int pcreErrorOffset;
    int reOptions = 0;
    int phase = 0;
    char *find = NULL;
    char *replace = NULL;

    if (findReturn)
        *findReturn = NULL;

    if (replaceReturn)
        *replaceReturn = NULL;

    if (optionsMask)
        *optionsMask = 0;

    *findPattern = NULL;

    if (findPatternExtra)
        *findPatternExtra = NULL;

    delim = *pattern++;

    if (delim < '!' || delim > 126 || IsAlpha(delim) || IsDigit(delim)) {
        if (replaceReturn) {
            if (!(error = strdup("Regular expression must start with a non-alpha, non-digit "
                                 "character to delimit the find, replace, and option strings")))
                PANIC;
        } else {
            if (!(error = strdup("Regular expression must start with a non-alpha, non-digit "
                                 "character to delimit the find and option strings")))
                PANIC;
        }
        goto Finished;
    }

    if (!(find = strdup(pattern)))
        PANIC;

    for (p = find; *p; p++) {
        if (phase == 2) {
            if (optionsMask && *p >= 'a' && *p <= 'z')
                *optionsMask |= 1 << (*p - 'a');
            if (*p == 'i')
                reOptions |= PCRE_CASELESS;
        }

        if (*p == '\\' && *(p + 1) == delim)
            StrCpyOverlap(p, p + 1);
        else if (*p == delim) {
            *p = 0;
            phase++;
            if (phase == 1) {
                if (replaceReturn == NULL)
                    phase++;
                else
                    replace = p;
            }
        }
    }

    /* At this point, if replace != NULL, then it points at part of find;
       for an error, that means we should NULL out replace to prevent attempts
       to free it during cleanup
    */

    if (phase < 2) {
        replace = NULL;
        if (!(error = malloc(64 + strlen(find))))
            PANIC;
        sprintf(error, "Invalid regular expression: %s", find);
        goto Finished;
    }

    if (replace) {
        if (!(replace = strdup(replace)))
            PANIC;
    }

    /* The realloc of find must come after the strdup of replace, or
       replace won't point to the right location any longer
    */
    find = UUStrRealloc(find);

    /* pcreError will point to a static string, so don't try to free it! */
    *findPattern = pcre_compile(find, reOptions, &pcreError, &pcreErrorOffset, NULL);
    if (*findPattern == NULL) {
        if (!(error = malloc(64 + strlen(pcreError) + strlen(find))))
            PANIC;
        sprintf(error, "Find pattern error %s at %d: %s", pcreError, pcreErrorOffset, find);
        error = UUStrRealloc(error);
        goto Finished;
    }

    if (findPatternExtra) {
        *findPatternExtra = pcre_study(*findPattern, 0, &pcreError);
        if (pcreError) {
            if (!(error = malloc(64 + strlen(pcreError) + strlen(find))))
                PANIC;
            sprintf(error, "Find pattern error %s at %d: %s", pcreError, pcreErrorOffset, find);
            error = UUStrRealloc(error);
            pcre_free(*findPattern);
            *findPattern = NULL;
            goto Finished;
        }
    }

Finished:
    if (error) {
        FreeThenNull(find);
        FreeThenNull(replace);
    } else {
        if (findReturn)
            *findReturn = find;
        else
            FreeThenNull(find);

        if (replaceReturn)
            *replaceReturn = replace;
        else
            FreeThenNull(replace);
    }

    return error;
}

char *ParseXMLRegularExpression(const char *pattern, xmlRegexpPtr *findPattern) {
    char *error = NULL;

    *findPattern = xmlRegexpCompile(BAD_CAST pattern);
    if (*findPattern == NULL) {
        if (!(error = malloc(64 + strlen(pattern))))
            PANIC;
        sprintf(error, "Find pattern error: %s", pattern);
    }

    return error;
}

void StrCpyOverlap(char *dst, char *src) {
    /* During benchmarking, the memmove/strlen version is
       six times faster than the loop version
    */

    /* memmove is buffer overlap safe;
       the + 1 allows for copying the final null
    */
    memmove(dst, src, strlen(src) + 1);

    /*
    while (*src) {
        *dst++ = *src++;
    }
    *dst = 0;
    */
}

int RenameWithReplace(const char *oldName, const char *newName) {
#ifdef WIN32
    // Unlike POSIX, Windows won't replace an existing file with rename so use
    // the closest equivalent of MoveFileEx called with MOVEFILE_REPLACE_EXISTING.
    // Remap reponse into rename semantics of 0 for success, -1 for error.
    if (MoveFileEx(oldName, newName, MOVEFILE_REPLACE_EXISTING)) {
        return 0;
    } else {
        SetErrno(GetLastError());
        return -1;
    }
#else
    // POSIX guarantees that the rename will replace the existing file
    return rename(oldName, newName);
#endif
}

size_t CurlMemoryBufferCallback(void *contents, size_t size, size_t nmemb, void *userp) {
    size_t realsize = size * nmemb;
    struct CURLMEMORYBUFFER *mem = (struct CURLMEMORYBUFFER *)userp;

    if (!(mem->memory = realloc(mem->memory, mem->size + realsize + 1)))
        PANIC;
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

void ScanDirectory(char *base,
                   int searchFlags,
                   BOOL (*callback)(char *base, char *file, int flags, void *context),
                   void *context) {
    struct node {
        struct node *next;
        char *path;
        int flags;
    } *entries = NULL, *p, *n, *last;
    char filename[MAX_PATH];
    int flags;
    BOOL processing = 1;
    int match;

#ifdef WIN32
    WIN32_FIND_DATA fileData;
    HANDLE hSearch;

    sprintf(filename, "%s%s*.*", base, DIRSEP);
    hSearch = FindFirstFile(filename, &fileData);
    if (hSearch != INVALID_HANDLE_VALUE) {
        do {
            if (strcmp(fileData.cFileName, ".") != 0 && strcmp(fileData.cFileName, "..") != 0) {
                flags = ((fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
                            ? SCAN_DIRECTORY_FILE
                            : SCAN_DIRECTORY_DIR;
                if (searchFlags & flags) {
                    if (!(n = calloc(1, sizeof(*n))))
                        PANIC;
                    if (!(n->path = strdup(fileData.cFileName)))
                        PANIC;
                    n->flags = flags;
#else
    DIR *dir;
    struct dirent *dirent;
    struct stat statBuf;

    dir = opendir(base);
    if (dir == NULL)
        return;
    while (dirent = readdir(dir)) {
        if (strcmp(dirent->d_name, ".") != 0 && strcmp(dirent->d_name, "..") != 0) {
            sprintf(filename, "%s%s%s", base, DIRSEP, dirent->d_name);
            if (stat(filename, &statBuf) != 0)
                continue;
            flags = S_ISREG(statBuf.st_mode) ? SCAN_DIRECTORY_FILE : SCAN_DIRECTORY_DIR;
            if (searchFlags & flags) {
                if (!(n = calloc(1, sizeof(*n))))
                    PANIC;
                if (!(n->path = strdup(dirent->d_name)))
                    PANIC;
                n->flags = flags;
#endif

                    for (last = NULL, p = entries; p; last = p, p = p->next) {
                        match = stricmp(p->path, n->path);
                        if (searchFlags & SCAN_DIRECTORY_DESC) {
                            match = -match;
                        }
                        if (match >= 0) {
                            break;
                        }
                    }
                    n->next = p;
                    if (last) {
                        last->next = n;
                    } else {
                        entries = n;
                    }

#ifdef WIN32
                }
            }
        } while (FindNextFile(hSearch, &fileData));
        FindClose(hSearch);
    }
#else
            }
        }
    }
    closedir(dir);
#endif

    for (p = entries; p != NULL;) {
        if (processing) {
            processing = (*callback)(base, p->path, p->flags, context);
        }
        last = p;
        p = p->next;
        FreeThenNull(last->path);
        FreeThenNull(last);
    }
}

/**
 * Variant of malloc that automatically PANICs on failure.
 */
void *UUmalloc(size_t size) {
    void *v;
    if (!(v = malloc(size))) {
        PANIC;
    }
    return v;
}

/**
 * Variant of calloc that automatically PANICs on failure.
 */
void *UUcalloc(size_t nmemb, size_t size) {
    void *v;
    if (!(v = calloc(nmemb, size))) {
        PANIC;
    }
    return v;
}

/**
 * Variant of realloc that automatically PANICs on failure.
 */
void *UUrealloc(void *ptr, size_t size) {
    void *v;
    if (!(v = realloc(ptr, size))) {
        PANIC;
    }
    return v;
}
