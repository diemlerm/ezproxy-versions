#define __UUFILE__ "efrereplace.c"

#define EXPRESSION_INTERNAL_FUNCTIONS

#include "common.h"
#include "usr.h"
#include "usrfinduser.h"
#include "expression.h"
#include "variables.h"
#include "efrereplace.h"

char *ExpressionFuncREReplace(struct EXPRESSIONCTX *ec) {
    char *find = NULL;
    char *replace = NULL;
    char *val = NULL;
    pcre *findPattern = NULL;
    pcre_extra *findPatternExtra = NULL;
    int currLen;
    int ovec[33]; /* 11 slots * 3 requires entries per slot */
    int nmat;
    int offset;
    char *curr = NULL;
    char *p;
    char *pe;
    char *next = NULL;
    char *f;
    BOOL global = FALSE;
    char *retrace;
    char *name;
    size_t nameLen = 0;
    char var[256];
    size_t varLen;
    char *value;
    int valueLen;
    int max;
    char *buffer1 = NULL, *buffer2 = NULL;
    size_t buffer1Len = 0, buffer2Len = 0;
    char *varValue = NULL;
    char *error = NULL;
    unsigned int optionsMask;

    find = ExpressionExpression(ec, TRUE);

    if (!ExpressionCheckToken(ec, COMMA))
        goto Finished;

    replace = ExpressionExpression(ec, TRUE);

    if (!ExpressionCheckToken(ec, COMMA))
        goto Finished;

    val = ExpressionExpression(ec, TRUE);

    if (error = ParseRegularExpression(find, &findPattern, &findPatternExtra, &optionsMask, NULL,
                                       NULL)) {
        UsrLog(ec->uip, "REReplace %s", error);
        FreeThenNull(error);
        goto Finished;
    }

    global = (optionsMask & 1 << ('g' - 'a')) != 0;

    curr = val;

    offset = 0;
    do {
        currLen = strlen(curr);
        nmat = pcre_exec(findPattern, findPatternExtra, curr, currLen, offset, 0, ovec,
                         sizeof(ovec) / sizeof(int));
        if (nmat <= 0) {
            break;
        }

        if (buffer1 == NULL) {
            buffer1Len = currLen + 4096;
            if (!(buffer1 = malloc(buffer1Len)))
                PANIC;
            next = buffer1;
        } else if (buffer2 == NULL) {
            buffer2Len = currLen + 4096;
            if (!(buffer2 = malloc(buffer2Len)))
                PANIC;
            next = buffer2;
        } else
            next = (curr == buffer1 ? buffer2 : buffer1);

        p = next;
        if (p == buffer1)
            pe = next + buffer1Len - 1; /* leave room for final null */
        else
            pe = next + buffer2Len - 2; /* leave room for final null */

        /* Copy everything that appears before the match */
        if (ovec[0] > 0) {
            memcpy(p, curr, ovec[0]);
            p += ovec[0];
        }

        for (f = replace; *f && p < pe;) {
            if (*f != '$') {
                if (*f == '\\') {
                    if (*f == '$') {
                        *p++ = '$'; /* single character insert has room */
                        f += 2;
                        continue;
                    }
                }
                *p++ = *f++; /* single character insert has room */
                continue;
            }

            retrace = f;
            name = NULL;

            f++;

            if (*f >= '0' && *f <= '9') {
                name = f;
                nameLen = 1;
                f++;
            } else if (*f == '{') {
                char *closeBrace = strchr(f, '}');
                if (closeBrace) {
                    name = f + 1;
                    nameLen = closeBrace - f - 1;
                    f = closeBrace + 1;
                }
            }
            /* If we didn't find a variable, just move a $ and retrace our steps */
            if (name == NULL || nameLen == 0) {
                *p++ = '$'; /* single character insert has room */
                f = retrace + 1;
                continue;
            }

            varLen = UUMIN(nameLen, sizeof(var) - 1);
            memcpy(var, name, varLen);
            var[varLen] = 0;

            value = NULL;
            valueLen = 0;

            if (strlen(var) == 1 && IsDigit(*name)) {
                int i = *name - '0';
                /* With one capture paren, nmat will be 2 (1st is always whole string match, which
                 * we provide by $0) */
                if (i < nmat) {
                    i = i * 2;
                    if (ovec[i] >= 0) {
                        value = curr + ovec[i];
                        valueLen = ovec[i + 1] - ovec[i];
                    }
                }
            } else {
                value = varValue = ExpressionVariable(ec, var, NULL, TRUE);
                valueLen = strlen(value);
            }

            if (value) {
                max = pe - p;

                if (value && valueLen) {
                    if (valueLen > max)
                        valueLen = max;
                    memcpy(p, value, valueLen);
                    p += valueLen;
                }
            }

            FreeThenNull(varValue);
        }

        /* Next time around, start matching where we ended in this loop */
        offset = p - next;

        /* And now append whatever was after the match */
        /* Copy what remains after the match */
        if (ovec[1] < currLen && p < pe) {
            max = UUMIN(currLen - ovec[1], pe - p);
            memcpy(p, curr + ovec[1], max);
            p += max;
        }

        *p = 0; /* room was held to insure this null will fit */
        curr = next;
    } while (global);

    if (val != curr) {
        FreeThenNull(val);
        val = curr;
        if (val == buffer1) {
            buffer1 = NULL;
            FreeThenNull(buffer2);
        } else {
            buffer2 = NULL;
            FreeThenNull(buffer1);
        }
        val = UUStrRealloc(val);
    }

Finished:
    if (findPatternExtra) {
        pcre_free(findPatternExtra);
        findPatternExtra = NULL;
    }

    if (findPattern) {
        pcre_free(findPattern);
        findPattern = NULL;
    }

    FreeThenNull(find);
    FreeThenNull(replace);
    FreeThenNull(buffer1);
    FreeThenNull(buffer2);
    return val;
}
