#ifndef __USREXTERNAL_H__
#define __USREXTERNAL_H__

#include "common.h"

/* Returns 0 if valid, 1 if not, 3 if unable to connect to host */
enum FINDUSERRESULT FindUserExternal(struct TRANSLATE *t,
                                     struct USERINFO *uip,
                                     char *externalUrl,
                                     char *post,
                                     char *valid,
                                     struct sockaddr_storage *pInterface);

#endif  // __USREXTERNAL_H__
