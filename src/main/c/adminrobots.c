/**
 * /robots.txt URL to send a static response indicating that web
 * crawlers (robots) should not attempt to index an EZproxy server.
 */

#define __UUFILE__ "adminrobots.c"

#include "common.h"
#include "uustring.h"

#include "adminrobots.h"

void AdminRobots(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;

    HTTPCode(t, 200, 0);
    HTTPContentType(t, "text/plain", 1);
    UUSendZ(s, "User-agent: *\nDisallow: /\n");
}
