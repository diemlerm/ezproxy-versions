/*
<table class="confluenceTable"><tbody>
<tr>
<td class="confluenceTd"> <strong>Attribute</strong>: An attribute is an atom of information which
is defined by the intersection of attribute name and attribute value.  Attributes must be considered
in terms of the subject about which they are asserted and the authority who is asserting the atom of
information is true. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Attribute Acceptance Policy (AAP)</strong>: AAP's define the rules
that map attributes and information out of the SAML attribute assertion into simpler forms that are
usable by the application for policy decisions.  These rules may also provide filters on who is
allowed to assert certain information. </td>
</tr>
<tr>
<td class="confluenceTd"> Attribute Authority (AA, deprecated): The AA is the portion of the IdP
responsible for issuing attributes on behalf of an organization.  This term has been deprecated, and
the AA is now the attribute query protocol handler function in the IdP. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Attribute Assertion</strong>: A SAML attribute assertion carries
attribute information about a subject such as a browser user.  In Shibboleth, the attribute
assertion conveys attributes once a context is established from the IdP to the SP. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Attribute Release Policy (ARP)</strong>: ARP's are rulesets
regarding attribute release.  These are combined and matrixed against the requester to compute an
effective ARP.  The effective ARP filters the set of attributes supplied by the directory released
to a given relying party by the IdP. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Attribute Resolver</strong>: The component of the IdP responsible
for retrieving attributes from various data sources or computing advanced attributes and performing
the necessary transformations for SAML transport. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Authentication Assertion</strong>: SAML authentication assertions
carries information regarding the act and results of the authentication of a principal by an
authority.  In Shibboleth, this is used to transport the handle or other form of name identifier to
the SP for authentication or subsequent attribute request. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Bossie</strong>: A completely insecure, free CA graciously
operated by Eric Norman of the University of Wisconsin which is often used for the initial testing
of Shibboleth installations.  Bossie is a <strong><em>THREAT</em></strong> to your infrastructure,
not an enabler. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>eduPerson</strong>: This object class was defined by MACE-Dir to
handle standard educational attributes in a way that would facilitate collaboration.  Shibboleth is
often used to transport eduPerson attributes, but the two are <em>distinct entities</em> and each
can be used individually with full functionality. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Entitlement</strong>: Entitlements form a specialized class of
attributes important enough to call out separately.  They can be used to identify specific group
membership or eligibility to use a given resource.  One method of deploying Shibboleth insulates the
decision-making logic used by the IdP from the SP by expressing entitlements instead of several
individual attributes. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>EPPN (eduPersonPrincipalName)</strong>: This identifier takes the
form <tt>principal@domain</tt> and is the most common form of identity in higher ed.  While it may
resemble an e-mail address, it is not one. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Handle</strong>: A handle is one form of name identifier and is
used in an authentication assertion to establish a referential identifier for attribute query in
classic Shibboleth.  The handle itself is completely opaque and temporary and should never be
directly used for authentication purposes, as it corresponds only to a particular unknown browser
user. </td>
</tr>
<tr>
<td class="confluenceTd"> Handle Service (HS, deprecated): Formerly, the portion of the IdP
responsible for handling single sign-on interoperability and functionality.  Now known as the SSO
Handler. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Federation</strong>: A federation is a collection of organizations
that agree to interoperate under a certain ruleset.  Federations will usually define trusted roots,
authorities, and attributes, along with distribution of metadata representing this information.
Shibboleth treats federations -- representing multiple relying parties -- like single relying
parties.  Federations are not required for the use of Shibboleth but can facilitate exchange
greatly. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Identity Provider (IdP, Origin)</strong>: The Identity Provider is
the authority responsible for generating and asserting authentication, authorization, and identity
information about principals in a security domain. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Lazy Session Establishment</strong>: This special form of session
establishment allows for access of a URL or resource prior to authentication or attribute transport,
preserving the ability for subsequent attribute transport at the application's request.  More
information is available in the introductions of the deployment guide. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Metadata</strong>: Shibboleth relies on metadata to identify and
distribute trusted IdP, SP, and certificate authority information.  Prior to 1.3, this took the form
of <tt>sites.xml</tt> and <tt>trust.xml</tt> ; now only <tt>metadata.xml</tt> , based on the new
SAML 2.0 metadata standards, is used. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Name Identifier</strong>: There are several different name
identifiers, each one representing a different meaning for the Subject field of the SAML
authentication assertion, and often, a different set of flows as well.  Handles are the name
identifier in traditional Shibboleth flows, and actual identities or persistentID's may also be used
either for attribute transport or as standalone sign-on assertions. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Persistent Identifier (eduPersonTargetedID)</strong>: This special
identifier type allows an IdP and SP to preserve a single identifier for one principal across all
current and future transactions involving that principal without revealing or using that principal's
identity.  These identifiers are opaque to SP's and vary per SP, protecting against collaborative
Denial of Privacy attacks, while preserving a 1:1 guaranteed mapping for liability and preference
management purposes. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Principal</strong>: The individual being authenticated and about
whom assertions are being issued.  Note the principal of an assertion is only the subject of the
assertion in cases where identity is directly expressed. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>providerID</strong>: The atom of trust implementation for both the
SP and IdP is the providerID of the corresponding partner in a transaction.  Often this will be
assigned by federations, but at other times individual providers will define their own.  Common
names may be used in addition to providerID's for UI purposes. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Relying Party</strong>: The relying party is defined per-flow and
is always the provider receiving and utilizing information from another entity in a given flow.
Generally, this will be a particular SP. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>SAML</strong>: The Security Assertion Mark-up Language consists of
a set of assertion format definitions and bindings for these assertions to protocols.  Shibboleth
supports several formats, protocols, and versions of SAML. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>Service Provider (SP, Target)</strong>: This entity is authorized
to request attributes about IdP users on behalf of a relying organization. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>SHAR (Shibboleth Attribute Requestor, deprecated)</strong>: This
was the component of the SP responsible for requesting attributes about a browser user with whom a
handle had already been associated, but is now a part of the SP package as a whole. <tt>shibd</tt>
performs this functionality. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>SHIRE (Shibboleth Indexical Reference Establisher,
deprecated)</strong>: The SHIRE functionality is responsible for helping to associate a browser user
with an identifier that the IdP and SP can both refer to.  It is now part of the SP package. </td>
</tr>
<tr>
<td class="confluenceTd"> <strong>URI (Uniform Resource Identifier)</strong>: The URI is a superset
of URN's and URL's, each of which is useful for uniquely naming things.  In Shibboleth, these are
often used to identify attributes, providers, and endpoints. </td>
</tr>
</tbody></table>
*/

#define __UUFILE__ "shib.c"

#include "common.h"

#ifdef WIN32
/* io.h, fcntl.h and share.h needed for _sopen in Win32 */
#include <io.h>
#include <fcntl.h>
#include <share.h>
#include <lm.h>
#endif

#include <sys/stat.h>

#include "uuxml.h"

#include "saml.h"

#include "usr.h"

#include "adminaudit.h"

struct SHIBBOLETHATTRIBUTE {
    struct SHIBBOLETHATTRIBUTE *next;
    char *name;
    char *scope;
    char *value;
    BOOL valid;
};

struct SHIBBOLETHCONTEXT {
    struct SHIBBOLETHATTRIBUTE *saList;
    char *issuer;
};

char *UsrShibbolethAuthGetValue(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                void *context,
                                const char *var,
                                const char *index) {
    struct SHIBBOLETHCONTEXT *sc = (struct SHIBBOLETHCONTEXT *)context;
    struct SHIBBOLETHATTRIBUTE *saList = (struct SHIBBOLETHATTRIBUTE *)sc->saList, *p;
    char *val = NULL;
    char *scope = NULL;
    size_t varLen;
    int indexVal = index ? atoi(index) : 0;
    int indexCurrent = 0;

    if (stricmp(var, "issuer") == 0) {
        if (sc->issuer) {
            if (!(val = strdup(sc->issuer)))
                PANIC;
        }
        return val;
    }

    if (stricmp(var, "shibversion") == 0) {
        if (!(val = strdup("1.2")))
            PANIC;
        return val;
    }

    if (scope = strchr(var, '@')) {
        varLen = scope - var;
        scope++;
    } else
        varLen = strlen(var);

    for (p = saList; p; p = p->next) {
        if (strnicmp(var, p->name, varLen) != 0 || *(p->name + varLen) != 0)
            continue;

        if (p->scope == NULL) {
            if (scope != NULL)
                continue;
        } else {
            if (scope == NULL)
                continue;
            if (strcmp(p->scope, scope) != 0)
                continue;
        }

        if (indexCurrent++ == indexVal) {
            if (p->value)
                if (!(val = strdup(p->value)))
                    PANIC;
            break;
        }
    }

    return val;
}

int UsrShibbolethInit(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    uip->result = resultValid;
    return 0;
}

int UsrShibbolethMapUser(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct SHIBBOLETHCONTEXT *sc = (struct SHIBBOLETHCONTEXT *)context;
    struct SHIBBOLETHATTRIBUTE *saList = (struct SHIBBOLETHATTRIBUTE *)sc->saList, *p;
    char *arg2;
    BOOL appendScope = 0;
    size_t len;

    for (;;) {
        if (*arg != '-')
            break;
        arg2 = SkipNonWS(arg);
        if (*arg2) {
            *arg2++ = 0;
            arg2 = SkipWS(arg2);
        }
        if (stricmp(arg, "-AppendScope") == 0)
            appendScope = 1;
        else if (stricmp(arg, "--") == 0) {
            arg = arg2;
            break;
        } else {
            Log("Unrecognized MapUser option: %s", arg);
        }
        arg = arg2;
    }

    for (p = saList; p; p = p->next) {
        char *newLogUser = NULL;
        if (stricmp(arg, p->name) != 0 || *p->value == 0)
            continue;
        len = strlen(p->value) + 1;
        if (appendScope && p->scope)
            len += strlen(p->scope) + 1;
        if (!(newLogUser = malloc(len)))
            PANIC;
        strcpy(newLogUser, p->value);
        if (appendScope && p->scope) {
            strcat(newLogUser, "@");
            strcat(newLogUser, p->scope);
        }
        Trim(newLogUser, TRIM_CTRL);
        StrCpy3(uip->logUser, newLogUser, sizeof(uip->logUser));
        FreeThenNull(newLogUser);
        return 0;
    }

    return 1;
}

int UsrShibbolethIfTest(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct SHIBBOLETHCONTEXT *sc = (struct SHIBBOLETHCONTEXT *)context;
    struct SHIBBOLETHATTRIBUTE *saList = (struct SHIBBOLETHATTRIBUTE *)sc->saList, *p;
    BOOL re = 0;
    xmlRegexpPtr xrp = NULL, xsrp = NULL;
    char *arg2;
    int result = 1;
    int rc;
    int scopeOption = 0;
    char *scopeVal = NULL;
    BOOL site = 0, anySite = 0;
    char *attribute = NULL;
    char save = 0;
    BOOL negate = 0;

    for (; *arg; arg = SkipWS(arg2)) {
        arg2 = SkipNonWS(arg);
        if (save = *arg2)
            *arg2++ = 0;
        if (site) {
            if (sc->issuer == NULL || strcmp(arg, sc->issuer) != 0) {
                if (uip->debug) {
                    Log("Test -site failed: %s to %s", arg, sc->issuer);
                }
                goto Finished;
            }
            if (uip->debug) {
                Log("Test -site matched: %s to %s", arg, sc->issuer);
            }
            site = 0;
            anySite = 1;
            continue;
        }
        if (scopeOption < 0) {
            scopeVal = arg;
            scopeOption = -scopeOption;
            continue;
        }
        if (stricmp(arg, "-site") == 0) {
            site = anySite = 1;
            continue;
        }
        if (stricmp(arg, "-re") == 0) {
            re = 1;
            continue;
        }
        if (stricmp(arg, "-scope") == 0) {
            scopeOption = -1;
            continue;
        }
        if (stricmp(arg, "-rescope") == 0) {
            scopeOption = -2;
            continue;
        }
        if (stricmp(arg, "-not") == 0) {
            negate = 1;
            continue;
        }
        if (attribute == NULL) {
            attribute = arg;
            continue;
        }
        if (save) {
            arg2--;
            *arg2 = save;
        }
        break;
    }

    if (attribute == NULL) {
        if (anySite)
            result = 0;
        goto Finished;
    }

    if (scopeOption == 2) {
        xsrp = xmlRegexpCompile(BAD_CAST scopeVal);
        if (xsrp == NULL) {
            Log("Invalid regular expression for Test -REScope: %s", scopeVal);
            goto Finished;
        }
    }

    if (re) {
        xrp = xmlRegexpCompile(BAD_CAST arg);
        if (xrp == NULL) {
            Log("Invalid regular expression for Test -RE: %s", arg);
            goto Finished;
        }
    }

    if (uip->debug) {
        if (scopeOption)
            Log("Test searching for attribute %s in scope %s with value %s", attribute, scopeVal,
                arg);
        else
            Log("Test searching for attribute %s with value %s", attribute, arg);
    }

    for (p = saList; p; p = p->next) {
        if (stricmp(attribute, p->name) != 0)
            continue;

        if (scopeOption <= 0 && p->scope != NULL)
            continue;

        if (uip->debug) {
            if (p->scope)
                Log("Found attribute %s in scope %s with value %s", p->name, p->scope, p->value);
            else
                Log("Found attribute %s with value %s", p->name, p->value);
        }

        if (scopeOption > 0 && p->scope == NULL) {
            if (uip->debug) {
                Log("Test specifies scope but attribute is unscoped; skipping");
            }
            continue;
        }

        if (scopeOption == 1 && (p->scope == NULL || strcmp(scopeVal, p->scope) != 0)) {
            if (uip->debug) {
                Log("Attribute in wrong scope; skipping");
            }
            continue;
        }

        if (scopeOption == 2) {
            if (p->scope == NULL)
                continue;
            rc = xmlRegexpExec(xsrp, BAD_CAST p->scope);
            if (rc != 1) {
                if (uip->debug) {
                    Log("Attribute in wrong scope; skipping");
                }
                continue;
            }
        }

        if (xrp) {
            rc = xmlRegexpExec(xrp, BAD_CAST p->value);
            if (rc == 1) {
                result = 0;
                break;
            }
        } else {
            if (stricmp(arg, p->value) == 0) {
                result = 0;
                break;
            }
        }

        if (uip->debug) {
            Log("Attribute value does not match");
        }
    }

    if (result == 0 && uip->debug) {
        Log("Attribute value/scope matched");
    }

Finished:
    if (xrp) {
        xmlRegFreeRegexp(xrp);
        xrp = NULL;
    }

    if (xsrp) {
        xmlRegFreeRegexp(xsrp);
        xsrp = NULL;
    }

    if (negate) {
        result = (result == 0 ? 1 : 0);
        if (uip->debug) {
            Log("Shibboleth Test result negated");
        }
    }

    return result;
}

void ShibbolethSendURLwithWAYF(struct UUSOCKET *s,
                               char *url,
                               char *wayf,
                               char *entityID,
                               char *tag) {
    char *me = (myUrlHttps ? myUrlHttps : myUrlHttp);
    time_t now;
    char *ur = NULL;
    BOOL newStyle = FALSE;
    char *p;

    if (entityID == NULL)
        entityID = shibbolethProviderId;
    else
        newStyle = TRUE;

    if (wayf == NULL) {
        wayf = shibbolethWAYF;
        if (wayf == NULL)
            return;
    }

    UUSendZ(s, wayf);

    if (p = strrchr(wayf, '?')) {
        if (*(p + 1) != 0)
            UUSendZ(s, "&");
    } else
        UUSendZ(s, "?");

    UUSendZ(
        s,
        "shire="); /* SHIRE means "Assertion Consumer Services"; SHAR means "Attribute Requester" */
    SendEscapedURL(s, me);
    SendEscapedURL(s, newStyle ? "/Shibboleth.sso/SAML/POST" : SHIBBOLETHSHIREURL);
    UUSendZ(s, "&target=");
    ur = URLReference(url, tag);
    UUSendZ(s, ur);
    FreeThenNull(ur);
    UUtime(&now);
    WriteLine(s, "&time=%d", now);
    if (entityID && *entityID) {
        UUSendZ(s, "&providerId=");
        SendEscapedURL(s, entityID);
    }
}

BOOL ShibbolethScopeValid(xmlChar **dv, xmlChar **rv, char *scope) {
    BOOL valid = 0;

    if (*dv == NULL)
        goto Finished;

    if (*rv == NULL || strcmp((char *)*rv, "false") == 0) {
        if (strcmp(scope, (char *)*dv) == 0)
            valid = 1;
    } else if (strcmp((char *)*rv, "true") == 0) {
        xmlRegexpPtr xrp = xmlRegexpCompile(*dv);
        if (xrp == NULL)
            Log("scope check unable to parse regular expression: %s", *dv);
        else {
            if (xmlRegexpExec(xrp, *dv) == 1)
                valid = 1;
            xmlRegFreeRegexp(xrp);
            xrp = NULL;
        }
    }

Finished:

    if (*rv) {
        xmlFree(*rv);
        *rv = NULL;
    }
    if (*dv) {
        xmlFree(*dv);
        *dv = NULL;
    }

    return valid;
}

struct SHIBBOLETHATTRIBUTE *ShibbolethShireExtractAttributes(xmlNodeSetPtr nodes,
                                                             xmlNodePtr shibOrigin,
                                                             BOOL includeInvalid) {
    xmlNodePtr cur, child, domainChild, extensionsChild;
    int i;
    char *an, *nv;
    char *scope;
    struct SHIBBOLETHATTRIBUTE *sa, *saList;
    xmlChar *dv, *rv;
    BOOL valid;

    saList = NULL;

    if (nodes == NULL)
        return NULL;

    for (i = 0; i < nodes->nodeNr; i++) {
        if (nodes->nodeTab[i]->type != XML_ELEMENT_NODE)
            continue;

        cur = nodes->nodeTab[i];

        an = (char *)xmlGetProp(cur, BAD_CAST "AttributeName");
        if (an == NULL)
            continue;

        for (child = cur->children; child; child = child->next) {
            if (child->name == NULL || strcmp((char *)child->name, "AttributeValue") != 0)
                continue;
            scope = (char *)xmlGetProp(child, BAD_CAST "Scope");
            valid = 1;
            if (scope) {
                valid = 0;
                dv = rv = NULL;
                for (domainChild = shibOrigin->children; domainChild;
                     domainChild = domainChild->next) {
                    if (domainChild->name == NULL)
                        continue;

                    if (strcmp((char *)domainChild->name, "Domain") == 0) {
                        if (dv = xmlNodeGetContent(domainChild)) {
                            rv = xmlGetProp(domainChild, BAD_CAST "regexp");
                            if (ShibbolethScopeValid(&dv, &rv, scope)) {
                                valid = 1;
                                break;
                            }
                        }
                    }

                    if (strcmp((char *)domainChild->name, "Extensions") == 0) {
                        for (extensionsChild = domainChild->children; extensionsChild;
                             extensionsChild = extensionsChild->next) {
                            if (extensionsChild->name == NULL)
                                continue;
                            if (strcmp((char *)extensionsChild->name, "Scope") != 0)
                                continue;
                            if (dv = xmlNodeGetContent(extensionsChild)) {
                                rv = xmlGetProp(extensionsChild, BAD_CAST "regexp");
                                if (ShibbolethScopeValid(&dv, &rv, scope)) {
                                    valid = 1;
                                    break;
                                }
                            }
                        }
                        if (valid)
                            break;
                    }
                }
            }

            if (valid == 0 && includeInvalid == 0)
                continue;

            if (nv = (char *)xmlNodeGetContent(child)) {
                /*
                printf("%s = %s", an, nv);
                if (scope) {
                    printf("@%s", scope);
                }
                printf("\n");
                */
                if (!(sa = calloc(1, sizeof(*sa))))
                    PANIC;
                if (!(sa->name = strdup(an)))
                    PANIC;
                if (!(sa->value = strdup(nv)))
                    PANIC;
                if (scope)
                    if (!(sa->scope = strdup(scope)))
                        PANIC;
                sa->valid = valid;
                sa->next = saList;
                saList = sa;
                xmlFree(nv);
                nv = NULL;
            }
            if (scope) {
                xmlFree(scope);
                scope = NULL;
            }
        }
        xmlFree(an);
        an = NULL;
    }

    return saList;
}

void ShibbolethShireShowAttributes(struct TRANSLATE *t,
                                   char *ni,
                                   struct SHIBBOLETHATTRIBUTE *saList) {
    struct UUSOCKET *s = &t->ssocket;
    struct SHIBBOLETHATTRIBUTE *sa;

    UUSendZ(s, "<p>Shibboleth Attributes for<br />\n");
    SendHTMLEncoded(s, ni);
    UUSendZ(s, "\n</p>\n");

    if (saList == NULL)
        UUSendZ(s, "<p>No attributes presented</p>\n");
    else {
        UUSendZ(s, "<table class='bordered-table'>\n");
        UUSendZ(s,
                "<tr><th scope='col'>Attribute</th><th scope='col'>Value</th><th "
                "scope='col'>Scope</th></tr>\n");
        for (sa = saList; sa; sa = sa->next) {
            UUSendZ(s, "<tr><td>");
            SendHTMLEncoded(s, sa->name);
            UUSendZ(s, "</td><td>");
            SendHTMLEncoded(s, sa->value);
            UUSendZ(s, "</td><td>");
            if (sa->scope) {
                SendHTMLEncoded(s, sa->scope);
                if (sa->valid == 0) {
                    UUSendZ(s,
                            "<br />(invalid scope for this origin; attribute ignored if present in "
                            "real request)");
                }
            } else {
                UUSendZ(s, "&nbsp;");
            }
            UUSendZ(s, "</td>\n");
        }
        UUSendZ(s, "</table>\n");
    }
}

BOOL AdminShibbolethShireCheckIssueInstant(xmlXPathContextPtr xpathCtx) {
    xmlXPathObjectPtr xpathObj = NULL;
    xmlNodeSetPtr nodes;
    char *ii = NULL;
    struct tm tm, rtm;
    time_t newDtLocal, newDtGm, now;
    int diff;
    BOOL result = 0;

    xpathObj = xmlXPathEvalExpression(BAD_CAST "/soap:Envelope/soap:Body/samlp:Response", xpathCtx);
    if (xpathObj == NULL || (nodes = xpathObj->nodesetval) == NULL || (nodes->nodeNr != 1) ||
        (ii = (char *)xmlGetProp(nodes->nodeTab[0], BAD_CAST "IssueInstant")) == NULL) {
        Log("Unable to locate Shibboleth Assertion IssueInstant");
        goto Finished;
    }

    if (debugLevel > 0) {
        Log("Assertion issued %s", ii);
    }

    memset(&tm, 0, sizeof(tm));
    if (sscanf(ii, "%d-%d-%dT%d:%d:%d", &tm.tm_year, &tm.tm_mon, &tm.tm_mday, &tm.tm_hour,
               &tm.tm_min, &tm.tm_sec) != 6) {
        Log("Unable to parse IssueInstant");
        goto Finished;
    }

    tm.tm_year -= 1900;
    tm.tm_mon--;

    newDtLocal = mktime(&tm);
    if (newDtLocal == (time_t)-1 || UUgmtime_r(&newDtLocal, &rtm) == NULL ||
        (newDtGm = mktime(&rtm)) == (time_t)-1) {
        Log("Invalid IssueInstant");
        goto Finished;
    }

    /* Now convert to GMT by computing time zone difference and applying */
    diff = newDtGm - newDtLocal;

    newDtGm = newDtLocal - diff;
    UUtime(&now);

    diff = (now < newDtGm ? newDtGm - now : now - newDtGm);

    if (diff < 300)
        result = 1;
    else {
        /* Shorten a string that is obviously too long to avoid logging issue for overtly bad II */
        if (strlen(ii) > 32)
            ii[32] = 0;
        Log("IssueInstant time disagreement, check system clock: %s", ii);
    }

Finished:
    if (ii) {
        xmlFree(ii);
        ii = NULL;
    }

    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    return result;
}

/*
 Attribute Authority (AA, deprecated): The AA is the portion of the IdP responsible for issuing
 attributes on behalf of an organization.
 */
BOOL AdminShibbolethShireGetAA(char *samlResponse,
                               char **ni,
                               xmlNodePtr *originNode,
                               char *useSsl,
                               char **host,
                               PORT *port,
                               char **aaPath,
                               int *certificate,
                               char **issuer,
                               struct sockaddr_storage *ipInterface) {
    char *ni1, *ni2, *ni3;
    char save = 0;
    xmlChar *nqp = NULL;
    xmlChar *aa = NULL;
    int result = 0;
    xmlNodeSetPtr nodes = NULL;
    char *p;
    char *colon, *endOfHost;
    char myUseSsl;
    struct SHIBBOLETHSITE *ss;
    xmlParserCtxtPtr ctxt = NULL;
    xmlDocPtr doc = NULL; /* the resulting document tree */
    xmlXPathContextPtr xpathCtx = NULL;
    xmlXPathObjectPtr xpathObj = NULL;

    *issuer = NULL;

    *ni = NULL;

    if (ipInterface)
        memset(ipInterface, 0, sizeof(*ipInterface));

    if (certificate)
        *certificate = 0;

    if (originNode)
        *originNode = NULL;

    if (useSsl)
        *useSsl = 0;

    if (host)
        *host = NULL;

    if (port)
        *port = 0;

    if (debugLevel > 128) {
        Log("%s\n", samlResponse);
    }

    ctxt = xmlCreatePushParserCtxt(NULL, NULL, samlResponse, strlen(samlResponse), NULL);
    if (ctxt == NULL) {
        Log("Non-XML response from AA");
        goto Finished;
    }

    xmlParseChunk(ctxt, samlResponse, 0, 1);

    doc = ctxt->myDoc;
    xpathCtx = xmlXPathNewContext(doc);
    if (xpathCtx == NULL) {
        Log("Error: unable to create new XPath context to find issuer");
        goto Finished;
    }

    xmlXPathRegisterNs(xpathCtx, BAD_CAST "samlp", BAD_CAST "urn:oasis:names:tc:SAML:1.0:protocol");
    xmlXPathRegisterNs(xpathCtx, BAD_CAST "soap",
                       BAD_CAST "http://schemas.xmlsoap.org/soap/envelope/");
    xmlXPathRegisterNs(xpathCtx, BAD_CAST "saml", BAD_CAST "urn:oasis:names:tc:SAML:1.0:assertion");

    xpathObj = xmlXPathEvalExpression(BAD_CAST "/samlp:Response/saml:Assertion", xpathCtx);

    if (xpathObj == NULL || (nodes = xpathObj->nodesetval) == NULL || (nodes->nodeNr != 1) ||
        (*issuer = (char *)xmlGetProp(nodes->nodeTab[0], BAD_CAST "Issuer")) == NULL) {
        Log("Unable to locate Shibboleth Assertion Issuer");
        goto Finished;
    }

    if ((ni1 = strstr(samlResponse, "samlp:Success")) && (ni1 = strstr(ni1, "<NameIdentifier")) &&
        (ni2 = strstr(ni1, "</NameIdentifier")) && (ni3 = strchr(ni2, '>'))) {
        ni3++;
        save = *ni3;
        *ni3 = 0;
        if (!(*ni = strdup(ni1)))
            PANIC;
        *ni3 = save;
    }

    if (*ni == NULL) {
        Log("No NameIdentifier in assertion");
        goto Finished;
    }

    if (debugLevel > 0)
        Log("SAML NameIdentifier: %s", *ni);

    /* Truncate responseIssuer if it is ridiculously long */
    if (strlen((char *)*issuer) > 256) {
        *(*issuer + 256) = 0;
    }

    if (!(nqp = malloc(strlen((char *)*issuer) + 100)))
        PANIC;

    nodes = NULL;

    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    for (ss = shibbolethSites; ss; ss = ss->next) {
        char **path, *paths[] = {
                         "//md:EntityDescriptor[@entityID = "
                         "'%s']/md:AttributeAuthorityDescriptor/md:AttributeService",
                         "//default:OriginSite[@Name = '%s']/default:AttributeAuthority",
                         "//default:OriginSite/default:AttributeAuthority[@Name = '%s']", NULL};
        if (LoadUpdateShibbolethMetadata(ss, FALSE) == 0)
            continue;

        UUAcquireMutex(&ss->mutex);

        for (path = paths; *path; path++) {
            sprintf((char *)nqp, *path, *issuer);
            xpathObj = xmlXPathEvalExpression(nqp, ss->xPathCtx);
            if (xpathObj) {
                nodes = xpathObj->nodesetval;
                if (nodes && nodes->nodeNr)
                    break;
                xmlXPathFreeObject(xpathObj);
                xpathObj = NULL;
                nodes = NULL;
            }
        }

        UUReleaseMutex(&ss->mutex);

        if (nodes && nodes->nodeNr)
            break;
    }

    if (nodes == NULL || nodes->nodeNr == 0 ||
        (aa = xmlGetProp(nodes->nodeTab[0], BAD_CAST "Location")) == NULL) {
        Log("Unable to locate AA for %s", *issuer);
        goto Finished;
    }

    if (originNode)
        *originNode = nodes->nodeTab[0]->parent;

    p = (char *)aa;

    if (strnicmp(p, "http://", 7) == 0) {
        myUseSsl = 0;
        p += 7;
    } else if (strnicmp(p, "https://", 8) == 0) {
        myUseSsl = 1;
        p += 8;
    } else
        goto Finished;

    if (useSsl)
        *useSsl = myUseSsl;

    ParseHost(p, &colon, &endOfHost, 0);

    if (colon) {
        *colon++ = 0;
        if (port)
            *port = atoi(colon);
    }

    if (port && *port == 0)
        *port = myUseSsl ? 443 : 80;

    if (endOfHost) {
        save = *endOfHost;
        *endOfHost = 0;
    }

    if (host)
        if (!(*host = strdup(p)))
            PANIC;

    if (save)
        *endOfHost = save;

    if (aaPath)
        if (!(*aaPath = strdup(endOfHost)))
            PANIC;

    if (certificate)
        *certificate = ss->certificates ? *ss->certificates : 0;

    if (ipInterface)
        memcpy(ipInterface, &ss->ipInterface, sizeof(*ipInterface));

    result = 1;

Finished:
    if (aa) {
        xmlFree(aa);
        aa = NULL;
    }

    FreeThenNull(nqp);

    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    if (xpathCtx) {
        xmlXPathFreeContext(xpathCtx);
        xpathCtx = NULL;
    }

    if (ctxt) {
        xmlFreeParserCtxt(ctxt);
        ctxt = NULL;
    }

    if (doc) {
        xmlFreeDoc(doc);
        doc = NULL;
    }

    return result;
}

void AdminShibbolethShire(struct TRANSLATE *t, char *query, char *post) {
    int shibUsrFile = -1;
    struct USRDIRECTIVE udc[] = {
        {"",        UsrShibbolethInit,    0},
        {"IfTest",  UsrShibbolethIfTest,  0},
        {"MapUser", UsrShibbolethMapUser, 0},
        {"Test",    UsrShibbolethIfTest,  0},
        {NULL,      NULL,                 0}
    };

    struct FILEREADLINEBUFFER frb;
    struct FILEINCLUSION fileIncludeNodeNewTopOfStack;
    enum FINDUSERRESULT result;
    struct USERINFO ui;
    const int ASTARGET = 0;
    const int ASSAMLRESPONSE = 1;
    struct SESSION *e;
    char *target;
    char *url = NULL;
    char *ni = NULL;
    struct UUSOCKET *s = &t->ssocket;
    struct UUSOCKET rr, *r = &rr;
    char phase;
    char *samlResponse;
    EVP_MD_CTX *mdctx = NULL;
    unsigned char md_value[EVP_MAX_MD_SIZE];
    unsigned int md_len;
    char hex[2 * EVP_MAX_MD_SIZE + 1];
    BOOL inHeader;
    struct tm rtm;
    xmlNodePtr shibOrigin;
    int shibCert = 0;
    struct sockaddr_storage shibInterface;
    char *env = NULL;
    char *env1 =
        "\
<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n\
<Body>\r\n\
<Request xmlns=\"urn:oasis:names:tc:SAML:1.0:protocol\" IssueInstant=\"%s\" MajorVersion=\"1\" MinorVersion=\"1\" Requestid='A%s' xmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\" xmlns:samlp=\"urn:oasis:names:tc:SAML:1.0:protocol\">\r\n\
<AttributeQuery Resource=\"%s\">\r\n\
<Subject xmlns=\"urn:oasis:names:tc:SAML:1.0:assertion\">\r\n\
%s\r\n\
</Subject>\r\n\
</AttributeQuery>\r\n\
</Request>\r\n\
</Body>\r\n\
</Envelope>\r\n";

    struct FORMFIELD ffs[] = {
        {"target",       NULL, 0, 0, 0                  },
        {"SAMLResponse", 0,    0, 0, FORMFIELDNOTRIMCTRL},
        {NULL,           NULL, 0, 0, 0                  }
    };
    char line[8192];
    char issueInstant[64];
    time_t now;
    char shibUseSsl = 0;
    char *shibHost = NULL;
    PORT shibPort = 443;
    char *shibAaPath = NULL;
    char *shibIssuer = NULL;
    xmlParserCtxtPtr ctxt = NULL;
    xmlDocPtr doc = NULL; /* the resulting document tree */
    int res;
    xmlXPathContextPtr xpathCtx = NULL;
    xmlXPathObjectPtr xpathObj = NULL;
    GROUPMASK gmAuto = NULL;
    BOOL urlReference;

    struct SHIBBOLETHATTRIBUTE *saList = NULL, *sa, *saNext;
    BOOL showAttributes;

    InitUserInfo(t, &ui, "shibboleth", NULL, NULL, NULL);

    t->finalStatus = 997;

    UUInitSocket(r, INVALID_SOCKET);

    if (AdminShibbolethUnavailable(t, shibbolethAvailable12, "1.2"))
        goto Finished;

    shibUsrFile = -2;
    if (!PushIncludeFileStack(&ui, &frb, SHIBOLDUSR, NULL, ui.fileInclusion,
                              &fileIncludeNodeNewTopOfStack, &shibUsrFile)) {
        if ((shibUsrFile == -1) && (errno != ENOENT)) {
            Log("Shibboleth disabled since " SHIBOLDUSR " exists but cannot be opened: %d", errno);
            HTTPCode(t, 500, 1);
            goto Finished;
        }
        if (shibUsrFile == -2) {
            /* failed to open the file for some other reason, see log */
            goto Finished;
        }
    } else {
        ui.fileInclusion = &fileIncludeNodeNewTopOfStack;
    }

    FindFormFields(query, post, ffs);

    target = ffs[ASTARGET].value;
    if (target == NULL)
        goto Finished;

    ui.urlReference = urlReference = DecodeURLReference(target);

    showAttributes = stricmp(target, "attributes") == 0;

    if (samlResponse = ffs[ASSAMLRESPONSE].value) {
        char *str;
        char *line;

        Decode64(samlResponse, samlResponse);
        if (AdminShibbolethShireGetAA(samlResponse, &ni, &shibOrigin, &shibUseSsl, &shibHost,
                                      &shibPort, &shibAaPath, &shibCert, &shibIssuer,
                                      &shibInterface) == 0)
            goto Finished;
        for (str = NULL, line = StrTok_R(samlResponse, "\r\n", &str); line;
             line = StrTok_R(NULL, "\r\n", &str)) {
            if (debugLevel > 0)
                Log("SAML: %s\n", line);
        }
    }

    UUtime(&now);
    strftime(issueInstant, sizeof(issueInstant), "%Y-%m-%dT%H:%M:%SZ", UUgmtime_r(&now, &rtm));

    if (!(mdctx = EVP_MD_CTX_new()))
        PANIC;
    EVP_DigestInit(mdctx, EVP_sha1());
    EVP_DigestUpdate(mdctx, samlResponse, strlen(samlResponse));
    EVP_DigestUpdate(mdctx, issueInstant, strlen(issueInstant));
    EVP_DigestFinal(mdctx, md_value, &md_len);

    DigestToHex(hex, md_value, md_len, 1);

    /* This is slight too large due to the %s in env1, but that's OK */
    if (!(env = calloc(1, strlen(env1) + strlen(shibbolethProviderId) + strlen(ni) +
                              strlen(issueInstant) + strlen(hex) + 1)))
        PANIC;
    sprintf(env, env1, issueInstant, hex, shibbolethProviderId, ni);

    if (debugLevel > 0)
        Log("%s", env);

    if (UUConnectWithSource4(r, shibHost, shibPort, "AdminShibbolethShire", &shibInterface,
                             shibUseSsl, shibCert, 0)) {
        /*
        InitUserInfo(t, &ui, ni, NULL, target);
        needtocleanupui.gm
        goto ForceValid;
        */
        goto Finished;
    }

    WriteLine(r,
              "POST %s HTTP/1.0\r\n\
Host: %s:%d\r\n\
Pragma: no-cache\r\n\
Accept: */*\r\n\
Content-Type: text/xml\r\n\
SOAPAction: http://www.oasis-open.org/committees/security\r\n\
Shibboleth: 1.2\r\n\
Content-Length: %d\r\n\
\r\n",
              shibAaPath, shibHost, shibPort, strlen(env));

    UUSendZ(r, env);

    if (debugLevel > 0) {
        Log("Sent envelope|POST %s HTTP/1.0|\
Host: %s:%d|\
Pragma: no-cache|\
Accept: */*|\
Content-Type: text/xml|\
SOAPAction: http://www.oasis-open.org/committees/security|\
Shibboleth: 1.2|\
Content-Length: %d|\
|%s",
            shibAaPath, shibHost, shibPort, strlen(env), env);
    }

    if (debugLevel > 0)
        Log("AA response");

    inHeader = 1;
    while (ReadLine(r, line, sizeof(line)) > 0) {
        if (debugLevel > 0)
            Log("%s", line);
        if (inHeader) {
            if (line[0] == 0)
                inHeader = 0;
            continue;
        }
        if (ctxt == NULL) {
            ctxt = xmlCreatePushParserCtxt(NULL, NULL, line, strlen(line), NULL);
            if (ctxt == NULL) {
                Log("Non-XML response from AA");
                goto Finished;
            }
        } else {
            xmlParseChunk(ctxt, line, strlen(line), 0);
        }
    }

    if (ctxt == NULL) {
        Log("AA did not respond; common cause is that " EZPROXYNAMEPROPER
            " ShibbolethMetadata Cert not accepted by Identity Provider");
        goto Finished;
    }

    xmlParseChunk(ctxt, line, 0, 1);

    doc = ctxt->myDoc;
    res = ctxt->wellFormed;

    xpathCtx = xmlXPathNewContext(doc);
    if (xpathCtx == NULL) {
        Log("Error: unable to create new XPath context");
        goto Finished;
    }

    xmlXPathRegisterNs(xpathCtx, BAD_CAST "samlp", BAD_CAST "urn:oasis:names:tc:SAML:1.0:protocol");
    xmlXPathRegisterNs(xpathCtx, BAD_CAST "soap",
                       BAD_CAST "http://schemas.xmlsoap.org/soap/envelope/");
    xmlXPathRegisterNs(xpathCtx, BAD_CAST "saml", BAD_CAST "urn:oasis:names:tc:SAML:1.0:assertion");

    if (!AdminShibbolethShireCheckIssueInstant(xpathCtx))
        goto Finished;

    xpathObj = xmlXPathEvalExpression(BAD_CAST
                                      "/soap:Envelope/soap:Body/samlp:Response/saml:Assertion/"
                                      "saml:AttributeStatement/saml:Attribute",
                                      xpathCtx);
    if (xpathObj == NULL) {
        Log("Error: unable to evaluate XPath expression");
        goto Finished;
    }

    saList = ShibbolethShireExtractAttributes(xpathObj->nodesetval, shibOrigin, showAttributes);

    if (showAttributes) {
        if (t->session && (t->session)->priv)
            AdminBaseHeader(t, 0, "Shibboleth Attributes",
                            " | <a href='/shibboleth'>Manage Shibboleth</a>");
        else
            AdminBaseHeader(t, AHNOTOPMENU, NULL, NULL);

        ShibbolethShireShowAttributes(t, ni, saList);
        if (t->session && (t->session)->priv)
            AdminFooter(t, 0);
        goto Finished;
    }

    ui.url = target;

    GroupMaskDefault(ui.gm);
    if (shibUsrFile >= 0) {
        struct SHIBBOLETHCONTEXT sc;
        sc.issuer = shibIssuer;
        sc.saList = saList;
        ui.flags = 0;
        /* authGetValue and/or authAggregateTest should always be assigned
           just before a call to UsrHandler or before the macro USRCOMMON1
           since both of these reset the authGetValue and authAggregateTest
           to NULL when the test completes to avoid having one of these functions
           called later during user.txt processing when the function may no
           longer match the authentication in effect
        */
        ui.authGetValue = UsrShibbolethAuthGetValue;
        result =
            UsrHandler(t, "Shibboleth", udc, &sc, &ui, SHIBOLDUSR, shibUsrFile, &frb, NULL, NULL);
        if (ui.denied)
            goto Finished;
    }

    /* ForceValid: */
    e = t->session;
    if (e == NULL) {
        e = StartSession(ui.logUser, ui.relogin, NULL);
        if (e == NULL) {
            HTTPCode(t, 503, 0);
            HTTPContentType(t, NULL, 1);
            UUSendZ(
                s, TAG_DOCTYPE_HTML_HEAD
                "</head><body>Maximum sessions reached, please try again later</body></html>\n");
            goto Finished;
        }
        /* GroupMaskOr needs to be here for AuditEvent, and below if user is merging two session */
        GroupMaskOr(e->gm, ui.gm);
        AuditEventLogin(t, AUDITLOGINSUCCESS, ui.logUser, e, NULL, "Shibboleth");
    } else
        UpdateUsername(t, e, ui.logUser, "Shibboleth");
    e->sin_addr = t->sin_addr;

    /* Note that we've logged in through Shibboleth, so if there is a logup event, no reason to
     * retry */
    e->loggedinShibboleth = 1;

    MergeSessionVariables(t, e);

    GroupMaskOr(e->gm, ui.gm);
    LoadDefaultCookies(e);
    e->priv = e->priv || ui.admin;
    e->sessionFlags |= ui.sessionFlags;
    e->docsCustomDir = t->docsCustomDir;
    AdminLoginVars(e, &ui, 1);
    memcpy(&e->ipInterface, &ui.sourceIpInterface, sizeof(e->ipInterface));
    if (ui.loginBanner[0])
        ReadConfigSetString(&e->loginBanner, ui.loginBanner);

    gmAuto = GroupMaskNew(0);
    URLIpAccess(t, target, NULL, gmAuto, NULL, NULL);
    GroupMaskOr(e->gm, gmAuto);
    LoadDefaultCookies(e);
    GroupMaskFree(&gmAuto);

    if (!(url = calloc(1, strlen(target) * 3 + 1)))
        PANIC;
    AddEncodedField(url, target, NULL);

    phase = e->notify != 0 ? 't' : 's';

    HTTPCode(t, 303, 0);
    NoCacheHeaders(s);
    if (*t->version) {
        if (e->notify == 0)
            SendEZproxyCookie(t, e);
        WriteLine(s, "Location: %s/connect?session=%c%s&qurl=", t->myUrl, phase, e->connectKey);
        UUSendZCRLF(s, url);
        HeaderToBody(t);
    }

    UUSendZ(s, "To access this site, go <a href='");
    WriteLine(s, "%s/connect?session=%c%s&qurl=", t->myUrl, phase, e->connectKey);
    UUSendZ(s, url);
    UUSendZ(s, "'>here</a>\r\n");

Finished:
    UUStopSocket(r, 0);

    if (mdctx) {
        EVP_MD_CTX_free(mdctx);
        mdctx = NULL;
    }

    if (t->finalStatus == 997) {
        SendShibFailure(t);
    }

    for (sa = saList; sa; sa = saNext) {
        saNext = sa->next;
        FreeThenNull(sa->name);
        FreeThenNull(sa->value);
        FreeThenNull(sa->scope);
        FreeThenNull(sa);
    }

    GroupMaskFree(&ui.gm);
    FreeThenNull(ni);

    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    if (xpathCtx) {
        xmlXPathFreeContext(xpathCtx);
        xpathCtx = NULL;
    }

    if (ctxt) {
        xmlFreeParserCtxt(ctxt);
        ctxt = NULL;
    }

    if (doc) {
        xmlFreeDoc(doc);
        doc = NULL;
    }

    FreeThenNull(url);
    FreeThenNull(ni);
    FreeThenNull(env);

    if (shibUsrFile >= 0) {
        close(shibUsrFile);
        shibUsrFile = -1;
    }
    PopIncludeFileStack(&ui, ui.fileInclusion, NULL);
    ui.fileInclusion = NULL;
}
