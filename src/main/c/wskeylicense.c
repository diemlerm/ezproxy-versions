/**
 * wskeylicense.c
 *
 *  Created on: Sep 18, 2013
 *      Author: smelserw
 *
 * License must follow a specific flow:
 * 1. Allocate memory and set default values for license, WskeyLicenseCreate()
 * 2. Attempt to read the license, readLicense(license)
 *      2a. also validates the signature with rsaVerify
 * 3. If a read fails
 *         3b. Set key on license structure instance, strncpy(license->key, key, license->keySize)
 *         3c. check key with wskey service, checkKeyWithWskey(license)
 * 4. Verify Signature, rsaVerify(signature, license)
 * 5. Verify not expired
 * 6. Call handleState(license) - This sets some info on the license
 * 7. Call writeMessages(license, now, revalidated) - Give status messages about license
 * 8. Write license to file, writeLicense(license)
 *
 */

#define __UUFILE__ "wskeylicense.c"

#include "wskeylicense.h"
#include "wskey.h"

#ifdef WIN32
#include "win32.h"
#endif

/**
 * Filename for wskey license
 */
static const char *WskeyFileName = "wskey.key";
static const char *WskeyNewFileName = "wskey.knw";

struct WSKEYLICENSE *activeLicense = NULL;

/**
 * BETA License?
 */
#if (EZPROXYRELEASELEVEL == 1) && EXPIREYEAR && EXPIREMONTH && EXPIREDAY
static const BOOL BETA = TRUE;
static const int BETA_YEAR = EXPIREYEAR;
static const int BETA_MONTH = EXPIREMONTH;
static const int BETA_DAY = EXPIREDAY;
#else
static const BOOL BETA = FALSE;
static const int BETA_YEAR = 0;
static const int BETA_MONTH = 0;
static const int BETA_DAY = 0;
#endif

// TODO: When move to only wskey license, move these variables to be local again...maybe.
// Have to look at the -E settings, which allow these to be altered.  Would have to make
// public getter and setter for these values.  This might be a good solution.
/**
 * Offset for how often the license thread should sleep for
 * on iteration of the loop. In seconds.
 */
// static const int WskeyThreadSleep = 60;

/**
 * Offset for how often wskey license should be verified. In seconds
 */
// static const int WskeyRecheckCycle = 90;// //should align with what is in
// guardianChargeMaximumLatency (globals.c)

/**
 * Offset for when a wskey is set to expired, after a successful
 * license check. In seconds
 */
// static const int WskeyExpirePeriod = ((60 * 60 * 24) * 30) * 3; //we give 3 month grace period

/**
 * If a check fail, this will be the time offset
 * until next check.  In seconds.
 */
// static const int WskeyCheckFailedCycle = 600;

// byte size
static const int wskeyTokenSize = 20;

// provided by the WSKey Team.
// This is in response xml (/authResponse/authorizeResponse/status/text())
static const char *sigPostfix = "SUCCESSAUTHORIZED";

/**
 * The public key for ezproxy/wskey communication.  Wskey has a private
 * key used for signing responses.
 * 1024 sha1 rsa public key. wskey used the corresponding private key to sign xml response
 */
static const char *wskeyPubKey =
    "-----BEGIN PUBLIC "
    "KEY-----\r\nMIGdMA0GCSqGSIb3DQEBAQUAA4GLADCBhwKBgQDWciefmNBuxyjyXM/"
    "bWTxkroNx\r\nqgPNx5zWbjwFwBQqBV9CAe7d3kZg5g4nY9iIjg9qsQ4S8mlyHdki+"
    "0BISwT6OllF\r\nQ58BRLCsNmCW8g8OOHBz4DvWGMk/"
    "4UOSwssIIKYJ0DKN6xLCcedIaRqqfr1f+854\nRgStx644nw4yNyYCZQIBAw==\r\n-----END PUBLIC "
    "KEY-----\r\n";

/* essential license functions */
static enum WSKEY_STATES rsaVerifySignature(char *signature, struct WSKEYLICENSE *license);
static void recheckLicense(struct WSKEYLICENSE *license, struct GUARDIAN *guardian);
static enum WSKEY_STATES checkKeyWithWskey(struct WSKEYLICENSE *license);
static BOOL writeLicense(struct WSKEYLICENSE *license);

/* helper functions */
static char *createToken();
static int timeToStr(char *buffer, time_t time);
static BOOL readLine(int handle, char *buffer, int size, BOOL success);
static BOOL writeLine(int handle, char *line, BOOL success);
static void setNonce(struct WSKEYLICENSE *license);
static void errLogger(const char *message, const BOOL showLicense, const char *key);
static void printErrorState(struct WSKEYLICENSE *license,
                            enum WSKEY_STATES state,
                            BOOL showLicense);
static void writeMessages(struct WSKEYLICENSE *license, time_t now, BOOL revalidated);
static void promptUserForKey(char *key, int size);
static void killCharge(struct WSKEYLICENSE *license);
static time_t getNextShutdownTime(const time_t time);
static time_t adjustedRevalidate(time_t baseTime, struct WSKEYLICENSE *license, char *myName);
static char *hTime(const time_t time, char *buffer, int bSize);

/**
 * Registered with guardian and loops indefinitely
 * to continue validation of license.
 *
 * @see main.c
 */
void WskeyBeginThread(void *v) {
    if (debugLevel)
        Log("Wskey license thread started.");

    activeLicense = (struct WSKEYLICENSE *)v;

    time_t now;

    for (;;) {
        ChargeAlive(GCWSKEYLICENSE, NULL);

        // this will set shutdown signal if relevant
        recheckLicense(activeLicense, guardian);

        UUtime(&now);
        int sleepTime;
        if (activeLicense->nextRevalidateAttempt <= now) {
            sleepTime = wskeyThreadSleep;
        } else {
            sleepTime = activeLicense->nextRevalidateAttempt - now;
            if (sleepTime > wskeyThreadSleep) {
                sleepTime = wskeyThreadSleep;
            }
        }

        // sleepTime is approximate; actual time is expected to be slightly longer
        // due to implementation, but this processing is not that sensitive

        if (debugLevel) {
            Log("Wskey thread sleeping for %d seconds", sleepTime);
        }
        const int sleepInterval = 10;
        for (;;) {
            SubSleep(sleepInterval, 0);
            sleepTime -= sleepInterval;
            if (sleepTime <= 0 || guardian->licenseChange) {
                break;
            }
        }
        if (debugLevel) {
            Log("Wskey license awakes.");
        }
    }
}

/**
 * Check current license key with wskey and print any errors
 * associated with wskey validation.
 *
 * If the license is good with wskey, then store the new
 * wksey.key file.
 */
void WskeyInitialize(struct WSKEYLICENSE *license) {
    // check license with wskey service
    int state = checkKeyWithWskey(license);
    if (state != WSKEY_SIG_SET) {
        printErrorState(license, state, TRUE);
        return;
    }

    state = rsaVerifySignature(license->signature, license);
    if (state != WSKEY_AUTHORIZED) {
        printErrorState(license, state, TRUE);
        return;
    }

    if (!writeLicense(license)) {
        Log("*** WARNING: Failed to write license file.");
        Log("*** WARNING: Restart may require license to be re-entered.");
    }

    time_t now;
    UUtime(&now);
    writeMessages(license, now, 0);
}

/**
 * NOTE: Make sure we don't keep creating licenses
 * and not free them.  This will be a memory leak
 */
struct WSKEYLICENSE *WskeyLicenseCreate(const char *key) {
    if (!wsKeyReadConfig) {
        ReadConfig(TRUE);
    }
    // initialize license
    struct WSKEYLICENSE *license;
    license = calloc(1, sizeof(struct WSKEYLICENSE));

    license->keySize = WSKEY_KEYSIZE;
    license->sigSize = WSKEY_SIGSIZE;
    license->tokenSize = WSKEY_TOKENSIZE;
    license->nonceSize = WSKEY_NONCESIZE;

    time_t now;
    UUtime(&now);

    license->lastRevalidateAttempt = now;
    license->expiry = getNextShutdownTime(now + wskeyExpirePeriod);
    license->lastRevalidateSuccess = now;
    license->revalidate = now + wskeyRecheckPeriod;
    license->warned = FALSE;

    if (key) {
        StrCpy3(license->key, (char *)key, license->keySize);
        license->revalidate = adjustedRevalidate(license->revalidate, license, myName);
    } else {
        license->key[0] = '\0';
    }
    license->signature[0] = '\0';
    license->nextRevalidateAttempt = license->revalidate;
    strcpy(license->token, createToken());

    return license;
}

/**
 *  Display beta expiration information.
 */
void WskeyBetaInfo() {
    if (BETA) {
        if (!WskeyIsBetaExpired()) {
            Log("BETA will expire and shutdown on %4d-%02d-%02d", BETA_YEAR, BETA_MONTH, BETA_DAY);
        } else {
            Log("BETA expired on %4d-%02d-%02d", BETA_YEAR, BETA_MONTH, BETA_DAY);
        }
        return;
    }
}

void GetWskeyKey(char **key) {
    struct WSKEYLICENSE *license = WskeyLicenseCreate(NULL);
    if (!WskeyReadLicense(license, TRUE))
        PANIC;
    *key = UUStrDup(license->key);
    free(license);
}

/**
 * Read the license file and log information about it
 */
void WskeyInfo() {
    time_t now;
    UUtime(&now);
    int bsize = 20;
    char buffer[bsize];
    struct WSKEYLICENSE *license = WskeyLicenseCreate(NULL);
    if (WskeyReadLicense(license, TRUE)) {
        if (isLicenseExpired(license)) {
            Log("Current license key expired on %s%s.", hTime(license->expiry, buffer, bsize),
                license->expiry > now ? " (too far in future; clock issue)" : "");
        } else {
            if (license->revalidate < now) {
                Log("EZproxy license key requires revalidation with OCLC by %s.",
                    hTime(license->expiry, buffer, bsize));
            } else {
                Log("EZproxy license key is valid.");
            }
        }

    } else {
        Log("Failed to read key file or key file was invalid.");
    }

    free(license);
    return;
}

/**
 * After ezproxy is running, this function should be called periodically
 * to check the license.  Will disable proxying if the license is expired.
 *
 * @see WskeyBeginThread, killCharge
 */
static void recheckLicense(struct WSKEYLICENSE *license, struct GUARDIAN *guardian) {
    if (license == NULL)
        PANIC;

    if (debugLevel) {
        Log("Checking wskey license...");
        WskeyInfo();
    }

    time_t now;
    UUtime(&now);

    // If we've been notified of a license change, pick up the new license
    // as long as it is valid.
    if (guardian->licenseChange) {
        struct WSKEYLICENSE *updatedLicense = WskeyLicenseCreate(NULL);
        if (WskeyReadLicense(updatedLicense, TRUE) && !isLicenseExpired(updatedLicense)) {
            memcpy(license, updatedLicense, sizeof(*license));
            WskeyInfo();
        }
        FreeThenNull(updatedLicense);
        guardian->licenseChange = 0;
    }

    if (BETA) {
        if (WskeyIsBetaExpired()) {
            WskeyBetaInfo();
            killCharge(license);
            // Set nextCheck in the future so we don't get called back to
            // here immediately before EZproxy can shutdown
            license->nextRevalidateAttempt = now + wskeyRecheckFailPeriod;
            return;
        }
        // Beta versions do not require license; if we don't have one, nothing
        // more to check for now
        if (license->key[0] == 0) {
            license->nextRevalidateAttempt = now + wskeyRecheckFailPeriod;
            return;
        }
    }

    if (debugLevel > 9998) {
        char buffer[20];
        Log("License data (now is %s): ", hTime(now, buffer, 20));
        Log("  expiry        : %s, (%d sec)", hTime(license->expiry, buffer, 20),
            (license->expiry - now));
        Log("  lastAttempt   : %s, (%d sec)", hTime(license->lastRevalidateAttempt, buffer, 20),
            (license->lastRevalidateAttempt - now));
        Log("  lastSuccess   : %s, (%d sec)", hTime(license->lastRevalidateSuccess, buffer, 20),
            (license->lastRevalidateSuccess - now));
        Log("  nextRevalidate: %s, (%d sec)", hTime(license->nextRevalidateAttempt, buffer, 20),
            (license->nextRevalidateAttempt - now));

        Log("  key           : %s", license->key);
        Log("  signature     : %s", license->signature);
        Log("  token         : %s", license->token);
        Log("  nonce         : %s", license->nonce);
    }

    // wait until we are supposed to check
    if (license->nextRevalidateAttempt > now && license->expiry > now) {
        if (debugLevel)
            Log("License is still valid, skip verification.");
        return;
    }

    license->nextRevalidateAttempt = now + wskeyRecheckFailPeriod;
    // If this isn't a beta and we expire before the normal
    // next recheck period, bring nextRecheckTime back to expiration
    if (license->nextRevalidateAttempt > license->expiry && !BETA) {
        license->nextRevalidateAttempt = license->expiry;
    }

    // copy the license and make updated version
    struct WSKEYLICENSE *newlicense = WskeyLicenseCreate(license->key);

    // examine new license
    int state = checkKeyWithWskey(newlicense);
    int state2 = rsaVerifySignature(newlicense->signature, newlicense);

    // signature failed
    if (state != WSKEY_SIG_SET) {
        printErrorState(license, state, FALSE);
    }

    // valid signature, but maybe not authorized
    if (state == WSKEY_SIG_SET && state2 != WSKEY_AUTHORIZED) {
        printErrorState(newlicense, state2, FALSE);
    }

    // if expired and either signature was not set or was not authorized
    if (isLicenseExpired(license) && (state != WSKEY_SIG_SET || state2 != WSKEY_AUTHORIZED)) {
        printErrorState(license, WSKEY_EXPIRED, FALSE);
        licenseValid = FALSE;
        FreeThenNull(newlicense);
        return;
    }

    BOOL revalidated = 0;
    // only write license if it was properly updated and valid
    if (state == WSKEY_SIG_SET && state2 == WSKEY_AUTHORIZED) {
        writeLicense(newlicense);
        // If we have been warning user about changes, let user know that
        // revalidation succeeded.
        revalidated = license->warned;
        // Make updated license our license; this also resets warned to false.
        memcpy(license, newlicense, sizeof(*license));
    }

    FreeThenNull(newlicense);

    writeMessages(license, now, revalidated);
}

/**
 * A little misleading the name.  This will kill the charge
 * if there is a guardian.  Does not do anything if there
 * is no charge.
 */
static void killCharge(struct WSKEYLICENSE *license) {
    // this can be called during setup, so do not
    // try and kill guardian before it is running
    if (guardian != NULL) {
        if (BETA) {
            Log("Shutting down expired BETA");
        } else {
            Log("Shutting down due to previous wskey license errors.");
        }
        guardian->chargeStatus = chargeStopping;
        guardian->chargeStop = 1;
    }
}

/**
 * Check license with wskey.  This uses external functions in wskey.c
 *
 * @return The state is the result of either
 * WskeyVerifyUsingProxies() or WSkeyVerify()
 *
 * @see WSkeyVerifyUisngProxies, WSkeyVerify
 */
static enum WSKEY_STATES checkKeyWithWskey(struct WSKEYLICENSE *license) {
    if (debugLevel)
        Log("Checking license with Wskey Service.");

        // possible winsock has not been initialized.  e.g. -v or -k startup arguments
#ifdef WIN32
    StartWinsock();
#endif

    // serialize license into nonce
    setNonce(license);

    // have to check if ezproxy is directed to use a proxy
    if (!wsKeyReadConfig) {
        if (debugLevel)
            Log("Have to read the config.txt");
        ReadConfig(1);
    }

    struct UUSOCKET socket, *s = &socket;
    enum WSKEY_STATES keystate = WSKEY_INIT;

    // connect with wskey
    UUInitSocket(s, INVALID_SOCKET);

    // force using proxies or try a normal connection
    if (debugLevel > 9998)
        Log("Force proxy connection: %s", (wsKeyForceProxy ? "TRUE" : "FALSE"));

    int error = wsKeyForceProxy
                    ? 1
                    : UUConnectWithSource3(s, wsKeyServiceHost, wsKeyServicePort, WSKEY_FACILITY,
                                           NULL, wsKeyServiceSsl, LICENSE_SERVER_CERT_IDX);

    // attempt to verify the wskey
    int state = (error)
                    ? WSKeyVerifyUsingProxies(s, wsKeyServiceHost, wsKeyServicePort,
                                              wsKeyServiceSsl, license)
                    : WSKeyVerify(s, wsKeyServiceHost, wsKeyServicePort, wsKeyServiceSsl, license);

    return state;
}

static int writeLicense(struct WSKEYLICENSE *license) {
    int f;
    char *errorMsg;
    int success = TRUE;

    SetErrno(0);

    f = UUopenCreateFD(WskeyNewFileName, O_TRUNC | O_CREAT | O_WRONLY, defaultFileMode);
    if (f < 0) {
        if (errno != ENOENT) {
            success = FALSE;
            errorMsg = ErrorMessage(GetLastError());
            Log("License file write error \"%s\" at open in %s.", errorMsg, WskeyNewFileName);
            FreeThenNull(errorMsg);
        }
    } else {
        char expire[11];
        char revalidate[11];
        timeToStr(expire, license->expiry);
        timeToStr(revalidate, license->revalidate);

        success = writeLine(f, license->signature, success);
        success = writeLine(f, license->key, success);
        success = writeLine(f, license->token, success);
        success = writeLine(f, expire, success);
        success = writeLine(f, revalidate, success);

        if (UUcloseFD(f) < 0) {
            errorMsg = ErrorMessage(GetLastError());
            Log("License file close error \"%s\" in %s.", errorMsg, WskeyNewFileName);
            FreeThenNull(errorMsg);
            success = 0;
        }
    }

    if (success) {
        if (RenameWithReplace(WskeyNewFileName, WskeyFileName)) {
            errorMsg = ErrorMessage(GetLastError());
            Log("License file replace error \"%s\" in %s.", errorMsg, WskeyNewFileName);
            FreeThenNull(errorMsg);
            success = 0;
        }
    }

    // Clean up any lingering new key file
    unlink(WskeyNewFileName);

    return success;
}

/**
 * Only returns true if license could be read and the
 * license signature has verified.  However, the license
 * may be altered with corrupt data.
 */
BOOL WskeyReadLicense(struct WSKEYLICENSE *license, BOOL showFileError) {
    BOOL success = TRUE;
    int dateSize = 11;
    char date[dateSize];

    char fname[strlen(WskeyFileName)];
    strcpy(fname, WskeyFileName);
    int f = UUopenExistantFD(fname, O_RDONLY);

    if (f < 0) {
        if (showFileError) {
            char *errorMsg = ErrorMessage(GetLastError());
            Log("Unable to open %s: %s", fname, errorMsg);
            FreeThenNull(errorMsg);
        }
        success = FALSE;
    } else {
        success = readLine(f, license->signature, license->sigSize, success);
        success = readLine(f, license->key, license->keySize, success);
        success = readLine(f, license->token, license->tokenSize, success);
        success = readLine(f, date, dateSize, success);
        if (success)
            license->expiry = atoll(date);
        success = readLine(f, date, dateSize, success);
        if (success)
            license->revalidate = license->nextRevalidateAttempt = atoll(date);

        setNonce(license);

        if (!success) {
            Log("Contents of %s are invalid", fname);
        }
    }

    UUcloseFD(f);

    // we read the file, but signature could still be bad
    success = success && (rsaVerifySignature(license->signature, license) == WSKEY_AUTHORIZED);
    licenseValid = success && !isLicenseExpired(license);
    return success;
}

static int timeToStr(char *buffer, time_t time) {
    return sprintf(buffer, "%u", time);
}

static void setNonce(struct WSKEYLICENSE *license) {
    int sSize = 1000;
    char serialized[sSize];
    char buffer[sSize];

    // copy license
    strcpy(serialized, license->key);
    strcat(serialized, "-");

    // copy expire time
    timeToStr(buffer, license->expiry);
    strncat(serialized, buffer, strlen(buffer));
    strcat(serialized, "-");

    // copy revalidate time
    *buffer = '\0';
    timeToStr(buffer, license->revalidate);
    strncat(serialized, buffer, strlen(buffer));
    strcat(serialized, "-");

    // copy the nonce
    *buffer = '\0';
    strcpy(buffer, license->token);
    strncat(serialized, buffer, strlen(buffer));

    strcpy(license->nonce, serialized);
}

BOOL isLicenseExpired(struct WSKEYLICENSE *license) {
    time_t now;
    UUtime(&now);

    // If we are passed the license expiration date, it is expired
    if (license->expiry < now) {
        return 1;
    }

    // If the revalidate value is later than if we computed it right now,
    // the clock may have been off or the timeout rules have been tightened and we treat it as
    // expired to force revalidation.
    if (license->revalidate > adjustedRevalidate(now + wskeyRecheckPeriod, license, myName)) {
        return 1;
    }

    // Not expired
    return 0;
}

/**
 * Verify a signature using RSA
 * @param signature A base64 encoded signature
 * @param license License with nonce set that was used to create signature.  Assumes
 * the nonce has been set by calling WSkeySetLicenseNonce(license) has been called.
 *
 * @see WSkeySetLicenseNonce
 */
static enum WSKEY_STATES rsaVerifySignature(char *signature, struct WSKEYLICENSE *license) {
    enum WSKEY_STATES result;
    BIO *bufio = NULL;
    RSA *pubKey = NULL;
    unsigned char *sigDecoded = NULL;
    char buffer[strlen(license->nonce) + strlen(sigPostfix) + 1];

    // Calling BIO_new_mem_buf with -1 as the second parameter insures the
    // memory BIO is read-only, so using this public key buffer directly by
    // casting it from const char * to char * is safe.
    bufio = BIO_new_mem_buf((char *)wskeyPubKey, -1);
    pubKey = PEM_read_bio_RSA_PUBKEY(bufio, 0, 0, 0);

    if (pubKey == NULL) {
        Log("Wskey signature check failed to load public key.");
        result = WSKEY_NOPUBKEY;
        goto Finished;
    }

    // decode the signature
    size_t sigSize = RSA_size(pubKey);
    sigDecoded = Decode64Binary(NULL, &sigSize, signature);

    strcpy(buffer, license->nonce);
    strcat(buffer, sigPostfix);

    unsigned char hash[SHA_DIGEST_LENGTH];
    SHA1((unsigned char *)buffer, strlen(buffer), hash);

    // 20 bytes = 160 bits, which is the size of the hash
    if (!RSA_verify(NID_sha1, hash, SHA_DIGEST_LENGTH, sigDecoded, sigSize, pubKey)) {
        result = WSKEY_BADSIG;
        goto Finished;
    }

    result = WSKEY_AUTHORIZED;

Finished:
    FreeThenNull(sigDecoded);
    if (pubKey) {
        RSA_free(pubKey);
        pubKey = NULL;
    }
    if (bufio) {
        BIO_free(bufio);
        bufio = NULL;
    }
    return result;
}

/**
 * TODO: re-add this once license is fully implemented.
 */
static void promptUserForKey(char *key, int size) {
    fflush(stdout);
    printf("Please enter license key: ");
    fflush(stdout);

    char buffer[size];
    fgets(buffer, size, stdin);

    char *pos;
    if ((pos = strchr(buffer, '\n')) != NULL)
        *pos = '\0';

    strncpy(key, buffer, size);

    printf("\n");
    fflush(stdout);
}

static char *createToken() {
    unsigned char buf[wskeyTokenSize];
    RAND_bytes(buf, wskeyTokenSize);
    return Encode64Binary(NULL, buf, wskeyTokenSize);
}

static BOOL writeLine(int handle, char *line, BOOL success) {
    if (!success)
        return FALSE;

    char *errorMsg;

    if (UUfputsFD(line, handle) < 0) {
        errorMsg = ErrorMessage(GetLastError());
        Log("License file write error \"%s\" in %s.", errorMsg, WskeyFileName);
        FreeThenNull(errorMsg);
        return FALSE;
    }

    if (UUfputcFD('\n', handle) < 0) {
        errorMsg = ErrorMessage(GetLastError());
        Log("License file write error \"%s\" in %s.", errorMsg, WskeyFileName);
        FreeThenNull(errorMsg);
        return FALSE;
    }

    return TRUE;
}

static BOOL readLine(int handle, char *buffer, int size, BOOL success) {
    if (!success)
        return FALSE;

    // the signature should be longest possible line + newline
    char line[WSKEY_SIGSIZE + 1];

    if (FileReadLine(handle, line, WSKEY_SIGSIZE + 1, NULL)) {
        strncpy(buffer, line, size);
    } else {
        Log("Failed to read license file line.");
        return FALSE;
    }
    return TRUE;
}

/**
 * Get next occurence matching the short day string (%a).
 * @see strftime
 * @return next coming time which matches the given day of the week. 0 on error.
 */
static time_t getNextShutdownTime(const time_t baseTime) {
    // Do not shift for non-prod periods for which next Tue-Thu at 9 AM may be inappropriate
    if (environment != 0) {
        return baseTime;
    }
    time_t next = baseTime;
    struct tm *tm = localtime(&next);
    // Normalize the time to 9 am local time
    tm->tm_hour = 9;
    tm->tm_min = 0;
    tm->tm_sec = 0;
    next = mktime(tm);
    int wday = tm->tm_wday;
    // now find next valid day to shutdown
    // we want expiration to always be in future so look
    // at least 1 day ahead
    for (;;) {
        next += ONE_DAY_IN_SECONDS;
        wday = (wday + 1) % 7;
        // Only set shutdown to occur mid-week on Tue through Thu
        if (wday >= 2 && wday <= 4) {
            return next;
        }
    }

    // never reached
    return 0;
}

/**
 * Get next time to revalidate adjusted to prevent EZproxy servers from all checking
 * in at the same time.
 */
static time_t adjustedRevalidate(time_t baseTime, struct WSKEYLICENSE *license, char *myName) {
    MD5_CTX md5Context;
    uint32_t md5Signature[4];
    uint32_t offset;

    // Do not shift time if recheck is less than a day as this will mess up dev configs
    if (wskeyRecheckPeriod < ONE_DAY_IN_SECONDS) {
        return baseTime;
    }

    // Compute an MD5 hash of the license key and hostname which will transform these values
    // into an even distribution, exclusive-or the parts together into a single 32-bit number,
    // then modulo up the resulting number in a seconds, minute, hour value for when
    // to make next request with goal of spacing out license update requests throughout
    // the day.
    MD5_Init(&md5Context);
    MD5_Update(&md5Context, (unsigned char *)license->key, strlen(license->key));
    MD5_Update(&md5Context, (unsigned char *)myName, strlen(myName));
    MD5_Final((unsigned char *)&md5Signature, &md5Context);
    offset = md5Signature[0] ^ md5Signature[1] ^ md5Signature[2] ^ md5Signature[3];

    struct tm rtm;
    struct tm *tm = UUlocaltime_r(&baseTime, &rtm);
    tm->tm_sec = offset % 60u;
    offset = offset / 60u;
    tm->tm_min = offset % 60u;
    offset = offset / 60u;
    tm->tm_hour = offset % 24u;
    baseTime = mktime(tm);
    return baseTime;
}

/**
 * We may not want the logs to get the key, so this allows us to say wether
 * or not to print a message with the key.
 */
static void errLogger(const char *message, const BOOL showLicense, const char *key) {
    char buffer[255];
    strcpy(buffer, message);
    if (showLicense && key != NULL && strlen(key)) {
        char *cat = " (%s).";
        strcat(buffer, cat);
        Log(buffer, key);

    } else {
        strcat(buffer, ".");
        Log(buffer);
    }
}

static void printErrorState(struct WSKEYLICENSE *license,
                            enum WSKEY_STATES state,
                            BOOL showLicense) {
    time_t now;
    char *commMsg = "Unable to contact the OCLC license server, please contact " SUPPORTEMAIL
                    " if you have questions or concerns.";

    // buffers used by some messages
    char tbuffer[20];
    char tbuffer2[20];
    char tbuffer3[20];

    switch (state) {
    // good states
    case WSKEY_AUTHORIZED:
        errLogger("Key is authorized", showLicense, license->key);
        break;
    case WSKEY_UNAUTHORIZED:
        errLogger("Key is unauthorized", showLicense, license->key);
        break;

    // starting state, should never end up here
    case WSKEY_INIT:
        errLogger("License was in initialize state. Internal error", showLicense, license->key);
        break;

    // signature has been set on the key
    case WSKEY_SIG_SET:
        Log("License signature was set successfully. Signature may still be invalid.");
        break;
    // error states
    case WSKEY_BADXML:
        Log("%s [bad xml response].", commMsg);
        break;
    case WSKEY_BADXPATH:
        Log("%s [xpath failed].", commMsg);
        break;
    case WSKEY_SERVERDOWN:
        Log("%s [service down].", commMsg);
        break;
    case WSKEY_NOT200:
        Log("%s [non 200 header response].", commMsg);
        break;
    case WSKEY_SOCKETERR:
        Log("%s [socket error].", commMsg);
        break;
    case WSKEY_NOSIG:
        Log("%s [no signature].", commMsg);
        break;
    case WSKEY_BADSIG:
        Log("Invalid signature.", commMsg);
        break;
    case WSKEY_NOPUBKEY:
        Log("No public key.  Cannot verify signature.", commMsg);
        break;
    case WSKEY_CLOCK_ERROR:
        UUtime(&now);
        Log("System clock and/or license times are corrupt.\n\tCurrent Time: %s\n\tNext Check  : "
            "%s\n\tExpiration  : %s",
            hTime(now, tbuffer, 20), hTime(license->nextRevalidateAttempt, tbuffer2, 20),
            hTime(license->expiry, tbuffer3, 20));
        break;
    case WSKEY_EXPIRED:
        UUtime(&now);
        Log("License validation call determined that your license key expired on %s, it is now %s",
            hTime(license->expiry, tbuffer2, 20), hTime(now, tbuffer, 20));
        break;
    // catchall
    case WSKEY_CATCH_ALL:
        Log("Unknown license error.  Please contact " SUPPORTEMAIL
            " if you have questions or concerns.");
        break;
    default:
        Log("Unknown license state reached.");
        break;
    }
}

static void writeMessages(struct WSKEYLICENSE *license, time_t now, BOOL revalidated) {
    char tbuffer[20];

    // Since the timestamps are unsigned, cast both to double  in case results are negative.
    double daysSinceValidation = difftime(now, license->revalidate) / ONE_DAY_IN_SECONDS;
    double daysUntilExpiration = difftime(license->expiry, now) / ONE_DAY_IN_SECONDS;
    double range = difftime(license->expiry, license->revalidate) / ONE_DAY_IN_SECONDS;
    double pctUntilExpiration = daysUntilExpiration / range;

    BOOL warn = FALSE;
    BOOL danger = FALSE;
    const char *msgLevel = "*** ALERT:";
    // Danger if license within 25% of expiration
    if (pctUntilExpiration <= 0.25) {
        msgLevel = "*** DANGER:";
        warn = TRUE;
        danger = TRUE;
        // Warning if license within 75% of expiration
    } else if (pctUntilExpiration <= 0.75) {
        msgLevel = "*** WARNING:";
        warn = TRUE;
    }

    if (danger) {
        Log("%s EZproxy has been unable to validate your license key with OCLC for %.1f days.",
            msgLevel, daysSinceValidation);
    }

    if (warn) {
        Log("%s EZproxy will stop if it cannot validate your license key with OCLC within %.1f "
            "days by %s.",
            msgLevel, daysUntilExpiration, hTime(license->expiry, tbuffer, 20));
        license->warned = TRUE;
    }

    // shutdown imminent
    if (danger) {
        // Log("%s Validation will cease retry and EZproxy will shut down in %.1f days.", msgLevel,
        // daysUntilExpiration);
        Log("%s Please contact " SUPPORTEMAIL " immediately.", msgLevel);
    } else if (warn) {
        Log("%s If you have concerns please contact " SUPPORTEMAIL ".", msgLevel);
    }

    if (revalidated) {
        Log("License key revalidated with OCLC.");
    }
}

static char *hTime(const time_t time, char *buffer, int bSize) {
    struct tm tm;
    strftime(buffer, bSize, "%Y-%m-%d %H:%M:%S", UUlocaltime_r(&time, &tm));
    return buffer;
}

BOOL WskeyIsBeta() {
    return BETA;
}

BOOL WskeyIsBetaExpired() {
    // If we aren't beta, we don't expire
    if (!BETA) {
        return FALSE;
    }

    time_t now;
    struct tm *tm, rtm;
    int year, month, day, hour;

    UUtime(&now);
    tm = UUlocaltime_r(&now, &rtm);
    year = tm->tm_year + 1900;
    month = tm->tm_mon + 1;
    day = tm->tm_mday;
    hour = tm->tm_hour;

    if (year < BETA_YEAR) {
        return FALSE;
    }
    if (year > BETA_YEAR) {
        return TRUE;
    }
    if (month < BETA_MONTH) {
        return FALSE;
    }
    if (month > BETA_MONTH) {
        return TRUE;
    }
    if (day < BETA_DAY) {
        return FALSE;
    }
    if (day > BETA_DAY) {
        return TRUE;
    }
    // Have beta shutdown after 9 am local time
    if (hour < 9) {
        return FALSE;
    }
    return TRUE;
}

struct WSKEYLICENSE *WskeyActiveLicense() {
    return activeLicense;
}
