#ifndef __VARIABLES_H__
#define __VARIABLES_H__

#include "common.h"

struct VARIABLES;
struct TRANSLATE;
struct FILEREADLINEBUFFER;

#define VARIABLESMAXLEN 8192

#define SESSIONVARPREFIX "session:"

struct VARIABLES *VariablesNew(char *name, BOOL shared);
BOOL VariablesSetValue(struct VARIABLES *v, const char *var, const char *val);
BOOL VariablesSetValueAppend(struct VARIABLES *v, const char *var, const char *val);
void VariablesSetOrDeleteValue(struct VARIABLES *v, const char *var, const char *val);
char *VariablesGetValue(struct VARIABLES *v, const char *var);
char *VariablesGetValueByGroupMask(struct VARIABLES *v, const char *var, GROUPMASK gm);
void VariablesDelete(struct VARIABLES *v, const char *var);
BOOL VariablesMerge(struct VARIABLES *dst, struct VARIABLES *src, const char *wildFilter);
void VariablesReset(struct VARIABLES *v);
void VariablesFree(struct VARIABLES **v);
void VariablesLoad(struct VARIABLES *v, int f, struct FILEREADLINEBUFFER *frb);
void VariablesSave(struct VARIABLES *v, int f);
int VariablesForAllByGroupMask(struct TRANSLATE *t,
                               struct VARIABLES *v,
                               void *context,
                               GROUPMASK gm,
                               void (*callBack)(struct TRANSLATE *t,
                                                struct VARIABLES *v,
                                                void *context,
                                                const char *name,
                                                const char *value,
                                                int count));
int VariablesForAll(struct TRANSLATE *t,
                    struct VARIABLES *v,
                    void *context,
                    void (*callBack)(struct TRANSLATE *t,
                                     struct VARIABLES *v,
                                     void *context,
                                     const char *name,
                                     const char *value,
                                     int count));
int VariablesTable(struct TRANSLATE *t,
                   struct VARIABLES *v,
                   const char *filter,
                   const char *header);
void AdminVariables(struct TRANSLATE *t, char *query, char *post);
BOOL VariablesAggregateTest(struct USERINFO *uip,
                            struct VARIABLES *v,
                            struct VARIABLES *sessionVariables,
                            const char *var,
                            const char *val,
                            enum AGGREGATETESTOPTIONS ato,
                            struct VARIABLES *rev);
int VariablesCount(struct VARIABLES *v, const char *var);
char *VariablesHTMLOptions(struct VARIABLES *v, const char *var, const char *defaultValue);
int VariablesSendHttpHeaders(struct TRANSLATE *t);

#endif  // __VARIABLES_H__
