#ifndef __USRHTTPBASIC_H__
#define __USRHTTPBASIC_H__

#include "common.h"

/* Returns 0 if valid, 1 if not, 3 if unable to connect to pop host */
int FindUserHttpBasic(char *url, struct USERINFO *uip, struct sockaddr_storage *pInterface);

#endif  // __USRHTTPBASIC_H__
