#define __UUFILE__ "adminmygroups.c"

#include "common.h"
#include "uustring.h"

#include "adminmygroups.h"

void AdminMygroups(struct TRANSLATE *t, char *query, char *post) {
    struct GROUP *g;
    struct UUSOCKET *s = &t->ssocket;
    const int AMADMIN = 0;

    struct FORMFIELD ffs[] = {
        {"admin", NULL, 0, 0, 10},
        {NULL, NULL, 0, 0}
    };

    FindFormFields(query, post, ffs);

    if (ffs[AMADMIN].value)
        AdminHeader(t, 0, "View Your Group Memberships");
    else
        HTMLHeader(t, htmlHeaderNoCache);

    UUSendZ(s, "<p>You are a member of the following group(s):</p>\n<ul>\n");

    for (g = groups; g; g = g->next) {
        if (GroupMaskOverlap(g->gm, (t->session)->gm)) {
            UUSendZ(s, "<li>");
            SendHTMLEncoded(s, g->name);
            UUSendZ(s, "</li>");
        }
    }
    UUSendZ(s, "</ul>\n");

    if (ffs[AMADMIN].value)
        AdminFooter(t, 0);
}
