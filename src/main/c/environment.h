/**
 * environment.h
 *
 *  Created on: Feb 17, 2022
 *      Author: nebletta
 */

#ifndef ENVIRONMENT_H_
#define ENVIRONMENT_H_

#define ENVIRONMENTDIR "environment"
#define ENVIRONMENTDB ENVIRONMENTDIR DIRSEP "environment_v1.db"

#include "common.h"
#include "sqlite3.h"
#include "sql.h"

sqlite3 *EnvironmentGetSharedDatabase();
sqlite3 *EnvironmentGetDatabase();
static void EnvironmentOpenDatabase(sqlite3 **db);
static void EnvironmentUpgradeDatabase();
static void EnvironmentInit();
void EnvironmentStart();

#endif /* ENVIRONMENT_H_ */
