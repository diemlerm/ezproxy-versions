/**
 * /status admin URL to view current server status.
 */

#define __UUFILE__ "adminstatus.c"

#include "usr.h"
#include "common.h"
#include "uustring.h"

#include "adminstatus.h"
#include "adminmv.h"

void AdminStatusIpTypes(struct TRANSLATE *t, int *ipIdx, int limit) {
    struct UUSOCKET *s = &t->ssocket;
    struct GROUP *g;
    char ipTypeAddress[INET6_ADDRSTRLEN];
    struct IPTYPE *l;

    if (*ipIdx >= limit)
        return;

    UUSendZ(s, "<tr><td>&nbsp;</td><td colspan='4'>\n");
    for (l = ipTypes + *ipIdx; *ipIdx < limit; (*ipIdx)++, l++) {
        WriteLine(s, "<div>%s %s", IPACCESSNAMES[l->ipAccess],
                  ipToStr(&l->start, ipTypeAddress, sizeof ipTypeAddress));

        if (!is_ip_equal(&l->start, &l->stop))
            WriteLine(s, "-%s", ipToStr(&l->stop, ipTypeAddress, sizeof ipTypeAddress));

        if (l->ipAccess == IPAAUTO && cGroups != 1) {
            UUSendZ(s, " groups");
            for (g = groups; g; g = g->next) {
                if (GroupMaskOverlap(g->gm, l->gmAuto)) {
                    UUSendZ(s, " ");
                    UUSendZ(s, g->name);
                }
            }
        }
        UUSendZ(s, "</div>\n");
    }
    UUSendZ(s, "</td></tr>\n");
}

static void AdminStatusHttpHeaders(struct TRANSLATE *t,
                                   struct HTTPHEADER *hh,
                                   BOOL global,
                                   BOOL ignoreGlobal) {
    struct UUSOCKET *s = &t->ssocket;

    if (ignoreGlobal) {
        UUSendZ(s, "<div>HTTPHeader -ignoreGlobal</div>");
    }
    for (; hh; hh = hh->next) {
        UUSendZ(s, "<div>HTTPHeader ");
        if (global) {
            UUSendZ(s, "-global ");
        }
        SendHTMLEncoded(s, HTTPHEADER_DIRECTION_NAMES[hh->direction]);
        UUSendZ(s, " ");
        SendHTMLEncoded(s, HTTPHEADER_PROCESSING_NAMES[hh->processing]);
        UUSendZ(s, " ");
        SendHTMLEncoded(s, hh->header);
        if (hh->expr) {
            UUSendZ(s, " ");
            SendHTMLEncoded(s, hh->expr);
        }
        UUSendZ(s, "</div>\n");
    }
}

static void AdminStatusMimeFilters(struct TRANSLATE *t, struct DATABASE *b) {
    struct UUSOCKET *s = &t->ssocket;
    struct MIMEFILTER *mf = b->mimeFiltersUser;

    for (; mf; mf = mf->next) {
        UUSendZ(s, "<div>MimeFilter ");
        SendHTMLEncoded(s, mf->mime);
        UUSendZ(s, " ");
        SendHTMLEncoded(s, mf->uri);
        UUSendZ(s, " ");
        if (mf->noDefault) {
            UUSendZ(s, "-");
        }
        SendHTMLEncoded(s, MIMEACTIONNAMES[mf->action]);
        UUSendZ(s, "</div>\n");
    }
}

static void AdminStatusPatterns(struct TRANSLATE *t, struct DATABASE *b) {
    struct UUSOCKET *s = &t->ssocket;
    struct PATTERNS *pp;

    if (b->patterns.pattern) {
        struct PATTERN *pp;
        for (pp = b->patterns.pattern; pp; pp = pp->next) {
            UUSendZ(s, "<div>Find ");
            SendHTMLEncoded(s, pp->matchString);
            UUSendZ(s, "</div>\n<div>Replace ");
            SendHTMLEncoded(s, pp->originalReplaceString);
            if (pp->originalReplaceString != pp->replaceString) {
                UUSendZ(s, "</div>\n<div>Use ");
                SendHTMLEncoded(s, pp->replaceString);
            }
            UUSendZ(s, "</div>\n");
        }
    }
}

int AdminPortSort(const void *v1, const void *v2) {
    struct HOST **h1 = (struct HOST **)v1;
    struct HOST **h2 = (struct HOST **)v2;
    return (int)(*h1)->myPort - (*h2)->myPort;
}

BOOL AdminStatusMatchingDb(char *db, struct DATABASE *b) {
    int i;
    char *p;

    if (b == NULL)
        return 0;

    i = b - databases;

    for (p = db; p && *p;) {
        for (; *p && !IsDigit(*p); p++)
            ;
        if (*p) {
            if (i == atoi(p))
                return 1;
            if (p = strchr(p, '.'))
                p++;
        }
    }
    return 0;
}

void AdminStatus(struct TRANSLATE *t, char *query, char *post) {
    int rows;
    int i, j;
    int ipIdx;
    struct UUSOCKET *s = &t->ssocket;
    struct HOST *h;
    struct SESSION *e;
    struct DATABASE *b;
    struct INDOMAIN *m;
    struct TRANSLATE *t1;
    struct PEER *c;
    char host[1024];
    int count = 0;
    char sport[7]; /* Need to allow for possible &nbsp; value */
    char da[MAXASCIIDATE], dc[MAXASCIIDATE], dr[MAXASCIIDATE];
    char *userNames = NULL;
    char buffer[533];
    BOOL pendingReap;
    struct hostent *hp;
    struct TRACKSOCKET *tsp;
    struct LOGINPORT *lp;
    const int FFHOSTNAME = 0;
    const int FFEXTENDED = 1;
    const int FFSESSION = 2;
    const int FFCONFIRM = 3;
    const int FFMAINT = 4;
    const int FFSORT = 5;
    const int FFDB = 6;
    const int FFSESSIONUSER = 7;
    struct FORMFIELD ffs[] = {
        {"hostname",    NULL, 0, 0, 0},
        {"extended",    NULL, 0, 0, 0},
        {"session",     NULL, 0, 0, 0},
        {"confirm",     NULL, 0, 0, 0},
        {"maint",       NULL, 0, 0, 0},
        {"sort",        NULL, 0, 0, 0},
        {"db",          NULL, 0, 0, 0},
        {"sessionuser", NULL, 0, 0, 0},
        {NULL,          NULL, 0, 0, 0}
    };
    BOOL lookupHostname = 0;
    BOOL extended = 0;
    BOOL sorting = 0;
    char *termSession;
    char *termUser;
    BOOL canReturn = 1;
    char ipBuffer[INET6_ADDRSTRLEN];
    int orphans = 0;
    int proxyByPortMappings = 0;
    char *maint;
    time_t now, past30;
    int past30Count = 0;
    int trs = 0;
    char *sortable = " sortable";
    char *currentProxyHost = NULL;
    char *currentProxySslHost = NULL;
    PORT currentProxyPort = 0;
    PORT currentProxySslPort = 0;
    char dbIdx[80];
    static time_t lastPortReset = 0;
    char *db = NULL;
    char comma[] = ",";
    enum OPTIONCOOKIE lastOptionCookie = OCALL;
    BOOL update = t->session->priv || GroupMaskOverlap(gmAdminStatusUpdate, t->session->gm);

    UUtime(&now);
    past30 = now - THIRTY_DAYS_IN_SECONDS;
    if (lastPortReset < past30)
        lastPortReset = 0;

    FindFormFields(query, post, ffs);
    lookupHostname = ffs[FFHOSTNAME].value != NULL;
    extended = ffs[FFEXTENDED].value != NULL;
    if (extended)
        sortable = "";
    sorting = ffs[FFSORT].value != NULL;
    db = ffs[FFDB].value;

    if (termUser = ffs[FFSESSIONUSER].value) {
        AdminBaseHeader(t, 0, "Session Status", " | <a href='/status'>Server status</a>");
        if (ffs[FFCONFIRM].value && strcmp(ffs[FFCONFIRM].value, "yes") == 0) {
            for (i = 0, e = sessions; i < cSessions; i++, e++) {
                if (e->active == 0)
                    continue;
                if (e->anonymous)
                    continue;
                if (usageLogUser & ULUSTATUSUSER) {
                    if (e->logUserFull && *e->logUserFull) {
                        if (strcmp(e->logUserFull, termUser) == 0) {
                            char key[MAXKEY + 1];
                            strcpy(key, e->key);
                            StopSession(e, "Administrator");
                            UUSendZ(s, "<p>Session ");
                            SendHTMLEncoded(s, key);
                            UUSendZ(s, " has been terminated.</p>\n");
                        }
                    }
                }
            }
        } else {
            char adminKey[MAXKEY + 1];
            int numberOfSessions = 0;
            char *logUserBrief = NULL;
            for (i = 0, e = sessions; i < cSessions; i++, e++) {
                if (e->active == 0)
                    continue;
                if (e->anonymous)
                    continue;
                if (usageLogUser & ULUSTATUSUSER) {
                    if (e->logUserFull && *e->logUserFull) {
                        if (strcmp(e->logUserFull, termUser) == 0) {
                            if (numberOfSessions == 0) {
                                strcpy(adminKey, e->adminKey);
                                if (e->logUserBrief && *e->logUserBrief) {
                                    if (!(logUserBrief = strdup(e->logUserBrief)))
                                        PANIC;
                                }
                            }
                            numberOfSessions++;
                        }
                    }
                }
            }
            UUSendZ(s, "<table class='bordered-table'>\n");
            WriteLine(s,
                      "<tr><th scope='row'>Session(s)&nbsp;</th><td align='right'>%d</td></tr>\n",
                      numberOfSessions);
            if ((usageLogUser & ULUSTATUSUSER) && logUserBrief && *logUserBrief) {
                UUSendZ(s, "<tr><th scope='row'>Username</th><td>");
                SendHTMLEncoded(s, logUserBrief);
                UUSendZ(s, "</td></tr>\n");
                FreeThenNull(logUserBrief);
            }
            UUSendZ(s, "</table>\n<p>");

            if (e == t->session) {
                UUSendZ(s, "<p>WARNING: This is your current session.</p>\n");
            }

            UUSendZ(s, "<p>\n");
            if (update) {
                if (numberOfSessions > 1) {
                    UUSendZ(s, "<a href='/status?sessionuser=");
                    SendUrlEncoded(s, termUser);
                    UUSendZ(s, "&confirm=yes'>Terminate all sessions</a><br>\n");
                } else if (numberOfSessions == 1) {
                    UUSendZ(s, "<a href='/status?session=");
                    SendUrlEncoded(s, adminKey);
                    UUSendZ(s, "&confirm=yes'>Terminate session</a><br>\n");
                }
            }
            if (numberOfSessions == 1 &&
                (t->session->priv || GroupMaskOverlap(gmAdminVariables, e->gm))) {
                UUSendZ(s, "<a href='/variables?session=");
                SendUrlEncoded(s, adminKey);
                UUSendZ(s, "'>View session variables</a><br>\n</p>\n");
            }
            UUSendZ(s, "</p>\n");
        }
        AdminFooter(t, 0);
        return;
    }

    if (termSession = ffs[FFSESSION].value) {
        AdminBaseHeader(t, 0, "Session Status", " | <a href='/status'>Server status</a>");
        if (!(e = FindSessionByAdminKey(termSession))) {
            UUSendZ(s, "<p>Session ");
            SendHTMLEncoded(s, termSession);
            UUSendZ(s, " does not exist.</p>\n");
        } else if (e->anonymous) {
            UUSendZ(s, "<p>Session ");
            SendHTMLEncoded(s, termSession);
            UUSendZ(s,
                    " is an anonymous session used to support AnonymousURL and/or SingleSession.  "
                    "It cannot be terminated.</p>");
        } else {
            if (update && ffs[FFCONFIRM].value && strcmp(ffs[FFCONFIRM].value, "yes") == 0) {
                StopSession(e, "Administrator");
                canReturn = e != t->session;
                UUSendZ(s, "<p>Session ");
                SendHTMLEncoded(s, e->key);
                UUSendZ(s, " has been terminated.</p>\n");
            } else {
                UUSendZ(s, "<table class='bordered-table'>\n");
                WriteLine(s, "<tr><th scope='row'>Session&nbsp;</th><td>%s</td></tr>\n", e->key);
                if ((usageLogUser & ULUSTATUSUSER) && e->logUserBrief && *e->logUserBrief) {
                    UUSendZ(s, "<tr><th scope='row'>Username</th><td>");
                    SendHTMLEncoded(s, e->logUserBrief);
                    UUSendZ(s, "</td></tr>\n");
                }
                WriteLine(s, "<tr><th scope='row'>Logged in</th><td>%s</td></tr>\n",
                          ASCIIDate(&e->created, da));
                WriteLine(s, "<tr><th scope='row'>Last authenticated</td><th>%s</td></tr>\n",
                          ASCIIDate(&e->lastAuthenticated, da));
                WriteLine(s, "<tr><th scope='row'>Last accessed</td><td>%s</th></tr>\n",
                          ASCIIDate(&e->accessed, da));
                WriteLine(s, "<tr><th scope='row'>IP</th><td>%s</td></tr>\n",
                          ipToStr(&e->sin_addr, ipBuffer, sizeof ipBuffer));

                if (AnyLocations()) {
                    struct LOCATION location;

                    FindLocationByNetwork(&location, &e->sin_addr);
                    UUSendZ(s, "<tr><th scope='row'>Location</th><td>");
                    SendHTMLEncoded(s, location.countryCode);
                    if (location.region[0]) {
                        UUSendZ(s, " ");
                        SendHTMLEncoded(s, location.region);
                    }
                    if (location.city[0]) {
                        UUSendZ(s, " ");
                        SendHTMLEncoded(s, location.city);
                    }
                    UUSendZ(s, "</td></tr>\n");
                }

                WriteLine(s, "<tr><th scope='row'>Source IP</th><td>%s</td></tr>\n",
                          ipToStr(&e->ipInterface, ipBuffer, sizeof ipBuffer));

                if (e->userObjectTicket[0]) {
                    UUSendZ(s, "<tr><th scope='row'>User Object Ticket</th><td>");
                    SendHTMLEncoded(s, e->userObjectTicket);
                    UUSendZ(s, "</td></tr>\n");
                }

                UUSendZ(s, "</table>\n<p>");

                if (e == t->session) {
                    UUSendZ(s, "<p>WARNING: This is your current session.</p>\n");
                }

                UUSendZ(s, "<p>\n");
                // termSession is sanitized by here, so follow use without encoding is safe
                if (update) {
                    WriteLine(
                        s, "<a href='/status?session=%s&confirm=yes'>Terminate session</a><br>\n",
                        termSession);
                }
                if (t->session->priv || GroupMaskOverlap(gmAdminVariables, e->gm)) {
                    WriteLine(s, "<a href='/variables?session=%s'>View session variables</a><br>\n",
                              termSession);
                    UUSendZ(s, "</p>\n");
                }
                UUSendZ(s, "</p>\n");
            }
        }
        AdminFooter(t, 0);
        return;
    }

    if (update && (maint = ffs[FFMAINT].value) != NULL && *maint == 'm') {
        int m = atoi(maint + 1);
        int count = 0;

        AdminBaseHeader(t, 0, "Host Maintenance", " | <a href='/status'>Server status</a>");

        if (m == 1 || m == 2) {
            for (i = 1, h = hosts + i; i < cHosts; i++, h++) {
                if (h == mvHost || h == mvHostSsl)
                    continue;
                if ((m == 1 && h->myDb == NULL) ||
                    (m == 2 && h->accessed < past30 && h->referenced < past30)) {
                    h->dontSave = 1;
                    count++;
                }
            }
            if (count == 0) {
                UUSendZ(s, "<p>No hosts meet this criterion.</p>\n");
            } else {
                SaveHosts();
                UUSendZ(s,
                        "<p>To complete the removal of these hosts, you must now <a "
                        "href='/restart'>restart EZproxy</a>.</p>\n");
            }
        }

        if (m == 3 && optionProxyType == PTHOST) {
            rebuildMainMaster = 2;
            for (i = 1, h = hosts + i; i < cHosts; i++, h++) {
                if (h->myPort != 0 && h->forceMyPort == 0) {
                    h->stopListener = 1;
                }
            }
            rebuildMainMaster = 1;
            UUSendZ(s, "<p>The ports listening for proxy by port have been stopped.\n");
        }

        if (m == 4) {
            for (i = 0, h = hosts + i; i < cHosts; i++, h++) {
                h->referenced = h->accessed = h->references = h->accesses = 0;
            }
            UUSendZ(s, "<p>Accessed and referenced dates and counts have been reset.</p>\n");
            /* Now that these are reset, all ports could be freed, but that's not a good idea,
               so we'll suppress that option for 30 days
            */
            lastPortReset = now;
        }

        if (m == 5) {
            struct HOST **hhStart, **hhEnd, **hh;
            int max = cHosts;
            PORT lowPort = firstPort;
            SOCKET oldSocket;
            SOCKET newSocket = INVALID_SOCKET;
            int failed = 0;
            int reassigned = 0;

            if (!(hhStart = hhEnd = calloc(max, sizeof(*hhStart))))
                PANIC;
            for (i = 0, h = hosts + i; i < max; i++, h++) {
                if (h->myPort) {
                    *hhEnd = h;
                    hhEnd++;
                }
            }

            rebuildMainMaster = 2;

            if (hhStart != hhEnd) {
                qsort(hhStart, hhEnd - hhStart, sizeof(*hhStart), AdminPortSort);
                RWLAcquireWrite(&rwlHosts);
                for (hh = hhStart; hh < hhEnd; hh++) {
                    if ((*hh)->myPort >= firstPort)
                        break;
                    for (; lowPort = NextPortFrom(lowPort); lowPort++) {
                        newSocket = StartListener(lowPort, &defaultIpInterface, hostSocketBacklog);
                        if (newSocket != INVALID_SOCKET)
                            break;
                        if (socket_errno == WSAEADDRINUSE)
                            continue;
                        failed = 1;
                        break;
                    }
                    oldSocket = (*hh)->socket;
                    (*hh)->socket = newSocket;
                    (*hh)->myPort = lowPort;
                    closesocket(oldSocket);
                    lowPort++;
                    reassigned++;
                }
                for (hh = hhEnd - 1; failed == 0 && hh >= hhStart; hh--) {
                    if ((*hh)->myPort > lowPort) {
                        for (; lowPort = NextPortFrom(lowPort); lowPort++) {
                            if ((*hh)->myPort <= lowPort) {
                                newSocket = INVALID_SOCKET;
                                break;
                            }
                            newSocket =
                                StartListener(lowPort, &defaultIpInterface, hostSocketBacklog);
                            if (newSocket != INVALID_SOCKET)
                                break;
                            if (socket_errno == WSAEADDRINUSE)
                                continue;
                            failed = 1;
                            break;
                        }
                        if (newSocket == INVALID_SOCKET)
                            break;
                        oldSocket = (*hh)->socket;
                        (*hh)->socket = newSocket;
                        (*hh)->myPort = lowPort;
                        closesocket(oldSocket);
                        reassigned++;
                    }
                }
                RWLReleaseWrite(&rwlHosts);
            }

            rebuildMainMaster = 1;

            FreeThenNull(hhStart);
            if (failed) {
                UUSendZ(
                    s,
                    "<p>Error encountered while reassigning ports; please try again later.</p>\n");
                SaveHosts();

            } else {
                if (reassigned) {
                    WriteLine(s, "<p>%d port%s reassigned.</p>\n", reassigned,
                              SIfNotOne(reassigned));
                    SaveHosts();
                } else
                    UUSendZ(s,
                            "<p>There were no gaps in the currently assigned ports so no ports "
                            "were reassigned.</p>\n");
            }
        }

        AdminFooter(t, 0);
        return;
    }

    if (update && db) {
        AdminBaseHeader(t, 0, "Database Conflict Detail",
                        " | <a href='/conflicts'>Check Database Conflicts</a>");
        goto ShowDatabases;
    }

    AdminHeader(t, (sorting ? AHSORTABLE : 0), "View Server Status");

    UUSendZ(s, "<p>");
    AdminStarted(s);
    UUSendZ(s, "</p>");

    if (!licenseValid) {
        WriteLine(s,
                  "<p class='failure'>This server has license issues. Please check messages.txt "
                  "for further information.</p>\n");
    }

    buffer[0] = 0;

    AdminCheckMV(t);

    UUSendZ(s, "<form action='/status' method='get'>\n");
    UUSendZ(s, "Options:&nbsp;&nbsp;&nbsp;");
    WriteLine(s,
              "<label><input name='hostname' type='checkbox'%s> Show \"From\" "
              "hostnames</label>&nbsp;&nbsp;&nbsp;",
              lookupHostname ? " checked" : "");
    WriteLine(s,
              "<label><input name='extended' type='checkbox'%s> Include extended "
              "information</label>&nbsp;&nbsp;&nbsp;",
              extended ? " checked='checked'" : "");
    WriteLine(
        s,
        "<label><input name='sort' type='checkbox'%s%s> Enable Sorting</label>&nbsp;&nbsp;&nbsp;",
        (sorting ? " checked='checked'" : ""),
        (optionDisableSortable || t->isMacMSIE ? " disabled='disabled'" : ""));
    UUSendZ(s, "<input type='submit' value='Update'>\n");
    UUSendZ(s, "</form>\n");

    UUSendZ(s,
            "<p><a href='#databases'>Databases</a>&nbsp;| <a "
            "href='#loginports'>Login&nbsp;Ports</a>&nbsp;| <a href='#hosts'>Hosts</a>&nbsp;| <a "
            "href='#hostmaintenance'>Host&nbsp;Maintenance</a>&nbsp;| <a "
            "href='#miscellaneous'>Miscellaneous</a></p>\n");

    if (cPeers) {
        UUSendZ(s, "<p>Cluster<p>\n");
        UUSendZ(s,
                "<table class='bordered-table sortable' id='clustertable'><tr align='center'><th "
                "scope='col'>Node</th><th scope='col'>Status</th><th scope='col'>Last "
                "Communication</th></tr>\n");
        for (i = 0, c = peers; i < cPeers; i++, c++) {
            ASCIIDate(&c->lastHeardFrom, da);
            WriteLine(s, "<tr id='trs%d'><td>%s:%d<td>%s<td>%s</tr>", trs++, c->peerName,
                      get_port(&c->peerAddress), peerStatusName[c->peerStatus], da);
        }
        UUSendZ(s, "</table>\n");
    }

    if (haPeersValid) {
        struct HAPEER *hap;
        UUSendZ(s,
                "<table class='bordered-table sortable' id='hatable'><caption>High Availability "
                "Peers</caption><tr align='center'><th scope='col'>Server</th><th "
                "scope='col'>Status</th><th scope='col'>Last Communication</th></tr>\n");
        for (hap = haPeers; hap; hap = hap->next) {
            ASCIIDate(&hap->lastHeardFrom, da);
            WriteLine(s, "<tr id='trs%d'><td>%s</td><td>%s</td><td>%s</td></tr>", trs++,
                      hap->haPeerName, haPeerStatusName[hap->haPeerStatus], da);
        }
        UUSendZ(s, "</table>\n");
    }

    pendingReap = 0;

    // User Sessions Table

    // Get List of userNames that have a session that could be terminated
    if (!(userNames = malloc(mSessions * (MAXUSERLEN + 1) + 2)))
        PANIC;
    *userNames = 0;
    for (i = 0, e = sessions; i < cSessions; i++, e++) {
        if (e->active == 0)
            continue;
        if (usageLogUser & ULUSTATUSUSER) {
            if (e->logUserFull && *e->logUserFull) {
                if (strlen(userNames) == 0) {
                    strcpy(userNames, "\n");
                    strcat(userNames, e->logUserFull);
                    strcat(userNames, "\n");
                } else {
                    char newUserName[MAXUSERLEN + 2] = "\n";
                    strcat(newUserName, e->logUserFull);
                    strcat(newUserName, "\n");
                    if (strstr(userNames, newUserName) == NULL) {
                        strcat(userNames, e->logUserFull);
                        strcat(userNames, "\n");
                    }
                }
            }
        }
    }

    if (strlen(userNames) > 0) {
        UUSendZ(s,
                "<table class='bordered-table sortable' id='usersessionstable'>\n<caption>User "
                "Summaries</caption><tr align='center'><th scope='col'>Username</th><th "
                "scope='col' class='sortnumeric'>Number of Sessions</th><th scope='col' "
                "class='sortnumeric'>Longest Session "
                "(minutes)</th></tr>\n");
        // Remove first and last \n
        memmove(userNames, userNames + 1, strlen(userNames));
        userNames[strlen(userNames) - 1] = 0;

        char *str;
        char delim[] = "\n";
        char *token = strtok_r(userNames, delim, &str);
        while (token != NULL) {
            UUSendZ(s, "<tr>\n");
            long numberOfSessions = 0;
            double longestSession = 0;
            for (i = 0, e = sessions; i < cSessions; i++, e++) {
                if (e->active == 0)
                    continue;
                if (e->anonymous)
                    continue;
                if (usageLogUser & ULUSTATUSUSER) {
                    if (e->logUserFull && *e->logUserFull) {
                        if (strcmp(e->logUserFull, token) == 0) {
                            numberOfSessions++;
                            double sessionLength =
                                difftime(e->accessed, e->created);  // Time in seconds
                            if (sessionLength > longestSession) {
                                longestSession = sessionLength;
                            }
                        }
                    }
                }
            }
            WriteLine(s, "<td><a href='/status?sessionuser=%s'>%s</a></td>", token, token);
            WriteLine(s, "<td align='right'>%ld</td>", numberOfSessions);
            WriteLine(s, "<td align='right'>%.0lf</td>",
                      longestSession / 60);  // Display in minutes
            UUSendZ(s, "</tr>\n");
            token = strtok_r(NULL, delim, &str);
        }
        UUSendZ(s, "</table>\n");
    }

    // Sessions Table
    UUSendZ(
        s,
        "<table class='bordered-table sortable' id='sessiontable'><caption>Sessions</caption><tr "
        "align='center'><th scope='col'>Session</th>");
    if (usageLogUser & ULUSTATUSUSER)
        UUSendZ(s, "<th scope='col'>Username</th>");
    WriteLine(s,
              "<th scope='col' class='sortip'>From%s</th><th scope='col'>Created</th><th "
              "scope='col'>Accessed%s</th><th scope='col'>Generic</th>\n",
              (AnyLocations() ? "<th scope='col'>Location</th>" : ""),
              (anySourceIP ? "<th scope='col' class='sortip'>Source IP</th>" : ""));

    // write the custom user fields titles
    struct ADMINUISESSION *ptr = adminUIsessFields;
    for (; ptr; ptr = ptr->next)
        WriteLine(s, "<th scope='col'>%s</th>", ptr->title);

    WriteLine(s, "</tr>");

    for (i = 0, e = sessions; i < cSessions; i++, e++) {
        if (e->active == 0)
            continue;
        if (e->active == 2)
            pendingReap = 1;
        if (is_ip_zero(&e->sin_addr))
            strcpy(host, "&nbsp;");
        else {
            char service[20];
            if (lookupHostname) {
                // if the lookup fails, it just puts the ip address in as host
                getnameinfo((struct sockaddr *)&e->sin_addr, sizeof e->sin_addr, host, sizeof host,
                            service, sizeof service, 0);
            } else
                ipToStr(&e->sin_addr, host, sizeof host);
        }

        ASCIIDate(&e->accessed, da);
        ASCIIDate(&e->created, dc);

        WriteLine(s, "<tr id='trs%d'><td><a href='/status?session=%s'>%s</a>%s", trs++, e->adminKey,
                  e->key, (e->active == 2 ? " (s)" : ""));
        if (usageLogUser & ULUSTATUSUSER)
            WriteLine(s, "<td>%s", (e->logUserFull && *e->logUserFull ? e->logUserFull : "&nbsp"));

        WriteLine(s, "<td>%s", host);

        if (AnyLocations()) {
            struct LOCATION location;
            FindLocationByNetwork(&location, &e->sin_addr);
            UUSendZ(s, "<td>");
            SendHTMLEncoded(s, location.countryCode);
            if (location.region[0]) {
                UUSendZ(s, " ");
                SendHTMLEncoded(s, location.region);
            }
            if (location.city[0]) {
                UUSendZ(s, " ");
                SendHTMLEncoded(s, location.city);
            }
            UUSendZ(s, "</td>");
        }

        WriteLine(s, "<td>%s<td>%s", dc, da);

        if (anySourceIP) {
            char ipBuffer[INET6_ADDRSTRLEN];
            WriteLine(s, "<td>%s", ipToStr(&e->ipInterface, ipBuffer, sizeof ipBuffer));
        }
        if (e->genericUser)
            WriteLine(s, "<td>%s%s", e->genericUser, e->expirable ? " (e)" : "");
        else
            UUSendZ(s, "<td>&nbsp;");
        /* WriteLine(s, "<td>%d", e->lifetime); */

        for (ptr = adminUIsessFields; ptr; ptr = ptr->next) {
            struct TRANSLATE temp;
            temp.session = e;
            char *result = ExpressionValue(&temp, NULL, NULL, ptr->expression);
            WriteLine(s, "<td>%s</td>", result);
        }

        UUSendZ(s, "</tr>\n");
    }
    UUSendZ(s, "</table>\n");
    if (pendingReap)
        UUSendZ(s, "(s) after a session indicates that the session is stopping\n");

ShowDatabases:
    UUSendZ(s, "<a name='databases'></a>\n");
    ipIdx = 0;
    WriteLine(s,
              "<table class='bordered-table%s' id='databasetable'><caption>Databases</caption><tr "
              "align='center'><th scope='col' class='sortnumeric'>Index</th><th "
              "scope='col'>Database</th><th scope='col' class='sortnumeric'>Hosts</th><th "
              "scope='col'>Domains</th><th scope='col'>URL</th></tr>\n",
              sortable);
    if (extended && globalHttpHeaders) {
        UUSendZ(s, "<tr><td>&nbsp;</td><td colspan='4'>");
        AdminStatusHttpHeaders(t, globalHttpHeaders, 1, 0);
        UUSendZ(s, "</td><tr>\n");
    }
    for (i = 0, b = databases; i < cDatabases; i++, b++) {
        if (db) {
            if (!AdminStatusMatchingDb(db, b))
                continue;
        }

        if (extended && db == NULL) {
            BOOL proxyChange, proxySslChange;
            AdminStatusIpTypes(t, &ipIdx, b->cIpTypes);

            if (b->optionCookie != lastOptionCookie) {
                UUSendZ(s, "<tr><td>&nbsp;</td><td colspan='4'>Option ");
                SendHTMLEncoded(s, OptionCookieNameSelect(b->optionCookie));
                UUSendZ(s, "</td></tr>\n");
                lastOptionCookie = b->optionCookie;
            }

            proxyChange = currentProxyHost != b->proxyHost || currentProxyPort != b->proxyPort;
            proxySslChange =
                currentProxySslHost != b->proxySslHost || currentProxySslPort != b->proxySslPort;
            if (proxyChange || proxySslChange) {
                UUSendZ(s, "<tr><td>&nbsp;</td><td colspan='4'>\n");
                if (proxyChange) {
                    UUSendZ(s, "<div>Proxy");
                    if (b->proxyHost) {
                        UUSendZ(s, " ");
                        SendHTMLEncoded(s, b->proxyHost);
                        WriteLine(s, ":%d", b->proxyPort);
                        if (b->proxyAuth) {
                            UUSendZ(s, " username:password");
                        }
                        UUSendZ(s, "</div>");
                    }
                    currentProxyHost = b->proxyHost;
                    currentProxyPort = b->proxyPort;
                }
                if (proxySslChange) {
                    UUSendZ(s, "<div>ProxySSL ");
                    if (b->proxySslHost) {
                        UUSendZ(s, " ");
                        SendHTMLEncoded(s, b->proxySslHost);
                        WriteLine(s, ":%d", b->proxySslPort);
                        if (b->proxySslAuth) {
                            UUSendZ(s, " username:password");
                        }
                        UUSendZ(s, "</div>");
                    }
                    currentProxySslHost = b->proxySslHost;
                    currentProxySslPort = b->proxySslPort;
                }
                UUSendZ(s, "</td></tr>\n");
            }
        }

        rows = 1;
        BOOL showExtendedDirectives = extended && (b->ignoreGlobalHttpHeaders || b->httpHeaders ||
                                                   b->mimeFiltersUser || b->patterns.pattern);
        if (showExtendedDirectives) {
            rows++;
        }
        WriteLine(s, "<tr  id='trs%d' valign='top'><td rowspan='%d'><a name='db%d'></a>%d</td>",
                  trs++, rows, i, i);
        UUSendZ(s, "<td>");
        SendHTMLEncoded(s, b->title);
        UUSendZ(s, "</td>");
        /*
        if (extended)
        */
        WriteLine(s, "<td>%d</td>", b->hostCount);
        UUSendZ(s, "<td nowrap>");
        m = b->dbdomains;
        if (m == NULL)
            UUSendZ(s, "&nbsp;");
        else
            for (; m; m = m->next) {
                WriteLine(s, "<div>%s%s ", (m->hostOnly ? "H" : "D"), (m->javaScript ? "J" : ""));
                SendHTMLEncoded(s, m->domain);
                if (m->port)
                    WriteLine(s, ":%d", m->port);
                UUSendZ(s, "</div>");
            }
        WriteLine(s, "</td><td><a href='/login?url=%s'>", b->url);
        SendHTMLEncoded(s, b->url);
        UUSendZ(s, "</a></td></tr>\n");
        if (showExtendedDirectives) {
            UUSendZ(s, "<td nowrap colspan='4'>\n");
            AdminStatusHttpHeaders(t, b->httpHeaders, 0, b->ignoreGlobalHttpHeaders);
            AdminStatusMimeFilters(t, b);
            AdminStatusPatterns(t, b);
        }
        UUSendZ(s, "</td></tr>\n");
    }
    if (extended && db == NULL) {
        AdminStatusIpTypes(t, &ipIdx, cIpTypes);
    }

    UUSendZ(s, "</table>\n");

    if (db = ffs[FFDB].value)
        goto ShowHosts;

    UUSendZ(s, "<a name='loginports'></a>\n");
    UUSendZ(s,
            "<table class='bordered-table sortable' id='loginporttable'><caption>Login "
            "ports</caption><tr align='center'><th scope='col' class='sortip'>Interface</th><th "
            "scope='col'>SSL</th><th scope='col' class='sortnumeric'>Port</th></tr>\n");
    for (i = 0, lp = loginPorts; i < cLoginPorts; i++, lp++) {
        char *lpType;
        switch (lp->useSsl) {
        case 0:
            lpType = "N";
            break;
        case USESSLMINPROXY:
            lpType = "MinProxy";
            break;
        case USESSLZ3950:
            lpType = "Z39.50";
            break;
        case USESSLSKIPPORT:
            lpType = NULL;
            break;
        default:
            lpType = "Y";
        }

        if (lpType)
            WriteLine(s, "<tr id='trs%d'><td>%s<td>%s<td>%d</tr>\n", trs++,
                      IPInterfaceName(ipBuffer, sizeof ipBuffer, &lp->ipInterface), lpType,
                      lp->port);
    }
    UUSendZ(s, "</table>\n");

ShowHosts:
    UUSendZ(s, "<a name='hosts'></a>\n");
    WriteLine(s,
              "<table class='bordered-table%s' id='hosttable'><caption>Hosts</caption><tr "
              "align='center'><th scope='col'>SSL</th><th scope='col' class='sortnumeric'>My "
              "Port</th><th scope='col' class='sortnumeric'>Database Index</th><th "
              "scope='col'>JavaScript Enabled</th><th scope='col'>Host</th><th "
              "scope='col'>Created</th><th scope='col'>Accessed</th><th scope='col' "
              "class='sortnumeric'>Counts</th><th scope='col'>Referenced</th><th scope='col' "
              "class='sortnumeric'>Counts</th></tr>\n",
              sortable);
    for (i = 0, h = hosts; i < cHosts; i++, h++) {
        if (db) {
            if (!AdminStatusMatchingDb(db, h->myDb))
                continue;
        }
        ASCIIDate(&h->accessed, da);
        ASCIIDate(&h->created, dc);
        ASCIIDate(&h->referenced, dr);
        if (optionProxyType == PTHOST && h->myPort != 0 && h->forceMyPort == 0)
            proxyByPortMappings++;
        if (i == 0 || h == mvHost || h == mvHostSsl)
            strcpy(dbIdx, "&nbsp;");
        else {
            if (h->accessed < past30 && h->referenced < past30)
                if (h->dontSave == 0)
                    past30Count++;
            if (h->myDb != NULL) {
                sprintf(dbIdx, "<a href='#db%" PRIdMAX "'>%" PRIdMAX "</a>",
                        (intmax_t)(h->myDb - databases), (intmax_t)(h->myDb - databases));
            } else {
                strcpy(dbIdx, "<a href='#-1'>-1</a>");
                if (h->dontSave == 0)
                    orphans++;
            }
        }
        if (h->myPort)
            sprintf(sport, "%d", h->myPort);
        else
            strcpy(sport, "&nbsp;");
        ;
        WriteLine(
            s,
            "<tr "
            "id='trs%d'><td>%c</td><td>%s</td><td>%s</td><td>%c</td><td>%s%s:%d%s</td><td>%s</"
            "td><td>%s</td><td align='right'>%d</td><td>%s</td><td align='right'>%d</td></tr>\n",
            trs++, (h->useSsl ? 'Y' : 'N'), sport, dbIdx, (h->javaScript ? 'Y' : 'N'),
            (h->ftpgw ? "ftp://" : ""), h->hostname, h->remport,
            (h->dontSave ? "<br>(will be removed on restart)" : ""), dc, da, h->accesses, dr,
            h->references);
        if (extended && h->proxyHostname && db == NULL)
            WriteLine(s, "<tr><td colspan='10'>%s</td></tr>\n", h->proxyHostname);
    }
    UUSendZ(s, "</table>\n");

    if (db)
        goto Finished;

    if (orphans) {
        UUSendZ(s,
                "<h2><a name='-1'></a>-1 Database Index</h2>\n<p>Host entries with a -1 database "
                "index do not have a corresponding database definition in your " EZPROXYCFG
                " file.</p>\n");
        UUSendZ(s,
                "<p>Hosts become orphaned if database definitions are inadvertently removed "
                "from " EZPROXYCFG);
        if (anyInvalidDomains)
            UUSendZ(s,
                    ", if they matched <a href='/messages?find=invalid+domain+ignored'>invalid "
                    "Domain directives</a> from older versions of " EZPROXYNAMEPROPER
                    " that are not allowed in the current version, ");
        UUSendZ(s,
                " or when such hosts are remnants from a database that you no longer proxy.</p>\n");
    }

    if (update) {
        UUSendZ(s, "<h2><a name='hostmaintenance'></a>Host Maintenance</h2>\n");
        UUSendZ(s, "<form action='/status' method='post'>\n");
        UUSendZ(s,
                "<div>To perform host maintenance, select one of the following options:</div>\n");

        UUSendZ(s,
                "<div><label><input type='radio' name='maint' value='' checked='checked'> No "
                "action</label></div>\n");

        if (orphans && anyInvalidDomains == 0)
            WriteLine(s,
                      "<div><label><input type='radio' name='maint' value='m1'> Remove %d orphaned "
                      "host%s that %s -1 database index%s (requires " EZPROXYNAMEPROPER
                      " restart)</label></div>\n",
                      orphans, SIfNotOne(orphans), HasHave(orphans, 0), EsIfNotOne(orphans));

        if (past30Count && lastPortReset == 0)
            WriteLine(s,
                      "<div><label><input type='radio' name='maint' value='m2'> Remove %d host%s "
                      "that %s not been used in over 30 days (requires " EZPROXYNAMEPROPER
                      " restart)</label></div>\n",
                      past30Count, SIfNotOne(past30Count), HasHave(past30Count, 0));

        if (proxyByPortMappings)
            WriteLine(
                s,
                "<div><label><input type='radio' name='maint' value='m3'> Shutdown %d port%s that "
                "%s previously assigned for proxy by port (no restart required)</label></div>\n",
                proxyByPortMappings, SIfNotOne(proxyByPortMappings),
                WasWere(proxyByPortMappings, 0));

        UUSendZ(s,
                "<div><label><input type='radio' name='maint' value='m4'> Reset accessed and "
                "referenced dates and counts of all hosts (no restart required)</label></div>\n");

        if (optionProxyType != PTHOST) {
            UUSendZ(s,
                    "<div><label><input type='radio' name='maint' value='m5'> Compress port usage "
                    "by reassigning higher ports into any available gaps (no restart "
                    "required)</label></div>\n");
        }

        UUSendZ(s, "<div>and then click <input type='submit' value='process'>.</div>\n</form>\n");
    }

    if (debugLevel > 0) {
        UUSendZ(s, "<h2>Translate status</h2>\n");
        for (count = 0, i = 0, t1 = translates; i < cTranslates; i++, t1++)
            if (t1->active) {
                struct UUSOCKET *w = t1->wsocketPtr;
                count++;
                WriteLine(s,
                          "status translate %d url %s len %" PRIdMAX " method %s send %" PRIuMAX
                          " recv %" PRIuMAX " send2 %" PRIuMAX " recv2 %" PRIuMAX " ",
                          i, t1->urlCopy, t1->contentLength, t1->method, (t1->ssocket).sendCount,
                          (t1->ssocket).recvCount, (w ? w->sendCount : 0), (w ? w->recvCount : 0));
                WriteLine(s, "stage %d ", t1->finalStatus);
                if (t1->raw == 2) {
                    WriteLine(s, "raw %d readofs %d readpage %d writeofs %d writepage %d<br>\n",
                              t1->rawReadOfs, t1->rawReadPage, t1->rawWriteOfs, t1->rawWritePage);
                } else {
                    UUSendZ(s, "non-raw<br>\n");
                }
            }
    }

    if (ts != NULL) {
        UUSendZ(s, "\n");
        UUSendZ(s,
                "<table class='bordered-table'><tr align='center'><caption>Tracked "
                "sockets</caption><th scope='col'>Socket</th><th scope='col'>Opened</th><th "
                "scope='col'>Stopped</th><th scope='col'>Facility</th><th "
                "scope='col'>Host</th></tr>\n");
        for (i = 0, tsp = ts; i < TSMAXSOCKET; i++, tsp++) {
            if (tsp->opened == 0)
                continue;
            ASCIIDate(&tsp->opened, da);
            ASCIIDate(&tsp->stopped, dr);
            WriteLine(s, "<tr><td>%d</td><td>%s</td><td>%s</td><td>%s</td><td>", i, da, dr,
                      tsp->facility);
            UUSendZ(s, tsp->host);
            UUSendZ(s, "</td></tr>\n");
        }
        UUSendZ(s, "</table>\n");
    }

    UUSendZ(s, "<h2><a name='miscellaneous'></a>Miscellaneous</h2>\n");
    WriteLine(s, "<div>Peak sessions active/limit: %d/%d</div>\n", cSessions, mSessions);
    WriteLine(s, "<div>Peak concurrent transfers active/limit: %d/%d</div>\n", cTranslates,
              mTranslates);
    if (debugLevel > 0)
        WriteLine(s, "<div>Current number of transfers active: %d</div>\n", count);
    WriteLine(s, "<div>Peak virtual hosts/limit: %d/%d</div>\n", cHosts, mHosts);

    if (optionTicks) {
        WriteLine(s, "<div>Main ticks: %d</div>\n", ticksMain);
        WriteLine(s, "<div>StopSockets ticks: %d</div>\n", ticksStopSockets);
        WriteLine(s, "<div>Raw transfer ticks: %d</div>\n", ticksRaw);
        WriteLine(s, "<div>Translate ticks: %d</div>\n", ticksTranslate);
        WriteLine(s, "<div>Cluster ticks: %d</div>\n", ticksCluster);
    }

    if (UUSslInitialized())
        UUSendZ(s, "<p>SSL is initialized\n");

Finished:
    FreeThenNull(userNames);
    AdminFooter(t, 0);
} /* AdminStatus */

void AdminStatusReset(struct TRANSLATE *t, char *query, char *post) {
    int i;
    struct UUSOCKET *s;
    struct HOST *h;
    struct FORMFIELD ffs[] = {
        {"reset", NULL, 0, 0, 10},
        {NULL,    NULL, 0, 0, 0 }
    };
    BOOL resetCounts = 0;
    BOOL resetAll = 0;

    s = &t->ssocket;
    AdminHeader(t, 0, "Status Reset");

    FindFormFields(query, post, ffs);

    if (ffs[0].value) {
        if (strcmp(ffs[0].value, "counts") == 0)
            resetCounts = 1;
        if (strcmp(ffs[0].value, "all") == 0)
            resetCounts = resetAll = 1;
    }

    if (resetCounts == 0 && resetAll == 0) {
        UUSendZ(s,
                "You can reset just <a href='/status-reset?reset=counts'>counts</a> of accesses "
                "and references ");
        UUSendZ(s, "or both the <a href='/status-reset?reset=all'>counts and dates</a>.");
        return;
    }

    for (i = 0, h = hosts; i < cHosts; i++, h++) {
        h->accesses = 0;
        h->references = 0;
        if (resetAll) {
            h->accessed = 0;
            h->referenced = 0;
        }
    }

    UUSendZ(s, "Reset complete, verify using the <a href='/status'>status</a> page.\n");

    AdminFooter(t, 0);

    NeedSave();
}
