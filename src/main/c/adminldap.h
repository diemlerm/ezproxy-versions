#ifndef __ADMINLDAP_H__
#define __ADMINLDAP_H__

#include "common.h"

/* Prototypes for adminldap.c */

void AdminLDAP(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINLDAP_H__
