#ifndef __USRL4U_H__
#define __USRL4U_H__

#include "common.h"

enum FINDUSERRESULT FindUserL4U(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                char *file,
                                int f,
                                struct FILEREADLINEBUFFER *frb,
                                struct sockaddr_storage *pInterface);

#endif  // __USRL4U_H__
