#define __UUFILE__ "main.c"

#include "common.h"
#include <curl/curl.h>
#include "ncpu.h"

#include "ezproxyversion.h"

#include "obscure.h"
#include "intrusion.h"
#include "wskeylicense.h"
#include "spur.h"

#ifdef WIN32
#include "win32.h"
#include <direct.h> /* for chdir */
#else
#include "unix.h"
#endif

#include "saml.h"
#include "adminaudit.h"
#include "adminsecurity.h"
#include "security.h"
#include "identifier.h"
#include "environment.h"

#ifdef WIN32
#define _SERVICE_MAIN
#include "service.h"

/* internal name of service */
char *SZAPPNAME = EZPROXYNAMEPROPER;
/* displayed name of service */
char *SZSERVICENAME = EZPROXYNAMEPROPER;
char *SZSERVICEDISPLAYNAME = EZPROXYNAMEPROPER;
/* list of service dependencies - "dep1\0dep2\0\0" */
char *SZDEPENDENCIES = "\0\0";
/* event log message file */
char *SZSERVICEMSGFILE = NULL;

/* Following needed to setup NetUserChangePassword for NT only (not avail 95/98) */

HINSTANCE netapi32Dll = NULL;
LPNetUserChangePassword sNetUserChangePassword = NULL;

#endif

void VisitUsOnTheWebOrContact(BOOL toStdout) {
    char *visit = "For further information or updates, visit " EZPROXYURLROOT
                  " or contact " EZPROXYHELPEMAIL ".";
    int (*pr)(const char *format, ...);
    char tr;

    pr = (toStdout ? printf : Log);
    tr = (toStdout ? '\n' : 0);

    (*pr)("%s%c", visit, tr);
}

void GetMyBinaryAndMyPath(char *argv0) {
    char binary[1024];
#ifndef WIN32
    struct stat buf;
#endif
    size_t len;

    if (myBinary)
        return;

    len = 512;
    for (;;) {
        if (!(myStartupPath = malloc(len)))
            PANIC;
        if (getcwd(myStartupPath, len)) {
            if (!(myStartupPath = realloc(myStartupPath, strlen(myStartupPath) + 1)))
                PANIC;
            break;
        }
        if (errno != ERANGE)
            PANIC;
        FreeThenNull(myStartupPath);
        len += 512;
    }

    binary[0] = 0;

#ifdef WIN32
    GetModuleFileName(NULL, binary, sizeof(binary));
#else
    if (*argv0 != 0 && (lstat(argv0, &buf) == 0) && S_ISLNK(buf.st_mode)) {
        char linkValue[MAX_PATH];
        if (readlink(argv0, linkValue, sizeof(linkValue) - 1) != -1) {
            if (realpath(linkValue, binary) == NULL) {
                binary[0] = 0;
            }
        }
    }
#endif /* #ifndef WIN32 */

    if (!(myBinary = strdup(binary[0] ? binary : argv0)))
        PANIC;
    if (!(myPath = strdup(myBinary)))
        PANIC;
    DirName(myPath);

    if (debugLevel > 0)
        Log("Changing default directory to %s", myPath);
    chdir(myPath);
}

void CheckLimits(int pass) {
#ifndef WIN32
    struct rlimit rlim;
#endif
    int maxSockets;

    /* In the first pass, we try for worst case; in the second pass, we actually know
       what potentialListeners is, so we generate the warnings only if we can't reach
       that level
    */
    maxSockets = mTranslates * 2 + (pass == 0 ? mHosts + cLoginPorts : potentialListeners) + 20;

    if (debugLevel)
        Log("pass %d, max sockets %d, potential listeners %d", pass, maxSockets,
            potentialListeners);

#ifndef WIN32
        /* This isn't POSIX, so just let it be */
#ifdef RLIMIT_NPROC
    if (pass == 1) {
        getrlimit(RLIMIT_NPROC, &rlim);
        rlim.rlim_cur = rlim.rlim_max;
        setrlimit(RLIMIT_NPROC, &rlim);
    }
#endif /* RLIMIT_NPROC */

    /* Increase the number of files (and sockets) that can be open from soft to hard limit
       Try to increase first up to the greater of rlim.rlim_max or maxSockets first.  If that
       doesn't go high enough, try to increase to rlim.rlim_max to at least go as high as
       possible (Solaris lets root raise the hard limit above rlim.rlim_max, Linux doesn't
       currently, but try it this way to try to get it as high as possible)
    */

    getrlimit(RLIMIT_NOFILE, &rlim);

    if (rlim.rlim_cur < maxSockets) {
        rlim.rlim_cur = rlim.rlim_max = (rlim.rlim_max < maxSockets ? maxSockets : rlim.rlim_max);
        setrlimit(RLIMIT_NOFILE, &rlim);
        getrlimit(RLIMIT_NOFILE, &rlim);
    }

    if (rlim.rlim_cur < rlim.rlim_max) {
        rlim.rlim_cur = rlim.rlim_max;
        setrlimit(RLIMIT_NOFILE, &rlim);
        getrlimit(RLIMIT_NOFILE, &rlim);
    }

    if (rlim.rlim_cur < maxSockets && pass > 0) {
        Log("");
        Log("WARNING: there are not enough file descriptors available");
        Log("  should all %d possible virtual web servers and %d possible concurrent", mHosts,
            mTranslates);
        Log("  transfers be active simultaneously.  Maybe set 'ulimit -n NNN' to something "
            "larger.");
        Log("  Contact " EZPROXYHELPEMAIL " for more information.");
        Log("");
    }
#endif /* !WIN32 */

#ifndef USEPOLL
    if (FD_SETSIZE < maxSockets && pass > 0) {
        Log("");
        Log("WARNING: FD_SETSIZE was compiled at %d, which is too small", FD_SETSIZE);
        Log("  should all %d possible virtual web server and %d possible concurrent", mHosts,
            mTranslates);
        Log("  transfers be active simultaneously.");
        Log("  Contact " EZPROXYHELPEMAIL " for more information.");
        Log("");
    }
#endif
}

void Usage(char *prog) {
    char sprog[strlen(prog) + 1];
    memset(sprog, ' ', strlen(prog));
    sprog[strlen(prog)] = 0;
    printf("\nUsage:\n");
    printf("  %s -h         Display this help text\n", prog);
    printf("  %s -c [port]  Check ability to connect to/from outside host\n", prog);
    printf("  %s -d dir     Change current directory to \"dir\" when " EZPROXYNAMEPROPER
           " starts\n",
           prog);
    printf("  %s -D N       Set an initial XDEBUG level which may be overridden in the %s file\n",
           prog, EZPROXYCFG);
    printf("  %s -k         Install license key (key must follow -k)\n", prog);
    printf("  %s -m         Create missing files\n", prog);
    printf("  %s -mg        Create missing optional group support files\n", prog);
    printf("  %s -ms        Create missing optional security support files\n", prog);
    printf("  %s -r         Restore all files to original versions, losing all changes\n", prog);
#ifdef WIN32
    printf("  %s -si        Install as a service\n", prog);
    printf("  %s -sl        List " EZPROXYNAMEPROPER " service(s)\n", prog);
    printf("  %s -sr [name] Remove as service\n", prog);
#else
    printf("  %s -si        Install startup script\n", prog);
    printf("  %s -sr        Remove startup script\n", prog);

#endif
    printf("  %s -v         Display program version number\n", prog);
    printf("  %s            Start " EZPROXYNAMEPROPER ", log messages to console\n", prog);
#ifndef WIN32
    printf("  %s start      Start " EZPROXYNAMEPROPER " as daemon process\n", prog);
#endif
    printf("  %s restart    Restart " EZPROXYNAMEPROPER "\n", prog);
    printf("  %s stop       Stop " EZPROXYNAMEPROPER "\n", prog);
#ifndef WIN32
    printf("  %s stopall    Force all instances of " EZPROXYNAMEPROPER " to stop\n", prog);
#endif
    printf("  %s status     Display " EZPROXYNAMEPROPER " status\n", prog);
    printf("  %s log [file] Rename ezproxy.log to [file] or reopen\n", prog);
    printf("  %s            ezproxy.log if no [file] specified\n", sprog);
    printf("\n");
    VisitUsOnTheWebOrContact(1);
}

void ShowVersion(BOOL toStdout) {
    int (*pr)(const char *format, ...);
    char tr;

    pr = (toStdout ? printf : Log);
    tr = (toStdout ? '\n' : 0);

    (*pr)("%s%c", EZPROXYVERSION, tr);

#ifdef WIN32
    if (InstalledAsService(NULL)) {
        (*pr)("%s is configured to run as a service.%c", EZPROXYNAMEPROPER, tr);
    }
#endif
    VisitUsOnTheWebOrContact(toStdout);
}

void Validate2(char *key) {
    struct WSKEYLICENSE check;
    struct WSKEYLICENSE *license = WskeyLicenseCreate(key);
    WskeyInitialize(license);
    WskeyInfo();
    FreeThenNull(license);
}

void ProxyPingPong(struct TRANSLATE *t) {
    time_t now, lastAccess, expires;

    int len;
    int rc;
    int timeout;
    SOCKET maxSock;
#ifdef USEPOLL
    struct pollfd pfds[2];
    unsigned int pfdc;
#else
    fd_set rfds, wfds;
    struct timeval pollInterval;
#endif
    struct {
        struct UUSOCKET *r;
        struct UUSOCKET *s;
        char *nab; /* next available buffer */
        char *eob; /* end of buffer */
        char buffer[32768];
        char eor; /* have received EOF on receive */
        char eos; /* have received fail on send */
    } *tf, transfers[2];
    int ticks = 0;
    int inactivity = UUMAX(t->ssocket.timeout, t->wsocketPtr->timeout);

    if (inactivity == 0)
        inactivity = 60;

    expires = 0;

    UUtime(&now);
    lastAccess = now;

    UUFlush(&t->ssocket);
    UUFlush(t->wsocketPtr);

    for (tf = transfers; tf <= transfers + 1; tf++) {
        memset(tf, 0, sizeof(*tf));
        if (tf == transfers) {
            tf->r = t->wsocketPtr;
            tf->s = &t->ssocket;
        } else {
            tf->r = &t->ssocket;
            tf->s = t->wsocketPtr;
        }
        tf->nab = tf->buffer;
        tf->eob = tf->buffer + sizeof(tf->buffer);
    }

Again:
    ticks++;
    UUtime(&now);
    if (expires && expires < now)
        goto Done;

    /* timeout starts out at 100 milliseconds.  It will be reduced to zero if there are any
       SSL sockets in an "unknown pending" state, so they can be scanned to determine just
       what to wait on */
    timeout = 100;
    maxSock = 0;
#ifdef USEPOLL
    pfdc = 0;
#else
    FD_ZERO(&rfds);
    FD_ZERO(&wfds);
#endif

    for (tf = transfers; tf <= transfers + 1; tf++) {
        UUtime(&now);

        if (tf->eos == 0) {
            if (tf->nab > tf->buffer) {
                len = UUSend(tf->r, tf->buffer, tf->nab - tf->buffer, UUSNOWAIT);
                if (minProxyDelay) {
                    SubSleep(minProxyDelay / 1000, minProxyDelay % 1000);
                }

                if (len > 0) {
                    lastAccess = now;
                    tf->nab -= len;
                    if (tf->nab > tf->buffer) {
                        memmove(tf->buffer, tf->buffer + len, tf->nab - tf->buffer);
                    }
                }
                if (len < 0 && socket_errno != WSAEWOULDBLOCK) {
                    tf->eos = 1;
                }
            }
            if (tf->eor && tf->eos == 0 && tf->nab == tf->buffer) {
                tf->eos = 1;
                UUShutdown(tf->r, SHUT_WR);
            }
        }

        if (tf->eor == 0) {
            if (tf->eos)
                tf->nab = tf->buffer;
            if (tf->nab < tf->eob) {
                len = UURecv(tf->s, tf->nab, tf->eob - tf->nab, UUSNOWAIT);
                if (len > 0) {
                    tf->nab += len;
                    lastAccess = now;
                }
                if (len == 0 || (len < 0 && socket_errno != WSAEWOULDBLOCK)) {
                    tf->eor = 1;
                    expires = now + 60;
                }
            }
        }
    }

    if (transfers[0].eor && transfers[0].eos && transfers[0].eor && transfers[1].eos)
        goto Done;

    if (now - lastAccess > inactivity)
        goto Done;

    if (maxSock == 0) {
        /* No explicit file descriptors to monitor, so just delay */
        /* (you'd think that'd be OK below, since select and poll understand such things, but Win32
           doesn't like do the "use select as a delay" thing */
        if (timeout > 0)
            SubSleep(0, timeout);
        goto Again;
    }

#ifdef USEPOLL
    TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(poll(&pfds[0], pfdc, timeout)));
    if (rc < 0) {
        Log("poll returned %d", socket_errno);
        PANIC;
    }
#else
    /* Note that Linux select destroys the timeout field, so it must be reset each time */
    pollInterval.tv_sec = 0;
    pollInterval.tv_usec = timeout * 1000;
    TEMP_FAILURE_RETRY_INT(
        rc = interrupted_neg_int(select(maxSock + 1, &rfds, &wfds, NULL, &pollInterval)));
    if (rc < 0) {
        Log("select returned %d", socket_errno);
        PANIC;
    }
#endif

    goto Again;

Done:
    UUStopSocket(&t->ftpsocket, 0);
    UUStopSocket(t->wsocketPtr, UUSSTOPNICE);

    UUStopSocket(&t->ssocket, UUSSTOPNICE);
}

void ProxyGateway(struct TRANSLATE *t) {
    ConnectToServer(t, 1, (t->host)->hostname, (t->host)->remport,
                    DatabaseOrSessionIpInterface(t, NULL), (t->host)->useSsl, NULL);

    UUSendZCRLF(t->wsocketPtr, t->buffer);

    ProxyPingPong(t);
}

void DoMinProxy(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket;
    struct UUSOCKET *w = t->wsocketPtr;
    char *p;
    char useSsl;
    char *field;
    char *nf;
    char *host;
    char *sPort;
    PORT port;
    char *slash;
    int retCode = 0;
    BOOL isConnect = strcmp(t->method, "CONNECT") == 0;
    int contentLength = -1;
    char *startsContentLength;
    char dummy[8];

    UUInitSocket(w, INVALID_SOCKET);

    /*  printf("%s\n", t->url); */
    if (p = FindField("Proxy-Authorization", t->requestHeaders)) {
        LinkFields(p);
        p = SkipWS(p);
        if (strnicmp(p, "basic ", 6) == 0)
            p = SkipWS(p + 5);
        else
            p = NULL;
    }

    if (p == NULL || strcmp(p, minProxyAuth) != 0) {
        // If someone tried to submit credentials, log the attempt as a failure
        if (p != NULL) {
            AuditEvent(t, AUDITMINPROXYFAILURE, NULL, NULL, NULL);
        }
        HTTPCode(t, 407, 0);
        UUSendZ(s, "Proxy-Authenticate: Basic realm=\"ezproxy\"\r\n");
        HTTPContentType(t, NULL, 1);
        UUSendZ(s, "Authentication required\n");
        goto Done;
    }

    host = SkipHTTPslashesAt(t->url, &useSsl);
    ParseHost(host, &sPort, &slash, TRUE);
    if (slash)
        *slash = 0;

    if (sPort) {
        *sPort++ = 0;
        port = atoi(sPort);
    } else {
        port = (isConnect ? 443 : 80);
    }

    if (*host == 0 || port == 0 ||
        (retCode = UUConnectWithSource(w, host, port, "DoMinProxy", &minProxyIpInterface)) != 0) {
        AuditEvent(t, AUDITMINPROXYSUCCESSBLOCKED, NULL, NULL, "%s:%d", host, port);
        HTTPCode(t, 503, 1);
        if (debugLevel > 90) {
            Log("host=%d, port=%d, UUConnectWithSource()=%d", host != 0, port != 0, retCode);
        }
        goto Done;
    }

    AuditEvent(t, AUDITMINPROXYSUCCESSCONNECT, NULL, NULL, "%s:%d", host, port);

    if (isConnect) {
        UUSendZ(s, "HTTP/1.0 200 Connection established\r\nConnection: close\r\n\r\n");
    } else {
        WriteLine(w, "%s ", t->method);

        if (slash)
            *slash = '/';
        else
            slash = "/";

        UUSendZ(w, slash);

        if (t->version)
            WriteLine(w, " %s", t->version);

        UUSendCRLF(w);

        for (field = t->requestHeaders; *field; field = nf) {
            LinkField(field);
            nf = NextField(field);
            if (StartsIWith(field, "Proxy-"))
                continue;
            /* Suppress the client's Connection request, which will be
               forced to Connection: close when this while loop ends
            */
            if (StartsIWith(field, "Connection:"))
                continue;

            if ((startsContentLength = StartsIWith(field, "Content-Length:"))) {
                startsContentLength = SkipWS(startsContentLength);
                if (isdigit(*startsContentLength)) {
                    int cl = atoi(startsContentLength);
                    if (cl > 0) {
                        contentLength = cl;
                    }
                }
            }

            UUSendZCRLF(w, field);
        }

        UUSendZ(w, "Connection: close\r\n\r\n");

        /* If there is content to upload, send it */
        if (contentLength > 0 && strcmp(t->method, "HEAD") != 0) {
            if (debugLevel > 0)
                Log("MinProxy POST phase 1 %" PRIdMAX " bytes", contentLength);
            t->finalStatus = 904;
            if (!RawTransfer(t, w, s, contentLength, optionPostCRLF)) {
                goto Done;
            }
            t->finalStatus = 905;
            if (!UUValidSocketSend(w)) {
                goto Done;
            }
            t->finalStatus = 907;

            /* Some web browsers add a CR/LF after the content.  The RFC doesn't say they should,
            but they do.  We used to reap this at the end of processing, but MBC had a problem with
            their AS/400 proxying, so now we try to consume this in non-blocking mode
            */
            if (debugLevel > 0)
                Log("POST phase 2");
            UURecv(s, dummy, sizeof(dummy), UUSNOWAIT);
            if (debugLevel > 0)
                Log("POST phase 3");
        }

        while (ReadLine(w, t->buffer, sizeof(t->buffer))) {
            if (t->buffer[0] == 0) {
                UUSendZ(s, "Connection: close\r\n\r\n");
                break;
            }
            /* Suppress the server's Connection request, which will be
               forced to Connection: close when this while loop ends
            */
            if (StartsIWith(t->buffer, "Connection:"))
                continue;
            UUSend2(s, t->buffer, strlen(t->buffer), "\r\n", 2, 0);
        }
    }

    ProxyPingPong(t);

Done:
    UUStopSocket(&t->ssocket, UUSSTOPNICE);
    UUStopSocket(t->wsocketPtr, UUSSTOPNICE);
    UUStopSocket(&t->ftpsocket, 0);
    EndTranslate(t, 0);
    ReleaseTranslate(t);
}

void HttpsRequired(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket;
    char line[256];

    /* Consume the header of the request, particularly since old Netscape will gripe
       if the header isn't all read before we start talking
    */
    while (ReadLine(s, line, sizeof(line)) && line[0]) {
    }

    /* We send a 200 code so our text file will display and not be replaced by an
        if (t->ftpsocket.o != INVALID_SOCKET) {
            UUStopSocket(t->wsocketPtr, 0);
            UUStopSocket(&t->ftpsocket, 0);
        }
    IE generic message, but reset the final status to a different value so the log
    file can note something is out of the ordinary
    */
    /* Pretend like we were able to parse the version */
    strcpy(t->version, "HTTP/1.0");
    t->clientVersionX1000 = 1000;
    HTTPCode(t, 200, 0);
    HTTPContentType(t, NULL, 1);
    t->finalStatus = 400;
    SendEditedFile(t, NULL, HTTPSHTM, 1, NULL, 0, NULL);
}

/**
 * Is called from main thread or as its own thread
 */
void DispatchTranslate(struct TRANSLATE *t) {
    /* The socket is passed through within t->ssocket so the SSL negotation can occur
       after the thread has started (if it's a thread at all, see optionSingleThreadedTranslations),
       instead of blocking the main loop thread
    */

    if (t->ssocketInitialized == 0) {
        if (t->useSsl == 1 && (t->loginPort == NULL || (t->loginPort->forceHttp == 0))) {
            if (debugLevel > 19)
                Log("%d Begin SSL negotiate", (t->ssocket).o);
            UUInitSocketSsl(&t->ssocket, (t->ssocket).o, UUSSLHTTP, t->acceptSslIdx, NULL);
            (t->ssocket).timeout = clientTimeout;
            if (debugLevel > 19)
                Log("%d End SSL negotiate valid %d http %d", (t->ssocket).o, (t->ssocket).sendOK,
                    (t->ssocket).acceptedHttp);
            if ((t->ssocket).acceptedHttp) {
                if (debugLevel > 0) {
                    Log("%d HTTP over HTTPS", (t->ssocket).o);
                }
                HttpsRequired(t);
                goto Finished;
            }
        } else {
            UUInitSocketStream(&t->ssocket, (t->ssocket).o);
            (t->ssocket).timeout = clientTimeout;
        }
        t->ssocketInitialized = 1;
        dprintf("new %d", (t->ssocket).o);
    } else {
        dprintf("Reusing raw %d", (t->ssocket).o);
        goto Reuse;
    }

    if (t->useSsl == USESSLZ3950) {
        DoZ3950Proxy(t);
        return;
    }

    goto SkipReuse;

Reuse:
    if (t->raw == 1) {
        t->raw = 2;
        goto DoRaw;
    }

    if (t->sentKeepAlive == 0)
        goto Finished;

    UUSendReset(&t->ssocket, UUSSTOPNICE);
    if (t->ssocket.o == INVALID_SOCKET)
        goto Finished;

    dprintf("Reusing %d", (t->ssocket).o);

    ResetTranslate(t);

    (t->ssocket).timeout = keepAliveTimeout;

SkipReuse:

    if (t->ftpsocket.o != INVALID_SOCKET || t->serverKeepAlive == 0) {
        if (t->wsocketPtr->o != INVALID_SOCKET)
            dprintf("Closing out remote %d", t->wsocketPtr->o);
        UUStopSocket(t->wsocketPtr, 0);
        UUStopSocket(&t->ftpsocket, 0);
    }

    if (t->wsocketPtr->o != INVALID_SOCKET) {
        dprintf("Reusing remote %d", t->wsocketPtr->o);
        UURecvReset(t->wsocketPtr, 0);
    }

    /* Reset the serverKeepAlive flag */
    t->serverKeepAlive = 0;

    t->finalStatus = 901;

    // ParseRequest setup most of t, return HTTP repsonse code error on error
    if (ParseRequest(t) || AdminNetworkTest(t) || ValidateSession(t)) {
        dprintf("Ending %d", (t->ssocket).o);
        EndTranslate(t, 1);
        goto Finished;
    }

    t->finalStatus = 902;

    UUtime(&(t->host)->accessed);
    (t->host)->accesses++;

    if (t->useSsl == USESSLMINPROXY) {
        DoMinProxy(t);
        return;
    }

    CheckUserAgent(t);

    if (optionDispatchLog) {
        static struct LOGSPU dl;
        static BOOL needInit = TRUE;

        if (needInit) {
            UUAcquireMutex(&mAccessFile);
            if (needInit) {
                dl.filename = strdup("dispatch.log");
                dl.format = strdup(LOGFORMAT);
                needInit = FALSE;
            }
            UUReleaseMutex(&mAccessFile);
        }
        LogRequest(t, &dl, NULL, NULL, NULL);
    }

    // not sure what goes on here, but will force a login if needed
    if (t->host == hosts || t->host == mvHost || t->host == mvHostSsl ||
        (t->host && t->host->myDb &&
         (t->host->myDb->netLibraryConfigs || t->host->myDb->fmgConfigs ||
          t->host->myDb->myiLibraryConfigs)) ||
        (t->session && ((t->session)->userLimitExceeded ||
                        UsageLimitExceeded(t, (t->session)->usageLimitExceeded, ULEVOLUME)))) {
        // launch the admin screen / login screen
        DoAdmin(t);
        goto Reuse;
    }

    if (AdminLicenseInvalid(t)) {
        goto Reuse;
    }

    t->logPort = (t->host)->remport;

    // check if we had a logged in user
    // ValidateRequest return 0 on valid, 1 on invalid user
    if (ValidateRequest(t))
        goto Finished;

    //@TODO look into this code
    // entry point for where we get content for user
    // send them out of ezproxy?
    // connect to host and proxy the information?

    // so I guess ftp protocols are just forwarded
    // ftpgw = ftp gateway
    if (t->host && (t->host)->ftpgw) {
        if (ForwardRequestFTP(t))
            goto Finished;

    } else {
        if (ConnectToServer(t, 0, (t->host)->hostname, (t->host)->remport,
                            DatabaseOrSessionIpInterface(t, NULL), (t->host)->useSsl,
                            (t->host)->myDb))
            goto Finished;

        if (ForwardRequest(t))
            goto Finished;

        t->finalStatus = 906;

        /* This half-close seemed like a good idea, but some servers took this to mean
           we wanted to stop everything
        */
        /* UUShutdown(r, SHUT_WR); */

        if (ProcessResponse(t)) {
            EndTranslate(t, 1);
            if (t->bodylessStatus) {
                dprintf("bodyless resuse %d", t->finalStatus);
                goto Reuse;
            }
            dprintf("not reusing on %d: %d", t->finalStatus, t->wsocketPtr->o);

            goto Finished;
        }
    }

    // printf("%sHTML %" PRIdMAX " %s on %d\n", (t->html ? "" : "Non-"), t->contentLength, t->url,
    // (t->host)->myPort);

    if (strcmp(t->method, "HEAD") == 0)
        goto Finished;

    /* A content length of -1 means unknown, greater than zero indicates a body, but
       0 is no body at all, in which could we wouldn't have anything to process
    */
    if (t->contentLength != 0) {
        /*this is where we write the body content*/
        if (t->html == 0 && t->javaScript == 0) {
            /* Technically, we'll come out with t-raw 1 and reuse will just
               dispatch us, but it seems cleaner this way...not to me Chris */
            if (optionUseRaw) {
                StartRaw(t, t->contentLength, 0);

            } else {
                ProcessNonHTML(t);
                EndTranslate(t, 1);
                if (t->wsocketPtr->recvChunking != 99)
                    goto Finished;
            }
        } else {
            ProcessHTML(t);
            UUFlush(&t->ssocket);
            EndTranslate(t, 1);
            if (t->wsocketPtr->recvChunking != 99)
                goto Finished;
        }
    } else {
        EndTranslate(t, 1);
        // Since there was no content received, there is no chunking
        // check required before reuse can be considered.
    }

    goto Reuse;

Finished:
    dprintf("Going through closeup %d", t->wsocketPtr->o);
    UUStopSocket(&t->ssocket, UUSSTOPNICE);
    UUStopSocket(t->wsocketPtr, 0);
    UUStopSocket(&t->ftpsocket, 0);
    ReleaseTranslate(t);

DoRaw:;
}

int CheckForIPReject(struct TRANSLATE *t) {
    enum IntrusionIPStatus iis = INTRUSION_IP_NONE;

    if (anyIntruderIPRejects) {
        struct INTRUDERIP *bg;
        if (bg = FindIntruderIP(t, 0, 0, 0))
            t->intruderIPLevel = IntruderIPLevel(bg, 0);
    }

    if (intrusionReject) {
        iis = IntrusionIPStatus(&t->sin_addr, 1);
        if (iis == INTRUSION_IP_BAD_IP && intrusionEnforce) {
            t->intruderIPLevel = 3;
        }
    }

    /* If this is from an IP address that we reject, kill it off */
    if (t->intruderIPLevel == 3 || IsRejectedIP(t)) {
        if (FileExists(REJECTHTM))
            return 2;
        return 1;
    }

    return 0;
}

/**
 * Wrapper for DispatchTranslate()
 */
void DoTranslate(void *v) {
    struct TRANSLATE *t = (struct TRANSLATE *)v;

    DispatchTranslate(t);
    OPENSSL_thread_stop();
}

/**
 * Decides how to start the translation.  Will either
 * begin a new thread or start the translation in current thread.  If the
 * thread start fails it will return a 503 error.
 */
int StartTranslate(struct TRANSLATE *t) {
    int failed;

    if (optionSingleThreadedTranslations) {
        DispatchTranslate(t);
        return 0;
    } else {
        failed = UUBeginThread(DoTranslate, (void *)t, "Translate");
        if (failed == 0)
            return 0;
    }

    /* Thread start failed. */

    if (t->ssocketInitialized == 0) {
        if (t->useSsl)
            UUInitSocketSsl(&t->ssocket, t->ssocket.o, UUSSLHTTP, t->acceptSslIdx, NULL);
        /* plain socket was already initialized in dispatchsocket
        else
            UUInitSocketStream(&t->ssocket, s);
        */
    }

    HTTPCode(t, 503, 0);
    if (*t->version) {
        UUSendZ(&t->ssocket, "Connection: close\r\nContent-type: text/html\r\n\r\n");
    }

    UUSendZ(&t->ssocket, "Unable to create new thread, please try again later.\n");
    UUStopSocket(&(t->ssocket), UUSSTOPNICE);
    EndTranslate(t, 1);
    ReleaseTranslate(t);

    return 1;
}

/**
 * This does the heavy lifting of the socket connection.  It will call functions to
 * initialize the socket, read the data, and setup host connections.
 */
BOOL DispatchSocket(int hostIndex,
                    SOCKET inSocket,
                    char useSsl,
                    PORT logPort,
                    struct LOGINPORT *lp) {
#define HOSTANDPORTMAXLEN 128
#define HOSTMAXLEN (HOSTANDPORTMAXLEN - 6 - 1 - 1)
    struct HOST *h;
    SOCKET s;
    char hostAndPort[HOSTANDPORTMAXLEN];
    BOOL reject = 0;
    static time_t firstFailedDispatch = 0;
    time_t now;
    int tries = 0;
    int acceptSslIdx = lp ? lp->acceptSslIdx : 0;
    int one = 1;
    struct linger linger;
    struct TRANSLATE *t;
    int j;

    if (hostIndex > 0 && optionForceSsl)
        useSsl = 1;

AcceptAgain:
    guardian->mainLocation = 1000;
    tries++;

    h = hosts + hostIndex;
    /*  printf("Select for %d\n", h->myPort); */

    /* Find a translation structure to use for this request, */
    for (j = 0;; j++) {
        /* When we hit the limit of active translations, open up a new one to use (if any new left)
         */
        if (j == cTranslates) {
            /* If this occurs, all translations are in process so we skip this connection for now
             * and */
            /* hope to be able to service it soon (should probably add some type of sleep factor
             * into the */
            /* select under these conditions */
            if (cTranslates == mTranslates) {
                if (firstFailedDispatch == 0)
                    UUtime(&firstFailedDispatch);
                else {
                    UUtime(&now);
                    if (now - firstFailedDispatch > 60) {
                        Log("Translates remained full for more than one minute; restarting");
                        exit(0);
                    }
                }
                Log("Maximum concurrent transfers reached");
                return 1;
            }
            cTranslates++;
            break;
        }
        if (translates[j].active == 0)
            break;
    }

    guardian->mainLocation = 1001;

    firstFailedDispatch = 0;
    t = translates + j;
    ResetTranslate(t);

    /* Before we use this translation, make sure we still have a customer waiting */
    t->peerLen = sizeof(t->Peer);

    // accept blocks till we have a connection, then the returned socket is actually a new socket
    s = MoveFileDescriptorHigh(accept(inSocket, (struct sockaddr *)&t->Peer, &t->peerLen));

    memcpy(&t->sin_addr, &t->Peer, sizeof(t->sin_addr));

    guardian->mainLocation = 1002;

    if (s == INVALID_SOCKET)
        return 0;

    if (debugLevel > 19 || optionLogThreadStartup)
        Log("%" SOCKET_FORMAT " Accept socket, errno=%d", s, (s == INVALID_SOCKET) ? errno : 0);

    guardian->mainLocation = 1003;

    t->intruderIP = NULL;
    t->intruderIPLevel = -1;

    /* With OptionAcceptXForwardedFor, we must defer this check until we can see the X-Forwarded-For
     * header */
    if (optionAcceptXForwardedFor == 0) {
        reject = CheckForIPReject(t);
        /* With 1, there is no REJECTHTM so just close down quick and easy */
        if (reject == 1) {
            /* Disable linger, to make this a quick, fast close */
            linger.l_onoff = 1;
            linger.l_linger = 0;
            setsockopt(s, SOL_SOCKET, SO_LINGER, (char *)&linger, sizeof(linger));
            closesocket(s);
            return 0;
        }
    }

    /* Change accepted socket to perform blocking since that's what the ParseXXX routines expect */
    /* SocketBlocking(s); */

    /*
    len = sizeof(sl);
    if (getsockname(s, (struct sockaddr *) &sl, &len) == 0) {
        hp = gethostbyaddr((char *) &sl.sin_addr, sizeof(sl.sin_addr), sl.sin_family);
        if (hp)
            printf("Connection on %s\n", hp->h_name);
        else
            printf("Connection on %s\n", UUinet_ntoa(ipBuffer, sp.sin_addr));
    }
    */

    /*
    hp = gethostbyaddr((char *) &t->peer.sin_addr, sizeof(t->peer.sin_addr), t->peer.sin_family);
    if (hp)
        printf("Connection from %s\n", hp->h_name);
    else
        printf("Connection from %s\n", UUinet_ntoa(ipBuffer, t->peer.sin_addr));
    */

    guardian->mainLocation = 1004;

    setsockopt(s, SOL_SOCKET, SO_KEEPALIVE, (char *)&one, sizeof(one));

    /* LINGER disabled since it was driving UMich crazy, and seems irrelevant now */
    /* This will force the eventual closesocket request to be a forced, hard close */
    linger.l_onoff = 1;
    linger.l_linger = 0;
    /*  setsockopt(s, SOL_SOCKET, SO_LINGER, (char *) &linger, sizeof(linger)); */

    guardian->mainLocation = 1005;
    /* Make socket non-blocking since that's what everyone expects in here now */
    SocketNonBlocking(s);
    guardian->mainLocation = 1006;

    if (optionDisableNagle)
        SocketDisableNagle(s);

    t->session = NULL;

    guardian->mainLocation = 1007;

    // really initializes some t fields and
    // points t->host = h
    InitTranslate(t, h);
    t->reject = reject;

    guardian->mainLocation = 1008;

    // initialize some more t fields
    t->useSsl = useSsl;
    t->acceptSslIdx = acceptSslIdx;
    t->loginPort = lp;
    t->myUrl = (t->useSsl && myUrlHttps ? myUrlHttps : myUrlHttp);
    t->logPort = logPort;
    t->clientKeepAlive = t->serverKeepAlive = 0;
    /* After DoTranslate stops, this will be redone to SSL as relevant */
    UUInitSocketStream(&t->ssocket, s);

    guardian->mainLocation = 1009;

    StrCpy3(hostAndPort, h->hostname, HOSTMAXLEN);
    sprintf(AtEnd(hostAndPort), ":%d", h->myPort);

    // marks the start time of socket and does some other manipulations on it
    TSOpenSocket(&t->ssocket, hostAndPort, "DispatchSocket");

    guardian->mainLocation = 1010;

    // launches the translation in main or as new thread
    // if this fails, a 503 is sent to user and result of 1
    // 0 means success
    if (StartTranslate(t) == 0) {
        /* We'll accept up to 5 connections, but then we have to move back to the main loop
           or we could seize it up
        */
        if (tries < 5)
            goto AcceptAgain;
    }

    return 0;
}

void DoRefreshShibbolethMetadata(void *v) {
    for (;;) {
        ChargeAlive(GCREFRESHSHIBBOLETHMETADATA, NULL);
        SetErrno(0);
        RetrieveLoadUpdateShibbolethMetadatas();
#ifndef WIN32
        if ((errno == ECANCELED)) {
            break;
        }
#endif
        if (debugLevel > 1)
            Log("RefreshShibbolethMetadata waiting...");

        SetErrno(0);
        SubSleep(ONE_DAY_IN_SECONDS, 0);
#ifndef WIN32
        if ((errno == ECANCELED)) {
            break;
        }
#endif
        if (debugLevel > 1)
            Log("RefreshShibbolethMetadata cycling...");
    }
    ChargeStopped(GCREFRESHSHIBBOLETHMETADATA, NULL);
}

time_t SecurityRuleComputeNextDigestEmail(time_t securityRuleNextDigestEmail) {
    time_t now;
    struct tm tm;
    char buffer[MAXASCIIDATE];

    UUtime(&now);
    // Add 1 to force to next day if we are at the exact same time
    now += 1;
    UUlocaltime_r(&now, &tm);
    // Default to 11:59 P.M.
    tm.tm_hour = 23;
    tm.tm_min = 59;
    tm.tm_sec = 0;
    securityRuleNextDigestEmail = mktime(&tm);
    if (securityRuleNextDigestEmail < now) {
        securityRuleNextDigestEmail += ONE_DAY_IN_SECONDS;
    }

    if (debugLevel > 0) {
        Log("Security rule next digest email at %s",
            ASCIIDate(&securityRuleNextDigestEmail, buffer));
    }
    return securityRuleNextDigestEmail;
}

void DoSendSecurityRuleDigestEmail(void *v) {
    DWORD currTick, lastTick;
    DWORD interval = 60 * 1000;
    time_t now;
    struct tm tm;
    lastTick = 0;
    time_t securityRuleNextDigestEmail = 0;
    securityRuleNextDigestEmail = SecurityRuleComputeNextDigestEmail(securityRuleNextDigestEmail);
    for (;;) {
        ChargeAlive(GCSENDSECURITYRULEDIGESTEMAIL, &now);
        currTick = GetTickCount();
        if (GetTickDiff(currTick, lastTick) > interval) {
            lastTick = currTick;
        } else {
            if (now >= securityRuleNextDigestEmail) {
                // Make sure we take the day of the week from the same time that drove this digest
                // email
                // Use this in case someone sets purge time to 23:59 and purge doesn't end until
                // after midnight.
                UUlocaltime_r(&securityRuleNextDigestEmail, &tm);
                DeleteExpiredNotificationTransactions();
                SendSecurityRuleDigestEmail();
                securityRuleNextDigestEmail =
                    SecurityRuleComputeNextDigestEmail(securityRuleNextDigestEmail);
            }
            SubSleep(0, 100);
        }
    }
}

void DoQueuedSecurityRuleEmail(void *v) {
    DWORD currTick, lastTick;
    DWORD interval = 60 * 1000 * 10;  // Every 10 minutes
    time_t now;
    struct tm tm;
    lastTick = 0;
    for (;;) {
        ChargeAlive(GCSENDSECURITYRULEDIGESTEMAIL, &now);
        currTick = GetTickCount();
        if (GetTickDiff(currTick, lastTick) > interval) {
            lastTick = currTick;
            DeleteExpiredNotificationTransactions();
            SendQueuedSecurityRuleEmail();
        } else {
            // Sleep for a minute
            SubSleep(60, 0);
        }
    }
}

void BeginStopSockets(void) {
    if (UUBeginThread(DoStopSockets, 0, guardianChargeName[GCSTOPSOCKETS]))
        PANIC;
}

void BeginHandleRaw(void) {
    if (UUBeginThread(DoHandleRaw, 0, guardianChargeName[GCHANDLERAW]))
        PANIC;
}

void BeginRefreshShibbolethMetadata(void) {
    if (UUBeginThread(DoRefreshShibbolethMetadata, 0,
                      guardianChargeName[GCREFRESHSHIBBOLETHMETADATA]))
        PANIC;
}

void BeginSendSecurityRuleDigestEmail(void) {
    if (UUBeginThread(DoSendSecurityRuleDigestEmail, 0,
                      guardianChargeName[GCSENDSECURITYRULEDIGESTEMAIL]))
        PANIC;
}

void BeginSendQueuedSecurityRuleEmail(void) {
    if (UUBeginThread(DoQueuedSecurityRuleEmail, 0,
                      guardianChargeName[GCSENDQUEUEDSECURITYRULEEMAIL]))
        PANIC;
}

static void BeginRefreshWskeyLicense(struct WSKEYLICENSE *wslicense) {
    if (UUBeginThread(WskeyBeginThread, wslicense, guardianChargeName[GCWSKEYLICENSE]))
        PANIC;
}

void BeginCluster(void) {
    if (UUBeginThread(DoCluster, 0, guardianChargeName[GCCLUSTER]))
        PANIC;
}

void BeginDNS(void) {
    if (cDnsPorts) {
        if (UUBeginThread(DoDNS, 0, guardianChargeName[GCDNS]))
            PANIC;
    }
}

void BeginSaveUsage(void) {
    if (UUBeginThread(DoSaveUsage, 0, guardianChargeName[GCSAVEUSAGE]))
        PANIC;
}

/* GenerateKey was for Harvard, no longer used
void GenerateKey(char *id)
{
    struct tm *tm, rtm;
    time_t now;
    size_t max;
*/
/* 14 is @ + 12 for date + 1 for final NULL */
/*
    max = MAXKEY - 14;

    if (strlen(id) > max) {
        printf("-g parameter may not exceed %d characters\n", max);
        return;
    }
    UUtime(&now);
    tm = UUlocaltime_r(&now, &rtm);
    printf("@%04d%02d%02d%02d%02d%s\n", tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
        tm->tm_hour, tm->tm_min, id);
}
*/

void CheckArguments(int argc,
                    char **argv,
                    int *ifmode,
                    char *ifsubtype,
                    char **checkPort,
                    char **guardianMap,
                    BOOL started) {
    int i;
    char *p = NULL;

    if (ifmode)
        *ifmode = IFTEST;
    if (ifsubtype)
        *ifsubtype = 0;
    if (checkPort != NULL)
        *checkPort = 0;
    if (guardianMap)
        *guardianMap = NULL;

    for (i = 1; i < argc; i++) {
        if (stricmp(argv[i], "-ttyDetach") == 0) {
            ttyDetach = TRUE;
        }

        if (strncmp(argv[i], "-D", 2) == 0) {
            if (strlen(argv[i]) > 2) {
                p = argv[i] + 2;
            } else if (i + 1 < argc) {
                i++;
                p = argv[i];
            } else {
                Usage(argv[0]);
                exit(0);
            }
            ReadConfigDebug(p);
        }

        if (strncmp(argv[i], "-T", 2) == 0) {
            if (strlen(argv[i]) > 2) {
                p = argv[i] + 2;
            } else if (i + 1 < argc) {
                i++;
                p = argv[i];
            }
            /* Here we explicitly ignore the "T" parameter. It's used as a tag on the command line
             * so something like "ps -ef | grep TABC" can find the commands so tagged. */
        }

        if (strncmp(argv[i], "-E", 2) == 0) {
            if (strlen(argv[i]) > 2) {
                p = argv[i] + 2;
            } else if (i + 1 < argc) {
                i++;
                p = argv[i];
            } else {
                exit(0);
            }
            ReadConfigEnvironment(p, TRUE);
        }

        if (strncmp(argv[i], "-m", 2) == 0) {
            if (ifmode)
                *ifmode = IFMISSING;
            if (ifsubtype)
                *ifsubtype = argv[i][2];
        }

        if (strncmp(argv[i], "-r", 2) == 0) {
            if (ifmode)
                *ifmode = IFREPLACE;
            if (ifsubtype)
                *ifsubtype = argv[i][2];
        }

        if (strcmp(argv[i], "-c") == 0) {
            if (checkPort)
                *checkPort = "";
            if (i + 1 < argc) {
                if (checkPort)
                    *checkPort = argv[i + 1];
                i++;
            }
        }

#ifdef WIN32
        if (strcmp(argv[i], "-si") == 0) {
            CmdInstallService(0);
            exit(0);
        }

        if (strcmp(argv[i], "-sw") == 0) {
            printf("This option was withdrawn in " EZPROXYNAMEPROPER " 2.0m.  Please see\n");
            printf("http://www.oclc.org/support/documentation/ezproxy/technote/1.htm\n");
            exit(0);
        }

        if (strcmp(argv[i], "-sr") == 0) {
            CmdRemoveService(i + 1 < argc ? argv[i + 1] : "");
            exit(0);
        }

        if (strcmp(argv[i], "-sl") == 0) {
            CmdListServices();
            exit(0);
        }
#else

        if (strcmp(argv[i], "-si") == 0) {
            StartupInstall();
            exit(0);
        }

        if (strcmp(argv[i], "-sr") == 0) {
            StartupRemove();
            exit(0);
        }
#endif

        // registering a license key
        if (strncmp(argv[i], "-k", 2) == 0 && started) {
            /* If we are running as root and there is a RunAs value,
             * lower privilege to avoid writing license with wrong ownership.
             */
#ifndef WIN32
            ReadConfig(1);
            SetUidGid();
#endif
            // no space was used between -k and the key
            if (strlen(argv[i]) > 2) {
                Validate2(argv[i] + 2);
                // Let running EZproxy know that key changed
                EZproxyControl(6, NULL, NULL);
            } else if (i + 1 < argc) {
                // a space was used between -k and key, so send the next arg as the key
                Validate2(argv[i + 1]);
                // Let running EZproxy know that key changed
                EZproxyControl(6, NULL, NULL);
            } else {
                // bad usage of -k, basically no key was given
                Usage(argv[0]);
            }
            exit(0);
        }

        if (strcmp(argv[i], "-?") == 0 || strcmp(argv[i], "-h") == 0) {
            Usage(argv[0]);
            exit(0);
        }

        /* This was for Harvard at one point, but is no longer used
        if (strcmp(argv[i], "-g") == 0 && i + 1 < argc) {
            GenerateKey(argv[i + 1]);
            exit(0);
        }
        */

        if (strcmp(argv[i], GUARDIANMAPOPTION) == 0 && i + 1 < argc) {
            if (guardianMap)
                *guardianMap = argv[++i];
        }

        if (strcmp(argv[i], "-v") == 0) {
            ShowVersion(1);
            WskeyInfo();
            exit(0);
        }

        if (strcmp(argv[i], "-d") == 0) {
            if (i + 1 == argc) {
                Usage(argv[0]);
                exit(0);
            }
            i++;
            if (myChangedPath == NULL) {
                if (chdir(argv[i]) != 0) {
                    Log("Unable to change directory to %s", argv[i]);
                    exit(0);
                }
                myChangedPath = argv[i];
            }
        }

        if (stricmp(argv[i], "stop") == 0) {
            EZproxyControl(0, argv[i], i < argc ? argv[i + 1] : NULL);
        }

        if (stricmp(argv[i], "start") == 0) {
            EZproxyControl(1, argv[i], i < argc ? argv[i + 1] : NULL);
            /* make the start disappear if we're really starting */
            argc = i - 1;
            break;
        }

        if (stricmp(argv[i], "stopall") == 0) {
#ifdef WIN32
            fprintf(stderr, "stopall is not available in " EZPROXYNAMEPROPER " for Windows\n");
#else
#define expansion_str(s) str(s)
#define str(s) #s

            char *currentPath = getenv("PATH");
            char *morePath = "PATH=/bin:/usr/bin:/sbin:/usr/sbin";
            char *newPath = NULL;
            char *cmdTerm = "\
ps -eo user,pid,comm,args \
| awk '$3 == \"" EZPROXYNAMEPROPER_ALL_LOWER "\" && $4$5$6$7$8$9 !~ /stopall/ { print \"kill -" expansion_str(SIGTERM) " \" $2 \" >/dev/null 2>&1 \
 && echo Stopped process \" $2 \" \
 || echo Unable to stop process \" $2 \" running under account \" $1 }' \
| sh";
            char *cmdKill = "\
ps -eo user,pid,comm,args \
| awk '$3 == \"" EZPROXYNAMEPROPER_ALL_LOWER "\" && $4$5$6$7$8$9 !~ /stopall/ { print \"kill -" expansion_str(SIGKILL) " \" $2 \" >/dev/null 2>&1 \
 && echo Stopped process \" $2 \" \
 || echo Unable to stop process \" $2 \" running under account \" $1 }' \
| sh";

            if (currentPath == NULL)
                newPath = morePath;
            else {
                /* +2 for the : between and the null at the end */
                if (!(newPath = malloc(strlen(currentPath) + strlen(morePath) + 2)))
                    PANIC;
                sprintf(newPath, "%s:%s", morePath, currentPath);
            }
            putenv(newPath);

            printf("Attempting to SIGTERM all running processes named '" EZPROXYNAMEPROPER_ALL_LOWER
                   "'.\n");
            fflush(stdout);
            system(cmdTerm);
            SubSleep(4, 0);
            printf("Attempting to SIGKILL all running processes named '" EZPROXYNAMEPROPER_ALL_LOWER
                   "'.\n");
            fflush(stdout);
            system(cmdKill);
            SubSleep(4, 0);
            unlink(IPCFILE);
            unlink(IPCFILELOCK);
#endif
            exit(0);
        }

        if (stricmp(argv[i], "restart") == 0 || stricmp(argv[i], "bounce") == 0) {
            EZproxyControl(2, argv[i], i < argc ? argv[i + 1] : NULL);
            /* make the restart disappear if we're really starting */
            argc = i - 1;
            break;
        }

        if (stricmp(argv[i], "log") == 0) {
            EZproxyControl(3, argv[i], i < argc ? argv[i + 1] : NULL);
        }

        if (stricmp(argv[i], "status") == 0) {
            EZproxyControl(4, argv[i], i < argc ? argv[i + 1] : NULL);
        }

        if (stricmp(argv[i], "kill") == 0) {
            EZproxyControl(5, argv[i], i < argc ? argv[i + 1] : NULL);
        }

        if (stricmp(argv[i], "account") == 0) {
            AccountMain(argc, argv, i);
        }
        if (stricmp(argv[i], "obscure") == 0) {
            if (i + 1 < argc) {
                char *obscure = NULL;
                obscure = ObscureString(argv[i + 1]);
                printf("%s\n", obscure);
                FreeThenNull(obscure);
            }
            exit(0);
        }
        if (stricmp(argv[i], "obscurefile") == 0) {
            if (i + 1 < argc) {
                ObscureFile(argv[i + 1]);
            }
            exit(0);
        }
        if (stricmp(argv[i], "sha512") == 0) {
            if (i + 1 < argc) {
                char *sha512 = NULL;
                sha512 = SHA512String(argv[i + 1], NULL);
                printf("%s\n", sha512);
                FreeThenNull(sha512);
            }
            exit(0);
        }
        if (stricmp(argv[i], "segfault") == 0) {
            char *invalidAddress = NULL;
            DisableDebugger();
            printf("Attempting to trigger diagnostic segment fault\n");
            *invalidAddress = 0;
            printf("Failed to trigger diagnostic segment fault\n");
            exit(0);
        }
    }
}

int setupMemDatabases() {
    int size = mDatabases * sizeof(struct DATABASE);
    databases = (struct DATABASE *)calloc(mDatabases, sizeof(struct DATABASE));
    if (databases == NULL) {
        Log("Exhausted memory databases");
        exit(1);
    }

    if (debugLevel > 16)
        Log("Allocated memory databases: %d.", size);

    return size;
}

int setupMemDnsPorts() {
    if (mDnsPorts == 0)
        dnsPorts = NULL;
    else {
        int size = mDnsPorts * sizeof(struct DNSPORT);
        dnsPorts = (struct DNSPORT *)calloc(mDnsPorts, sizeof(struct DNSPORT));
        if (dnsPorts == NULL) {
            Log("Exhausted memory dnsPorts");
            exit(1);
        }
        if (debugLevel > 16)
            Log("Allocated memory dnsPorts: %d.", size);

        return size;
    }
    return 0;
}

int setupMemSessions() {
    int size = mSessions * sizeof(struct SESSION);
    sessions = (struct SESSION *)calloc(mSessions, sizeof(struct SESSION));
    if (sessions == NULL) {
        Log("Exhausted memory sessions");
        exit(1);
    }
    if (debugLevel > 16)
        Log("Allocated memory sessions: %d.", size);

    return size;
}

int setupMemHosts() {
    /* Leave two extra for the MV http and https hosts */
    int size = (mHosts + 2) * sizeof(struct HOST);
    hosts = (struct HOST *)calloc(mHosts + 2, sizeof(struct HOST));
    if (hosts == NULL) {
        Log("Exhausted memory hosts");
        exit(1);
    }
    if (debugLevel > 16)
        Log("Allocated memory hosts: %d.", size);

    return size;
}

int setupMemProxyHosts() {
    mNeverProxyHosts = 5000;

    int size = mNeverProxyHosts * sizeof(*neverProxyHosts);
    neverProxyHosts = calloc(mNeverProxyHosts, sizeof(*neverProxyHosts));
    if (neverProxyHosts == NULL) {
        Log("Exhausted memory neverProxyHosts");
        PANIC;
    }
    if (debugLevel > 16)
        Log("Allocated memory neverProxyHosts: %d.", size);

    return size;
}

int setupMemPeers() {
    if (mPeers == 0)
        peers = NULL;
    else {
        int size = mPeers * sizeof(struct PEER);
        peers = (struct PEER *)calloc(mPeers, sizeof(struct PEER));
        if (peers == NULL) {
            Log("Exhausted memory peers");
            exit(1);
        }
        if (debugLevel > 16)
            Log("Allocated memory peers: %d.", size);

        return size;
    }
    return 0;
}

int setupMemReroutes() {
    if (mReroutes == 0)
        reroutes = NULL;
    else {
        int size = mReroutes * sizeof(struct REROUTE);
        reroutes = (struct REROUTE *)calloc(mReroutes, sizeof(struct REROUTE));
        if (reroutes == NULL) {
            Log("Exhausted memory reroutes");
            exit(1);
        }
        if (debugLevel > 16)
            Log("Allocated memory reroutes: %d.", size);

        return size;
    }
    return 0;
}

int setupMemTranslates() {
    int size = mTranslates * sizeof(struct TRANSLATE);
    translates = (struct TRANSLATE *)calloc(mTranslates, sizeof(struct TRANSLATE));
    if (translates == NULL) {
        Log("Exhausted memory translates");
        exit(1);
    }
    if (debugLevel > 16)
        Log("Allocated memory translates: %d.", size);

    return size;
}

int setupMemIpTypes() {
    int size;
    int totalSize = 0;

    if (mIpTypes == 0) {
        ipTypes = NULL;
    } else {
        size = mIpTypes * sizeof(*ipTypes);
        totalSize += size;
        ipTypes = (struct IPTYPE *)calloc(1, size);
        if (ipTypes == NULL) {
            Log("Exhausted memory ipTypes");
            exit(1);
        }
        if (debugLevel > 16) {
            Log("Allocated memory ipTypes: %d.", size);
        }
    }

    if (mRejectIpTypes == 0) {
        rejectIpTypes = NULL;
    } else {
        size = mRejectIpTypes * sizeof(*rejectIpTypes);
        totalSize += size;
        rejectIpTypes = (struct IPTYPE *)calloc(1, size);
        if (rejectIpTypes == NULL) {
            Log("Exhausted memory rejectIpTypes");
            exit(1);
        }
        if (debugLevel > 16) {
            Log("Allocated memory rejectIpTypes: %d.", size);
        }
    }

    if (mWhitelistIPs == 0) {
        whitelistIPs = NULL;
    } else {
        size = mWhitelistIPs * sizeof(*whitelistIPs);
        totalSize += size;
        whitelistIPs = (struct IPTYPE *)calloc(1, size);
        if (whitelistIPs == NULL) {
            Log("Exhausted memory whiteListIPs");
            exit(1);
        }
    }

    return totalSize;
}

int setupMemPdfRefreshes() {
    if (mPdfRefreshes == 0)
        pdfRefreshes = NULL;
    else {
        int size = mPdfRefreshes * sizeof(struct PDFREFRESH);
        pdfRefreshes = (struct PDFREFRESH *)calloc(mPdfRefreshes, sizeof(struct PDFREFRESH));
        if (pdfRefreshes == NULL) {
            Log("Exhausted memory pdfRefreshes");
            exit(1);
        }
        if (debugLevel > 16)
            Log("Allocated memory pdfRefreshes: %d.", size);
        return size;
    }
    return 0;
}

int setupMemStartingPointUrlRefreshes() {
    if (mStartingPointUrlRefreshes == 0)
        startingPointUrlRefreshes = NULL;
    else {
        int size = mStartingPointUrlRefreshes * sizeof(struct STARTINGPOINTURLREFRESH);
        startingPointUrlRefreshes = (struct STARTINGPOINTURLREFRESH *)calloc(
            mStartingPointUrlRefreshes, sizeof(struct STARTINGPOINTURLREFRESH));
        if (startingPointUrlRefreshes == NULL) {
            Log("Exhausted memory startingPointUrlRefreshes");
            exit(1);
        }
        if (debugLevel > 16)
            Log("Allocated memory startingPointUrlRefreshes: %d.", size);
        return size;
    }
    return 0;
}

int setupMemLogFilters() {
    if (mLogFilters == 0)
        logFilters = NULL;
    else {
        int size = mLogFilters * sizeof(struct LOGFILTER);
        logFilters = (struct LOGFILTER *)calloc(mLogFilters, sizeof(struct LOGFILTER));
        if (logFilters == NULL) {
            Log("Exhausted memory logFilters");
            exit(1);
        }
        if (debugLevel > 16)
            Log("Allocated memory logFilters: %d.", size);
        return size;
    }
    return 0;
}

int setupMemAnonymousUrls() {
    if (mAnonymousUrls == 0)
        anonymousUrls = NULL;
    else {
        int size = mAnonymousUrls * sizeof(struct ANONYMOUSURL);
        anonymousUrls = (struct ANONYMOUSURL *)calloc(mAnonymousUrls, sizeof(struct ANONYMOUSURL));
        if (anonymousUrls == NULL) {
            Log("Exhausted memory anonymousUrls");
            exit(1);
        }
        if (debugLevel > 16)
            Log("Allocated memory anonymousUrls: %d.", size);
        return size;
    }
    return 0;
}

int setupMemLoginPorts() {
    int size = mLoginPorts * sizeof(struct LOGINPORT);
    loginPorts = (struct LOGINPORT *)calloc(mLoginPorts, sizeof(struct LOGINPORT));
    if (loginPorts == NULL) {
        Log("Exhausted memory loginPorts");
        exit(1);
    }
    if (debugLevel > 16)
        Log("Allocated memory loginPorts: %d.", size);

    return size;
}

int setupMemHostNameEdits() {
    if (mProxyHostnameEdits == 0)
        proxyHostnameEdits = NULL;
    else {
        int size = mProxyHostnameEdits * sizeof(struct PROXYHOSTNAMEEDIT);
        proxyHostnameEdits = (struct PROXYHOSTNAMEEDIT *)calloc(mProxyHostnameEdits,
                                                                sizeof(struct PROXYHOSTNAMEEDIT));
        if (proxyHostnameEdits == NULL) {
            Log("Exhausted memory proxyHostnameEdits");
            exit(1);
        }
        if (debugLevel > 16)
            Log("Allocated memory proxyHostnameEdits: %d.", size);
        return size;
    }
    return 0;
}

void setupMemMimeFiltersRegex(struct MIMEFILTER *ptr, char *mimePattern, char *uriPattern) {
    char const *pcreError;  // pcreError will point to a static string, so don't try to free it!
    int pcreErrorOffset;

    ptr->regexMime = pcre_compile(mimePattern, PCRE_CASELESS, &pcreError, &pcreErrorOffset, NULL);
    if (ptr->regexMime == NULL) {
        Log("MIMEFILTER default setup failure: %s at %d: %s", pcreError, pcreErrorOffset,
            mimePattern);
        exit(1);

        if ((ptr->regexMimeExtra = pcre_study(ptr->regexMime, 0, &pcreError)) == NULL) {
            if (pcreError != NULL) {
                Log("MIMEFILTER default setup failure: %s: %s", &pcreError, mimePattern);
                exit(1);
            }
        }
    }

    ptr->regexUri = pcre_compile(uriPattern, PCRE_CASELESS, &pcreError, &pcreErrorOffset, NULL);
    if (ptr->regexUri == NULL) {
        Log("MIMEFILTER default setup failure: %s at %d: %s", pcreError, pcreErrorOffset,
            uriPattern);
        exit(1);

        if ((ptr->regexUriExtra = pcre_study(ptr->regexUri, 0, &pcreError)) == NULL) {
            if (pcreError != NULL) {
                Log("MIMEFILTER default setup failure: %s: %s", &pcreError, mimePattern);
                exit(1);
            }
        }
    }
}

int setupMemMimeFilters() {
    int defaultEntries = 7;
    int size = (cMimeFilters + defaultEntries) * sizeof(struct MIMEFILTER);
    mimeFiltersDefault =
        (struct MIMEFILTER *)calloc((cMimeFilters + defaultEntries), sizeof(struct MIMEFILTER));

    if (mimeFiltersDefault == NULL) {
        Log("Exhausted memory mimeFilters");
        exit(1);
    }
    if (debugLevel > 16)
        Log("Allocated memory mimeFilters %d", size);

    // setup the default entries
    struct MIMEFILTER *mimePtr = mimeFiltersDefault;

    // setup html
    mimePtr->action = html;
    setupMemMimeFiltersRegex(mimePtr, "text/html", ".*");
    mimePtr->next = mimeFiltersDefault + 1;

    // catch by html files
    mimePtr = mimePtr->next;
    mimePtr->action = html;
    setupMemMimeFiltersRegex(mimePtr, ".*", ".*\\.s?html?");
    mimePtr->next = mimeFiltersDefault + 2;

    mimePtr = mimePtr->next;
    mimePtr->action = image;
    setupMemMimeFiltersRegex(mimePtr, ".*", ".*\\.((png)|(jpe?g)|(gif))");
    mimePtr->next = mimeFiltersDefault + 3;

    // setup pdf
    mimePtr = mimePtr->next;
    mimePtr->action = pdf;
    setupMemMimeFiltersRegex(mimePtr, "application/pdf", ".*");
    mimePtr->next = mimeFiltersDefault + 4;

    // setup text/plain
    mimePtr = mimePtr->next;
    mimePtr->action = text;
    setupMemMimeFiltersRegex(mimePtr, "text/plain", ".*");
    mimePtr->next = mimeFiltersDefault + 5;

    // setup javascript by extension
    mimePtr = mimePtr->next;
    mimePtr->action = javascript;
    setupMemMimeFiltersRegex(mimePtr, ".*", ".*\\.js");
    mimePtr->next = mimeFiltersDefault + 6;

    // setup javascript
    mimePtr = mimePtr->next;
    mimePtr->action = javascript;
    setupMemMimeFiltersRegex(
        mimePtr,
        "^((text\\/css)|(text\\/xml)|(text\\/javascript)|(application\\/json)|(application\\/"
        "((rdf\\+)|(xhtml\\+)|(rss\\+))?xml)|(application\\/(x\\-)?javascript)|(video\\/"
        "x\\-ms\\-wvx))$",
        ".*");
    mimePtr->next = NULL;

    mimeFiltersUser = (cMimeFilters == 0) ? NULL : mimeFiltersDefault + defaultEntries;

    return size;
}

int setupMemAll(void) {
    int totalSize = 0;
    totalSize += setupMemDatabases();
    totalSize += setupMemDnsPorts();
    totalSize += setupMemSessions();
    totalSize += setupMemHosts();
    totalSize += setupMemProxyHosts();

    // dont know what this is about
    neverProxyHostsLast = neverProxyHosts + (mNeverProxyHosts - 1);

    totalSize += setupMemPeers();
    totalSize += setupMemReroutes();
    totalSize += setupMemTranslates();
    totalSize += setupMemIpTypes();
    totalSize += setupMemPdfRefreshes();
    totalSize += setupMemStartingPointUrlRefreshes();
    totalSize += setupMemLogFilters();
    totalSize += setupMemAnonymousUrls();
    totalSize += setupMemLoginPorts();
    totalSize += setupMemHostNameEdits();
    totalSize += setupMemMimeFilters();

    return totalSize;
}

/**
 * Starts up EZproxy.
 * If this was windows, then this was launched by service.c
 * Also there are a lot of globals accessed here
 * @see service.h
 * @see globals.c
 */
int StartEZproxy(int argc, char **argv, BOOL runAsService) {
    int rc = -1;
    int i;
    int pMHosts;
    SOCKET maxSock = SOCKET_MIN;
    SOCKET minSock = SOCKET_MAX;
    int nSock = 0;

    BOOL needDelay;
    time_t now;
    time_t nextStatus;
    time_t nextAuditPurge;
    time_t nextPeak = 0;
    int totalSize = 0;
    int size = 0;
    int ifmode;
    int dnsStatus = 0;
    char ifsubtype;
    char *checkPort = NULL;
    BOOL changed;
    struct HOST *h;
    char buffer[533];
    struct LOGINPORT *lp;
    BOOL numericHost = 0;
    char *guardianMap;
    intmax_t nCpu = 1L;

#ifdef USEPOLL
    struct pollfd *pfds, *pfdsMaster;
    unsigned int pfdc = 0, pfdcMaster = 0;
#else
    struct timeval pollInterval;

    // fd is for file descriptor table.  This table
    // contains pointers to all open i/o streams
    // the first 3 entries are for
    // standard input, standard out, standard error.  The socket
    // system call returns an entry into this table.  The accept
    // system call also adds an entry to this table which is used
    // read/writes
    fd_set rfds, rfdsMaster;
#endif

    // startupTime is a global.
    UUtime(&startupTime);

    // For historic reasons related to export restrictions and pre-OCLC licensing practices,
    // initializing SSL was previously delayed until absolutely necessary. Those reason
    // no longer apply so now we initialize this as the very start to ensure everything
    // that depends on it will have it ready.
    UUInitSsl();

    nCpu = cpusAvailable();

    /* Start out with lastModifiedMinimum as startup time; may be reduced if read from ezproxy.hst
     */
    lastModifiedMinimum = startupTime;
    InitMutexes();
    InitUUAvlTrees();
    putenv("LDAPNOINIT=1");

    /* Allow NumericHost to initialize its static patterns while we are
       still single-threaded
    */
    NumericHost(NULL);

    GetMyBinaryAndMyPath(argv[0]);

    xmlInitParser();

    ifmode = IFTEST;
    ifsubtype = 0;
    checkPort = NULL;
    guardianMap = NULL;

#ifdef WIN32
    runningAsService = runAsService;
    /* report the status to the service control manager. */
    if (runAsService)
        if (!ReportStatusToSCMgr(SERVICE_RUNNING, /* service state */
                                 NO_ERROR,        /* exit code */
                                 0))              /* wait hint */
            return 0;

    if ((netapi32Dll = LoadLibrary("NETAPI32.DLL")))
        sNetUserChangePassword =
            (LPNetUserChangePassword)GetProcAddress(netapi32Dll, "NetUserChangePassword");
    if (sNetUserChangePassword == NULL) {
        Log("NetUserChangePassword missing (normal for Win 95/98)");
        Log("Domain password changing disabled");
    }

    StartWinsock();

#else
    // set permissions for files and such used by this
    // application to only have access by ezproxy..so I think
    umask(0077);
    if (pthread_attr_init(&pthreadCreateDetached) != 0)
        PANIC;
    if (pthread_attr_setdetachstate(&pthreadCreateDetached, PTHREAD_CREATE_DETACHED) != 0)
        PANIC;
    if (pthread_attr_setscope(&pthreadCreateDetached, PTHREAD_SCOPE_SYSTEM) != 0)
        PANIC;

    changedUid = changedGid = runUid = runGid = 0;
#endif

    // the key check can result in checking against wskey service, so
    // we needed winsock already setup, not sure if this was necessary
    // for linux
    CheckArguments(argc, argv, &ifmode, &ifsubtype, &checkPort, &guardianMap, TRUE);

    // checks the port works and also saves
    // the primaryLoginPort.  PrimaryLoginPort is
    // set in the database.c during the read.
    if (checkPort) {
        CheckConnection(checkPort);
        exit(0); /* A command line arg has us only checking the port and nothing else. */
    }

    // promt user to setup default files or something
    InstallFiles(argv[0], ifmode, ifsubtype);

    if (optionProxyType == PTHOST) {  // This statement does nothing and should be removed.
        mHosts = DEFMAXHOSTS2;
    }
    pMHosts = mHosts;

    /*
     * After here, we are destine to become either the Guardian or the Charge.
     */

    /* We need to read in the runUid and runGid, umask, the options, max values, XDEBUG, and
     * Environment without reading the other configuration data yet */
    ReadConfig(1);

    UUAcquireMutex(&mMsgFile);
    CloseMsgFile();
    /* no terminal input after this point */
    NullFD(0);

    /* Now that we know the umask we can open the message file. */
    OpenMsgFile();
    UUReleaseMutex(&mMsgFile);

    /* Now that the message file is open, report into the message file just like the command line
     * arg "-v". */
    ShowVersion(0);

    /* ********************* Start Guardian process ************************* */

    if (Guardian(guardianMap, argc, argv))
        goto FinishedGuardian; /* Guardian is terminating */

    /* ********************* Start the Charge process ************************
     * Guardian is already running and has started this child process.
     * Any exit(0) call after this point will cause the child to be restarted by the guardian unless
     * guardian->chargeStatus = chargeStopping is set first.
     */

    if (strncmp(guardian->version, GUARDIANVERSION, strlen(guardian->version)) != 0) {
        Log("Guardian version mismatch; " EZPROXYNAMEPROPER
            " upgraded without stopping old version; aborting");
        exit(1);
    }

    AdminSslCheckWildcardsHttps(0);

    if (optionAllowDebugger == 0) {
        DisableDebugger();
    }
#ifndef WIN32
    EstablishChargeSignalHandlers();
#endif

    // just prepends fixes up the url for https? and :port number
    // uses primaryLoginPort
    SetMyUrlHttp();
    SetMyUrlHttps();

    changed = 0;
    if (mHosts != pMHosts) {
        changed = 1, Log("MaxVirtualHosts (MV) changed from %d to %d", pMHosts, mHosts);
    } else {
        Log("MaxVirtualHosts set to default %d", mHosts);
    }

    if (mTranslates != DEFMAXTRANSLATES)
        changed = 1,
        Log("MaxConcurrentTransfers (MC) changed from %d to %d", DEFMAXTRANSLATES, mTranslates);

    if (mSessions != DEFMAXSESSIONS) {
        changed = 1, Log("MaxSessions (MS) changed from %d to %d", DEFMAXSESSIONS, mSessions);
    } else {
        Log("MaxSessions set to default %d", mSessions);
    }

    if (mSessionLife != DEFSESSIONLIFE)
        changed = 1, Log("MaxLifetime (ML) changed from %d to %d minutes", DEFSESSIONLIFE / 60,
                         mSessionLife / 60);

    // setup all the global memory allocations
    totalSize = setupMemAll();

    if (debugLevel > 16) {
        Log("MD (max databases) %d", mDatabases);
        Log("MI (max E records) %d", mIpTypes);
        Log("Allocated memory total: %d.", totalSize);
    }

#ifdef USEPOLL
    /* Twice the required is a little extreme, but it's only an 8 byte structure, so it's not that
     * much extra */
    if (!(pfds = (struct pollfd *)calloc((mTranslates * 2 + mHosts) * 2, sizeof(struct pollfd))))
        PANIC;
    if (!(pfdsMaster =
              (struct pollfd *)calloc((mTranslates * 2 + mHosts) * 2, sizeof(struct pollfd))))
        PANIC;
#endif

    /* cSessions used to be here, but now that would destroy the anonymous session */

    cSessions = cIpTypes = /* cAuthHosts = */ cGroups = cHosts = cTranslates = cDatabases =
        cLoginPorts = 0;

    /* Now we can read all the rest of the configuration information */
    ReadConfig(0);

    if (myName[0] == 0) {
        Log("Unable to determine host name, please manually specify name in %s", EZPROXYCFG);
        exit(1);
    }

    SetMyHttpName();
    SetMyHttpsName();

    numericHost = NumericHost(myName);

    if (numericHost)
        myDomain = myName;
    else {
        myDomain = strchr(myName, '.');
        /* handling the NULL case deferred until
           after login cookie domain name established AND
           error reporting about lack of periods in the server name
        */
    }

    ReadConfigHAPeerValidate();

    /*
    if (myDomain == NULL) {
        Log("Host name \"%s\" does not include domain, please manually specify name in %s", myName,
    EZPROXYCFG); exit(1);
    }

    if (strchr(myDomain + 1, '.') == NULL) {
        Log("Host name \"%s\" must contain at least two periods, please manually specify name in
    %s", myName, EZPROXYCFG); exit(1);
    }
    */

    // If the user did not specify a login cookie domain and high-availability is enabled,
    // change default to the one that normally works with high-availability.
    if (loginCookieDomainType == LOGINCOOKIEDOMAINDEFAULT && haName) {
        loginCookieDomainType = LOGINCOOKIEDOMAINDOMAINNAME;
        Log("High-availability configuration detected; cookie domain set to host domain ");
    }

    switch (loginCookieDomainType) {
    case LOGINCOOKIEDOMAINDOMAINNAME:
        /* This may transfer the NULL domain case into loginCookieName;
           this is the expected and desired behavior as SendEZproxyCookie
           expects this as a flag to omit sending the domain
        */
        loginCookieDomain = myDomain;
        break;

    case LOGINCOOKIEDOMAINDEFAULT:
    case LOGINCOOKIEDOMAINHOSTNAME:
        /* The leading period is never valid on numeric hostnames */
        if (numericHost)
            loginCookieDomain = myName;
        else {
            if (!(loginCookieDomain = malloc(strlen(myName) + 2)))
                PANIC;
            sprintf(loginCookieDomain, ".%s", myName);
        }
        if (haName) {
            Log("WARNING: HAName and LoginCookieDomain Hostname can be incompatible");
        }
        break;

    case LOGINCOOKIEDOMAINNONE:
    case LOGINCOOKIEDOMAINMANUAL:
        /* no special processing required */
        break;
    }

    Log("Server name %s", myName);

    if (numericHost)
        Log("This numeric server name will cause some databases to fail.");
    else if (myDomain == NULL)
        Log("The use of a server name with no periods will cause some databases to fail.");

    if (haName && optionSafariCookiePatch)
        Log("WARNING: HAName and Option SafariCookiePatch can be incompatible");

    if (myDomain == NULL)
        myDomain = myName;

    if (strchr(myName, '_'))
        Log("WARNING: This name contains an underscore (_) which causes cookie problems with "
            "Internet Explorer");

    if (myUrlHttp && *myUrlHttp)
        Log("Server http URL %s", myUrlHttp);
    if (myUrlHttps && *myUrlHttps)
        Log("Server https URL %s", myUrlHttps);

    Log("Proxy by %s", (optionProxyType == PTHOST ? "hostname" : "port"));

    if (optionProxyType == PTPORT) {
        Log("Your EZproxy Server is configured for proxy by port.");
        Log("Proxy by host is the preferred method for employing EZproxy.");
        Log("Proxy by port will be deprecated in a future release.");
    }

    if (shibbolethAvailable12 == SHIBBOLETHAVAILABLEON) {
        Log("Shibboleth 1.2 enabled");
        optionShibboleth = 1;
    }

    if (shibbolethAvailable13 == SHIBBOLETHAVAILABLEON) {
        Log("Shibboleth 1.3 enabled");
        optionShibboleth = 1;
    }

    if (shibbolethAvailable20 == SHIBBOLETHAVAILABLEON) {
        Log("Shibboleth 2.0 enabled");
        optionShibboleth = 1;
    }

#ifndef WIN32
    if (getuid() != geteuid()) {
        Log(EZPROXYNAMEPROPER " binary program file is setuid to %d", geteuid());
    }
#endif

    if (logReferer)
        Log("WARNING: LOGREFERER can cause " EZPROXYNAMEPROPER
            " to crash so it should not be left enabled longer than needed");

    if (optionTrackSockets)
        TSInit();

    /* Boost soft limit enough to get listeners and restored hosts running */
    CheckLimits(0);

    UUInitSslKey(0, 0);
    if (curl_global_init(CURL_GLOBAL_NOTHING) != 0)
        PANIC;

    FindHost(myName, 0, 0); /* Set up admin host */

    if (WskeyIsBeta()) {
        WskeyBetaInfo();
        if (WskeyIsBetaExpired()) {
            guardian->chargeStatus = chargeStopping;
            guardian->chargeStop = 1;
            exit(0);
        }
    }

    // initialize license and begin checking
    struct WSKEYLICENSE *wslicense = WskeyLicenseCreate(NULL);
    if (WskeyReadLicense(wslicense, FALSE) && isLicenseExpired(wslicense) && wslicense->key[0]) {
        Log("Attempting to revalidate EZproxy license key with OCLC server.");
        Validate2(wslicense->key);
    } else {
        WskeyInfo();
    }

    // Don't start monitoring the license until further down the code in
    // case the monitoring thread wants us to die immediately.

    // starts the listeners for sockets.
    // Calls StartListener(...) on each loginPort struct
    StartLoginPorts();

    LoadHosts(); /* Restore previous hosts */
    /* LoadUsage must come after LoadHosts so sessions have been restored;
       if not, UsageLimitExceeded data will be lost in pruning
    */
    LoadUsage(); /* Restore previous usage stats */

    /* Create the MV error reporting host */
    FindHost(MVHOSTNAME, 0, 0);
    if (UUSslEnabled())
        FindHost(MVHOSTNAME, 1, 0);

    /* Now try to reboost limits more if the restore found that we will need more */
    CheckLimits(1);

    /* These must be started before we drop privilege */
    if (cDnsPorts)
        dnsStatus = StartDnsSockets();

#ifndef WIN32
    SetUidGid();
#endif

    nextExpire = nextSaveCookies = nextStatus = nextAuditPurge = 0;

    /* This must come after ReadConfig so default cookies can be loaded */

    if (cAnonymousUrls) {
        anonymousSession = StartSession(NULL, 0, NULL);
        anonymousSession->anonymous = 1;
        GroupMaskClear(anonymousSession->gm);
        GroupMaskNot(anonymousSession->gm);
        anonymousSession->confirmed = anonymousSession->created;
    }

    if (optionPrecreateHosts) {
        PrecreateHosts();
    }

    SaveHosts();

    if (cHosts == mHosts) {
        Log("WARNING: All %d virtual hosts are active; MaxVirtualHosts may need to be increased",
            mHosts);
    }

    if (cSessions == mSessions) {
        Log("WARNING: All %d sessions are active; MaxSessions may need to be increased", mSessions);
    }

    /* We have deferred exiting until here so the UID/GID were changed and the potentially
       new shared memory identified and master pid were saved */
    if (guardian->portOpenFailure)
        exit(0);

    // I think this loops throught the open socket connections and closes ones
    // that have timed out or are dead...cleanup for sockets
    BeginStopSockets();

    // this is for running ezproxy as a cluster, which is not completed.  Chris talked about
    // wanted to have this dones someday.  More of a personal goal than any user
    if (cPeers) {
        BeginCluster();
    }

    if (haPeersValid) {
        if (UUBeginThread(DoHA, 0, "HA"))
            PANIC;
    }

    // start a thread which cycles through all the translates
    // this seems to do send/receive of translates, which I think are
    // all the current connections, uses the file decriptor table rfds
    BeginHandleRaw();

    if (dnsStatus == 0) {
        BeginDNS();
        /* Give DNS a little time to start before we try to verify wildcard DNS entry */
        SubSleep(0, 500);
    }

    if (usageLimits || intruderIPAttempts) {
        BeginSaveUsage();
    }

    // starts a thread
    BeginRefreshShibbolethMetadata();

    // Move these here so they start and prepare the database before Security and Security Emails
    // start
    EnvironmentStart();
    SecurityInit();
    SpurInit();

    BeginSendQueuedSecurityRuleEmail();
    BeginSendSecurityRuleDigestEmail();

    CheckProxyByHostnameWildcardDNS();

    /* Cannot initialize patterns until hosts are loaded and cluster is running since patterns
       can create virtual web servers
    */
    InitAllPatterns(); /* Get patterns in translate routine ready */
    /* Put new pid information into ezproxy.hst */

    guardian->chargeStatus = chargeRunning;
    if (optionLogThreadStartup) {
        Log("Thread running:  %s.", guardianChargeName[GCMAIN]);
    }

    // writes stuff to auditFile
    AuditEvent(NULL, AUDITSYSTEM, NULL, NULL, "Startup");

    // Delay start the license monitor until here so that we hit the chargeRunning state
    // above first, leaving license monitoring free to immediately reverse that and send
    // us to the stop state if relevant
    BeginRefreshWskeyLicense(wslicense);
    SubSleep(1, 0);

    if (!ttyClosed) {
#ifdef WIN32
        Log("To configure " EZPROXYNAMEPROPER " as a service that runs in the background and");
        Log("starts when this system is booted, press CTRL/C and issue the commands:");
        Log("     %s -si", argv[0]);
        Log("     net start %s", EZPROXYNAMEPROPER);
#else
        Log("To configure " EZPROXYNAMEPROPER " to start automatically when the system is booted,");
        Log("press CTRL/C and issue the command:");
        Log("     %s -si", argv[0]);
        Log("To run " EZPROXYNAMEPROPER " in the background, press CTRL/C and issue the command:");
        Log("     %s start", argv[0]);
#endif
        Log("You may not see any additional messages, but " EZPROXYNAMEPROPER " is running.");
#ifdef WIN32
#define DO_THIS_OTHER_THING "close this window"
#else
#define DO_THIS_OTHER_THING "end this session"
#endif
        Log("If you press CTRL/C or " DO_THIS_OTHER_THING ", " EZPROXYNAMEPROPER
            " will stop running.");
    }

    SecurityStart();
    IdentifierStart();
    SpurStart();

    // outer loop handles dispatching the created socket connection to a client/host
    for (; !guardian->chargeStop;) {
        // not entirely sure what this inner loop does.  I think this is mainly for cleanup
        // and rebuild of ezproxy stuff
        for (; !guardian->chargeStop;) {
            ChargeAlive(GCMAIN, &now);
            guardian->mainLocation = 0;
            ticksMain++;
            /* ClusterRequest(NULL);
            if (now > testCluster + 3) {
                ClusterRequest(NULL);
                testCluster = now;
            }
            */

            /**
             * hostSaved in globals.c, default is 0
             */
            if (hostsSaved == 0 && now > hostsNeedSave + 5)
                SaveHosts();

            if (optionRecordPeaks && now > nextPeak) {
                if (pSessions < cSessions) {
                    pSessions = cSessions;
                    Log("Peak sessions %d", pSessions);
                }
                if (pTranslates < cTranslates) {
                    pTranslates = cTranslates;
                    Log("Peak concurrent transfers %d", pTranslates);
                }
                if (pHosts < cHosts) {
                    pHosts = cHosts;
                    Log("Peak virtual hosts %d", pHosts);
                }
                nextPeak = now + 60;
            }

            guardian->mainLocation = 1;
            /* Once a minute expire sessions */
            if (nextExpire <= now) {
                /* This will re-open msgFile if it's filename changed because it has the current
                 * date inserted into it. */
                UUAcquireMutex(&mMsgFile);
                OpenMsgFile();
                UUReleaseMutex(&mMsgFile);

                ExpireSessions();
                nextExpire = now + 30;
            }

            if (nextAuditPurge <= now) {
                AuditPurge();
                /* Set next check on audit purge as the top of the next hour */
                nextAuditPurge = ((time_t)(now / 3600) + 1) * 3600;
            }

            guardian->mainLocation = 2;
            /* Every 5 second processing */
            if (nextSaveCookies <= now) {
                /* Once every 5 seconds, save changed cookies */
                SaveCookies(0);
                nextSaveCookies = now + 5;
                /* If we had a guardian and it died, so should we */
                if (guardian->gpid && !ProcessAlive(guardian->gpid)) {
                    Log("Guardian appears dead; shutting down");
                    guardian->chargeStatus = chargeStopping;
                    guardian->chargeStop = 1;
                }
            }
            guardian->mainLocation = 3;

            /* Check to see if ezproxy.log needs to be reopend, and if so, do so */
            CheckReopenAccessFile(NULL, 0);
            CheckGuardianKillSession();
            guardian->mainLocation = 4;

            if (rebuildMainMaster) {
                if (debugLevel > 100) {
                    Log("Rebuild main master");
                }
                /* If rebuildMainMaster is 2, we are supposed to keep rebuilding this until
                   the value falls back to 1, so only reset this if we are at 1
                */
                if (rebuildMainMaster == 1)
                    rebuildMainMaster = 0;

#ifdef USEPOLL
                pfdcMaster = 0;
#else
                FD_ZERO(&rfdsMaster);
#endif
                maxSock = SOCKET_MIN;
                minSock = SOCKET_MAX;
                nSock = 0;

                // cleanup client sockets
                for (i = 0, lp = loginPorts; i < cLoginPorts; i++, lp++) {
                    /* This is the mark of USESSLSKIPPORT */
                    /* Could check for that, but if the socket is invalid, that is a more
                       compelling reason not to use it
                    */
                    if (lp->port == 0) {
                        Log("bad port 0");
                        continue; /* this should never happen */
                    }
                    if (lp->socket == INVALID_SOCKET)
                        continue;
#ifdef USEPOLL
                    pfdsMaster[pfdcMaster].fd = lp->socket;
                    pfdsMaster[pfdcMaster++].events = POLLIN;
#else
                    FD_SET(lp->socket, &rfdsMaster);
#endif
                    if (debugLevel > 100 && nextStatus <= now) {
                        Log("polling listener on %s:%" PORT_FORMAT " thru socket %" SOCKET_FORMAT,
                            myName, lp->port, lp->socket);
                    }
                    nSock++;
                    if (lp->socket > maxSock)
                        maxSock = lp->socket;
                    if (lp->socket < minSock)
                        minSock = lp->socket;
                }

                // cleanup host sockets
                for (i = 1, h = hosts + i; i < cHosts; i++, h++) {
                    if (h->stopListener) {
                        if (h->socket != INVALID_SOCKET) {
                            closesocket(h->socket);
                            h->socket = INVALID_SOCKET;
                        }
                        h->myPort = 0;
                        h->stopListener = 0;
                        NeedSave();
                    }
                    if (h->myPort == 0)
                        continue; /* It could be proxy by hostname in which case the primary login
                                     listeners, above, are used. */
                    if (h->socket == INVALID_SOCKET)
                        continue;

#ifdef USEPOLL
                    pfdsMaster[pfdcMaster].fd = h->socket;
                    pfdsMaster[pfdcMaster++].events = POLLIN;
#else
                    FD_SET(h->socket, &rfdsMaster);
#endif
                    if (debugLevel > 100 && nextStatus <= now) {
                        Log("polling listener on %s:%" PORT_FORMAT " (for %s:%" PORT_FORMAT
                            ") thru socket %u",
                            myName, h->myPort, h->hostname, h->remport, h->socket);
                    }
                    nSock++;
                    if (lp->socket > maxSock)
                        maxSock = lp->socket;
                    if (lp->socket < minSock)
                        minSock = lp->socket;
                }
            }

#ifdef USEPOLL
            if (pfdc = pfdcMaster)
                memcpy(pfds, pfdsMaster, pfdcMaster * sizeof(*pfds));
#else
            memcpy(&rfds, &rfdsMaster, sizeof(rfds));
#endif

            guardian->mainLocation = 5;

            if (optionStatus != 0 && nextStatus <= now) {
                Log("status: maxSock %u, minSock %u, nSock %u, cSessions %d, cTranslates %d",
                    maxSock, minSock, nSock, cSessions, cTranslates);
                if (debugLevel > 100) {
                    Log("cLoginPorts %d, cHosts %d, total %d", cLoginPorts, cHosts,
                        cLoginPorts + cHosts);
                }
                nextStatus = now + 60;
            }

            guardian->mainLocation = 6;

            /* We time out at .1 second so we can pick up connections on to new virtual servers */
            /* created by FindHost/StartListener as well as new raw transfers */
#ifdef USEPOLL
            PollSort(pfds, pfdc);
            TEMP_FAILURE_RETRY_INT(rc = interrupted_neg_int(poll(pfds, pfdc, 100)));
#else
            /* Note that Linux select destroys the timeout field, so it must be reset each time */
            pollInterval.tv_sec = 0;
            pollInterval.tv_usec = 100000;

            // http://msdn.microsoft.com/en-us/library/windows/desktop/ms740141(v=vs.85).aspx
            // select determines the status of one or more sockets, waiting to perform synchronous
            // I/O
            TEMP_FAILURE_RETRY_INT(
                rc = interrupted_neg_int(select(maxSock + 1, &rfds, NULL, NULL, &pollInterval)));
#endif
            guardian->mainLocation = 7;
#ifndef WIN32
            if ((rc < 0) && (errno == ECANCELED)) {
                break;
            }
#endif
            if (rebuildMainMaster) {
                /* We must ignore the contents of rfds and rfdsMaster because rebuildMainMaster != 0
                 * means that the FD numbers have been changed in some way.  If we were to process
                 * I/O based on the current FD numbers, we'd be doing I/O for the wrong FDs which
                 * may cause a thread to block or crash. We also ignore any errors.
                 */
                continue;
            }
            if (rc != 0)
                break;

            /* timeout */
        }
        // end main loop

#ifndef WIN32
        if ((rc < 0) && (errno == ECANCELED)) {
            break;
        }
#endif
        if (rc < 0) {
            Log("select or poll returned %d", socket_errno);
            exit(0); /* try a restart */
        }

        /* some FD needs service */

        /* needDelay is used to add a little breather if all translate structures in use */
        needDelay = 0;

        // Loop through the loginPorts for valid sockets.  THen we use
        // FD_ISSET or the Poll to check if this socket is in the file descriptor table (rfds)
        // and if it is, then we have a client waiting on a connection.  So dispatch it.  There
        // will be some blocking on this thread while the socket is accepted in the DispatchSocket
        // function.
        for (i = 0, lp = loginPorts; i < cLoginPorts; i++, lp++) {
            guardian->mainLocation = 8;
            if (lp->socket == INVALID_SOCKET)
                continue;
#ifdef USEPOLL
            if ((PollRevents(pfds, pfdc, lp->socket) & POLLIN) != 0) {
#else
            if (FD_ISSET(lp->socket, &rfds)) {
#endif
                if (debugLevel > 19)
                    Log("%d Dispatch port", lp->port);
                // connect the client socket...this actually takes our sockets which
                // have had bind and list called and performs the last step of accept,
                // which blocks till connection has happened.  This also loops inside itslef
                // for up to 5 connections
                //  a|=b is the same as a = a | b
                needDelay |= DispatchSocket(0, lp->socket, lp->useSsl, lp->port, lp);
            }
        }

        for (i = 1, h = hosts + i; i < cHosts; i++, h++) {
            guardian->mainLocation = 9;
            if (h->socket == INVALID_SOCKET)
                continue;
            if (h->myPort == 0)
                continue;
#ifdef USEPOLL
            if ((PollRevents(pfds, pfdc, h->socket) & POLLIN) != 0) {
#else
            if (FD_ISSET(h->socket, &rfds)) {
#endif
                if (debugLevel > 19)
                    Log("%d Dispatch virtual host port", h->myPort);
                // connect the host socket
                needDelay |= DispatchSocket(i, h->socket, h->useSsl, h->remport, NULL);
            }
        }

        guardian->mainLocation = 10;

        if (needDelay)
            SubSleep(1, 0);
    }
    // end charge stop loop

    ackCancel();
    guardian->mainLocation = 9999;

    Log(EZPROXYNAMEPROPER " shutting down");

    buffer[0] = 0;

    AuditEvent(NULL, AUDITSYSTEM, NULL, NULL, "Shutdown");

    if (optionLogThreadStartup) {
        Log("Thread exiting: %s.", guardianChargeName[GCMAIN]);
    }

FinishedGuardian:
    if (guardian->gpid == 0) {
        /* Running without Guardian.  This process must to the cleanup. */
        UnlinkIPCFILEandPidFile();
    };

    return 0;
}

/**
 * This is the main application entry point.  If this is windows, then we are
 * including service.c which has the main.  This will then launch the ServiceStart
 * function.
 */
#ifdef WIN32
VOID ServiceStart(DWORD argc, char **argv, BOOL runAsService) {
    StartEZproxy((int)argc, argv, runAsService);
}
#else
#ifndef UNITTESTING
int main(int argc, char **argv) {
    StartEZproxy(argc, argv, 0);
    return 0;
}
#endif
#endif

#ifdef WIN32
VOID ServiceStop() {
    if (guardian) {
        guardian->guardianStop = 1;
        guardian->chargeStop = 1;
    }
}
#endif
