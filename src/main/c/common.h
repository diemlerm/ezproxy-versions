#ifndef __COMMON_H__
#define __COMMON_H__

#ifdef HAVE_CONFIG_H
#include <config.h>

#if defined(WIN32) || defined(HAVE_GETHOSTBYNAME_R)
#undef NONREENTRANTGETHOSTBYNAME
#else
#undef NONREENTRANTGETHOSTBYNAME
#define NONREENTRANTGETHOSTBYNAME 1
#endif

#if defined(WIN32) || defined(HAVE_GETHOSTBYADDR_R)
#undef NONREENTRANTGETHOSTBYADDR
#else
#undef NONREENTRANTGETHOSTBYADDR
#define NONREENTRANTGETHOSTBYADDR 1
#endif

#if defined(HAVE_POLL)
#define USEPOLL
#else
#undef USEPOLL
#endif

#if defined(HAVE_BACKTRACE)
#define GNUBACKTRACE
#elif defined(HAVE_PRINTSTACK)
#define SOLBACKTRACE
#endif

#ifdef HAVE_CTIME_R
#undef UUTIME_R0
#ifdef CTIME_R_THREE_ARGS
#define UUTIME_R3 1
#undef UUTIME_R2
#else
#define UUTIME_R2 1
#undef UUTIME_R3
#endif
#elif defined(HAVE_CTIME)
#define UUTIME_R0 1
#undef UUTIME_R2
#undef UUTIME_R3
#else
/* A conservative assumption. */
#define UUTIME_R2
#endif

#if defined(HAVE_GETTIMEOFDAY)
#define UUTIMEGETTIMEOFDAY
#else
#undef UUTIMEGETTIMEOFDAY
#endif
#if defined(HAVE_GETHRTIME)
#define UUTIMEGETHRTIME
#else
#undef UUTIMEGETHRTIME
#endif

#ifdef HAVE_SYS_FILIO_H
#define NEEDFILIO
#else
#undef NEEDFILIO
#endif

#ifdef HAVE_MUTEX_INIT
/* # define OLDSOLARIS */
#endif

#ifndef HAVE_UTSNAME_H
#undef DISABLEUTSNAME
#else
#define DISABLEUTSNAME
#endif

#else

/*
 * We don't have <config.h> to include.  Make a few safe assumptions for modern UNIX/LINUX systems.
 * Also, it could be that a commandline definition sets some things, so respect those if any.
 */

#ifndef WIN32
/* A few safe assumptions for UNIX/LINUX. */
#define HAVE_GETGRGID
#define HAVE_GETPWUID
#define HAVE_GETGROUPS
#define GETGROUPS_T gid_t
#endif

#if (!(defined(WIN32) || defined(UUTIME_R0) || defined(UUTIME_R2) || defined(UUTIME_R3)))
/* A conservative assumption. */
#define UUTIME_R0
#endif

#if defined(WIN32)
#undef NONREENTRANTGETHOSTBYNAME
#elif defined(__GLIBC__)
#undef NONREENTRANTGETHOSTBYNAME
#define HAVE_GETHOSTBYNAME_R_SIX_ARGS
#elif defined(sun)
#undef NONREENTRANTGETHOSTBYNAME
#define HAVE_GETHOSTBYNAME_R_FIVE_ARGS
#elif defined(__osf__)
#undef NONREENTRANTGETHOSTBYNAME
#define HAVE_GETHOSTBYNAME_R_OSF
#elif defined(NONREENTRANTGETHOSTBYNAME)
/* done */
#else
/* A conservative assumption. */
#define NONREENTRANTGETHOSTBYNAME
#endif

#if defined(WIN32)
#undef NONREENTRANTGETHOSTBYADDR
#elif defined(UUGHBA6)
#undef NONREENTRANTGETHOSTBYADDR
#define HAVE_GETHOSTBYADDR_R_SEVEN_ARGS
#elif defined(UUGHBA7)
#undef NONREENTRANTGETHOSTBYADDR
#define HAVE_GETHOSTBYADDR_R_EIGHT_ARGS
#elif defined(NONREENTRANTGETHOSTBYADDR)
/* done */
#else
/* A conservative assumption. */
#define NONREENTRANTGETHOSTBYADDR
#endif

#endif

// Stringize value of s (e.g., turns mkstr(3) into "3")
#define mkstr(s) #s
/*
  UUTIME_R0 indicates that time functions are not re-entrant and must be wrapped
  UUTIME_R2 indicates that ctime_r takes 2 parameters (POSIX)
  UUTIME_R3 indicates that ctime_r takes 3 parameters (old Solaris)
  WIN32 indicates that the time functions are re-entrant
 */
#if (!(defined(WIN32) || defined(UUTIME_R0) || defined(UUTIME_R2) || defined(UUTIME_R3)))
#error ! (defined(WIN32) || defined (UUTIME_R0) || defined (UUTIME_R2) || defined (UUTIME_R3))
#endif

#if defined(UUTIMEGETTIMEOFDAY) && defined(UUTIMEGETHRTIME)
#undef UUTIMEGETTIMEOFDAY
#elif !(defined(UUTIMEGETTIMEOFDAY) || defined(UUTIMEGETHRTIME))
/* If neither defined, we will default to the simple time() function
 */
#endif

#include "ezproxyversion.h"

#ifndef EZPOS
#ifdef WIN32
#define EZPOS WINDOWS_NAME
#else
#define EZPOS UNIX_NAME
#endif
#endif

#ifdef WIN32
#ifndef FD_SETSIZE
#define FD_SETSIZE 8192
#endif
#endif

/* No HAVE_* symbol definitions beyond this point. */

#define NULLSAVESTR(x) ((x) != NULL) ? (x) : ""

/* SendFile flag constants */
#define SFREPORTIFMISSING 1
#define SFQUOTEDURL 2

#define CHECKHOST "iptest." EZPROXYNAMEPROPER_ALL_LOWER ".com"
#define CHECKPORT 80
#define CHECKURL "http://" CHECKHOST "/cgi-bin/ezpcheck32"

#include <errno.h>

/*
Don't confuse Microsoft's _snprintf (pre-C99) with POSIX snprintf.
MS does not null-terminate, and, has completely different behavior like
returning -1 instead of the would-be string length.  To emphasize this,
we always use the naming convention where we put a leading underscore
in front of the snprintf and vsnprintf calls for all platforms.
 */
#define _SNPRINTF_SENTINEL ((char)1)
#define _SNPRINTF_IS_OVERFLOW(array, sizeofArray) (array[sizeofArray - 1] != _SNPRINTF_SENTINEL)
#define _SNPRINTF_BEGIN(array, sizeofArray)          \
    do {                                             \
        array[sizeofArray - 1] = _SNPRINTF_SENTINEL; \
    } while (0)
#define _SNPRINTF_PANIC_OVERFLOW(array, sizeofArray) \
    if _SNPRINTF_IS_OVERFLOW (array, sizeofArray)    \
    PANIC
#ifdef WIN32
#define _SNPRINTF_END(array, sizeofArray)           \
    if _SNPRINTF_IS_OVERFLOW (array, sizeofArray) { \
        array[sizeofArray - 1] = 0;                 \
    }
#else
#define _snprintf snprintf
#define _vsnprintf vsnprintf
#define _SNPRINTF_END(array, sizeofArray) \
    do {                                  \
    } while (0)
#endif

#ifndef _MSC_VER
#include <inttypes.h>
#endif

#ifdef WIN32
#undef USEPOLL
#define GETGROUPS_T int
// #define pid_t int
// #define gid_t int
#ifndef SSIZE_MAX
#define SSIZE_MAX (32766)
#endif
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0400
#endif
#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <process.h>
#include <io.h>
#include <share.h>
#define DIRSEP "\\"
#define SOPEN(file) _sopen(file, _O_RDONLY, _SH_DENYNO, 0)
#define SBOPEN(file) _sopen(file, _O_RDONLY | _O_BINARY, _SH_DENYNO, 0)
#define socket_errno WSAGetLastError()
#define SetErrno(x) (WSASetLastError(x), errno = (x))
/* Following needed to setup NetUserChangePassword for NT only (not avail 95/98) */
extern HINSTANCE netapi32Dll;
typedef DWORD(APIENTRY *LPNetUserChangePassword)(LPWSTR, LPWSTR, LPWSTR, LPWSTR);
extern LPNetUserChangePassword sNetUserChangePassword;
#define socklen_t_M int
#define size_t_M int
#ifdef _MSC_VER
typedef long int intmax_t;
typedef unsigned long int uintmax_t;
#endif
#else
#include <limits.h>
#define GetLastError() errno
#define socklen_t_M socklen_t
#define size_t_M size_t
#define socket_errno errno
#define SetErrno(x) (errno = (x), errno = (x))
#define WSAEWOULDBLOCK EWOULDBLOCK
#define WSAECONNREFUSED ECONNREFUSED
#define WSAETIMEDOUT ETIMEDOUT
#define WSAEADDRINUSE EADDRINUSE
#define WSAENOTSOCK ENOTSOCK
#include <pthread.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/uio.h>
#ifdef NEEDFILIO
#include <sys/filio.h>
#endif
#include <sys/wait.h>
#include <sys/resource.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#define DIRSEP "/"
#define SOPEN(file) UUopenExistantFD(file, O_RDONLY)
#define SBOPEN(file) UUopenExistantFD(file, O_RDONLY)
#define stricmp strcasecmp
#define strnicmp strncasecmp
#include <sys/ioctl.h>
/* ioctlsocket and closesocket gets defined for apps.c and req.c by SSL libraries.
   Those also set MONOLITH, so use that to know that these shouldn't be redefined
*/
#ifndef MONOLITH
#define ioctlsocket ioctl
#define closesocket close
#endif /* #ifdef MONOLITH */
#ifndef O_BINARY
#define O_BINARY 0
#endif
#endif /* else #ifdef WIN32 */

/*
The select() API requires that the application pass in an array of bits in which one bit is
used to represent each descriptor number. When descriptor numbers are very large, it can
overflow the 30KB allocated memory size, forcing multiple iterations of the process. This
'overhead can adversely affect performance.

The poll() API allows the application to pass an array of structures rather than an array
of bits. Because each pollfd structure can contain up to 8 bytes, the application only
needs to pass one structure for each descriptor, even if descriptor numbers are very large.

So, we prefer poll(), but Windows doesn't have poll() nor does it have select.h
*/
#ifdef USEPOLL
#include <poll.h>
#else
#ifndef WIN32
#include <select.h>
#endif
#endif

/*
 A signal can arrive and be handled while an I/O primitive such as
 open or read is waiting for an I/O device. If the signal handler
 returns, the system faces the question: what should happen next?

 POSIX specifies one approach: make the primitive fail right away.
 The error code for this kind of failure is EINTR. This is flexible,
 but usually inconvenient. Typically, POSIX applications that use
 signal handlers must check for EINTR after each library function
 that can return it, in order to try the call again. Often
 programmers forget to check, which is a common source of error.
 */
#define TEMP_FAILURE_RETRY_INT(expression)           \
    while (((expression) < 0) && (errno == EINTR)) { \
    }
#define TEMP_FAILURE_RETRY_FILE(expression)              \
    while (((expression) == NULL) && (errno == EINTR)) { \
    }

#define XMLSEC_CRYPTO_OPENSSL
#define LIBXML_STATIC
#define XMLSEC_STATIC
#define LIBXML_VALID_ENABLED
#define XMLSEC_NO_CRYPTO_DYNAMIC_LOADING
#include <libxml/parser.h>
#include <libxml/xpath.h>

#ifndef PCRE_STATIC
#define PCRE_STATIC 1
#endif
#include <pcre.h>

#include "tags.h"
#include "uuavl.h"

#define DOCSDIR "docs" DIRSEP
#define FIDDLERDIR "fiddler"
#define EZPROXYCFG "config.txt"
#define EZPROXYUSR "user.txt"
#define INTRUSIONTXT "intrusion.txt"
#define INTRUSIONPEM "intrusion.pem"
#define EZPROXYKEY EZPROXYNAMEPROPER_ALL_LOWER ".key"
#define EZPROXYGRP EZPROXYNAMEPROPER_ALL_LOWER ".grp"
#define EZPROXYACC EZPROXYNAMEPROPER_ALL_LOWER ".acc"
#define EZPROXYTKN EZPROXYNAMEPROPER_ALL_LOWER ".tkn"
#define EZPROXYRND EZPROXYNAMEPROPER_ALL_LOWER ".rnd"
#define EZPROXYIPC EZPROXYNAMEPROPER_ALL_LOWER ".ipc"
#define COOKIEHTM DOCSDIR "cookie.htm"
#define CSRFHTM DOCSDIR "csrf.htm"
#define SUSPENDHTM DOCSDIR "suspend.htm"
#define LOGINHTM DOCSDIR "login.htm"
#define INTRUDERHTM DOCSDIR "intruder.htm"
#define REJECTHTM DOCSDIR "reject.htm"
#define LIMITHTM DOCSDIR "limit.htm"
#define LOGUPHTM DOCSDIR "logup.htm"
#define LOGINBUHTM DOCSDIR "loginbu.htm"
#define LOGINCPHTM DOCSDIR "logincp.htm"
#define LOGINCP2HTM DOCSDIR "logincp2.htm"
#define LOGINCP3HTM DOCSDIR "logincp3.htm"
#define LOGINCP4HTM DOCSDIR "logincp4.htm"
#define LOGINCP5HTM DOCSDIR "logincp5.htm"
#define LOGINCP6HTM DOCSDIR "logincp6.htm"
#define BADHOSTHTM DOCSDIR "badhost.htm"
#define BADCASHTM DOCSDIR "badcas.htm"
#define HTTPSHTM DOCSDIR "https.htm"
#define AADDHTM DOCSDIR "aadd.htm"
#define ARESETHTM DOCSDIR "areset.htm"
#define LOGOUTHTM DOCSDIR "logout.htm"
#define SHIBLOGOUTHTM DOCSDIR "shiblogout.htm"
#define MENUHTM DOCSDIR "menu.htm"
#define ITYPEHTM DOCSDIR "itype.htm"
#define IMBLOCKHTM DOCSDIR "imblock.htm"
#define IREFUSEDHTM DOCSDIR "irefused.htm"
#define IEXPIREDHTM DOCSDIR "iexpired.htm"
#define TEXPIREDHTM DOCSDIR "texpired.htm"
#define DENYHTM DOCSDIR "deny.htm"
#define WEXPIREDHTM DOCSDIR "wexpired.htm"
#define NEEDHOSTHTM DOCSDIR "needhost.htm"
#define MVHTM DOCSDIR "mv.htm"
#define MSHTM DOCSDIR "ms.htm"
#define EXPIREDHTM DOCSDIR "expired.htm"
#define TYPEHTM DOCSDIR "type.htm"
#define REFUSEDHTM DOCSDIR "refused.htm"
#define REFERERCOOKIEHTM DOCSDIR "referercookie.htm"
#define SCIFINDERHTM DOCSDIR "scifinder.htm"
#define SHIBFAILURE DOCSDIR "shibfailure.htm"
#define SHIBBOLETHSHIREURL "/Shibboleth.shire"
#define COOKIESDIR "cookies"
#define SSLDIR "ssl" DIRSEP
#define XMLDIR "xml" DIRSEP
#define MAILDIR "mail" DIRSEP
#define AUDITDIR "audit" DIRSEP
#define GARTNERDIR "gartner" DIRSEP
#define WSPUBLIC "/public"
#define WSLIMITED "/limited"
#define WSLOGGEDIN "/loggedin"
#define WSWELLKNOWN "/.well-known"
#define ACCOUNTDEFAULTMD5 "********************************"
#define MVHOSTNAME "mv." EZPROXYNAMEPROPER_ALL_LOWER ".com"
#define CSRF_TOKEN_LEN 24
#define CSRF_TOKEN_NAME "ezproxycsrftoken"

#define AHNOHTTPHEADER 1
#define AHNOHTMLHEADER 2
#define AHNOTOPMENU 4
#define AHSORTABLE 16
#define AHNOH1TITLE 32
#define AHNOEXPIRES 64
#define AHNOCSS 128

#define DBCGETNAME 1
#define DBCJAVASCRIPT 2
#define DBCIP 4
#define DBCPATTERN 8
#define DBCGROUP 16

#define ULULOGUSER 1
#define ULUSTATUSUSER 2
#define ULULIMIT 4
#define ULUTOKENKEY 8
#define ULUALLOWVARSU 16
#define ULUUSAGELIMIT 32
#define ULUEBRARY 64
#define ULUADDUSERHEADER 128
#define ULUUSERNAMECARETN 256
#define ULUAUDIT 512
#define ULUOVERDRIVE 1024
#define ULUNETLIBRARY 2048
#define ULUSSO 4096
#define ULUFORMVARS 8192
#define ULUFMG 16384
#define ULUMYILIBRARY 32768
#define ULUGARTNER 65536
#define ULUCAS 131072
#define ULUUSEROBJECT 262144
#define ULUSESSIONCOUNT 524288

#define MAXSESSIONVARS 10
#define MAXDBVARS 10
#define MAXLOGTAGLEN 32

#define USESSLMINPROXY 127
#define USESSLSKIPPORT 126
#define USESSLZ3950 125

#define HTTPDATELENGTH 30

#define REDIRECTPATCHSTRING "&" EZPROXYNAMEPROPER "RedirectPatch"

#define IFTEST 0
#define IFMISSING 1
#define IFREPLACE 2

#define SUPPORTEMAIL "support@oclc.org"

#define LOGIN_COOKIE_SAME_SITE_CLASSIC 0
#define LOGIN_COOKIE_SAME_SITE_LAX 1
#define LOGIN_COOKIE_SAME_SITE_NONE 2
#define LOGIN_COOKIE_SAME_SITE_MAX 3

#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <float.h>
#ifdef WIN32
#include <time.h>
#else
#include <sys/time.h>
#endif

/* On Windows, min and max are defined in stdlib.h, so these local defines must appear
   after the inclusion of stdio.h or there will be a redefinition error during compile
*/

#ifndef min
#define min(x1, x2) ((x1) > (x2) ? (x2) : (x1))
#endif

#ifndef max
#define max(x1, x2) ((x1) > (x2) ? (x1) : (x2))
#endif

#ifdef WIN32
#include "win32rwl.h"
#else
#include "unixrwl.h"
#endif

#ifndef INADDR_NONE
#define INADDR_NONE 0xffffffff
#endif

#ifndef LPVOID
#define LPVOID void *
#endif

#ifndef BOOL
#define BOOL int
#endif
#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

#ifndef PRIdMAX
#define PRIdMAX "ld"
#endif
#ifndef PRIuMAX
#define PRIuMAX "lu"
#endif
#ifndef PRIxMAX
#define PRIxMAX "lx"
#endif

#ifndef UNUSED
#define UNUSED(x) (void)x
#endif

#ifndef WIN32
#ifndef DWORD
#include <stdint.h>
#define DWORD uint32_t
#endif
#endif

#ifndef MAX_PATH
#ifdef WIN32
#define MAX_PATH _MAX_PATH
#else
#define MAX_PATH 512
#endif
#endif

int RWLInit(struct RWL *rwl, char *name);
int RWLDestroy(struct RWL *rwl);
int RWLAcquireRead(struct RWL *rwl);
int RWLAcquireWrite(struct RWL *rwl);
int RWLReleaseRead(struct RWL *rwl);
int RWLReleaseWrite(struct RWL *rwl);

#include <openssl/ssl.h>
#include <openssl/rand.h>
#include <openssl/err.h>
#include <openssl/md5.h>
#include <zlib.h>

#include "uusocket.h"

struct GROUPMASKNODE {
    /* size represents the number of unsigned ints, not the number of bytes */
    size_t size;
    unsigned int *bits;
};

typedef struct GROUPMASKNODE *GROUPMASK;

/* Stub declarations to resolve circular references in definitions. */
struct USERINFO;
struct FILEREADLINEBUFFER;
struct VARIABLES;
struct TRANSLATE;

#include "expression.h"
#include "variables.h"

#define WS " \t\r\n"

#define PANIC panic(__UUFILE__, __LINE__, errno)
#define PANIC0 panic0(__UUFILE__, __LINE__, errno, "")
#define PANICSTR panicstr(__UUFILE__, __LINE__, errno, "")
#define PANICMSG0(x) panic0(__UUFILE__, __LINE__, errno, x)
#define PANICMSGSTR(x) panic0(__UUFILE__, __LINE__, errno, x)
#define TRACEBACK(code)                                 \
    do {                                                \
        reportFileLineCode(__UUFILE__, __LINE__, code); \
    } while (0)
#define TRACEBACK_IF(condition, code)                       \
    do {                                                    \
        if (condition) {                                    \
            reportFileLineCode(__UUFILE__, __LINE__, code); \
        }                                                   \
    } while (0)
#define dprintf        \
    if (optionDprintf) \
    Log

enum DATEFORMAT { dateDMY, dateMDY, dateYMD };

enum FINDUSERRESULT {
    resultValid,   /* The user is successfully authenticated. */
    resultInvalid, /* The user is not yet authenticated.  Later statements may authenticate the
                      user. */
    resultExpired, /* The user is unauthenticated by session expiration.  Later statements may
                      authenticate the user. */
    resultRefused, /* The user is refused authentication services at the authority.  Later
                      statements may authenticate the user. */
    resultNotified /* The user is not authenticated and has been notified (or does not need to be
                      notified), and the rest of the authentication file will be skipped. */
};

enum HTMLHEADERCACHE { htmlHeaderOmitCache, htmlHeaderNoCache, htmlHeaderMaxAgeCache };

enum INTRUDERSTATUS { INTRUDERNOT, INTRUDEREVADE, INTRUDERNOLOG, INTRUDERREJECT };

enum AUTOLOGINBY { autoLoginByNone, autoLoginByIP, autoLoginByReferer, autoLoginByLast };

enum REDIRECTSAFEORNEVERPROXYCACHE {
    redirectSafeOrNeverProxyCacheUninitialized = 0,
    redirectSafeOrNeverProxyCacheFalse,
    redirectSafeOrNeverProxyCacheTrue
};

/* The following are the definitions for licenses. */
#define LICENSE_SERVER_DEVQA "wskey-m1.dev.oclc.org"
#define LICENSE_SERVER_DEVQA_PORT 80
#define LICENSE_SERVER_DEVQA_SSL 0
#define LICENSE_SERVER_PROD "authn.sd00.worldcat.org"
#define LICENSE_SERVER_PROD_PORT 443
#define LICENSE_SERVER_PROD_SSL 1
#define LICENSE_SERVER_CERT_IDX -2

#define ONE_HOUR_IN_SECONDS (60 * 60)
#define ONE_DAY_IN_SECONDS (24 * ONE_HOUR_IN_SECONDS)
#define TWO_DAYS_IN_SECONDS (2 * ONE_DAY_IN_SECONDS)
#define ONE_WEEK_IN_SECONDS (7 * ONE_DAY_IN_SECONDS)
#define FOURTEEN_DAYS_IN_SECONDS (14 * ONE_DAY_IN_SECONDS)
#define THIRTY_DAYS_IN_SECONDS (30 * ONE_DAY_IN_SECONDS)
#define NINETY_DAYS_IN_SECONDS (90 * ONE_DAY_IN_SECONDS)

extern BOOL licenseValid;

/* used for wskey proxy */
struct PROXY {
    char *user;
    char *pass;
    char *auth;
    PORT port;
    BOOL ssl;
    struct PROXY *next;
    char *host;
};

enum LOGINCOOKIEDOMAINTYPE {
    LOGINCOOKIEDOMAINDEFAULT,
    LOGINCOOKIEDOMAINDOMAINNAME,
    LOGINCOOKIEDOMAINHOSTNAME,
    LOGINCOOKIEDOMAINMANUAL,
    LOGINCOOKIEDOMAINNONE
};

#define GUARDIANGUID "{29832BCD-8023-46de-BAEF-647D08CCC7DF}"
#define ADJUST_TIME(recordedtime, now) ((recordedtime) > (now) ? (now) : (recordedtime))

#define GCMAIN 0
#define GCHANDLERAW 1
#define GCSTOPSOCKETS 2
#define GCREFRESHSHIBBOLETHMETADATA 3
#define GCCLUSTER 4
#define GCDNS 5
#define GCSAVEUSAGE 6
#define GCWSKEYLICENSE 7
#define GCDNSCACHE 8
#define GCSECURITY 9
#define GCIDENTIFIER 10
#define GCSENDSECURITYRULEDIGESTEMAIL 11
#define GCSENDQUEUEDSECURITYRULEEMAIL 12
#define GCSPUR 13
#define GCLAST GCSPUR

enum CHARGESTATUS {
    chargeBeingStarted,
    chargeStarting,
    chargeRunning,
    chargeFroze,
    chargeStopping,
    chargeBadConfig,
    chargeBadVersion
};
#define GCPORTFAILED 1
#define GCPRIVPORTFAILED 2
struct GUARDIAN {
    /* This preamble is fixed to insure that new versions can stop old versions */
    char version[32];
    int gpid;
    int cpid; /* Guardian's child PID, aka "charge". */
    enum CHARGESTATUS chargeStatus;
    volatile BOOL guardianStop;
    volatile BOOL chargeStop;
    int clientRequestPid;
    int serverGrantPid;
    int clientProcessPid;
    int serverCompletePid;
    BOOL licenseChange;
    /* end preamble */
    int portOpenFailure;
    volatile BOOL dontReopenAccessFile;
    int renameStatus;
    int mainLocation;
    int rawLocation;
    int stopSocketsLocation;
    int killSession;
    // volatile time_t charges[GCLAST + 1][GCCLAST + 1];
    volatile DWORD chargesCheckin[GCLAST + 1];
    // volatile DWORD chargesCheckinAck[GCLAST + 1];
    // volatile BOOL chargesCheckinInitialized[GCLAST + 1];
    volatile BOOL chargesRunning[GCLAST + 1];
    char renameLog[64];
    char arg[128];
};
#define GUARDIANMAPOPTION "-smid"
#define GUARDIANMAPENVVAR EZPROXYNAMEPROPER_ALL_UPPER GUARDIANMAPOPTION

#define IAmTheGuardian 1
#define IAmTheCharge 0

extern char *EZPROXYVERSION;
extern char *GUARDIANVERSION;

extern int ipcFile;
extern struct GUARDIAN *guardian;

/* ORIGMAXKEY must always be less than or equal to MAXKEY */
#define ORIGMAXKEY 16
#define MAXKEY 32
#define DEFAULTSESSIONKEYSIZE (MAXKEY - 1)
#define MAXMENU 64
#define MAXLBPEERID 12
/* MAXUSEROBJECTTICKET must not be less than 32 */
#define MAXUSEROBJECTTICKET (MAXLBPEERID + 20)
#define MAXDOCSCUSTOMDIR 32
#define MAXPDFURLS 8

#define MAXCONTROLSECRET 21

enum IPACCESS { IPALOCAL, IPAAUTO, IPAREMOTE, IPAREJECT, IPAWHITELIST };
extern char *IPACCESSNAMES[];

enum PEERSTATUS { PEERDOWN, PEERSTARTING, PEERSYNCING, PEERUP, PEERSTALE };
enum HAPEERSTATUS { HAPEERDOWN, HAPEERSTARTING, HAPEERSYNCING, HAPEERUP, HAPEERSTALE };
extern char *peerStatusName[];
extern char *haPeerStatusName[];

extern char *guardianChargeName[];
extern DWORD guardianChargeMaximumLatency[];
#define LATENCY_FACTOR 1000

extern int guardianChargeForceSeizureIndex;
extern int guardianChargeForceSeizureDelay;

enum ACCOUNTFINDRESULT {
    accountZero,
    accountMatch,
    accountDefault,
    accountCreated,
    accountWrong,
    accountNotFound,
    accountFileNotFound
};

/* optionCookieNames in OptionCookieNameSelect must be synchronized with these values */
enum OPTIONCOOKIE { OCALL, OCNONE, OCDOMAIN, OCPASSTHROUGH, OCMAX };
const char *OptionCookieNameSelect(int idx);

/* EDACHAR is for char field[10], EDPCHAR is used for char* where auto malloc is to be
   performed for decode (and preserve NULL values)
*/
enum ENCODETYPE { EDSTOP, EDCHAR, EDSHORT, EDINT, EDACHAR, EDPCHAR, EDRCHAR };

enum PROXYTYPE { PTPORT, PTHOST };

#define MAXCLUSTERDATA 512
#define DEFMAXIPTYPES 200
/* #define MAXAUTHHOSTS 50 */
#define DEFMAXSESSIONS 500
#ifdef BIGGIE
#define DEFMAXHOSTS 1000
#define DEFMAXTRANSLATES 1000
#define DEFMAXDATABASES 500
#else
#define DEFMAXHOSTS 200
#define DEFMAXHOSTS2 500 /* This value if proxy by hostname active */
#define DEFMAXTRANSLATES 200
#define DEFMAXDATABASES 200
#endif
#define DEFMAXPEERS 16
#define DEFMAXNEVERPROXYHOSTS 5000

/* DEFSESSIONLIFE is the interval of session inactivity that leads to automatic termination of a
 * session */
/* measured in seconds */
#define DEFSESSIONLIFE (2 * ONE_HOUR_IN_SECONDS)

#define REQUIREAUTHENTICATENEVER 0
#define REQUIREAUTHENTICATEPERM 1
#define REQUIREAUTHENTICATETEMP 2
#define REQUIREAUTHENTICATEMAX 3

/* Maximum number of second to wait for no data to move on a raw connection before terminating it */
/* Increased from 5 to 60 on 10/11/01 to see if it improves PDF file access problems */
/* #define MAXRAWWAIT 60 */
/* moved to binaryTimeout variable that can be set in EZPROXYCFG */

#define MAXASCIIDATE 24
#define MAXASCIIKMB 20
#define MAXASCIIDHM 40

#define MAXMETHOD 32
#define MAXURL 8192
#define MAXVERSION 24
#define MAXATTRBUFF 15360
#define MAXEDITBUFF 1024
#define MAXREQUESTHEADERS 24576
#define MAXBUFFER (MAXATTRBUFF + MAXEDITBUFF)
#define MAXINBUFFER 512
#define HTTPVERSION10 "HTTP/1.0"
#define HTTPVERSION11 "HTTP/1.1"

#define FINDREPLACE_IGNORECASE 1
#define FINDREPLACE_GLOBAL 2

#define ADMIN_UNAUTH 1
#define ADMIN_PRIV 2
#define ADMIN_NORELOGINCHECK 4
#define ADMIN_MUST_LOGIN 8
#define ADMIN_MAYBE_LOGIN 16
#define ADMIN_LICENSED 32
#define ADMIN_HTTPS 64

// gmAdminAny is the set of all Admin. groups so there is no GROUP_ADMIN_ANY
#define GROUP_ADMIN_AUDIT "Admin.Audit"
#define GROUP_ADMIN_DECRYPTVAR "Admin.DecryptVar"
#define GROUP_ADMIN_FIDDLER "Admin.Fiddler"
#define GROUP_ADMIN_GROUPS "Admin.Groups"
#define GROUP_ADMIN_IDENTIFIER "Admin.Identifier"
#define GROUP_ADMIN_INTRUSION "Admin.Intrusion"
#define GROUP_ADMIN_LDAP "Admin.LDAP"
#define GROUP_ADMIN_MESSAGES "Admin.Messages"
#define GROUP_ADMIN_RESTART "Admin.Restart"
#define GROUP_ADMIN_SECURITY "Admin.Security"
#define GROUP_ADMIN_SHIBBOLETH "Admin.Shibboleth"
#define GROUP_ADMIN_SSL_UPDATE "Admin.SSLUpdate"
#define GROUP_ADMIN_SSL_VIEW "Admin.SSLView"
#define GROUP_ADMIN_STATUS_UPDATE "Admin.StatusUpdate"
#define GROUP_ADMIN_STATUS_VIEW "Admin.StatusView"
#define GROUP_ADMIN_TOKEN "Admin.Token"
#define GROUP_ADMIN_USAGE "Admin.Usage"
#define GROUP_ADMIN_USAGELIMITS "Admin.UsageLimits"
#define GROUP_ADMIN_USER "Admin.User"
#define GROUP_ADMIN_VARIABLES "Admin.Variables"

#define SESSION_PRIV_ADMIN 1
#define SESSION_PRIV_SLIPTEST 2

#define SESSION_FLAG_ALLOW_COUNTRY_CHANGE 1

typedef unsigned char U48[6];

/* For these structures, comments are */
/* RO read-only once created */
/* UT updated from within a specific translate context so conflicts can't occur */
/* UA updated arbitrarily (requires guard to access) */

struct DENYIFREQUESTHEADER {
    struct DENYIFREQUESTHEADER *next;
    char *file;
    char *header;
};

struct HTTPMETHOD {
    struct HTTPMETHOD *next;
    char *method;
    size_t len;
    BOOL admin;
};

enum HTTPHEADER_DIRECTION { HHDNONE, HHDREQUEST, HHDRESPONSE };
extern char *HTTPHEADER_DIRECTION_NAMES[];
enum HTTPHEADER_PROCESSING {
    HHPNONE,
    HHPPROCESS,
    HHPBLOCK,
    HHPREWRITE,
    HHPUNREWRITE,
    HHEDIT,
    HHINJECT
};
extern char *HTTPHEADER_PROCESSING_NAMES[];

struct HTTPHEADER {
    struct HTTPHEADER *next;
    char *header;
    char *expr;
    BOOL wild;
    BOOL inject;
    enum HTTPHEADER_DIRECTION direction;
    enum HTTPHEADER_PROCESSING processing;
};

struct EXTRALOGINCOOKIE {
    struct EXTRALOGINCOOKIE *next;
    char *cookie;
};

struct OVERDRIVESITE {
    struct OVERDRIVESITE *next;
    char *site;
    char *url;
    char *secret;
    char *ilsName;
    char *libraryID;
    GROUPMASK gm;
    BOOL noTokens;
};

struct GARTNERCONFIG {
    struct GARTNERCONFIG *next;
    char *shortName;
    char *url;
    char *secret;
    DSA *dsa;
    GROUPMASK gm;
};

enum SSOHASH { SSOHASHMD5, SSOHASHSHA1, SSOHASHSHA256 };

struct SSOSITE {
    struct SSOSITE *next;
    char *site;
    char *secret;
    char *url;
    char *expression;
    GROUPMASK gm;
    int cIpTypes;
    enum SSOHASH ssoHash;
    BOOL anonymous;
};

struct EBRARYSITE {
    struct EBRARYSITE *next;
    char *site;
    char *host;
    char *proxyHost;
    char *proxyAuth;
    char *proxySslHost;
    char *proxySslAuth;
    GROUPMASK gm;
    struct sockaddr_storage ipInterface;
    BOOL redirectHttp;
    PORT port;
    PORT proxyPort;
    PORT proxySslPort;
    char useSsl;
};

struct CURLMEMORYBUFFER {
    char *memory;
    size_t size;
};

#ifdef WIN32
#ifdef UUWIN32DEADLOCKCHECK
#define UUMUTEXTYPE HANDLE
#else
#define UUMUTEXTYPE CRITICAL_SECTION
#endif
#define UUTHREADID DWORD
#else
#ifdef OLDSOLARIS
#define UUMUTEXTYPE mutex_t
#else
#define UUMUTEXTYPE pthread_mutex_t
#define UUTHREADID pthread_t
extern pthread_attr_t pthreadCreateDetached;
#endif
#endif

typedef struct {
    UUMUTEXTYPE mutex;
    const char *name;
    const char *file;
    UUTHREADID threadid;
    int line;
    int count;
    char multi;
    char logDuplicateAttempts;
} UUMUTEX;

struct TRACKMUTEX {
    struct TRACKMUTEX *next;
    UUMUTEX *mutex;
};

struct REROUTE {
    char *to;
    char *domain;
    size_t domainLen;
    BOOL hostOnly;
    BOOL quote;
};

struct MYDNSCACHE {
    char host[255];
    PORT port;
    int family;
    struct addrinfo *ai;
    time_t updated;
};

struct PATTERN {
    struct PATTERN *next;
    char *matchString;
    char *replaceString;
    size_t matchLen;
    size_t replaceLen;
    char *originalReplaceString;
    unsigned long currentStates;
    unsigned long addStates;
    unsigned long removeStates;
};

struct PATTERNSTATE {
    struct PATTERNSTATE *next;
    char *state;
    int idx;
    unsigned long mask;
};

struct PATTERNS {
    struct PATTERN *pattern;
    struct PATTERNSTATE *patternState;
};

struct STOPSOCKET {
    struct STOPSOCKET *next;
    SOCKET socket;
    time_t lastActivity;
    time_t expires;
    BOOL hasStopped;
    BOOL hasEOF;
    enum UUSOCKETTYPE socketType;
    SSL *ssl;
    BOOL hasSslShutdown;
    int sslShutdowns;
};

struct INTRUDERIP {
    struct INTRUDERIP *next;
    struct sockaddr_storage ip;
    struct sockaddr_storage forwardedIp;
    char ipText[(INET6_ADDRSTRLEN * 2 + 1)];  // we squish 2 with a delimiter in here sometimes
    time_t lastAttempt;
    int attempts;
    int maxAttempts;
    int rejectAttempts;
    int intruderInterval;
    int intruderExpires;
    /*
    int intruderTimeout;
    */
    BOOL stoppedLogging;
};

struct INTRUDERIPATTEMPT {
    struct INTRUDERIPATTEMPT *next;
    int maxAttempts;
    int rejectAttempts;
    int intruderInterval;
    int intruderExpires;
    /*
    int intruderTimeout;
    */
    struct sockaddr_storage start;
    struct sockaddr_storage stop;
    BOOL xForwardedFor;
    BOOL haveIpRange;
};

struct USAGEDATEHOUR {
    struct USAGEDATEHOUR *next;
    time_t dateHour;
    unsigned int transfers;
    unsigned int recvKBytes;
    unsigned int intruderUserAttempts;
};

struct USAGE {
    struct USAGE *next;
    struct USAGEDATEHOUR *usageDateHours;
    char *user;
    unsigned int transfers;
    unsigned int recvKBytes;
    unsigned int intruderUserAttempts;
    time_t when;
    time_t expires;
    time_t lastAttempt;
};

enum USAGELIMITEXCEEDEDTYPE { ULEVOLUME, ULEINTRUDER };

struct USAGELIMIT {
    struct USAGELIMIT *next;
    char *name;
    struct USAGE *usage;
    unsigned int maxRecvKBytes;
    unsigned int maxTransfers;
    unsigned int maxIntruderUserAttempts;
    unsigned int interval;
    unsigned int granularity;
    unsigned int oldestInterval;
    unsigned int expires;
    BOOL enforce;
    BOOL open;
    BOOL ignoreNormalLogin;
    BOOL ignoreAutoLoginIP;
    BOOL ignoreRefererLogin;
};

struct USAGELIMITEXCEEDED {
    struct USAGELIMITEXCEEDED *next;
    char *user;
    int monitoredCount;
    int enforcedCount;
    int intruderUserAttemptsCount;
    int links;
    time_t accessed;
};

struct DATABASEUSAGELIMIT {
    struct DATABASEUSAGELIMIT *next;
    struct USAGELIMIT *usageLimit;
};

#define MAXFILEREADBUFFER 1024

struct FILEREADLINEBUFFER {
    char *next;
    char *last;
    char atEof;
    char lastCr;
    char continuationChar;
    char buffer[MAXFILEREADBUFFER];
    char *pseudoFile;
};

struct CLUSTERREQUEST {
    struct CLUSTERREQUEST *next;
    struct PEERMSG *msg;
#ifdef WIN32
    HANDLE condVar;
#else
#ifndef OLDSOLARIS
    pthread_cond_t condVar;
#endif
#endif
    int phase;
    char processed;
};

struct STATICSTRINGCOPY {
    struct STATICSTRINGCOPY *left, *right;
    char *string;
};

struct PROXYHOSTNAMEEDIT {
    char *find;
    char *replace;
};

struct SPUEDIT {
    struct SPUEDIT *next;
    char *find;
    char *replace;
    pcre *findPattern;
    pcre_extra *findPatternExtra;
    int cIpTypes;
    char stop;
    char redirect;
    char global;
    char malformed;
    char autoLoginIP;
    char excludeIP;
    char includeIP;
};

struct SPUEDITVAR {
    struct SPUEDITVAR *next;
    char *var;
    char *val;
    size_t varLen;
};

struct LOGINPORT {
    PORT port;
    struct sockaddr_storage ipInterface;
    SOCKET socket;
    BOOL virtualPort;
    int acceptSslIdx;
    char *z3950Host;
    GROUPMASK gm;
    PORT z3950Port;
    char z3950Socks;
    char useSsl;
    char forceHttp;
};

struct IPTYPE {
    char *autoUser;
    struct sockaddr_storage start;
    struct sockaddr_storage stop;
    GROUPMASK gmAuto; /* If this is an auto login, use this group mask for the user */
    enum IPACCESS ipAccess;
};

struct DNSADDR {
    struct DNSADDR *next;
    struct sockaddr_storage start;
    struct sockaddr_storage stop;
    struct sockaddr_storage addr;
    // unsigned char addr[4];
};

struct DNSPORT {
    PORT port;
    struct sockaddr_storage ipInterface;
    SOCKET tcpSocket;
    SOCKET udpSocket;
    struct DNSADDR *da;
};

struct REDIRECTSAFEORNEVERPROXY {
    struct REDIRECTSAFEORNEVERPROXY *next;
    struct DATABASE *database;
    char *domain;
    BOOL anyWild;
    PORT port;
    BOOL redirectSafe;
};

#define MAXNEVERPROXYHOST 63

struct NEVERPROXYHOST {
    char host[MAXNEVERPROXYHOST + 1];
    PORT port;
    char useSsl;
    char ftpgw;
};

struct VALIDATE {
    struct VALIDATE *next;
    char *auth;
    char *path;
    BOOL referenced;
};

struct INDOMAIN {
    struct INDOMAIN *next; /* RO */
    char *domain;          /* RO */
    struct VALIDATE *validate;
    char *proxyHostname;     /* Only applies for hostOnly */
    struct in_addr sin_addr; /* RO */
    struct in_addr sin_mask; /* RO */
    size_t len;              /* RO */
    PORT port;               /* RO */
    PORT forceMyPort;        /* RO */
    BOOL hostOnly;
    BOOL javaScript;
    BOOL wildcard;
    char useSsl;  /* Only applies for hostOnly */
    char gateway; /* Only meaningful for hostOnly */
    char ftpgw;   /* Only meaningful for hostOnly */
};

struct ENCRYPTVAR {
    struct ENCRYPTVAR *next;
    char *var;
    unsigned char key[EVP_MAX_KEY_LENGTH];
    int keyLen;
    unsigned char ivec[EVP_MAX_IV_LENGTH];
};

struct FORMVARS {
    struct FORMVARS *next;
    char *var;
    char *val;
    xmlRegexpPtr varRE;
    BOOL suppress;
    BOOL expr;
};

struct NETLIBRARYCONFIG {
    struct NETLIBRARYCONFIG *next;
    char *wideNetLibrarySignatureKey;
    size_t wideNetLibrarySignatureKeyLen;
    char *netLibraryPrivateKey;
    char *netLibraryLibId;
    char *netLibraryPartnerId;
    GROUPMASK gm;
    BOOL noUserToken;
};

struct FMGCONFIG {
    struct FMGCONFIG *next;
    char *inst;
    char *key;
    char *userClass;
    GROUPMASK gm;
    BOOL anonymous;
};

struct MYILIBRARYCONFIG {
    struct MYILIBRARYCONFIG *next;
    char *inst;
    char *key;
    GROUPMASK gm;
    enum SSOHASH hash;
};

struct REFERER {
    struct REFERER *next;
    char *tag;
    char *fileName;
    GROUPMASK gm;
    struct sockaddr_storage start;
    struct sockaddr_storage stop;
    int line;
};

struct DATABASE {
    char *title;   /* RO */
    char *url;     /* RO */
    char *desc;    /* RO */
    char *getName; /* RO */
    char *getHost;
    char *getUrl;
    struct DATABASE *getRelated;
    char *proxyHost;
    char *proxyAuth;
    char *proxySslHost;
    char *proxySslAuth;
    char *allowVars;
    char *formMethod;
    char *addUserHeader;
    struct REFERER *referer;
    unsigned char *tokenKey;
    unsigned char *tokenSignatureKey;
    char *books24X7Site;
    char *eblSecret;
    char *eblSecretHash;
    struct FORMVARS *formVars;
    char *formSelect;
    char *formSubmit;
    char *vars[MAXDBVARS];
    struct ENCRYPTVAR *ev;
    struct DATABASEUSAGELIMIT *databaseUsageLimits;
    struct NETLIBRARYCONFIG *netLibraryConfigs;
    struct FMGCONFIG *fmgConfigs;
    struct MYILIBRARYCONFIG *myiLibraryConfigs;
    struct HTTPHEADER *httpHeaders;
    struct COOKIEFILTER *cookieFilters;
    BOOL ignoreGlobalHttpHeaders;
    int sslIdx;
    PORT proxyPort;
    PORT proxySslPort;
    char getUseSsl;
    struct PATTERNS patterns;
    PORT getPort;
    struct INDOMAIN *dbdomains; /* RO */
    GROUPMASK gm;
    struct sockaddr_storage ipInterface;
    int cIpTypes;
    int cAnonymousUrls;
    int sessionLife;
    int hostCount;
    size_t addUserHeaderColon;
    BOOL anyJavaScript;
    BOOL getAppend;
    BOOL getAppendEncoded;
    BOOL getRedirect;
    BOOL getRefresh;
    enum OPTIONCOOKIE optionCookie;
    BOOL sendNoCache;
    BOOL redirectPatch;
    BOOL optionNoHttpsHyphens;
    BOOL optionHideEZproxy;
    BOOL optionProxyFtp;
    BOOL metaFindMuseCookie;
    BOOL optionXForwardedFor;
    BOOL optionMetaEZproxyRewriting;
    BOOL optionUTF16;
    BOOL optionGroupInReferer;
    BOOL rewriteHost;
    BOOL hideFromMenu;
    BOOL optionLawyeePatch;
    BOOL addUserHeaderBase64;

    struct MIMEFILTER *mimeFiltersDefault;
    struct MIMEFILTER *mimeFiltersUser;

    struct ADDHEADER *addHeader;
};

struct REDIRECTSAFEORNEVERPROXYURLDATABASE {
    struct DATABASE *database;
    char result;
};

/**
 * next - A pointer to the next header to add
 * name - The header name
 * expression - An ezproxy expression to be evaluated with ExpressionValue()
 * len - The length of <name>
 */
struct ADDHEADER {
    struct ADDHEADER *next;
    char *name;
    char *expression;
    int len;
};

/**
 * Hold additional information to display for admin status screen session info.
 */
struct ADMINUISESSION {
    struct ADMINUISESSION *next;
    char *title;
    char *expression;
};

struct DATABASECONFLICT {
    struct DATABASECONFLICT *next;
    struct DATABASE *b1;
    struct DATABASE *b2;
    int conflictMask;
};

struct GROUP {
    struct GROUP *next;
    char *name; /* RO */
    char *denyFile;
    size_t nameLen;
    GROUPMASK gm;
    GROUPMASK gmMaster;
    int index;
};

struct COOKIE { /* entire UA */
    struct COOKIE *next;
    char *name;
    char *val;
    /* domain used for domain-based cookies, host used for non-domain-based cookies (so one or other
     * not NULL) */
    char *domain;
    /*  struct HOST *host; */
    char *path;
    GROUPMASK gm;
    time_t expires;
};

struct OPENSSLCONFCMD {
    struct OPENSSLCONFCMD *next;
    int cert;
    char *cmd;
    char *value;
};

struct PDFURL {
    struct PDFURL *blink, *flink;
    char *url;
};

enum LOCATIONSTATUS { locationStatusInit = 0, locationStatusFound = 1, locationStatusNotFound = 2 };

struct LOCATION {
    char countryName[32];
    char region[32];
    char regionName[32];
    char city[32];
    double latitude;
    double longitude;
    enum LOCATIONSTATUS status;
    char countryCode[3];
};

struct LOCATIONRANGE {
    struct LOCATIONRANGE *next;
    struct sockaddr_storage start;
    struct sockaddr_storage stop;
    const char *countryCode;
    const char *region;
    const char *city;
    double latitude;
    double longitude;
};

/* ***** ***** ***** If you add variables here, be sure to check the session merge logic in
   admin.c/AdminLogin in case CGI is performing a connection for an existing user
   ***** ***** *****
*/
struct SESSION {
    struct COOKIE *cookies; /* UA */
    struct PDFURL *pdfUrls;
    char *genericUser;
    char *logUserBrief;
    char *logUserFull;
    char *accountUsername;
    char *cpipSid;
    char *loginBanner;
    char *sessionProxyAuth;
    char *docsCustomDir;
    char *vars[MAXSESSIONVARS];
    char *refererDestUrl;
    char *logupDenyFile;
    struct DATABASE *logupDb;
    char *logupUrl;
    struct VARIABLES *sessionVariables;
    struct VARIABLES *identifierVariables;
    struct USAGELIMITEXCEEDED *usageLimitExceeded;
    struct USAGE *usage;
    char lastCountryCode[3];
    struct sockaddr_storage sin_addr;
    struct sockaddr_storage blockCountryIP; /* RO */
    time_t created;                         /* RO */
    time_t lastAuthenticated;
    time_t confirmed; /* UA */
    time_t accessed;  /* UA */
    time_t relogin;
    time_t cookiesSaved;
    time_t cookiesChanged;
    time_t sciFinderAuthorized;
    struct sockaddr_storage ipInterface;
    GROUPMASK gm;
    BOOL reloginRequired;
    int lifetime;
    int userLimitExceeded;
    int logupId;
    int nextFiddlerSessionIndex;
    unsigned int sessionFlags;
    enum PROXYTYPE proxyType;
    enum AUTOLOGINBY autoLoginBy;
    char key[MAXKEY];
    char connectKey[MAXKEY];
    char adminKey[MAXKEY];
    char userObjectTicket[MAXUSEROBJECTTICKET];
    char menu[MAXMENU];
    char csrfToken[CSRF_TOKEN_LEN];
    char active; /* 0 not active, 1 active, 2 terminating and should be freed when all links to
                    session gone */
    char priv;   /* As of 4.1 and later, priv is a mask based on SESSION_PRIV_*** */
    char expirable;
    char notify;
    char accountChangePass;
    char domainCookieSuffix;
    char anonymous;
    char loggedinShibboleth;
    char fiddlerActive;
};

struct HOST {
    char *hostname; /* RO */
    char *hostUrl;
    char *proxyHostname;       /* RO */
    struct VALIDATE *validate; /* RO */
    struct DATABASE *myDb;     /* RO */
    time_t created;            /* RO */
    time_t referenced;         /* UA */
    time_t accessed;           /* UA */
    int references;            /* UA */
    int accesses;              /* UA */
    struct IDENTIFIERSCOPE *identifierScope;
    SOCKET socket;    /* UA but only by main thread */
    PORT remport;     /* RO */
    PORT myPort;      /* RO */
    PORT forceMyPort; /* RO */
    BOOL javaScript;
    char active; /* RO */
    char useSsl;
    char gateway;
    char ftpgw;  // ftp gateway
    char stopListener;
    char dontSave;
    char neverProxy;
};

struct TRANSLATE {
    struct TRANSLATE *nextWaitingTranslate;
    struct TRANSLATE *nextRawTranslate;
    struct LOGINPORT *loginPort;
    struct SESSION *session;
    struct HOST *host;
    struct VARIABLES *localVariables;
    struct sockaddr_storage sin_addr;
    struct sockaddr_storage Peer;
    struct UUSOCKET ssocket, ftpsocket, wsocketTemp, *wsocketPtr;
    char *myUrl;
    char *tranWritePtr;
    char *museCookie;
    char *museReferer;
    char *docsCustomDir;
    char *logTag;
    char *fiddlerRequest;
    struct LOCATION location;
    struct INTRUDERIP *intruderIP;
    double mozillaVersion;
    int tranWriteRemain;
    int acceptSslIdx;
    int sequence;
    /*  char *rawReadPtr, *rawWritePtr; */
    /*  char rawBuffer[2][MAXRAWBUFF]; */
    time_t activated;
    time_t rawLast;
    time_t ifModifiedSince;
    time_t eblSecretTimestamp;
    unsigned long findReplaceStates;
    /*  int rawReadRemain; */
    /*  int rawReadCompleted; */
    /*  int rawWriteCompleted; */
    /*  short rawReadBytes; */
    /*  short rawWriteBytes; */
    /*  short rawReadMax; */
    /*  short rawWriteMax; */
    /* strategy 2 in copying */
    int rawReadPage;
    int rawWritePage;
    int rawReadOfs;
    int rawWriteOfs;
    unsigned int peerLen;
    int finalStatus;
    int intruderIPLevel;
    PORT logPort;
    int clientVersionX1000; /* version * 1000, so 1.0 is 1000, 1.1 is 1100, etc. */
    int serverVersionX1000; /* version * 1000, so 1.0 is 1000, 1.1 is 1100, etc. */
    char method[MAXMETHOD];
    char version[MAXVERSION];
    char url[MAXURL];
    char urlCopy[MAXURL];
    char urlRawRequested[MAXURL];
    intmax_t contentLength;
    char buffer[MAXBUFFER];
    char inbuffer[MAXINBUFFER];
    char dummy[MAXINBUFFER];
    char contentType[256];
    char requestHeadersBuffer[MAXREQUESTHEADERS];
    char *requestHeaders;
    char csrfToken[CSRF_TOKEN_LEN];
    BOOL html;
    BOOL javaScript;
    BOOL maybeLogin;
    int reject;
    int keepAlivesRemaining;
    BOOL sentPostContent;
    BOOL rewritingDisabled;
    BOOL safari;
    BOOL ignoreIntrusion;
    int zip;
    enum PROXYTYPE proxyType;
    enum REDIRECTSAFEORNEVERPROXYCACHE redirectSafeOrNeverProxyCache;
    char active;
    char raw;
    char needPdfRefresh;
    char isMSIE;
    char isMacMSIE;
    char isPDF;
    char expect100Continue;
    char requireAuthenticate;
    char suppressPostContent;
    char useSsl;
    char ssocketInitialized;
    char connectedByProxy;
    char clientKeepAlive;
    char denyClientKeepAlive;
    char serverKeepAlive;
    char excludeIPBanner;
    char excludeIPBannerSuppress;
    char logTagApproved;
    char logTagSpur;
    char sendChunkingAllowed;
    char sendContentLengthSent;
    char sendZipAllowed;
    char sentKeepAlive;
    char bodylessStatus;
    char rawReadEof;
    char alreadyEnded;
};

struct DISPATCH {
    char *cmd;
    void (*func)(struct TRANSLATE *t, char *query, char *post);
    int flags;
    char *groups;
    GROUPMASK gm;
};

struct LOGSPU {
    struct LOGSPU *next;
    FILE *file;
    char *filename;
    char *format;
    BOOL strftimeFilename;
    BOOL activeFilenameChanged;
    char activeFilename[MAX_PATH];
};

enum MIMEACTION { javascript, html, text, pdf, image, none };
extern char *MIMEACTIONNAMES[];

struct BYTESERVE {
    struct BYTESERVE *next;
    pcre *regexUri;
    pcre_extra *regexUriExtra;
    char *uri;
};

struct MIMEFILTER {
    struct MIMEFILTER *next;
    pcre *regexUri;
    pcre *regexMime;
    pcre_extra *regexUriExtra;
    pcre_extra *regexMimeExtra;
    char *mime;
    char *uri;
    BOOL noDefault;
    enum MIMEACTION action;
};

struct LOGTAGMAP {
    struct LOGTAGMAP *next;
    char *logTag;
    char *url;
    xmlRegexpPtr tagRE;
    xmlRegexpPtr urlRE;
    BOOL caseSensitive;
    BOOL approve;
};

/* This must match the space consumed by all fields up to and including datalen */
#define PEERMSGHDRSIZE 10
#define HAPEERMSGHDRSIZE 10
#define CLUSTERVERSION 0x0202
#define HAVERSION 0x0101

struct PEERMSG {
    short version;
    char type;
    char cmd;
    unsigned int msgid;
    unsigned short datalen;
    char data[MAXCLUSTERDATA];
};

enum PEERACK { PANOTWAITING, PAWAITING, PAACKED, PADIED };

struct PEER {
    char *peerName;
    struct sockaddr_storage peerAddress;
    time_t lastHeardFrom, lastSentTo;
    enum PEERSTATUS peerStatus;
    int smsglen;
    int rmsglen;
    enum PEERACK sAck;
    struct PEERMSG smsg; /* send message */
    struct PEERMSG rmsg; /* receive message */
    char pmstate;
};

enum HAPEERACK { HANOTWAITING, HAWAITING, HAACKED, HADIED };

struct HAPEERMSG {
    short version;
    char type;
    char cmd;
    unsigned int msgid;
    unsigned short datalen;
    char data[MAXCLUSTERDATA];
};

struct LBPEER {
    struct LBPEER *next;
    char *host;
    char *id;
    struct sockaddr_storage ipInterface;
    size_t idLen;
    PORT tcpPort;
    PORT udpPort;
    char useSsl;
};

struct HAPEER {
    struct HAPEER *next;
    UUMUTEX mutex;
    char *haPeerName;
    struct sockaddr_storage haPeerAddress;
    char *host;
    time_t lastHeardFrom, lastSentTo, lastFailure;
    enum HAPEERSTATUS haPeerStatus;
    enum HAPEERACK sAck;
    struct HAPEERMSG smsg; /* send message */
    struct HAPEERMSG rmsg; /* receive message */
    int failures;
    int trying;
    PORT port;
    PORT udpPort;
    char useSsl;
};

#define FORMFIELDQUERYSTRING 1
#define FORMFIELDALLOWMULTIVALUES 2
#define FORMFIELDTAKELASTVALUE 4
#define FORMFIELDNOTRIMCTRL 8

struct FORMFIELDMULTIVALUE {
    struct FORMFIELDMULTIVALUE *nextMultiValue;
    char *value;
};

struct FORMFIELD {
    char *name;
    char *value;
    BOOL rest;
    BOOL dontUnescape;
    size_t maxLen;
    unsigned long flags;
    struct FORMFIELDMULTIVALUE *nextMultiValue;
};

struct PDFREFRESH {
    char *userAgent;
};

struct STARTINGPOINTURLREFRESH {
    char *filter;
    char *userAgent;
    char mode;
};

struct LOGFILTER {
    char *filter;
};

enum COOKIEFILTER_PROCESSING { CFPBLOCK, CFPBLOCKMANDATORY, CFPPROCESS };

struct COOKIEFILTER {
    struct COOKIEFILTER *next;
    char *cookieName;
    enum COOKIEFILTER_PROCESSING processing;
};

struct ANONYMOUSURL {
    char *anonymousUrl;
    xmlRegexpPtr regularExpression;
    BOOL caseSensitive;
    BOOL options;
    char mode;
    char activeIP;
};

struct FILEINCLUSIONNAME {
    struct FILEINCLUSIONNAME *next;
    char *fileName;
    int fileNameIndex;
};

struct FILEINCLUSION {
    struct FILEINCLUSION *previous;
    struct FILEINCLUSIONNAME *fileNameListHead;
    char *fileName;
    int fileNameIndex;
    int lineNumber;
    int depth;
};

struct XFFREMOTEIPRANGE {
    struct XFFREMOTEIPRANGE *next;
    struct sockaddr_storage start;
    struct sockaddr_storage stop;
    BOOL trustInternal;
};

#define MYNAMELEN 256

extern struct XFFREMOTEIPRANGE *xffRemoteIPRanges;
extern char *xffRemoteIPHeader;
extern int connectWindow;
extern int environment;
extern volatile int networkTestsRunning;
extern char *monthNames[];
extern int UUDEFAULT_FAMILY;
extern struct sockaddr_storage defaultIpInterface;
extern struct sockaddr_storage minProxyIpInterface;
/* extern rwlock_t rwlData; */
#ifndef WIN32
extern uid_t runUid;
extern gid_t runGid;
extern char changedUid;
extern char changedGid;
#endif
extern struct IPTYPE *ipTypes;
extern struct IPTYPE *rejectIpTypes;
/* extern struct AUTHHOST *authHosts; */
extern struct DATABASE *databases;
extern struct TRACKMUTEX *trackMutexes;
extern struct EBRARYSITE *ebrarySites;
extern struct OVERDRIVESITE *overDriveSites;
extern struct GARTNERCONFIG *gartnerConfigs;
extern BOOL gartnerKeys;
extern GROUPMASK gartnerGm;
extern GROUPMASK gartnerGm;
extern GROUPMASK gmAdminAny;
extern GROUPMASK gmAdminAudit;
extern GROUPMASK gmAdminDecryptVar;
extern GROUPMASK gmAdminFiddler;
extern GROUPMASK gmAdminGroups;
extern GROUPMASK gmAdminIdentifier;
extern GROUPMASK gmAdminIntrusion;
extern GROUPMASK gmAdminLDAP;
extern GROUPMASK gmAdminMessages;
extern GROUPMASK gmAdminRestart;
extern GROUPMASK gmAdminSecurity;
extern GROUPMASK gmAdminShibboleth;
extern GROUPMASK gmAdminSSLUpdate;
extern GROUPMASK gmAdminSSLView;
extern GROUPMASK gmAdminStatusUpdate;
extern GROUPMASK gmAdminStatusView;
extern GROUPMASK gmAdminToken;
extern GROUPMASK gmAdminUsage;
extern GROUPMASK gmAdminUsageLimits;
extern GROUPMASK gmAdminUser;
extern GROUPMASK gmAdminVariables;
extern struct SSOSITE *ssoSites;
extern struct DENYIFREQUESTHEADER *denyIfRequestHeaders;
extern struct HTTPMETHOD *httpMethods;
extern struct HTTPHEADER *globalHttpHeaders;
extern struct HTTPHEADER *serverHttpHeaders;
extern struct EXTRALOGINCOOKIE *extraLoginCookies;
extern struct DNSPORT *dnsPorts;
extern struct GROUP *groups;
extern struct SESSION *sessions;
extern struct UUAVLTREE avlSessions;
extern struct UUAVLTREE avlIntrusionIPs;
extern struct IPTYPE *whitelistIPs;
extern struct HOST *hosts;
extern struct UUAVLTREE avlHosts;
extern struct UUAVLTREE avlNeverProxyHosts;
extern struct REDIRECTSAFEORNEVERPROXY *redirectSafeOrNeverProxy;
extern struct NEVERPROXYHOST *neverProxyHosts;
extern struct NEVERPROXYHOST *neverProxyHostsLast;
extern struct PEER *peers;
extern struct HAPEER *haPeers;
extern struct LBPEER *lbPeers;
extern struct LBPEER *myLbPeer;
extern BOOL haPeersValid;
extern struct REROUTE *reroutes;
extern struct TRANSLATE *translates;
extern struct TRANSLATE *firstWaitingTranslate;
extern struct TRANSLATE *lastWaitingTranslate;
extern struct PDFREFRESH *pdfRefreshes;
extern char *pdfRefreshPre;
extern char *pdfRefreshPost;
extern struct USAGELIMIT *usageLimits;
extern struct USAGELIMIT *intruderUsageLimit;
extern struct USAGELIMITEXCEEDED *usageLimitExceededs;
extern struct DATABASEUSAGELIMIT *localDatabaseUsageLimits;
extern struct INTRUDERIP *intruderIPs;
extern BOOL anyIntruderIPRejects;
extern BOOL anySourceIP;
extern struct STARTINGPOINTURLREFRESH *startingPointUrlRefreshes;
extern struct LOGFILTER *logFilters;
extern struct COOKIEFILTER *globalCookieFilters;
extern struct COOKIE *defaultCookies;
extern struct ANONYMOUSURL *anonymousUrls;
extern struct LOGINPORT *loginPorts;
extern struct PROXYHOSTNAMEEDIT *proxyHostnameEdits;
extern struct SPUEDIT *spuEdits;
extern struct SPUEDITVAR *spuEditVars;
extern struct STATICSTRINGCOPY *staticStringCopies;
extern struct INTRUDERIPATTEMPT *intruderIPAttempts;
extern int intruderLog;
extern int intruderReject;
extern int intruderTimeout;
extern struct LOCATIONRANGE *locationRanges;

extern struct DISPATCH loginDispatchTable[];

extern int loginSocketBacklog;
extern int hostSocketBacklog;
extern int dnsSocketBacklog;

extern char *mainMenu;
extern int cIpTypes;
extern int cRejectIpTypes;
extern int cWhitelistIPs;
/* extern int cAuthHosts; */
extern int cDatabases;
extern int cDnsPorts;
extern int cGroups;
extern int cSessions;
extern int cHosts;
extern int cPeers;
extern int cHaPeers;
extern int cReroutes;
extern int cTranslates;
extern int cPdfRefreshes;
extern int cStartingPointUrlRefreshes;
extern int cLogFilters;
extern int cAnonymousUrls;
extern int cLoginPorts;
extern int cMimeFilters;
extern int cProxyHostnameEdits;
extern int mIpTypes;
extern int mRejectIpTypes;
extern int mWhitelistIPs;
extern int mDatabases;
extern int mDnsPorts;
extern size_t mGroupMask;
extern int mSessions;
extern int mHosts;
extern int mNeverProxyHosts;
extern int mPeers;
extern int mHaPeers;
extern int mReroutes;
extern int mTranslates;
extern int mSessionLife;
extern int mPdfRefreshes;
extern int mStartingPointUrlRefreshes;
extern int mLogFilters;
extern int mAnonymousUrls;
extern int mLoginPorts;
extern int mProxyHostnameEdits;
extern int pSessions;
extern int pHosts;
extern int pTranslates;
extern int sessionKeySize;
extern char *tokenSalt;
extern char *autoLoginIPBanner;
extern char *excludeIPBanner;
extern char excludeIPBannerOnce;
extern int potentialListeners;
extern int maxIncreaseProxyHostnameEdit;
extern PORT firstPort;
extern PORT primaryLoginPort;
extern PORT primaryLoginPortSsl;
extern char myName[MYNAMELEN];
extern size_t myNameLen;
extern char myHttpName[MYNAMELEN];
extern char myHttpsName[MYNAMELEN];
extern char *serverHeader;
extern char *haName;
extern char *myBinary;
extern char *myPath;
extern char *myStartupPath;
extern char *myChangedPath;
extern char *p3pHeader;

extern struct HOST *mvHost;
extern struct HOST *mvHostSsl;
extern char *cpipSecret;
extern struct SESSION *anonymousSession;

extern char *mailServer;
extern char *mailFrom;
extern char *mailTo;

extern int defaultFileMode;
extern int defaultUmask;

extern int mutexTimeout;
#ifdef WIN32
extern DWORD dMutexTimeout;
#endif

extern char *myUrlHttp;
extern char *myUrlHttps;
extern char *myDomain;
extern BOOL ttyDetach;
extern BOOL ttyClosed;
extern int msgFile;
extern char *msgFilenameTemplate;
extern BOOL msgFilenameStrftime;
extern BOOL msgFilenameChanged;
extern char msgFilename[MAX_PATH];
extern int auditPurge;
extern char *auditFileTemplate;
extern time_t startupTime;
extern time_t dnsSerial;
extern time_t hostsNeedSave;
extern time_t nextExpire;
extern time_t nextSaveCookies;

extern int wskeyExpirePeriod;
extern int wskeyThreadSleep;
extern int wskeyRecheckPeriod;
extern int wskeyRecheckFailPeriod;
extern const char *wsKeyServiceHost;
extern BOOL wsKeyForceProxy;
extern BOOL wsKeyServiceSsl;
extern PORT wsKeyServicePort;
extern BOOL wsKeyReadConfig;
extern struct PROXY *wsKeyProxies;
extern int wsKeyProxiesCount;
extern time_t validation;
extern int hostsSaved;
extern int usageSaved;
extern char *firstProxyHost;
extern PORT firstProxyPort;
extern char *firstProxyAuth;
extern char *minProxyAuth;
extern int debugLevel;
extern int environment;
extern int usageLogUser;
extern char *shibbolethWAYF;
extern char *shibbolethProviderId;
extern BOOL anyEncryptVar;
extern BOOL anyInvalidDomains;
extern BOOL usageLogSession;
extern BOOL logMail;
extern BOOL logReferer;
extern BOOL optionEZproxyCookieHTTPOnly;
extern BOOL optionRedirectUnknown;
extern BOOL optionPrecreateHosts;
extern BOOL optionBlockCountryChange;
extern BOOL optionSuppressObfuscation;
extern BOOL optionDprintf;
extern BOOL optionAcceptXForwardedFor;
extern BOOL optionRecordPeaks;
extern BOOL optionCaptureSpur;
extern BOOL anyOptionGroupInReferer;
extern BOOL optionDispatchLog;
extern BOOL optionLogThreadStartup;
extern BOOL optionLoginReplaceGroups;
extern BOOL optionDisableTestEZproxyUsr;
extern BOOL optionDisableHTTP11;
extern BOOL optionTicketIgnoreExcludeIP;
extern BOOL optionEbraryUnencodedTokens;
extern BOOL optionUsernameCaretN;
extern BOOL optionUserObjectTestMode;
extern BOOL optionUserObjectGetTokenAlwaysRenew;
extern BOOL optionSilenceIPWarnings;
extern GROUPMASK gmUserObject;
extern BOOL optionAllowWebSubdirectories;
extern BOOL optionAllowLogoutURLRedirect;
extern BOOL optionPassiveCookie;
extern BOOL optionPostCRLF;
extern BOOL optionForceSsl;
extern BOOL optionAllowHTTPLogin;
extern BOOL optionForceHttpsAdmin;
extern BOOL optionWildcardHTTPS;
extern BOOL optionProxyUserObject;
extern BOOL optionIgnoreWildcardCertificate;
extern BOOL optionForceWildcardCertificate;
extern BOOL optionShibboleth;
extern BOOL optionLogSPUEdit;
extern BOOL optionAnyDNSHostname;
extern BOOL optionSecurityStartupStats;
extern BOOL optionSQLStats;
extern BOOL optionStatus;
extern BOOL optionTicks;
extern BOOL optionTrackSockets;
extern BOOL optionSingleSession;
extern BOOL optionSingleThreadedTranslations;
extern BOOL optionDisableNagle;
extern BOOL optionDisableSortable;
extern BOOL optionAllowDebugger;
extern BOOL optionRefererInHostname;
extern BOOL optionLoginNoCache;
extern BOOL optionLogDNS;
extern BOOL optionLogSockets;
extern BOOL optionFiddler;
extern BOOL optionLogSAML;
extern BOOL optionLogXML;
extern BOOL optionIgnoreSIGCHLD;
extern BOOL optionRelaxedRADIUS;
extern BOOL optionExcludeIPMenu;
extern BOOL optionMenuByGroups;
extern BOOL optionDisableTestLDAPInternal;
extern BOOL optionHide192;
extern BOOL optionAllowBadDomains;
extern BOOL optionReboot;
extern BOOL optionSafariCookiePatch;
extern BOOL optionHeaders;
extern BOOL optionCSRFToken;
extern int cacheControlMaxAge;
extern int loginCookieSameSite;

extern char *defaultCharset;

/* SSL strength control */
extern char *SSLCipherSuiteInbound;
extern char *SSLCipherSuiteOutbound;
extern BOOL sslHonorCipherOrder;
extern struct OPENSSLCONFCMD *openSSLConfCmds;

extern struct ADMINUISESSION *adminUIsessFields;

extern BOOL optionAllowSendChunking;
extern BOOL optionUseRaw;
extern int optionAllowSendGzip;
extern BOOL optionKeepAlive;
extern int keepAliveTimeout;
extern int maxKeepAliveRequests;
extern int receiveBufferSize;
extern int sendBufferSize;
extern int minProxyDelay;
extern char *proxyUrlPassword;
extern char *pidFileName;
extern int pidFileGuardian;
extern int pidFileCharge;

#define NOSPACEHST 1
#define NOSPACELOG 2
#define NOSPACECOOKIES 4

extern unsigned int noSpace;

extern char *mallocCheck;
extern int binaryTimeout;
extern int remoteTimeout;
extern int clientTimeout;
extern enum PROXYTYPE optionProxyType;
extern char *accountDefaultPassword;
extern BOOL optionRequireAuthenticate;
/* extern char *externAuth; */
extern char *base64;
extern struct LOGSPU *logSPUs;
extern struct LOGTAGMAP *logTagMaps;
extern struct MIMEFILTER *mimeFiltersDefault;
extern struct MIMEFILTER *mimeFiltersUser;
extern struct BYTESERVE *byteServes;
extern struct STOPSOCKET *stopSockets;
#ifdef WIN32
extern char *guardianSharedMemoryID;
extern BOOL runningAsService;
#else
extern int guardianSharedMemoryID;
extern int mySharedMemoryID;
#endif
extern pid_t guardianPid;
extern pid_t chargePid;
extern pid_t sessionPID;
extern int radiusTimeout;
extern int radiusRetry;
extern char *openURLResolver;
extern char *loginCookieName;
extern char *requireAuthenticateCookieName;
extern char *loginCookieDomain;
extern char *refererCookieName;
extern enum LOGINCOOKIEDOMAINTYPE loginCookieDomainType;
extern size_t loginCookieNameLen;
extern time_t lastModifiedMinimum;
extern char ezproxyCfgDigest[];
extern BOOL someKey;
extern char *privateComputerCookieName;
extern size_t privateComputerCookieNameLen;

extern int ticksMain;
extern int ticksCluster;
extern int ticksStopSockets;
extern int ticksRaw;
extern int ticksTranslate;

#ifdef UUTIME_R0
extern UUMUTEX mTimeFuncs;
#endif

extern struct RWL rwlHosts;
extern struct RWL rwlSessions;
extern struct RWL rwlIntrusionIPs;
extern struct RWL rwlIpPortStatuses;
extern struct RWL rwlDnsTable;
extern UUMUTEX mIpPortStatuses;
extern UUMUTEX mDnsTable;
extern UUMUTEX mAuditFile;
extern UUMUTEX mAccessFile;
extern UUMUTEX mMsgFile;
extern UUMUTEX mStopSockets;
extern UUMUTEX mActive;
extern UUMUTEX mToken;
extern UUMUTEX mCookies;
extern UUMUTEX mInitSslKey;
extern UUMUTEX mInitSsl;
extern UUMUTEX mCluster;
extern UUMUTEX mGetHostByName;
extern UUMUTEX mGetHostByNameTree;
extern UUMUTEX mAccountFile;
extern UUMUTEX mTrackSocket;
extern UUMUTEX mPdfUrls;
extern UUMUTEX mAdminSsl;
extern UUMUTEX mSslCtxCache;
extern UUMUTEX mIntruders;
extern UUMUTEX mUsage;
extern UUMUTEX mLDAPBind;
extern UUMUTEX mShibbolethShire;
extern UUMUTEX mCasTickets;
extern UUMUTEX mFiddler;
extern UUMUTEX mSecurity;
extern UUMUTEX mGenerateUniqueKey;

/* Must keep PROTOCOL values synchronized with protocolNames in global.c */
/* Also, there is logic that assumes that if a PROTOCOL > PROTOCOLHTTPS, then it is a non-http
   request, so always be sure that anything that seems like HTTP or HTTPS is <= PROTOCOLHTTPS
*/
enum PROTOCOL { PROTOCOLHTTP, PROTOCOLHTTPS, PROTOCOLFTP, PROTOCLLAST };
struct PROTOCOLDETAIL {
    enum PROTOCOL protocol;
    char *name;
    size_t nameLen;
    PORT defaultPort;
};
extern struct PROTOCOLDETAIL protocolDetails[];

extern volatile int restarting;
extern volatile int sigusr;
extern volatile int rebuildMainMaster;

#define LOGFILE EZPROXYNAMEPROPER_ALL_LOWER ".log"
#define FAILFILE EZPROXYNAMEPROPER_ALL_LOWER ".ftl"
#define HOSTFILE EZPROXYNAMEPROPER_ALL_LOWER ".hst"
#define HOSTFILENEW EZPROXYNAMEPROPER_ALL_LOWER ".hnw"
#define ULSFILE EZPROXYNAMEPROPER_ALL_LOWER ".uls"
#define ULSFILENEW EZPROXYNAMEPROPER_ALL_LOWER ".unw"
#define EZPROXYMSG "messages.txt"
#define IPCFILE EZPROXYNAMEPROPER_ALL_LOWER ".ipc"
#define IPCFILELOCK EZPROXYNAMEPROPER_ALL_LOWER ".lck"
#define IPCFILENEW EZPROXYNAMEPROPER_ALL_LOWER ".inw"
#define MIMETYPE "mimetype"
#define LOGFORMAT "%h %l %u %t \"%r\" %s %b"
#define FMT_FILE_INDEX "5"
#define FMT_LINE_NUMBER "5"

/* Prototypes for account.c */
enum ACCOUNTFINDRESULT AccountFind(char *user,
                                   char *pass,
                                   char *defaultPass,
                                   char *newPass,
                                   BOOL createIfNotFound);
void AccountMain(int argc, char **argv, int i);

/* Protoypes for admin.c */
void AdminRedirect(struct TRANSLATE *t);
void SendTokenizedURL(struct TRANSLATE *t,
                      struct DATABASE *b,
                      char *url,
                      char **token,
                      char **tokenSignature,
                      BOOL allowUsernamePassword);
void SendTokenizedURL2(struct TRANSLATE *t,
                       struct UUSOCKET *s,
                       struct DATABASE *b,
                       char *url,
                       char **token,
                       char **tokenSignature,
                       BOOL allowUsernamePassword);
void DoAdmin(struct TRANSLATE *t);
void AdminRelogin(struct TRANSLATE *t);
void AdminStarted(struct UUSOCKET *s);
char *AddEncodedField(char *s, char *f, char *stop);
char *AddEncodedFieldLimited(char *s, char *f, size_t limited, char *stop);
char *AddDoubleEncodedField(char *s, char *f, char *stop);
char *AddHTMLEncodedField(char *s, char *f, char *stop);
void AdminBaseHeader(struct TRANSLATE *t, int options, char *title, char *extra);
void AdminBaseHeader2(struct TRANSLATE *t, int options, char *title, char *extra, char *extraHead);
void AdminHeader(struct TRANSLATE *t, int options, char *title);
void AdminMinimalHeader(struct TRANSLATE *t, char *title);
void AdminMinimalFooter(struct TRANSLATE *t);
void AdminLoginVars(struct SESSION *e, struct USERINFO *uip, BOOL combine);
void AdminLogup(struct TRANSLATE *t,
                BOOL makeUrl,
                char *url,
                struct DATABASE *b,
                GROUPMASK requiredGm,
                BOOL isIntruder);
void AdminLogup2(struct TRANSLATE *t, char *up);
BOOL AdminNetworkTest(struct TRANSLATE *t);
void AdminFooter(struct TRANSLATE *t, BOOL suppressCopyright);
BOOL AdminReloginIfRequired(struct TRANSLATE *t);
struct OVERDRIVESITE *AdminOverDriveSite(char *site, size_t siteLen);
void WriteField(struct TRANSLATE *t,
                char *label,
                char *name,
                char *value,
                char *error,
                int size,
                int max,
                char *fieldType);
void WriteCheckbox(struct TRANSLATE *t, char *label, char *name, BOOL checked, char *error);
struct DISPATCH *FindLoginDispatch(char *url);
void LocalRedirect(struct TRANSLATE *t, char *url, int status, BOOL useRefresh, struct DATABASE *b);
void MergeSessionVariables(struct TRANSLATE *t, struct SESSION *e);
BOOL AdminSslCheckWildcardsHttps(BOOL running);
char *FirstSlashQuestion(char *s);
void SendObfuscated(struct UUSOCKET *s, char *p, char *stop);
char *ValidRefererHostname(char *host, size_t len, char *specific);
char *RefererURL(char *url, char *referer, int useSsl);
void AdminNoToken(struct TRANSLATE *t);
void AdminUnauthorized(struct TRANSLATE *t);
int UUCopyFile(char *newFile, char *oldFile);

/* Prototypes for host.c */

struct REDIRECTSAFEORNEVERPROXYURLDATABASE *RedirectSafeOrNeverProxyHost(
    char *host,
    PORT port,
    BOOL includeRedirectSafe,
    struct REDIRECTSAFEORNEVERPROXYURLDATABASE *RedirectSafeOrNeverProxyURLDatabase);
struct REDIRECTSAFEORNEVERPROXYURLDATABASE *RedirectSafeOrNeverProxyURL(
    char *url,
    BOOL includeRedirectSafe,
    struct REDIRECTSAFEORNEVERPROXYURLDATABASE *RedirectSafeOrNeverProxyURLDatabase);
struct DATABASE *NextMatchingDatabase(struct DATABASE *b, struct HOST *h);
void RestoreValidations(struct HOST *h);
void StartLoginPorts(void);
void SetMyUrlHttp(void);
void SetMyUrlHttps(void);
void SetMyHttpName(void);
void SetMyHttpsName(void);
SOCKET StartListener(PORT port, struct sockaddr_storage *mysl, int backlog);
char *GetSnipField(char *query, char *field, BOOL snipField, BOOL returnCopy);
void PrecreateHosts(void);

/* Prototypes for check.c */
void CheckConnection(char *checkPort);

/* Prototypes for cookie.c */
void SendDomainCookies(struct TRANSLATE *t, char *receivedCookies);
BOOL SetDomainCookie(struct TRANSLATE *t, char *field);
struct COOKIE *TossCookie(struct COOKIE *k);
struct COOKIE *ClusterTossCookie(struct COOKIE *k);
void ClusterAddCookie(struct SESSION *s, char *name, char *domain, char *path, time_t expires);
void MergeSessionCookies(struct TRANSLATE *t, char *srcSession);
void RestoreCookies(void);
void SaveCookies(BOOL mandatory);
void SendEZproxyCookie(struct TRANSLATE *t, struct SESSION *e);
void TossCookies(struct SESSION *s);

/* Prototypes for cluster.c */
void DoCluster(void *v);
struct PEER *FindPeerByAddress(struct sockaddr_storage *addr);
int DecodeFields(void *vbuffer, int *remain, ...);
int EncodeFields(void *vbuffer, int *remain, ...);
void ClusterInitMsg(struct PEERMSG *msg, char type, char cmd, int msgid);
void ClusterRequest(struct PEERMSG *msg);
void ClusterUpdateSession(struct SESSION *e);
void ClusterMsgCookie(struct SESSION *e,
                      char *name,
                      char *domain,
                      char *path,
                      time_t expires,
                      struct PEERMSG *msg);
void ClusterMsgSession(struct SESSION *e, struct PEERMSG *msg);
int strnullcmp(const char *s1, const char *s2);
int strinullcmp(const char *s1, const char *s2);
int StartUdpSocket(SOCKET *s, PORT port);

/* Prototypes for database.c */
BOOL DoShibbolethAcceptanceControl(int control, int condition1, int httpsGet);
BOOL DoShibbolethAcceptanceLogControl(int control, int condition1);
void ReadConfig(BOOL nameOnly);
void ReadConfigDebug(char *arg);
void ReadConfigEnvironment(const char *arg, BOOL nameOnly);
void WarnIfLowPort(int port, char checkingFirstPort);
struct HAPEER *FindHAPeer(char *name, BOOL add);
BOOL PublicAddress(struct sockaddr_storage *la);
void ReadConfigSetString(char **string, char *val);
void ReadConfigHAPeerValidate(void);
struct LBPEER *FindLBPeer(char *id, size_t idLen, BOOL add);

/* Prototypes for dns.c */
void DoDNS(void *v);
int StartDnsSockets(void);

/* Prototypes for files.c */
void InstallFiles(char *me, int ifmode, char subtype);
void FilesSecurityDefault(char **filename, char **fullname, char **filetext);

/* Prototypes for globals.c */
void InitMutexes(void);
void InitUUAvlTrees(void);

/* Prototypes for group.c */
struct GROUP *FindGroup(char *name, size_t len, BOOL add);
GROUPMASK MakeGroupMask(GROUPMASK gm, char *names, BOOL add, char mode);
GROUPMASK GroupMaskNew(size_t size);
void GroupMaskFree(GROUPMASK *g1);
void GroupMaskDebug(GROUPMASK gm);
GROUPMASK GroupMaskDefault(GROUPMASK g1);
GROUPMASK GroupMaskAnd(GROUPMASK g1, GROUPMASK g2);
GROUPMASK GroupMaskOr(GROUPMASK g1, GROUPMASK g2);
GROUPMASK GroupMaskMinus(GROUPMASK g1, GROUPMASK g2);
GROUPMASK GroupMaskNot(GROUPMASK g1);
BOOL GroupMaskWrite(GROUPMASK gm, struct UUSOCKET *s, struct USERINFO *uip);
BOOL GroupMaskOverlap(GROUPMASK g1, GROUPMASK g2);
GROUPMASK GroupMaskCopy(GROUPMASK g1, GROUPMASK g2);
void GroupMaskClear(GROUPMASK g1);
void GroupMaskSet(GROUPMASK g1, unsigned int group);
void GroupMaskReset(GROUPMASK g1, int group);
BOOL GroupMaskIsSet(GROUPMASK g1, int group);
BOOL GroupMaskEqual(GROUPMASK g1, GROUPMASK g2);
size_t GroupMaskCardinality(GROUPMASK g1);
BOOL GroupMaskIsNull(GROUPMASK g1);
void LoggedInGroups(void);
void ResizeAllGroupMasks();
BOOL SingletonGroupName(char *name);
BOOL IsAnAdminGroup(struct GROUP *g);

/* Prototypes for ha.c */
void DoHA(void *v);

/* Prototypes for host.c */
struct HOST *FindHost(char *ihost, char useSsl, char ftpgw);
struct HOST *ClusterFindHost(char *host, PORT remport, char useSsl, char ftpgw, BOOL fromCluster);
char *IPInterfaceName(char *buffer, int size, struct sockaddr_storage *sa);
BOOL IsALoginPort(PORT port);
BOOL IsAHostPort(PORT port);
BOOL IsAnActivePort(PORT port);
BOOL IsMyHost(char *host);
void LoadHosts(void);
#ifdef WIN32
void LoadIPC(int *gpid, int *cpid, char **smid, int *error);
#else
void LoadIPC(pid_t *gpid, pid_t *cpid, int *smid, int *error);
#endif
void LoadUsage(void);
struct DATABASE *MatchingDatabase(struct DATABASE *b,
                                  char *host,
                                  PORT remport,
                                  struct VALIDATE **validate,
                                  PORT *forceMyPort,
                                  BOOL *gateway,
                                  BOOL *javaScript);
void NeedSave(void);
void NeedSaveUsage(void);
PORT NextPortFrom(PORT n);
void DoSaveUsage(void *v);
void SaveHosts(void);
void SaveIPC(void);
void SaveUsage(void);
void DoSaveUsage(void *v);
void UnlinkIPCFILEandPidFile(void);
int UUAvlTreeCmpHost(const void *v1, const void *v2);
int UUAvlTreeCmpNeverProxyHosts(const void *v1, const void *v2);

BOOL ValidMenu(char *s);
BOOL ValidDocsCustomDir(char *s);
int VerifyHost(char *host, GROUPMASK gm, struct DATABASE **rb);
int VerifyHost2(char *host, GROUPMASK gm, struct DATABASE **rb, BOOL *isMyLoggedIn, int *isMe);

/* Prototypes for license.c */
void div48(U48 d1, unsigned char d2, U48 q, unsigned char *r);
unsigned char *Obscurity(int version);

/* Prototypes for location.c */
BOOL AnyLocations(void);
enum LOCATIONSTATUS FindLocationByHost(struct LOCATION *pLocation, struct sockaddr_storage *hostIp);
enum LOCATIONSTATUS FindLocationByNetwork(struct LOCATION *pLocation,
                                          struct sockaddr_storage *netIp);
enum LOCATIONSTATUS FindLocationByASCIIIP(struct LOCATION *pLocation, char *asciiIp);
enum LOCATIONSTATUS FindLocationByTranslate(struct TRANSLATE *t);
enum LOCATIONSTATUS FindLocationByTranslateNoCache(struct TRANSLATE *t);
void InitLocation(struct LOCATION *pLocation);
BOOL LoadLocation(char *file);

/* Prototypes for main.c */
void CheckArguments(int argc,
                    char **argv,
                    int *ifmode,
                    char *ifsubtype,
                    char **checkPort,
                    char **guardianMap,
                    BOOL started);
void ProxyGateway(struct TRANSLATE *t);
void ProxyPingPong(struct TRANSLATE *t);
int CheckForIPReject(struct TRANSLATE *t);
int StartTranslate(struct TRANSLATE *t);

/* Prototypes for misc.c */
#define SCAN_DIRECTORY_FILE 1
#define SCAN_DIRECTORY_DIR 2
#define SCAN_DIRECTORY_DESC 4
void ApplyProxyHostnameEdits(char *proxyHostname);
char *ASCIIDate(time_t *time, char *buffer);
char *ASCIIKMB(unsigned int x, int decimals, char *buffer);
char *ASCIIDHM(int seconds, char *buffer);
char *ASCIITrueOrFalse(BOOL state);
BOOL CheckIfDevice(char *filename);
BOOL CheckIfFile(char *fileName);
void CheckIsPdfUrl(struct TRANSLATE *t);
void CheckProxyByHostnameWildcardDNS(void);
void CheckReopenAccessFile(struct LOGSPU *logSPU, BOOL holdingMAccessFileMutex);
void CloseMsgFile(void);
BOOL CompareNameParts(char *n1, BOOL utf1, char *n2, BOOL utf2);
size_t CurlMemoryBufferCallback(void *contents, size_t size, size_t nmemb, void *userp);
struct sockaddr_storage *DatabaseOrSessionIpInterface(struct TRANSLATE *t, struct DATABASE *b);
void DirName(char *s);
char *NextField(char *buffer);
BOOL DecodeURLReference(char *url);
unsigned char *Decode64Binary(unsigned char *dst, size_t *dstLen, char *src);
void EncodeAngleBrackets(char *s, int max);
void EncodeFieldsToBuffer(char *buffer,
                          char *bufferEnd,
                          char *template,
                          struct TRANSLATE *t,
                          struct USERINFO *uip);
char *EncryptVar(char *f, struct DATABASE *b, char c);
char *ErrorMessage(int errorCode);
BOOL FileExists(char *file);
BOOL FileReadLine(int f, char *buffer, int max, struct FILEREADLINEBUFFER *frb);
char *FindField(const char *field, char *buffer);
char *FindFieldN(const char *field, char *buffer, int indexVal);
void FindReplace(char *s, size_t maxLen, char *find, char *replace, int flags);
void FindFormFields(char *query, char *post, struct FORMFIELD *ffs);
void FindFormFieldsFreeMultiValues(struct FORMFIELD *ffs);
void FindMIMEType(char *filename, char *mimeType, size_t mimeTypeLen, char *defaultMimeType);
void HeaderToBody(struct TRANSLATE *t);
char *ParseRegularExpression(const char *pattern,
                             pcre **findPattern,
                             pcre_extra **findPatternExtra,
                             unsigned int *optionsMask,
                             char **findReturn,
                             char **replaceReturn);
char *ParseXMLRegularExpression(const char *pattern, xmlRegexpPtr *findPattern);
void ParseHost(char *host, char **colon, char **endOfHostPort, BOOL allowAsterisk);
struct PROTOCOLDETAIL *ParseProtocol(char *url, char **endOfProtocol);
BOOL ParseProtocolHostPort(char *url,
                           char *defaultProtocol,
                           struct PROTOCOLDETAIL **protocol,
                           char **host,
                           char **endOfHost,
                           char **colon,
                           PORT *port,
                           BOOL *isDefaultPort,
                           char **endOfHostPort);
void printSelectBoxOptions(struct UUSOCKET *s, const char *selected, const int argcount, ...);
char *RemoveLogTag(char *url);
BOOL IsValidLogTag(char *logTag);
struct PDFURL *FindPdfUrl(struct TRANSLATE *t, BOOL add);
void FlushSocket(SOCKET s);
void GenericRedirect(struct TRANSLATE *t,
                     char *host,
                     PORT port,
                     char *url,
                     char useSsl,
                     int redirectCode);
void SimpleRedirect(struct TRANSLATE *t, char *url, int redirectCode);
void GetMyBinaryAndMyPath(char *argv0);
void MyNameRedirect(struct TRANSLATE *t, char *url, int redirectCode);
void HTMLHeader(struct TRANSLATE *t, enum HTMLHEADERCACHE hhc);
void CSRFHeader(struct TRANSLATE *t);
void HTMLErrorPageHeader(struct TRANSLATE *t, char *title);
void HTMLErrorPageFooter(struct TRANSLATE *t);
void HTMLErrorPage(struct TRANSLATE *t, char *title, char *error);
void InitializeFileReadLineBuffer(struct FILEREADLINEBUFFER *frb);
BOOL InitializeStackedIncludeFileNode(char *file,
                                      char *pseudoFile,
                                      char *pseudoFileName,
                                      struct FILEINCLUSION *fileNodeTopOfStack,
                                      struct FILEINCLUSION *fileNodeNew);
BOOL PushIncludeFileStack(struct USERINFO *uip,
                          struct FILEREADLINEBUFFER *frb,
                          char *file,
                          char *pseudoFile,
                          struct FILEINCLUSION *fileNodeTopOfStack,
                          struct FILEINCLUSION *fileNodeNewTopOfStack,
                          int *f);
void PopIncludeFileStack(struct USERINFO *uip,
                         struct FILEINCLUSION *fileNodeTopOfStack,
                         struct FILEINCLUSION *fileNodePreviousTopOfStack);
char *IncludeFileDisplayName(struct FILEINCLUSION *fileNode, char *buffer, size_t sizeofBuffer);
int InvalidSocket(SOCKET s, int operation);
enum IPACCESS URLIpAccess(struct TRANSLATE *t,
                          char *url,
                          int *sessionLife,
                          GROUPMASK gmAuto,
                          BOOL *anyExcluded,
                          char **autoUser);
BOOL IsRejectedIP(struct TRANSLATE *t);
BOOL IsIERedirectBrainDead(char *url);
void HTTPCode(struct TRANSLATE *t, int code, BOOL body);
void HTTPContentType(struct TRANSLATE *t, char *ct, BOOL endHeaders);
char *LinkField(char *buffer);
void LinkFields(char *buffer);
void safe_portable_checked_vsnprintf(char *buffer,
                                     const size_t size,
                                     const char *format,
                                     va_list ap);
void safe_portable_checked_snprintf(char *buffer, const size_t size, const char *format, ...);
void safe_portable_vsnprintf(char *buffer, const size_t size, const char *format, va_list ap);
void safe_portable_snprintf(char *buffer, const size_t size, const char *format, ...);
int print_hex(char *buffer, size_t size, unsigned char *value, size_t valueLen);
void LogVA(const char *format, va_list *ap);
int Log(const char *format, ...);
void LogVariables(struct VARIABLES *v, char *type);
/* void dprintf(const char *format, ...); */
void LogBinaryPacket(char *prefix, unsigned char *c, int len);
void LogRequest(struct TRANSLATE *t,
                struct LOGSPU *logSPU,
                char *url,
                char *accessType,
                struct DATABASE *b);
void LogSPUs(struct TRANSLATE *t, char *url, char *accessType, struct DATABASE *b);
char *LogSPUActiveFilename(struct LOGSPU *logSPU, BOOL haveMutex);
BOOL NameMatch(char *n1, char *n2, BOOL partialNameMatch);
void NoCacheHeaders(struct UUSOCKET *s);
void MaxAgeCacheHeader(struct UUSOCKET *s);
void CheckUserAgent(struct TRANSLATE *t);
BOOL NumericHost(const char *host);
BOOL NumericHostIp4(const char *host);
BOOL NumericHostIp6(const char *host);
int OpenMsgFile(void);
time_t ParseRelativeDate(char *s);
int ParseRequest(struct TRANSLATE *t);
void reportFileLineCode(char *file, int line, void *code);
void panic(const char *file, int line, int saveErrno);
int panic0(const char *file, int line, int saveErrno, const char *txt);
char *panicstr(const char *file, int line, int saveErrno, const char *txt);
extern volatile BOOL panicing;

char *PoundFix(char *url, size_t max, BOOL forceQuote);
BOOL ProcessAlive(int pid);
int ProcessKill(int pid);
char *ProxyHostname(char *hostname,
                    PORT remport,
                    PORT forceMyPort,
                    char useSsl,
                    char ftpgw,
                    BOOL optionNoHttpsHyphens);
char RandChar(void);
int RawTransfer(struct TRANSLATE *t,
                struct UUSOCKET *dst,
                struct UUSOCKET *src,
                intmax_t max,
                BOOL addCrLf);
#ifdef USEPOLL
void RawFlagSend(struct TRANSLATE *t,
                 int maxSend,
                 SOCKET *maxSock,
                 int *timeout,
                 struct pollfd *nextPfds,
                 unsigned int *nextPfdc);
void RawFlagRecv(struct TRANSLATE *t,
                 int maxRecv,
                 SOCKET *maxSock,
                 int *timeout,
                 struct pollfd *nextPfds,
                 unsigned int *nextPfdc);
#else
void RawFlagSend(struct TRANSLATE *t,
                 int maxSend,
                 SOCKET *maxSock,
                 int *timeout,
                 fd_set *nextRfds,
                 fd_set *nextWfds);
void RawFlagRecv(struct TRANSLATE *t,
                 int maxRecv,
                 SOCKET *maxSock,
                 int *timeout,
                 fd_set *nextRfds,
                 fd_set *nextWfds);
#endif
int ReadLine(struct UUSOCKET *s, char *buffer, int max);
char *UUErrorMessage();
BOOL RefreshStartingPointUrl(struct TRANSLATE *t, char *url);
void CheckRequireAuthenticate(struct TRANSLATE *t);
void ResetGuardian(BOOL real);
void SeizeMe(void);
void CrashMe(void);
void ScanDirectory(char *base,
                   int searchFlags,
                   BOOL (*callback)(char *base, char *file, int flags, void *context),
                   void *context);
int SendEditedFile(struct TRANSLATE *t,
                   struct USERINFO *uip,
                   char *file,
                   BOOL reportIfMissing,
                   char *url,
                   BOOL quoteURL,
                   ...);
int SendFile(struct TRANSLATE *t, char *file, int flags);
int SendFileMatches(struct TRANSLATE *t, char *file, int flags, char *matches, time_t earliest);
int SendFileTail(struct TRANSLATE *t, char *file, int flags, int tail);
void SendIfNonDefaultPort(struct UUSOCKET *s, PORT port, char useSsl);
int SendHTMLEncoded(struct UUSOCKET *s, const char *str);
int SendQuotedString(struct UUSOCKET *s, char *string);
int SendField(struct UUSOCKET *s,
              char *name,
              char *value,
              char *label,
              char *type,
              int size,
              int maxlength,
              int flags);
int SendHiddenField(struct UUSOCKET *s, char *name, char *value);
int SendTextField(struct UUSOCKET *s,
                  char *name,
                  char *value,
                  char *label,
                  int size,
                  int maxlength,
                  int flags);
int SendPasswordField(struct UUSOCKET *s,
                      char *name,
                      char *value,
                      char *label,
                      int size,
                      int maxlength,
                      int flags);
void SendURL(struct UUSOCKET *s, char *host, PORT port, char *url, char useSsl, char htmlEncoded);
char *URLReference(char *url, char *logTag);
void SendURLReference(struct UUSOCKET *s, char *url, char *logTag);
void SendEscapedURL(struct UUSOCKET *s, char *url);
void SendQuotedURL(struct TRANSLATE *t, char *url);
void SetProxyHostname(struct HOST *h);
char *SHA512String(char *input, char *salt);
char *SkipAt(char *s);
char *SkipFTPHTTPslashesAt(char *s, char *useSsl, char *ftpgw);
char *SkipHTTPslashesAt(char *s, char *useSsl);
void SocketBlocking(SOCKET s);
void SocketNonBlocking(SOCKET s);
void SocketSetBufferSize(SOCKET s);
void SocketDisableNagle(SOCKET s);
void SubSleep(int sec, int usec);
void StartRaw(struct TRANSLATE *t, intmax_t len, BOOL defer);
char *PeriodsToHyphens(char *str);
void StripScripts(char *url);
char *StrIStr(const char *main, const char *sub);
char *TrimHost(char *host, const char *charList, char *result);
void StrCpyOverlap(char *dst, char *src);
BOOL TailSeek(int file, int lines);
BOOL ValidateCheckDigit(char *digits);
BOOL WildCompare(const char *s, const char *w);
BOOL WildCompareCaseSensitive(const char *s, const char *w);
BOOL WildCompareArray(const char **a, const char *w, BOOL caseSensitive);
int WriteLine(struct UUSOCKET *s, char *format, ...);
int SendUrlEncoded(struct UUSOCKET *s, char *f);
void SendUrlEncodedEncrypted(struct UUSOCKET *s, char *f, struct DATABASE *b, char c);
void SendObfuscatedEncrypted(struct UUSOCKET *s, char *f, struct DATABASE *b, char c);
#ifdef USEPOLL
void PollSort(struct pollfd *pfds, unsigned int pfdc);
short PollRevents(struct pollfd *pfds, unsigned int pfdc, SOCKET s);
#endif
void DoStopSockets(void *v);
void DoHandleRaw(void *v);
void ParseCommand(char *cmd, size_t *cmdLen, char **arg);
BOOL MatchCommand(char *cmd, size_t cmdLen, char *match, size_t minMatch);
void NullFD(int fd);
int UUFILEFD(FILE *file);
int UUopenSimpleFD(const char *path, int flags, int mode);
int UUopenCreateFD(const char *path, int flags, int mode);
int UUopenExistantFD(const char *path, int flags);
int UUreadFD(int f, void *buf, size_t len);
int UUwriteFD(int f, void *buf, size_t len);
int UUfputsFD(char *s, int f);
int UUfputiFD(int i, int f);
int UUfputdFD(double i, int f);
int UUfputcFD(char c, int f);
void *UUmalloc(size_t size);
void *UUcalloc(size_t nmemb, size_t size);
void *UUrealloc(void *ptr, size_t size);
int RenameWithReplace(const char *oldName, const char *newName);
/*
int UUrecv(int s, void *buf, size_t len, int flags);
int UUsend(int s, void *buf, size_t len, int flags);
int UUsocket(int domain, int type, int protocol);
int UUclosesocket(SOCKET s);
*/
#define FreeThenNull(x) \
    {                   \
        if (x) {        \
            free(x);    \
            x = NULL;   \
        }               \
    }
#define xmlFreeThenNull(x) \
    {                      \
        if (x) {           \
            xmlFree(x);    \
            x = NULL;      \
        }                  \
    }

void WriteTrace(int f, int sig, void *faultAddr, char *message);

int UUcloseFD(int s);
time_t ParseHttpDate(char *s, time_t dt);
time_t ParseLocalDate(char *s, int offset);
char *FormatHttpDate(char *s, size_t len, time_t dt);
int UUmkdir(char *filename);

#ifdef UUPROFILETIME
time_t UUtimex(time_t *tloc, char *file, int line);
#define UUtime(x) UUtimex(x, __UUFILE__, __LINE__)
#else
time_t UUtime(time_t *tloc);
#endif

struct tm *UUgmtime_r(const time_t *clock, struct tm *result);
struct tm *UUlocaltime_r(const time_t *clock, struct tm *result);
char *UUctime_r(const time_t *clock, char *buf, int buflen);
UUTHREADID UUThreadId(void);

int GetChargeIndexFromName(char *name);
void ChargeAlive(int i, time_t *now);
void ChargeStopped(int i, time_t *now);
int CheckCharge();
DWORD GetTickDiff(DWORD endTick, DWORD startTick);
DWORD ChargeLatency(int i, DWORD endTick);
BOOL ChargeHasSeized(int i, DWORD endTick);

/* Prototypes for restart.c */
void EZproxyControl(int option, char *cmd, char *arg);

/* Prototypes for session.c */
int UUAvlTreeCmpSession(const void *v1, const void *v2);
void CheckGuardianKillSession(void);
void EnforceUserLimit(char *user, int userLimit);
/*struct SESSION *FindSessionByAuthHost(struct in_addr sin_addr); */
void AttachAdminKey(struct SESSION *e);
void AttachConnectKey(struct SESSION *e);
struct SESSION *FindSessionByAdminKey(const char *adminKey);
struct SESSION *FindSessionByConnectKey(const char *connectKey);
struct SESSION *FindSessionByGenericUser(char *user);
struct SESSION *FindSessionByCpipSid(char *sid);
struct SESSION *FindSessionByUserObjectTicket(char *userObjectTicket);
struct SESSION *FindNextSessionByIP(struct SESSION *start, struct sockaddr_storage *sin_addr);
BOOL FindSessionByCookie(struct TRANSLATE *t);
BOOL FindSessionByAnonymousUrl(struct TRANSLATE *t, char *url, struct DATABASE *b);
void SetLogUserBrief(struct SESSION *e);
BOOL StoreLogUser(struct SESSION *e, char *logUserFull);
struct SESSION *FindSessionByKey(char *key);
struct SESSION *FindSessionByConnectKeyInSameCountryWithinConnectWindow(struct TRANSLATE *t,
                                                                        char *key);
struct SESSION *StartSession(char *logUserFull, time_t relogin, char *clusterSession);
struct SESSION *ClusterStartSession(char *logUser,
                                    time_t relogin,
                                    char *clusterSession,
                                    BOOL fromCluster);
void StopSession(struct SESSION *e, char *reason);
void ClusterStopSession(struct SESSION *e, char *reason, BOOL fromCluster);
void ExpireSessions(void);
void ReapSessions(void);
time_t IPLastActive(struct sockaddr_storage *sin_addr,
                    BOOL mostRecent,
                    struct SESSION **which,
                    GROUPMASK gm);
int UpdateAccessByIP(struct sockaddr_storage *sin_addr);
void ClearUsageLimitExceeded(struct USAGELIMITEXCEEDED *ule,
                             struct USAGELIMIT *ul,
                             struct USAGE *u,
                             char *why);
struct INTRUDERIP *FindIntruderIP(struct TRANSLATE *t,
                                  struct sockaddr_storage *ip,
                                  struct sockaddr_storage *forwardedIp,
                                  BOOL add);
void FindUsage(struct USAGE **head,
               char *user,
               time_t dateHour,
               BOOL haveMutex,
               struct USAGE **uReturn,
               struct USAGEDATEHOUR **udReturn);
struct USAGELIMIT *FindUsageLimit(char *name, BOOL add);
struct USAGELIMITEXCEEDED *FindUsageLimitExceeded(char *user, BOOL create, BOOL haveMutex);
void LoadDefaultCookies(struct SESSION *e);
BOOL UsageLimitExceeded(struct TRANSLATE *t,
                        struct USAGELIMITEXCEEDED *ule,
                        enum USAGELIMITEXCEEDEDTYPE ulet);
void PruneIntruderIP(void);
void PruneUsageDateHour(struct USAGE *u, time_t oldest, unsigned int oldestInterval);
void PruneUsage(struct USAGE **head, unsigned int oldestInterval);
void PruneUsageLimitExceeded(void);
void ReleaseUsageLimitExceeded(struct USAGELIMITEXCEEDED *ule, BOOL haveMutex);
int IntruderIPLevel(struct INTRUDERIP *bg, time_t now);
time_t CurrentUsageDateHour(void);
void NoteIntruder(struct TRANSLATE *t, char *user);
enum INTRUDERSTATUS IsIntruder(struct TRANSLATE *t, char *user, char *retIpBuffer);
void UpdateUsername(struct TRANSLATE *t, struct SESSION *e, char *username, char *facility);
BOOL SessionExpirationReached(struct SESSION *e, time_t now);

/* Prototypes for shib.c */
void AdminShibbolethShire(struct TRANSLATE *t, char *query, char *post);
void ShibbolethSendURLwithWAYF(struct UUSOCKET *s,
                               char *url,
                               char *optionalWayf,
                               char *entityID,
                               char *logTag);

/* Protoypes for thread.c */
int UUBeginThread(void (*threadFunc)(void *va), void *threadArg, char *threadFuncName);

/* Prototypes for translate.c */
BOOL BodylessStatus(struct TRANSLATE *t, int code);
BOOL CompareJustHost(char *n1, char *n2, BOOL hyphenLikePeriod);
void DoTranslate(void *v);
BOOL DenyIfGoogleWebAccelerator(struct TRANSLATE *t);
int DetectHTML(char *s);
void InitTranslate(struct TRANSLATE *t, struct HOST *h);
void EditURL(struct TRANSLATE *t, char *url);
int HAReroute(struct TRANSLATE *t, BOOL fromAdminForm, char *url, size_t urlLen);
void ReverseEditURL(struct TRANSLATE *t, char *url, BOOL *changed);
void InitAllPatterns(void);
BOOL ForwardRequest(struct TRANSLATE *t);
BOOL ForwardRequestFTP(struct TRANSLATE *t);
void ProcessHTML(struct TRANSLATE *t);
void ProcessNonHTML(struct TRANSLATE *t);
int ProcessResponse(struct TRANSLATE *t);
void EndTranslate(struct TRANSLATE *t, BOOL logRequest);
void ReleaseTranslate(struct TRANSLATE *t);
void ResetTranslate(struct TRANSLATE *t);
BOOL ReloginRequired(struct SESSION *e);
int ValidateRequest(struct TRANSLATE *t);
int ValidateSession(struct TRANSLATE *t);
BOOL ConnectToServer(struct TRANSLATE *t,
                     BOOL skipProxy,
                     char *hostname,
                     PORT port,
                     struct sockaddr_storage *pInterface,
                     char useSsl,
                     struct DATABASE *b);
BOOL ProcessHttpHeadersList(struct TRANSLATE *t,
                            struct HTTPHEADER *hh,
                            struct UUSOCKET *w,
                            enum HTTPHEADER_DIRECTION direction,
                            char *field,
                            BOOL *lastOK);

/* Prototypes for functions in win32.c or unix.c */
void DisableDebugger(void);
int Guardian(char *guardianMap, int argc, char **argv);
void MapGuardian(BOOL allowFailure);

/* The UUXXXMutexBase and UUxxxMutexOS routines are not meant
   for direct use, but should be invoked through the UUxxxMutex macros
*/

int UUInitMutexBase(UUMUTEX *uumutex, const char *name);
int UUAcquireMutexBase(UUMUTEX *uumutex, const char *file, int line);
int UUTryAcquireMutexBase(UUMUTEX *uumutex, const char *file, int line);
int UUReleaseMutexBase(UUMUTEX *uumutex, const char *file, int line);

int UUInitMutexOS(UUMUTEX *uumutex);
int UUAcquireMutexOS(UUMUTEX *uumutex, const char *file, int line);
int UUTryAcquireMutexOS(UUMUTEX *uumutex);
int UUReleaseMutexOS(UUMUTEX *uumutex);

#define UUInitMutex(uumutex, name) UUInitMutexBase(uumutex, name)
#define UUAcquireMutex(uumutex) UUAcquireMutexBase(uumutex, __UUFILE__, __LINE__)
#define UUTryAcquireMutex(uumutex) UUTryAcquireMutexBase(uumutex, __UUFILE__, __LINE__)
#define UUReleaseMutex(uumutex) UUReleaseMutexBase(uumutex, __UUFILE__, __LINE__)
void UUDeadlockMutex(UUMUTEX *uumutex, char *file, int line);

void EnablePrivilege(void);
void SuspendPrivilege(void);
void AbdicatePrivilege(void);

long int interrupted_neg_int(long int rc);
FILE *interrupted_null_file(FILE *f);
void ackCancel();

/* Prototypes for z3950.c */
void DoZ3950Proxy(struct TRANSLATE *t);

/* Shared between unix.c and win32.c */
void SetEUidGid(void);
void ResetEUidGid(void);
void SetUidGid(void);

#ifdef WIN32
void StartWinsock(void);
VOID AddToMessageLog(char *format, ...);
#endif

#include "uustring.h"

#ifdef UU_USE_DMALLOC
#include <dmalloc.h>
#endif

#endif  // __COMMON_H__
