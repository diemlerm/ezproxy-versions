#define __UUFILE__ "spur.c"

#include "common.h"
#include "environment.h"
#include "spur.h"
#include "sql.h"
#include <sys/stat.h>
#include <math.h>

// This connection should not be shared. It is separate to ensure its values
// for changes is restricted just to changes happening in here.
static sqlite3 *db = NULL;
int spurRetention = 30 * 60;
static UUMUTEX mSpur;

static const char *startupCommands[] = {
    "CREATE TABLE IF NOT EXISTS spur("
    "id INTEGER PRIMARY KEY,"
    "created_at INT NOT NULL DEFAULT (strftime('%s', 'now')),"
    "referer_url TEXT NOT NULL,"
    "target_url TEXT NOT NULL COLLATE NOCASE"
    ")",
    NULL};

static void SpurUpgradeDatabase() {
    // No updates currently needed. See security.c for sample structure.
}

static void SpurOpenDatabase(sqlite3 **db) {
    SQLOK(sqlite3_open_v2(
        SPURDB, db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX, NULL));
    SQLOK(sqlite3_busy_timeout(*db, 60000));
}

void SpurInit() {
    const char **sql;

    SpurOpenDatabase(&db);

    for (sql = startupCommands; *sql; sql++) {
        SQLEXEC(db, *sql, NULL, NULL);
    }

    SpurUpgradeDatabase();
}

void SpurInsert(struct TRANSLATE *t, const char *refererUrl, const char *targetUrl) {
    static sqlite3_stmt *insertStmt = NULL;
    int col = 1;
    const char *sql =
        "INSERT INTO spur(referer_url, target_url) VALUES (:referer_url, :target_url)";
    sqlite3_int64 id;
    char tag[32];

    if (targetUrl == NULL) {
        return;
    }

    // It will not be unusual to receive a request without a referring URL.
    // In that case, use tagId 0 instead of creating a value.
    if (refererUrl == NULL || *refererUrl == 0) {
        id = 0;
    } else {
        UUAcquireMutex(&mSpur);
        if (insertStmt == NULL) {
            SQLPrepare(db, sql, &insertStmt);
        }

        SQLOK(SQLBindTextByName(insertStmt, ":referer_url", refererUrl, SQLITE_STATIC));
        SQLOK(SQLBindTextByName(insertStmt, ":target_url", targetUrl, SQLITE_STATIC));

        SQLDONE(sqlite3_step(insertStmt));
        id = sqlite3_last_insert_rowid(db);
        sqlite3_reset(insertStmt);
        sqlite3_clear_bindings(insertStmt);

        UUReleaseMutex(&mSpur);
    }

    // It should already be null, but just in case
    FreeThenNull(t->logTag);
    sprintf(tag, "r%" PRIdMAX, id);
    t->logTag = UUStrDup(tag);
    t->logTagApproved = 1;
    t->logTagSpur = 1;
}

char *SpurGetRefererUrl(const char *tag) {
    const char *sql;
    static sqlite3_stmt *selectStmt = NULL;
    char *refererUrl = NULL;
    int result;

    if (tag != NULL && *tag == 'r' && strlen(tag) > 1 && strlen(tag) < 20 && IsAllDigits(tag + 1)) {
        sqlite3_int64 id = strtoull(tag + 1, NULL, 10);
        if (id != 0) {
            UUAcquireMutex(&mSpur);
            if (selectStmt == NULL) {
                sql = "SELECT referer_url FROM spur WHERE id = :id";
                SQLPrepare(db, sql, &selectStmt);
            }

            SQLOK(SQLBindInt64ByName(selectStmt, ":id", id));

            result = sqlite3_step(selectStmt);
            if (result == SQLITE_ROW) {
                refererUrl = UUStrDup(sqlite3_column_text(selectStmt, 0));
            } else if (result != SQLITE_DONE) {
                SQLPANIC(result);
            }
            sqlite3_reset(selectStmt);
            UUReleaseMutex(&mSpur);
        }
    }

    return refererUrl;
}

static void SpurPurge() {
    DWORD startTick, endTick;
    int idx = 0;
    // Delete within a window to help get rid of any data from times
    // when clock may have been very off in the future.

    // clang-format off
    const char *deleteCommands[] = {
        "DELETE"
        " FROM"
            " spur"
        " WHERE"
            " created_at NOT BETWEEN :earliest AND :latest",
        NULL
    };
    // clang-format on

    sqlite3_stmt *deleteStmt = NULL;
    int deleted;
    time_t now, earliest, latest;
    const char **sql;

    UUtime(&now);
    earliest = now - spurRetention;
    latest = now + spurRetention;

    UUAcquireMutex(&mSpur);
    for (sql = deleteCommands; *sql; sql++, idx++) {
        if (optionSQLStats) {
            startTick = GetTickCount();
        }

        ChargeAlive(GCSPUR, NULL);
        SQLPrepare(db, *sql, &deleteStmt);
        SQLOK(SQLBindInt64ByName(deleteStmt, ":earliest", earliest));
        SQLOK(SQLBindInt64ByName(deleteStmt, ":latest", latest));
        SQLDONE(sqlite3_step(deleteStmt));
        deleted = sqlite3_changes(db);
        sqlite3_reset(deleteStmt);
        sqlite3_clear_bindings(deleteStmt);

        if (optionSQLStats) {
            endTick = GetTickCount();
            Log("SQL SpurPurge %d deleted %d in %d", idx, deleted, GetTickDiff(endTick, startTick));
        }
    }
    UUReleaseMutex(&mSpur);
}

static void DoSpur(void *v) {
    struct SECURITY_NODE *n;
    DWORD currTick, lastTick;
    DWORD interval = 60 * 1000;
    time_t now;
    struct tm tm;
    time_t lastPurge = 0;

    lastTick = 0;

    for (;;) {
        ChargeAlive(GCSPUR, &now);
        if (fabs(difftime(now, lastPurge)) > 15 * 60) {
            lastPurge = now;
            SpurPurge();
        }
        SubSleep(1, 0);
    }
}

void SpurStart() {
    struct stat statBuf;

    if (!optionCaptureSpur) {
        return;
    }

    if (stat(SPURDIR, &statBuf) < 0 && errno == ENOENT) {
        SetEUidGid();
        if (UUmkdir(SPURDIR) != 0)
            PANIC;
        ResetEUidGid();
    }

    UUInitMutex(&mSpur, "mSpur");
    if (UUBeginThread(DoSpur, NULL, guardianChargeName[GCSPUR]))
        PANIC;
}
