#define __UUFILE__ "admingroups.c"

#include "common.h"
#include "uustring.h"

#include "admingroups.h"

void AdminGroups(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s;
    int i;
    struct DATABASE *d;
    struct GROUP *g;
    char group[16];

    s = &t->ssocket;

    AdminHeader(t, 0, "View Database Group Assignments");

    if (post && *post && strstr(post, "update=")) {
        for (g = groups; g; g = g->next) {
            // Do not allow administrative group memberships to be manipulated
            if (IsAnAdminGroup(g)) {
                continue;
            }
            sprintf(group, "g_%d=on", g->index);
            if (strstr(post, group))
                GroupMaskOr((t->session)->gm, g->gm);
            else
                GroupMaskMinus((t->session)->gm, g->gm);
        }
        NeedSave();
    }

    UUSendZ(s, "<form action='/groups' method='post'>\n");

    UUSendZ(s, "<p>\n");
    UUSendZ(s,
            "<input type='submit' name='update' value='Update your current session's group "
            "memberships' />\n");
    UUSendZ(s, "</p>\n");

    UUSendZ(s, "<table class='bordered-table th-row-nobold'>\n");

    UUSendZ(s, "<tr align='center'><td>&nbsp;</td>");
    for (g = groups; g; g = g->next) {
        if (IsAnAdminGroup(g)) {
            continue;
        }
        UUSendZ(s, "<th scope='col'>");
        SendHTMLEncoded(s, g->name);
        UUSendZ(s, "</th>");
    }
    UUSendZ(s, "</tr>\n");

    UUSendZ(s, "<tr align='center'><th scope='row'>Your group memberships</td>");

    for (g = groups; g; g = g->next) {
        if (IsAnAdminGroup(g)) {
            continue;
        }
        WriteLine(s, "<td><input type='checkbox' name='g_%d' value='on'%s /></td>", g->index,
                  GroupMaskOverlap((t->session)->gm, g->gm) ? " checked='checked'" : "");
    }

    UUSendZ(s, "</tr>\n");

    for (i = 0, d = databases; i < cDatabases; i++, d++) {
        UUSendZ(s, "<tr align='center'><th scope='row'>");
        SendHTMLEncoded(s, d->title);
        UUSendZ(s, "</th>");
        for (g = groups; g; g = g->next) {
            if (IsAnAdminGroup(g)) {
                continue;
            }
            WriteLine(s, "<td>%s</td>", (GroupMaskOverlap(d->gm, g->gm) ? "*" : "&nbsp;"));
        }
        UUSendZ(s, "</tr>\n");
    }
    UUSendZ(s, "</table>\n");

    UUSendZ(s, "</form>\n");

    UUSendZ(s,
            "<h2>Admin Privilege Groups</h2>\n<p>Users can be assigned into one or more of the "
            "following groups to give them access to specific administrative features.</p><ul>\n");
    for (g = groups; g; g = g->next) {
        if (IsAnAdminGroup(g)) {
            UUSendZ(s, "<li>");
            SendHTMLEncoded(s, g->name);
            UUSendZ(s, "</li>\n");
        }
    }
    UUSendZ(s, "</ul>\n\n");

    AdminFooter(t, 0);
}
