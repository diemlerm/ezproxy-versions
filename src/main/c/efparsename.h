#ifndef __EFPARSENAME_H__
#define __EFPARSENAME_H__

#include "common.h"

char *ExpressionVariableParseName(struct EXPRESSIONCTX *ec, char *varIndex);
char *ExpressionFuncParseName(struct EXPRESSIONCTX *ec);

#endif  // __EFPARSENAME_H__
