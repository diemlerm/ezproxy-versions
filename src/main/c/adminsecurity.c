#define __UUFILE__ "adminsecurity.c"

#include "environment.h"
#include "common.h"
#include "adminsecurity.h"
#include "intrusion.h"
#include "intrusiontxt.h"
#include "obscure.h"
#include "wskeylicense.h"
#include "security.h"
#include "sql.h"
#include "adminaudit.h"
#include <jansson.h>
#include <curl/curl.h>

static void AdminSecurityHeader(struct TRANSLATE *t, int options, char *title) {
    AdminBaseHeader(t, options, title, " | <a href='/security'>Security</a>");
}

void AdminSecurityRules(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;

    // clang-format off
    const char *sql =
        "SELECT"
            " rule.id,"
            " CASE"
                " WHEN active = 1 AND extraconstraintvalid = 1 THEN 'Active'"
                " WHEN active = 1 AND extraconstraintvalid = 0 THEN 'Invalid where'"
                " ELSE 'Historic' END AS \"Status\","
            " rulename AS \"Rule\","
            " criterion AS \"Criterion\","
            " KMGT(boundary) AS \"Limit\","
            " period / 60 AS \"Period\","
            " action AS \"Action\","
            " CASE"
                " WHEN duration = 0 AND action = 'log' THEN 'Logged'"
                " WHEN duration = 0 AND action = 'block' THEN 'Never'"
                " ELSE duration / 60"
            " END AS \"Expires\","
            " KMGT((SELECT COUNT(*) FROM tripped WHERE tripped.ruleid = rule.id)) AS \"Tripped\","
            " file AS \"File\","
            " CASE"
                " WHEN lineno = 0 THEN ''"
                " ELSE lineno"
            " END AS \"Line\","
            " extraconstraint AS \"Where\""
        " FROM"
            " rule"
        " ORDER BY"
            " active DESC,"
            " extraconstraintvalid,"
            " rulename,"
            " file,"
            " lineno";
    // clang-format on

    sqlite3 *db = SecurityGetSharedDatabase();
    int rows, cols;
    char **result;
    char *errMsg = NULL;
    char dateBuffer[MAXASCIIDATE];
    int i;
    int j;
    char trippedBuffer[MAXASCIIDHM];
    char evidenceBuffer[MAXASCIIDHM];

    AdminBaseHeader(t, AHSORTABLE, "Security Rules", " | <a href='/security'>Security</a>");
    SQLOK(sqlite3_get_table(db, sql, &result, &rows, &cols, &errMsg));

    if (rows == 0) {
        WriteLine(s, "<p>No rules are active</p>\n");
    } else {
        WriteLine(s, "<table id='security-rules' class='bordered-table sortable'>\n");

        const char *td = "th";
        const char *tda = " scope='col'";
        for (i = 0; i <= rows; i++) {
            WriteLine(s, "<tr>\n");
            for (j = 1; j < cols; j++) {
                BOOL isHeader = i == 0;
                BOOL isNumeric = j == 4 || j == 5 || j == 8 || j == 10;
                WriteLine(s, "<%s%s%s>", td, tda,
                          isNumeric
                              ? isHeader ? " class='sortnumeric'" : " style='text-align: right'"
                              : "");
                if (i > 0 && j == 8) {
                    WriteLine(s, "<a href='/security/tripped?ruleid=%s'>", result[i * cols]);
                }
                SendHTMLEncoded(s, result[i * cols + j]);
                if (i > 0 && j == 10) {
                    WriteLine(s, "</a>");
                }

                WriteLine(s, "</%s>", td);
            }
            WriteLine(s, "</tr>\n");
            td = "td";
            tda = "";
        }
        WriteLine(s, "</table>\n");
    }
    sqlite3_free_table(result);

    WriteLine(s, "<p>");

    WriteLine(s,
              "Next Purge: %s<br>\n"
              "Vacuum Day: %s<br>\n"
              "Resolved Retention: %s<br>\n"
              "Evidence Retention: %s</p>\n",
              ASCIIDate(&securityNextPurge, dateBuffer), securityVacuumDayName,
              ASCIIDHM(securityResolvedRetention, trippedBuffer),
              ASCIIDHM(securityEvidenceRetention, evidenceBuffer));

    AdminFooter(t, 0);
}

static BOOL AdminSecurityTrippedEvidence(struct TRANSLATE *t, char *trippedid) {
    struct UUSOCKET *s = &t->ssocket;

    // clang-format off
    const char *factSql =
        "SELECT"
            " facttable,"
            " rulename,"
            " user"
        " FROM"
            " tripped"
            " INNER JOIN rule ON tripped.ruleid = rule.id"
        " WHERE"
            " tripped.id = ?";
    // clang-format on

    sqlite3_stmt *factStmt = NULL;

    // clang-format off
    const char *loginSql =
        "SELECT"
            " DATETIME(fact.at, 'unixepoch', 'localtime') AS \"When\","
            "fact.user AS \"User\","
            "ip AS \"IP\","
            "TRIM(country || ' ' || region || ' ' || city) AS \"Location\","
            "status AS \"Status\""
        " FROM"
            " tripped"
            " INNER JOIN evidence ON evidence.trippedid = tripped.id"
            " INNER JOIN login fact ON evidence.factid = fact.id"
        " WHERE"
            " tripped.id = %s"
        " ORDER BY"
            " fact.at,"
            " fact.user";
    // clang-format on

    // clang-format off
    const char *requestSql =
        "SELECT"
            " DATETIME(fact.at, 'unixepoch', 'localtime') AS \"When\","
            "fact.user AS \"User\","
            "ip AS \"IP\","
            "TRIM(country || ' ' || region || ' ' || city) AS \"Location\","
            "transferred AS \"Size\","
            "origin || path AS \"URL\""
        " FROM"
            " tripped"
            " INNER JOIN evidence ON evidence.trippedid = tripped.id"
            " INNER JOIN request fact ON evidence.factid = fact.id"
        " WHERE"
            " tripped.id = %s"
        " ORDER BY"
            " fact.at,"
            " fact.user";
    // clang-format on

    char sql[2048];
    sqlite3 *db = SecurityGetSharedDatabase();
    int rows, cols;
    char **result;
    char *errMsg = NULL;
    int i;
    int j;
    char *facttable = NULL;
    char *rulename = NULL;
    char *user = NULL;

    if (trippedid == NULL || *trippedid == 0 || strlen(trippedid) > 18 || !IsAllDigits(trippedid)) {
        return FALSE;
    }

    AdminBaseHeader(t, AHSORTABLE, "Tripped Security Rule Evidence",
                    " | <a href='/security'>Security</a> | <a href='/security/tripped'>Tripped "
                    "Security Rules</a>");

    SQLPrepare(db, factSql, &factStmt);
    SQLOK(sqlite3_bind_text(factStmt, 1, trippedid, -1, SQLITE_STATIC));

    int res = sqlite3_step(factStmt);
    if (res != SQLITE_ROW) {
        SQLDONE(res);
        sqlite3_finalize(factStmt);
        goto NoDetail;
    }
    if (!(facttable = strdup(sqlite3_column_text(factStmt, 0))))
        PANIC;
    if (!(rulename = strdup(sqlite3_column_text(factStmt, 1))))
        PANIC;
    if (!(user = strdup(sqlite3_column_text(factStmt, 2))))
        PANIC;
    sqlite3_finalize(factStmt);

    BOOL isLogin = stricmp(facttable, "login") == 0;
    // trippedid has been validated to 18 or fewer digits
    if (isLogin) {
        snprintf(sql, sizeof(sql), loginSql, trippedid);
    } else {
        snprintf(sql, sizeof(sql), requestSql, trippedid);
    }
    SQLOK(sqlite3_get_table(db, sql, &result, &rows, &cols, &errMsg));

    if (rows == 0) {
        goto NoDetail;
    } else {
        WriteLine(s, "<h2>");
        SendHTMLEncoded(s, rulename);
        WriteLine(s, " for ");
        SendHTMLEncoded(s, user);
        WriteLine(s, "</h2>\n");

        WriteLine(s, "<table id='security-tripped-detail' class='bordered-table sortable'>\n");

        const char *td = "th";
        const char *tda = " scope='col'";
        for (i = 0; i <= rows; i++) {
            WriteLine(s, "<tr>\n");
            BOOL isHeader = i == 0;
            for (j = 0; j < cols; j++) {
                BOOL isNumeric = isLogin == 0 && j == 4;
                WriteLine(s, "<%s%s%s>", td, tda,
                          isNumeric
                              ? isHeader ? " class='sortnumeric'" : " style='text-align: right'"
                              : "");
                SendHTMLEncoded(s, result[i * cols + j]);
                WriteLine(s, "</%s>", td);
            }
            WriteLine(s, "</tr>\n");
            td = "td";
            tda = "";
        }
        WriteLine(s, "</table>\n");
    }

    goto Finished;

NoDetail:
    WriteLine(s, "<p>No details are available.</p>\n");

Finished:
    sqlite3_free_table(result);
    FreeThenNull(facttable);
    FreeThenNull(rulename);
    FreeThenNull(user);
    AdminFooter(t, 0);
    return TRUE;
}

BOOL AdminSecurityTrippedExpire(struct TRANSLATE *t, char *post) {
    const int ASEXP = 0;
    struct FORMFIELD ffs[] = {
        {"exp", NULL, 0, 0, 18, FORMFIELDALLOWMULTIVALUES},
        {NULL, NULL, 0, 0}
    };
    struct FORMFIELDMULTIVALUE *ffmv;
    char *value;
    time_t now;
    sqlite3 *db = SecurityGetSharedDatabase();
    sqlite3_stmt *stmt;
    BOOL any = FALSE;

    // clang-format off
    const char *sql =
        "UPDATE"
            " tripped"
        " SET"
            " expires = :now"
        " WHERE"
            " id = :id"
            " AND expires > :now";
    // clang-format on

    FindFormFields(NULL, post, ffs);

    SQLPrepare(db, sql, &stmt);
    UUtime(&now);
    SQLBindInt64ByName(stmt, ":now", now);

    for (value = ffs[ASEXP].value, ffmv = ffs[ASEXP].nextMultiValue; value;) {
        if (*value && IsAllDigits(value) != 0) {
            SQLBindTextByName(stmt, ":id", value, SQLITE_STATIC);
            SQLDONE(sqlite3_step(stmt));
            SQLOK(sqlite3_reset(stmt));
            any = TRUE;
        }

        if (ffmv) {
            value = ffmv->value;
            ffmv = ffmv->nextMultiValue;
        } else {
            value = NULL;
        }
    }

    if (any) {
        SecurityEnqueueUpdateBlocked();
    }

    if (strcmp(t->method, "POST") == 0) {
        LocalRedirect(t, t->urlCopy, 303, 0, NULL);
        return TRUE;
    } else {
        return FALSE;
    }
}

static void AdminSecurityExemptionsRedirect(struct TRANSLATE *t, int status) {
    LocalRedirect(t, "/security/exemptions", status, FALSE, NULL);
}

void AdminSecurityAuditEvent(struct TRANSLATE *t,
                             const char *activity,
                             const char *id,
                             time_t expiresTime) {
    struct SESSION *e = t ? t->session : NULL;
    char *other = NULL;
    struct tm *tm, rtm;
    char sExpiresTime[32] = {0};

    if (expiresTime) {
        if (expiresTime != SQLNEVER) {
            tm = UUlocaltime_r(&expiresTime, &rtm);
            strftime(sExpiresTime, sizeof(sExpiresTime), " expires %Y-%m-%d", tm);
        } else {
            strcpy(sExpiresTime, " expires never");
        }
    }

    size_t otherLen = strlen(activity) + strlen(id) + strlen(sExpiresTime) + 5;

    if (!(other = malloc(otherLen)))
        PANIC;
    snprintf(other, otherLen, "%s %s%s", activity, id, sExpiresTime);

    Trim(other, TRIM_CTRL);
    AuditEvent(t, AUDITSECURITYADMIN, e ? e->logUserFull : NULL, e, other);
    FreeThenNull(other);
}

static int AdminSecurityExemptionsAdd(struct TRANSLATE *t,
                                      char *user,
                                      time_t expiresTime,
                                      char *comment) {
    sqlite3 *db = SecurityGetSharedDatabase();

    // clang-format off
    const char *sql =
        "INSERT INTO"
            " exemption(user, expires, comment)"
        " VALUES(:user, :expires, :comment)";
    // clang-format on

    sqlite3_stmt *stmt = NULL;

    SQLPrepare(db, sql, &stmt);
    SQLOK(SQLBindTextByName(stmt, ":user", user, SQLITE_STATIC));
    SQLOK(SQLBindInt64ByName(stmt, ":expires", expiresTime));
    SQLOK(SQLBindTextByName(stmt, ":comment", comment, SQLITE_STATIC));
    int result = sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    if (result != SQLITE_CONSTRAINT) {
        SQLOKORDONE(result);
        AdminSecurityAuditEvent(t, "add", user, expiresTime);
    }
    return result;
}

static void AdminSecurityExemptionsDelete(struct TRANSLATE *t, char *id) {
    sqlite3 *db = SecurityGetSharedDatabase();
    const char *sql = "DELETE FROM exemption WHERE user = :user";
    sqlite3_stmt *stmt = NULL;
    int changes;

    SQLPrepare(db, sql, &stmt);
    SQLBeginTransaction(db);
    SQLOK(SQLBindTextByName(stmt, ":user", id, SQLITE_STATIC));
    SQLOKORDONE(sqlite3_step(stmt));
    changes = sqlite3_changes(db);
    SQLCommitTransaction(db);
    sqlite3_finalize(stmt);

    if (changes) {
        AdminSecurityAuditEvent(t, "delete", id, 0);
    }
}

static int AdminSecurityExemptionsUpdate(struct TRANSLATE *t,
                                         char *id,
                                         char *user,
                                         time_t expiresTime,
                                         char *comment) {
    sqlite3 *db = SecurityGetSharedDatabase();

    // clang-format off
    const char *sql =
        "UPDATE"
            " exemption"
        " SET"
            " user = :user,"
            " expires = :expires,"
            " comment = :comment,"
            " expiresaudited = 0"
        " WHERE"
            " user = :id";
    // clang-format on

    sqlite3_stmt *stmt = NULL;
    int changes;
    int result;

    SQLPrepare(db, sql, &stmt);
    SQLBeginTransaction(db);
    SQLOK(SQLBindTextByName(stmt, ":id", id, SQLITE_STATIC));
    SQLOK(SQLBindTextByName(stmt, ":user", user, SQLITE_STATIC));
    SQLOK(SQLBindInt64ByName(stmt, ":expires", expiresTime));
    SQLOK(SQLBindTextByName(stmt, ":comment", comment, SQLITE_STATIC));
    result = sqlite3_step(stmt);
    changes = sqlite3_changes(db);
    if (result != SQLITE_CONSTRAINT) {
        SQLOKORDONE(result);
    }
    SQLCommitTransaction(db);
    sqlite3_finalize(stmt);

    if (changes) {
        AdminSecurityAuditEvent(t, "update", id, expiresTime);
    }

    return result;
}

static void AdminSecurityExemptionsGetValues(char *id, char **expires, char **comment) {
    sqlite3 *db = SecurityGetSharedDatabase();

    // clang-format off
    const char *sql =
        "SELECT"
            " CASE"
                " WHEN expires = NEVER() THEN ''"
                " ELSE SUBSTR(DATETIME(expires, 'unixepoch', 'localtime'), 1, 10)"
            " END,"
            " comment"
        " FROM"
            " exemption"
        " WHERE"
            " user = :user";
    // clang-format on

    sqlite3_stmt *stmt = NULL;

    *expires = NULL;
    *comment = NULL;

    if (id && *id) {
        SQLPrepare(db, sql, &stmt);
        SQLOK(SQLBindTextByName(stmt, ":user", id, SQLITE_STATIC));
        int result = sqlite3_step(stmt);
        if (result == SQLITE_ROW) {
            *expires = UUStrDup(sqlite3_column_text(stmt, 0));
            *comment = UUStrDup(sqlite3_column_text(stmt, 1));
        }
        sqlite3_finalize(stmt);
    }
}

static BOOL AdminSecurityExemptionsEdit(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;

    const int ASACTION = 0;
    const int ASID = 1;
    const int ASUSER = 2;
    const int ASEXPIRES = 3;
    const int ASCOMMENT = 4;

    struct FORMFIELD ffs[] = {
        {"action", NULL, 0, 0, 10},
        {"id", NULL, 0, 0, 128},
        {"user", NULL, 0, 0, 128},
        {"expires", NULL, 0, 0, 10},
        {"comment", NULL, 0, 0, 255},
        {NULL, NULL, 0, 0}
    };
    char *action;
    char *id;
    char *user;
    BOOL add = FALSE, edit = FALSE, update = FALSE, delete = FALSE;
    char *expires = NULL;
    char *userError = NULL;
    char *expiresError = NULL;
    char *comment = NULL;
    time_t expiresTime;
    int result;

    FindFormFields(query, post, ffs);
    action = ffs[ASACTION].value;
    id = ffs[ASID].value;
    user = ffs[ASUSER].value;

    if (action == NULL) {
        return FALSE;
    }

    if (stricmp(action, "cancel") == 0) {
        AdminSecurityExemptionsRedirect(t, 303);
        return TRUE;
    }

    add = stricmp(action, "add") == 0;
    edit = stricmp(action, "edit") == 0;
    Trim(id, TRIM_LEAD | TRIM_TRAIL);
    AllLower(id);
    if (id && *id) {
        if (*id) {
            update = stricmp(action, "update") == 0;
            delete = stricmp(action, "delete") == 0;
        }
    } else {
        id = NULL;
    }

    if (!(add || edit || update || delete)) {
        return FALSE;
    }

    if (delete) {
        AdminSecurityExemptionsDelete(t, id);
        AdminSecurityExemptionsRedirect(t, 303);
        return TRUE;
    }

    Trim(user, TRIM_LEAD | TRIM_TRAIL);
    if (edit || user == NULL || *user == 0) {
        user = id;
    }

    AllLower(user);

    if (edit) {
        AdminSecurityExemptionsGetValues(id, &expires, &comment);
    } else {
        expires = UUStrDup(ffs[ASEXPIRES].value);
        comment = UUStrDup(ffs[ASCOMMENT].value);
    }

    if (comment && stricmp(comment, "Never") == 0) {
        *comment = 0;
    }

    Trim(expires, TRIM_LEAD | TRIM_TRAIL);
    Trim(comment, TRIM_LEAD | TRIM_TRAIL);

    if (add || update) {
        if (expires && *expires) {
            expiresTime = ParseDate(expires);
            if (expiresTime == 0) {
                expiresError = "Invalid date (use YYYY-MM-DD or leave blank for Never)";
            }
        } else {
            expiresTime = SQLNEVER;
        }

        if (!expiresError) {
            if (add) {
                result = AdminSecurityExemptionsAdd(t, user, expiresTime, comment);
            } else {
                result = AdminSecurityExemptionsUpdate(t, id, user, expiresTime, comment);
            }
            if (result != SQLITE_CONSTRAINT) {
                AdminSecurityExemptionsRedirect(t, 303);
                return TRUE;
            }
            userError = "An exemption already exists for that user";
        }
    }

    AdminBaseHeader(t, AHSORTABLE, "Security Exemptions Edit",
                    " | <a href='/security'>Security</a>");

    WriteLine(s, "<form action='/security/exemptions' method='post'>\n");
    if (id) {
        SendHiddenField(s, "id", user);
    }
    WriteLine(s, "<table>\n");
    WriteField(t, "User", "user", user, userError, 0, 128, NULL);
    WriteField(t, "Expires", "expires", expires, expiresError, 0, 10, NULL);
    WriteField(t, "Comment", "comment", comment, NULL, 0, 255, NULL);
    WriteLine(s, "</table>\n");
    if (id && *id) {
        WriteLine(s, "<input name='action' type='submit' value='Update'>\n");
        WriteLine(s,
                  "<input name='action' type='submit' value='Delete' onclick='return "
                  "confirm(\"Really delete exemption?\")'>\n");
    } else {
        WriteLine(s, "<input name='action' type='submit' value='Add'>\n");
    }
    WriteLine(s, "<input name='action' type='submit' value='Cancel'>\n");
    WriteLine(s, "</form>\n");

    WriteLine(s,
              "<p>User supports wildcards <strong>?</strong> to match exactly one character, "
              "<strong>*</strong> to match any number of characters, and <strong>[]</strong> to "
              "match a list of characters such as [123] or [a-z].</p>\n");

    FreeThenNull(expires);
    FreeThenNull(comment);
    return TRUE;
}

void AdminSecurityExemptions(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    const int ASACTION = 0;
    const int ASUSER = 1;

    struct FORMFIELD ffs[] = {
        {"action", NULL, 0, 0, 10},
        {"user", NULL, 0, 0, 128},
        {NULL, NULL, 0, 0}
    };

    // clang-format off
    const char *selectSql =
        "SELECT"
            " user AS \"User\","
            // " DATETIME(created_at, 'unixepoch', 'localtime') AS \"Created\","
            " CASE"
                " WHEN expires = NEVER() THEN 'Never'"
                " ELSE SUBSTR(DATETIME(expires, 'unixepoch', 'localtime'), 1, 10)"
            " END AS \"Expires\","
            " CASE"
                " WHEN applied_at IS NULL THEN 'Never'"
                " ELSE DATETIME(applied_at, 'unixepoch', 'localtime')"
            " END AS \"Last Applied\","
            " applied AS \"Count\","
            " comment AS \"Comment\""
        " FROM"
            " exemption"
        " ORDER BY"
            " user";
    // clang-format on

    sqlite3 *db = SecurityGetSharedDatabase();
    int rows, cols;
    char **result;
    char *errMsg = NULL;
    int i;
    int j;
    time_t now;
    char nows[MAXASCIIDATE];

    if (AdminSecurityExemptionsEdit(t, query, post)) {
        return;
    }

    UUtime(&now);
    ASCIIDate(&now, nows);

    AdminBaseHeader(t, AHSORTABLE, "Security Exemptions", " | <a href='/security'>Security</a>");

    SQLOK(sqlite3_get_table(db, selectSql, &result, &rows, &cols, &errMsg));

    if (rows == 0) {
        WriteLine(s, "<p>No security exemptions exist</p>\n");
    } else {
        WriteLine(s, "<table id='security-exemptions' class='bordered-table sortable'>\n");
        const char *td = "th";
        const char *tda = " scope='col'";
        for (i = 0; i <= rows; i++) {
            WriteLine(s, "<tr>\n");
            for (j = 0; j < cols; j++) {
                WriteLine(s, "<%s%s>", td, tda);
                if (i > 0 && j == 0) {
                    WriteLine(s, "<a href='/security/exemptions?action=edit&id=");
                    SendUrlEncoded(s, result[i * cols + j]);
                    WriteLine(s, "'>");
                }
                SendHTMLEncoded(s, result[i * cols + j]);
                if (i > 0 && j == 0) {
                    WriteLine(s, "</a>");
                }
                WriteLine(s, "</%s>", td);
            }
            WriteLine(s, "</tr>\n");
            td = "td";
            tda = "";
        }

        WriteLine(s, "</table>\n");
    }
    sqlite3_free_table(result);

    WriteLine(s, "<p><a href='/security/exemptions?action=edit'>Add Exemption</a></p>\n");

Finished:
    AdminFooter(t, 0);
}

static void AdminSecurityNotificationsDelete() {
    sqlite3 *db = EnvironmentGetSharedDatabase();
    const char *sql = "DELETE FROM notificationemailaddress";
    sqlite3_stmt *stmt = NULL;
    SQLPrepare(db, sql, &stmt);
    SQLOKORDONE(sqlite3_step(stmt));
    sqlite3_finalize(stmt);

    const char *sql1 = "DELETE FROM receivesdailydigest";
    sqlite3_stmt *stmt1 = NULL;
    SQLPrepare(db, sql1, &stmt1);
    SQLOKORDONE(sqlite3_step(stmt1));
    sqlite3_finalize(stmt1);
}

static int AdminSecurityNotificationsAdd(char *emailaddress) {
    sqlite3 *db = EnvironmentGetSharedDatabase();

    // clang-format off
    const char *sql =
        "INSERT INTO"
            " notificationemailaddress(emailaddress)"
        " VALUES(:emailaddress)";
    // clang-format on

    sqlite3_stmt *stmt = NULL;
    int result;
    SQLPrepare(db, sql, &stmt);
    SQLOK(SQLBindTextByName(stmt, ":emailaddress", emailaddress, SQLITE_STATIC));
    result = sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    if (result != SQLITE_CONSTRAINT) {
        SQLOKORDONE(result);
    }
    return result;
}

static int AdminSecurityDigestNotificationsAdd(int receivesdailydigest) {
    sqlite3 *db = EnvironmentGetSharedDatabase();

    // clang-format off
    const char *sql =
        "INSERT INTO"
            " receivesdailydigest(receivesdailydigest)"
        " VALUES(:receivesdailydigest)";
    // clang-format on

    sqlite3_stmt *stmt = NULL;
    int result;
    SQLPrepare(db, sql, &stmt);
    SQLOK(SQLBindIntByName(stmt, ":receivesdailydigest", receivesdailydigest));
    result = sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    if (result != SQLITE_CONSTRAINT) {
        SQLOKORDONE(result);
    }
    return result;
}

static void AdminSecurityNotificationsDeleteWsKeyTableRows() {
    sqlite3 *db = EnvironmentGetSharedDatabase();
    const char *sql = "DELETE FROM wskey";
    sqlite3_stmt *stmt = NULL;
    SQLPrepare(db, sql, &stmt);
    SQLOKORDONE(sqlite3_step(stmt));
    sqlite3_finalize(stmt);
}

static void AdminSecurityNotificationsInsertWsKeyTableRowWithNewKey(char *newKey) {
    sqlite3 *db = EnvironmentGetSharedDatabase();

    // clang-format off
    const char *sql =
        "INSERT INTO"
            " wskey(id, key)"
        " VALUES(1, :key)";
    // clang-format on

    sqlite3_stmt *stmt = NULL;
    SQLPrepare(db, sql, &stmt);
    SQLOK(SQLBindTextByName(stmt, ":key", newKey, SQLITE_STATIC));
    int result = sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    if (result != SQLITE_CONSTRAINT) {
        SQLOKORDONE(result);
    }
}

static void AdminSecurityNotificationsUpdateWsKeyTableRowWithNewSecret(char *newSecret) {
    sqlite3 *db = EnvironmentGetSharedDatabase();

    // clang-format off
    const char *sql =
        "UPDATE"
            " wskey"
        " SET"
            " secret = :secret";
    // clang-format on

    sqlite3_stmt *stmt = NULL;
    SQLPrepare(db, sql, &stmt);
    SQLOK(SQLBindTextByName(stmt, ":secret", newSecret, SQLITE_STATIC));
    int result = sqlite3_step(stmt);
    if (result != SQLITE_CONSTRAINT) {
        SQLOKORDONE(result);
    }
    sqlite3_finalize(stmt);
}

void GetWskeySecret(char **secret) {
    sqlite3 *db = EnvironmentGetSharedDatabase();
    int rows, cols;
    char **result;
    char *errMsg = NULL;

    // clang-format off
    const char *selectWsKeySql =
            "SELECT"
                " secret"
            " FROM"
                " wskey";
    // clang-format on

    SQLOK(sqlite3_get_table(db, selectWsKeySql, &result, &rows, &cols, &errMsg));
    if (rows > 0) {
        char *dbSecret = NULL;
        dbSecret = UnobscureString(result[cols]);
        *secret = UUStrDup(dbSecret);
        FreeThenNull(dbSecret);
    }
    sqlite3_free_table(result);
}

void AdminSecurityNotifications(struct TRANSLATE *t, char *query, char *post) {
    int i;
    int rows, cols;
    char **result;
    char *errMsg = NULL;
    BOOL hasSecret;
    hasSecret = FALSE;
    sqlite3 *db = EnvironmentGetSharedDatabase();
    // check key against database key
    char *key = NULL;
    GetWskeyKey(&key);
    if (key == NULL) {
        char empty[] = "";
        SQLBeginTransaction(db);
        AdminSecurityNotificationsDeleteWsKeyTableRows();
        AdminSecurityNotificationsInsertWsKeyTableRowWithNewKey(empty);
        SQLCommitTransaction(db);
    } else {
        SQLBeginTransaction(db);
        char *dbKey = NULL;

        // clang-format off
        const char *selectWsKeySql =
                "SELECT"
                    " key,"
                    " secret"
                " FROM"
                    " wskey";
        // clang-format on

        SQLOK(sqlite3_get_table(db, selectWsKeySql, &result, &rows, &cols, &errMsg));
        if (rows > 0) {
            dbKey = malloc(strlen(result[cols]) + 1);
            if (dbKey == NULL)
                PANIC;
            strcpy(dbKey, result[cols]);
            if (result[cols + 1] != NULL) {
                char *dbSecret = NULL;
                dbSecret = UnobscureString(result[cols + 1]);
                if (strlen(dbSecret) > 0) {
                    hasSecret = TRUE;
                }
                FreeThenNull(dbSecret);
            }
            if (strcmp(dbKey, key) == 0) {
                // keys are same
            } else {
                AdminSecurityNotificationsDeleteWsKeyTableRows();
                AdminSecurityNotificationsInsertWsKeyTableRowWithNewKey(key);
            }
        } else {
            AdminSecurityNotificationsInsertWsKeyTableRowWithNewKey(key);
        }
        sqlite3_free_table(result);
        FreeThenNull(dbKey);
        SQLCommitTransaction(db);
    }
    FreeThenNull(key);
    if (strcmp(t->method, "POST") == 0) {
        static pcre *pattern = NULL;
        static pcre_extra *patternExtra = NULL;
        const char *pcreError;
        int pcreErrorOffset;
        static char *re = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$";
        if (pattern == NULL) {
            pattern = pcre_compile(re, PCRE_CASELESS, &pcreError, &pcreErrorOffset, NULL);
            if (pattern == NULL)
                PANIC;
            patternExtra = pcre_study(pattern, 0, &pcreError);
        }
        char *str;
        char delim[] = ",";
        const int ASEMAILRECIPIENTS = 0;
        const int ASRECEIVESDAILYDIGEST = 1;
        const int ASSECRET = 2;
        struct FORMFIELD ffs[] = {
            {"email-recipients", NULL, 0, 0, 1024},
            {"receives-daily-digest", NULL, 0, 0, 1},
            {"secret", NULL, 0, 0, 50},
            {NULL, NULL, 0, 0}
        };
        FindFormFields(NULL, post, ffs);
        if (ffs[ASSECRET].value != NULL) {
            // check generating token from key and secret
            char *newSecret = NULL;
            newSecret = UUStrDup(ffs[ASSECRET].value);
            char *obscure = NULL;
            obscure = ObscureString(newSecret);
            SQLBeginTransaction(db);
            AdminSecurityNotificationsUpdateWsKeyTableRowWithNewSecret(obscure);
            SQLCommitTransaction(db);
            FreeThenNull(obscure);
            if (getIntrusionPem() == NULL) {
                if (IntrusionReadConfig() == 0) {
                    return;
                }
            }
            char *wskeytoken = NULL;
            GetTokenFromWsKeyAndSecret(&wskeytoken);
            if (wskeytoken == NULL) {
                char emptySecret[] = "";
                char *obscure2 = NULL;
                obscure2 = ObscureString(emptySecret);
                SQLBeginTransaction(db);
                AdminSecurityNotificationsUpdateWsKeyTableRowWithNewSecret(obscure2);
                SQLCommitTransaction(db);
                FreeThenNull(obscure2);
            } else {
                hasSecret = TRUE;
            }
            FreeThenNull(wskeytoken);
        } else {
            int newEmailLength;
            char *emailrecipients = NULL;
            char *newemailrecipients;
            newEmailLength = strlen("\n") + 1;
            newemailrecipients = malloc(newEmailLength);
            if (newemailrecipients == NULL)
                PANIC;
            strcpy(newemailrecipients, "\n");
            int receivesdailydigest = 0;
            if (ffs[ASRECEIVESDAILYDIGEST].value != NULL) {
                receivesdailydigest = 1;
            }
            SQLBeginTransaction(db);
            AdminSecurityNotificationsDelete();
            SQLCommitTransaction(db);
            SQLBeginTransaction(db);
            AdminSecurityDigestNotificationsAdd(receivesdailydigest);
            emailrecipients = UUStrDup(ffs[ASEMAILRECIPIENTS].value);
            char *token = strtok_r(emailrecipients, delim, &str);
            while (token != NULL) {
                Trim(token, TRIM_LEAD | TRIM_TRAIL);
                if (strlen(token) > 0) {
                    AllLower(token);
                    BOOL isValid =
                        pcre_exec(pattern, patternExtra, token, strlen(token), 0, 0, 0, 0) >= 0;
                    if (isValid) {
                        // prevent duplicates
                        char *tempvalue;
                        tempvalue = malloc(strlen(token) + 3);
                        if (tempvalue == NULL)
                            PANIC;
                        strcpy(tempvalue, "\n");
                        strcat(tempvalue, token);
                        strcat(tempvalue, "\n");
                        if (strstr(newemailrecipients, tempvalue) == NULL) {
                            newEmailLength = newEmailLength + strlen(token) + 2;
                            newemailrecipients = realloc(newemailrecipients, newEmailLength);
                            if (newemailrecipients == NULL)
                                PANIC;
                            strcat(newemailrecipients, token);
                            strcat(newemailrecipients, "\n");
                            AdminSecurityNotificationsAdd(token);
                        }
                        FreeThenNull(tempvalue);
                    }
                }
                token = strtok_r(NULL, delim, &str);
            }
            SQLCommitTransaction(db);
            FreeThenNull(newemailrecipients);
        }
    }
    struct UUSOCKET *s = &t->ssocket;

    // clang-format off
    const char *selectSql =
        "SELECT"
            " emailaddress"
        " FROM"
            " notificationemailaddress"
        " ORDER BY"
            " emailaddress";
    // clang-format on

    // clang-format off
    const char *selectSql1 =
        "SELECT"
            " receivesdailydigest"
        " FROM"
            " receivesdailydigest";
    // clang-format on

    AdminBaseHeader(t, AHSORTABLE, "Tripped Security Rules Notifications",
                    " | <a href=\"/security\">Security</a>");

    SQLOK(sqlite3_get_table(db, selectSql, &result, &rows, &cols, &errMsg));

    WriteLine(s, "<form action='/security/notifications' method='post'>\n");
    WriteLine(s,
              "<label for='email-recipients'>Email Address(s) to Receive Notifications:</label>\n");
    WriteLine(s, "<div>\n");
    WriteLine(s,
              "<textarea id='email-recipients' name='email-recipients' rows='4' cols='80' "
              "maxlength='1024'>\n");
    if (rows > 0) {
        for (i = 1; i <= rows; i++) {
            if (i > 1) {
                WriteLine(s, ", ");
            }
            WriteLine(s, "%s", result[i * cols]);
        }
    }
    sqlite3_free_table(result);
    WriteLine(s, "</textarea>\n");
    WriteLine(s,
              "<br>e.g. user@institution.edu (use ', ' to separate email addresses, 1024 character "
              "limit)<br><br>\n");
    WriteLine(s, "</div>\n");

    SQLOK(sqlite3_get_table(db, selectSql1, &result, &rows, &cols, &errMsg));
    WriteLine(
        s,
        "<input name='receives-daily-digest' id='receives-daily-digest' type='checkbox' value='1'");
    if (rows > 0) {
        if (*result[1] == '1') {
            WriteLine(s, " checked");
        }
    }
    sqlite3_free_table(result);
    WriteLine(s, ">\n");
    WriteLine(s,
              "<label for='receives-daily-digest'> Receive Daily Email Digest (All email "
              "addresses)</label><br>\n");
    WriteLine(s, "<input name='action' id='action' type='submit' value='Submit'>\n");
    WriteLine(s, "</form>\n<br>\n");

    WriteLine(s, "<form action='/security/notifications' method='post'>\n");
    WriteLine(s, "<label for='secret'>WSKey Secret ");
    WriteLine(s,
              "<a "
              "href='https://help.oclc.org/Library_Management/EZproxy/Install_and_update_EZproxy/"
              "EZproxy_WSKeys' target='_blank'>*</a></label><br>\n");
    WriteLine(s, "<input name='secret' id='secret' size='70' type='text' value=''>\n");
    if (hasSecret) {
        WriteLine(s, "<input name='action' id='action' type='submit' value='Update'>\n");
    } else {
        WriteLine(s, "<input name='action' id='action' type='submit' value='Add'>\n");
    }
    WriteLine(s, "</form>\n");

    AdminFooter(t, 0);
}

static void AdminSecurityNotificationsQueuedTrippedIdsDelete(char **trippedIds) {
    sqlite3 *db = EnvironmentGetDatabase();
    const char *selectSqlStart = "DELETE FROM queuedtrippedrules WHERE id IN (";
    const char *selectSqlEnd = ")";
    char *selectSql;
    selectSql = malloc(strlen(selectSqlStart) + strlen(*trippedIds) + strlen(selectSqlEnd) + 1);
    if (selectSql == NULL)
        PANIC;
    strcpy(selectSql, selectSqlStart);
    strcat(selectSql, *trippedIds);
    strcat(selectSql, selectSqlEnd);
    sqlite3_stmt *stmt = NULL;
    SQLPrepare(db, selectSql, &stmt);
    SQLOKORDONE(sqlite3_step(stmt));
    sqlite3_finalize(stmt);
    FreeThenNull(selectSql);
}

static void AdminSecurityNotificationsGetQueuedTrippedIds(char **trippedIds) {
    char *delimiter = ", ";

    // clang-format off
    const char *selectSql =
        "SELECT"
            " id"
        " FROM"
            " queuedtrippedrules"
        " ORDER BY"
            " id";
    // clang-format on

    sqlite3 *db = EnvironmentGetDatabase();
    int i;
    int rows, cols;
    char *errMsg = NULL;
    char **result;
    SQLOK(sqlite3_get_table(db, selectSql, &result, &rows, &cols, &errMsg));
    if (rows > 0) {
        int messageLength;
        char *tempStorage;
        for (i = 1; i <= rows; i++) {
            if (i == 1) {
                messageLength = strlen(result[i * cols]) + 1;
                tempStorage = malloc(messageLength);
                if (tempStorage == NULL)
                    PANIC;
                strcpy(tempStorage, result[i * cols]);
            } else if (i > 1) {
                messageLength = messageLength + strlen(result[i * cols]) + strlen(delimiter) + 1;
                tempStorage = realloc(tempStorage, messageLength);
                if (tempStorage == NULL)
                    PANIC;
                strcat(tempStorage, delimiter);
                strcat(tempStorage, result[i * cols]);
            }
        }
        *trippedIds = UUStrDup(tempStorage);
        FreeThenNull(tempStorage);
    }
    sqlite3_free_table(result);
}

static void AdminSecurityNotificationsGetEmailAddresses(char **emailAddresses) {
    char *delimiter = ";";

    // clang-format off
    const char *selectSql =
        "SELECT"
            " emailaddress"
        " FROM"
            " notificationemailaddress"
        " ORDER BY"
            " emailaddress";
    // clang-format on

    sqlite3 *db = EnvironmentGetDatabase();
    int i;
    int rows, cols;
    char *errMsg = NULL;
    char **result;
    SQLOK(sqlite3_get_table(db, selectSql, &result, &rows, &cols, &errMsg));
    if (rows > 0) {
        int messageLength;
        char *tempStorage;
        for (i = 1; i <= rows; i++) {
            if (i == 1) {
                messageLength = strlen(result[i * cols]) + 1;
                tempStorage = malloc(messageLength);
                if (tempStorage == NULL)
                    PANIC;
                strcpy(tempStorage, result[i * cols]);
            } else if (i > 1) {
                messageLength = messageLength + strlen(result[i * cols]) + 2;
                tempStorage = realloc(tempStorage, messageLength);
                if (tempStorage == NULL)
                    PANIC;
                strcat(tempStorage, delimiter);
                strcat(tempStorage, result[i * cols]);
            }
        }
        *emailAddresses = UUStrDup(tempStorage);
        FreeThenNull(tempStorage);
    }
    sqlite3_free_table(result);
}

static int AdminSecurityNotificationTransactionsAdd(char *emailaddress, char *transactionid) {
    sqlite3 *db = EnvironmentGetDatabase();
    time_t now;
    UUtime(&now);

    // clang-format off
    const char *sql =
        "INSERT INTO"
            " notificationtransactions(emailaddress, transactionid, at)"
        " VALUES(:emailaddress, :transactionid, :now)";
    // clang-format on

    sqlite3_stmt *stmt = NULL;
    int result;
    SQLPrepare(db, sql, &stmt);
    SQLOK(SQLBindTextByName(stmt, ":emailaddress", emailaddress, SQLITE_STATIC));
    SQLOK(SQLBindTextByName(stmt, ":transactionid", transactionid, SQLITE_STATIC));
    SQLBindInt64ByName(stmt, ":now", now);
    result = sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    if (result != SQLITE_CONSTRAINT) {
        SQLOKORDONE(result);
    }
    return result;
}

void DeleteExpiredNotificationTransactions() {
    time_t when;
    sqlite3 *db = EnvironmentGetSharedDatabase();
    sqlite3_stmt *stmt;

    // clang-format off
    const char *sql =
        "DELETE FROM"
            " notificationtransactions"
        " WHERE"
            " at < :now";
    // clang-format on

    SQLPrepare(db, sql, &stmt);
    UUtime(&when);
    when -= 86400 * 30;
    SQLBindInt64ByName(stmt, ":now", when);
    SQLOKORDONE(sqlite3_step(stmt));
    sqlite3_finalize(stmt);
}

struct memoryStruct {
    char *response;
    size_t size;
};

static size_t writeMemoryCallback(void *data, size_t size, size_t nmemb, void *userp) {
    size_t realsize = size * nmemb;
    struct memoryStruct *mem = (struct memoryStruct *)userp;

    char *ptr = realloc(mem->response, mem->size + realsize + 1);
    if (ptr == NULL)
        return 0; /* out of memory! */

    mem->response = ptr;
    memcpy(&(mem->response[mem->size]), data, realsize);
    mem->size += realsize;
    mem->response[mem->size] = 0;

    return realsize;
}

static CURLcode SecurityEmailSslCtx(CURL *curl, void *sslCtx, void *parm) {
    CURLcode rv = CURLE_ABORTED_BY_CALLBACK;
    BIO *cbio = NULL;
    X509_STORE *cts = NULL;
    int i;
    STACK_OF(X509_INFO) *inf = NULL;

    if (getIntrusionPem() == NULL) {
        return rv;
    }

    if (!(cbio = BIO_new_mem_buf(getIntrusionPem(), -1)))
        PANIC;
    if (!(cts = SSL_CTX_get_cert_store((SSL_CTX *)sslCtx)))
        PANIC;

    if (!(inf = PEM_X509_INFO_read_bio(cbio, NULL, NULL, NULL)))
        PANIC;

    for (i = 0; i < sk_X509_INFO_num(inf); i++) {
        X509_INFO *itmp = sk_X509_INFO_value(inf, i);
        if (itmp->x509) {
            X509_STORE_add_cert(cts, itmp->x509);
        }
        if (itmp->crl) {
            X509_STORE_add_crl(cts, itmp->crl);
        }
    }

    sk_X509_INFO_pop_free(inf, X509_INFO_free);
    BIO_free(cbio);

    rv = CURLE_OK;
    return rv;
}

void GetTokenFromWsKeyAndSecret(char **token) {
    CURL *curl;
    CURLcode res;
    struct curl_slist *headers = NULL;
    struct memoryStruct chunk;
    json_t *root;
    size_t i;
    char *secret = NULL;
    char *key = NULL;
    GetWskeyKey(&key);
    GetWskeySecret(&secret);
    if (key == NULL || secret == NULL) {
        if (key == NULL) {
            Log("NULL WSKey.");
        }
        if (secret == NULL) {
            Log("NULL WSKey secret.");
        }
        goto Finished;
    }
    chunk.response = malloc(1);
    chunk.size = 0;
    char *msg;
    char *colon = ":";
    msg = malloc(strlen(key) + strlen(secret) + 2);
    if (msg == NULL)
        PANIC;
    strcpy(msg, key);
    strcat(msg, colon);
    strcat(msg, secret);
    char *msg64 = NULL;
    msg64 = Encode64(NULL, msg);
    FreeThenNull(msg);
    char *authorizationHeader;
    char *basic = "Authorization: Basic ";
    authorizationHeader = malloc(strlen(basic) + strlen(msg64) + 1);
    if (authorizationHeader == NULL)
        PANIC;
    strcpy(authorizationHeader, basic);
    strcat(authorizationHeader, msg64);
    FreeThenNull(msg64);

    char *message;
    char *grantType = "grant_type=client_credentials";
    char *scope = "&scope=notification:email";
    message = malloc(strlen(grantType) + strlen(scope) + 1);
    if (message == NULL)
        PANIC;
    strcpy(message, grantType);
    strcat(message, scope);

    /* get a curl handle */
    if (!(curl = curl_easy_init()))
        PANIC;
    /* First set the URL that is about to receive our POST. */

    // Make sure that the peer and host can be verified
    curl_easy_setopt(curl, CURLOPT_CAINFO, NULL);
    curl_easy_setopt(curl, CURLOPT_CAPATH, NULL);
    curl_easy_setopt(curl, CURLOPT_SSL_CTX_FUNCTION, SecurityEmailSslCtx);

    curl_easy_setopt(curl, CURLOPT_URL, "https://oauth.oclc.org/token");

    /* send all data to this function  */
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeMemoryCallback);
    /* we pass our 'chunk' struct to the callback function */
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

    if (!(headers = curl_slist_append(headers, "content-type: application/x-www-form-urlencoded")))
        PANIC;
    if (!(headers = curl_slist_append(headers, authorizationHeader)))
        PANIC;

    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    /* Now specify the POST data */
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, message);

    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 1L);
    /* Perform the request, res will get the return code */
    res = curl_easy_perform(curl);
    /* Check for errors */
    if (res == CURLE_OK) {
        json_error_t error;
        root = json_loads(chunk.response, 0, &error);
        if (root) {
            /* parse the json */
            json_t *responsemessage = json_object_get(root, "access_token");
            const char *accessToken = json_string_value(responsemessage);
            *token = UUStrDup(accessToken);
            json_decref(responsemessage);
        }
    } else {
        // Log what comes back from the server
        Log(chunk.response);
    }
    curl_slist_free_all(headers);
    /* always cleanup */
    curl_easy_cleanup(curl);
    free(chunk.response);
    FreeThenNull(authorizationHeader);
    FreeThenNull(message);
    curl_global_cleanup();

Finished:
    FreeThenNull(key);
    FreeThenNull(secret);
}

int SendEmailNotification(char *emailaddresses, char *message) {
    /* return 0 for success and 1 for failure */
    CURL *curl;
    CURLcode res;
    struct curl_slist *headers = NULL;
    struct memoryStruct chunk;
    chunk.response = malloc(1);
    chunk.size = 0;
    json_t *root;
    size_t i;
    if (getIntrusionPem() == NULL) {
        if (IntrusionReadConfig() == 0) {
            return 1;
        }
    }
    char *token = NULL;
    GetTokenFromWsKeyAndSecret(&token);
    if (token == NULL) {
        Log("The WSKey and secret pair did not generate a token.");
        return 1;
    }
    char *tokenHeaderStart = "Authorization: Bearer ";
    char *tokenHeader;
    tokenHeader = malloc(strlen(tokenHeaderStart) + strlen(token) + 1);
    strcpy(tokenHeader, tokenHeaderStart);
    strcat(tokenHeader, token);
    FreeThenNull(token);

    /* get a curl handle */
    if (!(curl = curl_easy_init()))
        PANIC;
    /* First set the URL that is about to receive our POST. */

    // Make sure that the peer and host can be verified
    curl_easy_setopt(curl, CURLOPT_CAINFO, NULL);
    curl_easy_setopt(curl, CURLOPT_CAPATH, NULL);
    curl_easy_setopt(curl, CURLOPT_SSL_CTX_FUNCTION, SecurityEmailSslCtx);

    // curl_easy_setopt(curl, CURLOPT_URL,
    // "http://nsnotifws01dxm1.dev.oclc.org:8080/notification/v2/email/send");
    curl_easy_setopt(curl, CURLOPT_URL,
                     "https://enterprise.api.oclc.org/notification/v2/email/send");

    /* send all data to this function  */
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeMemoryCallback);
    /* we pass our 'chunk' struct to the callback function */
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

    if (!(headers = curl_slist_append(headers, tokenHeader)))
        PANIC;

    if (!(headers = curl_slist_append(headers, "Content-Type: application/json")))
        PANIC;
    if (!(headers = curl_slist_append(headers, "X-OCLC-EmailType: HTML")))
        PANIC;
    if (!(headers = curl_slist_append(headers, "X-OCLC-DisableOptOut: true")))
        PANIC;

    /* InstitutionId */
    if (!(headers = curl_slist_append(headers, "X-OCLC-ClientId: EZPROXY")))
        PANIC;
    // if (!(headers = curl_slist_append(headers, "X-OCLC-ClientId: WSILL"))) PANIC;

    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    /* Now specify the POST data */
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, message);

    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 1L);
    /* Perform the request, res will get the return code */
    res = curl_easy_perform(curl);
    /* Check for errors */
    if (res == CURLE_OK) {
        json_error_t error;
        root = json_loads(chunk.response, 0, &error);
        if (root) {
            /* parse the json */
            json_t *responsemessage = json_object_get(root, "message");
            if (json_is_array(responsemessage)) {
                char *str;
                char delim[] = ";";
                char *emailaddress = strtok_r(emailaddresses, delim, &str);
                for (i = 0; i < json_array_size(responsemessage); i++) {
                    json_t *data = json_array_get(responsemessage, i);
                    json_t *transactionIdObject = json_object_get(data, "transactionId");
                    const char *transactionid = json_string_value(transactionIdObject);
                    AdminSecurityNotificationTransactionsAdd(emailaddresses, transactionid);
                    emailaddress = strtok_r(NULL, delim, &str);
                    json_decref(transactionIdObject);
                }
            }
        }
    } else {
        Log("Error sending email(s) to Notification Facade");
    }
    curl_slist_free_all(headers);
    /* always cleanup */
    curl_easy_cleanup(curl);
    free(chunk.response);
    curl_global_cleanup();
    FreeThenNull(tokenHeader);
    return 0;
}

void SendQueuedSecurityRuleEmail() {
    char *myUrl = (myUrlHttps && *myUrlHttps ? myUrlHttps : myUrlHttp);
    char *emailAddresses = NULL;
    AdminSecurityNotificationsGetEmailAddresses(&emailAddresses);
    if (emailAddresses == NULL) {
        FreeThenNull(emailAddresses);
        return;
    }
    char *trippedIds = NULL;
    AdminSecurityNotificationsGetQueuedTrippedIds(&trippedIds);
    if (trippedIds == NULL) {
        FreeThenNull(trippedIds);
        FreeThenNull(emailAddresses);
        return;
    }
    char *html;
    char *startHtml =
        "<!DOCTYPE html>\n<html>\n<head></head>\n<body>\n"
        "<p>The following EZproxy Security Rules were tripped on ";
    char *startHtml1 = ".</p>\n<p>&nbsp; </p>\n";
    html = malloc(strlen(startHtml) + strlen(myName) + strlen(startHtml1) + 1);
    if (html == NULL)
        PANIC;
    strcpy(html, startHtml);
    strcat(html, myName);
    strcat(html, startHtml1);

    // clang-format off
    const char *selectSqlStart =
        "SELECT"
            " DATETIME(tripped.at, 'unixepoch', 'localtime') AS \"When\","
            " rule.rulename AS \"Rule\","
            " tripped.user AS \"User\","
            " KMGT((SELECT COUNT(*) FROM tripped WHERE tripped.ruleid = rule.id)) AS \"Tripped\","
            " CASE"
                " WHEN expires = 0 THEN 'Logged'"
                " WHEN expires = EXEMPT() THEN 'Exempt'"
                " WHEN expires = NEVER() THEN 'Never'"
                " ELSE DATETIME(expires, 'unixepoch', 'localtime')"
            " END AS \"Expires\""
          " FROM"
            " tripped"
            " INNER JOIN rule ON rule.id = tripped.ruleid"
            " WHERE tripped.id IN (";
    // clang-format on

    // clang-format off
    const char *selectSqlEnd =
                      ")"
              " ORDER BY"
                  " tripped.at, tripped.ruleid, user";
    // clang-format on

    char *selectSql;
    selectSql = malloc(strlen(selectSqlStart) + strlen(trippedIds) + strlen(selectSqlEnd) + 1);
    if (selectSql == NULL)
        PANIC;
    strcpy(selectSql, selectSqlStart);
    strcat(selectSql, trippedIds);
    strcat(selectSql, selectSqlEnd);
    sqlite3 *db = SecurityGetSharedDatabase();
    int rows, cols;
    char **result;
    char *errMsg = NULL;
    SQLOK(sqlite3_get_table(db, selectSql, &result, &rows, &cols, &errMsg));
    if (rows > 0) {
        int i;
        int j;
        char *startTable =
            "<table id='security-tripped' cellspacing='0' cellpadding='0' border='1'>\n";
        char *endTable = "</table>";
        char *startRow = "<tr>\n";
        char *endRow = "</tr>\n";
        char *startHeaderCell = "<th>";
        char *endHeaderCell = "</th>";
        char *startCell = "<td>";
        char *endCell = "</td>";
        // make it easy to change the link later
        char *startHref = "<a href='";
        char *linkPage = "/security/tripped";
        char *endHrefStart = "'>";
        char *endHref = "</a>";
        html = realloc(html, strlen(html) + strlen(startTable) + 1);
        if (html == NULL)
            PANIC;
        strcat(html, startTable);
        for (i = 0; i <= rows; i++) {
            html = realloc(html, strlen(html) + strlen(startRow) + 1);
            if (html == NULL)
                PANIC;
            strcat(html, startRow);
            BOOL isHeader = i == 0;
            for (j = 0; j < cols; j++) {
                if (isHeader) {
                    html =
                        realloc(html, strlen(html) + strlen(startHeaderCell) +
                                          strlen(result[i * cols + j]) + strlen(endHeaderCell) + 3);
                    if (html == NULL)
                        PANIC;
                    strcat(html, startHeaderCell);
                    strcat(html, result[i * cols + j]);
                    strcat(html, endHeaderCell);
                } else {
                    BOOL isUser = j == 2;
                    if (isUser) {
                        html =
                            realloc(html, strlen(html) + strlen(startCell) + strlen(startHref) +
                                              strlen(myUrl) + strlen(linkPage) +
                                              strlen(endHrefStart) + strlen(result[i * cols + j]) +
                                              strlen(endHref) + strlen(endCell) + 3);
                        if (html == NULL)
                            PANIC;
                        strcat(html, startCell);
                        strcat(html, startHref);
                        strcat(html, myUrl);
                        strcat(html, linkPage);
                        strcat(html, endHrefStart);
                        strcat(html, result[i * cols + j]);
                        strcat(html, endHref);
                        strcat(html, endCell);
                    } else {
                        html =
                            realloc(html, strlen(html) + strlen(startCell) +
                                              strlen(result[i * cols + j]) + strlen(endCell) + 3);
                        if (html == NULL)
                            PANIC;
                        strcat(html, startCell);
                        strcat(html, result[i * cols + j]);
                        strcat(html, endCell);
                    }
                }
            }
            html = realloc(html, strlen(html) + strlen(endRow) + 1);
            if (html == NULL)
                PANIC;
            strcat(html, endRow);
        }
        html = realloc(html, strlen(html) + strlen(endTable) + 1);
        if (html == NULL)
            PANIC;
        strcat(html, endTable);
    }
    sqlite3_free_table(result);
    // optOut places the text right after the html so put the optOut in the html email body.
    char *endHtml =
        "\n<p>Contact your EZproxy Administrator to stop receiving these "
        "emails.</p>\n</body>\n</html>\n";
    html = realloc(html, strlen(html) + strlen(endHtml) + 1);
    if (html == NULL)
        PANIC;
    strcat(html, endHtml);
    json_t *json =
        json_pack("{ssssssssssssss}", "to", emailAddresses, "subject",
                  "EZproxy Security Rule Incident", "fromEmail", "noreply@oclc.org", "fromName",
                  "OCLCSupport (no replies)", "replyTo", "noreply@oclc.org", "text", html, "optOut",
                  "Contact your EZproxy Administrator to stop receiving these emails.");
    FreeThenNull(html);
    char *jsonEmailMessage = NULL;
    jsonEmailMessage = json_dumps(json, 0);
    json_decref(json);
    int sentEmailResult = SendEmailNotification(emailAddresses, jsonEmailMessage);
    FreeThenNull(emailAddresses);
    if (sentEmailResult == 0) {
        AdminSecurityNotificationsQueuedTrippedIdsDelete(&trippedIds);
    } else {
        /* Remove any trippedIds that do not exist in the tripped database table.
         * This is expected to happen if notifications are not able to be sent for a long time.
         * Once the supporting data is gone, there is no reason to keep the ids. */
        char *delimiter = ", ";
        char *comma = ",";
        char *replaceIds = NULL;
        replaceIds = malloc(strlen(delimiter) + strlen(trippedIds) + strlen(comma) + 1);
        if (replaceIds == NULL)
            PANIC;
        strcpy(replaceIds, delimiter);
        strcat(replaceIds, trippedIds);
        strcat(replaceIds, comma);

        // clang-format off
        const char *selectSql =
            "SELECT"
                " id"
            " FROM"
                " tripped"
            " ORDER BY"
                " id";
        // clang-format on

        char *errMsg1 = NULL;
        char **result1;
        SQLOK(sqlite3_get_table(db, selectSql, &result1, &rows, &cols, &errMsg1));
        if (rows > 0) {
            int i;
            /* replace , ##, with , then remove first delimiter and last comma when all done to get
             * final values for IN clause */
            for (i = 1; i <= rows; i++) {
                char *search;
                search = malloc(strlen(delimiter) + strlen(result1[i * cols]) + strlen(comma) + 1);
                if (search == NULL)
                    PANIC;
                strcpy(search, delimiter);
                strcat(search, result1[i * cols]);
                strcat(search, comma);
                char *sp;
                if ((sp = strstr(replaceIds, search)) == NULL) {
                    // 'search' is not in 'replaceIds'?
                } else {
                    int search_len = strlen(search);
                    int replace_len = strlen(comma);
                    int tail_len = strlen(sp + search_len);
                    memmove(sp + replace_len, sp + search_len, tail_len + 1);
                    memcpy(sp, comma, replace_len);
                }
                FreeThenNull(search);
            }
        }
        int replacedLength = strlen(replaceIds);
        if (replacedLength > 0) {
            /* remove leading , (comma space) */
            memmove(replaceIds, replaceIds + 2, replacedLength);
            /* remove trailing , */
            replaceIds[strlen(replaceIds) - 1] = '\0';
            AdminSecurityNotificationsQueuedTrippedIdsDelete(&replaceIds);
        }
        FreeThenNull(replaceIds);
        sqlite3_free_table(result1);
    }
    FreeThenNull(trippedIds);
}

void SendSecurityRuleDigestEmail() {
    char *myUrl = (myUrlHttps && *myUrlHttps ? myUrlHttps : myUrlHttp);
    char *emailAddresses = NULL;
    AdminSecurityNotificationsGetEmailAddresses(&emailAddresses);
    if (emailAddresses == NULL) {
        FreeThenNull(emailAddresses);
        return;
    }
    char *html;
    char *startHtml =
        "<!DOCTYPE html>\n<html>\n<head></head>\n<body>\n"
        "<p>Your daily digest of EZproxy Security Rules that were tripped on ";
    char *startHtml1 = ".</p>\n<p>&nbsp; </p>\n";
    html = malloc(strlen(startHtml) + strlen(myName) + strlen(startHtml1) + 1);
    if (html == NULL)
        PANIC;
    strcpy(html, startHtml);
    strcat(html, myName);
    strcat(html, startHtml1);

    // clang-format off
    const char *selectSql =
        "SELECT"
            " DATETIME(tripped.at, 'unixepoch', 'localtime') AS \"When\","
            " rule.rulename AS \"Rule\","
            " tripped.user AS \"User\","
            " KMGT((SELECT COUNT(*) FROM tripped WHERE tripped.ruleid = rule.id)) AS \"Tripped\","
            " CASE"
                " WHEN expires = 0 THEN 'Logged'"
                " WHEN expires = EXEMPT() THEN 'Exempt'"
                " WHEN expires = NEVER() THEN 'Never'"
                " ELSE DATETIME(expires, 'unixepoch', 'localtime')"
            " END AS \"Expires\""
        " FROM"
            " tripped"
            " INNER JOIN rule ON rule.id = tripped.ruleid"
            " WHERE tripped.at > strftime('%s','now') - (1 * 24 * 60 * 60)" /* 24 hours ago */
        " ORDER BY"
            " tripped.at, tripped.ruleid, user";
    // clang-format on

    sqlite3 *db = SecurityGetSharedDatabase();
    int i;
    int rows, cols;
    char **result;
    char *errMsg = NULL;
    SQLOK(sqlite3_get_table(db, selectSql, &result, &rows, &cols, &errMsg));
    if (rows > 0) {
        int i;
        int j;
        char *startTable =
            "<table id='security-tripped' cellspacing='0' cellpadding='0' border='1'>\n";
        char *endTable = "</table>";
        char *startRow = "<tr>\n";
        char *endRow = "</tr>\n";
        char *startHeaderCell = "<th>";
        char *endHeaderCell = "</th>";
        char *startCell = "<td>";
        char *endCell = "</td>";
        // make it easy to change the link later
        char *startHref = "<a href='";
        char *linkPage = "/security/tripped";
        char *endHrefStart = "'>";
        char *endHref = "</a>";
        html = realloc(html, strlen(html) + strlen(startTable) + 1);
        if (html == NULL)
            PANIC;
        strcat(html, startTable);
        for (i = 0; i <= rows; i++) {
            html = realloc(html, strlen(html) + strlen(startRow) + 1);
            if (html == NULL)
                PANIC;
            strcat(html, startRow);
            BOOL isHeader = i == 0;
            for (j = 0; j < cols; j++) {
                if (isHeader) {
                    html =
                        realloc(html, strlen(html) + strlen(startHeaderCell) +
                                          strlen(result[i * cols + j]) + strlen(endHeaderCell) + 3);
                    if (html == NULL)
                        PANIC;
                    strcat(html, startHeaderCell);
                    strcat(html, result[i * cols + j]);
                    strcat(html, endHeaderCell);
                } else {
                    BOOL isUser = j == 2;
                    if (isUser) {
                        html =
                            realloc(html, strlen(html) + strlen(startCell) + strlen(startHref) +
                                              strlen(myUrl) + strlen(linkPage) +
                                              strlen(endHrefStart) + strlen(result[i * cols + j]) +
                                              strlen(endHref) + strlen(endCell) + 3);
                        if (html == NULL)
                            PANIC;
                        strcat(html, startCell);
                        strcat(html, startHref);
                        strcat(html, myUrl);
                        strcat(html, linkPage);
                        strcat(html, endHrefStart);
                        strcat(html, result[i * cols + j]);
                        strcat(html, endHref);
                        strcat(html, endCell);
                    } else {
                        html =
                            realloc(html, strlen(html) + strlen(startCell) +
                                              strlen(result[i * cols + j]) + strlen(endCell) + 3);
                        if (html == NULL)
                            PANIC;
                        strcat(html, startCell);
                        strcat(html, result[i * cols + j]);
                        strcat(html, endCell);
                    }
                }
            }
            html = realloc(html, strlen(html) + strlen(endRow) + 1);
            if (html == NULL)
                PANIC;
            strcat(html, endRow);
        }
        html = realloc(html, strlen(html) + strlen(endTable) + 1);
        if (html == NULL)
            PANIC;
        strcat(html, endTable);
    } else {
        char *noTrippedRulesHtml = "\n<p>There were no tripped security rules.</p>\n";
        html = realloc(html, strlen(html) + strlen(noTrippedRulesHtml) + 1);
        if (html == NULL)
            PANIC;
        strcat(html, noTrippedRulesHtml);
    }
    sqlite3_free_table(result);
    // optOut places the text right after the html so put the optOut in the html email body.
    char *endHtml =
        "\n<p>Contact your EZproxy Administrator to stop receiving these "
        "emails.</p>\n</body>\n</html>\n";
    html = realloc(html, strlen(html) + strlen(endHtml) + 1);
    if (html == NULL)
        PANIC;
    strcat(html, endHtml);
    json_t *json = json_pack("{ssssssssssssss}", "to", emailAddresses, "subject",
                             "EZproxy Security Rule Incident Digest", "fromEmail",
                             "noreply@oclc.org", "fromName", "OCLCSupport (no replies)", "replyTo",
                             "noreply@oclc.org", "text", html, "optOut",
                             "Contact your EZproxy Administrator to stop receiving these emails.");
    FreeThenNull(html);
    char *jsonEmailMessage = NULL;
    jsonEmailMessage = json_dumps(json, 0);
    json_decref(json);
    SendEmailNotification(emailAddresses, jsonEmailMessage);
    FreeThenNull(jsonEmailMessage);
    FreeThenNull(emailAddresses);
}

void AdminSecurityTripped(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    const int ASTRIPPEDID = 0;
    const int ASRULEID = 1;
    const int ASLOGGED = 2;
    const int ASEXPIRED = 3;
    const int ASBLOCKED = 4;
    const int ASEXEMPT = 5;

    struct FORMFIELD ffs[] = {
        {"trippedid", NULL, 0, 0, 18},
        {"ruleid", NULL, 0, 0, 18},
        {"logged", NULL, 0, 0, 1},
        {"expired", NULL, 0, 0, 1},
        {"blocked", NULL, 0, 0, 1},
        {"exempt", NULL, 0, 0, 1},
        {NULL, NULL, 0, 0}
    };

    // clang-format off
    const char *template =
        "SELECT"
            " tripped.id AS \"all\","
            " DATETIME(tripped.at, 'unixepoch', 'localtime') AS \"When\","
            " rule.rulename AS \"Rule\","
            " tripped.user AS \"User\","
            " KMGT(boundary) AS \"Limit\","
            " KMGT(value) AS \"Observed\","
            " CASE"
                " WHEN expires = 0 THEN 'Logged'"
                " WHEN expires = EXEMPT() THEN 'Exempt'"
                " WHEN expires = NEVER() THEN 'Never'"
                " ELSE DATETIME(expires, 'unixepoch', 'localtime')"
            " END AS \"Expires\""
        " FROM"
            " tripped"
            " INNER JOIN rule ON rule.id = tripped.ruleid"
        " %s"
        " ORDER BY"
            " tripped.at, tripped.ruleid, user";
    // clang-format on

    char sql[2048];
    sqlite3 *db = SecurityGetSharedDatabase();
    char *ruleid;
    char *rulename = NULL;
    int rows, cols;
    char **result;
    char *errMsg = NULL;
    int i;
    int j;
    time_t now;
    char nows[MAXASCIIDATE];
    BOOL logged, expired, blocked, exempt;
    char constraint[512];
    char *conj = "";
    const char *nameSql = "SELECT rulename FROM rule WHERE id = :ruleid";
    sqlite3_stmt *nameStmt;

    if (AdminSecurityTrippedExpire(t, post)) {
        return;
    }

    FindFormFields(query, NULL, ffs);

    if (AdminSecurityTrippedEvidence(t, ffs[ASTRIPPEDID].value)) {
        return;
    }

    constraint[0] = 0;

    ruleid = ffs[ASRULEID].value;
    if (ruleid) {
        SQLPrepare(db, nameSql, &nameStmt);
        SQLOK(SQLBindTextByName(nameStmt, ":ruleid", ruleid, SQLITE_STATIC));
        int res = sqlite3_step(nameStmt);
        if (res != SQLITE_ROW) {
            SQLDONE(res);
            ruleid = NULL;
        } else {
            if (!(rulename = strdup(sqlite3_column_text(nameStmt, 0))))
                PANIC;
            sprintf(constraint, " WHERE ruleid = %s", ruleid);
        }
        sqlite3_finalize(nameStmt);
    }

    logged = ffs[ASLOGGED].value != NULL;
    expired = ffs[ASEXPIRED].value != NULL;
    blocked = ffs[ASBLOCKED].value != NULL;
    exempt = ffs[ASEXEMPT].value != NULL;

    if (!(logged || expired || blocked)) {
        blocked = TRUE;
        logged = expired = ruleid != NULL;
    }

    UUtime(&now);
    ASCIIDate(&now, nows);
    if (!(logged && expired && blocked && exempt)) {
        if (constraint[0]) {
            strcat(constraint, " AND");
        } else {
            strcat(constraint, " WHERE");
        }
        strcat(constraint, " (");
        conj = "";
        if (logged) {
            sprintf(AtEnd(constraint), "%s tripped.expires = 0", conj);
            conj = " OR ";
        }
        if (exempt) {
            sprintf(AtEnd(constraint), "%s tripped.expires = 1", conj);
            conj = " OR ";
        }
        if (expired) {
            sprintf(AtEnd(constraint),
                    "%s (tripped.expires > 1 AND tripped.expires <= %" PRIdMAX ")", conj, now);
            conj = " OR ";
        }
        if (blocked) {
            sprintf(AtEnd(constraint), "%s tripped.expires > %" PRIdMAX, conj, now);
            conj = " OR ";
        }
        strcat(constraint, ")");
    }

    AdminBaseHeader(t, AHSORTABLE, "Tripped Security Rules", " | <a href='/security'>Security</a>");

    WriteLine(s,
              "<form action='/security/tripped' method='get'>\n"
              "<style>label { margin-right: 0.5em; }</style>"
              "<label>Include:</label> ");
    WriteLine(s, "<label><input name='logged' type='checkbox' %s> Logged</label> ",
              logged ? "checked" : "");
    WriteLine(s, "<label><input name='expired' type='checkbox' %s> Expired</label> ",
              expired ? "checked" : "");
    WriteLine(s, "<label><input name='blocked' type='checkbox' %s> Blocked</label> ",
              blocked ? "checked" : "");
    WriteLine(s, "<label><input name='exempt' type='checkbox' %s> Exempt</label> ",
              exempt ? "checked" : "");
    if (ruleid != NULL) {
        WriteLine(s, "<input type='hidden' name='ruleid' value='%s'/>\n", ruleid);
    }
    WriteLine(s,
              "<input type='submit' value='Update'/>\n"
              "</form>\n");
    snprintf(sql, sizeof(sql), template, constraint);
    SQLOK(sqlite3_get_table(db, sql, &result, &rows, &cols, &errMsg));

    if (rows == 0) {
        WriteLine(s, "<p>No security rules under these constraints are tripped");
        if (rulename) {
            WriteLine(s, " for ");
            SendHTMLEncoded(s, rulename);
        }
        WriteLine(s, "</p>\n");
    } else {
        WriteLine(s, "<form method='post'>\n");
        WriteLine(s, "<table id='security-tripped' class='bordered-table sortable'\n");
        const char *td = "th";
        const char *tda = " scope='col'";
        for (i = 0; i <= rows; i++) {
            WriteLine(s, "<tr>\n");
            WriteLine(s, "<%s%s%s>", td, tda, (i == 0 ? " class='nosort'" : ""));
            char *when = result[i * cols + 6];
            if (i == 0 || *when == 'N' || (isdigit(*when) && strcmp(nows, when) < 0)) {
                WriteLine(s, "<input type='checkbox' name='exp' value='%s'/>", result[i * cols]);
            }
            WriteLine(s, "</%s>", td);
            for (j = 1; j < cols; j++) {
                BOOL isHeader = i == 0;
                BOOL isNumeric = j == 4 || j == 5;
                BOOL isBoundary = i > 0 && j == 5;
                WriteLine(s, "<%s%s>", td,
                          isNumeric
                              ? isHeader ? " class='sortnumeric'" : " style='text-align: right'"
                              : "");
                if (isBoundary) {
                    WriteLine(s, "<a href='/security/tripped?trippedid=%s'>", result[i * cols]);
                }
                SendHTMLEncoded(s, result[i * cols + j]);
                if (isBoundary) {
                    WriteLine(s, "</a>");
                }
                WriteLine(s, "</%s>", td);
            }
            WriteLine(s, "</tr>\n");
            td = "td";
            tda = "";
        }

        WriteLine(s, "</table>\n");
        WriteLine(s, "<p><input type='submit' value='Expire checked'/></p>\n");
        WriteLine(s, "</form>\n");
        WriteLine(s,
                  "<script>"
                  "(function() {\n"
                  "var exp = document.getElementsByName('exp');\n"
                  "var all;\n"
                  "var i;\n"
                  "for (i = 0; i < exp.length; i++) {\n"
                  "if (exp[i].value === 'all') {\n"
                  "all = exp[i];\n"
                  "}\n"
                  "}\n"
                  "all.addEventListener('click', function(el) {\n"
                  "var i;\n"
                  "for (i = 0; i < exp.length; i++) {\n"
                  "if (exp[i].checked !== all.checked) {\n"
                  "exp[i].checked = all.checked;\n"
                  "}\n"
                  "}\n"
                  "});\n"
                  "})();\n"
                  "</script>\n");
    }

    sqlite3_free_table(result);
    AdminFooter(t, 0);
    FreeThenNull(rulename);
}

static void AdminSecurityConfirmForm(struct TRANSLATE *t, char *path, char *name) {
    struct UUSOCKET *s = &t->ssocket;

    WriteLine(s, "<form method='/security/%s'>\n", path);
    WriteLine(s, "<p>%s can take an extended period of time.</p>", name);
    UUSendZ(s, "<p>To proceed, <label>type CONFIRM in this box\n");
    UUSendZ(s, "<input type='text' name='confirm' size='8' maxlength='8'></label> then click\n");
    UUSendZ(s, "<input type='submit' value='submit'></p></form>\n");
}

void AdminSecurityPurge(struct TRANSLATE *t, char *query, char *post) {
    const int ASCONFIRM = 0;
    struct FORMFIELD ffs[] = {
        {"confirm", NULL, 0, 0, 8},
        {NULL, NULL, 0, 0}
    };

    FindFormFields(query, post, ffs);

    if (ffs[ASCONFIRM].value && stricmp(ffs[ASCONFIRM].value, "confirm") == 0) {
        securityForcePurge = TRUE;
        // Allow enough time for security to notice that a forced purge has been requested and start
        // acting on it so the user will see that the purge is running once redirected back to the
        // main security page.
        SubSleep(0, 500);
        LocalRedirect(t, "/security", 302, 0, NULL);
        return;
    }

    AdminSecurityHeader(t, 0, "Purge Security Database");
    AdminSecurityConfirmForm(t, "/security/purge", "Purge");
}

void AdminSecurityVacuum(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    const int ASCONFIRM = 0;
    struct FORMFIELD ffs[] = {
        {"confirm", NULL, 0, 0, 8},
        {NULL, NULL, 0, 0}
    };

    FindFormFields(query, post, ffs);

    if (ffs[ASCONFIRM].value && stricmp(ffs[ASCONFIRM].value, "confirm") == 0) {
        securityForceVacuum = TRUE;
        // Allow enough time for security to notice that a forced vacuum has been requested and
        // start acting on it so the user will see that the purge is running once redirected back to
        // the main security page.
        SubSleep(0, 500);
        LocalRedirect(t, "/security", 302, 0, NULL);
        return;
    }

    AdminSecurityHeader(t, 0, "Vacuum Security Database");
    AdminSecurityConfirmForm(t, "/security/vacuum", "Vacuum");
}

void AdminSecurity(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    char dateBuffer[MAXASCIIDATE];

    AdminHeader(t, 0, "View Security Rules");

    WriteLine(s, "<p>\n");
    WriteLine(s, "<a href='/security/rules'>Security Rules</a><br/>\n");
    WriteLine(s, "<a href='/security/exemptions'>Security Exemptions</a><br/>\n");
    WriteLine(s, "<a href='/security/tripped'>Tripped Security Rules</a><br/>\n");
    WriteLine(s,
              "<a href='/security/notifications'>Tripped Security Rules Notifications</a><br/>\n");
    WriteLine(s, "<a href='/security/purge'>Purge Security Database</a><br/>");
    WriteLine(s, "<a href='/security/vacuum'>Vacuum Security Database</a><br/>");
    WriteLine(s, "</p>\n");

    if (securityPurgeStarted) {
        ASCIIDate(&securityPurgeStarted, dateBuffer),
            WriteLine(s, "<p>Security purge has been running since %s</p>\n", dateBuffer);
    } else if (securityForcePurge) {
        WriteLine(s, "<p>Security purge pending</p>\n");
    }

    if (securityVacuumStarted) {
        ASCIIDate(&securityVacuumStarted, dateBuffer),
            WriteLine(s, "<p>Security vacuum has been running since %s</p>\n", dateBuffer);
    } else if (securityForceVacuum) {
        WriteLine(s, "<p>Security vacuum pending</p>\n");
    }

    AdminFooter(t, 0);
}
