#define __UUFILE__ "efgeneral.c"

#define EXPRESSION_INTERNAL_FUNCTIONS

#include "common.h"
#include "usr.h"
#include "usrfinduser.h"
#include "expression.h"
#include "variables.h"
#include "efgeneral.h"
#include "efparsename.h"
#include "efrereplace.h"
#include "usertoken.h"
#include <math.h>

static char *ExpressionFuncStoredCookie(struct EXPRESSIONCTX *ec) {
    // short circuit out if any needed data is null
    if (!ec || !ec->t || !ec->t->session || !ec->t->session->cookies)
        return NULL;

    char *domain = ExpressionExpression(ec, TRUE);

    if (!ExpressionCheckToken(ec, COMMA)) {
        FreeThenNull(domain);
        return NULL;
    }

    char *name = ExpressionExpression(ec, TRUE);

    if (!name || !domain)
        return NULL;

    struct COOKIE *k;
    for (k = ec->t->session->cookies; k; k = k->next) {
        if (k->name == NULL || k->domain == NULL)
            continue;

        char *pos = strchr(k->name, '=');
        if (pos && stricmp(k->domain, domain) == 0 && strnicmp(name, k->name, strlen(name)) == 0 &&
            strnicmp(k->name, name, pos - k->name) == 0) {
            FreeThenNull(domain);
            FreeThenNull(name);
            return ExpressionStrdup(++pos);
        }
    }

    FreeThenNull(domain);
    FreeThenNull(name);
    return NULL;
}

static BOOL ExpressionFuncZeroArgs(struct EXPRESSIONCTX *ec) {
    ExpressionGetToken(ec);
    return ExpressionCheckToken(ec, RHPAREN);
}

static char *ExpressionFuncRewriteURL(struct EXPRESSIONCTX *ec) {
    char *inVal = ExpressionExpression(ec, TRUE);
    char *val = NULL;
    if (inVal) {
        if (!(val = malloc(strlen(inVal) + 256)))
            PANIC;
        strcpy(val, inVal);
        EditURL(ec->t, val);
        if (!(val = realloc(val, strlen(val) + 1)))
            PANIC;
    }
    FreeThenNull(inVal);
    return val;
}

static char *ExpressionFuncUnrewriteURL(struct EXPRESSIONCTX *ec) {
    char *inVal = ExpressionExpression(ec, TRUE);
    char *val = NULL;
    if (inVal) {
        if (!(val = malloc(strlen(inVal) + 256)))
            PANIC;
        strcpy(val, inVal);
        ReverseEditURL(ec->t, val, NULL);
        if (!(val = realloc(val, strlen(val) + 1)))
            PANIC;
    }
    FreeThenNull(inVal);
    return val;
}

static char *ExpressionFuncRequestURL(struct EXPRESSIONCTX *ec) {
    if (!ExpressionFuncZeroArgs(ec))
        return NULL;

    char *val = NULL;
    struct TRANSLATE *t = ec->t;

    if (t->host && t->host->myDb) {
        PORT defaultPort = 0;
        // Protocol can be up to 7, port up to 6, null null at end 1
        if (!(val = malloc(20 + strlen(t->host->hostname) + strlen(t->urlCopy))))
            PANIC;
        if (t->host->ftpgw) {
            strcpy(val, "ftp");
            defaultPort = 21;
        } else {
            if (t->host->useSsl) {
                strcpy(val, "https");
                defaultPort = 443;
            } else {
                strcpy(val, "http");
                defaultPort = 80;
            }
        }
        sprintf(AtEnd(val), "://%s", t->host->hostname);
        if (t->host->remport != defaultPort) {
            sprintf(AtEnd(val), ":%d", t->host->remport);
        }
        strcat(val, t->urlCopy);
    }

    return val;
}

static char *ExpressionFuncValidStartingPointURLTarget(struct EXPRESSIONCTX *ec) {
    char *url = ExpressionExpression(ec, TRUE);
    BOOL val = FALSE;
    int verifyHost2Result;
    struct DATABASE *b;
    BOOL isMe;

    if (url) {
        verifyHost2Result = VerifyHost2(url, NULL, &b, NULL, &isMe);
        val = b != NULL || isMe;
    }

    FreeThenNull(url);
    return ExpressionBoolean(val);
}

static char *ExpressionFuncCompareIP(struct EXPRESSIONCTX *ec) {
    char *val = NULL;
    char *ip1 = ExpressionExpression(ec, TRUE);
    if (ExpressionCheckToken(ec, COMMA)) {
        char *ip2 = ExpressionExpression(ec, TRUE);
        if (ip1 && ip2) {
            struct sockaddr_storage ip1addr, ip2addr;
            if (inet_pton_st2(ip1, &ip1addr) > 0 && inet_pton_st2(ip2, &ip2addr) > 0) {
                if (ip1addr.ss_family == ip2addr.ss_family) {
                    int result = compare_ip(&ip1addr, &ip2addr);
                    if (result < 0) {
                        result = -1;
                    }
                    if (result > 0) {
                        result = 1;
                    }
                    val = ExpressionInt(result);
                }
            }
        }
        FreeThenNull(ip2);
    }
    FreeThenNull(ip1);

    return val;
}

static char *ExpressionFuncSessionCount(struct EXPRESSIONCTX *ec) {
    char *user = ExpressionExpression(ec, TRUE);
    char *val = NULL;
    int i;
    struct SESSION *e;
    int count = 0;

    if (user) {
        for (i = 0, e = sessions; i < cSessions; i++, e++) {
            if (e->active != 1 || e->userLimitExceeded) {
                continue;
            }

            if (*user) {
                if (e->logUserFull == NULL || stricmp(e->logUserFull, user) != 0) {
                    continue;
                }
            }

            count++;
        }

        val = ExpressionInt(count);
    }

    return val;
}

static char *ExpressionFuncAuthenticated(struct EXPRESSIONCTX *ec) {
    if (!ExpressionFuncZeroArgs(ec))
        return NULL;

    return ExpressionBoolean(ec->uip->result == resultValid);
}

static char *ExpressionFuncChr(struct EXPRESSIONCTX *ec) {
    char *inVal = ExpressionExpression(ec, TRUE);
    char *val = NULL;

    if (inVal) {
        int i = atoi(inVal);
        if (i > 0 && i < 255) {
            if (!(val = (char *)malloc(2)))
                PANIC;
            *val = i;
            *(val + 1) = 0;
        }
        FreeThenNull(inVal);
    }

    return val;
}

static char *ExpressionFuncOrd(struct EXPRESSIONCTX *ec) {
    unsigned char *inVal = (unsigned char *)ExpressionExpression(ec, TRUE);
    char *val = NULL;

    if (inVal) {
        if (!(val = (char *)malloc(4)))
            PANIC;
        sprintf(val, "%d", *inVal);
        FreeThenNull(inVal);
    }

    return val;
}

static char *ExpressionFuncURLEncode(struct EXPRESSIONCTX *ec) {
    char *inVal = ExpressionExpression(ec, TRUE);
    char *val = NULL;

    if (inVal) {
        /* Worst case, every character has to be %xx escaped, taking 3 times the space */
        if (!(val = malloc(strlen(inVal) * 3 + 1)))
            PANIC;
        *val = 0;
        AddEncodedField(val, inVal, NULL);
        FreeThenNull(inVal);
        if (!(val = realloc(val, strlen(val) + 1)))
            PANIC;
    }

    return val;
}

static char *ExpressionFuncHash(struct EXPRESSIONCTX *ec) {
    char *hashFunc = NULL;
    char *toHash = NULL;
    char *val = NULL;
    BOOL upper = TRUE;
    EVP_MD_CTX *mdctx = NULL;

    hashFunc = ExpressionExpression(ec, TRUE);
    if (ExpressionCheckToken(ec, COMMA)) {
        char *toHash = ExpressionExpression(ec, TRUE);
        if (toHash) {
            unsigned char md_value[EVP_MAX_MD_SIZE];
            unsigned int md_len;

            if (ec->tokenType == COMMA) {
                char *upperVal = ExpressionExpression(ec, TRUE);
                if (ExpressionValueTrue(upperVal))
                    upper = TRUE;
                FreeThenNull(upperVal);
            }

            if (!(mdctx = EVP_MD_CTX_new()))
                PANIC;
            if (strcmp(hashFunc, "MD5") == 0)
                EVP_DigestInit(mdctx, EVP_md5());
            else if (strcmp(hashFunc, "SHA1") == 0)
                EVP_DigestInit(mdctx, EVP_sha1());
            else if (strcmp(hashFunc, "SHA256") == 0)
                EVP_DigestInit(mdctx, EVP_sha256());
            else if (strcmp(hashFunc, "SHA512") == 0)
                EVP_DigestInit(mdctx, EVP_sha512());
            else {
                UsrLog(ec->uip,
                       "Hash functions supported MD5, SHA1, SHA256, SHA512; invalid provided: %s",
                       hashFunc);
                goto Finished;
            }
            EVP_DigestUpdate(mdctx, (unsigned char *)toHash, strlen(toHash));
            EVP_DigestFinal(mdctx, md_value, &md_len);
            val = DigestToHex(NULL, md_value, md_len, upper);
        }
    }

Finished:
    if (mdctx) {
        EVP_MD_CTX_free(mdctx);
        mdctx = NULL;
    }
    FreeThenNull(hashFunc);
    FreeThenNull(toHash);
    return val;
}

static char *ExpressionFuncHTMLEncode(struct EXPRESSIONCTX *ec) {
    char *inVal = ExpressionExpression(ec, TRUE);
    char *val = NULL;
    char *src, *dst;

    if (inVal) {
        /* Worst case, every character has to be &#123; escaped, taking 6 times the space */
        if (!(val = malloc(strlen(inVal) * 6 + 1)))
            PANIC;
        for (src = inVal, dst = val; *src; src++) {
            if (*src == '<' || *src == '>' || *src == '&' || *src == '"' || *src == '\'') {
                sprintf(dst, "&#%d;", *src);
                dst = strchr(dst, 0);
            } else {
                *dst++ = *src;
            }
        }
        *dst = 0;
        FreeThenNull(inVal);
        if (!(val = realloc(val, strlen(val) + 1)))
            PANIC;
    }

    return val;
}

static char *ExpressionFuncInvalid(struct EXPRESSIONCTX *ec) {
    if (!ExpressionFuncZeroArgs(ec))
        return NULL;

    return ExpressionBoolean(ec->uip->result == resultInvalid);
}

static char *ExpressionFuncExpired(struct EXPRESSIONCTX *ec) {
    if (!ExpressionFuncZeroArgs(ec))
        return NULL;

    return ExpressionBoolean(ec->uip->result == resultExpired);
}

static char *ExpressionFuncRefused(struct EXPRESSIONCTX *ec) {
    if (!ExpressionFuncZeroArgs(ec))
        return NULL;

    return ExpressionBoolean(ec->uip->result == resultRefused);
}

static char *ExpressionFuncCountry(struct EXPRESSIONCTX *ec) {
    if (!ExpressionFuncZeroArgs(ec))
        return NULL;

    FindLocationByTranslate(ec->t);
    return ExpressionStrdup(ec->t->location.countryCode);
}

static char *ExpressionFuncCountryName(struct EXPRESSIONCTX *ec) {
    if (!ExpressionFuncZeroArgs(ec))
        return NULL;

    FindLocationByTranslate(ec->t);
    return ExpressionStrdup(ec->t->location.countryName);
}

static char *ExpressionFuncRegion(struct EXPRESSIONCTX *ec) {
    if (!ExpressionFuncZeroArgs(ec))
        return NULL;

    FindLocationByTranslate(ec->t);
    return ExpressionStrdup(ec->t->location.region);
}

static char *ExpressionFuncCity(struct EXPRESSIONCTX *ec) {
    if (!ExpressionFuncZeroArgs(ec))
        return NULL;

    FindLocationByTranslate(ec->t);
    return ExpressionStrdup(ec->t->location.city);
}

static char *ExpressionFuncLatitude(struct EXPRESSIONCTX *ec) {
    if (!ExpressionFuncZeroArgs(ec))
        return NULL;

    FindLocationByTranslate(ec->t);
    return ExpressionDouble(ec->t->location.latitude);
}

static char *ExpressionFuncLongitude(struct EXPRESSIONCTX *ec) {
    if (!ExpressionFuncZeroArgs(ec))
        return NULL;

    FindLocationByTranslate(ec->t);
    return ExpressionDouble(ec->t->location.longitude);
}

static double DegreesToRadians(double d) {
    const static double to_rad = (3.1415926536 / 180.0);
    return d * to_rad;
}

static char *ExpressionFuncLatLongDistance(struct EXPRESSIONCTX *ec) {
    char *th1 = NULL;
    char *ph1 = NULL;
    char *th2 = NULL;
    char *ph2 = NULL;
    char *defaultDistance = NULL;
    char *val = NULL;
    double rth1, rth2, rph, dz, dx, dy, d;
    static const double earth_radius_km = 6371.0;
    const double earth_radius_m = earth_radius_km / 1.609344;

    th1 = ExpressionExpression(ec, TRUE);
    if (ExpressionCheckToken(ec, COMMA)) {
        ph1 = ExpressionExpression(ec, TRUE);
        if (ExpressionCheckToken(ec, COMMA)) {
            defaultDistance = ExpressionExpression(ec, TRUE);
            if (ec->tokenType == COMMA) {
                th2 = defaultDistance;
                defaultDistance = NULL;
                ph2 = ExpressionExpression(ec, TRUE);
            } else {
                FindLocationByTranslate(ec->t);
                if (ec->t->location.latitude == 0 && ec->t->location.longitude == 0) {
                    val = defaultDistance;
                    defaultDistance = NULL;
                } else {
                    th2 = ExpressionDouble(ec->t->location.latitude);
                    ph2 = ExpressionDouble(ec->t->location.longitude);
                }
            }
        }
    }

    if (val == NULL && ph2 != NULL) {
        // Following based on Haversine function at http://rosettacode.org/wiki/Haversine_formula#C
        rth1 = DegreesToRadians(atof(th1));
        rth2 = DegreesToRadians(atof(th2));
        rph = DegreesToRadians(atof(ph1) - atof(ph2));
        dz = sin(rth1) - sin(rth2);
        dx = cos(rph) * cos(rth1) - cos(rth2);
        dy = sin(rph) * cos(rth1);
        d = asin(sqrt(dx * dx + dy * dy + dz * dz) / 2) * 2 * earth_radius_m;
        val = ExpressionDouble(d);
    }

    FreeThenNull(th1);
    FreeThenNull(ph1);
    FreeThenNull(defaultDistance);
    FreeThenNull(th2);
    FreeThenNull(ph2);
    return val;
}

static char *ExpressionFuncIP(struct EXPRESSIONCTX *ec) {
    char *val = NULL;

    if (!ExpressionFuncZeroArgs(ec))
        return NULL;

    if (!(val = malloc(INET6_ADDRSTRLEN)))
        PANIC;
    ipToStr(&ec->t->sin_addr, val, INET6_ADDRSTRLEN);
    return val;
}

static char *ExpressionFuncNull(struct EXPRESSIONCTX *ec) {
    char *val = NULL;

    ExpressionGetToken(ec);
    if (ExpressionCheckToken(ec, NAME)) {
        char *var = NULL;
        char *index = NULL;
        char *checkVal = NULL;

        if (!(var = strdup(ec->token)))
            PANIC;
        ExpressionGetToken(ec);
        if (ec->tokenType == LHBRACKET) {
            index = ExpressionExpression(ec, TRUE);
            if (ExpressionCheckToken(ec, RHBRACKET))
                ExpressionGetToken(ec);
        }

        checkVal = ExpressionVariable(ec, var, index, FALSE);
        val = ExpressionBoolean(checkVal == NULL);

        FreeThenNull(checkVal);
        FreeThenNull(var);
        FreeThenNull(index);
    }
    return val;
}

static BOOL ExpressionAggregateTestSimple(struct USERINFO *uip,
                                          char *testVal,
                                          enum AGGREGATETESTOPTIONS ato,
                                          char *realVal,
                                          struct VARIABLES *rev) {
    struct AGGREGATETESTCONTEXT atc;

    if (ExpressionAggregateTestInitialize(&atc, uip, testVal, ato)) {
        ExpressionAggregateTestValue(&atc, realVal);
        return ExpressionAggregateTestTerminate(&atc, rev);
    }

    return FALSE;
}

static char *ExpressionAggregateTest(struct EXPRESSIONCTX *ec, enum AGGREGATETESTOPTIONS ato) {
    char *val = NULL;
    char *var = NULL;
    char *testVal = NULL;

    ExpressionGetToken(ec);
    if (ExpressionCheckToken(ec, NAME)) {
        char *testVal = NULL;

        if (!(var = strdup(ec->token)))
            PANIC;
        ExpressionGetToken(ec);

        if (ato != ATOCOUNT) {
            if (ExpressionCheckToken(ec, COMMA))
                testVal = ExpressionExpression(ec, TRUE);
        } else {
            testVal = NULL;
        }

        if (ec->invalidMsg == NULL) {
            int result = 0;
            char *ns;
            if ((ns = StartsWith(var, "auth:")) && *ns) {
                if (ec->uip->authAggregateTest) {
                    result = (ec->uip->authAggregateTest)(ec->t, ec->uip, ec->context, ns, testVal,
                                                          ato, ec->t->localVariables);
                } else if (ec->uip->authGetValue) {
                    char *realVal = (ec->uip->authGetValue)(ec->t, ec->uip, ec->context, ns, NULL);
                    result = ExpressionAggregateTestSimple(ec->uip, testVal, ato, realVal,
                                                           ec->t->localVariables);
                    FreeThenNull(realVal);
                }
            } else {
                result = VariablesAggregateTest(
                    ec->uip, ec->t->localVariables,
                    (ec->t->session ? ec->t->session->sessionVariables : NULL), var, testVal, ato,
                    ec->t->localVariables);
            }

            if (ato == ATOCOUNT)
                val = ExpressionInt(result);
            else
                val = ExpressionBoolean(result);
        }
    }

    FreeThenNull(testVal);
    FreeThenNull(var);

    return val;
}

static char *ExpressionFuncAll(struct EXPRESSIONCTX *ec) {
    return ExpressionAggregateTest(ec, ATOALLEQ);
}

static char *ExpressionFuncAllRE(struct EXPRESSIONCTX *ec) {
    return ExpressionAggregateTest(ec, ATOALLRE);
}

static char *ExpressionFuncAllXMLRE(struct EXPRESSIONCTX *ec) {
    return ExpressionAggregateTest(ec, ATOALLXMLRE);
}

static char *ExpressionFuncAllWild(struct EXPRESSIONCTX *ec) {
    return ExpressionAggregateTest(ec, ATOALLWILD);
}

static char *ExpressionFuncAny(struct EXPRESSIONCTX *ec) {
    return ExpressionAggregateTest(ec, ATOANYEQ);
}

static char *ExpressionFuncAnyRE(struct EXPRESSIONCTX *ec) {
    return ExpressionAggregateTest(ec, ATOANYRE);
}

static char *ExpressionFuncAnyXMLRE(struct EXPRESSIONCTX *ec) {
    return ExpressionAggregateTest(ec, ATOANYXMLRE);
}

static char *ExpressionFuncAnyWild(struct EXPRESSIONCTX *ec) {
    return ExpressionAggregateTest(ec, ATOANYWILD);
}

static char *ExpressionFuncCount(struct EXPRESSIONCTX *ec) {
    return ExpressionAggregateTest(ec, ATOCOUNT);
}

static char *ExpressionFuncHTMLOptions(struct EXPRESSIONCTX *ec) {
    char *val = NULL;
    char *var = NULL;
    char *defaultVal = NULL;

    ExpressionGetToken(ec);
    if (ExpressionCheckToken(ec, NAME)) {
        if (!(var = strdup(ec->token)))
            PANIC;
        ExpressionGetToken(ec);

        if (ec->tokenType == COMMA) {
            defaultVal = ExpressionExpression(ec, TRUE);
        }

        val = VariablesHTMLOptions(ec->t->localVariables, var, defaultVal);
    }

    FreeThenNull(defaultVal);
    FreeThenNull(var);

    return val;
}

static char *ExpressionFuncLength(struct EXPRESSIONCTX *ec) {
    char *str = ExpressionExpression(ec, TRUE);
    char *val = ExpressionInt(str ? strlen(str) : 0);
    FreeThenNull(str);
    return val;
}

static char *ExpressionFuncLTrim(struct EXPRESSIONCTX *ec) {
    char *val = ExpressionExpression(ec, TRUE);
    Trim(val, TRIM_LEAD);
    return val;
}

static char *ExpressionFuncRTrim(struct EXPRESSIONCTX *ec) {
    char *val = ExpressionExpression(ec, TRUE);
    Trim(val, TRIM_TRAIL);
    return val;
}

static char *ExpressionFuncTrim(struct EXPRESSIONCTX *ec) {
    char *val = ExpressionExpression(ec, TRUE);
    Trim(val, TRIM_LEAD | TRIM_TRAIL);
    return val;
}

static char *ExpressionFuncCompress(struct EXPRESSIONCTX *ec) {
    char *val = ExpressionExpression(ec, TRUE);
    Trim(val, TRIM_COMPRESS);
    return val;
}

static char *ExpressionFuncToken(struct EXPRESSIONCTX *ec) {
    char *val = NULL;
    char *str = NULL;
    char *delim = NULL;
    char *index = NULL;
    char *s;
    char *p;
    char *q;

    str = ExpressionExpression(ec, TRUE);
    if (ExpressionCheckToken(ec, COMMA)) {
        delim = ExpressionExpression(ec, TRUE);
        if (ExpressionCheckToken(ec, COMMA)) {
            index = ExpressionExpression(ec, TRUE);
            if (str && delim && index) {
                int i = atoi(index);
                s = p = str;
                for (; *p; p++) {
                    for (q = delim; *q; q++) {
                        if (*p == *q) {
                            *p = 0;
                            i--;
                            if (i < 0)
                                goto BreakOuter;
                            s = p + 1;
                            break;
                        }
                    }
                }
            BreakOuter:
                /* If we have a string, move it to the front of the str variable;
                   but if not, change to zero-length string
                */
                if (i <= 0)
                    StrCpyOverlap(str, s);
                else
                    *str = 0;

                val = UUStrRealloc(str);
                str = NULL;
            }
        }
    }

    FreeThenNull(index);
    FreeThenNull(delim);
    FreeThenNull(str);

    return val;
}

static char *ExpressionFuncUpper(struct EXPRESSIONCTX *ec) {
    char *val = ExpressionExpression(ec, TRUE);
    AllUpper(val);
    return val;
}

static char *ExpressionFuncLower(struct EXPRESSIONCTX *ec) {
    char *val = ExpressionExpression(ec, TRUE);
    AllLower(val);
    return val;
}

enum EXPRESSIONFUNCUSERTYPE { EXPRESSIONFUNCUSERFILE, EXPRESSIONFUNCUSERSTRING };

static char *ExpressionFuncUserFileString(struct EXPRESSIONCTX *ec,
                                          enum EXPRESSIONFUNCUSERTYPE expressionFuncUserType) {
    struct TRANSLATE *t = ec->t;
    struct USERINFO *uip = ec->uip;
    BOOL result = FALSE;
    char *val = ExpressionExpression(ec, TRUE);

    if (uip && uip->debug) {
        UsrLog(uip, "%s(\"%s\")",
               expressionFuncUserType == EXPRESSIONFUNCUSERFILE ? "UserFile" : "UserString",
               (val == NULL) ? "" : val);
    }

    if (uip && val && *val) {
        void (*saveAuthSetValue)(struct TRANSLATE *t, struct USERINFO *uip, void *context,
                                 const char *var, const char *index, const char *val) =
            uip->authSetValue;
        char *(*saveAuthSetAggregateValue)(struct TRANSLATE *t, struct USERINFO *uip, void *context,
                                           const char *var, const char *val) =
            uip->authSetAggregateValue;
        char *(*saveAuthGetValue)(struct TRANSLATE *t, struct USERINFO *uip, void *context,
                                  const char *var, const char *index) = uip->authGetValue;
        int (*saveAuthAggregateTest)(struct TRANSLATE *t, struct USERINFO *uip, void *context,
                                     const char *var, const char *testVal,
                                     enum AGGREGATETESTOPTIONS ato, struct VARIABLES *rev) =
            uip->authAggregateTest;
        enum FINDUSERRESULT saveResult = uip->result;

        uip->authSetValue = NULL;
        uip->authSetAggregateValue = NULL;
        uip->authGetValue = NULL;
        uip->authAggregateTest = NULL;
        result = FindUser(t, uip,
                          expressionFuncUserType == EXPRESSIONFUNCUSERFILE ? val : "UserString()",
                          expressionFuncUserType == EXPRESSIONFUNCUSERSTRING ? val : NULL,
                          uip->fileInclusion, NULL, uip->isIntruder) > 0;
        uip->authGetValue = saveAuthGetValue;
        uip->authAggregateTest = saveAuthAggregateTest;
        uip->authSetValue = saveAuthSetValue;
        uip->authSetAggregateValue = saveAuthSetAggregateValue;
        uip->result = saveResult;
    }

    FreeThenNull(val);

    return ExpressionBoolean(result);
}

static char *ExpressionFuncUserFile(struct EXPRESSIONCTX *ec) {
    return ExpressionFuncUserFileString(ec, EXPRESSIONFUNCUSERFILE);
}

static char *ExpressionFuncUserString(struct EXPRESSIONCTX *ec) {
    return ExpressionFuncUserFileString(ec, EXPRESSIONFUNCUSERSTRING);
}

static char *ExpressionFuncGroupMember(struct EXPRESSIONCTX *ec) {
    char *val = ExpressionExpression(ec, TRUE);
    BOOL result;
    GROUPMASK gm = NULL;

    if (val == NULL)
        return NULL;

    gm = MakeGroupMask(NULL, val, 0, '=');
    result = GroupMaskOverlap(ec->uip->gm, gm);
    GroupMaskFree(&gm);
    FreeThenNull(val);

    return ExpressionBoolean(result);
}

static char *ExpressionFuncActiveGroupMember(struct EXPRESSIONCTX *ec) {
    char *val = ExpressionExpression(ec, TRUE);
    BOOL result = FALSE;
    GROUPMASK gm = NULL;

    if (val == NULL)
        return NULL;

    if (ec->t->session) {
        gm = MakeGroupMask(NULL, val, 0, '=');
        result = GroupMaskOverlap(ec->t->session->gm, gm);
        GroupMaskFree(&gm);
    }
    FreeThenNull(val);

    return ExpressionBoolean(result);
}

static char *ExpressionFuncNoGroups(struct EXPRESSIONCTX *ec) {
    if (!ExpressionFuncZeroArgs(ec))
        return NULL;

    return ExpressionBoolean(GroupMaskCardinality(ec->uip->gm) == 0);
}

static char *ExpressionFuncNoActiveGroups(struct EXPRESSIONCTX *ec) {
    if (!ExpressionFuncZeroArgs(ec))
        return NULL;

    return ExpressionBoolean(ec->t->session && GroupMaskCardinality(ec->t->session->gm) == 0);
}

static char *ExpressionFuncActiveSession(struct EXPRESSIONCTX *ec) {
    if (!ExpressionFuncZeroArgs(ec))
        return NULL;

    return ExpressionBoolean(ec->t->session != NULL);
}

static char *ExpressionFuncDateTime(struct EXPRESSIONCTX *ec) {
    struct tm tm;
    int i = 0;
    BOOL get = FALSE;

    ExpressionGetToken(ec);
    if (ec->tokenType == RHPAREN) {
        return ExpressionInt(UserInfoNow(ec->uip));
    }

    memset(&tm, 0, sizeof(tm));
    tm.tm_mday = 1;
    tm.tm_isdst = -1;

    do {
        int nextInt;
        char *nextVal = ExpressionExpression(ec, get);
        get = TRUE;
        if (nextVal == NULL) {
            ExpressionSetInvalid(ec, "expected number");
            break;
        }
        nextInt = atoi(nextVal);
        switch (i) {
        case 0:
            /* For 00-99, assume 2000-2099, which is 100-199 in tm.tm_year */
            if (nextInt < 100)
                tm.tm_year = nextInt + 100;
            else
                tm.tm_year = nextInt - 1900;
            break;
        case 1:
            tm.tm_mon = nextInt - 1;
            break;
        case 2:
            tm.tm_mday = nextInt;
            break;
        case 3:
            tm.tm_hour = nextInt;
            break;
        case 4:
            tm.tm_min = nextInt;
            break;
        case 5:
            tm.tm_sec = nextInt;
            break;
        }
        i++;
    } while (ec->tokenType == COMMA);

    return ExpressionInt(mktime(&tm));
}

static char *ExpressionFuncFormatDateTime(struct EXPRESSIONCTX *ec) {
    char *val = NULL;
    char *aWhen = NULL;
    char *format = ExpressionExpression(ec, TRUE);
    if (ExpressionCheckToken(ec, COMMA)) {
        char *aWhen = ExpressionExpression(ec, TRUE);
        if (format && aWhen) {
            time_t when = atoi(aWhen);
            size_t len = strlen(format) * 4 + 256;
            struct tm tm;

            if (!(val = calloc(1, len)))
                PANIC;
            UUlocaltime_r(&when, &tm);
            strftime(val, len, format, &tm);
            val = UUStrRealloc(val);
        }
    }
    FreeThenNull(aWhen);
    FreeThenNull(format);

    return val;
}

static char *ExpressionFuncQueryStringPass(struct EXPRESSIONCTX *ec) {
    if (!ExpressionFuncZeroArgs(ec))
        return NULL;

    return ExpressionBoolean(ec->uip->passFromQueryString);
}

static char *ExpressionFuncLeft(struct EXPRESSIONCTX *ec) {
    char *val = ExpressionExpression(ec, TRUE);
    if (ExpressionCheckToken(ec, COMMA)) {
        char *aLen = ExpressionExpression(ec, TRUE);
        int iLen = aLen ? atoi(aLen) : 0;
        if (val && iLen < (int)strlen(val))
            val[iLen] = 0;
        FreeThenNull(aLen);
    }
    return val;
}

static char *ExpressionFuncRight(struct EXPRESSIONCTX *ec) {
    char *val = ExpressionExpression(ec, TRUE);
    if (ExpressionCheckToken(ec, COMMA)) {
        char *aLen = ExpressionExpression(ec, TRUE);
        int iLen = aLen ? atoi(aLen) : 0;
        if (val && iLen < (int)strlen(val))
            StrCpyOverlap(val, val + strlen(val) - iLen);
        FreeThenNull(aLen);
    }
    return val;
}

static char *ExpressionFuncIndex(struct EXPRESSIONCTX *ec) {
    char *val = NULL;

    char *str = ExpressionExpression(ec, TRUE);
    if (ExpressionCheckToken(ec, COMMA)) {
        char *s;
        char *substr = ExpressionExpression(ec, TRUE);
        if (str && (s = strstr(str, substr ? substr : "")))
            val = ExpressionInt((s - str));
        else
            val = ExpressionInt(-1);
        FreeThenNull(substr);
    }
    FreeThenNull(str);

    return val;
}

static char *ExpressionFuncMid(struct EXPRESSIONCTX *ec) {
    char *val = ExpressionExpression(ec, TRUE);
    if (ExpressionCheckToken(ec, COMMA)) {
        char *aOffset = ExpressionExpression(ec, TRUE);
        int iOffset = aOffset ? atoi(aOffset) : 0;
        int iLen = val ? strlen(val) : 0;
        if (iLen < 0)
            iLen = 0;
        if (ec->tokenType == COMMA) {
            char *aLen = ExpressionExpression(ec, TRUE);
            iLen = aLen ? atoi(aLen) : 0;
        }
        if (iOffset > 0) {
            if (iOffset >= (int)strlen(val))
                *val = 0;
            else
                StrCpyOverlap(val, val + iOffset);
        }
        if (iLen < (int)strlen(val))
            val[iLen] = 0;
    }
    return val;
}

static char *ExpressionFuncJoin(struct EXPRESSIONCTX *ec) {
    char *val = NULL;
    char *delim = ExpressionExpression(ec, TRUE);

    while (ec->tokenType == COMMA) {
        char *joinVal = ExpressionExpression(ec, TRUE);
        if (joinVal) {
            char *newVal = NULL;
            if (*joinVal) {
                if (val) {
                    newVal = ExpressionCat(val, delim);
                    FreeThenNull(val);
                    val = newVal;
                }
                newVal = ExpressionCat(val, joinVal);
                FreeThenNull(val);
                FreeThenNull(joinVal);
                val = newVal;
                newVal = NULL;
            }
        }
    }

    FreeThenNull(delim);

    return val;
}

static char *ExpressionFuncCoalesce(struct EXPRESSIONCTX *ec) {
    char *val = NULL;

    do {
        char *nextVal = ExpressionExpression(ec, TRUE);
        if (val == NULL) {
            if (nextVal && *nextVal) {
                val = nextVal;
                nextVal = NULL;
            }
        }
        FreeThenNull(nextVal);
    } while (ec->tokenType == COMMA);

    return val;
}

static char *ExpressionFuncPPID(struct EXPRESSIONCTX *ec) {
    char *val = NULL;
    char *relyingPartyCode = ExpressionExpression(ec, TRUE);
    char *user = NULL;
    char argCount = 1;

    if (ec->tokenType == COMMA) {
        user = ExpressionExpression(ec, TRUE);
        argCount++;
    } else {
        if (ec->t && ec->t->session && ec->t->session->logUserBrief) {
            user = strdup(ec->t->session->logUserBrief);
        }
    }

    Trim(relyingPartyCode, TRIM_LEAD | TRIM_TRAIL | TRIM_CTRL);
    Trim(user, TRIM_LEAD | TRIM_TRAIL | TRIM_CTRL);

    if (relyingPartyCode == NULL || *relyingPartyCode == 0) {
        UsrLog(ec->uip, "PPID function requires a relying party code");
    } else if (user == NULL || *user == 0) {
        if (argCount == 1) {
            UsrLog(ec->uip, "PPID function does not have username to process");
        } else {
            UsrLog(ec->uip, "PPID function second argument is blank");
        }
    } else {
        AllLower(relyingPartyCode);
        AllLower(user);
        val = UserToken2(relyingPartyCode, user, TRUE);
    }

    FreeThenNull(relyingPartyCode);
    FreeThenNull(user);

    return val;
}

struct EXPRESSIONFUNC expressionFuncs[] = {
    {"Chr", ExpressionFuncChr},
    {"Ord", ExpressionFuncOrd},
    {"Length", ExpressionFuncLength},
    {"LTrim", ExpressionFuncLTrim},
    {"Left", ExpressionFuncLeft},
    {"Right", ExpressionFuncRight},
    {"Mid", ExpressionFuncMid},
    {"Index", ExpressionFuncIndex},
    {"RTrim", ExpressionFuncRTrim},
    {
     "Trim", ExpressionFuncTrim,
     },
    {
     "Token", ExpressionFuncToken,
     },
    {
     "Compress", ExpressionFuncCompress,
     },
    {"Join", ExpressionFuncJoin},
    {
     "Null", ExpressionFuncNull,
     },
    {
     "Hash", ExpressionFuncHash,
     },
    {
     "IP", ExpressionFuncIP,
     },
    {"Country", ExpressionFuncCountry},
    {"CountryName", ExpressionFuncCountryName},
    {"City", ExpressionFuncCity},
    {"Region", ExpressionFuncRegion},
    {"Latitude", ExpressionFuncLatitude},
    {"Longitude", ExpressionFuncLongitude},
    {"LatLongDistance", ExpressionFuncLatLongDistance},
    {"REReplace", ExpressionFuncREReplace},
    {"URLEncode", ExpressionFuncURLEncode},
    {"HTMLEncode", ExpressionFuncHTMLEncode},
    {"Upper", ExpressionFuncUpper},
    {"Lower", ExpressionFuncLower},
    {"All", ExpressionFuncAll},
    {"AllRE", ExpressionFuncAllRE},
    {"AllWild", ExpressionFuncAllWild},
    {"AllXMLRE", ExpressionFuncAllXMLRE},
    {"Any", ExpressionFuncAny},
    {"AnyRE", ExpressionFuncAnyRE},
    {"AnyWild", ExpressionFuncAnyWild},
    {"AnyXMLRE", ExpressionFuncAnyXMLRE},
    {"Count", ExpressionFuncCount},
    {"HTMLOptions", ExpressionFuncHTMLOptions},
    {"GroupMember", ExpressionFuncGroupMember},
    {"ActiveGroupMember", ExpressionFuncActiveGroupMember},
    {"NoGroups", ExpressionFuncNoGroups},
    {"NoActiveGroups", ExpressionFuncNoActiveGroups},
    {"ActiveSession", ExpressionFuncActiveSession},
    {"DateTime", ExpressionFuncDateTime},
    {"FormatDateTime", ExpressionFuncFormatDateTime},
    {"QueryStringPass", ExpressionFuncQueryStringPass},
    {"Authenticated", ExpressionFuncAuthenticated},
    {"Invalid", ExpressionFuncInvalid},
    {"Expired", ExpressionFuncExpired},
    {"Refused", ExpressionFuncRefused},
    {
     "UserFile", ExpressionFuncUserFile,
     },
    {
     "UserString", ExpressionFuncUserString,
     },
    {
     "ParseName", ExpressionFuncParseName,
     },
    {"Coalesce", ExpressionFuncCoalesce},
    {"StoredCookie", ExpressionFuncStoredCookie},
    {"RewriteURL", ExpressionFuncRewriteURL},
    {"UnrewriteURL", ExpressionFuncUnrewriteURL},
    {"RequestURL", ExpressionFuncRequestURL},
    {"ValidStartingPointURLTarget", ExpressionFuncValidStartingPointURLTarget},
    {"CompareIP", ExpressionFuncCompareIP},
    {"SessionCount", ExpressionFuncSessionCount},
    {"PPID", ExpressionFuncPPID},
    {NULL, NULL}
};
