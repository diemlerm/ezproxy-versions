#ifndef __ADMINSLIPTEST_H__
#define __ADMINSLIPTEST_H__

#include "common.h"

void AdminSlipTest(struct TRANSLATE *t, char *query, char *post);
BOOL AdminSlipTestReport(struct TRANSLATE *t);

#endif  // __ADMINSLIPTEST_H__
