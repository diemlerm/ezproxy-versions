#define __UUFILE__ "adminfmg.c"

#include "common.h"
#include "uustring.h"
#include "usertoken.h"

#include "adminfmg.h"

BOOL FMGHasInst(char *url) {
    char *query;
    if (url == NULL)
        return 0;
    for (query = strchr(url, '?'); query = StrIStr(query, "inst="); query++) {
        if (*(query - 1) == '&' || *(query - 1) == '?')
            return 1;
    }
    return 0;
}

void AdminFMG(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    struct DATABASE *b;
    struct SESSION *e = t->session;
    char *userToken = NULL;
    char *p;
    struct FMGCONFIG *fmgc = NULL;
    time_t now;
    char *instIn = NULL;

    char *params = NULL;
    size_t paramsMax;
    GROUPMASK requiredGm = NULL;
    EVP_MD_CTX *mdctx = NULL;
    /* Yes, the ivec should be EVP_MAX_MD_SIZE in case we change to a larger cipher later */
    unsigned char ivec[EVP_MAX_MD_SIZE];
    unsigned int mdLen;

    unsigned char *encryptedParams = NULL;
    char *base64EncryptedParams = NULL;
    EVP_CIPHER_CTX *ctx = NULL;
    int updateLen, finalLen;

    char *msg = NULL;

    if (t->host == NULL || (b = t->host->myDb) == NULL || (fmgc = b->fmgConfigs) == NULL) {
        HTTPCode(t, 404, 1);
        goto Finished;
    }

    instIn = GetSnipField(query, "inst", 0, 1);

    if (instIn == NULL) {
        GenericRedirect(t, t->host->hostname, t->host->remport, t->urlCopy, t->host->useSsl, 0);
        goto Finished;
    }

    if (e = t->session) {
        BOOL any = 0;
        for (;; fmgc = fmgc->next) {
            if (fmgc == NULL) {
                if (any) {
                    AdminLogup2(t, "");
                    goto Finished;
                }
                HTMLHeader(t, htmlHeaderOmitCache);
                UUSendZ(s, "The Films Media Group inst code ");
                SendHTMLEncoded(s, instIn);
                UUSendZ(s, " does not appear in " EZPROXYCFG ".\n");
                goto Finished;
            }
            if (instIn && stricmp(fmgc->inst, instIn) != 0)
                continue;
            any = 1;
            if (GroupMaskOverlap(e->gm, fmgc->gm))
                break;

            if (requiredGm)
                GroupMaskOr(requiredGm, fmgc->gm);
            else
                requiredGm = GroupMaskCopy(NULL, fmgc->gm);
        }
    }

    if (e == NULL) {
        AdminRelogin(t);
        goto Finished;
    }

    if (e->autoLoginBy != autoLoginByNone || e->logUserBrief == NULL) {
        AdminLogup(t, TRUE, NULL, NULL, requiredGm, FALSE);
    }

    if (fmgc->anonymous) {
        if (!(userToken = strdup("anonymous")))
            PANIC;
    } else {
        userToken = UserToken2("fmg", e->logUserBrief, 1);
        if (userToken == NULL) {
            AdminNoToken(t);
            goto Finished;
        }
    }

    /* The 256 covers the added &LibID=, &PatronID=value, &TTL=value, and &TS=value */
    paramsMax = 256 + strlen(fmgc->userClass);
    ;
    if (userToken)
        paramsMax += 5 + strlen(userToken) * 3;

    if (!(params = malloc(paramsMax)))
        PANIC;

    strcpy(params, "md5=");
    /* Leave room to drop in the MD5 signature in hex + 1 extra byte for the & separator */
    p = strchr(params, 0) + 33;

    time(&now);
    sprintf(p, "ts=%" PRIdMAX "&ver=1&salt=%c%c%c%c&user=", (intmax_t)now, RandChar(), RandChar(),
            RandChar(), RandChar());
    AddEncodedField(p, userToken, NULL);
    strcat(p, "&class=");
    AddEncodedField(p, fmgc->userClass, NULL);

    if (!(mdctx = EVP_MD_CTX_new()))
        PANIC;
    EVP_DigestInit(mdctx, EVP_md5());
    EVP_DigestUpdate(mdctx, p, strlen(p));
    EVP_DigestFinal(mdctx, ivec, &mdLen);
    DigestToHex(strchr(params, 0), ivec, mdLen, 1);
    *(p - 1) = '&';

    /* We digest the narrow version of the private key to form key and the initial vector */
    EVP_DigestInit(mdctx, EVP_md5());
    EVP_DigestUpdate(mdctx, (unsigned char *)fmgc->key, strlen(fmgc->key));
    EVP_DigestFinal(mdctx, ivec, &mdLen);

    if (!(ctx = EVP_CIPHER_CTX_new()))
        PANIC;
    EVP_CIPHER_CTX_init(ctx);
    EVP_EncryptInit(ctx, EVP_aes_128_cbc(), ivec, ivec);

    if (!(encryptedParams = malloc(strlen(params) + EVP_CIPHER_CTX_block_size(ctx) * 2)))
        PANIC;

    updateLen = 0;

    if (!EVP_EncryptUpdate(ctx, encryptedParams, &updateLen, (unsigned char *)params,
                           strlen(params)))
        goto Finished;

    if (!EVP_EncryptFinal(ctx, encryptedParams + updateLen, &finalLen))
        goto Finished;

    StrCpy3(t->buffer, t->url, sizeof(t->buffer));

    query = strchr(t->buffer, '?');
    if (query) {
        char *eol;
        GetSnipField(query, "inst", 1, 0);
        GetSnipField(query, "msg", 1, 0);
        for (eol = strchr(query, 0) - 1; eol >= query && *eol == '?'; eol--)
            *eol = 0;
    }

    strcat(t->buffer, strchr(t->buffer, '?') ? "&" : "?");
    base64EncryptedParams = Encode64Binary(NULL, encryptedParams, updateLen + finalLen);
    strcat(t->buffer, "inst=");
    AddEncodedField(t->buffer, fmgc->inst, NULL);
    strcat(t->buffer, "&msg=");
    AddEncodedField(t->buffer, base64EncryptedParams, NULL);

    GenericRedirect(t, t->host->hostname, t->host->remport, t->buffer, t->useSsl, 0);

    if (debugLevel) {
        Log("FMG URL generated with user token %s inst %s class %s", userToken, fmgc->inst,
            fmgc->userClass);
    }

Finished:
    if (ctx) {
        EVP_CIPHER_CTX_free(ctx);
        ctx = NULL;
    }
    if (mdctx) {
        EVP_MD_CTX_free(mdctx);
        mdctx = NULL;
    }
    FreeThenNull(msg);

    FreeThenNull(base64EncryptedParams);
    FreeThenNull(encryptedParams);
    FreeThenNull(params);
    FreeThenNull(userToken);
    FreeThenNull(instIn);

    GroupMaskFree(&requiredGm);
}
