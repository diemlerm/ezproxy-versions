/**
 * Called by other admin URLs when a user tries to access an unauthorized
 * area.  Provides user feedback and logs audit event of attempt.
 */

#define __UUFILE__ "adminunauthorized.c"

#include "common.h"
#include "uustring.h"

#include "adminunauthorized.h"
#include "adminaudit.h"

void AdminUnauthorized(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket;
    struct SESSION *e = t->session;
    char url[64];
    char *p;

    StrCpy3(url, t->urlCopy, sizeof(url));

    if (*url) {
        for (p = url + 1; *p; p++) {
            if (*p == '/' || *p == '?') {
                *p = 0;
                break;
            }
        }
    }

    AuditEvent(t, AUDITUNAUTHORIZED, e ? e->logUserFull : NULL, e, "%s", url);
    HTMLHeader(t, htmlHeaderNoCache);
    UUSendZ(s, "Unauthorized access attempt has been recorded.\n");
    t->finalStatus = 598;
}
