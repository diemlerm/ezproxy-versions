/**
 * /sso URL to provide allow EZproxy to share its SSO information with
 * external systems.
 */

#define __UUFILE__ "adminsso.c"

#include "common.h"
#include "uustring.h"

#include "adminsso.h"

#include "expression.h"

struct SSOSITE *AdminSSOSite(char *site, size_t siteLen) {
    struct SSOSITE *ssos;

    if (siteLen == 0)
        siteLen = FirstSlashQuestion(site) - site;

    for (ssos = ssoSites; ssos; ssos = ssos->next) {
        if (strnicmp(ssos->site, site, siteLen) == 0 && strlen(ssos->site) == siteLen)
            break;
    }

    return ssos;
}

void AdminSSO(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    char *site;
    struct SESSION *e = t->session;
    struct SSOSITE *ssos;
    const int ASCONTEXT = 0;
    struct FORMFIELD ffs[] = {
        {"context", NULL, 0, 0, 4096, FORMFIELDNOTRIMCTRL},
        {NULL, NULL, 0, 0}
    };
    char *context;
    char *p, *stop;
    time_t now;
    struct tm rtm;
    EVP_MD_CTX *mdctx = NULL;
    unsigned char md_value[EVP_MAX_MD_SIZE];
    unsigned int md_len;
    char plainText[512];
    unsigned char cryptText[512];
    int updateLen, finalLen;
    char *cryptTextBase64 = NULL;
    char ivec[16];
    EVP_CIPHER_CTX *ctx = NULL;
    char ipBuffer[INET6_ADDRSTRLEN];
    enum IPACCESS ipAccess = IPAREMOTE;
    char *username = NULL;
    char *expressionValue = NULL;

    FindFormFields(query, post, ffs);

    context = ffs[ASCONTEXT].value;

    if ((site = StartsIWith(t->urlCopy, "/SSO/")) == NULL || *site == 0) {
        if (t->maybeLogin) {
            // Don't force a login for an incomplete URL request
            t->maybeLogin = FALSE;
            return;
        }

        HTTPCode(t, 404, 1);
        goto Finished;
    }

    ssos = AdminSSOSite(site, 0);

    if (ssos == NULL) {
        if (t->maybeLogin) {
            // Do force a login request if someone is trying to probe to learn what
            // SSO sites we support
            return;
        }

        HTMLHeader(t, htmlHeaderOmitCache);
        UUSendZ(s, "EZproxy is not configured for this SSO name");
        goto Finished;
    }

    if (ssos->anonymous) {
        int i;
        for (i = 0; i < ssos->cIpTypes; i++) {
            if (compare_ip(&t->sin_addr, &ipTypes[i].start) >= 0 &&
                compare_ip(&ipTypes[i].stop, &t->sin_addr) >= 0) {
                ipAccess = ipTypes[i].ipAccess;
            }
        }
    }

    if (t->maybeLogin) {
        // Unless access is local, the user will have to log in
        t->maybeLogin = ipAccess != IPALOCAL;
        return;
    }

    if (ipAccess != IPALOCAL) {
        /* This next test obviously takes care of IPAREMOTE, but it also more
        subtly handles IPAAUTO since forcing the user through login will create
        the relevant session for AutoLoginIP
        */
        if (e == NULL) {
            AdminRelogin(t);
            goto Finished;
        }

        if (GroupMaskOverlap(e->gm, ssos->gm) == 0 ||
            (ssos->anonymous == 0 &&
             (e->autoLoginBy != autoLoginByNone || e->logUserBrief == NULL))) {
            AdminLogup(t, TRUE, NULL, NULL, ssos->gm, FALSE);
            goto Finished;
        }
    }

    if (!(mdctx = EVP_MD_CTX_new()))
        PANIC;
    if (ssos->ssoHash == SSOHASHMD5) {
        EVP_DigestInit(mdctx, EVP_md5());
        p = plainText + 33;
    } else if (ssos->ssoHash == SSOHASHSHA256) {
        EVP_DigestInit(mdctx, EVP_sha256());
        p = plainText + 65;
    } else {
        EVP_DigestInit(mdctx, EVP_sha1());
        p = plainText + 41;
    }

    if (ssos->anonymous) {
        username = "anonymous";
    } else if (ssos->expression == NULL) {
        username = e->logUserBrief;
    } else {
        expressionValue = ExpressionValue(t, NULL, NULL, ssos->expression);
        username = expressionValue ? expressionValue : "";
    }

    UUtime(&now);
    UUlocaltime_r(&now, &rtm);
    sprintf(p, "%c%c|%04d-%02d-%02d:%02d:%02d:%02d|%s|%s", RandChar(), RandChar(),
            rtm.tm_year + 1900, rtm.tm_mon + 1, rtm.tm_mday, rtm.tm_hour, rtm.tm_min, rtm.tm_sec,
            ipToStr(&t->sin_addr, ipBuffer, sizeof ipBuffer), username);

    EVP_DigestUpdate(mdctx, p, strlen(p));
    EVP_DigestFinal(mdctx, md_value, &md_len);

    DigestToHex(plainText, md_value, md_len, 0);
    *(p - 1) = '|';

    if (!(ctx = EVP_CIPHER_CTX_new()))
        PANIC;
    EVP_CIPHER_CTX_init(ctx);

    // Restore the original code instead of the following since
    // (1) this version wouldn't compile under Visual C 6.0 and
    // (2) there is a pretty specific expectation that there will be
    // 8 hex digits seeding the initial vector starting at the
    // beginning of ivec, and if a 64-bit quantity was dumped into
    // 16 hex digits instead, the hex digits we really want would
    // be sitting in the wrong end of ivec and the last digit would
    // be truncated.
    // The explicit type conversion to unsigned int truncates a
    // possibly 64-bit 'now' to the size expected by sprintf's '&08x'.
    // sprintf(ivec, "%0" PRINTF_INTMAX_HEX_WIDTH PRIXMAX, (intmax_t)now);
    sprintf(ivec, "%08x", (unsigned int)now);

    EVP_EncryptInit(ctx, EVP_des_ede3_cbc(), (unsigned char *)ssos->secret, (unsigned char *)ivec);

    if (!EVP_EncryptUpdate(ctx, cryptText, &updateLen, (unsigned char *)plainText,
                           strlen(plainText)))
        goto Finished;

    if (!EVP_EncryptFinal(ctx, cryptText + updateLen, &finalLen))
        goto Finished;

    cryptTextBase64 = Encode64Binary(NULL, cryptText, updateLen + finalLen);

    stop = t->buffer + sizeof(t->buffer) - 256;

    strcpy(t->buffer, ssos->url);

    if (strchr(t->buffer, '?') == NULL)
        strcat(t->buffer, "?");
    else
        strcat(t->buffer, "&");

    strcat(t->buffer, "sso=");
    AddEncodedField(strchr(t->buffer, 0), cryptTextBase64, stop);

    sprintf(strchr(t->buffer, 0), "&ts=%" PRIdMAX "", (intmax_t)now);

    if (context) {
        strcat(t->buffer, "&context=");
        AddEncodedField(strchr(t->buffer, 0), context, stop);
    }

    LocalRedirect(t, t->buffer, 0, 0, NULL);

Finished:
    if (ctx) {
        EVP_CIPHER_CTX_free(ctx);
        ctx = NULL;
    }
    if (mdctx) {
        EVP_MD_CTX_free(mdctx);
        mdctx = NULL;
    }
    FreeThenNull(cryptTextBase64);
    FreeThenNull(expressionValue);
}
