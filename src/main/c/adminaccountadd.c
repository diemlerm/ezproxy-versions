#define __UUFILE__ "adminaccountadd.c"

#include "common.h"
#include "uustring.h"
#include "usr.h"

#include "adminaccountadd.h"

void AdminAccountAdd(struct TRANSLATE *t, char *query, char *post) {
    const int AAAUSER = 0;
    struct FORMFIELD ffs[] = {
        {"user", NULL, 0, 0, MAXUSERLEN},
        {NULL,   NULL, 0, 0, 0         }
    };
    struct UUSOCKET *s = &t->ssocket;
    enum ACCOUNTFINDRESULT accountResult;

    AdminHeader(t, 0, "Account Add");

    FindFormFields(query, post, ffs);
    if (ffs[AAAUSER].value && strlen(ffs[AAAUSER].value) > 0) {
        if (strchr(ffs[AAAUSER].value, ':') != NULL) {
            UUSendZ(s, "Usernames may not contain colons\n");
            return;
        }
        accountResult = AccountFind(ffs[AAAUSER].value, NULL, accountDefaultPassword,
                                    accountDefaultPassword, 1);
        if (accountResult == accountCreated)
            UUSendZ(s, "Account created\n");
        else
            UUSendZ(s, "Account already exists\n");
    } else {
        SendFile(t, AADDHTM, SFREPORTIFMISSING);
    }

    AdminFooter(t, 0);
}
