/**
 * /user admin URL for testing user.txt configuration
 */

#define __UUFILE__ "adminuser.c"

#include "common.h"
#include "uustring.h"

#include "adminuser.h"
#include "usr.h"
#include "usrfinduser.h"

int UserInfoVariablesTable(struct TRANSLATE *t,
                           struct USERINFO *uip,
                           const char *filter,
                           const char *header) {
    struct UUSOCKET *s = &t->ssocket;
    int count = 0;
    int i = 0;
    char *var = NULL;
    char *val = NULL;

    for (; i < 2; i++) {
        switch (i) {
        case 0: {
            var = "login:user";
            val = uip->user;
        } break;
        case 1: {
            var = "login:loguser";
            val = uip->logUser;
        } break;
        }

        if (filter && StartsIWith(var, filter) == NULL)
            continue;
        if (count++ == 0) {
            if (header && *header)
                UUSendZ(s, header);
            UUSendZ(s, "<table class='bordered-table'>\n");
            UUSendZ(s, "<tr><th scope='col'>Variable</th><th scope='col'>Value</th></tr>\n");
        }
        UUSendZ(s, "<tr><td>");
        SendHTMLEncoded(s, var);
        UUSendZ(s, "</td><td>");
        if (val == NULL)
            UUSendZ(s, "<strong>NULL</strong>");
        else if (*SkipWS(val) == 0)
            UUSendZ(s, "&nbsp;");
        else
            SendHTMLEncoded(s, val);
        UUSendZ(s, "</td></tr>\n");
    }

    if (count)
        UUSendZ(s, "</table>\n");

    return count;
}

void AdminUser(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    struct SESSION *e = t->session;
    const int AUUSR = 0;
    const int AUUSER = 1;
    const int AUPASS = 2;
    const int AUPIN = 3;
    const int AUAUTH = 4;
    const int AUIP = 5;
    const int AUURL = 6;
    const int AUDEBUG = 7;
    const int AUGROUPNUMBER = 8;
    const int AUINSTNUMBER = 9;
    const int AUGROUPSYMBOL = 10;
    const int AUINSTSYMBOL = 11;
    const int AURENEW = 12;
    const int AURETURNURL = 13;
    const int AUCONTEXT = 14;
    struct FORMFIELD ffs[] = {
        {"usr", NULL, 0, 0, 0, FORMFIELDNOTRIMCTRL},
        {"user", NULL, 0, 0, MAXUSERLEN},
        {"pass", NULL, 0, 0, 64},
        {"pin", NULL, 0, 0, 32},
        {"auth", NULL, 0, 0, 0},
        {"ip", NULL, 0, 0, 0},
        {"url", NULL, 0, 0, 0},
        {"debug", NULL, 0, 0, 0},
        {"groupNumber", NULL, 0, 0, 10},
        {"instNumber", NULL, 0, 0, 10},
        {"groupSymbol", NULL, 0, 0, 10},
        {"instSymbol", NULL, 0, 0, 10},
        {"renew", NULL, 0, 0, 10},
        {"returnURL", NULL, 0, 0, 0},
        {"context", NULL, 0, 0, 0, FORMFIELDNOTRIMCTRL},
        {NULL, NULL, 0, 0, 0}
    };
    struct USERINFO ui;
    int userLimit;
    int result;
    GROUPMASK gmAuto = NULL;
    enum IPACCESS ipAccess;
    char *autoUser = NULL;
    struct VARIABLES *saveLocalVariables = NULL;
    struct VARIABLES *saveSessionVariables = NULL;
    struct VARIABLES *testVariables = NULL;
    BOOL forceDebug = 0;
    char *p;
    char *val;

    memset(&ui, 0, sizeof(ui));

    FindFormFields(query, post, ffs);

    AdminHeader(t, 0, "Test " EZPROXYUSR " Configuration");

    if (optionDisableTestEZproxyUsr) {
        UUSendZ(s, "<p>This feature is disabled on this server.</p>\n");
        goto Finished;
    }

    forceDebug = ffs[AUDEBUG].value && *ffs[AUDEBUG].value;

    if (ffs[AUUSR].value) {
        Trim(ffs[AUUSR].value, TRIM_TRAIL);
        if (*ffs[AUUSR].value == 0)
            ffs[AUUSR].value = NULL;
    }

    if (ffs[AUUSER].value) {
        struct sockaddr_storage saveIp;
        memcpy(&saveIp, &t->sin_addr, sizeof t->sin_addr);

        if (ffs[AUIP].value && *ffs[AUIP].value) {
            struct sockaddr_storage altIp;
            int family = NumericHostIp4(ffs[AUIP].value) ? AF_INET : AF_INET6;

            init_storage(&altIp, family, TRUE, 0);
            inet_pton_st(family, ffs[AUIP].value, &altIp);

            if (!is_addrnone(&altIp)) {
                memcpy(&t->sin_addr, &altIp, sizeof altIp);
                if (t->location.countryCode[0])
                    FindLocationByTranslateNoCache(t);
            }
        }

        gmAuto = GroupMaskNew(0);

        ipAccess = URLIpAccess(t, (ffs[AUURL].value && *ffs[AUURL].value ? ffs[AUURL].value : NULL),
                               NULL, gmAuto, NULL, &autoUser);
        if (IsRejectedIP(t)) {
            ipAccess = IPAREJECT;
        }

        if (*SkipWS(ffs[AUUSER].value) == 0)
            ffs[AUUSER].value = NULL;

        if (ffs[AUUSER].value)
            autoUser = NULL;

        InitUserInfo(t, &ui, ffs[AUUSER].value, ffs[AUPASS].value, ffs[AUURL].value, NULL);
        if (ffs[AUPIN].value && *ffs[AUPIN].value)
            ui.pin = ffs[AUPIN].value;
        if (IsOneOrMoreDigits(ffs[AUGROUPNUMBER].value))
            ui.groupNumber = ffs[AUGROUPNUMBER].value;
        if (IsOneOrMoreDigits(ffs[AUINSTNUMBER].value))
            ui.instNumber = ffs[AUINSTNUMBER].value;
        ui.groupSymbol = ffs[AUGROUPSYMBOL].value;
        ui.instSymbol = ffs[AUINSTSYMBOL].value;
        ui.logToSSocket = 1;
        ui.auth = ffs[AUAUTH].value;
        ui.forceDebug = forceDebug;

        saveLocalVariables = t->localVariables;
        testVariables = t->localVariables = VariablesNew("local", FALSE);

        saveSessionVariables = e->sessionVariables;
        e->sessionVariables = NULL;

        if ((ffs[AUUSR].value == NULL) || (*(ffs[AUUSR].value) == 0)) {
            result = FindUser(t, &ui, NULL, NULL, NULL, &userLimit, 0);
        } else {
            result = FindUser(t, &ui, "POST usr", ffs[AUUSR].value, NULL, &userLimit, 0);
        }

        t->localVariables = saveLocalVariables;
        e->sessionVariables = saveSessionVariables;
        /* testVariables is used to present variables found and is
           then cleaned up at the end of this routine
        */

        if (!is_ip_equal(&t->sin_addr, &saveIp)) {
            memcpy(&t->sin_addr, &saveIp, sizeof saveIp);
            if (t->location.countryCode[0])
                FindLocationByTranslateNoCache(t);
        }

        if (ui.logToSSocket == 2) {
            ui.logToSSocket = 3;
            UsrLog(&ui, "");
        }

        UUSendZ(s, "<h2>Test results</h2>\n<p>\n");

        if (ui.denied) {
            UUSendZ(s, "Access denied");
        } else if (ui.cgi) {
            UUSendZ(s, "CGI redirection");
        } else if (ui.wayf) {
            UUSendZ(s, "Shibboleth redirection");
        } else if (ffs[AUUSER].value == NULL && autoUser == NULL && (ui.user == NULL)) {
            UUSendZ(s, "No username specified<br>\n");
            if (ipAccess == IPALOCAL)
                UUSendZ(s,
                        "Access from ExcludeIP address, login not required, access not proxied, "
                        "redirect to real URL");
            else
                UUSendZ(s, "Present login page");
        } else if (autoUser == NULL && result == 0) {
            UUSendZ(s, "Invalid credentials");
        } else {
            int gc = GroupMaskCardinality(ui.gm);
            int gca = GroupMaskCardinality(gmAuto);

            if (autoUser) {
                UUSendZ(s, "AutoLoginIP as user ");
                SendHTMLEncoded(s, autoUser);
            } else
                UUSendZ(s, "Valid credentials");

            if (ui.admin)
                UUSendZ(s, "<br>\nAdministrative user");

            UUSendZ(s, "<br>\n");
            if (gc == 0)
                UUSendZ(s, "No assigned groups from " EZPROXYUSR);
            else {
                WriteLine(s, "Assigned group%s from " EZPROXYUSR ": ", SIfNotOne(gc));
                GroupMaskWrite(ui.gm, s, &ui);
            }

            if (GroupMaskCardinality(gmAuto) > 0) {
                WriteLine(s, "<br>\nAutoLoginIP assigned group%s: ", SIfNotOne(gca));
                GroupMaskWrite(gmAuto, s, &ui);
            }

            UUSendZ(s, "<br>Access from \n");
            switch (ipAccess) {
            case IPAREMOTE:
                UUSendZ(s, "IncludeIP address, login required, access proxied");
                break;
            case IPALOCAL:
                UUSendZ(s,
                        "ExcludeIP address, login not required, access not proxied, redirect to "
                        "real URL");
                break;
            case IPAAUTO:
                UUSendZ(s, "AutoLoginIP address, login not required, access proxied");
                break;
            case IPAREJECT:
                UUSendZ(s, "RejectIP address, access rejected to EZproxy");
                break;
            default:
                WriteLine(s, "unknown type %d", ipAccess);
                break;
            }
        }

        VariablesTable(t, testVariables, NULL, "</p><h2>Variables</h2><p>\n");
        UserInfoVariablesTable(t, &ui, NULL, "</p><h2>User Interface State Variables</h2><p>\n");

        UUSendZ(s, "</p>\n");

        UUSendHR(s);
    }

    UUSendZ(s, "<p>All of the following fields are optional.</p>\n");
    UUSendZ(s, "<form action='/user' method='post'>\n");
    UUSendZ(s, "<table class='loose-table'>");

    WriteField(t, "User", "user", ffs[AUUSER].value, NULL, 0, 0, NULL);
    WriteField(t, "Pass", "pass", ffs[AUPASS].value, NULL, 0, 0, "password");
    WriteField(t, "URL", "url", ffs[AUURL].value, NULL, 0, 0, NULL);
    WriteField(t, "IP", "ip", ffs[AUIP].value, NULL, 0, 0, NULL);
    WriteField(t, "Auth", "auth", ffs[AUAUTH].value, NULL, 0, 0, NULL);
    WriteField(t, "<a href='#iiipin' style='font-weight: bold'>III Pin</a>", "pin",
               ffs[AUPIN].value, NULL, 0, 0, NULL);
    if ((usageLogUser & ULUUSEROBJECT) || debugLevel > 7000) {
        WriteField(t, "Group Number", "groupNumber", ffs[AUGROUPNUMBER].value, NULL, 0, 0, NULL);
        WriteField(t, "Group Symbol", "instSymbol", ffs[AUGROUPSYMBOL].value, NULL, 0, 0, NULL);
        WriteField(t, "Inst Number", "instNumber", ffs[AUINSTNUMBER].value, NULL, 0, 0, NULL);
        WriteField(t, "Inst Symbol", "instSymbol", ffs[AUINSTSYMBOL].value, NULL, 0, 0, NULL);
        p = strchr(t->buffer, 0);  // borrow some buffer space
        strcat(p, "cookie:");
        strcat(p, privateComputerCookieName);
        val = NULL;
        val = ExpressionValue(t, NULL, NULL, p);
        *p = 0;  // return buffer space
        WriteField(t, "Private Computer Cookie", privateComputerCookieName, val, NULL, 0, 0, NULL);
        WriteField(t, "Renew Session", "renew", ffs[AURENEW].value, NULL, 0, 0, NULL);
        WriteField(t, "Return URL", "returnURL", ffs[AURETURNURL].value, NULL, 0, 0, NULL);
        WriteField(t, "Context", "context", ffs[AUCONTEXT].value, NULL, 0, 0, NULL);
    }

    WriteCheckbox(t, "Force Debug", "debug", forceDebug, NULL);

    UUSendZ(s, "<tr><td>&nbsp;</td><td><input type='submit' value='Test'></td></tr>\n</table>");
    UUSendZ(s,
            "\
<p>If the following box is blank, " EZPROXYUSR
            " will be used for testing.</p>\n\
<p>If the following box is not blank, its contents will be used instead of " EZPROXYUSR
            " for testing.</p>\n\
<textarea name='usr' cols='80' rows='20'>");
    SendHTMLEncoded(s, ffs[AUUSR].value);
    UUSendZ(s, "</textarea><br>\n");
    UUSendZ(s, "</form>\n");

    UUSendZ(s,
            "<p><a name='iiipin'></a><strong>III Pin</strong> is only used with the III Patron API "
            "authentication \"Password&nbsp;Both\" directive.  When using a pin as a password for "
            "other authentication methods, including when using III Patron API when not using "
            "\"Password&nbsp;Both\" or when using SIP, the test pin should be placed in the "
            "\"Pass\" field.</p>\n");

Finished:
    GroupMaskFree(&ui.gm);
    GroupMaskFree(&gmAuto);
    VariablesFree(&testVariables);

    AdminFooter(t, 0);
}
