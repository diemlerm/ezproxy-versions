#define __UUFILE__ "adminnetlibrary.c"

#include "common.h"
#include "uustring.h"
#include "usertoken.h"

#include "adminnetlibrary.h"

BOOL NetLibraryHasNLReturn(char *url) {
    char *query;
    if (url == NULL)
        return 0;
    for (query = strchr(url, '?'); query = StrIStr(query, "NLReturn="); query++) {
        if (*(query - 1) == '&' || *(query - 1) == '?')
            return 1;
    }
    return 0;
}

void AdminNetLibrary(struct TRANSLATE *t, char *query, char *post) {
    struct DATABASE *b;
    struct SESSION *e = t->session;
    char *userToken = NULL;
    char *p;
    struct NETLIBRARYCONFIG *nlc = NULL;
    char *queryCopy = NULL;
    char *nlReturn = NULL;
    char *productId = NULL;

    time_t now;

    struct tm tm;

    char *params = NULL;
    size_t paramsMax;

    char *wideParams = NULL;
    char *wideParamsEol;
    char *wideParamsSigEol;

    EVP_MD_CTX *mdctx = NULL;
    /* Yes, the ivec should be EVP_MAX_MD_SIZE; NetLibrary uses first 16 of sha256 hash */
    unsigned char ivec[EVP_MAX_MD_SIZE];
    unsigned int mdLen;

    unsigned char *encryptedParams = NULL;
    unsigned char *encryptedParamsEol;

    EVP_CIPHER_CTX *ctx = NULL;
    int updateLen, finalLen;

    char *msg = NULL;
    GROUPMASK requiredGm = NULL;

    if (t->host == NULL || (b = t->host->myDb) == NULL || (nlc = b->netLibraryConfigs) == NULL) {
        HTTPCode(t, 404, 1);
        return;
    }

    if (e = t->session) {
        for (;; nlc = nlc->next) {
            if (nlc == NULL) {
                AdminLogup2(t, "");
                goto Finished;
            }
            if (GroupMaskOverlap(e->gm, nlc->gm))
                break;
            if (requiredGm)
                GroupMaskOr(requiredGm, nlc->gm);
            else
                requiredGm = GroupMaskCopy(NULL, nlc->gm);
        }
    }

    if (strcmp(t->urlCopy, "/") == 0) {
        p = "?action=home&v=1";
    } else if (p = StartsIWith(t->urlCopy, "/urlapi.asp")) {
        if (*p == 'x' || *p == 'X')
            p++;
        if (*p != 0 && *p != '?')
            p = NULL;
    }

    if (p == NULL) {
        GenericRedirect(t, t->host->hostname, t->host->remport, t->urlCopy, t->host->useSsl, 0);
        goto Finished;
    }

    if (nlc->noUserToken == 0) {
        if (e == NULL || e->autoLoginBy != autoLoginByNone || e->logUserBrief == NULL) {
            AdminLogup(t, TRUE, NULL, NULL, nlc->gm, FALSE);
            goto Finished;
        }
        userToken = UserToken2("nl", e->logUserBrief, 1);
        if (userToken == NULL) {
            AdminNoToken(t);
            goto Finished;
        }
    }

    /* the strlen(p) * 3 insure there is enough space to change NLReturn into action
       even if people are sneaking in bad characters that have to be % encoded
    */
    /* The 256 covers the added &LibID=, &PatronID=value, &TTL=value, and &TS=value */
    paramsMax = strlen(p) * 3 + 4 * strlen(nlc->netLibraryLibId) + 256;
    /* User tokens are all formed from characters to do not have to be encrypted */
    if (userToken)
        paramsMax += 12 + strlen(userToken);

    if (!(params = malloc(paramsMax)))
        PANIC;

    strcpy(params, *p == '?' ? p + 1 : p);

    /* If this is an NLReturn, reform the URL based on values present */
    if (nlReturn = GetSnipField(params, "NLReturn", 1, 1)) {
        productId = GetSnipField(params, "product_id", 1, 1);
        strcpy(params, "action=");
        /* remap the value details to summary since action=details is not understood */
        if (stricmp(nlReturn, "details") == 0)
            strcat(params, "summary");
        else
            AddEncodedField(params, nlReturn, NULL);
        strcat(params, "&v=1");
        if (productId) {
            strcat(params, "&bookid=");
            AddEncodedField(params, productId, NULL);
        }
    }

    /* If someone is trying to sneak in the values we generate, snip them out of the URL */
    GetSnipField(params, "LibId", 1, 0);
    GetSnipField(params, "PatronId", 1, 0);
    GetSnipField(params, "TTL", 1, 0);
    GetSnipField(params, "TS", 1, 0);

    time(&now);
    now -= 7 * ONE_HOUR_IN_SECONDS;
    UUgmtime_r(&now, &tm);

    strcat(params, *p ? "&" : "?");

    strcat(params, "LibID=");
    AddEncodedField(params, nlc->netLibraryLibId, NULL);

    if (userToken) {
        strcat(params, "&PatronID=");
        AddEncodedField(params, userToken, NULL);
    }

    sprintf(strchr(params, 0), "&TTL=70&TS=%02d-%02d-%04d %02d:%02d:%02d", tm.tm_mon + 1,
            tm.tm_mday, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);

    /* Leave extra space for &sig= (10), double-null (2), and SHA256 signature (16) */
    if (!(wideParams = malloc(strlen(params) * 2 + 64)))
        PANIC;
    wideParamsEol = UUStrCpyToWide(wideParams, params);

    /* The signature key will be based on the wide string to this point, without
       the &sig=.  The sig is the SHA256 hash which will be generated and placed
       into the string after &sig=, but we prep for it now
    */
    wideParamsSigEol = UUStrCpyToWide(wideParamsEol, "&sig=");

    if (!(mdctx = EVP_MD_CTX_new()))
        PANIC;
    EVP_DigestInit(mdctx, EVP_sha256());
    /* Digest up to but not including the &sig= */
    EVP_DigestUpdate(mdctx, wideParams, wideParamsEol - wideParams);
    EVP_DigestUpdate(mdctx, nlc->wideNetLibrarySignatureKey, nlc->wideNetLibrarySignatureKeyLen);
    /* Place the hash right into wideParams at the very end */
    EVP_DigestFinal(mdctx, (unsigned char *)wideParamsSigEol, &mdLen);
    wideParamsEol = wideParamsSigEol + mdLen;

    /* We digest the narrow version of the private key to form the initial vector */
    EVP_DigestInit(mdctx, EVP_sha256());
    EVP_DigestUpdate(mdctx, (unsigned char *)nlc->netLibraryPrivateKey,
                     strlen(nlc->netLibraryPrivateKey));
    EVP_DigestFinal(mdctx, ivec, &mdLen);

    ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init(ctx);
    EVP_EncryptInit(ctx, EVP_aes_128_cbc(), (unsigned char *)nlc->netLibraryPrivateKey, ivec);

    /* Also leave room for adding on the &et=rj&pid=##### in wide characters */
    if (!(encryptedParams =
              malloc((wideParamsEol - wideParams) + EVP_CIPHER_CTX_block_size(ctx) * 2 + 64)))
        PANIC;

    updateLen = finalLen = 0;
    if (!EVP_EncryptUpdate(ctx, encryptedParams, &updateLen, (unsigned char *)wideParams,
                           wideParamsEol - wideParams))
        goto Finished;

    if (!EVP_EncryptFinal(ctx, encryptedParams + updateLen, &finalLen))
        goto Finished;

    encryptedParamsEol = encryptedParams + updateLen + finalLen;
    encryptedParamsEol = (unsigned char *)UUStrCpyToWide(
        (char *)encryptedParams + updateLen + finalLen, "&et=rj&pid=129");

    strcpy(t->buffer, "/urlapi.aspx?msg=");
    Encode64Binary(strchr(t->buffer, 0), encryptedParams, encryptedParamsEol - encryptedParams);

    GenericRedirect(t, t->host->hostname, t->host->remport, t->buffer, t->useSsl, 0);
    /*
    UUSendZ(s, "<br>\n<a href=\"http://ezproxy.netlibrary.com/urlapi.aspx?msg=");
    SendUrlEncoded(s, msg);
    UUSendZ(s, "\">URL</a>\n");
    */

Finished:
    if (ctx) {
        EVP_CIPHER_CTX_free(ctx);
        ctx = NULL;
    }
    if (mdctx) {
        EVP_MD_CTX_free(mdctx);
        mdctx = NULL;
    }
    FreeThenNull(msg);
    FreeThenNull(encryptedParams);
    FreeThenNull(wideParams);
    FreeThenNull(params);
    FreeThenNull(userToken);
    FreeThenNull(queryCopy);
    FreeThenNull(nlReturn);
    FreeThenNull(productId);
    GroupMaskFree(&requiredGm);
}
