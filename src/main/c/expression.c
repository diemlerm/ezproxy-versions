#define __UUFILE__ "expression.c"

#define EXPRESSION_INTERNAL_FUNCTIONS

#include "common.h"
#include "usr.h"
#include "usrfinduser.h"
#include "expression.h"
#include "variables.h"
#include "efgeneral.h"
#include "efparsename.h"

#define PANIC_IF_NULL(x) \
    if (!(x))            \
    PANIC

void ReportErrorInPlace(struct USERINFO *uip, struct EXPRESSIONCTX *ec) {
    char *place;
    int right = ec->curr - ec->start;
    int left = ec->prev - ec->start;

    if (!(place = calloc(1, strlen(ec->start) + 4 + 1)))
        PANIC;

    if ((left) > 0) {
        memcpy(&(place[0]), &(ec->start[0]), left);
    }
    if ((right - left) > 0) {
        memcpy(&(place[left + 2]), &(ec->start[(left)]), right - left);
    }
    if ((strlen(ec->start) - right) > 0) {
        memcpy(&(place[left + 2 + (right - left) + 2]), &(ec->start[right]),
               strlen(ec->start) - right);
    }

    place[2 + left - 1] = '*';
    place[2 + left - 2] = '*';
    place[2 + right + 0] = '*';
    place[2 + right + 1] = '*';

    UsrLog(uip, "Error, %s: %s", (ec->invalidMsg ? ec->invalidMsg : "unrecognized in this context"),
           place);

    FreeThenNull(place);
}

BOOL ExpressionAggregateTestInitialize(struct AGGREGATETESTCONTEXT *atc,
                                       struct USERINFO *uip,
                                       const char *testVal,
                                       enum AGGREGATETESTOPTIONS ato) {
    char *error;

    memset(atc, 0, sizeof(*atc));
    atc->uip = uip;
    atc->testVal = testVal ? testVal : "";

    if (ato == ATOCOUNT) {
        atc->ato = ato;
        atc->all = TRUE;
    } else if (ato == ATOALLEQ || ato == ATOALLRE || ato == ATOALLXMLRE || ato == ATOALLWILD) {
        /* The ALL versions all follow the ANY versions, so - 1 changes this from ALL to WILD
           which is what ExpressionAggregateTestValue expects
        */
        atc->ato = ato - 1;
        atc->all = TRUE;
    } else {
        atc->ato = ato;
        atc->any = TRUE;
    }

    if (atc->ato == ATOANYRE) {
        if (error = ParseRegularExpression(atc->testVal, &atc->rePattern, &atc->rePatternExtra,
                                           NULL, NULL, NULL)) {
            UsrLog(atc->uip, "RE error %s: %s", error, atc->testVal);
            FreeThenNull(error);
            return FALSE;
        }
    }

    if (atc->ato == ATOANYXMLRE) {
        if (error = ParseXMLRegularExpression(atc->testVal, &(atc->xmlRePattern))) {
            UsrLog(atc->uip, "XML RE error %s: %s", error, atc->testVal);
            FreeThenNull(error);
            return FALSE;
        }
    }

    atc->more = TRUE;

    return TRUE;
}

/* Returns TRUE if more testing needed to determine final outcome,
   FALSE if no more testing required
*/

BOOL ExpressionAggregateTestValue(struct AGGREGATETESTCONTEXT *atc, const char *realVal) {
    if (!atc->more)
        return FALSE;

    if (realVal == NULL)
        realVal = "";

    switch (atc->ato) {
    case ATOCOUNT:
        atc->count++;
        atc->result = TRUE;
        break;

    case ATOANYEQ:
        atc->result = strcmp(realVal, atc->testVal) == 0;
        break;

    case ATOANYWILD:
        atc->result = WildCompare(realVal, atc->testVal);
        break;

    case ATOANYRE:
        atc->nmat = pcre_exec(atc->rePattern, atc->rePatternExtra, realVal, strlen(realVal), 0, 0,
                              atc->ovec, sizeof(atc->ovec) / sizeof(atc->ovec[0]));

        if (atc->result = (atc->nmat >= 0)) {
            FreeThenNull(atc->lastRealVal);
            if (!(atc->lastRealVal = strdup(realVal)))
                PANIC;
        }
        break;

    case ATOANYXMLRE:
        atc->nmat = xmlRegexpExec(atc->xmlRePattern, BAD_CAST realVal);

        if (atc->result = (atc->nmat >= 0)) {
            FreeThenNull(atc->lastRealVal);
            if (!(atc->lastRealVal = strdup(realVal)))
                PANIC;
        }
        break;

    default:
        break;
    }

    if (atc->any) {
        /* If this matched, atc->result == TRUE and we don't need to continue, so ! atc->result
           If this didn't match, atc->result == FALSE and we do need to continue, still !
           atc->result
        */
        return atc->more = !atc->result;
    } else {
        /* If this matched, atc->result == TRUE and we still need to continue on, so atc->result
           If this didn't match, atc->result == FALSE and we do not need to continue, still
           atc->result */
        return atc->more = atc->result;
    }
}

int ExpressionAggregateTestTerminate(struct AGGREGATETESTCONTEXT *atc, struct VARIABLES *rev) {
    if (atc->lastRealVal) {
        if (atc->result && atc->ato == ATOANYRE && rev) {
            int i;
            for (i = 0; i < 10; i++) {
                char reVar[16];
                sprintf(reVar, "re:%d", i);
                if (i < atc->nmat) {
                    char *reVal;
                    pcre_get_substring(atc->lastRealVal, atc->ovec, sizeof(atc->ovec) / sizeof(int),
                                       i, (const char **)&reVal);
                    VariablesSetValue(rev, reVar, reVal);
                    FreeThenNull(reVal);
                } else {
                    VariablesDelete(rev, reVar);
                }
            }
        }

        FreeThenNull(atc->lastRealVal);
    }

    if (atc->rePatternExtra) {
        pcre_free(atc->rePatternExtra);
        atc->rePatternExtra = NULL;
    }

    if (atc->rePattern) {
        pcre_free(atc->rePattern);
        atc->rePattern = NULL;
    }

    if (atc->xmlRePattern) {
        xmlRegFreeRegexp(atc->xmlRePattern);
        atc->xmlRePattern = NULL;
    }

    return atc->ato == ATOCOUNT ? atc->count : atc->result;
}

char *ExpressionVariableIndexBufferStr(const char *var,
                                       const char *index,
                                       char *buffer,
                                       int start,
                                       int len) {
    if (index) {
        buffer[start + len - 1] = 1;
        _snprintf(buffer + start, len, "%s[%s]", var, index);
        if (buffer[start + len - 1] != 1) {
            return NULL;
        }
    } else {
        buffer[start + len - 1] = 1;
        _snprintf(buffer + start, len, "%s", var);
        if (buffer[start + len - 1] != 1) {
            return NULL;
        }
    }
    return &(buffer[start]);
}

char *ExpressionVariableIndexBufferNum(const char *var,
                                       int index,
                                       char *buffer,
                                       int start,
                                       int len) {
    buffer[start + len - 1] = 1;
    _snprintf(buffer + start, len, "%s[%d]", var, index);
    if (buffer[start + len - 1] != 1) {
        return NULL;
    }
    return &(buffer[start]);
}

char *ExpressionVariableIndex(const char *var, const char *index) {
    if (index) {
        char *varIndex;
        if (!(varIndex = (char *)malloc(strlen(var) + strlen(index) + 3)))
            PANIC;
        sprintf(varIndex, "%s[%s]", var, index);
        return varIndex;
    }
    return (char *)var;
}

char *ExpressionVariableIndexNum(const char *var, int index) {
    char *varIndex;
    if (!(varIndex = (char *)malloc(strlen(var) + 25 + 3)))
        PANIC;
    sprintf(varIndex, "%s[%d]", var, index);
    return varIndex;
}

static char *ExpressionVariableHttp(struct EXPRESSIONCTX *ec, const char *var, const char *index) {
    int indexVal = index ? atoi(index) : 0;
    int indexCurrent = 0;
    char *header;
    char *val = NULL;

    for (header = ec->t->requestHeaders; (header = FindField(var, header));) {
        LinkField(header);
        if (indexCurrent++ == indexVal) {
            if (!(val = strdup(header)))
                PANIC;
            break;
        }
    }

    return val;
}

static char *ExpressionVariableCookie(struct EXPRESSIONCTX *ec,
                                      const char *var,
                                      const char *index) {
    int indexVal = index ? atoi(index) : 0;
    int indexCurrent = 0;
    char *cookie;
    char *val = NULL;
    char *rc;

    for (cookie = ec->t->requestHeaders; (cookie = FindField("cookie", cookie));) {
        LinkField(cookie);
        for (rc = cookie; rc = strstr(rc, var); rc++) {
            if (rc == cookie || IsWS(*(rc - 1)) || *(rc - 1) == ';') {
                char *p = rc + strlen(var);
                if (*p == '=') {
                    char *semi;
                    size_t len;
                    if (indexCurrent++ < indexVal) {
                        continue;
                    }

                    p++;
                    if (semi = strchr(p, ';'))
                        len = semi - p;
                    else
                        len = strlen(p);
                    if (!(val = malloc(len + 1)))
                        PANIC;
                    memcpy(val, p, len);
                    *(val + len) = 0;
                    break;
                }
            }
        }
    }

    return val;
}

char *ExpressionBoolean(BOOL b) {
    char *val;

    if (!(val = strdup(b ? "1" : "0")))
        PANIC;
    return val;
}

BOOL ExpressionReadOnlyVariable(struct EXPRESSIONCTX *ec, const char *var, const char *index) {
    char *varIndex = ExpressionVariableIndex(var, index);
    char *ns;
    BOOL readOnlyVar = TRUE;
    BOOL nonLoginVar = TRUE;

    if ((ns = StartsWith(varIndex, "login:")) && *ns) {
        nonLoginVar = FALSE;
        if (strcmp(varIndex, "login:user") == 0) {
        } else if (strcmp(varIndex, "login:auth") == 0) {
        } else if (strcmp(varIndex, "login:url") == 0 && ec->uip->url && *ec->uip->url) {
        } else if (strcmp(varIndex, "login:pass") == 0) {
        } else if (strcmp(varIndex, "login:pin") == 0) {
        } else if (strcmp(varIndex, "login:loguser") == 0) {
        } else if (strcmp(varIndex, "login:http") == 0) {
        } else if (strcmp(varIndex, "login:https") == 0) {
        } else if (strcmp(varIndex, "login:groupNumber") == 0) {
        } else if (strcmp(varIndex, "login:instNumber") == 0) {
        } else if (strcmp(varIndex, "login:groupSymbol") == 0) {
        } else if (strcmp(varIndex, "login:instSymbol") == 0) {
        } else if (strcmp(varIndex, "login:renew") == 0) {
        } else if (strcmp(varIndex, "login:privateComputer") == 0) {
        } else if (strcmp(varIndex, "login:returnURL") == 0) {
        } else {
            nonLoginVar = TRUE;
        }
    }

    if (nonLoginVar) {
        if (strcmp(varIndex, "session:admin") == 0) {
        } else if (strcmp(varIndex, "session:created") == 0) {
        } else if (strcmp(varIndex, "session:lastAuthenticated") == 0) {
        } else if (strcmp(varIndex, "session:user") == 0) {
        } else if (strcmp(varIndex, "session:reloginRequired") == 0) {
        } else if (strcmp(varIndex, "server:url:http") == 0) {
        } else if (strcmp(varIndex, "server:url:https") == 0) {
        } else if (strcmp(varIndex, "server:url:strongest") == 0) {
        } else if (strcmp(varIndex, "server:loginCookieName") == 0) {
        } else if ((ns = StartsWith(var, "auth:")) && *ns) {
            if (ec->uip->authIsReadOnly) {
                readOnlyVar = (ec->uip->authIsReadOnly)(ec->t, ec->uip, ec->context, ns, index);
            }
        } else if ((ns = StartsWith(var, "http:")) && *ns) {
            /**
             * use to access environment variables from user.txt
             * For example the user.txt:
             * set HELLO="world"
             *
             * Then, the I can access this variable in user.txt common block
             * such as:
             * env:HELLO
             */
        } else if ((ns = StartsWith(var, "env:")) && *ns) {
        } else if ((ns = StartsWith(var, "cookie:")) && *ns) {
        } else if ((ns = StartsWith(var, "ParseName:")) && *ns) {
        } else {
            readOnlyVar = FALSE;
        }
    }

    if (var != varIndex)
        FreeThenNull(varIndex);

    return readOnlyVar;
}

char *ExpressionVariable(struct EXPRESSIONCTX *ec,
                         const char *var,
                         const char *index,
                         BOOL nullToEmptyString) {
    char *varIndex = ExpressionVariableIndex(var, index);
    char *val = NULL;
    char *ns;
    struct SESSION *e = ec->t->session;
    BOOL nonLoginVar = TRUE;

    if ((ns = StartsWith(varIndex, "login:")) && *ns) {
        nonLoginVar = FALSE;
        if (strcmp(varIndex, "login:user") == 0) {
            val = UUStrDup(ec->uip->user);
        } else if (strcmp(varIndex, "login:auth") == 0) {
            val = UUStrDup(ec->uip->auth);
        } else if (strcmp(varIndex, "login:url") == 0 && ec->uip->url && *ec->uip->url) {
            val = UUStrDup(ec->uip->url);
        } else if (strcmp(varIndex, "login:pass") == 0) {
            val = UUStrDup(ec->uip->pass);
        } else if (strcmp(varIndex, "login:pin") == 0) {
            val = UUStrDup(ec->uip->pin);
        } else if (strcmp(varIndex, "login:loguser") == 0) {
            val = UUStrDup(ec->uip->logUser);
        } else if (strcmp(varIndex, "login:http") == 0) {
            val = ExpressionBoolean(ec->t->useSsl == 0);
        } else if (strcmp(varIndex, "login:https") == 0) {
            val = ExpressionBoolean(ec->t->useSsl);
        } else if (strcmp(varIndex, "login:groupNumber") == 0) {
            val = UUStrDup(ec->uip->groupNumber);
        } else if (strcmp(varIndex, "login:instNumber") == 0) {
            val = UUStrDup(ec->uip->instNumber);
        } else if (strcmp(varIndex, "login:groupSymbol") == 0) {
            val = UUStrDup(ec->uip->groupSymbol);
        } else if (strcmp(varIndex, "login:instSymbol") == 0) {
            val = UUStrDup(ec->uip->instSymbol);
        } else if (strcmp(varIndex, "login:renew") == 0) {
            val = UUStrDup(ec->uip->renew);
        } else if (strcmp(varIndex, "login:privateComputer") == 0) {
            val = UUStrDup(ec->uip->privateComputer);
        } else if (strcmp(varIndex, "login:returnURL") == 0) {
            char *query;
            if (ec->uip->url && (query = strchr(ec->uip->url, '?'))) {
                char *returnURL = NULL;
                query++;
                if (returnURL = GetSnipField(query, "returnURL", FALSE, TRUE)) {
                    /* Make sure it at least looks like a URL */
                    if (ParseProtocol(returnURL, NULL) == NULL) {
                        FreeThenNull(returnURL);
                    } else {
                        char *context = GetSnipField(query, "context", FALSE, TRUE);
                        if (context == NULL) {
                            /* If no context, just include what we got for returnURL */
                            val = returnURL;
                            returnURL = NULL;
                        } else {
                            /* build the returnURL to include the context */
                            /* strlen(context) * 3 allows for worst-case % encoding, the +16 handles
                             * ?context= with ease */
                            if (!(val = malloc(strlen(returnURL) + strlen(context) * 3 + 16)))
                                PANIC;
                            strcpy(val, returnURL);
                            FreeThenNull(returnURL);
                            strcat(val, strchr(val, '?') ? "&" : "?");
                            strcat(val, "context=");
                            AddEncodedField(val, context, NULL);
                            FreeThenNull(context);
                        }
                    }
                }
                /* If we have a URL, strip out any attempt to inject scripts */
                if (val)
                    StripScripts(val);
            }
        } else {
            nonLoginVar = TRUE;
        }
    }

    if (nonLoginVar) {
        if (strcmp(varIndex, "session:admin") == 0) {
            val = ExpressionBoolean(e && e->priv != 0);
        } else if (strcmp(varIndex, "session:sessionid") == 0) {
            if (e)
                val = ExpressionStrdup(e->key);
        } else if (strcmp(varIndex, "session:created") == 0) {
            if (e)
                val = ExpressionInt(e->created);
        } else if (strcmp(varIndex, "session:lastAuthenticated") == 0) {
            if (e)
                val = ExpressionInt(e->lastAuthenticated);
        } else if (strcmp(varIndex, "session:user") == 0) {
            if (e)
                val = UUStrDup(e->logUserBrief);
        } else if (strcmp(varIndex, "session:reloginRequired") == 0) {
            if (e)
                val = ExpressionBoolean(e->reloginRequired);
        } else if (strcmp(varIndex, "server:url:http") == 0) {
            val = UUStrDup(myUrlHttp);
        } else if (strcmp(varIndex, "server:url:https") == 0) {
            val = UUStrDup(myUrlHttps);
        } else if (strcmp(varIndex, "server:url:strongest") == 0) {
            val = UUStrDup(myUrlHttps ? myUrlHttps : myUrlHttp);
        } else if (strcmp(varIndex, "server:loginCookieName") == 0) {
            val = UUStrDup(loginCookieName);
        } else if ((ns = StartsWith(var, "auth:")) && *ns) {
            if (ec->uip->authGetValue) {
                val = (ec->uip->authGetValue)(ec->t, ec->uip, ec->context, ns, index);
            }
        } else if ((ns = StartsWith(var, "http:")) && *ns) {
            val = ExpressionVariableHttp(ec, ns, index);
        } else if ((ns = StartsWith(var, "env:")) && *ns) {
            val = UUStrDup(getenv(ns));
        } else if ((ns = StartsWith(var, "cookie:")) && *ns) {
            val = ExpressionVariableCookie(ec, ns, index);
        } else if ((ns = StartsWith(var, "ParseName:")) && *ns) {
            val = ExpressionVariableParseName(ec, varIndex);
        } else {
            val = VariablesGetValue(ec->t->localVariables, varIndex);
            if (val == NULL && StartsWith(var, "session:") && e) {
                val = VariablesGetValue(e->sessionVariables, varIndex);
            }
        }
    }

    if (val == NULL && nullToEmptyString)
        val = UUStrDup("");

    if (debugLevel > 7000) {
        UsrLog(ec->uip, "found %s%s%s%s", varIndex, ((val == NULL) ? " is " : " = '"),
               ((val == NULL) ? "NULL" : val), ((val == NULL) ? "" : "'"));
    }

    if (var != varIndex)
        FreeThenNull(varIndex);

    return val;
}

char *ExpressionCat(char *str1, char *str2) {
    char *str = NULL;

    if (str1) {
        if (str2 == NULL) {
            if (!(str = strdup(str1)))
                PANIC;
        } else {
            if (!(str = malloc(strlen(str1) + strlen(str2) + 1)))
                PANIC;
            strcpy(str, str1);
            strcat(str, str2);
        }
    } else if (str2) {
        if (!(str = strdup(str2)))
            PANIC;
    }

    return str;
}

static char *ExpressionCatThenNull(char **str1, char **str2) {
    char *str;

    if (*str1) {
        if (*str2 == NULL) {
            str = *str1;
            *str1 = NULL;
        } else {
            if (!(str = malloc(strlen(*str1) + strlen(*str2) + 1)))
                PANIC;
            strcpy(str, *str1);
            strcat(str, *str2);
            FreeThenNull(*str1);
            FreeThenNull(*str2);
        }
    } else if (*str2) {
        str = *str2;
        *str2 = NULL;
    } else {
        if (!(str = malloc(1)))
            PANIC;
        *str = 0;
    }

    return str;
}

void ExpressionSetInvalid(struct EXPRESSIONCTX *ec, const char *invalidMsg) {
    if (ec->invalidMsg == NULL) {
        /* Don't change the text if it's already set. */
        ec->invalidMsg = invalidMsg;
    }
    ec->tokenType = INVALID;
}

static BOOL ExpressionInvalid(struct EXPRESSIONCTX *ec) {
    return ec->tokenType == INVALID;
}

static double ExpressionAtoD(char *val) {
    double result;

    if (val == NULL)
        return 0;

    /* Skip $ to simplify money tests */
    while (*val == '$' || *val == ' ')
        val++;

    /* and remove "," to simplify money tests (need to copy to pull that off) */
    if (strchr(val, ',')) {
        char *valCopy, *p;
        if (!(valCopy = strdup(val)))
            PANIC;
        for (p = valCopy; *p;)
            if (*p == ',')
                StrCpyOverlap(p, p + 1);
            else
                p++;
        result = atof(valCopy);
        FreeThenNull(valCopy);
    } else {
        result = atof(val);
    }

    return result;
}

static int ExpressionAtoI(char *val) {
    int result;

    if (val == NULL)
        return 0;

    /* Skip $ to simplify money tests */
    while (*val == '$' || *val == ' ')
        val++;

    /* and remove "," to simplify money tests (need to copy to pull that off) */
    if (strchr(val, ',')) {
        char *valCopy, *p;
        if (!(valCopy = strdup(val)))
            PANIC;
        for (p = valCopy; *p;)
            if (*p == ',')
                StrCpyOverlap(p, p + 1);
            else
                p++;
        result = atoi(valCopy);
        FreeThenNull(valCopy);
    } else {
        result = atoi(val);
    }

    return result;
}

static int ExpressionStrCmp(char *str1, char *str2) {
    if (str1 == NULL)
        str1 = "";
    if (str2 == NULL)
        str2 = "";
    return strcmp(str1, str2);
}

char *ExpressionDouble(double r) {
    char *val;
    char *period;
    char *e;

    if (!(val = malloc(32)))
        PANIC;
    sprintf(val, "%f", r);
    if (period = strchr(val, '.')) {
        for (e = strchr(val, 0) - 1; *e == '0'; e--)
            *e = 0;
        if (*(period + 1) == 0)
            *period = 0;
    }
    if (!(val = realloc(val, strlen(val) + 1)))
        PANIC;
    return val;
}

char *ExpressionInt(int i) {
    char *val;

    if (!(val = malloc(32)))
        PANIC;
    sprintf(val, "%d", i);
    if (!(val = realloc(val, strlen(val) + 1)))
        PANIC;
    return val;
}

static void ExpressionSetToken(struct EXPRESSIONCTX *ec,
                               enum TOKENTYPE tokenType,
                               char *start,
                               int len) {
    if (start == NULL)
        len = 0;
    else if (len == -1)
        len = strlen(start);

    /* By testing for <=, we guarantee that ec->tokenMax will end up
       large enough to also hold final, terminating NULL
    */
    if (ec->tokenMax <= (size_t)len) {
        while (ec->tokenMax <= (size_t)len) {
            if (ec->tokenMax == 0)
                ec->tokenMax = 64;
            else
                ec->tokenMax *= 2;
        }

        if (!(ec->token = realloc(ec->token, ec->tokenMax)))
            PANIC;
    }

    if (len)
        memcpy(ec->token, start, len);
    ec->token[len] = 0;
    ec->prev = start;
    ec->curr = start + len;
    ec->tokenType = tokenType;
}

void ExpressionGetToken(struct EXPRESSIONCTX *ec) {
    char *start = SkipWS(ec->curr);
    char next;
    char *p;
    int i;
    enum TOKENTYPE tokenType;

    if (ec->tokenType == INVALID)
        return;

    ec->prev = start; /* get rid of whitespace */

    if (*start == 0) {
        if (ec->tokenType == END) {
            ExpressionSetInvalid(ec, "unexpected end of expression");
            return;
        }
        ExpressionSetToken(ec, END, start, 0);
        return;
    }

    next = *(start + 1);

    if (IsDigit(*start)) {
        i = 0;
        for (p = start; IsDigit(*p) || *p == '.'; p++) {
            if (*p == '.') {
                i++;
                if (i == 2) {
                    break;
                }
            }
        }
        ExpressionSetToken(ec, NUMBER, start, p - start);
        return;
    }

    if (*start == '"' || *start == '\'') {
        ExpressionSetToken(ec, STRING, start, 1);
        for (p = start + 1; *p; p++) {
            if (*p == '\\' && *(p + 1) != 0)
                p++;
            else if (*p == *start) {
                ExpressionSetToken(ec, STRING, start + 1, p - start - 1);
                ec->curr = p + 1;
                for (p = ec->token; *p; p++) {
                    if (*p == '\\' && *(p + 1)) {
                        StrCpyOverlap(p, p + 1);
                    }
                }
                return;
            }
        }
        ExpressionSetInvalid(ec, "unmatched quote");
        return;
    }

    tokenType = NONE;

    if (next == '=') {
        switch (*start) {
        case '<':
            tokenType = NUMLE;
            break;
        case '=':
            tokenType = NUMEQ;
            break;
        case '>':
            tokenType = NUMGE;
            break;
        case '!':
            tokenType = NUMNE;
            break;
        case '+':
            tokenType = ASSIGN_ADD;
            break;
        case '-':
            tokenType = ASSIGN_SUB;
            break;
        case '*':
            tokenType = ASSIGN_MUL;
            break;
        case '/':
            tokenType = ASSIGN_DIV;
            break;
        case '.':
            tokenType = ASSIGN_CAT;
            break;
        }

        if (tokenType != NONE) {
            ExpressionSetToken(ec, tokenType, start, 2);
            return;
        }
    }

    if (next == '~') {
        if (*start == '=') {
            if ((*(start + 2)) == '<') {
                if ((*(start + 3)) == '>') {
                    ExpressionSetToken(ec, XMLREMATCH, start, 4);
                    return;
                }
            } else {
                ExpressionSetToken(ec, REMATCH, start, 2);
                return;
            }
        }
        if (*start == '!') {
            if ((*(start + 2)) == '<') {
                if ((*(start + 3)) == '>') {
                    ExpressionSetToken(ec, XMLNOREMATCH, start, 4);
                    return;
                }
            } else {
                ExpressionSetToken(ec, NOREMATCH, start, 2);
                return;
            }
        }
    }

    if (*start == '=' && next == '*') {
        ExpressionSetToken(ec, WILDMATCH, start, 2);
        return;
    }

    switch (*start) {
    case '&':
        if (next == '&') {
            ExpressionSetToken(ec, AND, start, 2);
            return;
        }
        ExpressionSetToken(ec, BITWISEAND, start, 1);
        return;

    case '|':
        if (next == '|') {
            ExpressionSetToken(ec, OR, start, 2);
            return;
        }
        ExpressionSetToken(ec, BITWISEOR, start, 1);
        return;

    case '=':
    case '<':
        if (next == '<') {
            ExpressionSetToken(ec, INVALID, start, 2);
            return;
        }

    case '>':
        if (next == '>') {
            ExpressionSetToken(ec, INVALID, start, 2);
            return;
        }

    case '+':
        if (next == '+') {
            ExpressionSetToken(ec, INVALID, start, 2);
            return;
        }

    case '-':
        if (next == '-') {
            ExpressionSetToken(ec, INVALID, start, 2);
            return;
        }

    case '*':
        if (next == '*') {
            ExpressionSetToken(ec, INVALID, start, 2);
            return;
        }

    case '/':
    case '%':
    case '(':
    case ')':
    case ',':
    case '!':
    case '[':
    case ']':
    case '.':
        if (next == '.') {
            ExpressionSetToken(ec, INVALID, start, 2);
            return;
        }

    case '?':
    case ':':
        ExpressionSetToken(ec, *start, start, 1);
        return;
    }

    tokenType = IsVarNameOrCompareOper(start, &p);
    ExpressionSetToken(ec, tokenType, start, max(1, p - start));
    if (tokenType != INVALID) {
        if (ec->tokenType == NAMEWITHQUOTES) {
            /* remove the quotes and the protective slashes. */
            char quote = 0;
            BOOL unbalancedQuote = FALSE;

            for (p = ec->token;
                 (*p != 0 && ((strchr("'\"", *p) != NULL) && (quote = *p))) || (*p != 0);) {
                if (*p == quote) {
                    StrCpyOverlap(p, p + 1);
                    for (; *p && (*p != quote);) {
                        if (*p == '\\' && *(p + 1)) {
                            StrCpyOverlap(p, p + 1);
                        }
                        p++;
                    }
                    if (*p) {
                        StrCpyOverlap(p, p + 1);
                    } else {
                        unbalancedQuote = TRUE;
                    }
                    quote = 0;
                } else {
                    p++;
                }
            }
            if (unbalancedQuote) {
                ExpressionSetInvalid(ec, "unmatched quote");
            } else {
                ec->tokenType = NAME;
            }
        }
    }
}

BOOL ExtendedVariableName(char *start, int *extended, int *superExtended, char **p) {
    char *p2;

    if ((p2 = StartsWith(start, "group:")) != NULL || (p2 = StartsWith(start, "http:")) != NULL ||
        (p2 = StartsWith(start, "cookie:")) != NULL) {
        *extended = TRUE;
        *p = p2;
        if ((p2 = StartsWith(*p, "xpath:")) != NULL) {
            *superExtended = TRUE;
            *p = p2;
        }
    } else if ((p2 = StartsWith(start, "auth:")) != NULL ||
               (p2 = StartsWith(start, "env:")) != NULL ||
               (p2 = StartsWith(start, "ParseName:")) != NULL ||
               (p2 = StartsWith(start, "session:")) != NULL ||
               (p2 = StartsWith(start, "login:")) != NULL ||
               (p2 = StartsWith(start, "db:")) != NULL || (p2 = StartsWith(start, "re:")) != NULL) {
        *superExtended = TRUE;
        *p = p2;
    }
    return *extended || *superExtended;
}

enum TOKENTYPE IsVarNameOrCompareOper(char *start, char **end) {
    char *p;
    enum TOKENTYPE tokenType = INVALID;
    int tokenLength = 0;
    BOOL moreThanJustAPrefix = FALSE;
    BOOL nameWithQuotes = FALSE;

    p = start;
    if (IsAlpha(*start)) {
        char quote = 0;
        BOOL extended = FALSE;
        BOOL superExtended = FALSE;
        ExtendedVariableName(start, &extended, &superExtended, &p);
        for (; IsAlpha(*p) || IsDigit(*p) || *p == ':' || *p == '_' || *p == '@' ||
               (extended && *p != 0 && (strchr("!#$%&*+-./^|~", *p) != NULL)) ||
               (superExtended && *p != 0 && (strchr("'\"", *p) != NULL) && (quote = *p));) {
            moreThanJustAPrefix = TRUE;
            if (*p == quote) {
                nameWithQuotes = TRUE;
                p++;
                while (*p && (*p != quote)) {
                    if (*p == '\\' && *(p + 1)) {
                        p++;
                    }
                    p++;
                }
                if (*p == quote) {
                    p++;
                }
                quote = 0;
            } else {
                p++;
            }
        }
        tokenLength = p - start;
        if ((tokenLength == 2) && (!nameWithQuotes)) {
            if (strnicmp(start, "LT", 2) == 0) {
                tokenType = STRLT;
            } else if (strnicmp(start, "LE", 2) == 0) {
                tokenType = STRLE;
            } else if (strnicmp(start, "EQ", 2) == 0) {
                tokenType = STREQ;
            } else if (strnicmp(start, "NE", 2) == 0) {
                tokenType = STRNE;
            } else if (strnicmp(start, "GT", 2) == 0) {
                tokenType = STRGT;
            } else if (strnicmp(start, "GE", 2) == 0) {
                tokenType = STRGE;
            }
        }

        if ((tokenType == INVALID) && (tokenLength > 0) && moreThanJustAPrefix) {
            if (nameWithQuotes) {
                tokenType = NAMEWITHQUOTES;
            } else {
                tokenType = NAME;
            }
        }
    }

    *end = p;
    return tokenType;
}

BOOL IsScalarVarName(char *start) {
    char *end;
    enum TOKENTYPE tokenType = IsVarNameOrCompareOper(start, &end);
    if ((tokenType == NAME) && (*end == 0)) {
        return TRUE;
    } else {
        return FALSE;
    }
}

BOOL IsVarName(char *start) {
    char *end;
    enum TOKENTYPE tokenType = IsVarNameOrCompareOper(start, &end);
    if (tokenType != NAME) {
        return FALSE;
    } else if (*end == 0) {
        return TRUE;
    } else if (*end == '[') {
        if (strchr(end, ']') == (strchr(end, 0) - 1)) {
            return TRUE;
        } else {
            return FALSE;
        }
    } else {
        return FALSE;
    }
}

BOOL ExpressionValueTrue(char *val) {
    char *p;

    if (val == NULL || *val == 0)
        return FALSE;

    for (p = val; *p; p++)
        if (*p != '0' && *p != '.')
            return TRUE;

    return FALSE;
}

static char *ExpressionComma(struct EXPRESSIONCTX *ec, BOOL get);
static char *ExpressionTerm(struct EXPRESSIONCTX *ec, BOOL get);

#define EXPECTED_A "expected a"
BOOL ExpressionCheckToken(struct EXPRESSIONCTX *ec, enum TOKENTYPE tokenType) {
    if (ec->tokenType == tokenType)
        return 1;

    switch (tokenType) {
    case NAME:
        ExpressionSetInvalid(ec, EXPECTED_A " variable name");
        break;

    case NUMBER:
        ExpressionSetInvalid(
            ec,
            EXPECTED_A " numeric literal (a sequence of digits with an optional decimal point) ");
        break;

    case STRING:
        ExpressionSetInvalid(
            ec, EXPECTED_A " string literal (text surrounded with either quotes or apostrophes)");
        break;

    case END:
        ExpressionSetInvalid(ec, "unexpected text");
        break;

    case NUMLE:
        ExpressionSetInvalid(ec, EXPECTED_A " numeric less-than-or-equal operator (<=)");
        break;

    case NUMGE:
        ExpressionSetInvalid(ec, EXPECTED_A " numeric greater-than-or-equal operator (>=)");
        break;

    case NUMEQ:
        ExpressionSetInvalid(ec, EXPECTED_A " numeric equal-to operator (==)");
        break;

    case NUMNE:
        ExpressionSetInvalid(ec, EXPECTED_A " numeric not-equal operator (!=)");
        break;

    case STRLT:
        ExpressionSetInvalid(ec, EXPECTED_A " string less-than matching operator (LT)");
        break;

    case STRLE:
        ExpressionSetInvalid(ec, EXPECTED_A " string less-than-or-equal matching operator (LE)");
        break;

    case STREQ:
        ExpressionSetInvalid(ec, EXPECTED_A " string equal-to matching operator (EQ)");
        break;

    case STRNE:
        ExpressionSetInvalid(ec, EXPECTED_A " string not-equal matching operator (NE)");
        break;

    case STRGT:
        ExpressionSetInvalid(ec, EXPECTED_A " string greater-than matching operator (GT)");
        break;

    case STRGE:
        ExpressionSetInvalid(ec, EXPECTED_A " string greater-than-or-equal matching operator (GE)");
        break;

    case WILDMATCH:
        ExpressionSetInvalid(ec, EXPECTED_A " wild-card matching operator (=*)");
        break;

    case REMATCH:
        ExpressionSetInvalid(ec, EXPECTED_A " regular expression matching operator (=~)");
        break;

    case NOREMATCH:
        ExpressionSetInvalid(ec, EXPECTED_A " negated regular expression matching operator (!~)");
        break;

    case XMLREMATCH:
        ExpressionSetInvalid(ec, EXPECTED_A "n XML regular expression matching operator (=~<>)");
        break;

    case XMLNOREMATCH:
        ExpressionSetInvalid(ec,
                             EXPECTED_A " negated XML regular expression matching operator (!~<>)");
        break;

    case AND:
        ExpressionSetInvalid(ec, EXPECTED_A " logical 'and' operator (&&)");
        break;

    case OR:
        ExpressionSetInvalid(ec, EXPECTED_A " logical 'or' operator (||)");
        break;

    case ASSIGN_ADD:
        ExpressionSetInvalid(ec, EXPECTED_A " numerical addition-assignment operator (+=)");
        break;

    case ASSIGN_SUB:
        ExpressionSetInvalid(ec, EXPECTED_A " numerical subtraction-assignment operator (-=)");
        break;

    case ASSIGN_MUL:
        ExpressionSetInvalid(ec, EXPECTED_A " numerical multiplication-assignment operator (*=)");
        break;

    case ASSIGN_DIV:
        ExpressionSetInvalid(ec, EXPECTED_A " numerical division-assignment operator (/=)");
        break;

    case ASSIGN_CAT:
        ExpressionSetInvalid(ec, EXPECTED_A " string concatenation operator (.)");
        break;

    case PLUS:
        ExpressionSetInvalid(ec, EXPECTED_A " numerical addition operator (+)");
        break;

    case PLUS_PLUS:
        ExpressionSetInvalid(ec, "not yet implemented (++)");
        break;

    case MINUS:
        ExpressionSetInvalid(ec, EXPECTED_A " numerical subtraction operator (-)");
        break;

    case MINUS_MINUS:
        ExpressionSetInvalid(ec, "not yet implemented (--)");
        break;

    case MULTIPLY:
        ExpressionSetInvalid(ec, EXPECTED_A " numerical multiplication operator (*)");
        break;

    case DIVIDE:
        ExpressionSetInvalid(ec, EXPECTED_A " numerical division operator (/)");
        break;

    case BITWISEAND:
        ExpressionSetInvalid(ec, EXPECTED_A " bitwise 'and' operator (&)");
        break;

    case BITWISEOR:
        ExpressionSetInvalid(ec, EXPECTED_A " bitwise 'or' operator (|)");
        break;

    case MODULUS:
        ExpressionSetInvalid(ec, EXPECTED_A " modulo operation (%)");
        break;

    case ASSIGN:
        ExpressionSetInvalid(ec, EXPECTED_A "n assignment operator (=)");
        break;

    case LHPAREN:
        ExpressionSetInvalid(ec, EXPECTED_A " left parenthesis");
        break;

    case RHPAREN:
        ExpressionSetInvalid(ec, EXPECTED_A " right parenthesis");
        break;

    case QUESTION:
        ExpressionSetInvalid(ec, EXPECTED_A " question mark (?)");
        break;

    case COLON:
        ExpressionSetInvalid(ec, EXPECTED_A " colon (:)");
        break;

    case COMMA:
        ExpressionSetInvalid(ec, EXPECTED_A " comma (,)");
        break;

    case NOT:
        ExpressionSetInvalid(ec, EXPECTED_A " logical negation operator (!)");
        break;

    case LHBRACKET:
        ExpressionSetInvalid(ec, EXPECTED_A " left bracket ([)");
        break;

    case RHBRACKET:
        ExpressionSetInvalid(ec, EXPECTED_A " right bracket (])");
        break;

    case NUMLT:
        ExpressionSetInvalid(ec, EXPECTED_A " numeric less-than operator (<)");
        break;

    case NUMGT:
        ExpressionSetInvalid(ec, EXPECTED_A " numeric greater-than operator (>)");
        break;

    case PERIOD:
        ExpressionSetInvalid(ec, EXPECTED_A " period (.)");
        break;

    default:
        ExpressionSetInvalid(ec, "unrecognized in this context");
        break;
    }

    return 0;
}

char *ExpressionStrdup(const char *val) {
    char *dup;
    /* If NULL provided, give back a copy of an empty string instead of NULL */
    if (!(dup = strdup(val ? val : "")))
        PANIC;
    return dup;
}

static BOOL ExpressionReMatch(struct EXPRESSIONCTX *ec, char *subject, char *re, int options) {
    BOOL result = 0;
    pcre *rePattern = NULL;
    int nmat;
    int ovec[30]; /* 10 slots * 3 requires entries per slot */
    int i;
    char var[16];
    char *error = NULL;

    if (error = ParseRegularExpression(re, &rePattern, NULL, NULL, NULL, NULL)) {
        Log("=~ %s", error);
        FreeThenNull(error);
        ExpressionSetInvalid(ec, "invalid regular expression");
        goto Finished;
    }

    nmat = pcre_exec(rePattern, NULL, subject, strlen(subject), 0, 0, ovec,
                     sizeof(ovec) / sizeof(int));

    if (nmat >= 0) {
        for (i = 0; i < 10; i++) {
            sprintf(var, "re:%d", i);
            if (i < nmat) {
                char *val;
                pcre_get_substring(subject, ovec, sizeof(ovec) / sizeof(int), i,
                                   (const char **)&val);
                VariablesSetValue(ec->t->localVariables, var, val);
                FreeThenNull(val);
            } else {
                VariablesDelete(ec->t->localVariables, var);
            }
        }
        result = 1;
    }

Finished:
    if (rePattern) {
        pcre_free(rePattern);
        rePattern = NULL;
    }

    return result;
}

static BOOL ExpressionXmlReMatch(struct EXPRESSIONCTX *ec, char *subject, char *re, int options) {
#define NULLValue "NULL"
#define PNS(x) ((char *)(x) ? (char *)(x) : (char *)NULLValue)
    BOOL result = 0;
    xmlRegexpPtr xrp = NULL;
    int i;

    xrp = xmlRegexpCompile(BAD_CAST re);
    if (xrp == NULL) {
        Log("xmlRegexpCompile failed for %s", PNS(re));
        ExpressionSetInvalid(ec, "invalid regular XML expression");
    } else {
        i = xmlRegexpExec(xrp, BAD_CAST subject);
        if (i < 0) {
            Log("xmlRegexpExec failed for %s", PNS(re));
            ExpressionSetInvalid(ec, "invalid regular XML expression");
        }
        result = i == 1 ? TRUE : FALSE;
        xmlRegFreeRegexp(xrp);
    }
    return result;
}

static void ExpressionPrimaryAssign(struct EXPRESSIONCTX *ec, char *var, char *index, char *val) {
    char *subVar;

    if (!ec->shortCircuiting && ec->tokenType != INVALID) {
        char *varIndex = ExpressionVariableIndex(var, index);

        if (ec->uip && ec->uip->debug) {
            UsrLog(ec->uip, "assignment %s=\"%s\"", varIndex, val);
        } else if (debugLevel > 10) {
            Log("assignment %s=\"%s\"", varIndex, val);
        }

        if (stricmp(var, "login:user") == 0) {
            if (ec->uip) {
                char *valEdited = NULL;
                if (val) {
                    PANIC_IF_NULL(valEdited = strdup(val));
                    Trim(valEdited, TRIM_CTRL);
                }
                if (valEdited && *valEdited) {
                    StrCpy3(ec->uip->altUser, valEdited, sizeof(ec->uip->altUser));
                    StrCpy3(ec->uip->logUser, valEdited, sizeof(ec->uip->logUser));
                    ec->uip->user = ec->uip->altUser;
                } else {
                    ec->uip->user = "";
                    ec->uip->logUser[0] = 0;
                }
                FreeThenNull(valEdited);
            }
        } else if (stricmp(var, "login:loguser") == 0) {
            if (ec->uip) {
                char *valEdited = NULL;
                if (val) {
                    PANIC_IF_NULL(valEdited = strdup(val));
                    Trim(valEdited, TRIM_CTRL);
                }
                if (valEdited && *valEdited) {
                    StrCpy3(ec->uip->logUser, valEdited, sizeof(ec->uip->logUser));
                    ec->uip->logUserAllowed = TRUE;
                } else {
                    ec->uip->logUser[0] = 0;
                }
                FreeThenNull(valEdited);
            }
        } else if ((subVar = StartsWith(var, "auth:")) != NULL) {
            if (ec->uip) {
                if (ec->uip->authSetValue != NULL) {
                    ec->uip->authSetValue(ec->t, ec->uip, ec->context, subVar, index, val);
                } else {
                    UsrLog(ec->uip, "Can't set because %s is a read-only variable.", varIndex);
                }
            }
        } else if (ExpressionReadOnlyVariable(ec, var, index)) {
            UsrLog(ec->uip, "Can't set because %s is a read-only variable.", varIndex);
        } else {
            VariablesSetValue(ec->t->localVariables, varIndex, val);
        }

        if (var != varIndex) {
            FreeThenNull(varIndex);
        }
    }
}

static char *ExpressionPrimary(struct EXPRESSIONCTX *ec, BOOL get) {
    struct EXPRESSIONFUNC *func;
    char *val = NULL;
    char *var = NULL;
    char *index = NULL;
    double result;
    char *otherVal = NULL;

    if (get)
        ExpressionGetToken(ec);

    switch (ec->tokenType) {
    case NUMBER:
    case STRING:
        if (!(val = strdup(ec->token)))
            PANIC;
        ExpressionGetToken(ec);
        return val;

    case BITWISEAND:
        /* like the address-of operator in "C" (&). */
        ExpressionGetToken(ec);
        if (!ExpressionCheckToken(ec, NAME)) {
            ExpressionSetInvalid(ec, "misplaced name-of-variable operator (&)");
            return NULL;
        }
        if (!(var = strdup(ec->token)))
            PANIC;
        ExpressionGetToken(ec);
        if (ec->tokenType == LHBRACKET) {
            index = ExpressionExpression(ec, TRUE);
            if (ExpressionCheckToken(ec, RHBRACKET))
                ExpressionGetToken(ec);
        }
        val = ExpressionVariableIndex(var, index);
        if (val != var) {
            FreeThenNull(var);
        }
        FreeThenNull(index);
        return val;

    case NAME:
        if (!(var = strdup(ec->token)))
            PANIC;
        ExpressionGetToken(ec);

        if (ec->tokenType == LHPAREN) {
            for (func = expressionFuncs; func->funcName; func++) {
                if (strcmp(var, func->funcName) == 0) {
                    FreeThenNull(var);
                    val = (func->func)(ec);
                    if (ExpressionCheckToken(ec, RHPAREN))
                        ExpressionGetToken(ec);
                    return val;
                }
            }

            ec->invalidMsg = "unknown function";
            FreeThenNull(var);
            return NULL;
        }

        if (ec->tokenType == LHBRACKET) {
            index = ExpressionExpression(ec, TRUE);
            if (ExpressionCheckToken(ec, RHBRACKET))
                ExpressionGetToken(ec);
        }

        if (ec->tokenType != ASSIGN)
            val = ExpressionVariable(ec, var, index, TRUE);

        switch (ec->tokenType) {
        case ASSIGN:
            FreeThenNull(val);
            val = ExpressionExpression(ec, TRUE);
            ExpressionPrimaryAssign(ec, var, index, val);
            break;

        case ASSIGN_ADD:
            result = ExpressionAtoD(val);
            if (otherVal = ExpressionTerm(ec, TRUE))
                result += ExpressionAtoD(otherVal);
            FreeThenNull(val);
            FreeThenNull(otherVal);
            val = ExpressionDouble(result);
            ExpressionPrimaryAssign(ec, var, index, val);
            break;

        case ASSIGN_SUB:
            result = ExpressionAtoD(val);
            if (otherVal = ExpressionTerm(ec, TRUE))
                result -= ExpressionAtoD(otherVal);
            FreeThenNull(val);
            FreeThenNull(otherVal);
            ExpressionPrimaryAssign(ec, var, index, val);
            break;

        case ASSIGN_MUL:
            result = ExpressionAtoD(val);
            if (otherVal = ExpressionTerm(ec, TRUE))
                result *= ExpressionAtoD(otherVal);
            FreeThenNull(val);
            FreeThenNull(otherVal);
            val = ExpressionDouble(result);
            ExpressionPrimaryAssign(ec, var, index, val);
            break;

        case ASSIGN_DIV:
            result = ExpressionAtoD(val);
            if (otherVal = ExpressionTerm(ec, TRUE)) {
                if (ExpressionAtoD(otherVal) == 0) {
                    if (ec->shortCircuiting) {
                        val = ExpressionDouble(0);
                    } else {
                        ExpressionSetInvalid(ec, "divide by zero");
                    }
                } else {
                    result /= ExpressionAtoD(otherVal);
                    FreeThenNull(val);
                    val = ExpressionDouble(result);
                    ExpressionPrimaryAssign(ec, var, index, val);
                }
                FreeThenNull(otherVal);
            }
            break;

        case ASSIGN_CAT:
            if (otherVal = ExpressionTerm(ec, TRUE)) {
                val = ExpressionCatThenNull(&val, &otherVal);
                ExpressionPrimaryAssign(ec, var, index, val);
            }
            break;

        default:
            break;
        }

        FreeThenNull(var);
        FreeThenNull(index);
        return val;

    case LHPAREN:
        val = ExpressionComma(ec, TRUE);
        if (ExpressionCheckToken(ec, RHPAREN))
            ExpressionGetToken(ec);
        return val;

    case NOT:
        val = ExpressionPrimary(ec, TRUE);
        result = val == NULL || atoi(val) == 0 ? 1 : 0;
        FreeThenNull(val);
        val = ExpressionDouble(result);
        return val;

    case MINUS:
        val = ExpressionPrimary(ec, TRUE);
        result = -ExpressionAtoD(val);
        FreeThenNull(val);
        val = ExpressionDouble(result);
        return val;

    default:
        break;
    }

    if (ec->tokenType == END) {
        ExpressionSetInvalid(ec, "unexpected end of expression");
    } else {
        ExpressionSetInvalid(ec, "unrecognized in this context");
    }

    return NULL;
}

static char *ExpressionTerm(struct EXPRESSIONCTX *ec, BOOL get) {
    char *val = ExpressionPrimary(ec, get);
    char *otherVal;
    double result = 0.0;
    int mod;

    for (;;) {
        switch (ec->tokenType) {
        case MULTIPLY:
            otherVal = ExpressionPrimary(ec, TRUE);
            result = ExpressionAtoD(val) * ExpressionAtoD(otherVal);
            break;

        case DIVIDE:
            otherVal = ExpressionPrimary(ec, TRUE);
            result = ExpressionAtoD(otherVal);
            if (result == 0) {
                if (ec->shortCircuiting)
                    result = 0;
                else
                    ExpressionSetInvalid(ec, "divide by zero");
            } else {
                result = ExpressionAtoD(val) / result;
            }
            break;

        case MODULUS:
            otherVal = ExpressionPrimary(ec, TRUE);
            mod = ExpressionAtoI(otherVal);
            if (mod == 0) {
                if (ec->shortCircuiting)
                    result = 0;
                else
                    ExpressionSetInvalid(ec, "modulus by zero");
            } else {
                result = ExpressionAtoI(val) % mod;
            }
            break;

        default:
            return val;
        }

        FreeThenNull(val);
        FreeThenNull(otherVal);
        if (ExpressionInvalid(ec))
            return NULL;

        val = ExpressionDouble(result);
    }
}

static char *ExpressionAddSubtract(struct EXPRESSIONCTX *ec, BOOL get) {
    char *val = ExpressionTerm(ec, get);
    char *otherVal = NULL;
    char *newVal = NULL;
    double result = 0.0;
    int iresult;

    for (;;) {
        switch (ec->tokenType) {
        case PLUS:
            result = ExpressionAtoD(val);
            if (otherVal = ExpressionTerm(ec, TRUE))
                result += ExpressionAtoD(otherVal);
            break;

        case MINUS:
            result = ExpressionAtoD(val);
            if (otherVal = ExpressionTerm(ec, TRUE))
                result -= ExpressionAtoD(otherVal);
            break;

        case BITWISEAND:
            iresult = ExpressionAtoI(val);
            if (otherVal = ExpressionTerm(ec, TRUE))
                result = iresult & ExpressionAtoI(otherVal);
            else
                result = 0;
            break;

        case BITWISEOR:
            iresult = ExpressionAtoI(val);
            if (otherVal = ExpressionTerm(ec, TRUE))
                result = iresult | ExpressionAtoI(otherVal);
            break;

        case PERIOD:
            otherVal = ExpressionTerm(ec, TRUE);
            newVal = ExpressionCatThenNull(&val, &otherVal);
            break;

        default:
            return val;
        }

        if (newVal) {
            val = newVal;
            newVal = NULL;
        } else {
            FreeThenNull(val);
            FreeThenNull(otherVal);
            val = ExpressionDouble(result);
        }
    }
}

static char *ExpressionComparison(struct EXPRESSIONCTX *ec, BOOL get) {
    char *val = ExpressionAddSubtract(ec, get);
    char *otherVal = NULL;
    BOOL result;

    if (ExpressionInvalid(ec))
        return val;

    for (;;) {
        switch (ec->tokenType) {
        case NUMLT:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : ExpressionAtoD(val) < ExpressionAtoD(otherVal);
            break;

        case NUMLE:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : ExpressionAtoD(val) <= ExpressionAtoD(otherVal);
            break;

        case NUMEQ:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : ExpressionAtoD(val) == ExpressionAtoD(otherVal);
            break;

        case NUMNE:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : ExpressionAtoD(val) != ExpressionAtoD(otherVal);
            break;

        case NUMGT:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : ExpressionAtoD(val) > ExpressionAtoD(otherVal);
            break;

        case NUMGE:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : ExpressionAtoD(val) >= ExpressionAtoD(otherVal);
            ;
            break;

        case STRLT:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : ExpressionStrCmp(val, otherVal) < 0;
            break;

        case STRLE:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : ExpressionStrCmp(val, otherVal) <= 0;
            break;

        case STREQ:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : ExpressionStrCmp(val, otherVal) == 0;
            break;

        case STRNE:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : ExpressionStrCmp(val, otherVal) != 0;
            break;

        case STRGT:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : ExpressionStrCmp(val, otherVal) > 0;
            break;

        case STRGE:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : ExpressionStrCmp(val, otherVal) >= 0;
            break;

        case WILDMATCH:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : WildCompare(val, otherVal);
            break;

        case REMATCH:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : ExpressionReMatch(ec, val, otherVal, 0);
            break;

        case NOREMATCH:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : (!ExpressionReMatch(ec, val, otherVal, 0));
            break;

        case XMLREMATCH:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : ExpressionXmlReMatch(ec, val, otherVal, 0);
            break;

        case XMLNOREMATCH:
            otherVal = ExpressionAddSubtract(ec, TRUE);
            result = ExpressionInvalid(ec) ? 0 : (!ExpressionXmlReMatch(ec, val, otherVal, 0));
            break;

        default:
            return val;
        }

        FreeThenNull(val);
        FreeThenNull(otherVal);
        val = ExpressionDouble(result ? 1 : 0);
    }
}

static char *ExpressionLogical(struct EXPRESSIONCTX *ec, BOOL get) {
    char *otherVal = NULL;
    char *val = ExpressionComparison(ec, get);
    int leftTrue;

    if (ExpressionInvalid(ec))
        return val;

    for (;;) {
        switch (ec->tokenType) {
        case AND:
            leftTrue = ExpressionValueTrue(val);
            if (!leftTrue)
                ec->shortCircuiting++;
            otherVal = ExpressionComparison(ec, TRUE);
            /* For AND, if left true, return right as value, otherwise change to NULL which becomes
             * "" */
            if (!leftTrue) {
                ec->shortCircuiting--;
                FreeThenNull(val);
                FreeThenNull(otherVal);
            } else {
                FreeThenNull(val);
                val = otherVal;
                otherVal = NULL;
            }
            break;

        case OR:
            leftTrue = ExpressionValueTrue(val);
            if (leftTrue)
                ec->shortCircuiting++;
            otherVal = ExpressionComparison(ec, TRUE);
            /* For OR, if left true, return left as value, otherwise return right as value */
            if (leftTrue) {
                ec->shortCircuiting--;
                FreeThenNull(otherVal);
            } else {
                FreeThenNull(val);
                val = otherVal;
                otherVal = NULL;
            }
            break;

        default:
            return val;
        }

        if (val == NULL) {
            if (!(val = strdup("")))
                PANIC;
        }
    }
}

static char *ExpressionTernary(struct EXPRESSIONCTX *ec, BOOL get) {
    char *val = ExpressionLogical(ec, get);
    BOOL isTrue;
    char *leftVal = NULL, *rightVal = NULL;

    if (ExpressionInvalid(ec))
        return val;

    if (ec->tokenType == QUESTION) {
        isTrue = ExpressionValueTrue(val);
        FreeThenNull(val);

        if (!isTrue)
            ec->shortCircuiting++;

        leftVal = ExpressionExpression(ec, TRUE);

        if (ExpressionInvalid(ec) || !ExpressionCheckToken(ec, COLON))
            return leftVal;

        /* If it is true, short-circuit the next token, but if it is false,
           we were short-circuiting and need to stop
        */
        if (isTrue) {
            ec->shortCircuiting++;
        } else {
            ec->shortCircuiting--;
        }

        rightVal = ExpressionExpression(ec, TRUE);

        if (isTrue) {
            ec->shortCircuiting--;
            val = leftVal;
            leftVal = NULL;
            FreeThenNull(rightVal);
        } else {
            val = rightVal;
            FreeThenNull(leftVal);
            rightVal = NULL;
        }
    }

    return val;
}

char *ExpressionExpression(struct EXPRESSIONCTX *ec, BOOL get) {
    return ExpressionTernary(ec, get);
}

static char *ExpressionComma(struct EXPRESSIONCTX *ec, BOOL get) {
    char *val = ExpressionExpression(ec, get);

    for (;;) {
        if (ExpressionInvalid(ec))
            return val;
        if (ec->tokenType != COMMA)
            return val;
        FreeThenNull(val);
        val = ExpressionExpression(ec, TRUE);
    }
}

static char *ExpressionEvaluate(struct EXPRESSIONCTX *ec, BOOL get) {
    char *val = ExpressionComma(ec, get);

    if (ExpressionInvalid(ec))
        return val;

    ExpressionCheckToken(ec, END);

    return val;
}

char *ExpressionValueReportError(struct TRANSLATE *t,
                                 struct USERINFO *uip,
                                 void *context,
                                 char *expr,
                                 BOOL reportErrors) {
    struct EXPRESSIONCTX ec;
    char *val = NULL;
    struct USERINFO ui;

    if (uip == NULL) {
        uip = &ui;
        InitUserInfo(t, uip, NULL, NULL, NULL, NULL);
    } else {
        ui.gm = NULL;
    }

    ec.t = t;
    ec.uip = uip;
    ec.context = context;
    ec.start = ec.curr = expr;
    ec.tokenType = NONE;
    ec.invalidMsg = NULL;
    ec.token = NULL;
    ec.tokenMax = 0;
    ec.shortCircuiting = 0;

    val = ExpressionEvaluate(&ec, TRUE);
    if (ExpressionInvalid(&ec)) {
        FreeThenNull(val);
        if (reportErrors) {
            if (uip->debug) {
                ReportErrorInPlace(uip, &ec);
            }
        }
    }

    FreeThenNull(ec.token);

    if (ui.gm)
        GroupMaskFree(&ui.gm);

    return val;
}

char *ExpressionValue(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *expr) {
    return ExpressionValueReportError(t, uip, context, expr, TRUE);
}

char *ExpressionVariableValue(struct TRANSLATE *t,
                              struct USERINFO *uip,
                              void *context,
                              char *var,
                              char *index,
                              BOOL nullToEmptyString) {
    struct EXPRESSIONCTX ec;
    char *val = NULL;
    char emptyString[1] = {0};

    ec.t = t;
    ec.uip = uip;
    ec.context = context;
    ec.start = ec.curr = emptyString;
    ec.tokenType = NONE;
    ec.invalidMsg = NULL;
    ec.token = NULL;
    ec.tokenMax = 0;
    ec.shortCircuiting = 0;

    val = ExpressionVariable(&ec, var, index, nullToEmptyString);
    if (ExpressionInvalid(&ec)) {
        FreeThenNull(val);
        ReportErrorInPlace(uip, &ec);
    }

    FreeThenNull(ec.token);

    return val;
}

char **ExpressionValueList(struct TRANSLATE *t,
                           struct USERINFO *uip,
                           void *context,
                           char *exprCommaList) {
    struct EXPRESSIONCTX ec;
    char **valList;
    char **valListTmp;
    int valListLen = 0;
    int valListLimit = 64;
    char *val = NULL;
    struct USERINFO ui;

    if (!(valList = calloc(valListLimit, sizeof(val))))
        PANIC;

    if (uip == NULL) {
        uip = &ui;
        InitUserInfo(t, uip, NULL, NULL, NULL, NULL);
    } else {
        ui.gm = NULL;
    }

    ec.t = t;
    ec.uip = uip;
    ec.context = context;
    ec.start = ec.curr = exprCommaList;
    ec.tokenType = NONE;
    ec.invalidMsg = NULL;
    ec.token = NULL;
    ec.tokenMax = 0;
    ec.shortCircuiting = 0;

    ExpressionGetToken(&ec);
    if (ec.tokenType != END) {
        val = ExpressionExpression(&ec, FALSE);
        for (;;) {
            if (ExpressionInvalid(&ec)) {
                if (valListLen == 0) {
                    valList[valListLen] = NULL;
                    valListLen++;
                }
                break;
            } else {
                valList[valListLen] = val;
                val = NULL;
                valListLen++;
                if (valListLen == valListLimit) {
                    valListLimit += 16;
                    valListTmp = valList;
                    if (!(valList = calloc(valListLimit, sizeof(val))))
                        PANIC;
                    memcpy(valList, valListTmp, (valListLimit - 16) * sizeof(val));
                }
                if (ec.tokenType != COMMA) {
                    break;
                }
                val = ExpressionExpression(&ec, TRUE);
            }
        }
    }
    ExpressionCheckToken(&ec, END);
    if (ExpressionInvalid(&ec)) {
        FreeThenNull(val);
        for (val = valList[valListLen]; val; val = valList[--valListLen]) {
            FreeThenNull(val);
        }
        FreeThenNull(valList);
        ReportErrorInPlace(uip, &ec);
    }

    FreeThenNull(ec.token);
    FreeThenNull(val);

    if (ui.gm)
        GroupMaskFree(&ui.gm);

    return valList;
}
