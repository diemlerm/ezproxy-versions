/**
 * Core user authentication module (processes user.txt)
 *
 * This is the core module for user authentication.  It is called from sections
 * of EZproxy that require user authentication processing.  It is responsible
 * for dispatching user authentication requests off to related modules.
 *
 * The user authentication modules called by this routine follow the naming
 * convention usr***.c.
 */

#define __UUFILE__ "usrfinduser.c"

#include "common.h"
#include "uustring.h"
#include "obscure.h"

#include "adminaudit.h"

#include "usr.h"
#include "usrfinduser.h"

#include "usrcas.h"
#include "usrxml.h"
#include "usrcommon.h"
#include "usrdomain.h"
#include "usrdraweb2.h"
#include "usrexternal.h"
#include "usrfollett.h"
#include "usrftp.h"
#include "usrhip.h"
#include "usrhttpbasic.h"
#include "usriii.h"
#include "usrimap.h"
#include "usrinsignia.h"
#include "usrl4u.h"
#include "usrldap.h"
#include "usrldap2.h"
#include "usrncip.h"
#include "usrodbc.h"
#include "usrpop.h"
#include "usrproxy.h"
#include "usrradius.h"
#include "usrsagebrush.h"
#include "usrshibboleth.h"
#include "usrsip.h"
#include "usrticket.h"
#include "usrtlc.h"

#include "openssl/des.h"

/* Validate a modulus 10 check digit, ignorning non numerics in the string in case its a barcode
 * surrounded */
/* by stop/start characters */

BOOL ValidateStocktonCheckDigit(char *digits) {
    char *p;
    int mult;
    int sum;
    int n;
    int check;

    if (digits == NULL || *digits == 0)
        return 0;

    check = -1;
    mult = 2;
    sum = 0;

    for (p = strchr(digits, 0) - 1; p >= digits; p--) {
        if (!IsDigit(*p))
            continue;
        if (check == -1) {
            check = *p - '0';
            continue;
        }
        n = (*p - '0') * mult;
        sum += n;
        mult *= 2;
        if (mult > 512)
            mult = 2;
    }

    sum = sum % 11;
    sum = 14 - sum;
    sum = sum % 10;

    return sum == check;
}

int FindUserIP(struct TRANSLATE *t, char *range) {
    char *nextRange;
    char *str;
    char *hyphen;
    struct sockaddr_storage peerIp, start, stop;
    static char *delim = ";,";

    memcpy(&peerIp, &t->sin_addr, sizeof t->sin_addr);

    // first we normalize this string, for accepting ":" as delimiter
    // for backwards compatability
    char *ptr;
    for (ptr = range; *ptr; ptr++) {
        if (*ptr == ':')
            *ptr = ';';
        if (*ptr == '[')
            ptr = strchr(ptr, ']');
    }

    for (str = NULL, nextRange = StrTok_R(range, delim, &str); nextRange;
         nextRange = StrTok_R(NULL, delim, &str)) {
        if (*nextRange == 0)
            continue;

        if ((hyphen = strchr(nextRange, '-'))) {
            *hyphen++ = 0;
            Trim(hyphen, TRIM_COLLAPSE);
        } else {
            hyphen = nextRange;
        }
        Trim(nextRange, TRIM_COLLAPSE);

        char b1[255], b2[255];
        TrimHost(nextRange, "[]", b1);
        TrimHost(hyphen, "[]", b2);

        int res1 = inet_pton_st2(b1, &start);
        int res2 = inet_pton_st2(b2, &stop);
        if (compare_ip(&peerIp, &start) >= 0 && compare_ip(&stop, &peerIp) >= 0)
            return 1;
    }

    return 0;
}

/* return < 0 means intruder, == 0 means user not found AKA user not logged-in, > 0 means user
 * logged-in, >= 2 means user is privileged */
int FindUser(struct TRANSLATE *t,
             struct USERINFO *uip,
             char *fileName,
             char *pseudoFileContent,
             struct FILEINCLUSION *fileIncludeStack,
             int *userLimit,
             enum INTRUDERSTATUS isIntruder) {
    BOOL ignorePassword = 0;
    BOOL found;
    BOOL foundInFile;
/*  GROUPMASK filegm; */
#ifdef WIN32
    int ntResult;
#endif
    char c1, c2;
    int f;
    char line[8192];
    char *password;
    char *options;
    char *opttok;
    BOOL match;
    size_t opttokLen;
    char *optval;
    BOOL optany;
    struct FILEREADLINEBUFFER frb;
    size_t ulen;
    char *u1, *u2;
    struct FILEINCLUSION fileIncludeNodeNewTopOfStack;
    char *ldapEdit;
    char *str;
    char *referer = NULL;
    BOOL skipping;
    BOOL skippable;
    BOOL sawLogin = 0;
    char useSsl;
    int sslIdx = 0;
    enum FINDUSERRESULT findUserResult;
    enum ACCOUNTFINDRESULT accountResult;
    struct sockaddr_storage interfaceSocket, *pInterface;
    char remoteHost[256] = {0};
    BOOL stop = 0;

    enum {
        optFile,
        optIfAuth,
        optCgi,
        optEzpedit,
        optAdmin,
        optCheckdigit,
        optFtp,
        optImap,
        optPop,
        optDeny,
        optGroup,
        optIii,
        optDomain,
        optLdap,
        optIgnorePassword,
        optRadius,
        optSecret,
        optDraweb2,
        optExternal,
        optSIP,
        optReferer,
        optPrefix,
        optAccount,
        optNote,
        optMenu,
        optStocktonCheckdigit,
        optRealm,
        optQCgi,
        optLogin,
        optLoginBu,
        optIfIP,
        optValid,
        optInterface,
        optPost,
        optSsl,
        optAsp,
        optLimit,
        optIfHttp,
        optIfHttps,
        optBanner,
        optProxy,
        optCrypt,
        optNoAPOP,
        optDebug,
        optVars,
        optTicket,
        optWayf,
        optUsrVars,
        optNCIP,
        optHostName,
        optODBC,
        optIfDay,
        optIfTime,
        optIfMonth,
        optIfYear,
        optCasLogin,
        optCasServiceValidate,
        optCas,
        optXml,
        optRelogin,
        optComment,
        optLifetime,
        optIfReferer,
        optDocsCustom,
        optIfLanguage,
        optIfUrl,
        optSourceIp,
        optObscurePassword,
        optIfAfter,
        optIfBefore,
        optHeaderUser,
        optIfHeader,
        optHttpBasic,
        optStop,
        optCommon,
        optSha512,
        optFollett,
        optSagebrush,
        optHIP,
        optInsignia,
        optL4U,
        optTLC,
        optShibboleth,
        optIgnoreCSRFToken,
        optLast
    } optIdx;
    struct {
        char *name;
        char *value;
    } optvals[] = {
        {"File",               NULL},
        {"IfAuth|Auth",        NULL},
        {"CGI",                NULL},
        {"ezpedit",            NULL},
        {"Admin",              NULL},
        {"Checkdigit",         NULL},
        {"FTP",                NULL},
        {"IMAP",               NULL},
        {"POP",                NULL},
        {"Deny",               NULL},
        {"Group",              NULL},
        {"III",                NULL},
        {"Domain",             NULL},
        {"LDAP",               NULL},
        {"IgnorePassword",     NULL},
        {"RADIUS",             NULL},
        {"Secret",             NULL},
        {"DRAWeb2",            NULL},
        {"External",           NULL},
        {"SIP",                NULL},
        {"Referer",            NULL},
        {"Prefix",             NULL},
        {"Account",            NULL},
        {"Note",               NULL},
        {"Menu",               NULL},
        {"StocktonCheckdigit", NULL},
        {"Realm",              NULL},
        {"QCGI",               NULL},
        {"Login",              NULL},
        {"LoginBU",            NULL},
        {"IfIP|IP",            NULL},
        {"Valid",              NULL},
        {"Interface",          NULL},
        {"Post",               NULL},
        {"SSL",                NULL},
        {"ASP",                NULL},
        {"Limit",              NULL},
        {"IfHttp",             NULL},
        {"IfHttps",            NULL},
        {"Banner",             NULL},
        {"Proxy",              NULL},
        {"Crypt",              NULL},
        {"NoAPOP",             NULL},
        {"Debug",              NULL},
        {"Vars",               NULL},
        {"Ticket",             NULL},
        {"WAYF",               NULL},
        {"UsrVars",            NULL},
        {"NCIP",               NULL},
        {"Hostname",           NULL},
        {"ODBC",               NULL},
        {"IfDay|Day",          NULL},
        {"IfTime|Time",        NULL},
        {"IfMonth|Month",      NULL},
        {"IfYear|Year",        NULL},
        {"CASLogin",           NULL},
        {"CASServiceValidate", NULL},
        {"CAS",                NULL},
        {"XML",                NULL},
        {"Relogin",            NULL},
        {"Comment",            NULL},
        {"Lifetime",           NULL},
        {"IfReferer",          NULL},
        {"DocsCustom",         NULL},
        {"IfLanguage",         NULL},
        {"IfURL",              NULL},
        {"SourceIP",           NULL},
        {"ObscurePassword",    NULL},
        {"IfAfter",            NULL},
        {"IfBefore",           NULL},
        {"HeaderUser",         NULL},
        {"IfHeader",           NULL},
        {"HTTPBasic",          NULL},
        {"Stop",               NULL},
        {"Common",             NULL},
        {"SHA512",             NULL},
        {"Follett",            NULL},
        {"Sagebrush",          NULL},
        {"HIP",                NULL},
        {"Insignia",           NULL},
        {"L4U",                NULL},
        {"TLC",                NULL},
        {"Shibboleth",         NULL},
        {"IgnoreCSRFToken",    NULL},
        {NULL,                 NULL}
    };
    enum CSRFTOKENSTATE {
        csrfTokenStateValid,
        csrfTokenStateInvalidMask,
        csrfTokenStateInvalidIgnore
    } csrfTokenState = uip->csrfValid ? csrfTokenStateValid : csrfTokenStateInvalidIgnore;
    char *csrfSaveUser = "";
    char *csrfSavePass = "";
    // Now that we've flagged that we are ignoring invalid CSRF, update flag to match
    uip->csrfValid = TRUE;

    uip->isIntruder = isIntruder;
    uip->debug |= uip->forceDebug;

    if (fileName == NULL) {
        fileName = EZPROXYUSR;
        if (userLimit)
            *userLimit = 0;
        /*
         *retgm = 0;
         */
        uip->gm = GroupMaskDefault(uip->gm);
    }

    if (uip->user == NULL)
        uip->user = "";
    if (uip->pass == NULL)
        uip->pass = "";
    if (uip->pin == NULL)
        uip->pin = "";

    if (!PushIncludeFileStack(uip, &frb, fileName, pseudoFileContent, fileIncludeStack,
                              &fileIncludeNodeNewTopOfStack, &f)) {
        PopIncludeFileStack(uip, uip->fileInclusion, fileIncludeStack);
        uip->fileInclusion = fileIncludeStack;
        return FALSE;
    } else {
        uip->fileInclusion = &fileIncludeNodeNewTopOfStack;
    }

    ulen = strlen(uip->user);
    found = 0;
    optany = 0;
    skipping = 0;
    /* Parse lines of the form username[:password[:option[=value][,option[=value]]*]] except the
     * LDAP= is special. */
    while (stop == 0 && FileReadLine(f, line, sizeof(line), &frb)) {
        uip->fileInclusion->lineNumber++;
        /* Can't figure out why ::login and ::loginbu should short-circuit this, so now they don't
         */
        /*
        if (sawLogin) {
            found = -2;
            break;
        }
        */
        if (*(str = SkipWS(line)) == '#') {
            if (uip->debug) {
                UsrListing(uip, TRUE, "%s", line);
            }
            continue;
        }
        if (skipping) {
            if (*str == '/') {
                skipping = 0;
            }
            if (uip->debug) {
                UsrListing(uip, skipping, "%s", line);
            }
            continue;
        }
        if (uip->debug) {
            UsrListing(uip, FALSE, "%s", line);
        }
        Trim(line, TRIM_LEAD | TRIM_TRAIL);
        /* Reset the admin flag since we are moving on to the next user */
        uip->admin = 0;
        /* Reset option values to NULL if any of them are currently set */
        if (optany)
            for (optIdx = 0; optIdx < optLast; optIdx++)
                optvals[optIdx].value = NULL;

        options = NULL;
        if ((password = strchr(line, ':')) == NULL)
            password = "";
        else {
            *password++ = 0; /* terminates 'line' at the end of the username field */
            if ((options = strchr(password, ':'))) {
                *options++ = 0;
                /* If we find an "ldap=", change all the commas after to a semi-colon
                   so people can use normal syntax; also means ldap= must be last option
                   on the line

                   LDAP options become all one field from this routine's point of view.
                */
                if ((ldapEdit = StrIStr(options, "LDAP=")))
                    for (; (ldapEdit = strchr(ldapEdit, ','));)
                        *ldapEdit = ';';
                for (str = NULL, opttok = StrTok_R(options, ",", &str); opttok;
                     opttok = StrTok_R(NULL, ",", &str)) {
                    while (IsWS(*opttok))
                        opttok++;
                    if ((optval = strchr(opttok, '='))) {
                        *optval++ = 0;
                        Trim(optval, TRIM_LEAD | TRIM_TRAIL);
                    } else
                        optval = "";
                    Trim(opttok, TRIM_TRAIL);
                    if (*opttok == 0)
                        continue;
                    opttokLen = strlen(opttok);
                    for (optIdx = 0, match = 0; optIdx < optLast; optIdx++) {
                        char *name;
                        char *bar;
                        for (name = optvals[optIdx].name;;) {
                            /* Check all aliases of the option token. */
                            bar = strchr(name, '|');
                            if (bar == NULL) {
                                if (stricmp(name, opttok) == 0) {
                                    match = 1;
                                }
                                /* Since bar was null, we break the for-loop since no more matches
                                 * to check */
                                break;
                            } else {
                                if ((size_t)(bar - name) == opttokLen &&
                                    strnicmp(name, opttok, opttokLen) == 0) {
                                    match = 1;
                                    break;
                                }
                                /* Since we had a bar, we can try after it for the next possible
                                 * match */
                                name = bar + 1;
                            }
                        }
                        if (match)
                            break;
                    }
                    if (match) {
                        optvals[optIdx].value = optval;
                        optany = 1;
                    }
                }
            }
            Trim(password, TRIM_LEAD | TRIM_TRAIL);
        }

        if (csrfTokenState != csrfTokenStateValid) {
            // CAS, CGI, QCGI and Ticket don't use CSRF, so when they are present, ignore CSRF token
            // requirement
            if (optvals[optIgnoreCSRFToken].value || optvals[optTicket].value ||
                optvals[optCas].value || optvals[optCasServiceValidate].value ||
                optvals[optCasLogin].value || optvals[optCgi].value || optvals[optQCgi].value) {
                if (csrfTokenState == csrfTokenStateInvalidMask) {
                    uip->user = csrfSaveUser;
                    uip->pass = csrfSavePass;
                    csrfTokenState = csrfTokenStateInvalidIgnore;
                    uip->csrfValid = TRUE;
                }
            } else {
                if (csrfTokenState == csrfTokenStateInvalidIgnore) {
                    csrfSaveUser = uip->user;
                    csrfSavePass = uip->pass;
                    uip->user = "";
                    uip->pass = "";
                    csrfTokenState = csrfTokenStateInvalidMask;
                    uip->csrfValid = FALSE;
                }
            }
        }

        uip->debug = (optvals[optDebug].value != NULL) || uip->forceDebug;

        if ((uip->debug) && (debugLevel >= 8000)) {
            UsrLog(uip, "recognized user name '%s'.", line);
            UsrLog(uip, "recognized password '%s'.", password[0] == 0 ? "NON-NULL" : "NULL");
            if (optany) {
                for (optIdx = 0; optIdx < optLast; optIdx++) {
                    if (optvals[optIdx].value != NULL) {
                        if (*(optvals[optIdx].value) != 0) {
                            UsrLog(uip, "recognized option '%s'='%s'.", optvals[optIdx].name,
                                   optvals[optIdx].value);

                        } else {
                            UsrLog(uip, "recognized option '%s'.", optvals[optIdx].name);
                        }
                    }
                }
            } else {
                UsrLog(uip, "recognized no options.");
            }
        }

        if (optvals[optFile].value && pseudoFileContent) {
            optvals[optFile].value = NULL;
            UsrLog(uip,
                   "Security restrictions disable the use of ::File in testing configurations");
        }

        if (stricmp(line, "zagar") == 0 && stricmp(password, "debug") == 0) {
            static char zagarDebug = 1;
            if (zagarDebug) {
                zagarDebug = 0;
                UsrLog(uip, "zagar:debug ignored");
            }
            continue;
        }

        //@TODO add check for oclcsupport:ma1234:admin

        if (stricmp(line, "demo") == 0 && *password == 0) {
            static char demoWarning = 1;
            if (demoWarning) {
                demoWarning = 0;
                UsrLog(uip, "demo ignored");
            }
            continue;
        }

        if (stricmp(line, "testuser") == 0 && stricmp(password, "testpass") == 0) {
            static char testuserWarning = 1;
            if (testuserWarning) {
                testuserWarning = 0;
                UsrLog(uip, "testuser:testpass ignored");
            }
            continue;
        }

        skippable = optvals[optCas].value != NULL || optvals[optDraweb2].value != NULL ||
                    optvals[optIii].value != NULL ||
                    (optvals[optLdap].value != NULL && optvals[optLdap].value[0] == 0) ||
                    optvals[optNCIP].value != NULL || optvals[optODBC].value != NULL ||
                    optvals[optSIP].value != NULL || optvals[optTicket].value != NULL ||
                    optvals[optCommon].value != NULL || optvals[optShibboleth].value != NULL ||
                    optvals[optFollett].value != NULL || optvals[optSagebrush].value != NULL ||
                    optvals[optHIP].value != NULL || optvals[optInsignia].value != NULL ||
                    optvals[optL4U].value != NULL || optvals[optTLC].value != NULL;

        /* Part 1: Conditional tests that can prevent this line from being considered further */

        if (optvals[optIfAfter].value) {
            if (UsrIfAfter(t, uip, NULL, optvals[optIfAfter].value)) {
                skipping = skippable;
                continue;
            }
        }

        if (optvals[optIfAuth].value) {
            if (UsrIfAuth(t, uip, NULL, optvals[optIfAuth].value)) {
                skipping = skippable;
                continue;
            }
        }

        if (optvals[optIfBefore].value) {
            if (UsrIfBefore(t, uip, NULL, optvals[optIfBefore].value)) {
                skipping = skippable;
                continue;
            }
        }

        if (optvals[optIfDay].value) {
            if (UsrIfDay(t, uip, NULL, optvals[optIfDay].value)) {
                skipping = skippable;
                continue;
            }
        }

        if (optvals[optIfHeader].value) {
            if (UsrIfHeader(t, uip, NULL, optvals[optIfHeader].value)) {
                skipping = skippable;
                continue;
            }
        }

        if (optvals[optIfHttp].value) {
            if (UsrIfHttp(t, uip, NULL, optvals[optIfHttp].value)) {
                skipping = skippable;
                continue;
            }
        }

        if (optvals[optIfHttps].value) {
            if (UsrIfHttps(t, uip, NULL, optvals[optIfHttps].value)) {
                skipping = skippable;
                continue;
            }
        }

        if (optvals[optIfIP].value != NULL && !FindUserIP(t, optvals[optIfIP].value)) {
            skipping = skippable;
            continue;
        }

        if (optvals[optIfLanguage].value) {
            if (UsrIfLanguage(t, uip, NULL, optvals[optIfLanguage].value)) {
                skipping = skippable;
                continue;
            }
        }

        if (optvals[optIfMonth].value) {
            if (UsrIfMonth(t, uip, NULL, optvals[optIfMonth].value)) {
                skipping = skippable;
                continue;
            }
        }

        if (optvals[optIfReferer].value) {
            if (UsrIfReferer(t, uip, NULL, optvals[optIfReferer].value)) {
                skipping = skippable;
                continue;
            }
        }

        if (optvals[optIfTime].value) {
            if (UsrIfTime(t, uip, NULL, optvals[optIfTime].value)) {
                skipping = skippable;
                continue;
            }
        }

        if (optvals[optIfUrl].value) {
            if (UsrIfURL(t, uip, NULL, optvals[optIfUrl].value)) {
                skipping = skippable;
                continue;
            }
        }

        if (optvals[optIfYear].value) {
            if (UsrIfYear(t, uip, NULL, optvals[optIfYear].value)) {
                skipping = skippable;
                continue;
            }
        }

        /* Part 2: Things that can be changed (banners, concurrency limits, etc.) */

        if (optvals[optStop].value) {
            stop = 1;
            if (uip->debug) {
                UsrLog(uip, "Stopping");
            }
        }

        if (optvals[optComment].value) {
            char *comment, *end;

            comment = optvals[optComment].value;
            if (*comment == '+') {
                comment++;
            } else {
                *uip->comment = 0;
            }

            if (*uip->comment == 0)
                strcpy(uip->comment, "# ");

            end = strchr(uip->comment, 0);
            StrCpy3(end, comment, sizeof(uip->comment) - (end - uip->comment));
        }

        if ((useSsl = optvals[optSsl].value != NULL)) {
            sslIdx = atoi(optvals[optSsl].value);
            if (sslIdx == 0)
                sslIdx = -1;
        } else {
            sslIdx = 0;
        }

        if (optvals[optLimit].value) {
            *userLimit = atoi(optvals[optLimit].value);
            /* It seems silly to check first instead of just or'ing, but we are
               multi-threaded now, so it's better not to be writing it unless we
               have to
            */
            if ((usageLogUser & ULULIMIT) == 0)
                usageLogUser |= ULULIMIT;
        }

        if (optvals[optInterface].value != NULL) {
            if (UUGetHostByName(optvals[optInterface].value, &interfaceSocket, 0, 0))
                continue;
            pInterface = &interfaceSocket;
        } else {
            pInterface = NULL;
        }

        if (optvals[optIgnorePassword].value != NULL)
            ignorePassword = 1;

        if (optvals[optLifetime].value) {
            uip->lifetime = atoi(optvals[optLifetime].value) * 60;
        }

        /* Allow old vars= to be equivalent to new usrvars= */
        if (optvals[optVars].value && optvals[optUsrVars].value == NULL)
            optvals[optUsrVars].value = optvals[optVars].value;

        if (optvals[optUsrVars].value != NULL) {
            char *v = optvals[optUsrVars].value;
            if (*v == 0) {
                memset(&uip->vars, 0, sizeof(uip->vars));
            } else {
                char *vstr, *vopttok, *vn, *vv;
                for (vn = NULL, vstr = NULL, vopttok = StrTok_R(v, ";", &vstr);;
                     vopttok = StrTok_R(NULL, ";", &vstr)) {
                    if (vn == NULL) {
                        if (vopttok == NULL)
                            break;
                        vn = vopttok;
                        continue;
                    }
                    vv = (vopttok ? vopttok : "");
                    if (strlen(vn) == 1 && IsDigit(*vn))
                        StrCpy3(uip->vars[*vn - '0'], vv, sizeof(uip->vars[0]));
                    vn = NULL;
                    if (vopttok == NULL)
                        break;
                }
            }
        }

        if (optvals[optBanner].value != NULL) {
            UsrBanner(t, uip, NULL, optvals[optBanner].value);
        }

        if (optvals[optSourceIp].value) {
            UsrSourceIP(t, uip, NULL, optvals[optSourceIp].value);
        }

        if (optvals[optRelogin].value) {
            UsrRelogin(t, uip, NULL, optvals[optRelogin].value);
        }

        if (optvals[optLogin].value != NULL) {
            if (ValidMenu(optvals[optLogin].value)) {
                sprintf(uip->login, "%s%s", DOCSDIR, optvals[optLogin].value);
                /* if no loginbu specified, duplicate login to loginbu */
                if (optvals[optLoginBu].value == NULL)
                    strcpy(uip->loginBu, uip->login);
                sawLogin = 1;
            } else {
                UsrLog(uip, "references invalid login %s", optvals[optLogin].value);
            }
        }

        if (optvals[optLoginBu].value != NULL) {
            if (ValidMenu(optvals[optLoginBu].value)) {
                sprintf(uip->loginBu, "%s%s", DOCSDIR, optvals[optLoginBu].value);
                sawLogin = 1;
            } else {
                UsrLog(uip, "references invalid login %s", optvals[optLoginBu].value);
            }
        }

        if (optvals[optMenu].value != NULL) {
            if (ValidMenu(optvals[optMenu].value))
                sprintf(uip->menu, "%s%s", DOCSDIR, optvals[optMenu].value);
            else
                UsrLog(uip, "references invalid menu %s", optvals[optMenu].value);
        }

        if (optvals[optDocsCustom].value != NULL) {
            UsrDocsCustom(t, uip, NULL, optvals[optDocsCustom].value);
        }

        if (optvals[optGroup].value != NULL) {
            MakeGroupMask(uip->gm, optvals[optGroup].value, 0, '=');
        }

        if (isIntruder != INTRUDERNOT)
            continue;

        /* Part 3: Actual authentication tests */

        if (optvals[optCas].value || optvals[optCasServiceValidate].value ||
            optvals[optCasLogin].value) {
            /* CAS applies the File= directive differently, so if CAS is in use, we take the
               value and then block it out from further processing; without this, a filter file with
               * could end up matching any username when CAS is not being used
            */
            char *filterFile = optvals[optFile].value;
            optvals[optFile].value = NULL;
            if (optvals[optCas].value)
                optvals[optCasServiceValidate].value = optvals[optCasLogin].value = NULL;
            if (*(uip->user) == 0 && uip->logup == 0 &&
                (optvals[optCas].value ||
                 (optvals[optCasLogin].value && optvals[optCasServiceValidate].value))) {
                findUserResult =
                    FindUserCAS(t, uip, fileName, f, &frb, pInterface, optvals[optCasLogin].value,
                                optvals[optCasServiceValidate].value);
                if (findUserResult == resultValid) {
                    /* CAS is unusual since the username comes after the fact; we allow the file
                       directive to be processed after, once the username is actually available
                    */
                    if (filterFile) {
                        foundInFile = FindUser(t, uip, filterFile, NULL, uip->fileInclusion,
                                               userLimit, isIntruder);
                        if (uip->denied)
                            break;
                        if (foundInFile == 0)
                            continue;
                    }
                    goto ForceValid;
                }
                if (uip->deniedNotified)
                    break;
            }
            skipping = skippable;
            continue;
        }

        if (optvals[optXml].value) {
            findUserResult = FindUserXML(t, uip, fileName, f, &frb, pInterface);
            if (findUserResult == resultValid)
                goto ForceValid;
            if (findUserResult == resultNotified) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            if (uip->deniedNotified) {
                break;
            }
            continue;
        }

        foundInFile = 0;
        /* We only go into file= directives if we have a username that we're looking for */
        /* (hence, all authentication must be in main EZPROXYUSR file) */
        if (optvals[optFile].value != NULL &&
            (*(uip->user) != 0 || stricmp(optvals[optFile].value, "ezproxy.usr") == 0)) {
            /*          fileFound = FindUser(t, uip, optvals[optFile].value, &fn, requiredGm, gm,
             * &filegm, url, userLimit, isIntruder); */
            foundInFile = FindUser(t, uip, optvals[optFile].value, NULL, uip->fileInclusion,
                                   userLimit, isIntruder);
            if (uip->denied)
                break;
            if (foundInFile == 0) {
                /* This causes III or DRAWeb2 directives to be skipped that don't match the auth
                 * value */
                skipping = skippable;
                continue;
            }
        }

        if (optvals[optDraweb2].value != NULL && *(uip->user) != 0) {
            findUserResult = FindUserDraweb2(t, uip, fileName, f, &frb, pInterface);
            if (findUserResult == resultValid)
                goto ForceValid;
            if (findUserResult == resultNotified) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            continue;
        }

        if (optvals[optExternal].value != NULL && *(uip->user) != 0) {
            findUserResult =
                FindUserExternal(t, uip, optvals[optExternal].value, optvals[optPost].value,
                                 optvals[optValid].value, pInterface);
            if (findUserResult == resultValid)
                goto ForceValid;
            if (findUserResult == resultNotified) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            continue;
        }

        if (accountDefaultPassword != NULL && optvals[optAccount].value != NULL &&
            *(uip->user) != 0) {
            accountResult = AccountFind(uip->user, uip->pass, accountDefaultPassword, NULL, 0);
            if (accountResult < accountWrong) {
                uip->accountResult = accountResult;
                goto ForceValid;
            }
            continue;
        }

        if (optvals[optSIP].value != NULL) {
            findUserResult =
                FindUserSip(t, optvals[optSIP].value, uip, fileName, f, &frb, pInterface);
            if (findUserResult == resultValid)
                goto ForceValid;
            if (findUserResult == resultNotified) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            continue;
        }

        if (optvals[optNCIP].value != NULL) {
            findUserResult = FindUserNCIP(t, uip, fileName, f, &frb, pInterface);
            if (findUserResult == resultValid)
                goto ForceValid;
            if (findUserResult == resultNotified) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            continue;
        }

        if (optvals[optHttpBasic].value && *(uip->user) != 0) {
            if (FindUserHttpBasic(optvals[optHttpBasic].value, uip, pInterface) == resultValid)
                goto ForceValid;
            continue;
        }

        if (optvals[optFtp].value != NULL && *(uip->user) != 0) {
            if (FindUserFtp(optvals[optFtp].value, uip->user, uip->pass, pInterface, useSsl,
                            uip->debug) == 0)
                goto ForceValid;
            continue;
        }

        if (optvals[optIii].value != NULL) {
            findUserResult = FindUserIii(t, uip, fileName, f, &frb, pInterface);
            if (findUserResult == resultValid)
                goto ForceValid;
            if (findUserResult == resultNotified) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            continue;
        }

        if (optvals[optTicket].value != NULL) {
            findUserResult = FindUserTicket(t, uip, fileName, f, &frb, pInterface);
            if (findUserResult == resultValid)
                goto ForceValid;
            if (findUserResult == resultNotified) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            continue;
        }

        if (optvals[optImap].value != NULL && *(uip->user) != 0) {
            if (FindUserImap(optvals[optImap].value, uip->user, uip->pass, pInterface, useSsl) == 0)
                goto ForceValid;
            continue;
        }

        if (optvals[optPop].value != NULL && *(uip->user) != 0) {
            if (FindUserPop(optvals[optPop].value, uip->user, uip->pass, pInterface, useSsl,
                            uip->debug, optvals[optNoAPOP].value != NULL) == 0)
                goto ForceValid;
            continue;
        }

        if (optvals[optRadius].value != NULL && *(uip->user) != 0) {
            if (FindUserRadius(optvals[optRadius].value, uip->user, uip->pass,
                               optvals[optSecret].value, optvals[optRealm].value, pInterface,
                               uip->debug) == 0)
                goto ForceValid;
            continue;
        }

        if (optvals[optProxy].value != NULL && *(uip->user) != 0) {
            if (FindUserProxy(optvals[optProxy].value, uip, uip->user, uip->pass, pInterface) == 0)
                goto ForceValid;
            continue;
        }

#ifdef WIN32
        if (optvals[optDomain].value != NULL && *(uip->user) != 0) {
            ntResult = FindUserNtDomain(t, uip, optvals[optDomain].value, uip->newPass,
                                        uip->verifyPass, uip->domain);
            if (ntResult == 0)
                goto ForceValid;
            if (ntResult == -1) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            continue;
        }

        if (optvals[optODBC].value != NULL && *(uip->user) != 0) {
            findUserResult = FindUserODBC(t, uip, fileName, f, &frb, pInterface);
            if (findUserResult == resultValid)
                goto ForceValid;
            if (findUserResult == resultNotified) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            continue;
        }
#endif

        if (optvals[optCommon].value) {
            findUserResult = FindUserCommon(t, uip, fileName, f, &frb, pInterface);
            if (findUserResult == resultValid)
                goto ForceValid;
            if (findUserResult == resultNotified) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            continue;
        }

        if (optvals[optFollett].value) {
            findUserResult = FindUserFollett(t, uip, fileName, f, &frb, pInterface);
            if (findUserResult == resultValid)
                goto ForceValid;
            if (findUserResult == resultNotified) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            continue;
        }

        if (optvals[optSagebrush].value) {
            findUserResult = FindUserSagebrush(t, uip, fileName, f, &frb, pInterface);
            if (findUserResult == resultValid)
                goto ForceValid;
            if (findUserResult == resultNotified) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            continue;
        }

        if (optvals[optHIP].value) {
            findUserResult = FindUserHIP(t, uip, fileName, f, &frb, pInterface);
            if (findUserResult == resultValid)
                goto ForceValid;
            if (findUserResult == resultNotified) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            continue;
        }

        if (optvals[optInsignia].value) {
            findUserResult = FindUserInsignia(t, uip, fileName, f, &frb, pInterface);
            if (findUserResult == resultValid)
                goto ForceValid;
            if (findUserResult == resultNotified) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            continue;
        }

        if (optvals[optL4U].value) {
            findUserResult = FindUserL4U(t, uip, fileName, f, &frb, pInterface);
            if (findUserResult == resultValid)
                goto ForceValid;
            if (findUserResult == resultNotified) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            continue;
        }

        if (optvals[optTLC].value) {
            findUserResult = FindUserTLC(t, uip, fileName, f, &frb, pInterface);
            if (findUserResult == resultValid)
                goto ForceValid;
            if (findUserResult == resultNotified) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            continue;
        }

        if (optvals[optLdap].value != NULL && *(uip->user) != 0) {
            if (optvals[optLdap].value[0]) {
                if (FindUserLdap(optvals[optLdap].value, uip->user, uip->pass, pInterface, useSsl,
                                 sslIdx) == 0)
                    goto ForceValid;
            } else {
                findUserResult = FindUserLDAP2(t, uip, fileName, f, &frb, pInterface);
                if (findUserResult == resultValid)
                    goto ForceValid;
                if (findUserResult == resultNotified) {
                    uip->denied = uip->deniedNotified = 1;
                    break;
                }
            }
            continue;
        }

        if (optvals[optReferer].value != NULL && *(uip->user) == 0) {
            if (GroupMaskOverlap(uip->gm, uip->requiredGm)) {
                if (referer == NULL) {
                    if ((referer = FindField("referer", t->requestHeaders)))
                        LinkField(referer);
                }
                if (referer) {
                    if (logReferer) {
                        UsrLog(uip, "Referer compare %s and %s", referer,
                               optvals[optReferer].value);
                    }
                    if (WildCompare(referer, optvals[optReferer].value)) {
                        found = 1;
                        strcpy(uip->logUserReferer, "referer-");
                        ipToStr(&t->sin_addr, strchr(uip->logUserReferer, 0), INET6_ADDRSTRLEN);
                        StrCpy3(uip->logUser, uip->logUserReferer, sizeof(uip->logUser));
                        uip->autoLoginBy = autoLoginByReferer;
                        uip->logUserAllowed = TRUE;
                        goto ForceValid;
                    }
                } else {
                    if (logReferer)
                        UsrLog(uip, "Referer header blank");
                }
            }
            continue;
        }

        if (optvals[optHeaderUser].value && *(uip->user) == 0) {
            char *hhu;
            if ((hhu = FindField(optvals[optHeaderUser].value, t->requestHeaders))) {
                LinkField(hhu);
                StrCpy3(uip->logUserReferer, hhu, sizeof(uip->logUserReferer));
                Trim(uip->logUserReferer, TRIM_LEAD | TRIM_TRAIL | TRIM_CTRL);
                if (uip->logUserReferer[0]) {
                    uip->user = uip->logUserReferer;
                    uip->autoLoginBy = autoLoginByNone;
                    if (optvals[optFile].value) {
                        foundInFile = FindUser(t, uip, optvals[optFile].value, NULL,
                                               uip->fileInclusion, userLimit, isIntruder);
                        if (uip->denied)
                            break;
                        if (foundInFile == 0) {
                            uip->user = "";
                            continue;
                        }
                    }
                    StrCpy3(uip->logUser, uip->user, sizeof(uip->logUser));
                    goto ForceValid;
                }
            }
            continue;
        }

        if (optvals[optHostName].value != NULL &&
            (optvals[optDeny].value != NULL || *(uip->user) == 0)) {
            BOOL deny = optvals[optDeny].value != NULL;

            if (deny || GroupMaskOverlap(uip->gm, uip->requiredGm)) {
                if (remoteHost[0] == 0) {
                    UUGetHostByAddr(&t->sin_addr, remoteHost, sizeof remoteHost);
                }
                if (WildCompare(remoteHost, optvals[optHostName].value)) {
                    if (deny)
                        goto ForceDeny;
                    found = 1;
                    goto ForceValid;
                }
            }
            continue;
        }

        if (optvals[optWayf].value && *(uip->user) == 0) {
            if (!(uip->wayf = strdup(optvals[optWayf].value)))
                PANIC;
            found = 1;
            goto ForceValid;
        }

        if (optvals[optCgi].value != NULL && *(uip->user) == 0 && uip->urlReference == 0) {
            if (!(uip->cgi = strdup(optvals[optCgi].value)))
                PANIC;
            found = 1;
            goto ForceValid;
        }

        if (optvals[optQCgi].value != NULL && *(uip->user) == 0 && uip->urlReference == 0) {
            if (!(uip->qcgi = strdup(optvals[optQCgi].value)))
                PANIC;
            found = 1;
            goto ForceValid;
        }

        if (optvals[optShibboleth].value && *(uip->user) == 0) {
            findUserResult = FindUserShibboleth(t, uip, fileName, f, &frb, pInterface);
            if (uip->wayf) {
                found = 1;
                goto ForceValid;
            }
            if (findUserResult == resultNotified) {
                uip->denied = uip->deniedNotified = 1;
                break;
            }
            continue;
        }

        if (line[0] == 0) {
            if (foundInFile == 0)
                continue;
            goto ForceValid;
        }

        if (strcmp(line, "*") != 0) {
            for (u1 = uip->user, u2 = line; *u1 && *u2; u1++, u2++) {
                c1 = StripISO88591Diacritics(*u1);
                if (isupper(c1))
                    c1 = tolower(c1);
                c2 = StripISO88591Diacritics(*u2);
                if (isupper(c2))
                    c2 = tolower(c2);
                if (c1 == c2 || (IsDigit(c1) && c2 == '#'))
                    continue;
                else
                    break;
            }
            if (*u1 != 0 || *u2 != 0)
                continue;
        }

    ForceDeny:
        if (optvals[optDeny].value != NULL) {
            char denyFile[MAXMENU];
            uip->denied = 1;
            AuditEventLogin(t, AUDITLOGINDENIED, uip->user, NULL, uip->pass,
                            optvals[optDeny].value);
            if (ValidMenu(optvals[optDeny].value)) {
                sprintf(denyFile, "%s%s", DOCSDIR, optvals[optDeny].value);
                HTMLHeader(t, htmlHeaderOmitCache);
                SendEditedFile(t, uip, denyFile, 1, uip->url, 1, NULL);
                uip->deniedNotified = 1;
            }
            break;
        }

        if (optvals[optObscurePassword].value != NULL) {
            char *unobscure;
            BOOL match;
            if (!(unobscure = UnobscureString(optvals[optObscurePassword].value)))
                PANIC;
            match = strcmp(uip->pass, unobscure) == 0;
            FreeThenNull(unobscure);
            if (!match)
                continue;
        } else if (optvals[optCrypt].value != NULL) {
            char cryptValue[32];
            if (strcmp(optvals[optCrypt].value,
                       DES_fcrypt(uip->pass, optvals[optCrypt].value, cryptValue)) != 0)
                continue;
        } else if (optvals[optSha512].value) {
            char *sha512;
            BOOL match;
            if (!(sha512 = SHA512String(uip->pass, optvals[optSha512].value)))
                PANIC;
            match = strcmp(optvals[optSha512].value, sha512) == 0;
            FreeThenNull(sha512);
            if (!match)
                continue;
        } else {
            if (ignorePassword == 0) {
                for (u1 = password, u2 = uip->pass; *u1 && *u2; u1++, u2++) {
                    c1 = StripISO88591Diacritics(*u1);
                    if (isupper(c1))
                        c1 = tolower(c1);
                    c2 = StripISO88591Diacritics(*u2);
                    if (isupper(c2))
                        c2 = tolower(c2);
                    if (c1 != c2)
                        break;
                }
                if (*u1 || *u2)
                    continue;
            }
        }

        if (optvals[optCheckdigit].value != NULL && !ValidateCheckDigit(uip->user))
            continue;
        if (optvals[optStocktonCheckdigit].value != NULL && !ValidateStocktonCheckDigit(uip->user))
            continue;
        if (optvals[optEzpedit].value != NULL)
            uip->ezpedit = 1;

    ForceValid:
        /* If this wasn't a CGI line, and we didn't manually set loguser with set login:loguser=,
           force the name to be the user= value, not loguser= value
        */
        if (optvals[optCgi].value == NULL && uip->logUserAllowed == 0)
            StrCpy3(uip->logUser, uip->user, sizeof(uip->logUser));
        if (optvals[optAdmin].value != NULL)
            uip->admin = 1;
        if (found == 0)
            found = 1;
        if (optvals[optPrefix].value != NULL)
            StrCpy3(uip->logPrefix, optvals[optPrefix].value, sizeof(uip->logPrefix));
        if (optvals[optAsp].value != NULL)
            uip->asp = 1;
        /*      *retgm = (fileFound ? filegm : gm); */ /* this was before 3.0e */
        /* *retgm = gm; */ /* this was before 3.0g with GroupMask changes */
        break;
    }

    if (pseudoFileContent == NULL)
        close(f);

    if (fileIncludeStack == NULL && found > 0 && uip->denied)
        found = 0;

    if (found <= 0 || uip->denied || uip->deniedNotified)
        uip->admin = 0;

    if (fileIncludeStack == NULL && found == 0)
        GroupMaskClear(uip->gm);

    if (csrfTokenState != csrfTokenStateValid) {
        uip->csrfValid = FALSE;
        if (csrfTokenState == csrfTokenStateInvalidMask) {
            uip->user = csrfSaveUser;
            uip->pass = csrfSavePass;
            csrfTokenState = csrfTokenStateInvalidIgnore;
        }
    }

    if (fileIncludeStack == NULL && uip->user && *uip->user && found <= 0) {
        t->denyClientKeepAlive = uip->user != NULL && *uip->user != 0;
        AuditEventLogin(t, (isIntruder != INTRUDERNOT ? AUDITLOGININTRUDERIP : AUDITLOGINFAILURE),
                        uip->user, NULL, uip->pass, uip->csrfValid ? "" : "No CSRF Token");
    }

    if (FALSE && ((fileIncludeStack == NULL) && uip->debug)) {
        /* Left in the source code for it's documentation value. */
        if (uip->denied) {
            UsrLog(uip, "Denied");
            if (uip->deniedNotified) {
                UsrLog(uip, "Notified");
            }
        }
        if (!uip->denied) {
            int gc = GroupMaskCardinality(uip->gm);

            if (uip->cgi) {
                UsrLog(uip, "CGI redirection");
            } else if (uip->wayf) {
                UsrLog(uip, "SAML/Shibboleth redirection");
            } else if (uip->user && *uip->user && found == 0) {
                if (isIntruder != INTRUDERNOT) {
                    UsrLog(uip, "Intruder");
                } else {
                    UsrLog(uip, "Unauthenticated");
                }
            } else if (uip->user && *uip->user && found == 1) {
                UsrLog(uip, "Authenticated");
            } else if (found == 0) {
                UsrLog(uip, "Login failure");
            } else if (found == 1) {
                UsrLog(uip, "Auto login");
            }
            if (uip->user == NULL) {
                UsrLog(uip, "No user name");
            } else {
                UsrLog(uip, "User name is '%s'", uip->user);
                if (uip->pass == NULL) {
                    UsrLog(uip, "No password");
                } else {
                    UsrLog(uip, "Password is not null");
                }
            }
            if (uip->pin == NULL) {
                UsrLog(uip, "No pin");
            } else {
                UsrLog(uip, "Pin is '%s'", uip->pin);
            }
            if (uip->admin)
                UsrLog(uip, "Administrative user");
            if (gc == 0)
                UsrLog(uip, "No assigned groups");
            else {
                UsrLog(uip, "Assigned group%s: ", SIfNotOne(gc));
                GroupMaskWrite(uip->gm, NULL, uip);
            }
        }
    }

    PopIncludeFileStack(uip, uip->fileInclusion, fileIncludeStack);
    uip->fileInclusion = fileIncludeStack;

    return found;
}
