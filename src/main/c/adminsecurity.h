#ifndef __ADMINSECURITY_H__
#define __ADMINSECURITY_H__

#include "common.h"
#include "environment.h"
#include "security.h"

void DeleteExpiredNotificationTransactions();
void GetTokenFromWsKeyAndSecret(char **token);
void AdminSecurityAuditEvent(struct TRANSLATE *t,
                             const char *activity,
                             const char *id,
                             time_t expiresTime);
void AdminSecurity(struct TRANSLATE *t, char *query, char *post);
void AdminSecurityExemptions(struct TRANSLATE *t, char *query, char *post);
void AdminSecurityNotifications(struct TRANSLATE *t, char *query, char *post);
void AdminSecurityPurge(struct TRANSLATE *t, char *query, char *post);
void AdminSecurityRules(struct TRANSLATE *t, char *query, char *post);
void AdminSecurityTripped(struct TRANSLATE *t, char *query, char *post);
void AdminSecurityVacuum(struct TRANSLATE *t, char *query, char *post);
void SendQueuedSecurityRuleEmail();
void SendSecurityRuleDigestEmail();
BOOL SecurityEmailReadConfig();

#endif  // __ADMINSECURITY_H__
