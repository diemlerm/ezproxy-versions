#ifndef __ADMINCPIP_H__
#define __ADMINCPIP_H__

#include "common.h"

void AdminCPIPGetConfigVersion2(struct TRANSLATE *t, char *query, char *post);
void AdminCPIPIdentify(struct TRANSLATE *t, char *query, char *post);
void AdminCPIPPickup(struct TRANSLATE *t, char *query, char *post);
void AdminCPIPLastActive(struct TRANSLATE *t, char *query, char *post);
void AdminCPIPDeauthenticate(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINCPIP_H__
