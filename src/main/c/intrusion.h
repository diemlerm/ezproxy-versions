#ifndef __INTRUSION_H__
#define __INTRUSION_H__

enum IntrusionIPStatus {
    INTRUSION_IP_BAD_IP,         // Returned as BadIP
    INTRUSION_IP_NONE,           // No entry returned (204 status)
    INTRUSION_IP_WHITELISTED,    // Returned as Whitelist
    INTRUSION_IP_INVALID_STATE,  // State was either missing or an unrecognized value
    INTRUSION_IP_NET_FAILURE,    // Nothing returned due to network failure
    INTRUSION_IP_PRIVATE         // Address is a private address
};

extern BOOL intrusionEnforce;
extern BOOL intrusionEvade;
extern BOOL intrusionReject;
extern char *intrusionVersion;
extern char *intrusionProxySslHost;
extern PORT intrusionProxySslPort;
extern char *intrusionProxySslAuth;

int UUAvlTreeCmpIntrusionIPs(const void *v1, const void *v2);
BOOL IntrusionInvalidConfig();
BOOL IntrusionReadConfig();
enum IntrusionIPStatus IntrusionIPStatus(struct sockaddr_storage *inIp, BOOL audit);
char *getIntrusionPem();

#endif
