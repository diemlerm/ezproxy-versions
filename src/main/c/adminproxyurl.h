#ifndef __ADMINPROXYURL_H__
#define __ADMINPROXYURL_H__

#include "common.h"

void AdminProxyUrl(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINPROXYURL_H__
