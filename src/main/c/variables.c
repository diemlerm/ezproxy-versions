/**
 * Support for expression variables.
 *
 * When dereferencing a variable name to a value, the variable XXX is an alias for the variable
 * XXX[0].
 */

#define __UUFILE__ "variables.c"

#include "common.h"
#include "expression.h"
#include "variables.h"
#include <limits.h>

struct VARIABLE {
    char *var;
    char *val;
};

struct VARIABLES {
    struct UUAVLTREE avlVariables;
    struct RWL rwl;
    BOOL shared;
};

enum VARIABLESFINDFUNCTION { variablesFindSet, variablesFindGet, variablesFindDelete };

int UUAvlTreeCmpVariable(const void *v1, const void *v2) {
    struct VARIABLE *var1 = (struct VARIABLE *)v1;
    struct VARIABLE *var2 = (struct VARIABLE *)v2;
    int lLeft;
    char *pLeft;
    char *pLeft2;
    long int iLeft;
    int lRight;
    char *pRight;
    char *pRight2;
    long int iRight;
    int k;

    pLeft = var1->var;
    pRight = var2->var;

    if (!(pLeft))
        PANIC;
    if (!(pRight))
        PANIC;

    for (k = 0; k == 0;) {
        /* try comparing them as numbers, if one isn't a number, let it be LONG_MAX */
        errno = 0;
        iLeft = strtol(pLeft, &(pLeft2), 10);
        if (errno != ERANGE && errno != 0) {
            iLeft = LONG_MAX;
            pLeft2 = pLeft;
        }
        errno = 0;
        iRight = strtol(pRight, &(pRight2), 10);
        if (errno != ERANGE && errno != 0) {
            iRight = LONG_MAX;
            pRight2 = pRight;
        }
        k = (iLeft < iRight) ? -1 : (iLeft == iRight) ? 0 : 1;
        if (k == 0) {
            /* if they're equal as numbers, try comparing their string length. */
            lLeft = (pLeft2 - pLeft);
            lRight = (pRight2 - pRight);
            k = (lLeft < lRight) ? -1 : (lLeft == lRight) ? 0 : 1;
            if (k == 0) {
                /* if they're equal as numbers, and as strings, try comparing the terminating byte.
                 */
                k = ((unsigned char)*pLeft2) - ((unsigned char)*pRight2);
                if (k != 0) {
                    /* if they're not equal we have to translate brackets and commas to zeros. */
                    if (*pLeft2 == '[') {
                        k = -1;
                    } else if (*pRight2 == '[') {
                        k = 1;
                    } else if (*pLeft2 == ',') {
                        k = -1;
                    } else if (*pRight2 == ',') {
                        k = 1;
                    }
                } else {
                    /* if they're equal, we have to decide if we can move on to the next byte. */
                    if (*pLeft2 != 0) {
                        pLeft = pLeft2 + 1;
                        pRight = pRight2 + 1;
                    } else {
                        break;
                    }
                }
            }
        }
    }

    return (k < 0) ? -1 : (k == 0) ? 0 : 1;
}

struct VARIABLES *VariablesNew(char *name, BOOL shared) {
    struct VARIABLES *n;

    if (!(n = (struct VARIABLES *)calloc(1, sizeof(*n))))
        PANIC;
    UUAvlTreeInit(&n->avlVariables, name, UUAvlTreeCmpVariable, NULL, NULL);

    if (shared) {
        RWLInit(&n->rwl, name);
        n->shared = 1;
    }

    return n;
}

static struct VARIABLE *VariablesFind(struct VARIABLES *v,
                                      const char *var,
                                      enum VARIABLESFINDFUNCTION func) {
    struct VARIABLE *p, *n, find;
    char *e;

    find.var = (char *)var;
    if (p = UUAvlTreeFind(&v->avlVariables, &find)) {
        if (func == variablesFindDelete) {
            UUAvlTreeDelete(&v->avlVariables, p);
            FreeThenNull(p->var);
            FreeThenNull(p->val);
            return NULL;
        }
        return p;
    }

    if (func != variablesFindSet)
        return NULL;

    if ((e = strchr(var, '[')) && (!((e != var) && (e = strchr(var, ']')) && (*(++e) == 0)))) {
        Log("Invalid variable name: %s", var);
        return NULL;
    } else {
        if (!(n = calloc(1, sizeof(*n))))
            PANIC;
        if (!(n->var = strdup(var)))
            PANIC;
        UUAvlTreeInsert(&v->avlVariables, n);
    }

    return n;
}

BOOL VariablesSetValue(struct VARIABLES *v, const char *var, const char *val) {
    struct VARIABLE *p;
    char *oldVal;
    char *newVal;
    BOOL changed = FALSE;

    if (var == NULL)
        return changed;

    if (v->shared)
        RWLAcquireWrite(&v->rwl);

    p = VariablesFind(v, var, variablesFindSet);
    if (p == NULL) {
        goto Finished;
    }

    /* If the value didn't change, nothing to do */
    if (p->val && val && strcmp(p->val, val) == 0)
        goto Finished;

    changed = TRUE;

    if (val == NULL)
        newVal = NULL;
    else if (!(newVal = strdup(val)))
        PANIC;

    oldVal = p->val;
    p->val = newVal;
    FreeThenNull(oldVal);

Finished:
    if (v->shared)
        RWLReleaseWrite(&v->rwl);

    return changed;
}

BOOL VariablesSetValueAppend(struct VARIABLES *v, const char *var, const char *val) {
    struct VARIABLE *p;
    char *oldVal;
    char *newVal;
    BOOL changed = FALSE;

    if ((val == NULL) || (*val == 0) || (var == NULL)) {
        return FALSE;
    }

    if (v->shared)
        RWLAcquireWrite(&v->rwl);

    p = VariablesFind(v, var, variablesFindSet);
    if (p == NULL) {
        goto Finished;
    }

    changed = TRUE;

    if (p->val == NULL) {
        if (!(p->val = strdup(val)))
            PANIC;
    } else {
        if (!(newVal = malloc(strlen(p->val) + strlen(val) + 1)))
            PANIC;
        strcpy(newVal, p->val);
        strcat(newVal, val);

        oldVal = p->val;
        p->val = newVal;
        FreeThenNull(oldVal);
    }

Finished:
    if (v->shared)
        RWLReleaseWrite(&v->rwl);

    return changed;
}

void VariablesSetOrDeleteValue(struct VARIABLES *v, const char *var, const char *val) {
    if (val && *val)
        VariablesSetValue(v, var, val);
    else
        VariablesDelete(v, var);
}

BOOL VariablesAggregateTest(struct USERINFO *uip,
                            struct VARIABLES *v,
                            struct VARIABLES *sessionVariables,
                            const char *var,
                            const char *testVal,
                            enum AGGREGATETESTOPTIONS ato,
                            struct VARIABLES *rev) {
    struct AGGREGATETESTCONTEXT atc;
    struct VARIABLE *p;
    UUAVLTREEITERCTX uatic;
    size_t varLen = strlen(var);

    if (!ExpressionAggregateTestInitialize(&atc, uip, testVal, ato))
        return FALSE;

    if (v->shared)
        RWLAcquireRead(&v->rwl);

    for (p = (struct VARIABLE *)UUAvlTreeIterFirst(&v->avlVariables, &uatic); p;
         p = (struct VARIABLE *)UUAvlTreeIterNext(&v->avlVariables, &uatic)) {
        if (strncmp(p->var, var, varLen) != 0)
            continue;
        if (*(p->var + varLen) != '[' && *(p->var + varLen) != 0)
            continue;
        if (!ExpressionAggregateTestValue(&atc, p->val))
            break;
    }

    if (atc.more && sessionVariables && v != sessionVariables && StartsWith(var, "session:")) {
        /* Although there is some risk of deadlock, in reality, the majority case will be that
           the "v" variables are local variables, which don't do a RWL lock, so we really can't
           deadlock
        */
        if (sessionVariables->shared)
            RWLAcquireRead(&sessionVariables->rwl);

        for (p = (struct VARIABLE *)UUAvlTreeIterFirst(&sessionVariables->avlVariables, &uatic); p;
             p = (struct VARIABLE *)UUAvlTreeIterNext(&sessionVariables->avlVariables, &uatic)) {
            if (strncmp(p->var, var, varLen) != 0)
                continue;
            if (*(p->var + varLen) != '[' && *(p->var + varLen) != 0)
                continue;
            /* If we already examined this value from the other (likely local) variables, don't
               evaluate the session copy
            */
            if (VariablesFind(v, p->var, variablesFindGet))
                continue;
            if (!ExpressionAggregateTestValue(&atc, p->val))
                break;
        }

        if (sessionVariables->shared)
            RWLReleaseRead(&sessionVariables->rwl);
    }

    if (v->shared)
        RWLReleaseRead(&v->rwl);

    return ExpressionAggregateTestTerminate(&atc, rev);
}

int VariablesCount(struct VARIABLES *v, const char *var) {
    int count = 0;
    struct VARIABLE *p;
    UUAVLTREEITERCTX uatic;
    size_t varLen;

    if (v->shared)
        RWLAcquireRead(&v->rwl);

    varLen = strlen(var);

    for (p = (struct VARIABLE *)UUAvlTreeIterFirst(&v->avlVariables, &uatic); p;
         p = (struct VARIABLE *)UUAvlTreeIterNext(&v->avlVariables, &uatic)) {
        if (strncmp(p->var, var, varLen) != 0)
            continue;
        if (*(p->var + varLen) != '[' && *(p->var + varLen) != 0)
            continue;
        count++;
    }

    if (v->shared)
        RWLReleaseRead(&v->rwl);

    return count;
}

char *VariablesGetValue(struct VARIABLES *v, const char *var) {
    struct VARIABLE *p;
    char *copy = NULL;

    if (v->shared)
        RWLAcquireRead(&v->rwl);

    p = VariablesFind(v, var, variablesFindGet);
    if (p && p->val) {
        if (!(copy = strdup(p->val)))
            PANIC;
    }

    if (v->shared)
        RWLReleaseRead(&v->rwl);

    return copy;
}

char *VariablesGetValueByGroupMask(struct VARIABLES *v, const char *var, GROUPMASK gm) {
    struct VARIABLE *p;
    UUAVLTREEITERCTX uatic;
    size_t varLen;
    char *val = NULL;
    GROUPMASK gmTest = NULL;
    char *bracket;

    if (v->shared)
        RWLAcquireRead(&v->rwl);

    varLen = strlen(var);

    for (p = (struct VARIABLE *)UUAvlTreeIterFirst(&v->avlVariables, &uatic); p;
         p = (struct VARIABLE *)UUAvlTreeIterNext(&v->avlVariables, &uatic)) {
        if (strncmp(p->var, var, varLen) != 0)
            continue;
        bracket = p->var + varLen;
        if (*bracket != 0 && *bracket != '[')
            continue;
        /* If this the base scalar, hold that value, but allow that it
           can be overridden by any non-scalar that matches the group
        */
        if (*bracket == 0)
            val = p->val;
        else {
            /* Check to see if the index is in a group overlap */
            char *index;
            char *eos;
            if (!(index = strdup(bracket + 1)))
                PANIC;
            /* strip any trailing, closing bracket */
            eos = AtEnd(index);
            if (eos > index && *(eos - 1) == ']')
                *(eos - 1) = 0;
            gmTest = MakeGroupMask(gmTest, index, 0, '=');
            FreeThenNull(index);
            /* If it does overlap, use this value; here, we override the
               base scalar as well
            */
            if (GroupMaskOverlap(gm, gmTest)) {
                val = p->val;
                break;
            }
        }
    }

    /* If anything made it through, go ahead and dup it for return */
    if (val) {
        if (!(val = strdup(val)))
            PANIC;
    }

    if (v->shared)
        RWLReleaseRead(&v->rwl);

    GroupMaskFree(&gmTest);

    return val;
}

int VariablesForAllByGroupMask(struct TRANSLATE *t,
                               struct VARIABLES *v,
                               void *context,
                               GROUPMASK gm,
                               void (*callBack)(struct TRANSLATE *t,
                                                struct VARIABLES *v,
                                                void *context,
                                                const char *name,
                                                const char *value,
                                                int count)) {
    struct VARIABLE *p, *scalar = NULL, *vector = NULL;
    UUAVLTREEITERCTX uatic;
    GROUPMASK gmTest = NULL;
    char *bracket;
    int count = 0;
    char *name = NULL;
    char *lastName = NULL;

    if (callBack == NULL) {
        return 0;
    }

    if (v->shared) {
        RWLAcquireRead(&v->rwl);
    }

    for (p = (struct VARIABLE *)UUAvlTreeIterFirst(&v->avlVariables, &uatic); p;
         p = (struct VARIABLE *)UUAvlTreeIterNext(&v->avlVariables, &uatic)) {
        FreeThenNull(name);
        if (!(name = strdup(p->var)))
            PANIC;
        bracket = strchr(name, '[');
        if (bracket) {
            char *eos;
            *bracket++ = 0;
            eos = AtEnd(bracket);
            if (eos > bracket && *(eos - 1) == ']') {
                *(eos - 1) = 0;
            }
        }

        if (lastName && strcmp(name, lastName) != 0) {
            if (vector) {
                callBack(t, v, context, lastName, vector->val, ++count);
            } else if (scalar) {
                callBack(t, v, context, lastName, scalar->val, ++count);
            }
            scalar = vector = NULL;
            FreeThenNull(lastName);
        }

        if (lastName == NULL) {
            if (!(lastName = strdup(name)))
                PANIC;
        }

        if (bracket == NULL) {
            scalar = p;
        } else if (vector == NULL) {
            gmTest = MakeGroupMask(gmTest, bracket, 0, '=');
            if (GroupMaskOverlap(gm, gmTest)) {
                vector = p;
            }
        }
    }

    if (vector) {
        callBack(t, v, context, lastName, vector->val, ++count);
    } else if (scalar) {
        callBack(t, v, context, lastName, scalar->val, ++count);
    }

    if (v->shared) {
        RWLReleaseRead(&v->rwl);
    }

    if (count == 0) {
        callBack(t, v, context, NULL, NULL, 0);
    }

    GroupMaskFree(&gmTest);
    FreeThenNull(name);
    FreeThenNull(lastName);

    return count;
}

int VariablesForAll(struct TRANSLATE *t,
                    struct VARIABLES *v,
                    void *context,
                    void (*callBack)(struct TRANSLATE *t,
                                     struct VARIABLES *v,
                                     void *context,
                                     const char *name,
                                     const char *value,
                                     int count)) {
    struct VARIABLE *p;
    UUAVLTREEITERCTX uatic;
    int count = 0;

    if (callBack == NULL) {
        return 0;
    }
    if (v->shared) {
        RWLAcquireRead(&v->rwl);
    }

    for (p = (struct VARIABLE *)UUAvlTreeIterFirst(&v->avlVariables, &uatic); p;
         p = (struct VARIABLE *)UUAvlTreeIterNext(&v->avlVariables, &uatic)) {
        count++;
        callBack(t, v, context, p->var, p->val, count);
    }

    if (v->shared) {
        RWLReleaseRead(&v->rwl);
    }

    if (count == 0) {
        callBack(t, v, context, NULL, NULL, 0);
    }

    return count;
}

void VariablesDelete(struct VARIABLES *v, const char *var) {
    if (v->shared)
        RWLAcquireWrite(&v->rwl);

    VariablesFind(v, var, variablesFindDelete);

    if (v->shared)
        RWLReleaseWrite(&v->rwl);
}

BOOL VariablesMerge(struct VARIABLES *dst, struct VARIABLES *src, const char *wildFilter) {
    struct VARIABLE *p;
    BOOL changed = FALSE;
    UUAVLTREEITERCTX uatic;
    BOOL first = TRUE;

    /* This could be much more efficient using a balanced-line algorithm
       between both variables sets, but the number of variables
       will typically be so small that the code duplication
       is not warranted at this time
    */

    if (src == dst || src == NULL || dst == NULL)
        return FALSE;

    if (src->shared)
        RWLAcquireRead(&src->rwl);

    /* Don't bother locking the destination until we find at least one variable
       that actually needs to be stored
    */
    for (p = (struct VARIABLE *)UUAvlTreeIterFirst(&src->avlVariables, &uatic); p;
         p = (struct VARIABLE *)UUAvlTreeIterNext(&src->avlVariables, &uatic)) {
        if (wildFilter == NULL || WildCompare(p->var, wildFilter)) {
            if (first) {
                if (dst->shared)
                    RWLAcquireWrite(&dst->rwl);
                first = FALSE;
            }
            if (VariablesSetValue(dst, p->var, p->val))
                changed = TRUE;
        }
    }

    if (!first)
        if (dst->shared)
            RWLReleaseWrite(&dst->rwl);

    if (src->shared)
        RWLReleaseRead(&src->rwl);

    return changed;
}

void VariablesReset(struct VARIABLES *v) {
    struct VARIABLE *p, *next;
    UUAVLTREEITERCTX uatic;

    if (v) {
        if (v->shared)
            RWLAcquireWrite(&v->rwl);

        for (p = (struct VARIABLE *)UUAvlTreeIterFirst(&v->avlVariables, &uatic); p; p = next) {
            next = (struct VARIABLE *)UUAvlTreeIterNext(&v->avlVariables, &uatic);
            UUAvlTreeDelete(&v->avlVariables, p);
            FreeThenNull(p->var);
            FreeThenNull(p->val);
            FreeThenNull(p);
        }

        if (v->shared)
            RWLReleaseWrite(&v->rwl);
    }
}

void VariablesFree(struct VARIABLES **v) {
    if (*v) {
        VariablesReset(*v);
        if ((*v)->shared)
            RWLDestroy(&(*v)->rwl);
        FreeThenNull(*v);
    }
}

void VariablesLoad(struct VARIABLES *v, int f, struct FILEREADLINEBUFFER *frb) {
    char line[21850]; /* allow for values up to 16k in size */
    char *var = NULL;
    char *val = NULL;

    if (v->shared)
        RWLAcquireWrite(&v->rwl);

    while (FileReadLine(f, line, sizeof(line), frb)) {
        if (line[0] == 'R') {
            FreeThenNull(var);
            var = Decode64(NULL, line + 1);
            Trim(var, TRIM_CTRL);
            continue;
        }

        if (line[0] == 'L') {
            if (var == NULL || *var == 0)
                continue;
            val = Decode64(NULL, line + 1);
            VariablesSetValue(v, var, val);
            FreeThenNull(val);
        }
    }

    FreeThenNull(var);

    if (v->shared)
        RWLReleaseWrite(&v->rwl);
}

void VariablesSave(struct VARIABLES *v, int f) {
    struct VARIABLE *p;
    UUAVLTREEITERCTX uatic;
    char *base64 = NULL;

    if (v->shared)
        RWLAcquireRead(&v->rwl);

    for (p = (struct VARIABLE *)UUAvlTreeIterFirst(&v->avlVariables, &uatic); p;
         p = (struct VARIABLE *)UUAvlTreeIterNext(&v->avlVariables, &uatic)) {
        if (p->var && *p->var) {
            base64 = Encode64(NULL, p->var);
            write(f, "R", 1);
            write(f, base64, strlen(base64));
            FreeThenNull(base64);

            if (p->val && *p->val) {
                base64 = Encode64(NULL, p->val);
                write(f, "\nL", 2);
                write(f, base64, strlen(base64));
            }
            write(f, "\n", 1);
        }
    }

    if (v->shared)
        RWLReleaseRead(&v->rwl);
}

char *VariablesHTMLOptions(struct VARIABLES *v, const char *var, const char *defaultValue) {
    struct VARIABLE *p;
    UUAVLTREEITERCTX uatic;
    size_t varLen = strlen(var);
    char *val = NULL;
    struct HTMLOPTION {
        struct HTMLOPTION *next;
        char *value;
        char *label;
    } *htmlOptions = NULL, *n, *q, *last;
    char *index;
    size_t len = 0;
    char *e;
    size_t defaultValueLen = defaultValue ? strlen(defaultValue) : 0;

    if (v->shared)
        RWLAcquireRead(&v->rwl);

    for (p = (struct VARIABLE *)UUAvlTreeIterFirst(&v->avlVariables, &uatic); p;
         p = (struct VARIABLE *)UUAvlTreeIterNext(&v->avlVariables, &uatic)) {
        if (strncmp(p->var, var, varLen) != 0)
            continue;
        index = p->var + varLen;
        if (*index != '[')
            continue;
        if (p->val == NULL || *p->val == 0)
            continue;
        index++;

        for (q = htmlOptions, last = NULL; q; last = q, q = q->next) {
            int c = stricmp(q->label, p->val);
            if (c >= 0)
                break;
        }

        if (!(n = calloc(1, sizeof(*n))))
            PANIC;
        n->value = index;
        n->label = p->val;
        n->next = q;
        if (last)
            last->next = n;
        else
            htmlOptions = n;

        /* the *6 allows every char to become #&###; and the 32 is for
        <option value=""></option>
        */
        len += strlen(n->label) * 6 + strlen(n->label) * 6 + 32;
    }

    if (len) {
        len += 32; /* add more remove for selected='selected' */
        if (!(val = malloc(len)))
            PANIC;
        *val = 0;
        for (q = htmlOptions; q; q = n) {
            strcat(val, "<option value='");
            e = AddHTMLEncodedField(val, q->value, NULL);
            if (e > val && *(e - 1) == ']')
                *(e - 1) = 0;
            strcat(val, "'");
            if (defaultValueLen && strncmp(defaultValue, q->value, defaultValueLen) == 0 &&
                strlen(q->value) - 1 == defaultValueLen) {
                strcat(val, " selected='selected'");
                defaultValueLen = 0;
            }
            strcat(val, ">");
            AddHTMLEncodedField(val, q->label, NULL);
            strcat(val, "</option>\n");
            n = q->next;
            FreeThenNull(q);
        }
    }

    if (v->shared)
        RWLReleaseRead(&v->rwl);

    return UUStrRealloc(val);
}

int VariablesTable(struct TRANSLATE *t,
                   struct VARIABLES *v,
                   const char *filter,
                   const char *header) {
    struct VARIABLE *p;
    struct UUSOCKET *s = &t->ssocket;
    UUAVLTREEITERCTX uatic;
    int count = 0;

    if (v->shared)
        RWLAcquireRead(&v->rwl);

    for (p = (struct VARIABLE *)UUAvlTreeIterFirst(&v->avlVariables, &uatic); p;
         p = (struct VARIABLE *)UUAvlTreeIterNext(&v->avlVariables, &uatic)) {
        if (filter && StartsIWith(p->var, filter) == NULL)
            continue;
        if (count++ == 0) {
            if (header && *header)
                UUSendZ(s, header);
            UUSendZ(s, "<table class='bordered-table'>\n");
            UUSendZ(s, "<tr><th scope='col'>Variable</th><th scope='col'>Value</th></tr>\n");
        }
        UUSendZ(s, "<tr><td>");
        SendHTMLEncoded(s, p->var);
        UUSendZ(s, "</td><td>");
        if (p->val == NULL)
            UUSendZ(s, "<strong>NULL</strong>");
        else if (*SkipWS(p->val) == 0)
            UUSendZ(s, "&nbsp;");
        else
            SendHTMLEncoded(s, p->val);
        UUSendZ(s, "</td></tr>\n");
    }

    if (v->shared)
        RWLReleaseRead(&v->rwl);

    if (count)
        UUSendZ(s, "</table>\n");

    return count;
}

int VariablesSendHttpHeaders(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket;
    struct VARIABLES *v = t->localVariables;
    struct VARIABLE *p;
    char *q;
    UUAVLTREEITERCTX uatic;
    int count = 0;

    if (v->shared)
        RWLAcquireRead(&v->rwl);

    for (p = (struct VARIABLE *)UUAvlTreeIterFirst(&t->localVariables->avlVariables, &uatic); p;
         p = (struct VARIABLE *)UUAvlTreeIterNext(&t->localVariables->avlVariables, &uatic)) {
        if ((q = StartsWith(p->var, "http:header")) && (*q == '[' || *q == 0)) {
            if (p->val == NULL)
                continue;
            /* This can only shorten val, so it is safe */
            Trim(p->val, TRIM_TRAIL);
            if (*p->val == 0)
                continue;
            UUSend2(s, p->val, strlen(p->val), "\r\n", 2, 0);
            count++;
        }

        /* For cookie, anything after @ is sent as the domain, and any index is sent as expiration,
        such as cookie:abc@yourlib.org[Fri, 01-Jan-1999 00:00:00 GMT] ="value" being sent as
        Set-Cookie: abc=value; domain=yourlib.org; expires=Fri, 01-Jan-1999 00:00:00 GMT
        */
        if ((q = StartsWith(p->var, "setcookie:")) && *q) {
            char *at = strchr(q, '@');
            char *bracket = strchr(q, '[');
            char *stop;

            if (at > bracket)
                at = NULL;

            stop = at ? at : bracket;

            UUSendZ(s, "Set-Cookie: ");
            UUSend(s, q, stop ? stop - q : strlen(q), 0);
            UUSendZ(s, "=");

            if (p->val && *p->val)
                UUSendZ(s, p->val);

            if (at) {
                at++;
                if (*at) {
                    UUSendZ(s, "; domain=");
                    UUSend(s, at, (bracket ? bracket - at : strlen(at)), 0);
                }
            }

            if (p->val == NULL || *p->val == 0)
                UUSendZ(s, "; expires=Fri, 01-Jan-1999 00:00:00 GMT");
            else if (bracket && strlen(bracket) > 2) {
                char *dateIn;
                time_t when;
                BOOL validWhen = FALSE;

                if (!(dateIn = strdup(bracket + 1)))
                    PANIC;
                if (bracket = strrchr(dateIn, ']'))
                    *bracket = 0;

                if (IsOneOrMoreDigits(dateIn)) {
                    when = atoi(dateIn);
                    validWhen = TRUE;
                } else {
                    when = ParseLocalDate(dateIn, 0);
                    validWhen = when != 0;
                }

                UUSendZ(s, "; expires=");
                if (validWhen) {
                    char httpDate[HTTPDATELENGTH];
                    UUSendZ(s, FormatHttpDate(httpDate, sizeof(httpDate), when));
                } else {
                    UUSendZ(s, dateIn);
                }

                FreeThenNull(dateIn);
            }

            UUSendCRLF(s);
        }
    }

    if (v->shared)
        RWLReleaseRead(&v->rwl);

    return count;
}

void AdminVariables(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    struct SESSION *e;
    const int AVSESSION = 0;
    struct FORMFIELD ffs[] = {
        {"session", NULL, 0, 0, MAXKEY},
        {NULL,      NULL, 0, 0, 0     }
    };

    FindFormFields(query, post, ffs);

    AdminHeader(t, 0, "Session Variables");

    if (ffs[AVSESSION].value) {
        e = FindSessionByAdminKey(ffs[AVSESSION].value);
        if (e == NULL) {
            UUSendZ(s, "<p>Invalid session: ");
            SendHTMLEncoded(s, ffs[AVSESSION].value);
            UUSendZ(s, "</p>\n");
            goto Finished;
        }
    } else {
        e = t->session;
    }

    if (e) {
        if (VariablesTable(t, e->sessionVariables, NULL, NULL) == 0) {
            UUSendZ(s, "<p>This session does not have any session variables.</p>\n");
        }
    }

Finished:
    AdminFooter(t, 0);
}
