#ifndef __ADMINFIDDLER_H__
#define __ADMINFIDDLER_H__

#include "common.h"

void AdminFiddler(struct TRANSLATE *t, char *query, char *post);
void FiddlerCleanup(void);

#endif  // __ADMINFIDDLER_H__
