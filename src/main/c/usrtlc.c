/**
 * TLC (small ILS) user authentication
 *
 * This authentication method supports common conditions and actions.
 */

#define __UUFILE__ "usrtlc.c"

#include "common.h"

#include "usr.h"
#include "usrtlc.h"

#include "uuxml.h"

#include "html.h"

struct TLCCTX {
    char *patronName;
    char *patronStatus;
    char *overdue;
    char *fee;
};

static char *UsrTLCAuthGetValue(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                void *context,
                                const char *var,
                                const char *index) {
    struct TLCCTX *tlcCtx = (struct TLCCTX *)context;
    char *val = NULL;

    if (strcmp(var, "Name") == 0)
        val = tlcCtx->patronName;
    else if (strcmp(var, "Status") == 0)
        val = tlcCtx->patronStatus;
    else if (strcmp(var, "Overdue") == 0)
        val = tlcCtx->overdue;
    else if (strcmp(var, "Fee") == 0)
        val = tlcCtx->fee;
    else
        UsrLog(uip, "Unknown TLC auth variable: %s", var);

    if (val)
        if (!(val = strdup(val)))
            PANIC;

    return val;
}

int UsrTLCIfTest(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct TLCCTX *tlcCtx = (struct TLCCTX *)context;
    char *arg2;
    char compareOperator;
    char compareType;
    char *val;
    int result = 1;

    arg2 = SkipNonWS(arg);
    if (*arg2) {
        *arg2++ = 0;
        arg2 = SkipWS(arg2);
    }

    arg2 = UsrTestComparisonOperatorType(arg2, &compareOperator, &compareType);

    if (stricmp(arg, "Name") == 0)
        val = tlcCtx->patronName;
    else if (stricmp(arg, "Status") == 0)
        val = tlcCtx->patronStatus;
    else if (stricmp(arg, "Overdue") == 0)
        val = tlcCtx->overdue;
    else if (stricmp(arg, "Fee") == 0)
        val = tlcCtx->fee;
    else {
        UsrLog(uip, "TLC unknown test variable: %s", arg);
        return 1;
    }

    if (val == NULL)
        val = "";

    result = UsrTestCompare((char *)val, arg2, compareOperator, compareType) ? 0 : 1;

    if (uip->debug)
        UsrLog(uip, "TLC test %s %s against %s result %s", arg, val, arg2,
               result ? "FALSE" : "TRUE");

    return result;
}

int UsrTLCURL(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    int result = 1;
    char *user, *pass;
    struct TLCCTX *tlcCtx = (struct TLCCTX *)context;
    xmlXPathContextPtr xpathCtx = NULL;
    xmlXPathObjectPtr xpathObj = NULL;
    xmlChar *value = NULL;
    xmlNodeSetPtr nodes = NULL;
    time_t expires = 0;
    htmlParserCtxtPtr ctxt = NULL;
    struct COOKIE *cookies = NULL;
    int i;
    char *p;
    struct tm tm;
    int tries = 0;
    int tdLi;
    xmlChar *iframe = NULL;

    user = uip->user ? uip->user : "";
    pass = uip->pass ? uip->pass : "";

    if (*user == 0 || strlen(user) > 32 || strlen(pass) > 32)
        goto Finished;

    strcpy(t->buffer, "FormId=0&Config=PAC&Branch=%2C0%2C&PatronId=&ItemsToRenew=&LoginID=");
    AddEncodedField(t->buffer, user, NULL);
    strcat(t->buffer, "&PIN=");
    AddEncodedField(t->buffer, pass, NULL);

    ctxt = GetHTML(arg, t->buffer, &cookies, NULL, uip->pInterface, 0, 0, NULL, NULL, uip->debug);

    if (ctxt)
        uip->result = resultInvalid;

Again:
    if (ctxt == NULL) {
        UsrLog(uip, "TLC server did not respond for %s\n", arg);
        goto Finished;
    }

    xpathCtx = xmlXPathNewContext(ctxt->myDoc);
    if (xpathCtx == NULL)
        goto Finished;

    for (tdLi = 0; tdLi < 2; tdLi++) {
        xpathObj = xmlXPathEvalExpression(tdLi ? BAD_CAST "//li" : BAD_CAST "//td", xpathCtx);
        if (xpathObj == NULL || (nodes = xpathObj->nodesetval) == NULL || (nodes->nodeNr == 0)) {
            continue;
        }

        for (i = 0; i < nodes->nodeNr; i++) {
            value = xmlNodeGetContent(nodes->nodeTab[i]);
            Trim((char *)value, TRIM_COMPRESS | TRIM_NBSP);
            if (p = StartsIWith((char *)value, "Patron Name:")) {
                p = SkipWS(p);
                FreeThenNull(tlcCtx->patronName);
                if (*p)
                    if (!(tlcCtx->patronName = strdup(p)))
                        PANIC;
            }
            if ((p = StartsIWith((char *)value, "Patron Status:")) ||
                (p = StartsIWith((char *)value, "Status:"))) {
                p = SkipWS(p);
                FreeThenNull(tlcCtx->patronStatus);
                if (*p)
                    if (!(tlcCtx->patronStatus = strdup(p)))
                        PANIC;
                uip->result = resultValid;
            }
            if ((p = StartsIWith((char *)value, "Fee:")) ||
                (p = StartsIWith((char *)value, "Fees($):"))) {
                p = SkipWS(p);
                if (*p == '$')
                    p++;
                FreeThenNull(tlcCtx->fee);
                if (*p)
                    if (!(tlcCtx->fee = strdup(p)))
                        PANIC;
            }
            if ((p = StartsIWith((char *)value, "Overdue:")) ||
                (p = StartsIWith((char *)value, "Items Overdue:"))) {
                p = SkipWS(p);
                FreeThenNull(tlcCtx->overdue);
                if (*p)
                    if (!(tlcCtx->overdue = strdup(p)))
                        PANIC;
            }
            if (p = StartsIWith((char *)value, "Patron Card Expiration:")) {
                char *q;
                p = SkipWS(p);
                memset(&tm, 0, sizeof(tm));
                for (tm.tm_mon = 0; tm.tm_mon < 12; tm.tm_mon++) {
                    if ((q = StartsIWith(p, monthNames[tm.tm_mon])) && *q == ' ')
                        break;
                }
                if (q) {
                    sscanf(q, "%d, %d", &tm.tm_mday, &tm.tm_year);
                    if (tm.tm_year >= 1900)
                        tm.tm_year -= 1900;
                    tm.tm_isdst = -1;
                    expires = mktime(&tm);
                }
            }

            xmlFreeThenNull(value);
        }
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    if (uip->result != resultValid && tries == 0) {
        iframe = UUxmlSimpleGetContent(NULL, xpathCtx, "//iframe[@id = 'displayframe']/@src");
    }

    xmlXPathFreeContext(xpathCtx);
    xpathCtx = NULL;

    UUxmlFreeParserCtxtAndDoc(&ctxt);

    if (uip->result == resultValid || tries++ > 0)
        goto Finished;

    if (iframe) {
        char *iframeAbs = AbsoluteURL(arg, (char *)iframe);
        ctxt =
            GetHTML(iframeAbs, NULL, &cookies, NULL, uip->pInterface, 0, 0, NULL, NULL, uip->debug);
        FreeThenNull(iframeAbs);
        goto Again;
    }

Finished:
    if (uip->result == resultValid) {
        time_t now;
        UUtime(&now);
        if (expires && now >= expires) {
            uip->result = resultExpired;
        }
    }

    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    if (xpathCtx) {
        xmlXPathFreeContext(xpathCtx);
        xpathCtx = NULL;
    }

    UUxmlFreeParserCtxtAndDoc(&ctxt);
    GetHTMLFreeCookies(&cookies);
    xmlFreeThenNull(iframe);

    if (uip->debug) {
        UsrLog(uip, "TLC results for %s", user);
        if (uip->result == resultValid) {
            UsrLog(uip, "Account valid");
        }
        if (uip->result == resultExpired) {
            UsrLog(uip, "Account expired");
        }
        if (tlcCtx->patronName)
            UsrLog(uip, "Name: %s", tlcCtx->patronName);
        if (tlcCtx->patronStatus)
            UsrLog(uip, "Status: %s", tlcCtx->patronStatus);
        if (tlcCtx->fee)
            UsrLog(uip, "Fee: %s", tlcCtx->fee);
        if (tlcCtx->overdue)
            UsrLog(uip, "Overdue: %s", tlcCtx->overdue);
        if (expires) {
            UUlocaltime_r(&expires, &tm);
            UsrLog(uip, "Expires: %04d-%02d-%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
        }
    }

    return result;
}

enum FINDUSERRESULT FindUserTLC(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                char *file,
                                int f,
                                struct FILEREADLINEBUFFER *frb,
                                struct sockaddr_storage *pInterface) {
    struct USRDIRECTIVE udc[] = {
        {"IfTest", UsrTLCIfTest, 0},
        {"URL",    UsrTLCURL,    0},
        {NULL,     NULL,         0}
    };
    enum FINDUSERRESULT result;
    struct TLCCTX TLCCtx;

    memset(&TLCCtx, 0, sizeof(TLCCtx));

    uip->flags = 0;
    uip->authGetValue = UsrTLCAuthGetValue;
    result = UsrHandler(t, "TLC", udc, &TLCCtx, uip, file, f, frb, pInterface, NULL);

    FreeThenNull(TLCCtx.patronName);
    FreeThenNull(TLCCtx.patronStatus);
    FreeThenNull(TLCCtx.overdue);
    FreeThenNull(TLCCtx.fee);

    return result;
}
