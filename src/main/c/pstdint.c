/*  A portable stdint.h
 ****************************************************************************
 *  BSD License:
 ****************************************************************************
 *
 *  Copyright (c) 2005-2007 Paul Hsieh
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. The name of the author may not be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 *  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/*
 *  Please compile with the maximum warning settings to make sure macros are not
 *  defined more than once.
 */
#ifdef PSTDINT_BUILD

#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#define DECLU(bits) uint##bits##_t u##bits = UINT##bits##_C(0);
#define DECLD(bits) int##bits##_t d##bits = INT##bits##_C(0);

#define DECL(us, bits) DECL##us(bits)

#define TESTUMAX(bits)                                     \
    u##bits = ~u##bits;                                    \
    if (UINT##bits##_MAX != u##bits) {                     \
        printf("Something wrong with UINT%d_MAX\n", bits); \
        result = EXIT_FAILURE;                             \
    }

int main(int argc, char **argv) {
    int result = EXIT_SUCCESS;
    DECL(D, 8);
    DECL(U, 8);
    DECL(D, 16);
    DECL(U, 16);
    DECL(D, 32);
    DECL(U, 32);
#ifdef INT64_MAX
    DECL(D, 64);
    DECL(U, 64);
#endif
    intmax_t dmax = INTMAX_C(0);
    uintmax_t umax = UINTMAX_C(0);
    char str0[256], str1[256];

    sprintf(str0, "%" PRId8 " %x\n", 0, ~0);

    sprintf(str1, "%" PRId8 " %x\n", d8, ~0);
    if (0 != strcmp(str0, str1)) {
        printf("Something wrong with d8 : %s\n", str1);
        result = EXIT_FAILURE;
    }
    sprintf(str1, "%" PRIu8 " %x\n", u8, ~0);
    if (0 != strcmp(str0, str1)) {
        printf("Something wrong with u8 : %s\n", str1);
        result = EXIT_FAILURE;
    }
    sprintf(str1, "%" PRId16 " %x\n", d16, ~0);
    if (0 != strcmp(str0, str1)) {
        printf("Something wrong with d16 : %s\n", str1);
        result = EXIT_FAILURE;
    }
    sprintf(str1, "%" PRIu16 " %x\n", u16, ~0);
    if (0 != strcmp(str0, str1)) {
        printf("Something wrong with u16 : %s\n", str1);
        result = EXIT_FAILURE;
    }
    sprintf(str1, "%" PRId32 " %x\n", d32, ~0);
    if (0 != strcmp(str0, str1)) {
        printf("Something wrong with d32 : %s\n", str1);
        result = EXIT_FAILURE;
    }
    sprintf(str1, "%" PRIu32 " %x\n", u32, ~0);
    if (0 != strcmp(str0, str1)) {
        printf("Something wrong with u32 : %s\n", str1);
        result = EXIT_FAILURE;
    }
#ifdef INT64_MAX
    sprintf(str1, "%" PRId64 " %x\n", d64, ~0);
    if (0 != strcmp(str0, str1)) {
        printf("Something wrong with d64 : %s\n", str1);
        result = EXIT_FAILURE;
    }
#endif
    sprintf(str1, "%" PRIdMAX " %x\n", dmax, ~0);
    if (0 != strcmp(str0, str1)) {
        printf("Something wrong with dmax : %s\n", str1);
        result = EXIT_FAILURE;
    }
    sprintf(str1, "%" PRIuMAX " %x\n", umax, ~0);
    if (0 != strcmp(str0, str1)) {
        printf("Something wrong with umax : %s\n", str1);
        result = EXIT_FAILURE;
    }

    TESTUMAX(8);
    TESTUMAX(16);
    TESTUMAX(32);
#ifdef INT64_MAX
    TESTUMAX(64);
#endif

    printf("%s: %s.\n", argv[0], result == 0 ? "success" : "failure");
    return result;
}

#endif
