/**
 * ncpu.c
 *
 * Discover the number of CPUs available to the program.
 *
 *  Created on: May 25, 2010
 *      Author: Steven Eastman
 */

/* Here we try to turn on any native APIs */
#undef _BSD_TYPES
#define _BSD_TYPES 1
#undef _DARWIN_C_SOURCE
#define _DARWIN_C_SOURCE 1
#undef _GNU_SOURCE
#define _GNU_SOURCE 1
#undef _NETBSD_SOURCE
#define _NETBSD_SOURCE 1
#undef __BSD_VISIBLE
#define __BSD_VISIBLE 1
#undef _XOPEN_SOURCE
#define _XOPEN_SOURCE 500

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef WIN32
#include <windows.h>
// # include <inttypes.h>
// typedef int ssize_t;
// typedef unsigned int size_t;
// typedef long int intmax_t;
// typedef long unsigned int uintmax_t;
#else
#include <unistd.h>
#include <inttypes.h>
#endif

/* 7.18.4.2. Macros for greatest-width integer constants if they're not
 * already defined.  Solaris 8 doesn't define them even though it claims
 * to be POSIX compliant. */
#ifndef INTMAX_C
#if LONG_MAX >> 30 == 1
#define INTMAX_C(x) x##LL
#elif defined GL_INT64_T
#define INTMAX_C(x) INT64_C(x)
#else
#define INTMAX_C(x) x##L
#endif
#endif
#ifndef UINTMAX_C
#if ULONG_MAX >> 31 == 1
#define UINTMAX_C(x) x##ULL
#elif defined GL_UINT64_T
#define UINTMAX_C(x) UINT64_C(x)
#else
#define UINTMAX_C(x) x##UL
#endif
#endif
#ifndef PRIdMAX
#define PRIdMAX "ld"
#endif
#ifndef PRIuMAX
#define PRIuMAX "lu"
#endif
#ifndef PRIxMAX
#define PRIxMAX "lx"
#endif

/* The standard "C" headers from 1989.  Every system has these even Windows. */
#include <limits.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>

#include <sys/types.h>
#ifdef HAVE_SYS_PARAM_H
#include <sys/param.h>
#endif
#ifdef HAVE_SYS_SYSINFO_H
#include <sys/sysinfo.h>
#endif
#ifdef HAVE_MACHINE_HAL_SYSINFO_H
#include <machine/hal_sysinfo.h>
#endif
#ifdef HAVE_SYS_MPCTL_H
#include <sys/mpctl.h>
#endif
#ifdef HAVE_SYS_SCOKET_H
#include <sys/socket.h>
#endif
#ifdef HAVE_SYS_GMON_H
#include <sys/gmon.h>
#endif
#ifdef HAVE_MACH_VM_PARM_H
#include <mach/vm_param.h>
#endif
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif
#ifdef HAVE_NETINET_ICMP_VAR_H
#include <netinet/icmp_var.h>
#endif
#ifdef HAVE_NETINET_ICMP6_H
#include <netinet/icmp6.h>
#endif
#ifdef HAVE_NETINET_UDP_VAR_H
#include <netinet/udp_var.h>
#endif
#ifdef HAVE_SYS_SYSCTL_H
#include <sys/sysctl.h>
#endif

#ifdef WIN32
typedef long int intmax_t;
#endif

intmax_t cpusAvailable(void) {
    intmax_t ncpus = INTMAX_C(-1);
#if defined(CTL_HW) && defined(HW_NCPU) && defined(HAVE_SYSCTL)
    intmax_t len = sizeof(ncpus);
    int mib[2];
#endif
#ifdef HAVE_CPU_INFO
    struct cpu_info c;
#endif
#ifdef WIN32
    SYSTEM_INFO sysinfo;
#endif

#ifdef WIN32
    if (ncpus == INTMAX_C(-1)) {
        GetSystemInfo(&sysinfo);
        ncpus = sysinfo.dwNumberOfProcessors;
    }
#endif

#if defined(_SC_NPROCESSORS_CONF) && defined(HAVE_SYSCONF)
    if (ncpus == INTMAX_C(-1)) {
        ncpus = sysconf(_SC_NPROCESSORS_CONF);
    }
#endif

#if defined(_SC_NPROCESSORS_ONLN) && defined(HAVE_SYSCONF)
    if (ncpus == INTMAX_C(-1)) {
        ncpus = sysconf(_SC_NPROCESSORS_ONLN);
    }
#endif

#if defined(_SC_NPROC_ONLN) && defined(HAVE_SYSCONF)
    if (ncpus == INTMAX_C(-1)) {
        ncpus = sysconf(_SC_NPROC_ONLN);
    }
#endif

#if defined(CTL_HW) && defined(HW_NCPU) && defined(HAVE_SYSCTL)
    if (ncpus == INTMAX_C(-1)) {
        size_t j;
        size_t k;
        mib[0] = CTL_HW;
        mib[1] = HW_NCPU;
        if (sysctl(&mib[0], 2, &j, &k, NULL, 0) != 0) {
            ncpus = -1;
            len = -1;
        } else {
            ncpus = (intmax_t)j;
            len = (intmax_t)k;
        }
    }
#endif

#if defined(HAVE_GETSYSINFO) && defined(GSI_CPU_INFO)
    if (ncpus == INTMAX_C(-1)) {
        if (getsysinfo(GSI_CPU_INFO, &c, sizeof(c), 0, NULL, NULL) == -1) {
            ncpus = -1;
        } else {
            ncpus = c.cpus_in_box;
        }
    }
#endif

#if defined(HAVE_MPCTL) && defined(MPC_GETNUMSPUS)
    if (ncpus == INTMAX_C(-1)) {
        ncpus = mpctl(MPC_GETNUMSPUS, NULL, NULL);
    }
#endif

    return ncpus;
}

#ifdef NCPU_BUILD
int main(int argc, char **argv) {
    long int ncpus = INTMAX_C(-1);

    ncpus = cpusAvailable();
    if (ncpus == INTMAX_C(-1)) {
        printf("%s: this system cannot report the number of CPUs available.\n", argv[0],
               (intmax_t)ncpus);
    } else {
        printf("%s: %" PRIdMAX "\n", argv[0], (intmax_t)ncpus);
    }

    return 0;
}
#endif
