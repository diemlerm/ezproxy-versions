#define __UUFILE__ "adminproxyurl.c"

#include "common.h"
#include "uustring.h"

#include "adminproxyurl.h"

void AdminProxyUrl(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    xmlNodeSetPtr nodes = NULL;
    xmlParserCtxtPtr ctxt = NULL;
    xmlDocPtr inDoc = NULL; /* the resulting document tree */
    xmlXPathContextPtr xpathCtx = NULL;
    xmlXPathObjectPtr xpathObj = NULL;
    /* xmlDtdPtr dtd = NULL;     */ /* DTD pointer */
    xmlDocPtr outDoc = NULL;        /* document pointer */
    xmlNodePtr root_node = NULL, main_node, node = NULL;
    xmlChar *docTxt = NULL;
    xmlChar *password = NULL;
    int docTxtLen;
    xmlChar *url;
    int i;
    size_t max;
    int port;
    char sport[6];
    const int APXML = 0;
    struct FORMFIELD ffs[] = {
        {"xml", NULL, 0, 0, 0, FORMFIELDNOTRIMCTRL},
        {NULL, NULL, 0, 0}
    };
    char *inXml = NULL;
    char *defaultForm = TAG_DOCTYPE_HTML_HEAD
        "\
</head>\n\
<body>\n\
<form action='/proxy_url' method='post'>\n\
<textarea name='xml' rows='10' cols='80'>&lt;?xml version=\"1.0\"?&gt;\n\
&lt;proxy_url_request password=\"secret\"&gt;\n\
&lt;urls&gt;\n\
&lt;url&gt;http://www.somedb.com&lt;/url&gt;\n\
&lt;url&gt;http://www.otherdb.com/search/&lt;/url&gt;\n\
&lt;/urls&gt;\n\
&lt;/proxy_url_request&gt;</textarea>\n\
<br>\n\
<input type='submit'>\n\
</form>\n\
</body>\n\
</html>\n";

    if (proxyUrlPassword == NULL) {
        HTTPCode(t, 404, 1);
        goto Finished;
    }

    if ((post == NULL && query != NULL && strncmp(query, "xml=", 4) == 0) ||
        (post != NULL && strncmp(post, "xml=", 4) == 0)) {
        FindFormFields(query, post, ffs);
        inXml = ffs[APXML].value;
    } else {
        inXml = post;
    }

    if (inXml == NULL || *inXml == 0) {
        HTMLHeader(t, htmlHeaderOmitCache);
        UUSendZ(s, defaultForm);
        goto Finished;
    }

    if (inXml != NULL)
        ctxt = xmlCreatePushParserCtxt(NULL, NULL, inXml, strlen(inXml), NULL);

    if (ctxt == NULL) {
        HTTPCode(t, 400, 0);
        HTTPContentType(t, NULL, 1);
        UUSendZ(s, "XML missing or unparseable\n");
        goto Finished;
    }

    xmlParseChunk(ctxt, inXml, 0, 1);

    inDoc = ctxt->myDoc;

    xpathCtx = xmlXPathNewContext(inDoc);
    if (xpathCtx == NULL) {
        Log("Error: unable to create new XPath context to find proxy_urls");
        HTTPCode(t, 503, 1);
        goto Finished;
    }

    xpathObj = xmlXPathEvalExpression(BAD_CAST "/proxy_url_request", xpathCtx);

    if (xpathObj != NULL && (nodes = xpathObj->nodesetval) != NULL && nodes->nodeNr == 1) {
        password = xmlGetProp(nodes->nodeTab[0], BAD_CAST "password");
    }

    if (password == NULL || proxyUrlPassword == NULL ||
        strcmp((char *)password, proxyUrlPassword) != 0) {
        HTTPCode(t, 400, 0);
        HTTPContentType(t, NULL, 1);
        UUSendZ(s, "Authorization failure\n");
        goto Finished;
    }

    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    if (xpathCtx) {
        xmlXPathFreeContext(xpathCtx);
        xpathCtx = NULL;
    }

    xpathCtx = xmlXPathNewContext(inDoc);
    if (xpathCtx == NULL) {
        Log("Error: unable to create new XPath context to find proxy_urls");
        HTTPCode(t, 503, 1);
        goto Finished;
    }

    outDoc = xmlNewDoc(BAD_CAST "1.0");
    root_node = xmlNewNode(NULL, BAD_CAST "proxy_url_response");
    xmlDocSetRootElement(outDoc, root_node);
    /*
     * Creates a DTD declaration. Isn't mandatory.
     */
    /* dtd = xmlCreateIntSubset(doc, BAD_CAST "root", NULL, BAD_CAST "tree2.dtd"); */
    main_node = xmlNewChild(root_node, NULL, BAD_CAST "proxy_urls", NULL);

    xpathObj = xmlXPathEvalExpression(BAD_CAST "/proxy_url_request/urls/url", xpathCtx);

    if (xpathObj != NULL && (nodes = xpathObj->nodesetval) != NULL) {
        max = sizeof(t->buffer) - 256;
        for (i = 0; i < nodes->nodeNr; i++) {
            if (nodes->nodeTab[i] == NULL)
                continue;
            url = xmlNodeGetContent(nodes->nodeTab[i]);
            if (url) {
                node = xmlNewTextChild(main_node, NULL, BAD_CAST "url", url);
                if (VerifyHost((char *)url, NULL, NULL) != 0) {
                    xmlNewProp(node, BAD_CAST "proxy", BAD_CAST "false");
                    xmlNewProp(node, BAD_CAST "scheme", BAD_CAST "");
                    xmlNewProp(node, BAD_CAST "hostname", BAD_CAST "");
                    xmlNewProp(node, BAD_CAST "port", BAD_CAST "");
                    xmlNewProp(node, BAD_CAST "login_path", BAD_CAST "");
                    xmlNewProp(node, BAD_CAST "encode", BAD_CAST "false");
                } else {
                    xmlNewProp(node, BAD_CAST "proxy", BAD_CAST "true");
                    if (strnicmp(t->buffer, "https", 5) == 0) {
                        xmlNewProp(node, BAD_CAST "scheme", BAD_CAST "https");
                        xmlNewProp(node, BAD_CAST "hostname", BAD_CAST myHttpsName);
                        port = (primaryLoginPortSsl == 443 ? 0 : primaryLoginPortSsl);
                    } else {
                        xmlNewProp(node, BAD_CAST "scheme", BAD_CAST "http");
                        xmlNewProp(node, BAD_CAST "hostname", BAD_CAST myName);
                        port = (primaryLoginPort == 80 ? 0 : primaryLoginPort);
                    }
                    if (port == 0)
                        sport[0] = 0;
                    else
                        sprintf(sport, "%d", port);
                    xmlNewProp(node, BAD_CAST "port", BAD_CAST sport);
                    xmlNewProp(node, BAD_CAST "login_path", BAD_CAST "/login?qurl=");
                    xmlNewProp(node, BAD_CAST "encode", BAD_CAST "true");
                }
                xmlFree(url);
                url = NULL;
            }
        }
    }

    HTTPCode(t, 200, 0);
    HTTPContentType(t, "text/xml", 1);

    xmlDocDumpMemoryEnc(outDoc, &docTxt, &docTxtLen, "UTF-8");
    UUSend(s, docTxt, docTxtLen, 0);

Finished:

    if (password) {
        xmlFree(password);
        password = NULL;
    }

    if (docTxt) {
        xmlFree(docTxt);
        docTxt = NULL;
    }

    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    if (xpathCtx) {
        xmlXPathFreeContext(xpathCtx);
        xpathCtx = NULL;
    }

    if (outDoc) {
        xmlFreeDoc(outDoc);
        outDoc = NULL;
    }

    if (inDoc) {
        xmlFreeDoc(inDoc);
        inDoc = NULL;
    }

    if (ctxt) {
        xmlFreeParserCtxt(ctxt);
        ctxt = NULL;
    }
}
