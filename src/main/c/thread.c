#define __UUFILE__ "thread.c"

#include "common.h"

#define OPENSSL_THREAD_DEFINES
#include <openssl/opensslconf.h>
#ifndef OPENSSL_THREADS
#error OpenSSL not compiled with thread support!
#endif
#include <pthread.h>

#include <openssl/err.h>

struct DORUNTHREAD {
    void (*threadFunc)(void *va);
    void *threadArg;
    char *threadName;
};

#ifdef WIN32
void UUDoRunThread(void *v)
#else
void *UUDoRunThread(void *v)
#endif
{
    struct DORUNTHREAD *drt = (struct DORUNTHREAD *)v;
    /* disassociate the v value from drt to avoid any confusion */
    v = NULL;

    /* drt->threadArg and drt->threadFunc belongs to this thread's creator, not this thread. */
    /* drt->threadName and drt belong to this thread. */

    if (optionLogThreadStartup)
        Log("Thread running: %s.", drt->threadName);

    (drt->threadFunc)(drt->threadArg);

    if (optionLogThreadStartup)
        Log("Thread exiting: %s.", drt->threadName);

    FreeThenNull(drt->threadName); /* drt->threadName belongs to this thread. */
    FreeThenNull(drt);
    /* Remove thread-specific data used by SSL to avoid memory leak */
    if (UUSslInitialized())
        ERR_remove_state(0);

#ifndef WIN32
    return NULL;
#endif
}

int UUBeginThread(void (*threadFunc)(void *va), void *threadArg, char *threadFuncName) {
    struct DORUNTHREAD *drt;
    char *name = NULL;
    int failed;
#ifdef WIN32
    unsigned long tstatus;
#else
    int pstatus;
    pthread_t threadId;
    pthread_attr_t pthreadAttrs;
    size_t stackSizeDefault = 0;
#endif

    if (!(drt = (struct DORUNTHREAD *)calloc(1, sizeof(*drt))))
        PANIC;
    if (!(name = strdup(threadFuncName)))
        PANIC; /* a thread-safe copy of the name */

    /* drt will belong to the new thread. */
    drt->threadFunc = threadFunc; /* drt->threadFunc belongs to the thread's creator, not the
                                     thread.  The owner is responsible for it's existence. */
    drt->threadArg =
        threadArg;          /* drt->threadArg belongs to the thread's creator, not the thread. */
    drt->threadName = name; /* drt->threadName belongs to the new thread. */

    failed = 0;

    if (optionLogThreadStartup)
        Log("Thread starting: %s.", threadFuncName);

#ifdef WIN32
    tstatus = _beginthread(UUDoRunThread, 0, drt);

    if (tstatus == 1L) {
        Log("_beginthread failed with %d for %s (errno %d)", errno, threadFuncName, errno);
        failed = 1;
    }
#else
    if (pthread_attr_init(&pthreadAttrs) != 0)
        PANIC;
    if (pthread_attr_setdetachstate(&pthreadAttrs, PTHREAD_CREATE_DETACHED) != 0)
        PANIC;
    pstatus = pthread_create(&threadId, &pthreadAttrs, UUDoRunThread, (void *)drt);
    if (optionLogThreadStartup)
        Log("TS p");
    if (pstatus != 0) {
        Log("pthread_create failed %d for %s", pstatus, threadFuncName);
        if (pstatus == ENOMEM) {
            Log("Insufficient memory to create thread stack: %s.", threadFuncName);
            if (pthread_attr_getstacksize(&pthreadAttrs, &stackSizeDefault) != 0) {
                Log("The system default stack size cannot be determined.");
            } else {
                Log("The system default stack size is: %d.", stackSizeDefault);
            }
            Log("The system default stack size may be so high that EZproxy can't create the number "
                "of threads required.");
            Log("You may try 'ulimit -s 512' before running EZproxy.");
        }
        failed = 1;
    }
#endif

    /* This drt is normally freed by the thread, so only free it here if the
       thread failed to be created.
    */
    if (failed) {
        FreeThenNull(drt->threadName);
        FreeThenNull(drt);
    } else {
        drt = NULL;
    }

    return failed;
}
