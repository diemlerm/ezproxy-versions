#define __UUFILE__ "admincas.c"

#include "common.h"
#include "uuxml.h"
#include "variables.h"

#include "admincas.h"

struct CASSERVICEURL *casServiceUrls = NULL;

#define MAXCASTICKET 16

struct CASTICKET {
    struct CASTICKET *next;
    struct CASSERVICEURL *csu;
    char ticket[MAXCASTICKET];
    char key[MAXKEY];
    unsigned char md5Hash[16];
    time_t issued;
    BOOL anonymous;
};

struct CASTICKET *casTickets = NULL;

BOOL optionCasTestAttributes = FALSE;

#define CASTICKETLIFETIME 60

static void AdminCASServiceHash(char *service, unsigned char *hash) {
    memset(hash, 0, 16);

    // Creating a hash unique to the service goes beyond the CAS protocol specification
    // and caused problems for the SirsiDynix Enterprise client. The original logic is
    // retained as reference in case similar functionality is required in a future revision
    // of the spec.

    /*
    MD5_CTX md5Context;



    if (service == NULL || *service == 0) {
        memset(hash, 0, 16);
    } else {
        MD5_Init(&md5Context);
        MD5_Update(&md5Context, (unsigned char *)service, (unsigned int)strlen(service));
        MD5_Final(hash, &md5Context);
    }
    */
}

static struct SESSION *AdminCASFindTicket(char *service,
                                          char *ticket,
                                          BOOL *invalidService,
                                          struct CASSERVICEURL **csu) {
    struct CASTICKET *p, *last;
    struct SESSION *e = NULL;
    time_t now;
    unsigned char checkHash[16];
    char *q;

    if (csu) {
        *csu = NULL;
    }

    if (invalidService)
        *invalidService = 0;

    q = strchr(ticket, '-');
    if (q == NULL) {
        q = ticket;
    } else {
        q++; /* skip over the dash. */
    }

    UUAcquireMutex(&mCasTickets);
    for (p = casTickets, last = NULL; p; last = p, p = p->next) {
        if (strcmp(p->ticket, q) == 0)
            break;
    }
    if (p) {
        UUtime(&now);
        if (p->issued + CASTICKETLIFETIME > now) {
            AdminCASServiceHash(service, checkHash);
            if (memcmp(p->md5Hash, checkHash, sizeof(checkHash)) != 0) {
                if (invalidService)
                    *invalidService = 1;
            } else {
                e = FindSessionByKey(p->key);
                if (csu) {
                    *csu = p->csu;
                }
            }
        }
        if (last)
            last->next = p->next;
        else
            casTickets = p->next;
        FreeThenNull(p);
    }
    UUReleaseMutex(&mCasTickets);

    return e;
}

static struct CASTICKET *AdminCASCreateTicket(struct SESSION *e,
                                              struct CASSERVICEURL *csu,
                                              char *service) {
    struct CASTICKET *n, *p, *last, *next;
    char ticket[MAXCASTICKET];
    char *q;
    int i;
    time_t now, when;

    if (e == NULL)
        return NULL;

    UUtime(&now);
    when = now - CASTICKETLIFETIME;

    for (;;) {
        for (q = ticket, i = MAXCASTICKET - 1; i > 0; i--) {
            *q++ = RandChar();
        }
        *q = 0;

        UUAcquireMutex(&mCasTickets);
        for (p = casTickets, last = NULL; p; p = next) {
            next = p->next;
            if (p->issued > when) {
                if (strcmp(p->ticket, ticket) == 0)
                    break;
                last = p;
                continue;
            }
            /* expire old tickets */
            if (last)
                last->next = next;
            else
                casTickets = next;
            FreeThenNull(p);
        }
        /* If the ticket value we picked is already active, try again */
        if (p) {
            UUReleaseMutex(&mCasTickets);
            continue;
        }

        if (!(n = calloc(1, sizeof(*n))))
            PANIC;
        StrCpy3(n->ticket, ticket, sizeof(n->ticket));
        StrCpy3(n->key, e->key, sizeof(n->key));
        n->issued = now;
        n->csu = csu;
        n->next = casTickets;
        casTickets = n;
        UUReleaseMutex(&mCasTickets);
        AdminCASServiceHash(service, n->md5Hash);
        return n;
    }
}

/**
 * Check to see if the service URL provided by the client is authorized.
 * If the service URL is not authorized, feedback is sent to the client and
 * no further processing should be performed.
 *
 * @param t Current request translate.
 * @param service service URL provided in request.
 * @return CASSERVICEURL that matches the service URL or NULL if no match.
 */
static struct CASSERVICEURL *AdminCASValidateServiceURL(struct TRANSLATE *t, char *service) {
    struct UUSOCKET *s = &t->ssocket;
    struct CASSERVICEURL *csu;

    if (service == NULL || *service == 0) {
        HTMLErrorPage(t, "\"service\" parameter missing from URL", NULL);
        return NULL;
    }

    for (csu = casServiceUrls; csu; csu = csu->next) {
        if (WildCompare(service, csu->url))
            break;
    }

    if (csu == NULL) {
        if (FileExists(BADCASHTM)) {
            HTMLHeader(t, htmlHeaderOmitCache);
            SendEditedFile(t, NULL, BADCASHTM, 1, service, 1, NULL);
        } else {
            HTMLErrorPageHeader(t, "Unauthorized Service URL");
            UUSendZ(s, "<p>The " EZPROXYNAMEPROPER
                       " CAS server is not configured to respond to this service URL.</p>");
            HTMLErrorPageFooter(t);
        }
        return NULL;
    }

    return csu;
}

void AdminCASLogin(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    struct SESSION *e = t->session;
    char *q;
    char *renewQuery;
    struct CASTICKET *ct;
    const int ACSERVICE = 0;
    const int ACTICKET = 1;
    const int ACGATEWAY = 2;
    const int ACRENEW = 3;
    struct FORMFIELD ffs[] = {
        {"service", NULL, 0, 0, 0},
        {"ticket",  NULL, 0, 0, 0},
        {"gateway", NULL, 0, 0, 0},
        {"renew",   NULL, 0, 0, 4},
        {NULL,      NULL, 0, 0, 0}
    };
    struct CASSERVICEURL *csu;
    BOOL renew = FALSE;
    UNUSED(ACTICKET);

    if ((usageLogUser & ULUCAS) == 0) {
        HTTPCode(t, 404, 1);
        return;
    }

    FindFormFields(query, post, ffs);

    renew = ffs[ACRENEW].value && *ffs[ACRENEW].value;
    csu = AdminCASValidateServiceURL(t, ffs[ACSERVICE].value);
    if (csu == NULL) {
        return;
    }

    if (e == NULL || renew || !GroupMaskOverlap(e->gm, csu->gm) ||
        (csu->anonymous == 0 && (e->logUserBrief == NULL || e->autoLoginBy != autoLoginByNone))) {
        /* If gateway was specified and we have no session, the protocol says we should just
           return to the service URL without a ticket
        */
        if (ffs[ACGATEWAY].value != NULL) {
            SimpleRedirect(t, ffs[ACSERVICE].value, 302);
            return;
        }

        if (e == NULL) {
            StrCpy3(t->url, t->urlCopy, sizeof(t->url));
            renewQuery = strchr(t->url, '?');
            GetSnipField(renewQuery, "renew", TRUE, FALSE);
            AdminRelogin(t);
            return;
        }

        if (e && renew) {
            e->reloginRequired = TRUE;
            StrCpy3(t->url, t->urlCopy, sizeof(t->url));
            renewQuery = strchr(t->url, '?');
            GetSnipField(renewQuery, "renew", TRUE, FALSE);
            AdminRelogin(t);
            return;
        }

        AdminLogup(t, TRUE, NULL, NULL, csu->gm, FALSE);
        return;
    }

    ct = AdminCASCreateTicket(e, csu, ffs[ACSERVICE].value);
    if (ct == NULL) {
        HTTPCode(t, 500, 1);
        return;
    }

    ct->anonymous = csu->anonymous;

    strcpy(t->buffer, ffs[ACSERVICE].value);

    if (!(q = strchr(t->buffer, '?')))
        strcat(t->buffer, "?");
    else {
        if (*(q + 1))
            strcat(t->buffer, "&");
    }

    strcat(t->buffer, "ticket=ST-");
    AddEncodedField(t->buffer, ct->ticket, NULL);

    SimpleRedirect(t, t->buffer, 302);
}

void AdminCASLogout(struct TRANSLATE *t, char *query, char *post) {
    const int ACSERVICE = 0;
    struct FORMFIELD ffs[] = {
        {"service", NULL, 0, 0, 0},
        {NULL,      NULL, 0, 0, 0}
    };
    struct CASSERVICEURL *csu;

    if ((usageLogUser & ULUCAS) == 0) {
        HTTPCode(t, 404, 1);
        return;
    }

    FindFormFields(query, post, ffs);

    csu = AdminCASValidateServiceURL(t, ffs[ACSERVICE].value);
    if (csu == NULL) {
        return;
    }

    StopSession(t->session, "/cas/logout");
    SimpleRedirect(t, ffs[ACSERVICE].value, 302);
}

static char *AdminCASUsername(struct CASSERVICEURL *csu, struct SESSION *e) {
    char *user;

    if (!csu->anonymous) {
        user = VariablesGetValueByGroupMask(e->sessionVariables, SESSIONVARPREFIX "cas:user",
                                            csu->scope);
        if (user == NULL && e->logUserBrief) {
            if (!(user = strdup(e->logUserBrief)))
                PANIC;
        }
    }

    if (user == NULL || *user == 0) {
        if (!(user = strdup("anonymous")))
            PANIC;
    }

    return user;
}

static void AdminCASAddAttribute(struct TRANSLATE *t,
                                 struct VARIABLES *v,
                                 void *context,
                                 const char *name,
                                 const char *value,
                                 int count) {
    xmlTextWriterPtr writer = (xmlTextWriterPtr)context;
    char *attribute;

    if (name != NULL && value != NULL) {
        attribute = StartsWith(name, SESSIONVARPREFIX);
        if (attribute != NULL && StartsWith(attribute, "cas:") &&
            strcmp(attribute, "cas:user") != 0) {
            if (xmlTextWriterWriteElement(writer, BAD_CAST attribute, BAD_CAST value) < 0)
                PANIC;
        }
    }
}

static void AdminCASServiceValidate23(struct TRANSLATE *t, char *query, char *post, int version) {
    struct UUSOCKET *s = &t->ssocket;
    xmlTextWriterPtr writer = NULL;
    xmlBufferPtr buf = NULL;
    BOOL invalidService;
    const int ACSERVICE = 0;
    const int ACTICKET = 1;
    struct FORMFIELD ffs[] = {
        {"service", NULL, 0, 0, 0},
        {"ticket",  NULL, 0, 0, 0},
        {NULL,      NULL, 0, 0, 0}
    };
    struct SESSION *e;
    struct CASSERVICEURL *csu;

    if ((usageLogUser & ULUCAS) == 0) {
        HTTPCode(t, 404, 1);
        return;
    }

    FindFormFields(query, post, ffs);
    e = AdminCASFindTicket(ffs[ACSERVICE].value, ffs[ACTICKET].value, &invalidService, &csu);

    if (!(buf = xmlBufferCreate()))
        PANIC;
    if (!(writer = xmlNewTextWriterMemory(buf, 0)))
        PANIC;
    if (xmlTextWriterStartDocument(writer, NULL, "UTF-8", NULL) < 0)
        PANIC;

    /* Start root element */
    if (xmlTextWriterStartElementNS(writer, BAD_CAST "cas", BAD_CAST "serviceResponse",
                                    BAD_CAST "http://www.yale.edu/tp/cas") < 0)
        PANIC;

    /* Ths was logic to test for attributes from another name space */
    /* if (xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:vcu", BAD_CAST
     * "http://login.vcu.edu/cas") < 0) PANIC; */

    if (e == NULL) {
        if (xmlTextWriterStartElement(writer, BAD_CAST "cas:authenticationFailure") < 0)
            PANIC;
        if (ffs[ACTICKET].value == NULL || *ffs[ACTICKET].value == 0) {
            if (xmlTextWriterWriteAttribute(writer, BAD_CAST "code", BAD_CAST "INVALID_REQUEST") <
                0)
                PANIC;
            if (xmlTextWriterWriteString(writer, BAD_CAST "Ticket missing") < 0)
                PANIC;
        } else {
            if (invalidService) {
                if (xmlTextWriterWriteAttribute(writer, BAD_CAST "code",
                                                BAD_CAST "INVALID_SERVICE") < 0)
                    PANIC;
                if (xmlTextWriterWriteFormatString(
                        writer, "Ticket %s was valid but did not match specified service",
                        ffs[ACTICKET].value) < 0)
                    PANIC;
            } else {
                if (xmlTextWriterWriteAttribute(writer, BAD_CAST "code",
                                                BAD_CAST "INVALID_TICKET") < 0)
                    PANIC;
                if (xmlTextWriterWriteFormatString(writer, "Ticket %s not recognized",
                                                   ffs[ACTICKET].value) < 0)
                    PANIC;
            }
        }

        /* Close cas:authenticationFailure */
        if (xmlTextWriterEndElement(writer) < 0)
            PANIC;
    } else {
        char *user = AdminCASUsername(csu, e);

        if (xmlTextWriterStartElement(writer, BAD_CAST "cas:authenticationSuccess") < 0)
            PANIC;
        if (xmlTextWriterWriteElement(writer, BAD_CAST "cas:user", BAD_CAST user) < 0)
            PANIC;
        FreeThenNull(user);

        if (version == 3) {
            struct tm tm;
            char authenticationDate[32];

            if (xmlTextWriterStartElement(writer, BAD_CAST "cas:attributes") < 0)
                PANIC;

            UUgmtime_r(&e->created, &tm);
            strftime(authenticationDate, sizeof(authenticationDate), "%Y-%m-%dT%H:%M:%SZ", &tm);
            if (xmlTextWriterWriteElement(writer, BAD_CAST "cas:authenticationDate",
                                          BAD_CAST authenticationDate) < 0)
                PANIC;

            VariablesForAllByGroupMask(t, e->sessionVariables, writer, csu->scope,
                                       AdminCASAddAttribute);

            /* Close cas:attributes */
            if (xmlTextWriterEndElement(writer) < 0)
                PANIC;
        }

        /* Close cas:authenticationSuccess */
        if (xmlTextWriterEndElement(writer) < 0)
            PANIC;
    }

    if (optionCasTestAttributes) {
        if (xmlTextWriterStartElementNS(writer, BAD_CAST "oclc", BAD_CAST "attributes",
                                        BAD_CAST EZPROXYURLROOT "castestattributes") < 0)
            PANIC;
        if (xmlTextWriterWriteElement(writer, BAD_CAST "oclc:groupMembership", BAD_CAST "test1") <
            0)
            PANIC;
        if (xmlTextWriterWriteElement(writer, BAD_CAST "oclc:groupMembership", BAD_CAST "test2") <
            0)
            PANIC;
        /* Close oclc:attributes */
        if (xmlTextWriterEndElement(writer) < 0)
            PANIC;
    }

    /* close cas:serviceResponse */
    if (xmlTextWriterEndElement(writer) < 0)
        PANIC;

    /* How to add a formatted element value
    rc = xmlTextWriterWriteFormatElement(writer, BAD_CAST "ENTRY_NO", "%d", 20);
    */

    if (xmlTextWriterEndDocument(writer) < 0)
        PANIC;

    if (xmlTextWriterFlush(writer) < 0)
        PANIC;

    HTTPCode(t, 200, 0);
    NoCacheHeaders(s);
    HTTPContentType(t, "text/xml", 1);

    UUSendZCRLF(s, (char *)buf->content);

    if (writer) {
        xmlFreeTextWriter(writer);
        writer = NULL;
    }

    if (buf) {
        xmlBufferFree(buf);
        buf = NULL;
    }
}

void AdminCASServiceValidate(struct TRANSLATE *t, char *query, char *post) {
    AdminCASServiceValidate23(t, query, post, 2);
}

void AdminCASP3ServiceValidate(struct TRANSLATE *t, char *query, char *post) {
    AdminCASServiceValidate23(t, query, post, 3);
}

void AdminCASValidate(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    const int ACSERVICE = 0;
    const int ACTICKET = 1;
    struct FORMFIELD ffs[] = {
        {"service", NULL, 0, 0, 0},
        {"ticket",  NULL, 0, 0, 0},
        {NULL,      NULL, 0, 0, 0}
    };
    struct SESSION *e;
    struct CASSERVICEURL *csu;

    if ((usageLogUser & ULUCAS) == 0) {
        HTTPCode(t, 404, 1);
        return;
    }

    FindFormFields(query, post, ffs);
    e = AdminCASFindTicket(ffs[ACSERVICE].value, ffs[ACTICKET].value, NULL, &csu);

    HTTPCode(t, 200, 0);
    NoCacheHeaders(s);
    HTTPContentType(t, "text/plain", 1);

    if (e) {
        char *user = AdminCASUsername(csu, e);
        UUSendZ(s, "yes\r\n");
        UUSendZCRLF(s, user);
        FreeThenNull(user);
    } else {
        UUSendZ(s, "no\r\n");
    }
}

static BOOL AdminCASAdminSendGroups(struct UUSOCKET *s, GROUPMASK gm) {
    struct GROUP *g;
    BOOL any = FALSE;

    WriteLine(s, "<td>");

    for (g = groups; g; g = g->next) {
        if (GroupMaskOverlap(gm, g->gm)) {
            if (any) {
                WriteLine(s, "<br>\n");
            } else {
                any = TRUE;
            }
            SendHTMLEncoded(s, g->name);
        }
    }

    if (!any) {
        WriteLine(s, "&nbsp;");
    }

    WriteLine(s, "</td>\n");

    return any;
}
void AdminCASAdmin(struct TRANSLATE *t, char *query, char *post) {
    struct CASSERVICEURL *csu;
    struct UUSOCKET *s = &t->ssocket;

    AdminHeader(t, 0, "Manage CAS");

    WriteLine(s, "<table class='bordered-table'>\n");
    WriteLine(s,
              "<thead><tr><th scope='col'>Service URL</th><th scope='col'>Authorized "
              "Groups</th><th scope='col'>Attribute Scope</th></tr></thead>\n");
    WriteLine(s, "<tbody>\n");
    for (csu = casServiceUrls; csu; csu = csu->next) {
        WriteLine(s, "<tr><td>");
        SendHTMLEncoded(s, csu->url);
        WriteLine(s, "</td>");
        AdminCASAdminSendGroups(s, csu->gm);
        AdminCASAdminSendGroups(s, csu->scope);
        WriteLine(s, "</tr>\n");
    }
    WriteLine(s, "</tbody>\n</table>\n");

    AdminFooter(t, 0);
}
