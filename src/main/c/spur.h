#ifndef __SPUR_H__
#define __SPUR_H__

#include "common.h"
#include "environment.h"

#define SPURDIR ENVIRONMENTDIR
#define SPURDB SPURDIR DIRSEP "spur_v1.db"

void SpurInit();
void SpurStart();
void SpurInsert(struct TRANSLATE *t, const char *refererUrl, const char *targetUrl);
char *SpurGetRefererUrl(const char *tag);

#endif  // SPUR_H
