/**
 * HTTP proxy user authentication
 *
 * This user authentication method was created for a university in
 * Australia.  It was never received public documentation, and should
 * not.  This method allows authentication against an HTTP proxy server
 * using a test URL.  If the proxy authorizes the URL, then EZproxy not
 * only authenticates the user, but also starts sending all outbound
 * requests through the proxy server under the same credentials.
 *
 * This authentication method DOES NOT SUPPORT common conditions and actions.
 */

#define __UUFILE__ "usrproxy.c"

#include "common.h"
#include "uustring.h"

#include "usr.h"
#include "usrproxy.h"

/* Returns 0 if valid, 1 if not, 3 if unable to connect to pop host */
int FindUserProxy(char *proxy,
                  struct USERINFO *uip,
                  char *user,
                  char *pass,
                  struct sockaddr_storage *pInterface) {
    static BOOL warned = 0;
    int result = 1;
    struct UUSOCKET rr, *r = &rr;
    char *tempUnencoded = NULL, *tempEncoded = NULL;
    char line[256];
    BOOL first;
    int status;
    BOOL mpa = 0;

    UUInitSocket(r, INVALID_SOCKET);

    if (firstProxyHost == NULL || firstProxyPort == 0) {
        if (warned == 0) {
            Log("Proxy option in " EZPROXYUSR " requires Proxy statement in " EZPROXYCFG);
            warned = 1;
        }
        goto Finished;
    }

    if (strchr(user, ':') != NULL)
        goto Finished;

    if (!(tempUnencoded = malloc(strlen(user) + strlen(pass) + 2)))
        PANIC;
    sprintf(tempUnencoded, "%s:%s", user, pass);
    tempEncoded = Encode64(NULL, tempUnencoded);
    FreeThenNull(tempUnencoded);

    if (strlen(tempEncoded) + 1 > sizeof(uip->sessionProxyAuth))
        goto Finished;

    if (UUConnectWithSource2(r, firstProxyHost, firstProxyPort, "FindUserProxy", pInterface, 0))
        goto Finished;

    mpa = 0;
    if (strnicmp(proxy, "mpa;", 4) == 0) {
        proxy += 4;
        mpa = 1;
    }

    UUSendZ(r, "HEAD ");
    UUSendZ(r, proxy);
    UUSendZ(r, " HTTP/1.0\r\n");

    UUSendZ(r, "Proxy-Authorization: basic ");
    UUSendZCRLF(r, tempEncoded);

    UUSendCRLF(r);

    first = 1;
    while (ReadLine(r, line, sizeof(line))) {
        if (first) {
            first = 0;
            if (strnicmp(line, "HTTP/", 5) == 0) {
                status = atoi(SkipNonWS(SkipWS(line)));
                if (status >= 200 && status < 300) {
                    result = 0;
                    if (mpa)
                        StrCpy3(uip->sessionProxyAuth, tempEncoded, sizeof(uip->sessionProxyAuth));
                }
            }
        }
    }

    UUStopSocket(r, 0);

Finished:
    FreeThenNull(tempUnencoded);
    FreeThenNull(tempEncoded);

    return result;
}
