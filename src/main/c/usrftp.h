#ifndef __USRFTP_H__
#define __USRFTP_H__

#include "common.h"

/* Returns 0 if valid, 1 if not, 3 if unable to connect to ftp host */
int FindUserFtp(char *ftpHost,
                char *user,
                char *pass,
                struct sockaddr_storage *pInterface,
                char useSsl,
                BOOL debug);

#endif  // __USRFTP_H__
