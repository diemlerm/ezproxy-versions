#ifndef __USRNCIP_H__
#define __USRNCIP_H__

#include "common.h"

enum FINDUSERRESULT FindUserNCIP(struct TRANSLATE *t,
                                 struct USERINFO *uip,
                                 char *file,
                                 int f,
                                 struct FILEREADLINEBUFFER *frb,
                                 struct sockaddr_storage *pInterface);

#endif  // __USRNCIP_H__
