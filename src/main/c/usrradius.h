#ifndef __USRRADIUS_H__
#define __USRRADIUS_H__

#include "common.h"

/* Returns 0 if valid, 1 if not, 3 if unable to connect to pop host */
int FindUserRadius(char *radiusHost,
                   char *user,
                   char *pass,
                   char *secret,
                   char *realm,
                   struct sockaddr_storage *pInterface,
                   BOOL debug);

#endif  // __USRRADIUS_H__
