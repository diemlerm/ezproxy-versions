#ifndef __USRODBC_H__
#define __USRODBC_H__

#include "common.h"

enum FINDUSERRESULT FindUserODBC(struct TRANSLATE *t,
                                 struct USERINFO *uip,
                                 char *file,
                                 int f,
                                 struct FILEREADLINEBUFFER *frb,
                                 struct sockaddr_storage *pInterface);

#endif  // __USRODBC_H__
