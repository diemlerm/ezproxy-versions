#ifndef __ADMINACCOUNTRESET_H__
#define __ADMINACCOUNTRESET_H__

#include "common.h"

void AdminAccountReset(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINACCOUNTRESET_H__
