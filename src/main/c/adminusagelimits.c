/**
 * /usagelimits admin URL to view and manage user usage limits
 */

#define __UUFILE__ "adminusagelimits.c"

#include "common.h"
#include "uustring.h"

#include "adminusagelimits.h"
#include "adminaudit.h"

void AdminUsageLimits(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    struct USAGELIMIT *ul;
    struct USAGELIMITEXCEEDED *ule;
    struct USAGE *u, *uNext;
    time_t expiresDate;
    char ta[MAXASCIIDATE];
    char mb[MAXASCIIKMB];
    BOOL any;
    BOOL anyToClear = 0;
    char *key, *arg1, *next, *arg2, *arg3;
    BOOL cleared;
    unsigned int minutes;
    int tableIdx = 0;
    int trs = 0;

    AdminHeader(t, AHSORTABLE, "View Usage Limits and Clear Suspensions");

    if (usageLimits == NULL ||
        (usageLimits == intruderUsageLimit && intruderUsageLimit->next == NULL)) {
        UUSendZ(s,
                "<p>No usage limits are defined.  See <a "
                "href='http://www.oclc.org/support/documentation/ezproxy/cfg/usagelimit//"
                "'>www.oclc.org/us/en/support/documentation/ezproxy/cfg/usagelimit/</a> for "
                "configuration information.</p>\n");
        AdminFooter(t, 0);
        return;
    }

    UUSendZ(s, "<form action='/usagelimits' method='post'>\n");

    UUSendZ(s, "<p>\n");
    UUSendZ(s, "<a href='/usagelimits?clearall=on'>Clear all suspensions</a><br />\n");
    if (auditEventsMask & AUDITUSAGELIMITMASK) {
        WriteLine(s,
                  "View suspensions recorded <a href='/audit?date=today&event=%s'>for today</a> | "
                  "<a href='/audit?date=-1&event=%s'>since yesterday</a> | <a "
                  "href='/audit?date=all&event=%s'>all</a><br />\n",
                  AuditEventNameSelect(AUDITUSAGELIMIT), AuditEventNameSelect(AUDITUSAGELIMIT),
                  AuditEventNameSelect(AUDITUSAGELIMIT));
    } else {
        UUSendZ(s,
                "View suspensions recorded in " EZPROXYMSG
                " <a href='/messages?from=ul&find=+usage+limit+&since=today'>for today</a> | <a "
                "href='/messages?from=ul&find=+usage+limit+&since=yesterday'>since yesterday</a> | "
                "<a href='/messages?from=ul&find=+usage+limit+'>all</a><br />\n");
    }
    UUSendZ(s, "</p>\n");

    any = 0;
    for (next = (post ? post : query); NextKEV(next, &key, &arg1, &next, 0);) {
        if (arg1 == NULL)
            continue;
        if (strcmp(key, "clearall") == 0) {
            UUAcquireMutex(&mUsage);
            for (ul = usageLimits; ul; ul = ul->next) {
                if (ul == intruderUsageLimit)
                    continue;
                for (u = ul->usage; u; u = uNext) {
                    uNext = u->next;
                    if (u->when > 0 && (ule = FindUsageLimitExceeded(u->user, 0, 1))) {
                        ClearUsageLimitExceeded(ule, ul, u, "administrator (all)");
                    }
                }
            }
            UUReleaseMutex(&mUsage);
            UUSendZ(s, "<h2>All suspensions cleared</h2>\n");
        }
        if (strcmp(key, "clear") == 0) {
            arg2 = NextWSArg(arg1);
            arg3 = NextWSArg(arg2);
            cleared = 0;
            UUAcquireMutex(&mUsage);
            if ((ule = FindUsageLimitExceeded(arg1, 0, 1)) && (ul = FindUsageLimit(arg3, 0)) &&
                ul != intruderUsageLimit && (FindUsage(&ul->usage, arg1, 0, 1, &u, NULL), u) &&
                u->when == atoi(arg2)) {
                ClearUsageLimitExceeded(ule, ul, u, "administrator");
                cleared = 1;
            }
            UUReleaseMutex(&mUsage);
            if (cleared) {
                if (any == 0) {
                    UUSendZ(s, "<h2>Suspensions cleared</h2>\n<p>\n");
                    any = 1;
                }
                SendHTMLEncoded(s, arg3);
                UUSendZ(s, " ");
                SendHTMLEncoded(s, arg1);
                UUSendZ(s, "<br>\n");
            }
        }
    }

    if (any) {
        UUSendZ(s, "</p>\n");
        NeedSaveUsage();
    }

    for (ul = usageLimits; ul; ul = ul->next) {
        if (ul == intruderUsageLimit)
            continue;

        UUAcquireMutex(&mUsage);
        PruneUsage(&ul->usage, ul->oldestInterval);
        UUReleaseMutex(&mUsage);

        UUSendZ(s, "<h2>Usage limit ");
        SendHTMLEncoded(s, ul->name);
        UUSendZ(s, "</h2>\n<p>\n");

        WriteLine(s, "Limit action: %s<br>\n", (ul->enforce ? "enforce" : "monitor"));

        minutes = ul->interval / 60;
        WriteLine(s, "Data collection interval: %d minute%s<br>\n", minutes, SIfNotOne(minutes));

        UUSendZ(s, "Transfer limit: ");
        if (ul->maxTransfers == 0)
            UUSendZ(s, "(unlimited)");
        else
            WriteLine(s, "%d", ul->maxTransfers);
        UUSendZ(s, "<br>\n");

        WriteLine(s, "Megabyte limit: %s<br>\n",
                  (ul->maxRecvKBytes ? ASCIIKMB(ul->maxRecvKBytes, -1, mb) : "(unlimited)"));

        if (ul->expires) {
            minutes = ul->expires / 60;
            WriteLine(s, "Suspensions: expire after %d minute%s %s elapsed<br>\n", minutes,
                      SIfNotOne(minutes), HasHave(minutes, 0));
        } else {
            UUSendZ(s, "Suspensions: must be manually cleared<br>\n");
        }

        if (ul->ignoreNormalLogin)
            UUSendZ(s, "Ignore user who are logged in by normal login<br>\n");

        if (ul->ignoreAutoLoginIP)
            UUSendZ(s, "Ignore users who are logged in by AutoLoginIP<br>\n");

        if (ul->ignoreRefererLogin)
            UUSendZ(s, "Ignore users who are logged in by referring URL<br>\n");

        UUSendZ(s, "</p>\n");

        any = 0;
        for (u = ul->usage; u; u = u->next) {
            /* If suspended, check suspension, and if it clears up, skip this to next user */
            if (u->when && (ule = FindUsageLimitExceeded(u->user, 0, 0)) &&
                (UsageLimitExceeded(NULL, ule, ULEVOLUME), u->when == 0))
                continue;

            if (any == 0) {
                WriteLine(s, "<table class='bordered-table sortable' id='ul%d'>\n", tableIdx++);
                UUSendZ(s,
                        "<tr><th scope='col'>Clear</th><th scope='col'>User</th><th scope='col' "
                        "class='sortnumeric'>Transfers</th><th scope='col' "
                        "class='sortnumeric'>Megabytes</th><th>Suspended</th>");
                if (ul->expires)
                    UUSendZ(s, "<th scope='col'>Expires</th>");
                UUSendZ(s, "</tr>\n");
                any = 1;
            }

            WriteLine(s, "<tr id='trs%d'><td>", trs++);

            if (u->when) {
                WriteLine(s, "<input type='checkbox' name='clear' id='clear-%d' value='", trs);
                SendQuotedURL(t, u->user);
                WriteLine(s, " %d ", u->when);
                SendQuotedURL(t, ul->name);
                UUSendZ(s, "'>");
                anyToClear = 1;
            } else {
                UUSendZ(s, "&nbsp;");
            }

            WriteLine(s, "</td><td><label for='clear-%d'>", trs);
            SendHTMLEncoded(s, u->user);

            WriteLine(s, "</label></td><td>%d</td><td>%s</td><td>%s</td>", u->transfers,
                      ASCIIKMB(u->recvKBytes, 2, mb),
                      (u->when ? ASCIIDate(&u->when, ta) : "&nbsp;"));
            if (ul->expires) {
                expiresDate = (u->when && ul->expires) ? u->when + ul->expires : 0;
                WriteLine(s, "<td>%s</td>", (expiresDate ? ASCIIDate(&expiresDate, ta) : "&nbsp"));
            }
            UUSendZ(s, "</tr>\n");
        }
        if (any)
            UUSendZ(s, "</table>\n");
        else
            UUSendZ(s, "<p>No current activity</p>\n");
    }

    if (anyToClear)
        UUSendZ(s, "<p><input type='submit' value='Clear suspensions from checked users'></p>");

    UUSendZ(s, "</form>\n");

    AdminFooter(t, 0);
}

void AdminUsageLimitExceeded(struct TRANSLATE *t, char *query, char *post) {
    HTMLHeader(t, htmlHeaderNoCache);
    SendFile(t, SUSPENDHTM, SFREPORTIFMISSING);
}
