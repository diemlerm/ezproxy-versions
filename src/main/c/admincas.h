#ifndef __ADMINCAS_H__
#define __ADMINCAS_H__

#include "common.h"

struct CASSERVICEURL {
    struct CASSERVICEURL *next;
    char *url;
    BOOL anonymous;
    GROUPMASK gm;
    GROUPMASK scope;
};

extern struct CASSERVICEURL *casServiceUrls;
extern BOOL optionCasTestAttributes;

void AdminCASAdmin(struct TRANSLATE *t, char *query, char *post);
void AdminCASLogin(struct TRANSLATE *t, char *query, char *post);
void AdminCASLogout(struct TRANSLATE *t, char *query, char *post);
void AdminCASServiceValidate(struct TRANSLATE *t, char *query, char *post);
void AdminCASP3ServiceValidate(struct TRANSLATE *t, char *query, char *post);
void AdminCASValidate(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINCAS_H__
