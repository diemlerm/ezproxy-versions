#ifndef __ADMINADMIN_H__
#define __ADMINADMIN_H__

#include "common.h"

void AdminAdmin(struct TRANSLATE *t, char *query, char *post);
BOOL AdminLicenseInvalid(struct TRANSLATE *t);

#endif  // __ADMINADMIN_H__
