/**
 * /token admin URL to cross reference tokens to usernames and usernames to tokens.
 */

#define __UUFILE__ "admintoken.c"

#include "common.h"
#include "uustring.h"
#include "usertoken.h"

#include "admintoken.h"
void AdminTokenKeyField(struct TRANSLATE *t,
                        int type,
                        struct DATABASE *b,
                        BOOL hidden,
                        BOOL first,
                        int lastKey,
                        char *ppid) {
    struct UUSOCKET *s = &t->ssocket;
    char *label = NULL;

    if (type < 0)
        return;

    if (first)
        UUSendZ(s, "<p>Choose the database that generated this token:</p>\n<p>\n");

    UUSendZ(s, "<div>");

    WriteLine(s, "<input name='key' type='%s' %s value='%d/", hidden ? "hidden" : "radio",
              lastKey == type ? "checked " : "", type);
    if (b)
        WriteLine(s, "%d' id='%d/%d", b - databases, type, b - databases);
    UUSendZ(s, "'>");

    if (!hidden) {
        if (b) {
            label = b->title;
        } else if (type == 1) {
            label = "ebrary";
            /*
            UUSendZ(s, "ebrary site ");
            SendHTMLEncoded(s, ebs->site);
            */
        } else if (type == 2) {
            label = "OverDrive";
            /*
            UUSendZ(s, "OverDrive site ");
            SendHTMLEncoded(s, ods->site);
            */
        } else if (type == 3) {
            label = "NetLibrary";
        } else if (type == 4) {
            label = "Films Media Group";
        } else if (type == 5) {
            label = "Gartner";
        } else if (type == 6) {
            UUSendZ(s,
                    "<label for='ppid'>PPID relying party code </label><input name='ppid' "
                    "id='ppid' value='");
            if (ppid && *ppid) {
                SendHTMLEncoded(s, ppid);
            }
            UUSendZ(s, "'>");
        }
        if (label) {
            WriteLine(s, "<label for='%d/%d'>", type, b - databases);
            SendHTMLEncoded(s, label);
            UUSendZ(s, "</label>");
        }
    } else {
        if (type == 6) {
            UUSendZ(s,
                    "<label for='ppid'>PPID relying party code </label><input name='ppid' "
                    "id='ppid' value='");
            if (ppid && *ppid) {
                SendHTMLEncoded(s, ppid);
            }
            UUSendZ(s, "'>");
        }
    }

    UUSendZ(s, "</div>\n");
}

char *DecryptTokensTitle(void) {
    static char title[128] = {0};
    int count = 1;  // PPID always
    int remain = 0;

    if (title[0])
        return title;

    if (usageLogUser & ULUTOKENKEY)
        count++;
    if (usageLogUser & ULUEBRARY)
        count++;
    if (usageLogUser & ULUFMG)
        count++;
    if (usageLogUser & ULUGARTNER)
        count++;
    if (usageLogUser & ULUOVERDRIVE)
        count++;
    if (usageLogUser & ULUNETLIBRARY)
        count++;

    if (count == 0)
        return NULL;

    remain = count;

    strcpy(title, "Manage ");

    if (usageLogUser & ULUTOKENKEY) {
        strcat(title, "Books24x7");
        remain--;
        if (remain > 1)
            strcat(title, ", ");
        if (remain == 1)
            strcat(title, " and ");
    }

    if (usageLogUser & ULUEBRARY) {
        strcat(title, "ebrary");
        remain--;
        if (remain > 1)
            strcat(title, ", ");
        if (remain == 1)
            strcat(title, " and ");
    }

    if (usageLogUser & ULUFMG) {
        strcat(title, "Films Media Group");
        remain--;
        if (remain > 1)
            strcat(title, ", ");
        if (remain == 1)
            strcat(title, " and ");
    }

    if (usageLogUser & ULUGARTNER) {
        strcat(title, "Gartner");
        remain--;
        if (remain > 1)
            strcat(title, ", ");
        if (remain == 1)
            strcat(title, " and ");
    }

    if (usageLogUser & ULUNETLIBRARY) {
        strcat(title, "NetLibrary");
        remain--;
        if (remain > 1)
            strcat(title, ", ");
        if (remain == 1)
            strcat(title, " and ");
    }

    if (usageLogUser & ULUOVERDRIVE) {
        strcat(title, "OverDrive");
        remain--;
        if (remain > 1)
            strcat(title, ", ");
        if (remain == 1)
            strcat(title, " and ");
    }

    strcat(title, "PPID tokens");

    return title;
}

void AdminToken(struct TRANSLATE *t, char *query, char *post) {
    struct DATABASE *b, *firstToken = NULL;
    struct UUSOCKET *s = &t->ssocket;
    struct EBRARYSITE *ebs;
    struct OVERDRIVESITE *ods;
    char *value;
    char *key;
    int count = 0;
    unsigned char *tokenKey = NULL;
    char *resultValue = NULL;
    const int ATVALUE = 0;
    const int ATKEY = 1;
    const int ATPPID = 2;
    const int ATACTION = 3;
    struct FORMFIELD ffs[] = {
        {"value",  NULL, 0, 0, 64},
        {"key",    NULL, 0, 0, 64},
        {"ppid",   NULL, 0, 0, 64},
        {"action", NULL, 0, 0, 7 },
        {NULL,     NULL, 0, 0, 0 }
    };
    int i;
    char *tt = DecryptTokensTitle();
    char *tokenType = NULL;
    BOOL decrypt;
    int firstType = -1;
    int lastKey = -1;

    if (tt == NULL) {
        HTTPCode(t, 404, 1);
        return;
    }

    FindFormFields(query, post, ffs);

    value = ffs[ATVALUE].value;
    key = ffs[ATKEY].value;

    if (value) {
        Trim(value, TRIM_LEAD | TRIM_TRAIL);
    }

    AdminHeader(t, 0, tt);

    decrypt = ffs[ATACTION].value == NULL || stricmp(ffs[ATACTION].value, "encrypt") != 0;

    if (value && *value && key && *key) {
        if (strnicmp(key, "0/", 2) == 0) {
            lastKey = 0;
            i = atoi(key + 2);
            if (i >= 0 && i < cDatabases) {
                b = databases + i;
                tokenType = b->title;
                tokenKey = b->tokenKey;
                if (decrypt) {
                    resultValue = UserTokenDecrypt(value, tokenKey);
                } else {
                    resultValue = UserToken(t, b->tokenKey, value);
                }
            }
        } else if (strnicmp(key, "1/", 2) == 0) {
            lastKey = 1;
            tokenType = "ebrary";
            ebs = ebrarySites /* AdminEbrarySite(key + 2, 0) */;
            if (ebs) {
                tokenKey = (unsigned char *)"ebr";
                if (decrypt) {
                    resultValue = UserTokenDecrypt2(value, (char *)tokenKey);
                } else {
                    resultValue = UserToken2((char *)tokenKey, value, 0);
                }
            }
        } else if (strnicmp(key, "2/", 2) == 0) {
            lastKey = 2;
            tokenType = "OverDrive";
            ods = overDriveSites /* AdminOverDriveSite(key + 2) */;
            if (ods) {
                tokenKey = (unsigned char *)"ods";
                if (decrypt) {
                    resultValue = UserTokenDecrypt2(value, (char *)tokenKey);
                } else {
                    resultValue = UserToken2((char *)tokenKey, value, 0);
                }
            }
        } else if (strnicmp(key, "3/", 2) == 0) {
            lastKey = 3;
            tokenType = "NetLibrary";
            tokenKey = (unsigned char *)"nl";
            if (decrypt) {
                resultValue = UserTokenDecrypt2(value, (char *)tokenKey);
            } else {
                resultValue = UserToken2((char *)tokenKey, value, 0);
            }
        } else if (strnicmp(key, "4/", 2) == 0) {
            lastKey = 4;
            tokenType = "Films Media Group";
            tokenKey = (unsigned char *)"fmg";
            if (decrypt) {
                resultValue = UserTokenDecrypt2(value, (char *)tokenKey);
            } else {
                resultValue = UserToken2((char *)tokenKey, value, 0);
            }
        } else if (strnicmp(key, "5/", 2) == 0) {
            lastKey = 5;
            tokenType = "Gartner";
            tokenKey = (unsigned char *)"g";
            if (decrypt) {
                resultValue = UserTokenDecrypt2(value, (char *)tokenKey);
            } else {
                resultValue = UserToken2((char *)tokenKey, value, 0);
            }
        } else if (strnicmp(key, "6/", 2) == 0) {
            lastKey = 6;
            tokenType = "PPID";
            tokenKey = ffs[ATPPID].value ? ffs[ATPPID].value : "";
            if (decrypt) {
                resultValue = UserTokenDecrypt2(value, (char *)tokenKey);
            } else {
                resultValue = UserToken2((char *)tokenKey, value, 0);
            }
        }

        if (tokenType) {
            UUSendZ(s, "<p>");
            if (decrypt) {
                WriteLine(s, "%s token ", tokenType);
            } else {
                UUSendZ(s, "EZproxy user ");
            }
            SendHTMLEncoded(s, value);
            UUSendZ(s, " is ");
            if (decrypt) {
                if (resultValue == NULL)
                    UUSendZ(s, "not assigned to an " EZPROXYNAMEPROPER " user");
                else {
                    UUSendZ(s, "assigned to " EZPROXYNAMEPROPER " user ");
                    SendHTMLEncoded(s, resultValue);
                }
            } else {
                if (resultValue == NULL)
                    WriteLine(s, "not assigned to %s %s token", IndefiniteArticle(tokenType, 0),
                              tokenType);
                else {
                    WriteLine(s, "assigned to %s token ", tokenType);
                    SendHTMLEncoded(s, resultValue);
                }
            }
            UUSendZ(s, "</p>\n");
            FreeThenNull(resultValue);
        }
    }

    firstToken = NULL;

    UUSendZ(s, "<form action='/token' method='post'>\n");

    for (i = 0, b = databases; i < cDatabases; i++, b++) {
        if (b->tokenKey == NULL)
            continue;
        count++;
        if (count == 1) {
            firstType = 0;
            firstToken = b;
        } else {
            if (count == 2) {
                AdminTokenKeyField(t, firstType, firstToken, 0, 1, lastKey, NULL);
            }
            AdminTokenKeyField(t, 0, b, 0, 0, lastKey, NULL);
        }
    }

    if (ebrarySites) {
        count++;
        if (count == 1)
            firstType = 1;
        else {
            if (count == 2) {
                AdminTokenKeyField(t, firstType, firstToken, 0, 1, lastKey, NULL);
            }
            AdminTokenKeyField(t, 1, NULL, 0, 0, lastKey, NULL);
        }
    }

    /*
    for (ebs = ebrarySites; ebs; ebs = ebs->next) {
        if (firstEbs == NULL) {
            firstEbs = ebs;
            if (firstToken == NULL) {
                UUSendZ(s, "<form action='/token' method='post'>\n");
                continue;
            }
            if (anySent == 0) {
            AdminTokenKeyField(t, firstToken, NULL, NULL, 0);
            anySent = 1;
        }
        if (anySent == 0) {
            UUSendZ(s, "Choose the database that generated this token:<p>\n");
            AdminTokenKeyField(t, NULL, firstEbs, NULL, 0);
            anySent = 1;
        }
        AdminTokenKeyField(t, NULL, ebs, NULL, 0);
    }
    */

    if (usageLogUser & ULUFMG) {
        count++;
        if (count == 1)
            firstType = 4;
        else {
            if (count == 2) {
                AdminTokenKeyField(t, firstType, firstToken, 0, 1, lastKey, NULL);
            }
            AdminTokenKeyField(t, 4, NULL, 0, 0, lastKey, NULL);
        }
    }

    if (usageLogUser & ULUGARTNER) {
        count++;
        if (count == 1)
            firstType = 5;
        else {
            if (count == 2) {
                AdminTokenKeyField(t, firstType, firstToken, 0, 1, lastKey, NULL);
            }
            AdminTokenKeyField(t, 5, NULL, 0, 0, lastKey, NULL);
        }
    }

    if (usageLogUser & ULUNETLIBRARY) {
        count++;
        if (count == 1)
            firstType = 3;
        else {
            if (count == 2) {
                AdminTokenKeyField(t, firstType, firstToken, 0, 1, lastKey, NULL);
            }
            AdminTokenKeyField(t, 3, NULL, 0, 0, lastKey, NULL);
        }
    }

    if (overDriveSites) {
        count++;
        if (count == 1)
            firstType = 2;
        else {
            if (count == 2) {
                /* Either firstToken or ebrarySites will be non-null, since
                   if both were non-null, count would be > 2
                */
                AdminTokenKeyField(t, firstType, firstToken, 0, 1, lastKey, NULL);
            }
            AdminTokenKeyField(t, 2, NULL, 0, 0, lastKey, NULL);
        }
    }

    /*
    for (ods = overDriveSites; ods; ods = ods->next) {
        if (firstOds == NULL) {
            firstOds = ods;
            if (firstToken == NULL && firstEbs == NULL) {
                UUSendZ(s, "<form action='/token' method='post'>\n");
                continue;
            }
            AdminTokenKeyField(t, firstToken, firstEbs, NULL, 0);
            anySent = 1;
        }
        if (anySent == 0) {
            UUSendZ(s, "Choose the database that generated this token:<p>\n");
            AdminTokenKeyField(t, NULL, NULL, firstOds, 0);
            anySent = 1;
        }
        AdminTokenKeyField(t, NULL, NULL, ods, 0);
    }
    */

    count++;
    if (count == 1) {
        firstType = 6;
    } else {
        if (count == 2) {
            AdminTokenKeyField(t, firstType, firstToken, 0, 1, lastKey, NULL);
        }
        AdminTokenKeyField(t, 6, NULL, 0, 0, lastKey, ffs[ATPPID].value);
    }

    switch (count) {
    case 0:
        UUSendZ(s, "</form>\n");
        UUSendZ(s, EZPROXYCFG
                " does not contain any TokenKey, ebrarySite, FMG, NetLibrary or OverDriveSite "
                "statements\n");
        AdminFooter(t, 0);
        return;

    case 1:
        AdminTokenKeyField(t, firstType, firstToken, 1, 0, lastKey, ffs[ATPPID].value);
        break;

    default:
        UUSendZ(s, "</p>\n");
    }

    WriteLine(s,
              "\
<div><select name='action'>\n\
<option value='decrypt'%s>Token to username</option>\n\
<option value='encrypt'%s>Username to token</option>\n\
</select>\n",
              (decrypt ? " selected" : ""), (decrypt ? "" : " selected"));

    UUSendZ(s, "<input type='text' name='value' value='");
    if (value) {
        SendHTMLEncoded(s, value);
    }
    UUSendZ(s, "'></div>\n<input type='submit' value='Process'>\n</form>\n");

    AdminFooter(t, 0);
    /* AdminToken */
}
