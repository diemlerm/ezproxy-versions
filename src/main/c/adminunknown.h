#ifndef __ADMINUNKNOWN_H__
#define __ADMINUNKNOWN_H__

#include "common.h"

void AdminUnknown(struct TRANSLATE *t, char *url);

#endif  // __ADMINUNKNOWN_H__
