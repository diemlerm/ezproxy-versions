/**
 * /scifinder URL invoked by users to open access for using
 * the CAS SciFinder Scholar client software.
 *
 * This module provides part of the support for CAS SciFinder Scholar,
 * a Z39.50-based client for searching chemical research.  This URL
 * tells EZproxy to mark the session to allow an incoming Z39.50 connection
 * from the user's IP address for two minutes.  This works in concert with
 * z3950.c.
 */

#define __UUFILE__ "adminscifinder.c"

#include "common.h"
#include "uustring.h"

#include "adminscifinder.h"

void AdminSciFinder(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    int i;
    struct LOGINPORT *lp;
    struct SESSION *e;
    const int ASTIMEOUT = 0;
    struct FORMFIELD ffs[] = {
        {"timeout", NULL, 0, 0, 10},
        {NULL, NULL, 0, 0}
    };
    BOOL timeout;

    FindFormFields(query, post, ffs);
    timeout = ffs[ASTIMEOUT].value != NULL;

    for (lp = loginPorts, i = 0;; i++, lp++) {
        if (i >= cLoginPorts) {
            HTTPCode(t, 404, 1);
            goto Finished;
        }
        if (lp->useSsl == USESSLZ3950)
            break;
    }

    if ((e = t->session) == NULL) {
        AdminRelogin(t);
        goto Finished;
    }

    if (!GroupMaskOverlap(e->gm, lp->gm)) {
        AdminLogup(t, TRUE, NULL, NULL, lp->gm, FALSE);
        goto Finished;
    }

    HTMLHeader(t, htmlHeaderNoCache);
    if (FileExists(SCIFINDERHTM)) {
        SendEditedFile(t, NULL, SCIFINDERHTM, 1, NULL, 0, NULL);
    } else {
        time_t now;
        UUSendZ(s, TAG_DOCTYPE_HTML_HEAD "<title>SciFinder Access</title>\n");
        if (!timeout) {
            WriteLine(s, "<meta content='120; url=%s/scifinder?timeout=on' http-equiv='refresh'>\n",
                      t->myUrl);
            UUtime(&e->sciFinderAuthorized);
        }
        UUSendZ(s, "</head>\n<body>\n");
        if (timeout) {
            WriteLine(s,
                      "<p>If you want to connect to SciFinder Scholar again, start by clicking <a "
                      "href='/scifinder?id=%d'>here</a>.</p>\n",
                      UUtime(&now));
        } else {
            UUSendZ(s, "<p>You now have 2 minutes to launch SciFinder Scholar.</p>\n");
        }
        UUSendZ(s, "</body>\n</html>\n");
    }

Finished:;
}
