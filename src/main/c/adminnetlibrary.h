#ifndef __ADMINNETLIBRARY_H__
#define __ADMINNETLIBRARY_H__

#include "common.h"

BOOL NetLibraryHasNLReturn(char *url);
void AdminNetLibrary(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINNETLIBRARY_H__
