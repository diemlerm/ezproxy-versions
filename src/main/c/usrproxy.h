#ifndef __USRPROXY_H__
#define __USRPROXY_H__

#include "common.h"

/* Returns 0 if valid, 1 if not, 3 if unable to connect to pop host */
int FindUserProxy(char *proxy,
                  struct USERINFO *uip,
                  char *user,
                  char *pass,
                  struct sockaddr_storage *pInterface);

#endif  // __USRPROXY_H__
