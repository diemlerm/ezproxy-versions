/**
 * CAS (Central Authentication Server) user authentication
 *
 * CAS is an SSO technology developed by Yale.  This module provides support
 * for EZproxy to access CAS as a CAS client.  See also admincas.c, which
 * allows EZproxy to act as a CAS server for other CAS clients.
 *
 * This authentication method supports common conditions and actions.
 */

#define __UUFILE__ "usrcas.c"

#include "common.h"
#include "usrcas.h"

#ifdef WIN32
/* io.h, fcntl.h and share.h needed for _sopen in Win32 */
#include <io.h>
#include <fcntl.h>
#include <share.h>
#include <lm.h>
#endif

#include <sys/stat.h>

#include "usr.h"

#include "uuxml.h"

struct CASNS {
    struct CASNS *next;
    char *ns;
    char *urn;
};

struct CASCTX {
    xmlParserCtxtPtr ctxt;
    xmlDocPtr doc; /* the resulting document tree */
    xmlXPathContextPtr xpathCtx;
    struct sockaddr_storage *pInterface;
    struct CASNS *casNs;
};

static BOOL CASAggregateTest(struct TRANSLATE *t,
                             struct USERINFO *uip,
                             void *context,
                             const char *var,
                             const char *testVal,
                             enum AGGREGATETESTOPTIONS ato,
                             struct VARIABLES *rev) {
    struct AGGREGATETESTCONTEXT atc;
    struct CASCTX *casCtx = (struct CASCTX *)context;
    xmlXPathObjectPtr xpathObj = NULL;
    xmlNodeSetPtr nodes = NULL;
    int i;

    if (!ExpressionAggregateTestInitialize(&atc, uip, testVal, ato))
        return FALSE;

    if (casCtx->xpathCtx == NULL)
        goto Finished;

    if (strchr(var, '/') == NULL) {
        char *tempTag;
        if (!(tempTag = malloc(strlen(var) + 16)))
            PANIC;
        sprintf(tempTag, "//*/%s", var);
        if (uip->debug) {
            UsrLog(uip, "CAS auth tag lacks path; changed to %s", tempTag);
        }
        xpathObj = xmlXPathEvalExpression(BAD_CAST tempTag, casCtx->xpathCtx);
        FreeThenNull(tempTag);
    } else {
        xpathObj = xmlXPathEvalExpression(BAD_CAST var, casCtx->xpathCtx);
    }

    if (xpathObj == NULL || (nodes = xpathObj->nodesetval) == NULL || (nodes->nodeNr == 0)) {
        goto Finished;
    }

    for (i = 0; i < nodes->nodeNr; i++) {
        xmlChar *xmlVal = xmlNodeGetContent(nodes->nodeTab[i]);
        BOOL stop = !(ExpressionAggregateTestValue(&atc, (char *)xmlVal));
        xmlFreeThenNull(xmlVal);
        if (stop)
            break;
    }

Finished:
    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    return ExpressionAggregateTestTerminate(&atc, rev);
}

char *UsrCASAuthGetValue(struct TRANSLATE *t,
                         struct USERINFO *uip,
                         void *context,
                         const char *var,
                         const char *index) {
    struct CASCTX *casCtx = (struct CASCTX *)context;
    xmlXPathObjectPtr xpathObj = NULL;
    xmlNodeSetPtr nodes = NULL;
    int i;
    char *val = NULL;
    int indexVal = index ? atoi(index) : 0;
    int indexCurrent = 0;

    if (casCtx->xpathCtx == NULL)
        goto Finished;

    if (uip->debug) {
        UsrLog(uip, "CAS auth searching for tag %s", var);
    }

    if (strchr(var, '/') == NULL) {
        char *tempTag;
        if (!(tempTag = malloc(strlen(var) + 16)))
            PANIC;
        sprintf(tempTag, "//*/%s", var);
        if (uip->debug) {
            UsrLog(uip, "CAS auth tag lacks path; changed to %s", tempTag);
        }
        xpathObj = xmlXPathEvalExpression(BAD_CAST tempTag, casCtx->xpathCtx);
        FreeThenNull(tempTag);
    } else {
        xpathObj = xmlXPathEvalExpression(BAD_CAST var, casCtx->xpathCtx);
    }

    if (xpathObj == NULL || (nodes = xpathObj->nodesetval) == NULL || (nodes->nodeNr == 0)) {
        goto Finished;
    }

    for (i = 0; i < nodes->nodeNr; i++) {
        xmlChar *xmlVal = xmlNodeGetContent(nodes->nodeTab[i]);
        if (indexVal == indexCurrent++) {
            if (!(val = strdup((char *)xmlVal)))
                PANIC;
            xmlFreeThenNull(xmlVal);
            goto Finished;
        }
        xmlFreeThenNull(xmlVal);
    }

Finished:
    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    return val;
}

static int UsrCASMsgAuth(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct CASCTX *casCtx = (struct CASCTX *)context;
    char *line, *str;
    xmlChar *buffer = NULL;
    int len = 0;

    if (casCtx->doc == NULL)
        return 1;

    UsrLog(uip, "CAS auth response");
    xmlDocDumpFormatMemory(casCtx->doc, &buffer, &len, xmlIndentTreeOutput);

    if (buffer == NULL)
        return 1;

    for (str = NULL, line = StrTok_R((char *)buffer, "\r\n", &str); line;
         line = StrTok_R(NULL, "\r\n", &str)) {
        if (*line)
            UsrLog(uip, "CAS: %s", line);
    }

    xmlFreeThenNull(buffer);
    return 0;
}

int UsrCASNamespace(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct CASCTX *casCtx = (struct CASCTX *)context;
    char *arg2;
    struct CASNS *n;

    arg2 = SkipNonWS(arg);
    if (*arg2) {
        *arg2++ = 0;
        arg2 = SkipWS(arg2);
        if (*arg2) {
            if (!(n = calloc(1, sizeof(*n))))
                PANIC;
            if (!(n->ns = strdup(arg)))
                PANIC;
            if (!(n->urn = strdup(arg2)))
                PANIC;
            n->next = casCtx->casNs;
            casCtx->casNs = n;
        }
    }

    return 0;
}

void UsrCasURL(struct TRANSLATE *t, struct USERINFO *uip, struct CASCTX *casCtx, char *casLogin) {
    char *eob = t->buffer + sizeof(t->buffer) - 1;
    char *concatUrl = "?";
    char *ur = NULL;

    if (casLogin) {
        strcpy(t->buffer, casLogin);
        strcat(t->buffer, strchr(t->buffer, '?') == NULL ? "?" : "&");
    } else {
        *t->buffer = 0;
    }

    strcat(t->buffer, "service=");
    AddEncodedField(t->buffer, t->useSsl ? myUrlHttps : myUrlHttp, eob);
    AddEncodedField(t->buffer, "/login", eob);

    if (uip->auth && *uip->auth) {
        AddEncodedField(t->buffer, concatUrl, eob);
        concatUrl = "&";
        AddEncodedField(t->buffer, "auth=", eob);
        AddDoubleEncodedField(t->buffer, uip->auth, eob);
    }

    if (t->logTag) {
        AddEncodedField(t->buffer, concatUrl, eob);
        concatUrl = "&";
        AddEncodedField(t->buffer, "tag=", eob);
        AddDoubleEncodedField(t->buffer, t->logTag, eob);
    }

    if (uip->url && *uip->url) {
        AddEncodedField(t->buffer, concatUrl, eob);
        concatUrl = "&";

        ur = URLReference(uip->url, t->logTag);
        AddEncodedField(t->buffer, "qurl=", eob);
        /* ur doesn't have to be double-encoded since it uses all "safe" characters
           that require no encoding at all
        */
        AddEncodedField(t->buffer, ur, eob);
        FreeThenNull(ur);
    }

    if (uip->renew || (t->session && t->session->reloginRequired))
        strcat(t->buffer, "&renew=true");
}

void UsrCASClearXML(struct CASCTX *casCtx) {
    if (casCtx) {
        if (casCtx->xpathCtx) {
            xmlXPathFreeContext(casCtx->xpathCtx);
            casCtx->xpathCtx = NULL;
        }

        if (casCtx->ctxt) {
            xmlFreeParserCtxt(casCtx->ctxt);
            casCtx->ctxt = NULL;
        }

        /* Even though the doc is initially part of the parser context, the call above
        to xmlFreeParserCtxt does not free the document.
        */
        if (casCtx->doc) {
            xmlFreeDoc(casCtx->doc);
            casCtx->doc = NULL;
        }
    }
}

/* This always returns 1 (failure) since, at any phase, there is no condition
   that would be a valid to follow based on the condition of this statement
*/
int UsrCASLoginURL(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct CASCTX *casCtx = (struct CASCTX *)context;

    /* The check for urlReference allows us to avoid entering an endless loop */
    if (uip->urlReference == 0 && (uip->ticket == NULL || *uip->ticket == 0)) {
        UsrCasURL(t, uip, casCtx, arg);
        LocalRedirect(t, t->buffer, 0, 0, NULL);
        uip->denied = uip->deniedNotified = uip->skipping = 1;
        uip->result = resultNotified;
    }
    return 1;
}

int UsrCASServiceValidateURL(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct CASCTX *casCtx = (struct CASCTX *)context;
    struct UUSOCKET rr, *r = &rr;
    BOOL inHeader;
    char line[8192];
    int res;
    xmlXPathObjectPtr xpathObj = NULL;
    xmlNodeSetPtr nodes = NULL;
    xmlChar *user = NULL;
    char *host, *colon, *endOfHost;
    char useSsl;
    PORT port;
    char save = 0;
    BOOL cas1 = 0;
    BOOL utf16 = 0;
    BOOL cas1Valid = 0;
    int result = 1;
    struct CASNS *p;

    UUInitSocket(r, INVALID_SOCKET);

    if (uip->ticket == NULL || *uip->ticket == 0)
        goto Finished;

    UsrCASClearXML(casCtx);

    UsrCasURL(t, uip, casCtx, NULL);

    if (uip->ticket == NULL || *uip->ticket == 0) {
        LocalRedirect(t, t->buffer, 0, 0, NULL);
        uip->denied = uip->deniedNotified = 1;
        goto Finished;
    }

    if (uip->user == NULL || uip->pass == NULL || strlen(uip->user) > 64 || strlen(uip->pass) > 64)
        goto Finished;

    host = SkipHTTPslashesAt(arg, &useSsl);
    ParseHost(host, &colon, &endOfHost, 0);

    if (endOfHost) {
        save = *endOfHost;
        *endOfHost = 0;
    }

    port = 0;

    if (colon) {
        *colon = 0;
        port = atoi(colon + 1);
    } else if (host != arg) {
        port = (useSsl ? 443 : 80);
    }

    if (port == 0) {
        UsrLog(uip, "CAS Server port missing");
        goto Finished;
    }

    if (UUConnectWithSource2(r, host, port, "CAS", casCtx->pInterface, useSsl))
        goto Finished;

    if (colon)
        *colon = ':';

    UUSendZ(r, "GET ");

    if (endOfHost) {
        *endOfHost = save;
        UUSendZ(r, endOfHost);
        WriteLine(r, "%c", strchr(endOfHost, '?') == NULL ? '?' : '&');
        *endOfHost = 0;
    } else {
        UUSendZ(r, "/?");
    }

    UUSendZ(r, t->buffer);
    UUSendZ(r, "&ticket=");
    SendUrlEncoded(r, uip->ticket);

    WriteLine(r,
              " HTTP/1.0\r\n\
Host: %s\r\n\
Pragma: no-cache\r\n\
User-Agent: Mozilla/4.0 (compatible; EZproxy)\r\n\
Accept: */*\r\n\
Connection: close\r\n\
Cache-Control: no-cache\r\n\
\r\n",
              host);

    if (debugLevel > 5)
        UsrLog(uip, "The response");

    inHeader = 1;
    while (ReadLine(r, line, sizeof(line)) > 0) {
        if (uip->debug)
            UsrLog(uip, "CASServiceValidate %s", line);

        if (inHeader) {
            if (line[0] == 0) {
                inHeader = 0;
                if (utf16) {
                    r->utf16 = 1;
                }
            } else {
                Trim(line, TRIM_LEAD | TRIM_TRAIL);
                if (StartsIWith(line, "Content-Type:") && StrIStr(line, "charset=UTF-16") != NULL) {
                    utf16 = 1;
                }
            }
            continue;
        }

        if (line[0] == 0)
            continue;

        if (cas1) {
            if (cas1Valid && cas1 == 1) {
                if (!(user = BAD_CAST strdup(line)))
                    PANIC;
            }
            cas1++;
            continue;
        }

        if (casCtx->ctxt == NULL) {
            if (cas1 == 0 && (stricmp(line, "yes") == 0 || stricmp(line, "no") == 0)) {
                cas1 = 1;
                cas1Valid = stricmp(line, "yes") == 0;
                continue;
            }
            casCtx->ctxt = xmlCreatePushParserCtxt(NULL, NULL, line, strlen(line), NULL);
            if (casCtx->ctxt == NULL) {
                UsrLog(uip, "Non-XML response from CAS");
                goto Finished;
            }
        } else {
            xmlParseChunk(casCtx->ctxt, line, strlen(line), 0);
        }
    }

    if (cas1) {
        if (cas1Valid == 0)
            goto Finished;
    } else {
        if (casCtx->ctxt == NULL) {
            UsrLog(uip, "CAS server did not respond\n");
            goto Finished;
        }

        xmlParseChunk(casCtx->ctxt, line, 0, 1);

        casCtx->doc = casCtx->ctxt->myDoc;
        res = casCtx->ctxt->wellFormed;

        if (debugLevel > 5)
            printf("\n\n");

        casCtx->xpathCtx = xmlXPathNewContext(casCtx->doc);
        if (casCtx->xpathCtx == NULL) {
            UsrLog(uip, "Error: unable to create new XPath context");
            goto Finished;
        }

        xmlXPathRegisterNs(casCtx->xpathCtx, BAD_CAST "cas", BAD_CAST "http://www.yale.edu/tp/cas");

        for (p = casCtx->casNs; p; p = p->next)
            xmlXPathRegisterNs(casCtx->xpathCtx, BAD_CAST p->ns, BAD_CAST p->urn);

        xpathObj = xmlXPathEvalExpression(
            BAD_CAST "/cas:serviceResponse/cas:authenticationSuccess/cas:user", casCtx->xpathCtx);
        if (xpathObj == NULL || (nodes = xpathObj->nodesetval) == NULL || (nodes->nodeNr == 0)) {
            goto Finished;
        }

        user = xmlNodeGetContent(nodes->nodeTab[0]);
    }

    *uip->logUser = 0;

    if (user) {
        Trim((char *)user, TRIM_CTRL);
        StrCpy3(uip->logUser, (char *)user, sizeof(uip->logUser));
    }

    if (*uip->logUser == 0)
        strcpy(uip->logUser, "cas");

    uip->user = uip->logUser;
    uip->result = resultValid;

    result = 0;

    if (t->session) {
        t->session->reloginRequired = 0;
    }

Finished:
    UUStopSocket(r, 0);

    xmlFreeThenNull(user);

    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    return result;
}

int UsrCASIfTest(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct CASCTX *casCtx = (struct CASCTX *)context;
    int result = 1;
    xmlXPathObjectPtr xpathObj = NULL;
    xmlNodeSetPtr nodes = NULL;
    int i;
    xmlChar *val = NULL;
    char *arg2;
    char *tag = NULL;
    BOOL re = 0;
    BOOL negate = 0;
    char save;
    xmlRegexpPtr xrp = NULL;

    if (casCtx->xpathCtx == NULL || arg == NULL)
        goto Finished;

    for (; *arg; arg = SkipWS(arg2)) {
        arg2 = SkipNonWS(arg);
        if (save = *arg2)
            *arg2++ = 0;
        if (stricmp(arg, "-re") == 0) {
            re = 1;
            continue;
        }
        if (stricmp(arg, "-not") == 0) {
            negate = 1;
            continue;
        }
        if (tag == NULL) {
            tag = arg;
            continue;
        }
        if (save) {
            arg2--;
            *arg2 = save;
        }
        break;
    }

    if (tag == NULL || *arg == 0)
        goto Finished;

    if (uip->debug) {
        UsrLog(uip, "CAS Test searching for tag %s with value %s", tag, arg);
    }

    if (strchr(tag, '/') == NULL) {
        char *tempTag;
        if (!(tempTag = malloc(strlen(tag) + 16)))
            PANIC;
        sprintf(tempTag, "//*/%s", tag);
        if (uip->debug) {
            UsrLog(uip, "CAS Test tag lacks path; changed to %s", tempTag);
        }
        xpathObj = xmlXPathEvalExpression(BAD_CAST tempTag, casCtx->xpathCtx);
        FreeThenNull(tempTag);
    } else {
        xpathObj = xmlXPathEvalExpression(BAD_CAST tag, casCtx->xpathCtx);
    }

    if (xpathObj == NULL || (nodes = xpathObj->nodesetval) == NULL || (nodes->nodeNr == 0)) {
        goto Finished;
    }

    if (re) {
        xrp = xmlRegexpCompile(BAD_CAST arg);
        if (xrp == NULL) {
            UsrLog(uip, "Invalid regular expression for Test -RE: %s", arg);
            goto Finished;
        }
    }

    for (i = 0; i < nodes->nodeNr; i++) {
        val = xmlNodeGetContent(nodes->nodeTab[i]);
        if (uip->debug)
            UsrLog(uip, "CAS Test value %s", val);
        if (xrp) {
            int rc;
            rc = xmlRegexpExec(xrp, BAD_CAST val);
            if (rc == 1) {
                result = 0;
            }
        } else {
            if (stricmp(arg, (char *)val) == 0) {
                result = 0;
            }
        }
        xmlFreeThenNull(val);
        if (result == 0)
            break;
    }

    if (uip->debug) {
        if (result == 0)
            UsrLog(uip, "CAS tag value found");
        else
            UsrLog(uip, "CAS tag value not found");
    }

    if (negate) {
        result = (result == 0 ? 1 : 0);
        if (uip->debug) {
            UsrLog(uip, "CAS Test result negated");
        }
    }

Finished:
    if (xrp) {
        xmlRegFreeRegexp(xrp);
        xrp = NULL;
    }

    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    return result;
}

enum FINDUSERRESULT FindUserCAS(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                char *file,
                                int f,
                                struct FILEREADLINEBUFFER *frb,
                                struct sockaddr_storage *pInterface,
                                char *casLogin,
                                char *casServiceValidate) {
    struct CASCTX *casCtx, casCtxBuffer;
    struct CASNS *p, *next;

    struct USRDIRECTIVE udc[] = {
        {"IfTest",             UsrCASIfTest,             0},
        {"LoginURL",           UsrCASLoginURL,           0},
        {"MsgAuth",            UsrCASMsgAuth,            0},
        {"Namespace",          UsrCASNamespace,          0},
        {"ServiceValidateURL", UsrCASServiceValidateURL, 0},
        {"Test",               UsrCASIfTest,             0},
        {NULL,                 NULL,                     0}
    };

    memset(&casCtxBuffer, 0, sizeof(casCtxBuffer));
    casCtx = &casCtxBuffer;
    casCtx->pInterface = pInterface;

    if (uip->user && *uip->user) {
        uip->result = resultInvalid;
        goto Finished;
    }

    if (casLogin) {
        if (uip->ticket && *uip->ticket)
            UsrCASServiceValidateURL(t, uip, casCtx, casServiceValidate);
        else {
            UsrCASLoginURL(t, uip, casCtx, casLogin);
        }
    } else {
        /* authGetValue and/or authAggregateTest should always be assigned
           just before a call to UsrHandler or before the macro USRCOMMON1
           since both of these reset the authGetValue and authAggregateTest
           to NULL when the test completes to avoid having one of these functions
           called later during user.txt processing when the function may no
           longer match the authentication in effect
        */
        uip->authGetValue = UsrCASAuthGetValue;
        uip->authAggregateTest = CASAggregateTest;
        uip->flags = USRUSERMAYBENULL;
        UsrHandler(t, "CAS", udc, casCtx, uip, file, f, frb, pInterface, NULL);
    }

Finished:
    UsrCASClearXML(casCtx);

    for (p = casCtx->casNs; p; p = next) {
        next = p->next;
        FreeThenNull(p->ns);
        FreeThenNull(p->urn);
        FreeThenNull(p);
    }

    return uip->result;
}
