#ifndef __USRIMAP_H__
#define __USRIMAP_H__

#include "common.h"

/* Returns 0 if valid, 1 if not, 3 if unable to connect to imap host */
int FindUserImap(char *imapHost,
                 char *user,
                 char *pass,
                 struct sockaddr_storage *pInterface,
                 char useSsl);

#endif  // __USRIMAP_H__
