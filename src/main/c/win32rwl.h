#ifndef __WIN32RWL_H__
#define __WIN32RWL_H__

#include "common.h"

struct RWL {
    char *name;
    CRITICAL_SECTION cs;
    HANDLE condRead;
    HANDLE condWrite;
    DWORD writeThread;
    int waitRead;
    int waitWrite;
    int activeRead;
    int activeWrite;
};

#endif  // __WIN32RWL_H__
