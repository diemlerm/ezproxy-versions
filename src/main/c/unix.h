#ifndef __UNIX_H__
#define __UNIX_H__

void handle_sigchld(int arg);
void handle_sigterm(int arg);
void handle_sighup(int arg);
void handle_sigint(int arg);
void handle_sigusr1(int arg);
void handle_sigusr2(int arg);
void EstablishChargeSignalHandlers(void);
void StartupInstall(void);
void StartupRemove(void);
DWORD GetTickCount(void);

#endif  // __UNIX_H__
