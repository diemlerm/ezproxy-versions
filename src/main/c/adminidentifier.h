#ifndef __ADMINIDENTIFIER_H__
#define __ADMINIDENTIFIER_H__

#include "common.h"

void AdminIdentifier(struct TRANSLATE *t, char *query, char *post);
void AdminIdentifierScopes(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINIDENTIFIER_H__
