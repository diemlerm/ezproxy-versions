#ifndef __USRSAGEBRUSH_H__
#define __USRSAGEBRUSH_H__

#include "common.h"

enum FINDUSERRESULT FindUserSagebrush(struct TRANSLATE *t,
                                      struct USERINFO *uip,
                                      char *file,
                                      int f,
                                      struct FILEREADLINEBUFFER *frb,
                                      struct sockaddr_storage *pInterface);

#endif  // __USRSAGEBRUSH_H__
