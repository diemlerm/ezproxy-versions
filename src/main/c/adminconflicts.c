#define __UUFILE__ "adminconflicts.c"

#include "common.h"
#include "uustring.h"

#include "adminconflicts.h"

static BOOL DomainOverlap(struct INDOMAIN *m1, struct INDOMAIN *m2) {
    int i;
    struct INDOMAIN *mt;
    char *end;

    /* This function will try reversing m1 and m2 to test the possible combinations */
    for (i = 0; i < 2; i++, mt = m1, m1 = m2, m2 = mt) {
        if (m1->hostOnly) {
            if (m2->hostOnly) {
                /* Two host */
                return stricmp(m1->domain, m2->domain) == 0 && m1->port == m2->port;
            }
            /* First host, second domain */
        }

        /* Second host, first domain */
        if (m2->hostOnly) {
            /* Loop to retry as first host, second domain */
            continue;
        }

        /* Both domain */

        if ((end = EndsIWith(m1->domain, m2->domain)) && (end == m1->domain || *(end - 1) == '.'))
            return 1;
    }

    return 0;
}

static void FreeDatabaseConflicts(struct DATABASECONFLICT **dbc) {
    struct DATABASECONFLICT *p, *n;

    for (p = *dbc; p; p = n) {
        n = p->next;
        free(p);
    }
    *dbc = NULL;
}

static struct DATABASECONFLICT *FindDatabaseConflict(struct DATABASECONFLICT **dbc,
                                                     struct DATABASE *b1,
                                                     struct DATABASE *b2) {
    struct DATABASECONFLICT *p, *last, *n;

    if (b1 > b2) {
        struct DATABASE *bt = b1;
        b1 = b2;
        b2 = bt;
    }

    for (last = NULL, p = *dbc; p; last = p, p = p->next) {
        if (p->b1 < b1)
            continue;
        if (p->b1 > b1)
            break;
        if (p->b2 < b2)
            continue;
        if (p->b2 > b2)
            break;
        return p;
    }

    if (!(n = calloc(1, sizeof(*n))))
        PANIC;
    n->b1 = b1;
    n->b2 = b2;
    n->next = p;
    if (last)
        last->next = n;
    else
        *dbc = n;

    return n;
}

static BOOL CheckDatabaseConflicts(struct DATABASECONFLICT **dbc) {
    int i1, i2;
    struct DATABASE *b1, *b2;
    struct INDOMAIN *m1, *m2;
    int conflicts;
    int anyConflicts = 0;
    int i;
    struct HOST *h;
    struct VALIDATE *validate;
    PORT forceMyPort;
    BOOL gateway;
    BOOL javaScript;
    struct DATABASECONFLICT *p;
    GROUPMASK gmAnd;

    if (dbc)
        *dbc = NULL;

    gmAnd = GroupMaskNew(0);

    for (i1 = 0, b1 = databases + i1; i1 < cDatabases; i1++, b1++) {
        for (i2 = i1 + 1, b2 = databases + i2; i2 < cDatabases; i2++, b2++) {
            conflicts = 0;

            if (b1->getName && b2->getName && stricmp(b1->getName, b2->getName) == 0)
                conflicts |= DBCGETNAME;

            for (m1 = b1->dbdomains; m1; m1 = m1->next) {
                for (m2 = b2->dbdomains; m2; m2 = m2->next) {
                    if (DomainOverlap(m1, m2)) {
                        if (m1->javaScript != m2->javaScript)
                            conflicts |= DBCJAVASCRIPT;
                        if (b1->cIpTypes != b2->cIpTypes)
                            conflicts |= DBCIP;
                        if (b1->patterns.pattern != b2->patterns.pattern)
                            conflicts |= DBCPATTERN;
                    }
                }
            }

            if (conflicts) {
                anyConflicts = 1;
                if (dbc) {
                    p = FindDatabaseConflict(dbc, b1, b2);
                    p->conflictMask |= conflicts;
                }
            }
        }
    }

    for (i = 0, h = hosts + i; i < cHosts; i++, h++) {
        if (b1 = h->myDb) {
            for (b2 = b1; b2 = MatchingDatabase(b2 + 1, h->hostname, h->remport, &validate,
                                                &forceMyPort, &gateway, &javaScript);) {
                conflicts = 0;

                /* For group conflicts, we really only care about initial database that
                   matches a real host to all subsequent, allowing the following scenario
                   not to be a conflict:

                   Group A+B
                   Title -hide Some Database
                   URL http://www.somedb.com
                   Domain somedb.com

                   Group A
                   Title Some Appearing Database URL
                   URL http://www.somedb.com/a

                   Group B
                   Title Some Other Appearing Dtabase URL
                   URL http://www.somedb.com/b
                */

                GroupMaskCopy(gmAnd, b1->gm);
                GroupMaskAnd(gmAnd, b2->gm);
                if (!GroupMaskEqual(gmAnd, b2->gm))
                    conflicts |= DBCGROUP;

                if (h->javaScript != javaScript)
                    conflicts |= DBCJAVASCRIPT;

                if (b1->cIpTypes != b2->cIpTypes)
                    conflicts |= DBCIP;

                if (b1->patterns.pattern != b2->patterns.pattern)
                    conflicts |= DBCPATTERN;

                if (conflicts) {
                    anyConflicts = 1;
                    if (dbc) {
                        p = FindDatabaseConflict(dbc, b1, b2);
                        p->conflictMask |= conflicts;
                    }
                }
            }
        }
    }

    GroupMaskFree(&gmAnd);

    return anyConflicts;
}

void AdminConflicts(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    struct DATABASECONFLICT *dbc = NULL;
    struct DATABASECONFLICT *p;

    AdminHeader(t, 0, "Check Database Definition Conflicts");

    if (!CheckDatabaseConflicts(&dbc)) {
        UUSendZ(s, "<p>No database conflicts found</p>\n");
    } else {
        UUSendZ(s, "<p>Conflicts exist between the following databases:</p>\n");
        UUSendZ(s, "<p>\n");
        for (p = dbc; p; p = p->next) {
            WriteLine(s, " <a href='/status?extended=on&db=%d.%d'>", p->b1 - databases,
                      p->b2 - databases);
            WriteLine(s, "%d ", p->b1 - databases);
            SendHTMLEncoded(s, p->b1->title);
            WriteLine(s, " and %d ", p->b2 - databases);
            SendHTMLEncoded(s, p->b2->title);

            if (p->conflictMask & DBCIP) {
                UUSendZ(s, " AutoLoginIP/ExcludeIP/IncludeIP");
            }

            if (p->conflictMask & DBCPATTERN) {
                UUSendZ(s, " Find/Replace");
            }

            if (p->conflictMask & DBCGETNAME) {
                UUSendZ(s, " GetName");
            }

            if (p->conflictMask & DBCGROUP) {
                UUSendZ(s, " Group");
            }

            if (p->conflictMask & DBCJAVASCRIPT) {
                UUSendZ(s, " JavaScript");
            }

            UUSendZ(s, "</a><br />\n");
        }
        UUSendZ(s, "</p>\n");
    }

    FreeDatabaseConflicts(&dbc);

    AdminFooter(t, 0);
}
