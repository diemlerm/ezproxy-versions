#ifndef __UUXML_H__
#define __UUXML_H__

#include "common.h"

#define XMLSEC_CRYPTO_OPENSSL
#define LIBXML_STATIC
#define XMLSEC_STATIC
#define XMLSEC_NO_CRYPTO_DYNAMIC_LOADING

#include <libxml/globals.h>
#include <libxml/xmlmemory.h>
#include <libxml/xpathInternals.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>
#include <libxml/HTMLparser.h>

#ifndef LIBXML_THREAD_ENABLED
#error LIBXML not compiled with thread support!
#endif

char *UUxmlNodeName(xmlNodePtr node);
char *UUxmlXPathTypeToString(xmlXPathObjectType type);
xmlNodePtr UUxmlFindChild(xmlNodePtr parent, char *childName);
xmlChar *UUxmlSimpleGetContent(xmlDocPtr doc, xmlXPathContextPtr xpathCtx, char *tag);
xmlChar *UUxmlSimpleGetContentSnprintf(xmlDocPtr doc,
                                       xmlXPathContextPtr xpathCtx,
                                       char *buffer,
                                       int bufferSize,
                                       char *format,
                                       ...);
xmlChar *UUxmlSimpleGetAttribute(xmlDocPtr doc,
                                 xmlXPathContextPtr xpathCtx,
                                 char *tag,
                                 char *attribute);
xmlChar *UUxmlSimpleGetAttributeSnprintf(xmlDocPtr doc,
                                         xmlXPathContextPtr xpathCtx,
                                         char *attribute,
                                         char *buffer,
                                         int bufferSize,
                                         char *format,
                                         ...);
void UUxmlFreeParserCtxtAndDoc(xmlParserCtxtPtr *ctxt);
void UUxmlSecInit(void);
void LogXMLDoc(xmlDocPtr doc, xmlChar *textDoc);
FILE *LogXMLDocGetFileName(char **rfn);
void LogXMLDocLogOnly(xmlDocPtr doc, xmlChar *textDoc);
void WriteXMLDocAsHTML(struct UUSOCKET *s, xmlDocPtr doc);

#endif  // __UUXML_H__
