#define __UUFILE__ "admincpip.c"

#include "common.h"
#include "uustring.h"

#include "admincpip.h"
#include "adminaudit.h"

static void AdminCPIPSendUrl(struct TRANSLATE *t, char *name, char *url, char *oid) {
    struct UUSOCKET *s = &t->ssocket;

    WriteLine(s, "%s=", name);
    SendUrlEncoded(s, t->myUrl);
    SendUrlEncoded(s, "/cpip/");
    SendUrlEncoded(s, url);
    SendUrlEncoded(s, (strchr(url, '?') ? "&" : "?"));
    SendUrlEncoded(s, "secret=");
    SendUrlEncoded(s, cpipSecret);
    WriteLine(s, "&%sOIDlist=%s&", name, oid);
}

static BOOL AdminCPIPCheck(struct TRANSLATE *t, char *secret) {
    if (secret == NULL || cpipSecret == NULL || strcmp(secret, cpipSecret) != 0) {
        HTTPCode(t, 404, 1);
        return 1;
    }

    return 0;
}

void AdminCPIPGetConfigVersion2(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    const int oidIdx = 0;
    const int secretIdx = 1;
    struct FORMFIELD ffs[] = {
        {"oid",    NULL, 0, 0, 64},
        {"secret", NULL, 0, 0, 0 },
        {NULL,     NULL, 0, 0, 0 }
    };

    FindFormFields(query, post, ffs);

    if (AdminCPIPCheck(t, ffs[secretIdx].value))
        return;

    HTMLHeader(t, htmlHeaderOmitCache);

    if (ffs[oidIdx].value == NULL || strcmp(ffs[oidIdx].value, "1.3.6.1.4.1.4409.1.1.1.1") != 0) {
        UUSendZ(s, "error=2\r\n");
        return;
    }

    AdminCPIPSendUrl(t, "authenticate", "identify", "1.3.6.1.4.1.4409.1.1.3.1");
    AdminCPIPSendUrl(t, "lastactive", "lastactive?ezpsid=place", "1.3.6.1.4.1.4409.1.1.5.1");
    AdminCPIPSendUrl(t, "deauthenticate", "deauthenticate?ezpsid=place",
                     "1.3.6.1.4.1.4409.1.1.6.1");

    UUSendZ(s,
            "createonlogin=0&"
            "sendtimeout=false&"
            "desturl_paramname=destURL&"
            "sendcpsession=true&"
            "sendlogin=true&"
            "useSISCredentials=true&"
            "sessionPlaceHolder=place&"
            "error=0\r\n");
}

void AdminCPIPIdentify(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    const int oidIdx = 0;
    const int uidIdx = 1;
    const int sidIdx = 2;
    const int loginIdx = 3;
    const int secretIdx = 4;
    struct FORMFIELD ffs[] = {
        {"oid",    NULL, 0, 0, 64},
        {"uid",    NULL, 0, 0, 64},
        {"sid",    NULL, 0, 0, 64},
        {"login",  NULL, 0, 0, 64},
        {"secret", NULL, 0, 0, 0 },
        {NULL,     NULL, 0, 0, 0 }
    };
    char *oid, *uid, *sid, *login;
    struct SESSION *e = NULL;

    FindFormFields(query, post, ffs);

    if (AdminCPIPCheck(t, ffs[secretIdx].value))
        return;

    oid = ffs[oidIdx].value;
    uid = ffs[uidIdx].value;
    sid = ffs[sidIdx].value;
    login = ffs[loginIdx].value;

    HTMLHeader(t, htmlHeaderOmitCache);

    if (sid == NULL || *sid == 0) {
        UUSendZ(s, "error=2&status=sid+missing\r\n");
        return;
    }
    if (uid == NULL || *uid == 0) {
        UUSendZ(s, "error=2&status=uid+missing\r\n");
        return;
    }

    e = FindSessionByCpipSid(sid);
    if (e == NULL) {
        e = StartSession(login, 0, NULL);
        if (e == NULL) {
            UUSendZ(s, "error=2\r\n");
            return;
        }
        AuditEventLogin(t, AUDITLOGINSUCCESS, login, e, NULL, "CPIP");
        ReadConfigSetString(&e->cpipSid, sid);
        GroupMaskDefault(e->gm);
    } else
        UpdateUsername(t, e, login, "CPIP");
    e->sin_addr = t->sin_addr;

    MergeSessionVariables(t, e);

    UUSendZ(s, "pickup=");
    SendUrlEncoded(s, t->myUrl);
    SendUrlEncoded(s, "/connect?session=");
    UUSendZ(s, (e->notify ? "u" : "t"));
    SendUrlEncoded(s, e->connectKey);
    UUSendZ(s, "&");

    WriteLine(s, "extsession=%s&error=0\r\n", e->key);
}

void AdminCPIPPickup(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    const int oidIdx = 0;
    const int uidIdx = 1;
    const int sidIdx = 2;
    const int destUrlIdx = 3;
    const int secretIdx = 4;
    struct FORMFIELD ffs[] = {
        {"oid",     NULL, 0, 0, 64  },
        {"uid",     NULL, 0, 0, 64  },
        {"sid",     NULL, 0, 0, 64  },
        {"destURL", NULL, 0, 0, 1024},
        {"secret",  NULL, 0, 0, 0   },
        {NULL,      NULL, 0, 0, 0   }
    };
    char *oid, *uid, *sid, *destUrl;
    struct SESSION *e = NULL;

    FindFormFields(query, post, ffs);

    if (AdminCPIPCheck(t, ffs[secretIdx].value))
        return;

    oid = ffs[oidIdx].value;
    uid = ffs[uidIdx].value;
    sid = ffs[sidIdx].value;
    destUrl = ffs[destUrlIdx].value;

    HTMLHeader(t, htmlHeaderOmitCache);

    if (sid == NULL || *sid == 0) {
        UUSendZ(s, "error=2&status=sid+missing\r\n");
        return;
    }
    if (uid == NULL || *uid == 0) {
        UUSendZ(s, "error=2&status=uid+missing\r\n");
        return;
    }

    e = FindSessionByCpipSid(sid);
    if (e == NULL) {
        e = StartSession(uid, 0, NULL);
        if (e == NULL) {
            UUSendZ(s, "error=2\r\n");
            return;
        }
        AuditEventLogin(t, AUDITLOGINSUCCESS, uid, e, NULL, "CPIP");
        ReadConfigSetString(&e->cpipSid, sid);
    } else
        UpdateUsername(t, e, uid, "CPIP");
    e->sin_addr = t->sin_addr;

    MergeSessionVariables(t, e);

    WriteLine(s, "extsession=%s&error=0\r\n", e->key);
}

void AdminCPIPLastActive(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    const int oidIdx = 0;
    const int sidIdx = 1;
    const int secretIdx = 2;
    struct FORMFIELD ffs[] = {
        {"oid",    NULL, 0, 0, 64},
        {"sid",    NULL, 0, 0, 64},
        {"secret", NULL, 0, 0, 0 },
        {NULL,     NULL, 0, 0, 0 }
    };
    char *oid, *sid;
    struct SESSION *e = NULL;
    struct tm *tm, rtm;

    FindFormFields(query, post, ffs);

    if (AdminCPIPCheck(t, ffs[secretIdx].value))
        return;

    oid = ffs[oidIdx].value;
    sid = ffs[sidIdx].value;

    HTMLHeader(t, htmlHeaderOmitCache);

    if (sid == NULL || *sid == 0) {
        UUSendZ(s, "error=2&status=sid+missing\r\n");
        return;
    }

    e = FindSessionByCpipSid(sid);
    if (e == NULL) {
        UUSendZ(s, "error=1\r\n");
        return;
    }

    if (e->accessed) {
        tm = UUgmtime_r(&e->accessed, &rtm);
        if (tm) {
            WriteLine(s, "time=%04d%02d%02d%02d%02d%02d&error=0\r\n", tm->tm_year + 1900,
                      tm->tm_mon + 1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
            return;
        }
    }

    UUSendZ(s, "time=0&error=0\r\n");
}

void AdminCPIPDeauthenticate(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    const int oidIdx = 0;
    const int sidIdx = 1;
    const int secretIdx = 2;
    struct FORMFIELD ffs[] = {
        {"oid",    NULL, 0, 0, 64},
        {"sid",    NULL, 0, 0, 64},
        {"secret", NULL, 0, 0, 0 },
        {NULL,     NULL, 0, 0, 0 }
    };
    char *oid, *sid;
    struct SESSION *e = NULL;

    FindFormFields(query, post, ffs);

    if (AdminCPIPCheck(t, ffs[secretIdx].value))
        return;

    oid = ffs[oidIdx].value;
    sid = ffs[sidIdx].value;

    HTMLHeader(t, htmlHeaderOmitCache);

    if (sid == NULL || *sid == 0) {
        UUSendZ(s, "error=2&status=sid+missing\r\n");
        return;
    }

    e = FindSessionByCpipSid(sid);
    if (e == NULL) {
        UUSendZ(s, "error=1\r\n");
        return;
    }

    StopSession(e, "CPIP deauthenticate");
    UUSendZ(s, "error=0\r\n");
}
