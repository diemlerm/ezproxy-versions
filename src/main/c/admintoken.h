#ifndef __ADMINTOKEN_H__
#define __ADMINTOKEN_H__

#include "common.h"

char *DecryptTokensTitle(void);
void AdminToken(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINTOKEN_H__
