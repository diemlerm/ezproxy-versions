/**
 * /reboot admin URL is an undocumented feature to reboot the local system.
 *
 * The /reboot admin URL is only available if config.txt contains
 * "Option Reboot".  If this option does exist, this URL can be used
 * to reboot the operating system.
 */

#define __UUFILE__ "adminreboot.c"

#include "common.h"
#include "uustring.h"

#include "adminreboot.h"

#ifdef WIN32
static void RebootWindows(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket;
    HANDLE hToken;         // handle to process token
    TOKEN_PRIVILEGES tkp;  // pointer to token structure
    BOOL rc;

    // Get the current process token handle so we can get shutdown
    // privilege.

    if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)) {
        WriteLine(s, "OpenProcessToken failed %d", GetLastError());
        return;
    }

    // Get the LUID for shutdown privilege.

    LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid);

    tkp.PrivilegeCount = 1;  // one privilege to set
    tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

    // Get shutdown privilege for this process.

    AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0);

    if (GetLastError() != ERROR_SUCCESS) {
        WriteLine(s, "AdjustTokenPrivileges enable failed %d", GetLastError());
        return;
    }

    UUSendZ(s, "Reboot initiated\n");
    rc = InitiateSystemShutdown(NULL, NULL, 0, TRUE, TRUE);
    if (rc == 0) {
        WriteLine(s, "<br>Reboot request failed %d\n", GetLastError());
    }
}
#endif

void AdminReboot(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;

    const int ARCONFIRM = 0;
    const int ARPID = 1;

    struct FORMFIELD ffs[] = {
        {"confirm", NULL, 0, 0, 10},
        {"pid", NULL, 0, 0, 32},
        {NULL, NULL, 0, 0}
    };
    BOOL confirmed;

    if (!optionReboot) {
        HTTPCode(t, 404, 1);
        return;
    }

    FindFormFields(query, post, ffs);

    confirmed = ffs[ARCONFIRM].value && stricmp(ffs[ARCONFIRM].value, "reboot") == 0 &&
                ffs[ARPID].value && atoi(ffs[ARPID].value) == guardian->cpid;

    if (t->session == NULL || (t->session)->priv == 0) {
        AdminUnauthorized(t);
        return;
    }

    AdminHeader(t, 0, "Reboot");

#ifndef WIN32
    UUSendZ(s, "This option is only available under Windows.\n");
#else

    if (confirmed) {
        UUSendZ(s, EZPROXYNAMEPROPER " will now attempt to reboot Windows");
        AdminFooter(t, 0);
        UUFlush(s);
        SaveHosts();
        Log("Administrative user requested /reboot");
        RebootWindows(t);
        return;
    }

    UUSendZ(s, "<p>You have requested that " EZPROXYNAMEPROPER " reboot this system.</p>\n");
    UUSendZ(s, "<p></p><form action='/reboot' method='post'>\n");
    WriteLine(s, "<input type='hidden' name='pid' value='%d'>\n", guardian->cpid);
    UUSendZ(s, "If you really want " EZPROXYNAMEPROPER
               " to reboot this system, <label>type REBOOT in this box\n");
    UUSendZ(s, "<input type='text' name='confirm' size='8' maxlength='8'></label> then click\n");
    UUSendZ(s, "<input type='submit' value='here'></form>\n");
    AdminFooter(t, 0);

#endif
}
