#define __UUFILE__ "location.c"

#include "common.h"
#include <maxminddb.h>

BOOL mmdbOpen = FALSE;
MMDB_s mmdb;

#ifndef O_BINARY
#define O_BINARY 0
#endif

char *NextCSV(char *s, char **n) {
    char *p;
    BOOL quote = 0;

    if (s == NULL)
        return NULL;

    if (*s == '"') {
        quote = 1;
        s++;
    }

    for (p = s;; p++) {
        if (*p == 0) {
            *n = NULL;
            break;
        }
        if (*p == '"') {
            if (*(p + 1) == '"') {
                StrCpyOverlap(p, p + 1);
                continue;
            }
            *p = 0;
            quote = 0;
            continue;
        }
        if (*p != ',' || quote)
            continue;
        *p = 0;
        *n = p + 1;
        break;
    }
    return s;
}

BOOL AnyLocations(void) {
    return locationRanges != NULL || mmdbOpen;
}

void InitLocation(struct LOCATION *pLocation) {
    memset(pLocation, 0, sizeof(*pLocation));
}

BOOL MMDB_get_string(char *dst, size_t max, MMDB_entry_s *const start, ...) {
    va_list ap;
    MMDB_entry_data_s entry_data;
    char *buffer = NULL;
    uint32_t len;

    va_start(ap, start);
    int status = MMDB_vget_value(start, &entry_data, ap);
    va_end(ap);

    dst[0] = 0;
    if (status == MMDB_SUCCESS) {
        if (entry_data.has_data && entry_data.type == MMDB_DATA_TYPE_UTF8_STRING) {
            len = UUMIN(entry_data.data_size, max - 1);
            if (len > 0) {
                memcpy(dst, entry_data.utf8_string, len);
            }
            dst[len] = 0;
        }
    }

    return status == MMDB_SUCCESS;
}

BOOL MMDB_get_double(double *dst, MMDB_entry_s *const start, ...) {
    va_list ap;
    MMDB_entry_data_s entry_data;
    char *buffer = NULL;

    va_start(ap, start);
    int status = MMDB_vget_value(start, &entry_data, ap);
    va_end(ap);

    *dst = 0;
    if (status == MMDB_SUCCESS) {
        if (entry_data.has_data && entry_data.type == MMDB_DATA_TYPE_DOUBLE) {
            *dst = entry_data.double_value;
        }
    }

    return status == MMDB_SUCCESS;
}

enum LOCATIONSTATUS FindLocationByGeoIP(struct LOCATION *pLocation,
                                        struct sockaddr_storage *hostIp) {
    int country_id;
    int mmdb_error;

    MMDB_lookup_result_s result =
        MMDB_lookup_sockaddr(&mmdb, (struct sockaddr *)hostIp, &mmdb_error);

    if (mmdb_error == MMDB_SUCCESS && result.found_entry) {
        MMDB_get_string(pLocation->countryCode, sizeof(pLocation->countryCode), &result.entry,
                        "country", "iso_code", NULL);
        MMDB_get_string(pLocation->countryName, sizeof(pLocation->countryName), &result.entry,
                        "country", "names", "en", NULL);
        MMDB_get_string(pLocation->region, sizeof(pLocation->region), &result.entry, "subdivisions",
                        "0", "iso_code", NULL);
        MMDB_get_string(pLocation->regionName, sizeof(pLocation->regionName), &result.entry,
                        "subdivisions", "0", "names", "en", NULL);
        MMDB_get_string(pLocation->city, sizeof(pLocation->city), &result.entry, "city", "names",
                        "en", NULL);
        MMDB_get_double(&pLocation->latitude, &result.entry, "location", "latitude", NULL);
        MMDB_get_double(&pLocation->longitude, &result.entry, "location", "longitude", NULL);
        pLocation->status = locationStatusFound;
    }

    if (pLocation->status == locationStatusNotFound) {
        strcpy(pLocation->countryCode, "--");
        strcpy(pLocation->countryName, "N/A");
    } else if (strcmp(pLocation->countryCode, "--") == 0) {
        pLocation->status = locationStatusNotFound;
    }

    return pLocation->status;
}

enum LOCATIONSTATUS FindLocationByHost(struct LOCATION *pLocation,
                                       struct sockaddr_storage *hostIp) {
    struct LOCATIONRANGE *lr;

    InitLocation(pLocation);
    pLocation->status = locationStatusNotFound;

    for (lr = locationRanges; lr; lr = lr->next) {
        if (compare_ip(hostIp, &lr->start) >= 0 && compare_ip(&lr->stop, hostIp) >= 0) {
            StrCpy3(pLocation->countryCode, (char *)lr->countryCode,
                    sizeof(pLocation->countryCode));
            StrCpy3(pLocation->region, (char *)lr->region, sizeof(pLocation->region));
            StrCpy3(pLocation->city, (char *)lr->city, sizeof(pLocation->city));
            pLocation->latitude = lr->latitude;
            pLocation->longitude = lr->longitude;
            if (strcmp(pLocation->countryCode, "--") != 0)
                pLocation->status = locationStatusFound;
            goto Finished;
        }
    }

    if (!PublicAddress(hostIp)) {
        StrCpy3(pLocation->countryCode, "98", sizeof(pLocation->countryCode));
        StrCpy3(pLocation->countryName, "Private Network", sizeof(pLocation->countryName));
        pLocation->status = locationStatusFound;
        goto Finished;
    }

    if (mmdbOpen) {
        FindLocationByGeoIP(pLocation, hostIp);
        goto Finished;
    }

    if (locationRanges == NULL) {
        StrCpy3(pLocation->countryCode, "99", sizeof(pLocation->countryCode));
        StrCpy3(pLocation->countryName, "No data loaded", sizeof(pLocation->countryName));
    }

Finished:
    if (pLocation->countryCode[0] == 0)
        StrCpy3(pLocation->countryCode, "97", sizeof(pLocation->countryCode));

    if (pLocation->countryName[0] == 0)
        StrCpy3(pLocation->countryName, "Unknown", sizeof(pLocation->countryName));

    return pLocation->status;
}

enum LOCATIONSTATUS FindLocationByNetwork(struct LOCATION *pLocation,
                                          struct sockaddr_storage *netIp) {
    return FindLocationByHost(pLocation, netIp);
}

enum LOCATIONSTATUS FindLocationByASCIIIP(struct LOCATION *pLocation, char *asciiIp) {
    struct sockaddr_storage sa;
    init_storage(&sa, 0, 0, 0);
    inet_pton_st2(asciiIp, &sa);
    return FindLocationByNetwork(pLocation, &sa);
}

static void ShowIP(char *first, ...) {
    va_list ap;
    char *p;
    struct LOCATION location;

    va_start(ap, first);
    for (p = first; p; p = va_arg(ap, char *)) {
        FindLocationByASCIIIP(&location, p);
        Log("%s is %s %s\n", p, location.countryCode, location.countryName);
    }
    va_end(ap);
}

BOOL ConvertLocation(char *gzfile, char *nongzfile) {
    gzFile fi = NULL;
    int fo = -1;
    int result;
    char buffer[32768];
    int retResult = 0;

    Log("Decompressing location file %s to %s", gzfile, nongzfile);

    fi = gzopen(gzfile, "rb");
    if (fi == NULL) {
        Log("Location unable to open %s: %d", gzfile, errno);
        goto Cleanup;
    }

    fo = open(nongzfile, O_CREAT | O_WRONLY | O_TRUNC | O_BINARY, defaultFileMode);
    if (fo < 0) {
        Log("Location unable to create %s: %d", nongzfile, errno);
        goto Cleanup;
    }

    for (;;) {
        result = gzread(fi, buffer, sizeof(buffer));
        if (result == 0)
            break;
        if (result < 0) {
            Log("Location failed reading %s: %d", gzfile, errno);
            goto Cleanup;
        }
        if (write(fo, buffer, result) != result) {
            Log("Location failed creating %s: %d", nongzfile, errno);
            goto Cleanup;
        }
    }

    retResult = 1;

Cleanup:
    if (fi) {
        gzclose(fi);
        fi = NULL;
    }

    if (fo >= 0) {
        close(fo);
        if (debugLevel > 19) {
            Log("%" SOCKET_FORMAT " Closed ", fo);
        }
        fo = -1;
    }

    return retResult;
}

static void dump_metadata(MMDB_s *mmdb) {
    const char *meta_dump =
        "\n"
        "  Database metadata\n"
        "    Node count:    %i\n"
        "    Record size:   %i bits\n"
        "    IP version:    IPv%i\n"
        "    Binary format: %i.%i\n"
        "    Build epoch:   %llu (%s)\n"
        "    Type:          %s\n"
        "    Languages:     %s\n";

    size_t i;
    char date[40];
    char languages[2048] = {0};
    const time_t epoch = (const time_t)mmdb->metadata.build_epoch;
    strftime(date, 40, "%F %T UTC", gmtime(&epoch));

    for (i = 0; i < mmdb->metadata.languages.count; i++) {
        if (languages[0]) {
            strcat(languages, " ");
        }
        strcat(languages, mmdb->metadata.languages.names[i]);
    }

    Log(meta_dump, mmdb->metadata.node_count, mmdb->metadata.record_size, mmdb->metadata.ip_version,
        mmdb->metadata.binary_format_major_version, mmdb->metadata.binary_format_minor_version,
        mmdb->metadata.build_epoch, date, mmdb->metadata.database_type, languages);

    Log("    Description:\n");
    for (i = 0; i < mmdb->metadata.description.count; i++) {
        Log("      %s:   %s\n", mmdb->metadata.description.descriptions[i]->language,
            mmdb->metadata.description.descriptions[i]->description);
    }
}

BOOL LoadLocation(char *file) {
    // struct stat gzstat, nongzstat;
    char *gzfile = NULL;
    char *nongzfile = NULL;
    int retResult = 0;
    BOOL converted = 0;
    int f;

    if (mmdbOpen) {
        Log("Location -File may only appear once");
        goto Cleanup;
    }

    f = SBOPEN(file);
    if (f < 0) {
        Log("Unable to open location file %s: %s", file, ErrorMessage(errno));
        return 0;
    }
    close(f);

    int status = MMDB_open(file, MMDB_MODE_MMAP, &mmdb);

    if (status != MMDB_SUCCESS) {
        Log("Location file is not a GeoIP2 mmdb file: %s", file);
        goto Cleanup;
    }

    mmdbOpen = TRUE;
    retResult = 1;

    /*
    dump_metadata(&mmdb);
    ShowIP("2600:1409:0:598::2add", "68.98.212.84", "192.168.1.3", "24.249.162.194", "2.6.190.56",
    "2.6.190.55", "2.6.190.63", "2.6.190.64", "2.255.255.255", "3.0.0.0", "222.251.0.0",
    "222.252.0.0", "222.255.255.255", NULL);
    */
    goto Cleanup;

Cleanup:
    FreeThenNull(nongzfile);

    return retResult;
}

enum LOCATIONSTATUS FindLocationByTranslateNoCache(struct TRANSLATE *t) {
    return FindLocationByNetwork(&t->location, &t->sin_addr);
}

enum LOCATIONSTATUS FindLocationByTranslate(struct TRANSLATE *t) {
    if (t->location.status == locationStatusInit)
        FindLocationByTranslateNoCache(t);
    return t->location.status;
}
