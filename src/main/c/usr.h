#ifndef __USR_H__
#define __USR_H__

#include "common.h"

#define STATEMENT_RESULT_TYPE int
#define STATEMENT_RESULT_SUCCESS ((int)0)
#define STATEMENT_RESULT_FAILURE ((int)1)

#define USRLASTREFUSED 1
#define USRLASTINVALID 2
#define USRLASTEXPIRED 4
#define USRPREURL 8
#define USRUSERMAYBENULL 16

#define MAXUSERLEN 128
#define MAXUSERINFOVARS 10
#define MAXUSERINFOVARLEN 128
#define MAXRENEWLEN 4

struct USERINFO {
    char *user; /* I */
    char *pass; /* I */
    char *url;
    char *renew;
    char *privateComputer;
    char *ticket;     /* I */
    char *newPass;    /* I */
    char *verifyPass; /* I */
    char *auth;       /* I */
    char *cgi;        /* O */
    char *qcgi;       /* O */
    char *wayf;       /* O */
    struct SHIBBOLETHSITE *wayfss;
    char *wayfEntityID;
    char logUser[MAXUSERLEN];
    char altUser[MAXUSERLEN]; /* This is used to store value if login:user used to store a value */
    char *accountUser;
    char *domain;
    char *pin;
    char *context;
    char *saveReferer;
    char *saveLanguage;
    char *groupNumber;
    char *instNumber;
    char *groupSymbol;
    char *instSymbol;
    struct TRANSLATE *translate;
    struct FILEINCLUSION *fileInclusion;
    GROUPMASK gm;
    GROUPMASK requiredGm;
    struct sockaddr_storage sourceIpInterface;
    struct sockaddr_storage *pInterface;
    int userLevel; /* O */
    int limit;
    int flags;
    int relogin;
    int lifetime;
    unsigned int sessionFlags;
    /* now and tm are NOT initialized by InitUserInfo;
       use UserInfoNow and UserInfoTm which will initialize and return values
    */
    time_t now;
    struct tm tm;
    BOOL wayfRedirect;
    BOOL wayfDS;
    BOOL ezpedit; /* O */
    BOOL admin;   /* O */
    BOOL asp;
    BOOL skipping;
    BOOL deniedNotified;
    BOOL denied;
    BOOL expiredSeen;
    BOOL logup;
    BOOL debug;
    BOOL urlReference;
    BOOL passFromQueryString;
    BOOL logToSSocket;
    BOOL spuRedirect;
    BOOL forceDebug;
    BOOL logUserAllowed;
    BOOL isIntruder;
    BOOL pseudoFile; /* not used */
    BOOL csrfValid;
    enum AUTOLOGINBY autoLoginBy;
    enum FINDUSERRESULT result;
    enum ACCOUNTFINDRESULT accountResult;
    char logPrefix[129];
    char menu[MAXMENU];
    char login[MAXMENU];
    char loginBu[MAXMENU];
    char loginBanner[MAXMENU];
    char vars[MAXUSERINFOVARS][MAXUSERINFOVARLEN];
    char comment[81];
    char sessionProxyAuth[128];
    char logUserReferer[MAXUSERLEN];
    char spuEdited[MAXURL];
    int connectionsTries;
    int connectionsTimeout;
    BOOL (*authIsReadOnly)
    (struct TRANSLATE *t, struct USERINFO *uip, void *context, const char *var, const char *index);
    void (*authSetValue)(struct TRANSLATE *t,
                         struct USERINFO *uip,
                         void *context,
                         const char *var,
                         const char *index,
                         const char *val);
    char *(*authSetAggregateValue)(struct TRANSLATE *t,
                                   struct USERINFO *uip,
                                   void *context,
                                   const char *var,
                                   const char *val);
    char *(*authGetValue)(struct TRANSLATE *t,
                          struct USERINFO *uip,
                          void *context,
                          const char *var,
                          const char *index);
    int (*authAggregateTest)(struct TRANSLATE *t,
                             struct USERINFO *uip,
                             void *context,
                             const char *var,
                             const char *testVal,
                             enum AGGREGATETESTOPTIONS ato,
                             struct VARIABLES *rev);
};

struct USRATTRIBUTE {
    struct USRATTRIBUTE *next;
    char *name;
    char *value;
};

#define USRDIRECTIVE_FLAG_PARM_REST_OF_LINE (0)
#define USRDIRECTIVE_FLAG_EVAL_1_PARM (1)
#define USRDIRECTIVE_FLAG_EVAL_N_PARMS (2)
#define USRDIRECTIVE_FLAG_EVAL_FALLBACK (4)
struct USRDIRECTIVE {
    char *directive;
    int (*func)(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
    int flags;
    int (*funcn)(struct TRANSLATE *t, struct USERINFO *uip, void *context, char **argList);
};

/**
 * @param t
 * @param title The name of the authentication mechanism being used
 * @param uds
 * @param context Context set by the authentication block
 * @param uip User information
 * @param file
 * @param f
 * @param frb
 * @param sockadd_in
 * @param resetFunc This is a reset command to be call on context.  Only LDAP currently uses this.
 */
enum FINDUSERRESULT UsrHandler(struct TRANSLATE *t,
                               char *title,
                               struct USRDIRECTIVE *uds,
                               void *context,
                               struct USERINFO *uip,
                               char *file,
                               int f,
                               struct FILEREADLINEBUFFER *frb,
                               struct sockaddr_storage *pInterface,
                               void (*resetFunc)());

void InitUserInfo(struct TRANSLATE *t,
                  struct USERINFO *uip,
                  char *user,
                  char *pass,
                  char *url,
                  GROUPMASK gm);

int UsrBanner(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrDeny(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrDocsCustom(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrGroup(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrIfAfter(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrIfAuth(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrIfBefore(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrIfDay(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrIfHeader(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrIfHttp(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrIfHttps(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrIfLanguage(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrIfMonth(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrIfReferer(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrIfTime(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrIfURL(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrIfYear(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrIgnore(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
time_t UserInfoNow(struct USERINFO *uip);
int UsrLog(struct USERINFO *uip, const char *format, ...);
int UsrListing(struct USERINFO *uip, BOOL skipping, const char *format, ...);
int UsrRelogin(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
int UsrSourceIP(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg);
char *UsrTestComparisonOperatorType(char *s, char *compareOperator, char *compareType);
BOOL UsrTestCompare(char *value, char *testValue, char compareOperator, char compareType);
char UsrDispatch(struct TRANSLATE *t,
                 char *title,
                 struct USRDIRECTIVE *uds,
                 void *context,
                 struct USERINFO *uip,
                 char *cmd,
                 size_t cmdLen,
                 char *arg);
BOOL UsrSkipping(struct USERINFO *uip);

#endif  // __USR_H__
