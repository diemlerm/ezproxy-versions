#define __UUFILE__ "telnet.c"

#include "common.h"

#ifdef WIN32
/* io.h, fcntl.h and share.h needed for _sopen in Win32 */
#include <io.h>
#include <fcntl.h>
#include <share.h>
#include <lm.h>
#endif

#include <sys/stat.h>

#include "telnet.h"

int TelnetInit(struct TELNETSTATE *ts) {
    memset(ts, 0, sizeof(*ts));
    ts->tsState = TSDATA;
    return 0;
}

int TelnetRecv(struct TELNETSTATE *ts, struct UUSOCKET *s, char *msg, size_t len) {
    int status;
    int remainIn, remainOut;
    unsigned char *p, *q;
    unsigned char buffer[32];
    unsigned char *umsg = (unsigned char *)msg;
    remainOut = len;

    if (ts->hitEof)
        return 0;

    q = umsg;
    remainOut = len;

    for (;;) {
        UUFlush(s);
        if (q != umsg)
            break;

        status = UURecv(s, ts->buffer, UUMIN(remainOut, sizeof(ts->buffer)), 0);

        if (status <= 0) {
            ts->hitEof = 1;
            break;
        }

        remainIn = status;
        for (p = ts->buffer; remainIn > 0; p++, remainIn--) {
            if (ts->tsState == TSDATA) {
                if (*p == TCIAC) {
                    ts->tsState = TSIAC;
                } else {
                    /* Filter out NULL characters, such as have been seen from VTLS SIP server */
                    if (*p) {
                        *q++ = *p;
                        remainOut--;
                    }
                }
                continue;
            }

            if (ts->tsState == TSIAC) {
                if (*p == TCIAC) {
                    *q++ = TCIAC;
                    remainOut--;
                    ts->tsState = TSDATA;
                    continue;
                }
                if (*p == TCNOP) {
                    ts->tsState = TSDATA;
                    continue;
                }
                if (*p == TCWILL || *p == TCWONT) {
                    ts->tsSubState = *p;
                    ts->tsState = TSWOPT;
                    continue;
                }
                if (*p == TCDO || *p == TCDONT) {
                    ts->tsSubState = *p;
                    ts->tsState = TSDOPT;
                    continue;
                }
            }

            if (ts->tsState == TSWOPT) {
                buffer[0] = TCIAC;
                buffer[1] = TCDONT;
                buffer[2] = *p;
                UUSend(s, buffer, 3, 0);
                ts->tsState = TSDATA;
                continue;
            }

            if (ts->tsState == TSDOPT) {
                buffer[0] = TCIAC;
                buffer[1] = TCWONT;
                buffer[2] = *p;
                UUSend(s, buffer, 3, 0);
                ts->tsState = TSDATA;
            }

            if (ts->tsState == TSSUBNEG) {
                if (*p == TSIAC)
                    ts->tsState = TSSUBIAC;
                continue;
            }

            if (ts->tsState == TSSUBIAC) {
                if (*p == TSSUBIAC)
                    ts->tsState = TSDATA;
                else
                    ts->tsState = TSSUBNEG;
                continue;
            }
        }
    }

    return q - umsg;
}

void TelnetSetNoEcho(struct TELNETSTATE *ts, struct UUSOCKET *s) {
    unsigned char noEcho[4] = {TCIAC, TCDONT, TOECHO, 0};

    UUSend(s, noEcho, sizeof(noEcho) - 1, 0);
}

int TelnetSend(struct TELNETSTATE *ts, struct UUSOCKET *s, char *msg, size_t len) {
    /* should encode TSIAC, but we don't use it now, so... */
    return UUSend(s, msg, len, 0);
}

int TelnetSendString(struct TELNETSTATE *ts, struct UUSOCKET *s, char *str) {
    return TelnetSend(ts, s, str, strlen(str));
}

BOOL TelnetRecvLine(struct TELNETSTATE *ts, struct UUSOCKET *s, char *buffer, int max) {
    int rc;
    BOOL any;
    char c;

    any = 0;
    /* Leave room for the final null by decrementing upfront */
    max--;
    for (;;) {
        rc = TelnetRecv(ts, s, &c, 1);
        if (rc != 1)
            break;
        any = 1;
        if (ts->lastCr) {
            ts->lastCr = 0;
            if (c == 10)
                continue;
        }
        if (c == 13) {
            ts->lastCr = 1;
            break;
        }
        if (c == 10)
            break;
        /* Since readline was called, we assume the caller intended us to read all the way to
         * newline */
        /* But, only store incoming chars if there is still buffer space left */
        if (max > 0) {
            *buffer++ = c;
            max--;
        }
    }
    *buffer = 0;

    return any;
}
