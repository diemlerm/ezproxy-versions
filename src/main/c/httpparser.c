#define __UUFILE__ "httpparser.c"

#include "httpparser.h"

int HTTPHeaderParse(struct UUSOCKET *socket, int *responseCode) {
    // the UU stuff is a mess, no easy producer/consumer api
    // so just read 1 line at a time to be lazy
    char ch;

    // circular buffer
    int bMax = 4;
    int bPos = 0;
    char buffer[bMax + 1];
    char orderedBuffer[bMax + 1];
    buffer[bMax + 1] = '\0';
    orderedBuffer[bMax + 1] = '\0';

    // store a copy of each line
    int lPos = 0;
    int lineMax = 250;
    char line[lineMax];
    char bigBuffer[600];

    int contentLength = -1;

    for (; UURecv(socket, &ch, 1, 0) == 1 && !UUSocketEof(socket); bPos++, lPos++) {
        bigBuffer[lPos] = ch;
        if (lPos > lineMax) {
            // printf("%s", bigBuffer);
            lPos = 0;
            if (debugLevel)
                Log("Max header line length exceeded.");
        }

        // ch is the character
        buffer[bPos] = ch;

        // TODO: add these characters to some buffer
        // to create the header string
        line[lPos] = ch;

        // 10 is '\n'
        if (ch == 10) {
            line[++lPos] = '\0';

            // skip ahead to ":"
            char *ptr = &line[0];
            while (*ptr != ':' && *ptr != '\0' && ptr < &line[lineMax])
                ptr++;

            *ptr = '\0';

            // check for the content length header
            char *temp = SkipWS(line);

            // read content length
            if (strnicmp(temp, "Content-Length", 14) == 0) {
                contentLength = atoi((SkipWS(++ptr)));
            } else if (strnicmp(temp, "HTTP/", 5) == 0) {
                int major = -1;
                int minor = -1;

                // looks like we have valid major minor format
                if (*(temp + 6) == '.') {
                    *(temp + 6) = '\0';
                    major = atoi((temp + 5));
                    minor = atoi((temp + 7));
                }

                // get the response
                *responseCode = atoi(SkipWS(temp + 8));
            }

            // reset the line
            line[0] = '\0';
            lPos = -1;
        }

        // translate our buffer loop into ordered char array
        int i = bPos + 1;
        for (; i < bMax; i++)
            orderedBuffer[i - (bPos + 1)] = buffer[i];
        int j = 0;
        for (; j <= bPos; j++)
            orderedBuffer[i + j - (bPos + 1)] = buffer[j];

        // max sure our buffer has proper ending, a little defensive here
        orderedBuffer[4] = '\0';

        // check if we are at end of header
        if (strcmp("\r\n\r\n", orderedBuffer) == 0)
            break;

        orderedBuffer[2] = '\0';
        if (strcmp("\n\n", orderedBuffer) == 0)
            break;

        // reset buffer to start overwriting buffer
        if (bPos == (bMax - 1))
            bPos = -1;
    }

    return contentLength;
}

void HTTPBodyParse(char *body, const int maxBodySize, int *length, struct UUSOCKET *socket) {
    char ch;
    int pos = 0;

    for (; pos < maxBodySize && !UUSocketEof(socket) && UURecv(socket, &ch, 1, 0) == 1; pos++)
        body[pos] = ch;

    if (*length != pos)
        *length = pos;
}
