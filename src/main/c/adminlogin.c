#define __UUFILE__ "adminlogin.c"

#include "common.h"
#include "uustring.h"
#include "usr.h"

#include "adminlogin.h"
#include "adminaudit.h"
#include "adminfmg.h"
#include "adminpassword.h"
#include "adminunknown.h"
#include "usrfinduser.h"
#include "spur.h"

#include "uuxml.h"
#include "html.h"
#include "saml.h"

BOOL AdminMaybeLogin(struct TRANSLATE *t, struct DISPATCH *dp, char *query, char *post) {
    char *queryCopy = NULL;
    char *postCopy = NULL;
    BOOL mustLogin = 0;

    if (query)
        if (!(queryCopy = strdup(query)))
            PANIC;

    if (post)
        if (!(postCopy = strdup(post)))
            PANIC;

    t->maybeLogin = TRUE;
    (dp->func)(t, queryCopy && *queryCopy == '?' ? queryCopy + 1 : queryCopy, postCopy);
    FreeThenNull(queryCopy);
    FreeThenNull(postCopy);
    if (t->maybeLogin) {
        mustLogin = 1;
        t->maybeLogin = FALSE;
    }

    return mustLogin;
}

BOOL AdminLoginBanner(struct TRANSLATE *t, char *url, BOOL isConnect) {
    struct SESSION *e;

    if (isConnect && (e = t->session) && e->loginBanner && FileExists(e->loginBanner)) {
        HTMLHeader(t, optionLoginNoCache ? htmlHeaderNoCache : htmlHeaderMaxAgeCache);
        SendEditedFile(t, NULL, e->loginBanner, 1, url, 1, NULL);
        return 0;
    }
    return 1;
}

void AdminForceHTTPSLogin(struct TRANSLATE *t, char *url, char *auth, char *tag) {
    struct UUSOCKET *s = &t->ssocket;
    int parts = (url ? 1 : 0) + (auth ? 1 : 0) + (tag ? 1 : 0);
    int parts2;
    HTTPCode(t, 302, 0);
    NoCacheHeaders(s);
    if (t->version) {
        WriteLine(s, "Location: %s/login", myUrlHttps);
        parts2 = parts;
        if (parts2) {
            UUSendZ(s, "?");
            if (auth) {
                UUSendZ(s, "auth=");
                SendUrlEncoded(s, auth);
                parts2--;
                if (parts2 > 0)
                    UUSendZ(s, "&");
            }
            if (tag) {
                UUSendZ(s, "tag=");
                SendUrlEncoded(s, tag);
                parts2--;
                if (parts2 > 0)
                    UUSendZ(s, "&");
            }
            if (url) {
                UUSendZ(s, "qurl=");
                SendUrlEncoded(s, url);
            }
        }
        UUSendCRLF(s);
        HeaderToBody(t);
    }

    WriteLine(s, "To access this site, go <a href='%s/login", myUrlHttps);
    parts2 = parts;
    if (parts2) {
        UUSendZ(s, "?");
        if (auth) {
            UUSendZ(s, "auth=");
            SendUrlEncoded(s, auth);
            parts2--;
            if (parts2 > 0)
                UUSendZ(s, "&");
        }
        if (tag) {
            UUSendZ(s, "tag=");
            SendUrlEncoded(s, tag);
            parts2--;
            if (parts2 > 0)
                UUSendZ(s, "&");
        }
        if (url) {
            UUSendZ(s, "qurl=");
            SendUrlEncoded(s, url);
        }
    }
    UUSendZ(s, "'>here</a>\r\n");
}

BOOL AdminCGILogin(struct TRANSLATE *t, struct USERINFO *uip, char *url, BOOL logup) {
    char *p;
    char *q;
    char *pe;
    char *wayf;
    struct UUSOCKET *s = &t->ssocket;
    int result = 0;
    struct SESSION *e = t->session;

    if (uip->cgi == NULL && uip->qcgi == NULL && uip->wayf == NULL)
        goto Finished;

    if (uip->wayf) {
        if (logup && e && e->loggedinShibboleth)
            goto Finished;

        if (url == NULL || *url == 0)
            url = "menu";

        if (*uip->wayf && uip->wayfss) {
            char *ur = URLReference(url, t->logTag);
            SAMLAuthnRequest(t, BAD_CAST uip->wayf, ur, uip->wayfss, uip->wayfRedirect);
            FreeThenNull(ur);
            result = 1;
            goto Finished;
        }

        wayf = uip->wayf;
        if (*wayf == 0)
            wayf = shibbolethWAYF;

        HTTPCode(t, 302, 0);
        NoCacheHeaders(s);
        if (t->version) {
            UUSendZ(s, "Location: ");
            if (uip->wayfDS)
                SAMLSendURLforDS(s, url, wayf, uip->wayfEntityID, t->logTag);
            else
                ShibbolethSendURLwithWAYF(s, url, wayf, uip->wayfEntityID, t->logTag);
            UUSendCRLF(s);
            HeaderToBody(t);
        }

        UUSendZ(s, TAG_DOCTYPE_HTML_HEAD);
        UUSendZ(s, "</head>\n<body>\n");
        UUSendZ(s, "To access this document, wait a moment or click <a href='");
        if (uip->wayfDS)
            SAMLSendURLforDS(s, url, wayf, uip->wayfEntityID, t->logTag);
        else
            ShibbolethSendURLwithWAYF(s, url, wayf, uip->wayfEntityID, t->logTag);
        UUSendZ(s, "'>here</a> to continue\n");
        UUSendZ(s, "</body></html>\n");
        result = 1;
        goto Finished;
    }

    if (uip->cgi == NULL || strchr(uip->cgi, '^') == NULL) {
        strcpy(t->buffer, (uip->qcgi ? uip->qcgi : uip->cgi));
        p = strchr(t->buffer, 0);
        if (logup && t->session) {
            sprintf(p, "logup=%s&", (t->session)->key);
            p = strchr(p, 0);
        }
        sprintf(p, "%surl=", (uip->qcgi ? "q" : ""));
        if (url) {
            q = strchr(t->buffer, 0);
            strcat(t->buffer, url);
            if (uip->qcgi)
                PoundFix(q, sizeof(t->buffer) - (q - t->buffer), 1);
        }
    } else {
        size_t len;

        if (logup && StrIStr(uip->cgi, "^L") == NULL)
            goto Finished;

        for (p = uip->cgi, q = t->buffer, pe = t->buffer + UUMIN(MAXURL, sizeof(t->buffer)) - 1;
             *p && q < pe;) {
            if (*p != '^') {
                *q++ = *p++;
                continue;
            }
            p++;
            switch (*p) {
            case 'a':
            case 'A':
                if (uip->auth && *uip->auth) {
                    *q = 0;
                    AddEncodedField(q, uip->auth, pe);
                    q = strchr(q, 0);
                }
                break;

            case 'u':
            case 'U':
                if (t->logTag) {
                    *q = 0;
                    AddEncodedField(q, t->logTag, pe);
                    AddEncodedField(q, "$", pe);
                    q = strchr(q, 0);
                }
                if (url && *url) {
                    *q = 0;
                    AddEncodedField(q, url, pe);
                    q = strchr(q, 0);
                }
                break;

            case 'v':
            case 'V':
                if (url && *url) {
                    if (t->logTag) {
                        if ((size_t)(pe - q) < strlen(t->logTag) + 2) {
                            strcpy(q, t->logTag);
                            strcat(q, "$");
                            q = strchr(q, 0);
                        }
                    }
                    len = UUMIN(strlen(url), (size_t)(pe - q));
                    memcpy(q, url, len);
                    q += len;
                }
                break;

            case 'r':
            case 'R':
                if (url && *url) {
                    char *ur = URLReference(url, t->logTag);
                    *q = 0;
                    AddEncodedField(q, ur, pe);
                    FreeThenNull(ur);
                    q = strchr(q, 0);
                }
                break;

            case 'l':
            case 'L':
                *q = 0;
                AddEncodedField(q, logup ? "true" : "false", pe);
                q = strchr(q, 0);
                break;

            case 's':
            case 'S':
                if (t->session) {
                    *q = 0;
                    AddEncodedField(q, (t->session)->key, pe);
                    q = strchr(q, 0);
                }
                break;

            default:
                *q++ = *p;
            }
            p++;
        }
        *q = 0;
    }

    LocalRedirect(t, t->buffer, 0, 0, NULL);
    result = 1;

Finished:
    FreeThenNull(uip->cgi);
    FreeThenNull(uip->qcgi);
    FreeThenNull(uip->wayf);
    FreeThenNull(uip->wayfEntityID);

    return result;
}

BOOL AdminLoginReroute(struct TRANSLATE *t, char *url) {
    struct REROUTE *rr;
    int i;
    char *host;
    char *port;
    char *endOfHost;
    char *p;
    char useSsl;
    size_t len;
    char save;
    BOOL result = 0;

    if (reroutes == NULL)
        return 0;

    host = SkipHTTPslashesAt(url, &useSsl);
    ParseHost(host, &port, &endOfHost, 0);
    if (port)
        endOfHost = port;

    if (save = *endOfHost)
        *endOfHost = 0;

    len = endOfHost - host;

    for (i = 0, rr = reroutes; i < cReroutes; i++, rr++) {
        if (rr->domainLen > len)
            continue;
        p = host + len - rr->domainLen;
        if (strnicmp(p, rr->domain, len) != 0)
            continue;
        if (rr->domainLen != len) {
            if (rr->hostOnly)
                continue;
            if (*(p - 1) != '.')
                continue;
        }
        result = 1;
        break;
    }

    if (save)
        *endOfHost = save;

    if (result) {
        struct UUSOCKET *s;

        s = &t->ssocket;

        HTTPCode(t, 302, 0);
        NoCacheHeaders(s);
        if (t->version) {
            UUSendZ(s, "Location: ");
            if (rr->to)
                UUSendZ(s, rr->to);
            if (rr->quote)
                SendUrlEncoded(s, url);
            else
                UUSendZ(s, url);
            UUSendCRLF(s);
            HeaderToBody(t);
        }

        UUSendZ(s, "To access this site, go <a href='");
        if (rr->to)
            UUSendZ(s, rr->to);
        if (rr->quote)
            SendUrlEncoded(s, url);
        else
            UUSendZ(s, url);
        UUSendZ(s, "'>here</a>\r\n");
    }

    return result;
}

void AdminLoginVars(struct SESSION *e, struct USERINFO *uip, BOOL combine) {
    int v;

    for (v = 0; v < MAXSESSIONVARS && v < MAXUSERINFOVARS; v++) {
        if (uip->vars[v][0]) {
            FreeThenNull(e->vars[v]);
            if (!(e->vars[v] = strdup(uip->vars[v])))
                PANIC;
        } else if (combine == 0 && e->vars[v]) {
            char *old = e->vars[v];
            e->vars[v] = NULL;
            FreeThenNull(old);
        }
    }
}

void AdminLoginGetSendField(struct TRANSLATE *t,
                            struct DATABASE *b,
                            char *var,
                            char *val,
                            char *query,
                            BOOL expr) {
    struct UUSOCKET *s = &t->ssocket;
    struct SESSION *e = t->session;
    char *queryVal = NULL;
    char *caret;

    if (val == NULL) {
        queryVal = GetSnipField(query, var, 0, 1);
        if (queryVal == NULL)
            return;
    }

    UUSendZ(s, "<input type='hidden' name='");
    SendObfuscated(s, var, NULL);
    UUSendZ(s, "' value='");
    if (queryVal) {
        SendObfuscated(s, queryVal, NULL);
        FreeThenNull(val);
    } else if (expr) {
        char *exprVal = ExpressionValue(t, NULL, NULL, val);
        SendObfuscated(s, exprVal, NULL);
        FreeThenNull(exprVal);
    } else {
        while (*val) {
            caret = strchr(val, '^');
            if (val != caret)
                SendObfuscated(s, val, caret);
            if (caret == NULL)
                break;
            caret++;
            if (*caret == 0) {
                SendObfuscated(s, "^", NULL);
                break;
            }
            val = caret + 1;
            if (IsDigit(*caret)) {
                int i = *caret - '0';
                if (e && e->vars[i])
                    SendObfuscatedEncrypted(s, e->vars[i], b, *caret);
            } else if (IsAlpha(*caret)) {
                if (*caret == 'i' || *caret == 'I') {
                    char ipBuffer[INET6_ADDRSTRLEN];
                    ipToStr(&t->sin_addr, ipBuffer, sizeof ipBuffer);
                    SendObfuscatedEncrypted(s, ipBuffer, b, 'i');
                }
                if (*caret == 'u' || *caret == 'U') {
                    SendObfuscatedEncrypted(s, e->logUserBrief, b, 'u');
                }
            } else {
                SendObfuscated(s, caret, caret + 1);
            }
        }
    }
    UUSendZ(s, "'>");
    if (optionSuppressObfuscation)
        UUSendZ(s, "\n");
}

BOOL AdminLoginGetDynamicHasChild(xmlNodePtr child, char *testName) {
    xmlChar *name = NULL;

    for (; child; child = child->next) {
        if (child->children)
            if (AdminLoginGetDynamicHasChild(child->children, testName))
                return 1;
        if (child->name == NULL || stricmp((char *)child->name, "input") != 0)
            continue;
        if (name = xmlGetProp(child, BAD_CAST "name")) {
            if (WildCompare((char *)name, testName)) {
                xmlFreeThenNull(name);
                return 1;
            }
        }
        FreeThenNull(name);
    }

    return 0;
}

void AdminLoginGetDynamicRecurse(struct TRANSLATE *t,
                                 struct DATABASE *b,
                                 char *query,
                                 xmlNodePtr child,
                                 char *sent) {
    struct FORMVARS *p;
    BOOL release;
    int count;
    int i;
    BOOL root = 0;

    if (sent == NULL && b->formVars) {
        root = 1;
        for (p = b->formVars, count = 0; p; p = p->next)
            count++;
        if (count)
            if (!(sent = calloc(1, count)))
                PANIC;
    }

    for (; child; child = child->next) {
        if (child->children)
            AdminLoginGetDynamicRecurse(t, b, query, child->children, sent);
        if (child->name == NULL)
            continue;
        if (stricmp((char *)child->name, "input") == 0) {
            xmlChar *name = NULL;
            xmlChar *value = NULL;

            if (name = xmlGetProp(child, BAD_CAST "name")) {
                release = 1;
                for (p = b->formVars, i = 0; p; p = p->next, i++) {
                    if (p->varRE) {
                        if (xmlRegexpExec(p->varRE, BAD_CAST name) != 1)
                            continue;
                    } else {
                        if (strcmp(p->var, (char *)name) != 0)
                            continue;
                    }
                    if (p->suppress || sent[i])
                        release = 0;
                    break;
                }

                if (release) {
                    char *sendVal = NULL;

                    if (p) {
                        sent[i] = 1;
                        if (p->val)
                            sendVal = p->val;
                    }

                    if (sendVal == NULL) {
                        value = xmlGetProp(child, BAD_CAST "value");
                        sendVal = (char *)value;
                    }

                    AdminLoginGetSendField(t, b, (char *)name, sendVal, query, FALSE);
                }

                /*
                if (strstr((char *) name, "username")) {
                    SendObfuscated(s, "cschah", NULL);
                } else if (strstr((char *) name, "password")) {
                    SendObfuscated(s, "virology", NULL);
                } else if (value = xmlGetProp(child, BAD_CAST "value")) {
                    SendObfuscated(s, (char *) value, NULL);
                }
                */
                xmlFreeThenNull(name);
                xmlFreeThenNull(value);
            }
        }
    }

    /* Any specified fields that haven't already been sent get sent now;
       this should mainly matter for form variables that have
       multiple values
    */

    if (root) {
        for (p = b->formVars, i = 0; p; p = p->next, i++) {
            if (sent[i] == 0 && p->suppress == 0 && p->varRE == NULL) {
                AdminLoginGetSendField(t, b, p->var, p->val, query, p->expr);
                sent[i] = 0;
            }
        }

        FreeThenNull(sent);
    }
}

BOOL AdminLoginGetDynamic(struct TRANSLATE *t,
                          struct DATABASE *b,
                          char *url,
                          char *accessType,
                          char *query) {
    struct UUSOCKET *s = &t->ssocket;
    xmlParserCtxtPtr ctxt = NULL;
    xmlDocPtr doc = NULL; /* the resulting document tree */
    xmlXPathContextPtr xpathCtx = NULL;
    xmlXPathObjectPtr xpathObj = NULL;
    xmlNodeSetPtr nodes = NULL;
    int i;
    xmlNodePtr cur;
    xmlChar *action = NULL;
    xmlChar *method = NULL;
    char *finalURL = NULL;
    char *absoluteAction = NULL;
    BOOL anyForm = 0;

    ctxt = GetHTML(b->getUrl, NULL, NULL, &finalURL, NULL, 0, 0, NULL, NULL, debugLevel);
    if (ctxt == NULL || (doc = ctxt->myDoc) == NULL)
        goto Finished;

    xpathCtx = xmlXPathNewContext(doc);
    xpathObj = xmlXPathEvalExpression(BAD_CAST "//form", xpathCtx);

    if (xpathObj == NULL || (nodes = xpathObj->nodesetval) == NULL)
        goto Finished;

    for (i = 0; i < nodes->nodeNr; i++) {
        if (nodes->nodeTab[i]->type != XML_ELEMENT_NODE)
            continue;

        cur = nodes->nodeTab[i];

        if (b->formSelect && AdminLoginGetDynamicHasChild(cur, b->formSelect) == 0)
            continue;

        action = xmlGetProp(cur, BAD_CAST "action");

        absoluteAction = AbsoluteURL(finalURL, (char *)action ? (char *)action : "");

        anyForm = 1;
        dprintf("Action is %s", absoluteAction);

        method = xmlGetProp(cur, BAD_CAST "method");

        UUSendZ(s, "<form name='EZproxyForm' action='");
        SendQuotedURL(t, absoluteAction);
        UUSendZ(s, "'");

        if (method) {
            UUSendZ(s, " method='");
            SendQuotedURL(t, (char *)method);
            UUSendZ(s, "'");
        }

        UUSendZ(s, ">\n");

        AdminLoginGetDynamicRecurse(t, b, query, cur->children, NULL);

        xmlFreeThenNull(action);
        xmlFreeThenNull(method);

        break;
    }

Finished:
    if (anyForm == 0) {
        UUSendZ(s, "<p>The dynamic form ");
        SendHTMLEncoded(s, b->getName);
        UUSendZ(s,
                " is not working properly.  If this error persists, please report it to "
                "your " EZPROXYNAMEPROPER " administrator.</p>\n");
    }

    if (xpathObj) {
        xmlXPathFreeObject(xpathObj);
        xpathObj = NULL;
    }

    if (xpathCtx) {
        xmlXPathFreeContext(xpathCtx);
        xpathCtx = NULL;
    }

    UUxmlFreeParserCtxtAndDoc(&ctxt);

    FreeThenNull(finalURL);
    FreeThenNull(absoluteAction);

    return anyForm;
}

void AdminLoginGet(struct TRANSLATE *t, char *url, char *accessType) {
    struct UUSOCKET *s = &t->ssocket;
    struct UUSOCKET *r = t->wsocketPtr;
    struct SESSION *e = t->session;
    char *field;
    BOOL lastOK;
    char *validFields[] = {"User-Agent:", "Referer:",         "Date:",           "From:",
                           "Accept:",     "Accept-Language:", "Accept-Charset:", NULL};
    char **vfp;
    char *nf;
    char *referer;
    BOOL sentDomainCookies;
    char *name;
    size_t namelen;
    struct DATABASE *b;
    char *slashQuestion;
    char *slash;
    char *query = NULL;
    int i;
    char *q;
    char *pa;
    char *token = NULL;
    char *tokenSignature = NULL;
    enum IPACCESS ipAccess;
    char *logUrl = NULL;
    char *logMyUrl = myUrlHttp ? myUrlHttp : myUrlHttps;
    char *slashAppend = NULL;
    char *login2Group = NULL;
    char *tg = NULL;

    UUInitSocket(r, INVALID_SOCKET);

    name = StrIStr(url, "/login/");
    if (name == NULL)
        goto NotFound;

    if (!(logUrl = malloc(strlen(logMyUrl) + strlen(url) + 1)))
        PANIC;
    sprintf(logUrl, "%s%s", logMyUrl, name);

    name += 7;

    if (strnicmp(name, "2/", 2) == 0) {
        name += 2;
        login2Group = name;
        name = strchr(name, '/');
        if (name == NULL)
            goto NotFound;
        name++;
    }

    if (slashQuestion = FirstSlashQuestion(name)) {
        namelen = slashQuestion - name;
        slash = *slashQuestion == '/' ? slashQuestion : NULL;
    } else {
        namelen = strlen(name);
        slash = NULL;
    }

    for (i = 0, b = databases;; i++, b++) {
        if (i == cDatabases)
            goto NotFound;
        if (b->getName == NULL)
            continue;
        if (strnicmp(b->getName, name, namelen) == 0 && namelen == strlen(b->getName)) {
            if (e) {
                struct DATABASE *rb;
                for (rb = b; rb; rb = rb->getRelated) {
                    if (GroupMaskOverlap(e->gm, rb->gm)) {
                        b = rb;
                        break;
                    }
                }
            }
            break;
        }
    }

    if (b->optionGroupInReferer == 0 && login2Group != NULL)
        goto NotFound;

    /* If user lacks a valid cookie, try to reroute them through login since they may be using */
    /* a bookmark or may have been idle beyond cookie expiration */
    ipAccess = URLIpAccess(t, url, NULL, NULL, NULL, NULL);
    if (ipAccess != IPALOCAL && ipAccess != IPAAUTO && e == NULL) {
        HTTPCode(t, 302, 0);
        NoCacheHeaders(s);
        q = PoundFix(t->url, sizeof(t->url), 0);
        if (*t->version) {
            WriteLine(s, "Location: %s/login?%surl=", t->myUrl, q);
            /*
            UUSendZ(s, t->myUrl);
            */
            UUSendZCRLF(s, url);
            HeaderToBody(t);
        }

        WriteLine(s, "To access this site, go <a href='%s/login?%surl=", t->myUrl, q);
        /*
        UUSendZ(s, t->myUrl);
        */
        UUSendZ(s, url);
        UUSendZ(s, "'>here</a>\r\n");

        goto Finished;
    }

    if (login2Group) {
        size_t len;
        BOOL match = 0;

        len = name - login2Group - 1;
        if (len == 0)
            goto NotFound;
        if (!(tg = malloc(len + 1)))
            PANIC;
        memcpy(tg, login2Group, len);
        *(tg + len) = 0;
        if (SingletonGroupName(tg)) {
            GROUPMASK limitMask = MakeGroupMask(NULL, tg, 0, '=');
            match = GroupMaskOverlap(b->gm, limitMask);
            GroupMaskFree(&limitMask);
        }
        if (match == 0)
            goto NotFound;
        if (optionRefererInHostname) {
            char *period;
            char *host = FindField("Host", t->requestHeaders);
            PeriodsToHyphens(tg);
            if (host == NULL || (period = StartsIWith(host, tg)) == NULL || *period != '.')
                goto NotFound;
        }
    }

    if (ipAccess != IPALOCAL) {
        if (b->optionGroupInReferer) {
            if (login2Group == NULL) {
                struct GROUP *g;

                for (g = groups; g; g = g->next) {
                    if (GroupMaskOverlap(g->gm, b->gm) && GroupMaskOverlap(g->gm, e->gm)) {
                        break;
                    }
                }
                if (g) {
                    char *saveName;
                    if (!(saveName = strdup(name)))
                        PANIC;
                    HTTPCode(t, 302, 0);
                    NoCacheHeaders(s);
                    RefererURL(t->buffer, g->name, StartsIWith(b->getUrl, "https://") != NULL);
                    sprintf(AtEnd(t->buffer), "/login/2/%s/%s", g->name, saveName);
                    if (*t->version) {
                        WriteLine(s, "Location: ");
                        UUSendZCRLF(s, t->buffer);
                        HeaderToBody(t);
                    }

                    UUSendZ(s, "To access this site, go <a href='");
                    UUSendZ(s, t->buffer);
                    UUSendZ(s, "'>here</a>\r\n");
                    FreeThenNull(saveName);
                    goto Finished;
                }
                goto NotFound;
            }
        }
    }

    if (b->formMethod) {
        struct FORMVARS *p;
        BOOL anyForm = 1;

        if (slashQuestion && (query = strchr(slashQuestion, '?'))) {
            if (!(query = strdup(query + 1)))
                PANIC;
        }

        HTMLHeader(t, htmlHeaderOmitCache);
        /*
        HTTPCode(t, 200, 0);
        HeaderToBody(t);
        */
        UUSendZ(s, TAG_DOCTYPE_HTML_HEAD "<title>");
        SendHTMLEncoded(s, b->title);
        UUSendZ(s, "</title></head>\n<body onload='EZproxyCheckBack()'>\n");

        if (stricmp(b->formMethod, "dynamic") == 0) {
            anyForm = AdminLoginGetDynamic(t, b, url, accessType, query);
        } else {
            WriteLine(s, "<form name='EZproxyForm' method='%s' action='", b->formMethod);
            strcpy(t->buffer, b->getUrl);
            if (ipAccess != IPALOCAL && b->rewriteHost)
                EditURL(t, t->buffer);
            SendQuotedURL(t, t->buffer);
            UUSendZ(s, "'>");

            for (p = b->formVars; p; p = p->next) {
                AdminLoginGetSendField(t, b, p->var, p->val, query, p->expr);
            }
        }

        if (anyForm) {
            if (b->formSubmit)
                UUSendZ(s, b->formSubmit);
            else
                UUSendZ(s,
                        "If your browser does not continue automatically, click <input "
                        "type='submit' value='here'>");
            UUSendZ(s,
                    "\n\
</form>\n\
<form name='EZproxyTrack'><input type='hidden' name='back' value='1'>\
<script language='JavaScript'>\n\
<!--\n\
function EZproxyCheckBack() {\
  var goForward = (document.EZproxyTrack.back.value==1);\
  document.EZproxyTrack.back.value=2;\
  document.EZproxyTrack.back.defaultValue=2;\
  if (goForward) { document.EZproxyForm.submit(); }\
}\n\
-->\n\
</script>\n");
        } else {
            UUSendZ(s, "</form>");
        }
        UUSendZ(s, "</body></html>\n");

        LogSPUs(t, url, accessType, b);
        goto DoRaw;
    }

    if (b->getAppend) {
        if (slash && *slash && *(slash + 1)) {
            if (!(slashAppend = strdup(slash + 1)))
                PANIC;
            if (b->getAppendEncoded) {
                UnescapeString(slashAppend);
                Trim(slashAppend, TRIM_CTRL);
            }
        }
    } else if (slash != NULL) {
        *slash = 0;
        slash = NULL;
    }

    if (b->getRedirect) {
        if (t->version) {
            if (b->getRefresh) {
                HTTPCode(t, 200, 0);
                NoCacheHeaders(s);
                HTTPContentType(t, NULL, 0);
                UUFlush(s);
                UUSendZ(s, "Refresh: 0; URL=");
            } else {
                HTTPCode(t, 302, 0);
                NoCacheHeaders(s);
                UUFlush(s);
                UUSendZ(s, "Location: ");
            }
            /* UUSend2(s, b->getUrl, strlen(b->getUrl), slash, (slash ? strlen(slash) : 0), 0); */
            SendTokenizedURL(t, b, b->getUrl, &token, &tokenSignature, 0);
            SendTokenizedURL(t, b, slashAppend, &token, &tokenSignature, 0);
            UUSendCRLF(s);
            HeaderToBody(t);
        }
        UUSendZ(s, TAG_DOCTYPE_HTML_HEAD);
        if (t->finalStatus == 200) {
            UUSendZ(s, "<meta http-equiv='Refresh' content='0; URL=");
            /* UUSend2(s, b->getUrl, strlen(b->getUrl), slash, (slash ? strlen(slash) : 0), 0); */
            SendTokenizedURL(t, b, b->getUrl, &token, &tokenSignature, 0);
            SendTokenizedURL(t, b, slashAppend, &token, &tokenSignature, 0);
            UUSendZ(s, "'>");
        }
        UUSendZ(s, "</head>\n<body>\n");
        UUSendZ(s, "To access this document, wait a moment or click <a href='");
        /* UUSend2(s, b->getUrl, strlen(b->getUrl), slash, (slash ? strlen(slash) : 0), 0); */
        SendTokenizedURL(t, b, b->getUrl, &token, &tokenSignature, 0);
        SendTokenizedURL(t, b, slashAppend, &token, &tokenSignature, 0);
        UUSendZ(s, "'>here</a> to continue\n");
        UUSendZ(s, "</body></html>\n");

        LogSPUs(t, logUrl, accessType, b);
        goto DoRaw;
    }

    if (ConnectToServer(t, 0, b->getHost, b->getPort, DatabaseOrSessionIpInterface(t, b),
                        b->getUseSsl, b)) {
        goto Finished;
    }

    UUSendZ(r, "GET ");
    if (t->connectedByProxy) {
        WriteLine(r, "http%s://%s:%d", (b->getUseSsl ? "s" : ""), b->getHost, b->getPort);
    }

    SendTokenizedURL2(t, r, b, b->getUrl, &token, &tokenSignature, 0);
    SendTokenizedURL2(t, r, b, slashAppend, &token, &tokenSignature, 0);
    UUSendZ(r, " HTTP/1.0\r\n");

    /* For SSL, this was done in ConnectToServer as part of the CONNECT sequence */
    pa = firstProxyAuth;
    if (e && e->sessionProxyAuth)
        pa = e->sessionProxyAuth;
    if (t->connectedByProxy && b->getUseSsl == 0 && pa != NULL) {
        UUSendZ(r, "Proxy-authorization: Basic ");
        UUSendZCRLF(r, pa);
    }
    sentDomainCookies = 0;
    lastOK = 0;
    referer = NULL;
    for (field = t->requestHeaders; *field; field = nf) {
        LinkField(field);
        nf = NextField(field);
        lastOK = 0;
        for (vfp = validFields; *vfp != NULL; vfp++) {
            if (strnicmp(field, *vfp, strlen(*vfp)) == 0) {
                lastOK = 1;
                break;
            }
        }

        /*      if (!lastOK) */
        /*          printf("Not-"); */

        /*      printf("Sending: %s\n", field); */
        if (lastOK) {
            UUSendZCRLF(r, field);
        }
    }

    UUSendZ(r, "Host: ");
    UUSendZ(r, b->getHost);
    WriteLine(r, ":%d\r\n", b->getPort);

    WriteLine(r, "Connection: close\r\n");

    UUSendCRLF(r);

    LogSPUs(t, url, accessType, b);

    if (optionUseRaw) {
        StartRaw(t, 0, 0);
        goto DoRaw;
    } else {
        ProcessNonHTML(t);
        goto Finished;
    }

NotFound:
    HTTPCode(t, 404, 1);

Finished:
    UUStopSocket(r, 0);
    EndTranslate(t, 1);

DoRaw:
    FreeThenNull(logUrl);
    FreeThenNull(token);
    FreeThenNull(tokenSignature);
    FreeThenNull(slashAppend);
    FreeThenNull(query);
    FreeThenNull(tg);
}

static BOOL CheckCSRFToken(struct TRANSLATE *t, char *token) {
    char *cookie;
    char *c, *p;
    BOOL result = FALSE;

    if (!optionCSRFToken) {
        result = TRUE;
    } else if (token && *token) {
        for (cookie = t->requestHeaders; (cookie = FindField("cookie", cookie));) {
            LinkField(cookie);
            for (c = cookie; (c = strstr(c, CSRF_TOKEN_NAME));) {
                // Make sure we are at the start of this cookie value
                if (c > cookie) {
                    if (*(c - 1) != ' ' && *(c - 1) == ';') {
                        c++;
                        continue;
                    }
                }

                c += strlen(CSRF_TOKEN_NAME);
                if (*c == '=') {
                    c++;
                    if ((p = StartsWith(c, token)) && (*p == 0 || *p == ';')) {
                        result = TRUE;
                        break;
                    }
                }
            }
        }
    }

    return result;
}

static void WarnCSRFInvalid(struct TRANSLATE *t, struct USERINFO *uip, char *url, char *title) {
    if (SendEditedFile(t, uip, CSRFHTM, 0, url, 1, title, NULL)) {
        WriteLine(&t->ssocket, TAG_DOCTYPE_HTML_HEAD
                  "<title>Login Failure</title></head>"
                  "<body>"
                  "<p>Login failed due to missing or invalid " CSRF_TOKEN_NAME
                  " cookie.</p>"
                  "</body>"
                  "</html>");
    }
}

#define USER_NOT_IDENTIFIED (0)
#define USER_NEGATIVELY_IDENTIFIED (-1)
#define USER_POSITIVELY_IDENTIFIED (1)
#define USER_SOMEHOW_ESPECIALLY_IDENTIFIED (2)
void AdminLogin(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s;
    char phase;
    struct DATABASE *b;
    struct SESSION *e;
    char connectKey[MAXKEY];
    char destUrl[MAXURL];
    char *p;
    char *q;
    char *url;
    char *user;
    int userLevel = USER_NOT_IDENTIFIED;
    char *logUserFull = NULL;
    char logUserAuto[MAXUSERLEN];
    int sessionLife;
    GROUPMASK gm = NULL, requiredGm = NULL, gmAuto = NULL;
    struct USERINFO ui;
    char *title;
    enum IPACCESS ipAccess;
    BOOL mustChangePass;
    BOOL isLoginURL = 0;
    enum ACCOUNTFINDRESULT accountResult;
    int userLimit = 0;
    BOOL isConnect = 0;
    BOOL localRefresh = 0;
    int isMe = 0;
    BOOL urlReference = 0;
    const int ALSESSION = 0;
    const int ALURL = 1;
    const int ALUSER = 2;
    const int ALPASS = 3;
    const int ALCURRENT = 4;
    const int ALAUTH = 5;
    const int ALLOGUSER = 6;
    const int ALOLDPASS = 7;
    const int ALNEWPASS = 8;
    const int ALVERIFYPASS = 9;
    const int ALPASSWORD = 10;
    const int ALDOMAIN = 11;
    const int ALQURL = 12;
    const int ALPIN = 13;
    const int ALDESTURL = 14;
    const int ALREFRESH = 15;
    const int ALGROUP = 16;
    const int ALTICKET = 17;
    const int ALCONTEXT = 18;
    const int ALTAG = 19;
    const int ALLOGUP = 20;
    const int ALGROUPNUMBER = 21;
    const int ALINSTNUMBER = 22;
    const int ALGROUPSYMBOL = 23;
    const int ALINSTSYMBOL = 24;
    const int ALRENEW = 25;
    const int ALPRIVATECOMPUTER = 26;
    const int ALCSRFTOKEN = 27;
    enum INTRUDERSTATUS isIntruder = INTRUDERNOT;
    struct FORMFIELD ffs[] = {
        {"session", NULL, 0, 0, MAXKEY},
        {"url", NULL, 0, 0, 0},
        {"user", NULL, 0, 0, MAXUSERLEN},
        {"pass", NULL, 0, 0, 64},
        {"current", NULL, 0, 0, 32},
        {"auth", NULL, 0, 0, 32},
        {"loguser", NULL, 0, 0, MAXUSERLEN},
        {"oldpass", NULL, 0, 0, 64},
        {"newpass", NULL, 0, 0, 64},
        {"verifypass", NULL, 0, 0, 64},
        {"password", NULL, 0, 0, 64},
        {"domain", NULL, 0, 0, 32},
        {"qurl", NULL, 0, 0, 0},
        {"pin", NULL, 0, 0, 32},
        {"destURL", NULL, 0, 0, 0},
        {"refresh", NULL, 0, 0, 0},
        {"group", NULL, 0, 0, 0},
        {"ticket", NULL, 0, 0, 512},
        {"context", NULL, 0, 0, 512},
        {"tag", NULL, 0, 0, 32},
        {"logup", NULL, 0, 0, 10},
        {"groupNumber", NULL, 0, 0, 10},
        {"instNumber", NULL, 0, 0, 10},
        {"groupSymbol", NULL, 0, 0, 10},
        {"instSymbol", NULL, 0, 0, 10},
        {"renew", NULL, 0, 0, MAXRENEWLEN},
        {"privateComputer", NULL, 0, 10},
        {CSRF_TOKEN_NAME, NULL, 0, 0},
        {NULL, NULL, 0, 0, 0}
    };
    time_t loginStart, loginStop;
    static int totalLoginElapsed = 0;
    static int totalLoginCount = 0;
    char intruderIp[INET6_ADDRSTRLEN];
    char *autoUser = NULL;
    BOOL mustLogin = FALSE;
    BOOL shibbolethAttributes = FALSE;
    char *logupUrl = NULL;
    char *refererUrl;
    char buffer[533];

    gm = GroupMaskNew(0);
    requiredGm = GroupMaskDefault(NULL);

    UUtime(&loginStart);

    mustChangePass = 0;

    accountResult = accountWrong;
    sessionLife = 0;
    s = &t->ssocket;

    isConnect = strnicmp(t->url, "/connect", 8) == 0;
    if (stricmp(t->url, "/password") == 0) {
        if (accountDefaultPassword == NULL) {
            HTTPCode(t, 404, 1);
            if (debugLevel > 8000)
                Log("unauthenticated, no default password.");
            goto Finished;
        }
        mustChangePass = 1;
    }

    /* If this is a GET method, we want to pull the query string out as unharmed as possible */
    if (query != NULL) {
        for (url = query; url = StrIStr(url, "url="); url++)
            if (url == query || *(url - 1) == '&')
                break;
        if (url) {
            ffs[ALURL].value = url + 4; /* Skip url= */
            if (url == query)
                query = NULL;
            else
                *(url - 1) = 0;
        }
    }

    FindFormFields(query, post, ffs);

    if (ffs[ALLOGUP].value && (e = t->session)) {
        struct DATABASE *logupDb = NULL;
        char *logupDenyFile = NULL;
        UUAcquireMutex(&mActive);
        if (atoi(ffs[ALLOGUP].value) == e->logupId) {
            if (e->logupUrl)
                if (!(logupUrl = strdup(e->logupUrl)))
                    PANIC;
            logupDb = e->logupDb;
            logupDenyFile = e->logupDenyFile;
        }
        UUReleaseMutex(&mActive);
        if (logupDenyFile) {
            CSRFHeader(t);
            SendEditedFile(t, &ui, logupDenyFile, 1, logupUrl, 1,
                           logupDb && logupDb->title ? logupDb->title : "", NULL);
            if (debugLevel > 8000)
                Log("unauthenticated, logup denied, logupDenyFile sent.");
            goto Finished;
        }
        HTTPCode(t, 404, 1);
        if (debugLevel > 8000)
            Log("unauthenticated, logup denied, no logupDenyFile.");
        goto Finished;
    }

    /* treat a blank username like no username specified */
    if (ffs[ALUSER].value && *SkipWS(ffs[ALUSER].value) == 0)
        ffs[ALUSER].value = NULL;

    if (ffs[ALREFRESH].value && stricmp(ffs[ALREFRESH].value, "local") == 0) {
        localRefresh = 1;
    }

    if (ffs[ALDESTURL].value)
        ffs[ALURL].value = ffs[ALDESTURL].value;

    if (ffs[ALQURL].value)
        ffs[ALURL].value = ffs[ALQURL].value;

    urlReference = DecodeURLReference(ffs[ALURL].value);

    t->logTag = RemoveLogTag(ffs[ALURL].value);

    if (t->logTag == NULL && IsValidLogTag(ffs[ALTAG].value)) {
        if (!(t->logTag = strdup(ffs[ALTAG].value)))
            PANIC;
    }

    if (t->logTag == NULL && optionCaptureSpur) {
        if (refererUrl = FindField("Referer", t->requestHeaders))
            LinkField(refererUrl);
        SpurInsert(t, refererUrl, ffs[ALURL].value);
    }

    if (t->session &&
        (mustChangePass || (t->session)->accountChangePass || ffs[ALPASSWORD].value)) {
        AdminPassword(t, ffs[ALOLDPASS].value, ffs[ALNEWPASS].value, ffs[ALVERIFYPASS].value,
                      ffs[ALURL].value);
        if (debugLevel > 8000)
            Log("unauthenticated, must change password.");
        goto Finished;
    }

    phase = 0;
    connectKey[0] = 0;
    if (p = ffs[ALSESSION].value) {
        if ((phase = *p++)) {
            StrCpy3(connectKey, p, MAXKEY);
        }
    }

    /* If we've got a username, rip any potentially invalid control characters out of it */
    if (ffs[ALUSER].value)
        Trim(ffs[ALUSER].value, TRIM_CTRL | TRIM_LEAD | TRIM_TRAIL);

    /* Take control characters out of password, but leave alone otherwise */
    if (ffs[ALPASS].value)
        Trim(ffs[ALPASS].value, TRIM_CTRL);

    if (ffs[ALURL].value == NULL && strnicmp(t->url, "/login/", 7) == 0) {
        size_t urlLen = strlen(t->url);
        size_t myUrlLen = strlen(t->myUrl);
        if (urlLen > 7 && urlLen + myUrlLen + 1 <= sizeof(t->url)) {
            memmove(t->url + myUrlLen, t->url, urlLen + 1);
            memmove(t->url, t->myUrl, myUrlLen);
            ffs[ALURL].value = t->url;
            isLoginURL = 1;
        }
    }

    if (url = ffs[ALURL].value) {
        StrCpy3(destUrl, ffs[ALURL].value, sizeof(destUrl));
        EncodeAngleBrackets(destUrl, sizeof(destUrl));
        url = destUrl;
        ReverseEditURL(t, url, NULL);
    }

    if ((shibbolethAttributes = url != NULL && stricmp(url, "shibbolethattributes") == 0)) {
        mustLogin = TRUE;
    } else {
        // If the target URL fails standard parsing (e.g., include auth@ before the hostname),
        // invalidate it.
        if (url != NULL &&
            !ParseProtocolHostPort(url, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)) {
            url = NULL;
        }
    }

    gmAuto = GroupMaskNew(0);

    ipAccess = URLIpAccess(t, url, &sessionLife, gmAuto, NULL, &autoUser);

    if (t->session) {
        size_t had = GroupMaskCardinality((t->session)->gm);
        GroupMaskOr((t->session)->gm, gmAuto);
        if (had != GroupMaskCardinality((t->session)->gm))
            NeedSave();
    }

    userLevel = USER_NOT_IDENTIFIED;
    InitUserInfo(t, &ui, ffs[ALUSER].value, ffs[ALPASS].value, url, gm);

    ui.csrfValid = CheckCSRFToken(t, ffs[ALCSRFTOKEN].value);

    if (ui.spuRedirect) {
        LocalRedirect(t, ui.spuEdited, 302, localRefresh, NULL);
        if (optionLogSPUEdit || (debugLevel > 8000))
            Log("SPUEdit redirect to '%s'", ui.spuEdited);
        LogSPUs(t, ui.spuEdited, "spuedit", NULL);
        goto Finished;
    }

    if (url && !shibbolethAttributes) {
        int verifyHost2Result;
        if (AdminLoginReroute(t, url)) {
            if (debugLevel > 8000)
                Log("unauthenticated, redirected by AdminLoginReroute.");
            goto Finished;
        }
        /*      printf("URL is %s\n", url); */
        verifyHost2Result = VerifyHost2(url, requiredGm, &b, NULL, &isMe);
        if (isLoginURL && b == NULL) {
            HTTPCode(t, 404, 1);
            if (debugLevel > 8000)
                Log("unauthenticated, requests to log into a nonexistent database/resource.");
            goto Finished;
        }
        if ((ffs[ALUSER].value == NULL || (optionTicketIgnoreExcludeIP == 0 && b != NULL &&
                                           ffs[ALTICKET].value && *(ffs[ALTICKET].value))) &&
            url != NULL &&
            (ipAccess == IPALOCAL || (b != NULL && b->getName && b->optionGroupInReferer == 0 &&
                                      b->rewriteHost == 0 && ipAccess == IPAAUTO))) {
            if (b && b->getName &&
                (isLoginURL || t->session || excludeIPBanner == NULL ||
                 t->excludeIPBannerSuppress || !FileExists(excludeIPBanner))) {
                /* LogSPU handled within AdminLoginGet */
                AdminLoginGet(t, url, "local");
                if (debugLevel > 8000)
                    Log("unauthenticated, redirected by AdminLoginGet.");
            } else {
                if (b == NULL && isMe == 0) {
                    AdminUnknown(t, url);
                    if (debugLevel > 8000)
                        Log("unauthenticated, redirected by AdminUnknown.");
                } else {
                    if (t->session == NULL && excludeIPBanner && t->excludeIPBannerSuppress == 0 &&
                        FileExists(excludeIPBanner))
                        t->excludeIPBanner = 1;
                    LocalRedirect(t, url, 302, localRefresh, b);
                    if (t->excludeIPBanner == 0 && (b == NULL || b->referer == NULL))
                        LogSPUs(t, url, "local", b);
                    if (debugLevel > 8000)
                        Log("unauthenticated, redirected by LocalRedirect.");
                }
            }
            goto Finished;
        }
        /* For NetLibrary, if we are at the forced login phase and we are
           here by autologin, pretend we don't have a session to force
           the normal login process to occur
        */
        if (b && b->netLibraryConfigs) {
            ipAccess = IPAREMOTE;
            if (t->session && t->session->autoLoginBy != autoLoginByNone &&
                ffs[ALSESSION].value == NULL) {
                mustLogin = 1;
            }
        }
        /* As for NetLibrary, so for FMG */
        if (b && b->fmgConfigs && FMGHasInst(url)) {
            ipAccess = IPAREMOTE;
            if (t->session && t->session->autoLoginBy != autoLoginByNone &&
                ffs[ALSESSION].value == NULL) {
                mustLogin = 1;
            }
        }
        /*
        if (isMe && t->session && t->session->priv == 0 && t->session->autoLoginBy !=
        autoLoginByNone) { struct DISPATCH *dp; char *endOfHostPort; ParseProtocolHostPort(url,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, &endOfHostPort); if (endOfHostPort && (dp =
        FindLoginDispatch(endOfHostPort))) { if (dp->flags & (ADMIN_PRIV | ADMIN_MUST_LOGIN))
                    mustLogin = 1;
                else if (dp->flags & (ADMIN_MAYBE_LOGIN)) {
                    mustLogin = AdminMaybeLogin(t, dp, query, post);
                }
            }
        }
        */
        if (isMe) {
            struct DISPATCH *dp;
            char *endOfHostPort;
            ParseProtocolHostPort(url, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &endOfHostPort);
            if (endOfHostPort && (dp = FindLoginDispatch(endOfHostPort))) {
                if (dp->flags & ADMIN_MAYBE_LOGIN) {
                    if (AdminMaybeLogin(t, dp, query, post))
                        mustLogin = 1;
                } else if (t->session && t->session->priv == 0 &&
                           t->session->autoLoginBy != autoLoginByNone) {
                    if (dp->flags & (ADMIN_PRIV | ADMIN_MUST_LOGIN))
                        mustLogin = 1;
                }
            }
        }

        if (b && t->session == NULL) {
            /* the "phase == 'r'" test is looking for scenarios where someone tried to log
               in, but the cookie wasn't accepted.  This is specifically used to help allow
               the WebSlide browser to authenticate for access.  Here, we know that the user
               attempted to log in, the cookie was blocked, but now the IP is valid for
               an IP-based anonymous URL
            */
            if (phase == 0 || phase == 'r') {
                // If FindSessionByAnonymousUrl is successful, it will set t->session to the
                // anonymous session. If this happens, the logic that follows must be sure to
                // reset t->session to a different value.
                if (FindSessionByAnonymousUrl(t, url, b)) {
                    if (phase == 'r') {
                        t->session =
                            FindSessionByConnectKeyInSameCountryWithinConnectWindow(t, connectKey);
                    } else {
                        // This is redundant with logic that should immediately follow here
                        // that checks if t->session is set to the anonymous session, but it
                        // duplicated here defensively in case that logic is relocated.
                        t->session = NULL;
                    }
                }
            }
        }
    }

    // Insure that if the user is somehow linked to the anonymous session, that connection
    // is destroyed to prevent changing anything in the anonymous session.
    if (t->session && t->session->anonymous) {
        t->session = NULL;
    }

    logUserFull = ffs[ALUSER].value;

    if (ffs[ALRENEW].value && *ffs[ALRENEW].value && strcmp(ffs[ALRENEW].value, "false") != 0 &&
        t->session) {
        t->session->reloginRequired = TRUE;
        t->session->userObjectTicket[0] = 0;
    }

    user = ffs[ALUSER].value;
    if (mustLogin || user != NULL || (connectKey[0] == 0 && t->session == NULL) ||
        (t->session && t->session->reloginRequired) ||
        (t->session && isConnect == 0 && t->session->autoLoginBy != autoLoginByNone &&
         GroupMaskOverlap(t->session->gm, requiredGm) == 0)) {
        ui.user = ffs[ALUSER].value;
        ui.pass = ffs[ALPASS].value;
        ui.renew = ffs[ALRENEW].value;
        ui.privateComputer = ffs[ALPRIVATECOMPUTER].value;
        ui.passFromQueryString =
            ffs[ALPASS].value != NULL && (ffs[ALPASS].flags & FORMFIELDQUERYSTRING) != 0;
        ui.ticket = ffs[ALTICKET].value;
        ui.pin = ffs[ALPIN].value;
        ui.newPass = ffs[ALNEWPASS].value;
        ui.verifyPass = ffs[ALVERIFYPASS].value;
        ui.domain = ffs[ALDOMAIN].value;
        ui.context = ffs[ALCONTEXT].value;
        ui.urlReference = urlReference;
        if (IsOneOrMoreDigits(ffs[ALGROUPNUMBER].value))
            ui.groupNumber = ffs[ALGROUPNUMBER].value;
        if (IsOneOrMoreDigits(ffs[ALINSTNUMBER].value))
            ui.instNumber = ffs[ALINSTNUMBER].value;
        ui.groupSymbol = ffs[ALGROUPSYMBOL].value;
        ui.instSymbol = ffs[ALINSTSYMBOL].value;
        /* auth is handled differently; if field present but blank, map to NULL since
           FindUserNTDomain expects it to work this way
        */
        ui.auth = (ffs[ALAUTH].value && *(ffs[ALAUTH].value) ? ffs[ALAUTH].value : NULL);

        if (ffs[ALLOGUSER].value) {
            /* Sanitize the passed-in logUser value against control characters */
            Trim(ffs[ALLOGUSER].value, TRIM_CTRL);
            StrCpy3(ui.logUser, ffs[ALLOGUSER].value, sizeof(ui.logUser));
        } else {
            StrCpy3(ui.logUser, ffs[ALUSER].value, sizeof(ui.logUser));
        }

        if (ipAccess == IPAAUTO && ffs[ALUSER].value == NULL) {
            userLevel = USER_POSITIVELY_IDENTIFIED;
            GroupMaskCopy(gm, gmAuto);
            if (autoUser == NULL)
                autoUser = "auto";
            sprintf(logUserAuto, "%s-", autoUser);

            ipToStr(&t->sin_addr, strchr(logUserAuto, 0),
                    sizeof(logUserAuto) - strlen(logUserAuto) - 1);
            // UUinet_ntoa(strchr(logUserAuto, 0), t->sin_addr);

            logUserFull = logUserAuto;
            ui.autoLoginBy = autoLoginByIP;
            if (autoLoginIPBanner)
                StrCpy3(ui.loginBanner, autoLoginIPBanner, sizeof(ui.loginBanner));
            if (debugLevel > 8000)
                Log("autoLoginBy autoLoginByIP");
        } else {
            if (ffs[ALUSER].value &&
                (isIntruder = IsIntruder(t, ffs[ALUSER].value, intruderIp)) != INTRUDERNOT) {
                int loginDelay;

                t->denyClientKeepAlive = 1;
                if (isIntruder == INTRUDERREJECT) {
                    t->finalStatus = 597;
                    if (debugLevel > 8000)
                        Log("unauthenticated, intruder rejected with HTTP code %d.",
                            t->finalStatus);
                    goto Finished;
                }
                if (isIntruder != INTRUDERNOLOG)
                    Log("Evading intrusion attempt from '%s' for user '%s'", intruderIp,
                        NULLSAVESTR(ui.user));
                if (FileExists(INTRUDERHTM)) {
                    HTMLHeader(t, htmlHeaderOmitCache);
                    SendEditedFile(t, &ui, INTRUDERHTM, 1, url, 1, NULL);
                    if (debugLevel > 8000)
                        Log("unauthenticated, intruder sent " INTRUDERHTM " file.");
                    goto Finished;
                }
                if (totalLoginCount > 0) {
                    loginDelay =
                        (int)((float)1000.0 * ((float)totalLoginElapsed / (float)totalLoginCount));
                    if (loginDelay)
                        SubSleep(loginDelay / 1000, loginDelay % 1000);
                    if (debugLevel > 8000)
                        Log("unauthenticated, intruder delayed by %d.", loginDelay);
                }
            } else {
                UUtime(&loginStart);
                if (debugLevel > 8000)
                    Log("unauthenticated, login started at %s.", ctime(&loginStart));
            }
            ui.requiredGm = requiredGm;
            if (debugLevel > 8000)
                Log("FindUser");
            userLevel = FindUser(t, &ui, NULL, NULL, NULL, &userLimit, isIntruder);
            if (ffs[ALUSER].value && userLevel <= USER_NOT_IDENTIFIED) {
                NoteIntruder(t, ffs[ALUSER].value);
            }
            if (isIntruder == INTRUDERNOT) {
                UUtime(&loginStop);
                if (loginStop >= loginStart) {
                    totalLoginElapsed += (loginStop - loginStart);
                    totalLoginCount++;
                }
            }
            if (ui.deniedNotified) {
                if (debugLevel > 8000)
                    Log("unauthenticated, user denied by UserFile logic.");
                goto Finished;
            }
            if (ffs[ALGROUP].value) {
                GROUPMASK tgm = MakeGroupMask(NULL, ffs[ALGROUP].value, 0, '=');
                GroupMaskAnd(gm, tgm);
                GroupMaskFree(&tgm);
            }
            if (ui.accountResult == accountDefault)
                mustChangePass = 1;
            logUserFull = ui.logUser;
            if (logUserFull != NULL && ui.logPrefix[0] != 0) {
                StrCpy3(strchr(ui.logPrefix, 0), ui.logUser,
                        sizeof(ui.logPrefix) - strlen(ui.logPrefix));
                logUserFull = ui.logPrefix;
            }
        }

        if (AdminCGILogin(t, &ui, url, 0)) {
            if (debugLevel > 8000)
                Log("unauthenticated, a CGI script will decide authentication");
            goto Finished;
        }

        if (userLevel <= USER_NOT_IDENTIFIED) {
            if (userLevel == USER_NOT_IDENTIFIED) {
                title = (VerifyHost(url, NULL, &b) == 0 && b != NULL && b->title != NULL ? b->title
                                                                                         : "");
                if (myUrlHttps && t->useSsl == 0 && optionAllowHTTPLogin == 0) {
                    AdminForceHTTPSLogin(t, url, ffs[ALAUTH].value, ffs[ALTAG].value);
                    if (debugLevel > 8000)
                        Log("unauthenticated, redirected by AdminForceHTTPSLogin.");
                } else {
                    CSRFHeader(t);
                    if (user && optionCSRFToken && !ui.csrfValid) {
                        WarnCSRFInvalid(t, &ui, url, title);
                    } else {
                        SendEditedFile(t, &ui, (user != NULL ? ui.loginBu : ui.login), 1, url, 1,
                                       title, NULL);
                    }
                    if (debugLevel > 8000)
                        Log("unauthenticated, sent login/loginbu file.");
                }
            }
            goto Finished;
        }
    }

    if (usageLogUser == 0)
        logUserFull = NULL;

    if (url == NULL)
        url = "menu";

    if (ui.ezpedit) {
        if (e = FindSessionByGenericUser(user)) {
            /* Unless the current key has been requested, set the current key to expire */
            if (ffs[ALCURRENT].value == NULL) {
                UUtime(&e->accessed);
                e->expirable = 1;
                e->lifetime = mSessionLife;
                ClusterUpdateSession(e);
                e = NULL;
            }
        }
        if (e == NULL) {
            e = StartSession(NULL, ui.relogin, NULL);
            if (e == NULL)
                goto MaxSessions;
            ReadConfigSetString(&e->genericUser, user);
            if (debugLevel > 8000)
                Log("new session '%s'", e->key);
        } else {
            if (debugLevel > 8000)
                Log("old session '%s'", e->key);
        }
        MergeSessionVariables(t, e);

        e->expirable = 0;
        e->lifetime = 0;
        GroupMaskCopy(e->gm, gm);
        GroupMaskOr(e->gm, gmAuto);
        LoadDefaultCookies(e);
        memcpy(&e->ipInterface, &ui.sourceIpInterface, sizeof(e->ipInterface));
        if (ui.loginBanner[0])
            ReadConfigSetString(&e->loginBanner, ui.loginBanner);
        AdminLoginVars(e, &ui, 1);
        ClusterUpdateSession(e);
        WriteLine(s, "ezpedit\n%s/connect?session=s%s\n", t->myUrl, e->connectKey);
        if (debugLevel > 8000)
            Log("authenticated, redirected to %s/connect?session=s%s", t->myUrl, e->connectKey);
        goto Finished;
    }

    if (connectKey[0] == 0 && t->session == NULL) {
        if (ui.autoLoginBy != IPAAUTO)
            EnforceUserLimit(logUserFull, userLimit);
        e = StartSession(logUserFull, ui.relogin, NULL);
        if (e == NULL)
            goto MaxSessions;
        e->autoLoginBy = ui.autoLoginBy;
        SetLogUserBrief(e);
        MergeSessionVariables(t, e);

        if (ui.csrfValid && ffs[ALCSRFTOKEN].value) {
            strcpy(e->csrfToken, ffs[ALCSRFTOKEN].value);
        }

        if (sessionLife != 0)
            e->lifetime = sessionLife;
        if (ui.lifetime != 0)
            e->lifetime = ui.lifetime;
        if (userLevel != USER_NOT_IDENTIFIED) {
            e->priv = e->priv || userLevel >= USER_SOMEHOW_ESPECIALLY_IDENTIFIED || ui.admin;
            e->sessionFlags |= ui.sessionFlags;
        }
        if (ui.accountResult > accountZero && ui.accountResult < accountWrong) {
            if (!(e->accountUsername = strdup(user)))
                PANIC;
        }

        if (ui.menu[0])
            StrCpy3(e->menu, ui.menu, sizeof(e->menu));

        e->docsCustomDir = t->docsCustomDir;

        if (ui.sessionProxyAuth[0])
            ReadConfigSetString(&e->sessionProxyAuth, ui.sessionProxyAuth);
        AdminLoginVars(e, &ui, 1);
        e->accountChangePass = mustChangePass;
        e->sin_addr = t->sin_addr;
        GroupMaskCopy(e->gm, gm);
        GroupMaskOr(e->gm, gmAuto);
        LoadDefaultCookies(e);
        AuditEventLogin(t, AUDITLOGINSUCCESS, logUserFull, e, ui.pass, ui.comment);
        memcpy(&e->ipInterface, &ui.sourceIpInterface, sizeof(e->ipInterface));
        if (ui.loginBanner[0])
            ReadConfigSetString(&e->loginBanner, ui.loginBanner);
        phase = e->notify != 0 ? 't' : 's';
        ClusterUpdateSession(e);
    } else {
        if (debugLevel > 8000)
            Log("Looking for an existing session by connect key '%s'", connectKey);
        if (e = t->session) {
            struct SESSION *newSession = NULL;
            int i;
            if (connectKey[0]) {
                newSession = FindSessionByConnectKeyInSameCountryWithinConnectWindow(t, connectKey);
                if (newSession && e != newSession && newSession->anonymous == 0 &&
                    newSession->genericUser == NULL) {
                    logUserFull = newSession->logUserFull;
                    if (optionLoginReplaceGroups)
                        GroupMaskCopy(e->gm, newSession->gm);
                    else
                        GroupMaskOr(e->gm, newSession->gm);
                    LoadDefaultCookies(e);
                    if (e->autoLoginBy != autoLoginByNone &&
                        newSession->autoLoginBy == autoLoginByNone) {
                        /* StoreLogUser ends up calling SetLogUserBrief, so this change
                           does end up reflected if relevant
                        */
                        e->autoLoginBy = autoLoginByNone;
                    }
                    memcpy(&e->ipInterface, &newSession->ipInterface, sizeof(e->ipInterface));
                    e->priv |= newSession->priv;
                    strcpy(e->menu, newSession->menu);
                    e->docsCustomDir = newSession->docsCustomDir;
                    if (e->lifetime < newSession->lifetime)
                        e->lifetime = newSession->lifetime;
                    if (e->lifetime < ui.lifetime)
                        e->lifetime = ui.lifetime;
                    if (e->cpipSid == NULL && newSession->cpipSid != NULL) {
                        e->cpipSid = newSession->cpipSid;
                        newSession->cpipSid = NULL;
                    }
                    for (i = 0; i < MAXSESSIONVARS; i++) {
                        if (newSession->vars[i]) {
                            FreeThenNull(e->vars[i]);
                            e->vars[i] = newSession->vars[i];
                            newSession->vars[i] = NULL;
                        }
                    }
                    if (newSession->sessionProxyAuth) {
                        FreeThenNull(e->sessionProxyAuth);
                        e->sessionProxyAuth = newSession->sessionProxyAuth;
                    }
                    memcpy(&e->ipInterface, &ui.sourceIpInterface, sizeof(e->ipInterface));
                    if (newSession->loginBanner) {
                        FreeThenNull(e->loginBanner);
                        e->loginBanner = newSession->loginBanner;
                        newSession->loginBanner = NULL;
                    }
                    VariablesMerge(e->sessionVariables, newSession->sessionVariables, NULL);
                    MergeSessionVariables(t, e);
                    NeedSave();
                    if (debugLevel > 8000)
                        Log("reuse old session '%s'", newSession->key);
                }
            }

            if (userLevel > USER_NOT_IDENTIFIED) {
                /*
                size_t had = GroupMaskCardinality(e->gm);
                */
                if (optionLoginReplaceGroups) {
                    if (connectKey[0] == 0)
                        GroupMaskCopy(e->gm, gm);
                } else {
                    GroupMaskOr(e->gm, gm);
                }
                GroupMaskOr(e->gm, gmAuto);
                LoadDefaultCookies(e);
                /*
                if (had != GroupMaskCardinality(e->gm))
                    NeedSave();
                */

                if (memcmp(&e->ipInterface, &ui.sourceIpInterface, sizeof(e->ipInterface)) != 0) {
                    memcpy(&e->ipInterface, &ui.sourceIpInterface, sizeof(e->ipInterface));
                    /* NeedSave(); */
                }

                UUtime(&e->lastAuthenticated);

                /* This NeedSave() is mandatory for lastAuthenticated, so the other NeedSave()
                   invocations in this block are simply redundant
                */
                NeedSave();
            }

            if (e->autoLoginBy != autoLoginByNone && ui.user && *(ui.user)) {
                /* StoreLogUser ends up calling SetLogUserBrief, so this change
                   does end up reflected if relevant
                */
                e->autoLoginBy = autoLoginByNone;
                NeedSave();
            }

            if (userLevel != USER_NOT_IDENTIFIED) {
                e->priv = e->priv || userLevel >= USER_SOMEHOW_ESPECIALLY_IDENTIFIED || ui.admin;
                e->sessionFlags |= ui.sessionFlags;
            }

            if (logUserFull) {
                if (StoreLogUser(e, logUserFull)) {
                    NeedSave();
                }
                AuditEventLogin(t, AUDITLOGINSUCCESSRELOGIN, logUserFull, e, ui.pass, ui.comment);
            }

            if (user != NULL) {
                e->relogin = ui.relogin;
                e->reloginRequired = 0;
            }
            ClusterUpdateSession(e);

            /* Check to see if we're required to change password, and if so, send page */
            /*
            if (e->accountChangePass || mustChangePass) {
                HTMLHeader(t, htmlHeaderNoCache);
                SendEditedFile(t, uip, LOGINCPHTM, 1, url, 1, "", NULL);
                goto Finished;
            }
            */

            /* Check to see if our group access is inadequate; if so, send upgrade login page */
            if (strnicmp(url, "http://", 7) == 0 || strnicmp(url, "https://", 8) == 0 ||
                strnicmp(url, "ftp://", 6) == 0) {
                /* isMe comes back 0 for not me, 1 for me but not a known /login/ URL, 2 for known
                 * /login/ URL */
                /* Test was previously isMe == 0 shortcut, but now isMe != 1 is more correct */
                if (VerifyHost2(url, requiredGm, &b, NULL, &isMe) == 0 && isMe != 1 &&
                    GroupMaskOverlap(e->gm, requiredGm) == 0) {
                    if (myUrlHttps && t->useSsl == 0 && optionAllowHTTPLogin == 0) {
                        AdminForceHTTPSLogin(t, url, ffs[ALAUTH].value, ffs[ALTAG].value);
                        if (debugLevel > 8000)
                            Log("authentication insufficient, redirect by AdminForceHTTPSLogin");
                    } else {
                        AdminLogup(t, FALSE, url, b, requiredGm, isIntruder);
                        if (debugLevel > 8000)
                            Log("authenticated, redirect by AdminLogup");

                        /*
                        struct GROUP *g;
                        char *denyFile;

                        InitUserInfo(t, &ui, NULL, NULL, url, gm);
                        ui.logup = 1;
                        FindUser(t, &ui, NULL, NULL, requiredGm, &userLimit, isIntruder);
                        if (AdminCGILogin(t, &ui, url, 1))
                            goto Finished;
                        title = (b != NULL && b->title != NULL ? b->title : "");
                        HTMLHeader(t, optionLoginNoCache ? htmlHeaderNoCache :
                        htmlHeaderMaxAgeCache);

                        if (e->autoLoginBy != autoLoginByNone)
                            denyFile = ui.login;
                        else {
                            denyFile = LOGUPHTM;
                            for (g = groups; g; g = g->next) {
                                if (GroupMaskOverlap(requiredGm, g->gm) && GroupMaskOverlap(e->gm,
                        g->gm) == 0 && g->denyFile) { denyFile = g->denyFile; break;
                                }
                            }
                        }

                        SendEditedFile(t, uip, denyFile, 1, url, 1, title, NULL);
                        */
                    }
                    goto Finished;
                }

                /* I have no idea why this was here
                if (isLoginURL)
                    HTTPCode(t, 404, 1);
                else {
                */
                if (AdminLoginBanner(t, url, isConnect)) {
                    strcpy(t->buffer, url);
                    /* LogSPU handled in AdminRedirect */
                    AdminRedirect(t);
                    if (debugLevel > 8000)
                        Log("authenticated, redirected by AdminRedirect");
                }
            } else {
                if (AdminLoginBanner(t, url, isConnect)) {
                    MyNameRedirect(t, url, 0);
                    if (debugLevel > 8000)
                        Log("authenticated, redirected by MyNameRedirect");
                }
            }
            goto Finished;
        }

        e = FindSessionByConnectKeyInSameCountryWithinConnectWindow(t, connectKey);
        if (e == NULL) {
            HTMLHeader(t, htmlHeaderOmitCache);
            UUSendZ(s, "Attempt to authenticate to non-existent session\n");
            if (debugLevel > 8000)
                Log("authenticated, but attempted to authenticate to non-existent session");
            goto Finished;
        }

        /* If we've come by a generic key, we need to have a new session */
        if (e->genericUser != NULL) {
            GroupMaskCopy(gm, e->gm);
            e = StartSession(logUserFull, ui.relogin, NULL);
            if (e == NULL)
                goto MaxSessions;
            MergeSessionVariables(t, e);

            AuditEvent(t, AUDITLOGINSUCCESS, logUserFull, e, ui.pass);
            GroupMaskCopy(e->gm, gm);
            GroupMaskOr(e->gm, gmAuto);
            LoadDefaultCookies(e);
            if (ui.loginBanner[0])
                ReadConfigSetString(&e->loginBanner, ui.loginBanner);
            memcpy(&e->ipInterface, &ui.sourceIpInterface, sizeof(e->ipInterface));
            phase = e->notify != 0 ? 'u' : 't';
        }
        e->sin_addr = t->sin_addr;

        if (userLevel != USER_NOT_IDENTIFIED) {
            e->priv = e->priv || userLevel >= USER_SOMEHOW_ESPECIALLY_IDENTIFIED || ui.admin;
            e->sessionFlags |= ui.sessionFlags;
        }

        ClusterUpdateSession(e);

        // Detect attempt to hijack existing session
        if (e && e->csrfToken[0] && !CheckCSRFToken(t, e->csrfToken)) {
            HTMLHeader(t, htmlHeaderOmitCache);
            WarnCSRFInvalid(t, &ui, url, "");
            goto Finished;
        }

        if (phase == 'r') {
            if (t->docsCustomDir == NULL && e && e->docsCustomDir)
                t->docsCustomDir = e->docsCustomDir;
            HTMLHeader(t, htmlHeaderOmitCache);
            SendEditedFile(t, &ui, COOKIEHTM, 1, url, 1, NULL);
            /* Some firewalls were doing a pre-fetch of session information and getting the cookie
               error, causing non-existent session errors.  The e->confirmed flag is now used as a
               timeout indicator instead to cause session that are never claimed to be expired
            */
            /* StopSession(e); */
            if (debugLevel > 8000)
                Log("authenticated, pre-fetch of session information detected");
            goto Finished;
        }

        phase--;
    }

    StrCpyOverlap(t->url, url);
    url = t->url;
    q = PoundFix(t->url, sizeof(t->url), 0);

    if (e->notify == 1) {
        HTTPCode(t, 200, 0);
        if (*t->version) {
            HTTPContentType(t, NULL, 0);
            /* Setting the extra values made IE 3 not recognize the cookie */
            /*          UUSendZ(s, "Set-Cookie: ezproxy=%s; Version=1; Path=/; Domain=%s;
             * Max-Age=120\r\n\r\n", e->key, myDomain); */
            SendEZproxyCookie(t, e);
            HeaderToBody(t);
        }

        buffer[0] = 0;
        // RegExpiryExplainStrCat(buffer, &license);

        UUSendZ(s, TAG_DOCTYPE_HTML_HEAD "<title>EZproxy Unregistered Copy</title></head><body>\n");
        UUSendZ(s, buffer);
        UUSendZ(
            s,
            "  It has been provided to you by <a href='http://www.oclc.com/ezproxy/'>OCLC</a>.\n");
        UUSendZ(s,
                "After it has expired, you must purchase a license for this product or remove this "
                "product from your server.\n<p>\n");
        UUSendZ(s,
                "If you would like to receive a demonstration key that can temporarily suppress "
                "this unregistered copy message or to request further information, send an\n");
        UUSendZ(s, "e-mail message to <a href='mailto:" EZPROXYHELPEMAIL "'>" EZPROXYHELPEMAIL
                   "</a>.<p>\n");
        WriteLine(s, "<a href='%s/connect?session=%c%s&%surl=", t->myUrl, phase, e->connectKey, q);
        if (t->logTag)
            WriteLine(s, "%s$", t->logTag);
        UUSendZ(s, url);
        UUSendZ(s, "'>Continue...</a>\n");
        UUSendZ(s, "</body></html>\n");
        if (debugLevel > 8000)
            Log("authenticated, Unregistered Copy");
    } else {
        if (ui.asp) {
            HTTPCode(t, 200, 0);
            HTTPContentType(t, "text/plain", 1);
        } else {
            HTTPCode(t, 303, 0);
            NoCacheHeaders(s);
            if (*t->version) {
                if (e->notify == 0)
                    SendEZproxyCookie(t, e);
                WriteLine(s, "Location: %s/connect?session=%c%s&%surl=", t->myUrl, phase,
                          e->connectKey, q);
                if (t->logTag)
                    WriteLine(s, "%s$", t->logTag);
                UUSendZCRLF(s, url);
            }
            HeaderToBody(t);
        }

        if (ui.asp == 0)
            UUSendZ(s, "To access this site, go <a href='");

        WriteLine(s, "%s/connect?session=%c%s&%surl=", t->myUrl, phase, e->connectKey, q);
        if (t->logTag)
            WriteLine(s, "%s$", t->logTag);
        UUSendZ(s, url);

        if (ui.asp == 0)
            UUSendZ(s, "'>here</a>");

        UUSendCRLF(s);
        if (debugLevel > 8000)
            Log("authenticated, redirected to %s/connect?session=%c%s&%surl=", t->myUrl, phase,
                e->connectKey, q);
    }
    if (e->notify > 0) {
        e->notify--;
        ClusterUpdateSession(e);
    }

    goto Finished;

MaxSessions:
    HTTPCode(t, 503, 0);
    HTTPContentType(t, NULL, 1);
    if (SendEditedFile(t, &ui, MSHTM, 0, url, 1, NULL) != 0) {
        UUSendZ(s, TAG_DOCTYPE_HTML_HEAD
                "</head><body>Maximum sessions reached, please try again later</body></html>\n");
    }
    if (debugLevel > 90) {
        Log("maximum sessions reached");
    }
    goto Finished;

Finished:
    if (gm) {
        GroupMaskFree(&gm);
        /* ui.gm points to gm, so since gm is free, set ui.gm to free as well */
        ui.gm = NULL;
    }

    GroupMaskFree(&gmAuto);
    GroupMaskFree(&requiredGm);
    FreeThenNull(logupUrl);
}
