#define __UUFILE__ "adminpassword.c"

#include "common.h"
#include "uustring.h"

#include "adminpassword.h"

void AdminPassword(struct TRANSLATE *t, char *oldPass, char *newPass, char *verifyPass, char *url) {
    char *cpReason;
    int cpNumber;
    char cpFile[32];
    enum ACCOUNTFINDRESULT accountResult;

    if ((t->session)->accountUsername == NULL) {
        cpReason = "The password on this account cannot be changed.";
        cpNumber = 3;
        goto CpResponse;
    }

    if (oldPass == NULL)
        oldPass = "";
    if (newPass == NULL)
        newPass = "";
    if (verifyPass == NULL)
        verifyPass = "";

    if (stricmp(newPass, verifyPass) != 0) {
        cpReason = "The two copies of your new password did not match.";
        cpNumber = 4;
        goto CpResponse;
    }

    if (strcmp(oldPass, "") == 0 && strcmp(newPass, "") == 0) {
        if ((t->session)->accountChangePass) {
            cpReason = "You must change your password before proceeding.";
            cpNumber = 1;
            goto CpResponse;
        }

        cpReason = "";
        cpNumber = 0;
        goto CpResponse;
    }

    if (stricmp(newPass, accountDefaultPassword) == 0) {
        cpReason = "Your default password may not be your new password.";
        cpNumber = 5;
        goto CpResponse;
    }

    if (strlen(newPass) < 6) {
        cpReason = "Your new password must be at least six characters long.";
        cpNumber = 6;
        goto CpResponse;
    }

    accountResult =
        AccountFind((t->session)->accountUsername, oldPass, accountDefaultPassword, newPass, 0);
    if (accountResult >= accountWrong) {
        cpReason = "Your old password was not correct.";
        cpNumber = 7;
        goto CpResponse;
    }

    cpReason = "Your password has been changed.";
    cpNumber = 2;
    (t->session)->accountChangePass = 0;

CpResponse:
    HTMLHeader(t, htmlHeaderNoCache);
    strcpy(cpFile, DOCSDIR "logincp");
    if (cpNumber)
        sprintf(strchr(cpFile, 0), "%d", cpNumber);
    strcat(cpFile, ".htm");

    if (SendEditedFile(t, NULL, cpFile, cpNumber == 2 || cpNumber == 3, url, 1, cpReason, NULL) &&
        cpNumber != 2 && cpNumber != 3)
        SendEditedFile(t, NULL, LOGINCPHTM, 1, url, 1, cpReason, NULL);
}
