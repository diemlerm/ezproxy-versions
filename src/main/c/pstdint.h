#ifndef _PSTDINT_H_INCLUDED
/*  A portable stdint.h
 ****************************************************************************
 *  BSD License:
 ****************************************************************************
 *
 *  Copyright (c) 2005-2007 Paul Hsieh\n\
 *  All rights reserved.\n\
 *  \n\
 *  Redistribution and use in source and binary forms, with or without\n\
 *  modification, are permitted provided that the following conditions\n\
 *  are met:\n\
 *  \n\
 *  1. Redistributions of source code must retain the above copyright\n\
 *     notice, this list of conditions and the following disclaimer.\n\
 *  2. Redistributions in binary form must reproduce the above copyright\n\
 *     notice, this list of conditions and the following disclaimer in the\n\
 *     documentation and/or other materials provided with the distribution.\n\
 *  3. The name of the author may not be used to endorse or promote products\n\
 *     derived from this software without specific prior written permission.\n\
 *  \n\
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR\n\
 *  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES\n\
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.\n\
 *  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,\n\
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT\n\
 *  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,\n\
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY\n\
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT\n\
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF\n\
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.\n\
 *
 *
 *  Version 0.1.11
 *
 *  The ANSI C standard committee, for the C99 standard, specified the
 *  inclusion of a new standard include file called
 *  stdint.h which is included by inttypes.h.  This is
 *  a very useful and long desired include file which contains several
 *  very precise definitions for integer scalar types that is
 *  critically important for making portable several classes of
 *  applications including cryptography, hashing, variable length
 *  integer libraries and so on.  But for most developers its likely
 *  useful just for programming sanity.
 *
 *  The problem is that most compiler vendors have decided not to
 *  implement the C99 standard, and the next C++ language standard
 *  (which has a lot more mindshare these days) will be a long time in
 *  coming and its unknown whether or not it will include stdint.h or
 *  how much adoption it will have.  Either way, it will be a long time
 *  before all compilers come with a stdint.h and it also does nothing
 *  for the extremely large number of compilers available today which
 *  do not include this file, or anything comparable to it.
 *
 *  So that's what this file is all about.  Its an attempt to build a
 *  single universal include file that works on as many platforms as
 *  possible to deliver what stdint.h is supposed to.  A few things
 *  that should be noted about this file:
 *
 *    1) It is not guaranteed to be portable and/or present an identical
 *       interface on all platforms.  The extreme variability of the
 *       ANSI C standard makes this an impossibility right from the
 *       very get go. Its really only meant to be useful for the vast
 *       majority of platforms that possess the capability of
 *       implementing usefully and precisely defined, standard sized
 *       integer scalars.  Systems which are not intrinsically 2s
 *       complement may produce invalid constants.
 *
 *    2) There is an unavoidable use of non-reserved symbols.
 *
 *    3) Other standard include files are invoked.
 *
 *    4) This file may come in conflict with future platforms that do
 *       include stdint.h.  The hope is that one or the other can be
 *       used with no real difference.
 *
 *    5) In the current verison, if your platform can't represent
 *       int32_t, int16_t and int8_t, it just dumps out with a compiler
 *       error.
 *
 *    6) 64 bit integers may or may not be defined.  Test for their
 *       presence with the test: #ifdef INT64_MAX or #ifdef UINT64_MAX.
 *       Note that this is different from the C99 specification which
 *       requires the existence of 64 bit support in the compiler.  If
 *       this is not defined for your platform, yet it is capable of
 *       dealing with 64 bits then it is because this file has not yet
 *       been extended to cover all of your system's capabilities.
 *
 *    7) (u)intptr_t may or may not be defined.  Test for its presence
 *       with the test: #ifdef PTRDIFF_MAX.  If this is not defined
 *       for your platform, then it is because this file has not yet
 *       been extended to cover all of your system's capabilities, not
 *       because its optional.
 *
 *    8) The following might not been defined even if your platform is
 *       capable of defining it:
 *
 *       WCHAR_MIN
 *       WCHAR_MAX
 *       (u)int64_t
 *       PTRDIFF_MIN
 *       PTRDIFF_MAX
 *       (u)intptr_t
 *
 *    9) The following have not been defined:
 *
 *       WINT_MIN
 *       WINT_MAX
 *
 *   10) The criteria for defining (u)int_least(*)_t isn't clear,
 *       except for systems which don't have a type that precisely
 *       defined 8, 16, or 32 bit types (which this include file does
 *       not support anyways). Default definitions have been given.
 *
 *   11) The criteria for defining (u)int_fast(*)_t isn't something I
 *       would trust to any particular compiler vendor or the ANSI C
 *       committee.  It is well known that "compatible systems" are
 *       commonly created that have very different performance
 *       characteristics from the systems they are compatible with,
 *       especially those whose vendors make both the compiler and the
 *       system.  Default definitions have been given, but its strongly
 *       recommended that users never use these definitions for any
 *       reason (they do *NOT* deliver any serious guarantee of
 *       improved performance -- not in this file, nor any vendor's
 *       stdint.h).
 *
 *   12) The following macros:
 *
 *       PRIX16
 *       PRIX32
 *       PRIX64
 *       PRIX8
 *       PRIXFAST16
 *       PRIXFAST32
 *       PRIXFAST64
 *       PRIXFAST8
 *       PRIXLEAST16
 *       PRIXLEAST32
 *       PRIXLEAST64
 *       PRIXLEAST8
 *       PRIXMAX
 *       PRIXPTR
 *       PRId16
 *       PRId32
 *       PRId64
 *       PRId8
 *       PRIdFAST16
 *       PRIdFAST32
 *       PRIdFAST64
 *       PRIdFAST8
 *       PRIdLEAST16
 *       PRIdLEAST32
 *       PRIdLEAST64
 *       PRIdLEAST8
 *       PRIdMAX
 *       PRIdPTR
 *       PRIi16
 *       PRIi32
 *       PRIi64
 *       PRIi8
 *       PRIiFAST16
 *       PRIiFAST32
 *       PRIiFAST64
 *       PRIiFAST8
 *       PRIiLEAST16
 *       PRIiLEAST32
 *       PRIiLEAST64
 *       PRIiLEAST8
 *       PRIiMAX
 *       PRIiPTR
 *       PRIo16
 *       PRIo32
 *       PRIo64
 *       PRIo8
 *       PRIoFAST16
 *       PRIoFAST32
 *       PRIoFAST64
 *       PRIoFAST8
 *       PRIoLEAST16
 *       PRIoLEAST32
 *       PRIoLEAST64
 *       PRIoLEAST8
 *       PRIoMAX
 *       PRIoPTR
 *       PRIu16
 *       PRIu32
 *       PRIu64
 *       PRIu8
 *       PRIuFAST16
 *       PRIuFAST32
 *       PRIuFAST64
 *       PRIuFAST8
 *       PRIuLEAST16
 *       PRIuLEAST32
 *       PRIuLEAST64
 *       PRIuLEAST8
 *       PRIuMAX
 *       PRIuPTR
 *       PRIx16
 *       PRIx32
 *       PRIx64
 *       PRIx8
 *       PRIxFAST16
 *       PRIxFAST32
 *       PRIxFAST64
 *       PRIxFAST8
 *       PRIxLEAST16
 *       PRIxLEAST32
 *       PRIxLEAST64
 *       PRIxLEAST8
 *       PRIxMAX
 *       PRIxPTR
 *       SCNd16
 *       SCNd32
 *       SCNd64
 *       SCNd8
 *       SCNdFAST16
 *       SCNdFAST32
 *       SCNdFAST64
 *       SCNdFAST8
 *       SCNdLEAST16
 *       SCNdLEAST32
 *       SCNdLEAST64
 *       SCNdLEAST8
 *       SCNdMAX
 *       SCNdPTR
 *       SCNi16
 *       SCNi32
 *       SCNi64
 *       SCNi8
 *       SCNiFAST16
 *       SCNiFAST32
 *       SCNiFAST64
 *       SCNiFAST8
 *       SCNiLEAST16
 *       SCNiLEAST32
 *       SCNiLEAST64
 *       SCNiLEAST8
 *       SCNiMAX
 *       SCNiPTR
 *       SCNo16
 *       SCNo32
 *       SCNo64
 *       SCNo8
 *       SCNoFAST16
 *       SCNoFAST32
 *       SCNoFAST64
 *       SCNoFAST8
 *       SCNoLEAST16
 *       SCNoLEAST32
 *       SCNoLEAST64
 *       SCNoLEAST8
 *       SCNoMAX
 *       SCNoPTR
 *       SCNu16
 *       SCNu32
 *       SCNu64
 *       SCNu8
 *       SCNuFAST16
 *       SCNuFAST32
 *       SCNuFAST64
 *       SCNuFAST8
 *       SCNuLEAST16
 *       SCNuLEAST32
 *       SCNuLEAST64
 *       SCNuLEAST8
 *       SCNuMAX
 *       SCNuPTR
 *       SCNx16
 *       SCNx32
 *       SCNx64
 *       SCNx8
 *       SCNxFAST16
 *       SCNxFAST32
 *       SCNxFAST64
 *       SCNxFAST8
 *       SCNxLEAST16
 *       SCNxLEAST32
 *       SCNxLEAST64
 *       SCNxLEAST8
 *
 *       are strings which have been defined as the format strings required
 *       by printf formats to correctly output
 *       (u)intmax_t, (u)int64_t, (u)int32_t, (u)int16_t, (u)least64_t,
 *       (u)least32_t, (u)least16_t and (u)intptr_t types respectively.
 *
 *       In addition, the following macros are defined:
 *
 *       PRINTF_INTMAX_HEX_WIDTH
 *       PRINTF_INT64_HEX_WIDTH
 *       PRINTF_INT32_HEX_WIDTH
 *       PRINTF_INT16_HEX_WIDTH
 *       PRINTF_INT8_HEX_WIDTH
 *       PRINTF_INTMAX_DEC_WIDTH
 *       PRINTF_INT64_DEC_WIDTH
 *       PRINTF_INT32_DEC_WIDTH
 *       PRINTF_INT16_DEC_WIDTH
 *       PRINTF_INT8_DEC_WIDTH
 *
 *       Which specifies the maximum number of characters required to
 *       print the number of that type in either hexadecimal or decimal.
 *       These are an extension beyond what C99 specifies must be in
 *       stdint.h.
 *
 *  Compilers tested (all with 0 warnings at their highest respective
 *  settings): Borland Turbo C 2.0, WATCOM C/C++ 11.0 (16 bits and 32
 *  bits), Microsoft Visual C++ 6.0 (32 bit), Microsoft Visual Studio
 *  .net (VC7), Intel C++ 4.0, GNU gcc v3.3.3
 *
 *  This file should be considered a work in progress.  Suggestions for
 *  improvements, especially those which increase coverage are strongly
 *  encouraged.
 *
 *  Acknowledgements
 *
 *  The following people have made significant contributions to the
 *  development and testing of this file:
 *
 *  Chris Howie
 *  John Steele Scott
 *  Dave Thorup
 *
 *  Additional Authors
 *
 *  Steven Eastman, OCLC
 *
 */

#include <stddef.h>
#include <limits.h>
#include <signal.h>

#if defined(__STDC__)
#define PREDEF_STANDARD_C_1989
#if defined(__STDC_VERSION__)
#define PREDEF_STANDARD_C_1990
#if (__STDC_VERSION__ >= 199409L)
#define PREDEF_STANDARD_C_1994
#endif
#if (__STDC_VERSION__ >= 199901L)
#define PREDEF_STANDARD_C_1999
#define STDINT_H_UINTPTR_T_DEFINED
#include <inttypes.h>
#else
#endif
#else
#endif
#else
#endif

/*
 *  For gcc with _STDINT_H, fill in the PRINTF_INT*_MODIFIER macros, and
 *  do nothing else.  On the Mac OS X version of gcc this is _STDINT_H_.
 */

#if ((defined(STDINT_H_UINTPTR_T_DEFINED) || defined(INTPTR_MAX)) ||                    \
     (defined(__WATCOMC__) && (defined(_STDINT_H_INCLUDED) || __WATCOMC__ >= 1250))) && \
    !defined(_PSTDINT_H_INCLUDED)
#define _PSTDINT_H_INCLUDED

/* The #if has determined that this is a C99 environment.  And, because Microsoft Doesn't
 * support C99, this can't be a Microsoft Visual C++ environment.  */

/* Solaris 7 and 8 have inttypes.h and no stdint.h.  The C99 standard says that inttypes.h includes
 * stdint.h.  The presents of inttypes.h implies C99. */

#ifndef PRIXMAX
#define PRIXMAX PRIxMAX
#endif

/*
 *  Width of hexadecimal for number field.
 */

#define PRINTF_INT64_HEX_WIDTH "16"
#define PRINTF_INT32_HEX_WIDTH "8"
#define PRINTF_INT16_HEX_WIDTH "4"
#define PRINTF_INT8_HEX_WIDTH "2"
#define PRINTF_INTMAX_HEX_WIDTH PRINTF_INT64_HEX_WIDTH

/*
 *  Width of decimal for number field.
 */

#define PRINTF_INT64_DEC_WIDTH "20"
#define PRINTF_INT32_DEC_WIDTH "10"
#define PRINTF_INT16_DEC_WIDTH "5"
#define PRINTF_INT8_DEC_WIDTH "3"
#define PRINTF_INTMAX_DEC_WIDTH PRINTF_INT64_DEC_WIDTH

/*
 *  Something really weird is going on with Open Watcom.  Just pull some of
 *  these duplicated definitions from Open Watcom's stdint.h file for now.
 */

#if defined(__WATCOMC__) && __WATCOMC__ >= 1250
#if !defined(INT64_C)
#define INT64_C(x) (x + (INT64_MAX - INT64_MAX))
#endif
#if !defined(UINT64_C)
#define UINT64_C(x) (x + (UINT64_MAX - UINT64_MAX))
#endif
#if !defined(INT32_C)
#define INT32_C(x) (x + (INT32_MAX - INT32_MAX))
#endif
#if !defined(UINT32_C)
#define UINT32_C(x) (x + (UINT32_MAX - UINT32_MAX))
#endif
#if !defined(INT16_C)
#define INT16_C(x) (x)
#endif
#if !defined(UINT16_C)
#define UINT16_C(x) (x)
#endif
#if !defined(INT8_C)
#define INT8_C(x) (x)
#endif
#if !defined(UINT8_C)
#define UINT8_C(x) (x)
#endif
#if !defined(UINT64_MAX)
#define UINT64_MAX 18446744073709551615ULL
#endif
#if !defined(INT64_MAX)
#define INT64_MAX 9223372036854775807LL
#endif
#if !defined(UINT32_MAX)
#define UINT32_MAX 4294967295UL
#endif
#if !defined(INT32_MAX)
#define INT32_MAX 2147483647L
#endif
#if !defined(INTMAX_MAX)
#define INTMAX_MAX INT64_MAX
#endif
#if !defined(INTMAX_MIN)
#define INTMAX_MIN INT64_MIN
#endif
#endif

#endif

#if defined(_MSC_VER) && (_MSC_VER >= 1600) && (!defined(_PSTDINT_H_INCLUDED))
#define _PSTDINT_H_INCLUDED
/* Visual Studio 2010 and later */
#include <stdint.h>
#include "pinttypes.h"
#endif

#if defined(_MSC_VER) && (_MSC_VER < 1600) && (!defined(_PSTDINT_H_INCLUDED))
#define _PSTDINT_H_INCLUDED
/* Visual Studio earlier than 2010 */
#include <windows.h>
#include "pstdint2.h"
#include "pinttypes.h"
#endif

#if (!defined(STDINT_H_UINTPTR_T_DEFINED)) && (!defined(_PSTDINT_H_INCLUDED))
#define _PSTDINT_H_INCLUDED

#ifndef SIZE_MAX
#define SIZE_MAX (~(size_t)0)
#endif

/*
 *  Deduce the type assignments from limits.h under the assumption that
 *  integer sizes in bits are powers of 2, and follow the ANSI
 *  definitions.
 */

#ifndef UINT8_MAX
#define UINT8_MAX 0xff
#endif
#ifndef uint8_t
#if (UCHAR_MAX == UINT8_MAX) || defined(S_SPLINT_S)
typedef unsigned char uint8_t;
#define UINT8_C(v) ((uint8_t)v)
#else
#error "Platform not supported"
#endif
#endif

#ifndef INT8_MAX
#define INT8_MAX 0x7f
#endif
#ifndef INT8_MIN
#define INT8_MIN INT8_C(0x80)
#endif
#ifndef int8_t
#if (SCHAR_MAX == INT8_MAX) || defined(S_SPLINT_S)
typedef signed char int8_t;
#define INT8_C(v) ((int8_t)v)
#else
#error "Platform not supported"
#endif
#endif
#define PRINTF_INT8_MODIFIER ""
#define PRIX8 PRINTF_INT8_MODIFIER "X"
#define PRIXFAST8 PRINTF_INT8_MODIFIER "X"
#define PRIXLEAST8 PRINTF_INT8_MODIFIER "X"
#define PRId8 PRINTF_INT8_MODIFIER "d"
#define PRIdFAST8 PRINTF_INT8_MODIFIER "d"
#define PRIdLEAST8 PRINTF_INT8_MODIFIER "d"
#define PRIi8 PRINTF_INT8_MODIFIER "i"
#define PRIiFAST8 PRINTF_INT8_MODIFIER "i"
#define PRIiLEAST8 PRINTF_INT8_MODIFIER "i"
#define PRIo8 PRINTF_INT8_MODIFIER "o"
#define PRIoFAST8 PRINTF_INT8_MODIFIER "o"
#define PRIoLEAST8 PRINTF_INT8_MODIFIER "o"
#define PRIu8 PRINTF_INT8_MODIFIER "u"
#define PRIuFAST8 PRINTF_INT8_MODIFIER "u"
#define PRIuLEAST8 PRINTF_INT8_MODIFIER "u"
#define PRIx8 PRINTF_INT8_MODIFIER "x"
#define PRIxFAST8 PRINTF_INT8_MODIFIER "x"
#define PRIxLEAST8 PRINTF_INT8_MODIFIER "x"
#define SCNd8 PRINTF_INT8_MODIFIER "d"
#define SCNdFAST8 PRINTF_INT8_MODIFIER "d"
#define SCNdLEAST8 PRINTF_INT8_MODIFIER "d"
#define SCNi8 PRINTF_INT8_MODIFIER "i"
#define SCNiFAST8 PRINTF_INT8_MODIFIER "i"
#define SCNiLEAST8 PRINTF_INT8_MODIFIER "i"
#define SCNo8 PRINTF_INT8_MODIFIER "o"
#define SCNoFAST8 PRINTF_INT8_MODIFIER "o"
#define SCNoLEAST8 PRINTF_INT8_MODIFIER "o"
#define SCNu8 PRINTF_INT8_MODIFIER "u"
#define SCNuFAST8 PRINTF_INT8_MODIFIER "u"
#define SCNuLEAST8 PRINTF_INT8_MODIFIER "u"
#define SCNx8 PRINTF_INT8_MODIFIER "x"
#define SCNxFAST8 PRINTF_INT8_MODIFIER "x"
#define SCNxLEAST8 PRINTF_INT8_MODIFIER "x"

#ifndef UINT16_MAX
#define UINT16_MAX 0xffff
#endif
#ifndef uint16_t
#if (UINT_MAX == UINT16_MAX) || defined(S_SPLINT_S)
typedef unsigned int uint16_t;
#ifndef PRINTF_INT16_MODIFIER
#define PRINTF_INT16_MODIFIER ""
#endif
#define UINT16_C(v) ((uint16_t)(v))
#elif (USHRT_MAX == UINT16_MAX)
typedef unsigned short uint16_t;
#define UINT16_C(v) ((uint16_t)(v))
#ifndef PRINTF_INT16_MODIFIER
#define PRINTF_INT16_MODIFIER "h"
#endif
#else
#error "Platform not supported"
#endif
#endif

#ifndef INT16_MAX
#define INT16_MAX 0x7fff
#endif
#ifndef INT16_MIN
#define INT16_MIN INT16_C(0x8000)
#endif
#ifndef int16_t
#if (INT_MAX == INT16_MAX) || defined(S_SPLINT_S)
typedef signed int int16_t;
#define INT16_C(v) ((int16_t)(v))
#ifndef PRINTF_INT16_MODIFIER
#define PRINTF_INT16_MODIFIER ""
#endif
#elif (SHRT_MAX == INT16_MAX)
typedef signed short int16_t;
#define INT16_C(v) ((int16_t)(v))
#ifndef PRINTF_INT16_MODIFIER
#define PRINTF_INT16_MODIFIER "h"
#endif
#else
#error "Platform not supported"
#endif
#endif
#ifdef PRINTF_INT16_MODIFIER
#define PRIX16 PRINTF_INT16_MODIFIER "X"
#define PRIXFAST16 PRINTF_INT16_MODIFIER "X"
#define PRIXLEAST16 PRINTF_INT16_MODIFIER "X"
#define PRId16 PRINTF_INT16_MODIFIER "d"
#define PRIdFAST16 PRINTF_INT16_MODIFIER "d"
#define PRIdLEAST16 PRINTF_INT16_MODIFIER "d"
#define PRIi16 PRINTF_INT16_MODIFIER "i"
#define PRIiFAST16 PRINTF_INT16_MODIFIER "i"
#define PRIiLEAST16 PRINTF_INT16_MODIFIER "i"
#define PRIo16 PRINTF_INT16_MODIFIER "o"
#define PRIoFAST16 PRINTF_INT16_MODIFIER "o"
#define PRIoLEAST16 PRINTF_INT16_MODIFIER "o"
#define PRIu16 PRINTF_INT16_MODIFIER "u"
#define PRIuFAST16 PRINTF_INT16_MODIFIER "u"
#define PRIuLEAST16 PRINTF_INT16_MODIFIER "u"
#define PRIx16 PRINTF_INT16_MODIFIER "x"
#define PRIxFAST16 PRINTF_INT16_MODIFIER "x"
#define PRIxLEAST16 PRINTF_INT16_MODIFIER "x"
#define SCNd16 PRINTF_INT16_MODIFIER "d"
#define SCNdFAST16 PRINTF_INT16_MODIFIER "d"
#define SCNdLEAST16 PRINTF_INT16_MODIFIER "d"
#define SCNi16 PRINTF_INT16_MODIFIER "i"
#define SCNiFAST16 PRINTF_INT16_MODIFIER "i"
#define SCNiLEAST16 PRINTF_INT16_MODIFIER "i"
#define SCNo16 PRINTF_INT16_MODIFIER "o"
#define SCNoFAST16 PRINTF_INT16_MODIFIER "o"
#define SCNoLEAST16 PRINTF_INT16_MODIFIER "o"
#define SCNu16 PRINTF_INT16_MODIFIER "u"
#define SCNuFAST16 PRINTF_INT16_MODIFIER "u"
#define SCNuLEAST16 PRINTF_INT16_MODIFIER "u"
#define SCNx16 PRINTF_INT16_MODIFIER "x"
#define SCNxFAST16 PRINTF_INT16_MODIFIER "x"
#define SCNxLEAST16 PRINTF_INT16_MODIFIER "x"
#endif

#ifndef UINT32_MAX
#define UINT32_MAX (0xffffffffUL)
#endif
#ifndef uint32_t
#if (ULONG_MAX == UINT32_MAX) || defined(S_SPLINT_S)
typedef unsigned long uint32_t;
#define UINT32_C(v) v##UL
#ifndef PRINTF_INT32_MODIFIER
#define PRINTF_INT32_MODIFIER "l"
#endif
#elif (UINT_MAX == UINT32_MAX)
typedef unsigned int uint32_t;
#ifndef PRINTF_INT32_MODIFIER
#define PRINTF_INT32_MODIFIER ""
#endif
#define UINT32_C(v) v##U
#elif (USHRT_MAX == UINT32_MAX)
typedef unsigned short uint32_t;
#define UINT32_C(v) ((unsigned short)(v))
#ifndef PRINTF_INT32_MODIFIER
#define PRINTF_INT32_MODIFIER ""
#endif
#else
#error "Platform not supported"
#endif
#endif

#ifndef INT32_MAX
#define INT32_MAX (0x7fffffffL)
#endif
#ifndef INT32_MIN
#define INT32_MIN INT32_C(0x80000000)
#endif
#ifndef int32_t
#if (LONG_MAX == INT32_MAX) || defined(S_SPLINT_S)
typedef signed long int32_t;
#define INT32_C(v) v##L
#ifndef PRINTF_INT32_MODIFIER
#define PRINTF_INT32_MODIFIER "l"
#endif
#elif (INT_MAX == INT32_MAX)
typedef signed int int32_t;
#define INT32_C(v) v
#ifndef PRINTF_INT32_MODIFIER
#define PRINTF_INT32_MODIFIER ""
#endif
#elif (SHRT_MAX == INT32_MAX)
typedef signed short int32_t;
#define INT32_C(v) ((short)(v))
#ifndef PRINTF_INT32_MODIFIER
#define PRINTF_INT32_MODIFIER ""
#endif
#else
#error "Platform not supported"
#endif
#endif
#ifdef PRINTF_INT32_MODIFIER
#define PRIX32 PRINTF_INT32_MODIFIER "X"
#define PRIXFAST32 PRINTF_INT32_MODIFIER "X"
#define PRIXLEAST32 PRINTF_INT32_MODIFIER "X"
#define PRId32 PRINTF_INT32_MODIFIER "d"
#define PRIdFAST32 PRINTF_INT32_MODIFIER "d"
#define PRIdLEAST32 PRINTF_INT32_MODIFIER "d"
#define PRIi32 PRINTF_INT32_MODIFIER "i"
#define PRIiFAST32 PRINTF_INT32_MODIFIER "i"
#define PRIiLEAST32 PRINTF_INT32_MODIFIER "i"
#define PRIo32 PRINTF_INT32_MODIFIER "o"
#define PRIoFAST32 PRINTF_INT32_MODIFIER "o"
#define PRIoLEAST32 PRINTF_INT32_MODIFIER "o"
#define PRIu32 PRINTF_INT32_MODIFIER "u"
#define PRIuFAST32 PRINTF_INT32_MODIFIER "u"
#define PRIuLEAST32 PRINTF_INT32_MODIFIER "u"
#define PRIx32 PRINTF_INT32_MODIFIER "x"
#define PRIxFAST32 PRINTF_INT32_MODIFIER "x"
#define PRIxLEAST32 PRINTF_INT32_MODIFIER "x"
#define SCNd32 PRINTF_INT32_MODIFIER "d"
#define SCNdFAST32 PRINTF_INT32_MODIFIER "d"
#define SCNdLEAST32 PRINTF_INT32_MODIFIER "d"
#define SCNi32 PRINTF_INT32_MODIFIER "i"
#define SCNiFAST32 PRINTF_INT32_MODIFIER "i"
#define SCNiLEAST32 PRINTF_INT32_MODIFIER "i"
#define SCNo32 PRINTF_INT32_MODIFIER "o"
#define SCNoFAST32 PRINTF_INT32_MODIFIER "o"
#define SCNoLEAST32 PRINTF_INT32_MODIFIER "o"
#define SCNu32 PRINTF_INT32_MODIFIER "u"
#define SCNuFAST32 PRINTF_INT32_MODIFIER "u"
#define SCNuLEAST32 PRINTF_INT32_MODIFIER "u"
#define SCNx32 PRINTF_INT32_MODIFIER "x"
#define SCNxFAST32 PRINTF_INT32_MODIFIER "x"
#define SCNxLEAST32 PRINTF_INT32_MODIFIER "x"
#endif

/*
 *  The macro stdint_int64_defined is temporarily used to record
 *  whether or not 64 integer support is available.  It must be
 *  defined for any 64 integer extensions for new platforms that are
 *  added.
 */

#undef stdint_int64_defined
#if (defined(__STDC__) && defined(__STDC_VERSION__)) || defined(S_SPLINT_S)
#if (__STDC__ && __STDC_VERSION >= 199901L) || defined(S_SPLINT_S)
#define stdint_int64_defined
typedef long long int64_t;
typedef unsigned long long uint64_t;
#define UINT64_C(v) v##ULL
#define INT64_C(v) v##LL
#ifndef PRINTF_INT64_MODIFIER
#define PRINTF_INT64_MODIFIER "ll"
#endif
#endif
#endif

#if !defined(stdint_int64_defined)
#if defined(__GNUC__)
#define stdint_int64_defined
__extension__ typedef long long int64_t;
__extension__ typedef unsigned long long uint64_t;
#define UINT64_C(v) v##ULL
#define INT64_C(v) v##LL
#ifndef PRINTF_INT64_MODIFIER
#define PRINTF_INT64_MODIFIER "ll"
#endif
#elif defined(__MWERKS__) || defined(__SUNPRO_C) || defined(__SUNPRO_CC) || \
    defined(__APPLE_CC__) || defined(_LONG_LONG) || defined(_CRAYC) || defined(S_SPLINT_S)
#define stdint_int64_defined
typedef long long int64_t;
typedef unsigned long long uint64_t;
#define UINT64_C(v) v##ULL
#define INT64_C(v) v##LL
#ifndef PRINTF_INT64_MODIFIER
#define PRINTF_INT64_MODIFIER "ll"
#endif
#elif (defined(__WATCOMC__) && defined(__WATCOM_INT64__)) || \
    (defined(_MSC_VER) && _INTEGRAL_MAX_BITS >= 64) ||       \
    (defined(__BORLANDC__) && __BORLANDC__ > 0x460) || defined(__alpha) || defined(__DECC)
#define stdint_int64_defined
typedef __int64 int64_t;
typedef unsigned __int64 uint64_t;
#define UINT64_C(v) v##UI64
#define INT64_C(v) v##I64
#ifndef PRINTF_INT64_MODIFIER
#define PRINTF_INT64_MODIFIER "I64"
#endif
#endif
#endif
#ifdef PRINTF_INT64_MODIFIER
#define PRIX64 PRINTF_INT64_MODIFIER "X"
#define PRIXFAST64 PRINTF_INT64_MODIFIER "X"
#define PRIXLEAST64 PRINTF_INT64_MODIFIER "X"
#define PRId64 PRINTF_INT64_MODIFIER "d"
#define PRIdFAST64 PRINTF_INT64_MODIFIER "d"
#define PRIdLEAST64 PRINTF_INT64_MODIFIER "d"
#define SCNd64 PRINTF_INT64_MODIFIER "d"
#define SCNdFAST64 PRINTF_INT64_MODIFIER "d"
#define SCNdLEAST64 PRINTF_INT64_MODIFIER "d"
#define PRIi64 PRINTF_INT64_MODIFIER "i"
#define PRIiFAST64 PRINTF_INT64_MODIFIER "i"
#define PRIiLEAST64 PRINTF_INT64_MODIFIER "i"
#define SCNi64 PRINTF_INT64_MODIFIER "i"
#define SCNiFAST64 PRINTF_INT64_MODIFIER "i"
#define SCNiLEAST64 PRINTF_INT64_MODIFIER "i"
#define PRIo64 PRINTF_INT64_MODIFIER "o"
#define PRIoFAST64 PRINTF_INT64_MODIFIER "o"
#define PRIoLEAST64 PRINTF_INT64_MODIFIER "o"
#define SCNo64 PRINTF_INT64_MODIFIER "o"
#define SCNoFAST64 PRINTF_INT64_MODIFIER "o"
#define SCNoLEAST64 PRINTF_INT64_MODIFIER "o"
#define PRIu64 PRINTF_INT64_MODIFIER "u"
#define PRIuFAST64 PRINTF_INT64_MODIFIER "u"
#define PRIuLEAST64 PRINTF_INT64_MODIFIER "u"
#define SCNu64 PRINTF_INT64_MODIFIER "u"
#define SCNuFAST64 PRINTF_INT64_MODIFIER "u"
#define SCNuLEAST64 PRINTF_INT64_MODIFIER "u"
#define PRIx64 PRINTF_INT64_MODIFIER "x"
#define PRIxFAST64 PRINTF_INT64_MODIFIER "x"
#define PRIxLEAST64 PRINTF_INT64_MODIFIER "x"
#define SCNx64 PRINTF_INT64_MODIFIER "x"
#define SCNxFAST64 PRINTF_INT64_MODIFIER "x"
#define SCNxLEAST64 PRINTF_INT64_MODIFIER "x"
#endif

#if !defined(LONG_LONG_MAX) && defined(INT64_C)
#define LONG_LONG_MAX INT64_C(9223372036854775807)
#endif
#ifndef ULONG_LONG_MAX
#define ULONG_LONG_MAX UINT64_C(18446744073709551615)
#endif

#if !defined(INT64_MAX) && defined(INT64_C)
#define INT64_MAX INT64_C(9223372036854775807)
#endif
#if !defined(INT64_MIN) && defined(INT64_C)
#define INT64_MIN INT64_C(-9223372036854775808)
#endif
#if !defined(UINT64_MAX) && defined(INT64_C)
#define UINT64_MAX UINT64_C(18446744073709551615)
#endif

/*
 *  Width of hexadecimal for number field.
 */

#ifndef PRINTF_INT64_HEX_WIDTH
#define PRINTF_INT64_HEX_WIDTH "16"
#endif
#ifndef PRINTF_INT32_HEX_WIDTH
#define PRINTF_INT32_HEX_WIDTH "8"
#endif
#ifndef PRINTF_INT16_HEX_WIDTH
#define PRINTF_INT16_HEX_WIDTH "4"
#endif
#ifndef PRINTF_INT8_HEX_WIDTH
#define PRINTF_INT8_HEX_WIDTH "2"
#endif

/*
 *  Width of decimal for number field.
 */

#ifndef PRINTF_INT64_DEC_WIDTH
#define PRINTF_INT64_DEC_WIDTH "20"
#endif
#ifndef PRINTF_INT32_DEC_WIDTH
#define PRINTF_INT32_DEC_WIDTH "10"
#endif
#ifndef PRINTF_INT16_DEC_WIDTH
#define PRINTF_INT16_DEC_WIDTH "5"
#endif
#ifndef PRINTF_INT8_DEC_WIDTH
#define PRINTF_INT8_DEC_WIDTH "3"
#endif

/*
 *  Ok, lets not worry about 128 bit integers for now.  Moore's law says
 *  we don't need to worry about that until about 2040 at which point
 *  we'll have bigger things to worry about.
 */

#ifdef stdint_int64_defined
typedef int64_t intmax_t;
typedef uint64_t uintmax_t;
#define INTMAX_MAX INT64_MAX
#define INTMAX_MIN INT64_MIN
#define UINTMAX_MAX UINT64_MAX
#define UINTMAX_C(v) UINT64_C(v)
#define INTMAX_C(v) INT64_C(v)
#ifndef PRINTF_INTMAX_MODIFIER
#define PRINTF_INTMAX_MODIFIER PRINTF_INT64_MODIFIER
#endif
#ifndef PRINTF_INTMAX_HEX_WIDTH
#define PRINTF_INTMAX_HEX_WIDTH PRINTF_INT64_HEX_WIDTH
#endif
#ifndef PRINTF_INTMAX_DEC_WIDTH
#define PRINTF_INTMAX_DEC_WIDTH PRINTF_INT64_DEC_WIDTH
#endif
#else
typedef int32_t intmax_t;
typedef uint32_t uintmax_t;
#define INTMAX_MAX INT32_MAX
#define UINTMAX_MAX UINT32_MAX
#define UINTMAX_C(v) UINT32_C(v)
#define INTMAX_C(v) INT32_C(v)
#ifndef PRINTF_INTMAX_MODIFIER
#define PRINTF_INTMAX_MODIFIER PRINTF_INT32_MODIFIER
#endif
#ifndef PRINTF_INTMAX_HEX_WIDTH
#define PRINTF_INTMAX_HEX_WIDTH PRINTF_INT32_HEX_WIDTH
#endif
#ifndef PRINTF_INTMAX_DEC_WIDTH
#define PRINTF_INTMAX_DEC_WIDTH PRINTF_INT32_DEC_WIDTH
#endif
#endif
#ifdef PRINTF_INTMAX_MODIFIER
#define PRIXMAX PRINTF_INTMAX_MODIFIER "X"
#define PRIdMAX PRINTF_INTMAX_MODIFIER "d"
#define SCNdMAX PRINTF_INTMAX_MODIFIER "d"
#define PRIiMAX PRINTF_INTMAX_MODIFIER "i"
#define SCNiMAX PRINTF_INTMAX_MODIFIER "i"
#define PRIoMAX PRINTF_INTMAX_MODIFIER "o"
#define SCNoMAX PRINTF_INTMAX_MODIFIER "o"
#define PRIuMAX PRINTF_INTMAX_MODIFIER "u"
#define SCNuMAX PRINTF_INTMAX_MODIFIER "u"
#define PRIxMAX PRINTF_INTMAX_MODIFIER "x"
#endif

/*
 *  Because this file currently only supports platforms which have
 *  precise powers of 2 as bit sizes for the default integers, the
 *  least definitions are all trivial.  Its possible that a future
 *  version of this file could have different definitions.
 */

#ifndef stdint_least_defined
typedef int8_t int_least8_t;
typedef uint8_t uint_least8_t;
typedef int16_t int_least16_t;
typedef uint16_t uint_least16_t;
typedef int32_t int_least32_t;
typedef uint32_t uint_least32_t;
#define PRINTF_LEAST32_MODIFIER PRINTF_INT32_MODIFIER
#define PRINTF_LEAST16_MODIFIER PRINTF_INT16_MODIFIER
#define UINT_LEAST8_MAX UINT8_MAX
#define INT_LEAST8_MAX INT8_MAX
#define UINT_LEAST16_MAX UINT16_MAX
#define INT_LEAST16_MAX INT16_MAX
#define UINT_LEAST32_MAX UINT32_MAX
#define INT_LEAST32_MAX INT32_MAX
#define INT_LEAST8_MIN INT8_MIN
#define INT_LEAST16_MIN INT16_MIN
#define INT_LEAST32_MIN INT32_MIN
#ifdef stdint_int64_defined
typedef int64_t int_least64_t;
typedef uint64_t uint_least64_t;
#define PRINTF_LEAST64_MODIFIER PRINTF_INT64_MODIFIER
#define UINT_LEAST64_MAX UINT64_MAX
#define INT_LEAST64_MAX INT64_MAX
#define INT_LEAST64_MIN INT64_MIN
#endif
#endif
#undef stdint_least_defined

/*
 *  The ANSI C committee pretending to know or specify anything about
 *  performance is the epitome of misguided arrogance.  The mandate of
 *  this file is to *ONLY* ever support that absolute minimum
 *  definition of the fast integer types, for compatibility purposes.
 *  No extensions, and no attempt to suggest what may or may not be a
 *  faster integer type will ever be made in this file.  Developers are
 *  warned to stay away from these types when using this or any other
 *  stdint.h.
 */

typedef int_least8_t int_fast8_t;
typedef uint_least8_t uint_fast8_t;
typedef int_least16_t int_fast16_t;
typedef uint_least16_t uint_fast16_t;
typedef int_least32_t int_fast32_t;
typedef uint_least32_t uint_fast32_t;
#define UINT_FAST8_MAX UINT_LEAST8_MAX
#define INT_FAST8_MAX INT_LEAST8_MAX
#define UINT_FAST16_MAX UINT_LEAST16_MAX
#define INT_FAST16_MAX INT_LEAST16_MAX
#define UINT_FAST32_MAX UINT_LEAST32_MAX
#define INT_FAST32_MAX INT_LEAST32_MAX
#define INT_FAST8_MIN INT_LEAST8_MIN
#define INT_FAST16_MIN INT_LEAST16_MIN
#define INT_FAST32_MIN INT_LEAST32_MIN
#ifdef stdint_int64_defined
typedef int_least64_t int_fast64_t;
typedef uint_least64_t uint_fast64_t;
#define UINT_FAST64_MAX UINT_LEAST64_MAX
#define INT_FAST64_MAX INT_LEAST64_MAX
#define INT_FAST64_MIN INT_LEAST64_MIN
#endif

#undef stdint_int64_defined

/*
 *  Whatever piecemeal, per compiler thing we can do about the wchar_t
 *  type limits.
 */

#if defined(__WATCOMC__) || defined(_MSC_VER) || defined(__GNUC__)
#include <wchar.h>
#ifndef WCHAR_MIN
#define WCHAR_MIN 0
#endif
#ifndef WCHAR_MAX
#define WCHAR_MAX ((wchar_t)-1)
#endif
#endif

/*
 *  Whatever piecemeal, per compiler/platform thing we can do about the
 *  (u)intptr_t types and limits.
 */

#if defined(_MSC_VER) && defined(_UINTPTR_T_DEFINED)
#define STDINT_H_UINTPTR_T_DEFINED
#endif

#if (!(defined(STDINT_H_UINTPTR_T_DEFINED) || defined(INTPTR_MAX)))
#if defined(__alpha__) || defined(__ia64__) || defined(__x86_64__) || defined(_WIN64)
#define stdint_intptr_bits 64
#elif defined(__WATCOMC__) || defined(__TURBOC__)
#if defined(__TINY__) || defined(__SMALL__) || defined(__MEDIUM__)
#define stdint_intptr_bits 16
#else
#define stdint_intptr_bits 32
#endif
#elif defined(__i386__) || defined(_WIN32) || defined(WIN32)
#define stdint_intptr_bits 32
#elif defined(__INTEL_COMPILER)
/* TODO -- what will Intel do about x86-64? */
#endif

#ifdef stdint_intptr_bits
#define stdint_intptr_glue3_i(a, b, c) a##b##c
#define stdint_intptr_glue3(a, b, c) stdint_intptr_glue3_i(a, b, c)
#ifndef PRINTF_INTPTR_MODIFIER
#define PRINTF_INTPTR_MODIFIER stdint_intptr_glue3(PRINTF_INT, stdint_intptr_bits, _MODIFIER)
#endif
#ifndef PTRDIFF_MAX
#define PTRDIFF_MAX stdint_intptr_glue3(INT, stdint_intptr_bits, _MAX)
#endif
#ifndef PTRDIFF_MIN
#define PTRDIFF_MIN stdint_intptr_glue3(INT, stdint_intptr_bits, _MIN)
#endif
#ifndef UINTPTR_MAX
#define UINTPTR_MAX stdint_intptr_glue3(UINT, stdint_intptr_bits, _MAX)
#endif
#ifndef INTPTR_MAX
#define INTPTR_MAX stdint_intptr_glue3(INT, stdint_intptr_bits, _MAX)
#endif
#ifndef INTPTR_MIN
#define INTPTR_MIN stdint_intptr_glue3(INT, stdint_intptr_bits, _MIN)
#endif
#ifndef INTPTR_C
#define INTPTR_C(x) stdint_intptr_glue3(INT, stdint_intptr_bits, _C)(x)
#endif
#ifndef UINTPTR_C
#define UINTPTR_C(x) stdint_intptr_glue3(UINT, stdint_intptr_bits, _C)(x)
#endif
typedef stdint_intptr_glue3(uint, stdint_intptr_bits, _t) uintptr_t;
typedef stdint_intptr_glue3(int, stdint_intptr_bits, _t) intptr_t;
#else
/* TODO -- This following is likely wrong for some platforms, and does
   nothing for the definition of uintptr_t. */
typedef ptrdiff_t intptr_t;
#endif
#define STDINT_H_UINTPTR_T_DEFINED
#endif
#ifdef PRINTF_INTPTR_MODIFIER
#define PRIXPTR PRINTF_INTPTR_MODIFIER "X"
#define PRIdPTR PRINTF_INTPTR_MODIFIER "d"
#define SCNdPTR PRINTF_INTPTR_MODIFIER "d"
#define PRIiPTR PRINTF_INTPTR_MODIFIER "i"
#define SCNiPTR PRINTF_INTPTR_MODIFIER "i"
#define PRIoPTR PRINTF_INTPTR_MODIFIER "o"
#define SCNoPTR PRINTF_INTPTR_MODIFIER "o"
#define PRIuPTR PRINTF_INTPTR_MODIFIER "u"
#define SCNuPTR PRINTF_INTPTR_MODIFIER "u"
#define PRIxPTR PRINTF_INTPTR_MODIFIER "x"
#endif

/*
 *  Assumes sig_atomic_t is signed and we have a 2s complement machine.
 */

#ifndef SIG_ATOMIC_MAX
#define SIG_ATOMIC_MAX ((((sig_atomic_t)1) << (sizeof(sig_atomic_t) * CHAR_BIT - 1)) - 1)
#endif

#endif

#endif
