#ifndef __USRSHIBBOLETH_H__
#define __USRSHIBBOLETH_H__

#include "common.h"

enum FINDUSERRESULT FindUserShibboleth(struct TRANSLATE *t,
                                       struct USERINFO *uip,
                                       char *file,
                                       int f,
                                       struct FILEREADLINEBUFFER *frb,
                                       struct sockaddr_storage *pInterface);

#endif  // __USRSHIBBOLETH_H__
