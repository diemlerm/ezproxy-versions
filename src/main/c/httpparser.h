/**
 * httpparser.h
 *
 *  Created on: Jul 12, 2013
 *      Author: smelserw
 */

#ifndef HTTPPARSER_H_
#define HTTPPARSER_H_

#include <string.h>
#include <stdlib.h>

#include "common.h"
#include "uusocket.h"
#include "uustring.h"

// used by httpparser, included by common or something
// #include "misc.c"

/**
 * Parse the header
 * TODO: Make this more robust
 * @param socket The socket to read and parse header information from
 * @param responseCode The will get set during the header parsing
 * @return The length of the message, -1 on error
 */
int HTTPHeaderParse(struct UUSOCKET *socket, int *responseCode);

/**
 * Keep reading from socket till end of file read.  Does not support
 * chunked encoding.
 *
 * @param body Pointer to char array to write to.
 * @param maxBodySize The max amount to write to buffer
 * @param length Pointer to body length.  Will be updated to how much was
 * read from socket or maxBodySize.
 * @param socket Pointer to socket to read from.
 */
void HTTPBodyParse(char *body, const int maxBodySize, int *length, struct UUSOCKET *socket);

#endif /* HTTPPARSER_H_ */
