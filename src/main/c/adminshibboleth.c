/**
 * /shibboleth admin URL to view Shibboleth configuration data.
 */

#define __UUFILE__ "adminshibboleth.c"

#include "common.h"
#include "saml.h"
#include "uuxml.h"

#include "adminshibboleth.h"

struct IDPNODE {
    xmlChar *name;
    xmlChar *entityID;
};

int UUAvlTreeCmpIDPNode(const void *v1, const void *v2) {
    struct IDPNODE *var1 = (struct IDPNODE *)v1;
    struct IDPNODE *var2 = (struct IDPNODE *)v2;
    int cmp;

    cmp = stricmp((char *)var1->name, (char *)var2->name);
    if (cmp != 0)
        return cmp;

    return stricmp((char *)var1->entityID, (char *)var2->entityID);
}

int UUAvlTreeDupIDPNode(char *name, void *v1, void *v2) {
    return -1;
}

static BOOL AdminShibbolethIDPTree(struct UUAVLTREE *idpTree, char *xPath) {
    struct SHIBBOLETHSITE *ss;
    xmlXPathObjectPtr xpathObj = NULL;
    xmlNodeSetPtr nodes = NULL;
    int i;
    BOOL any = FALSE;
    struct IDPNODE *n;

    UUAvlTreeInit(idpTree, "IDP Tree", UUAvlTreeCmpIDPNode, UUAvlTreeDupIDPNode, NULL);

    for (ss = shibbolethSites; ss; ss = ss->next) {
        if (ss->entityID == NULL)
            continue;
        if (LoadUpdateShibbolethMetadata(ss, FALSE) == 0)
            continue;

        xpathObj = xmlXPathEvalExpression(BAD_CAST xPath, ss->xPathCtx);
        if (xpathObj) {
            if (nodes = xpathObj->nodesetval) {
                for (i = 0; i < nodes->nodeNr; i++) {
                    xmlChar *entityID = NULL;
                    if (entityID = xmlGetProp(nodes->nodeTab[i], BAD_CAST "entityID")) {
                        if (*entityID == 0) {
                            xmlFreeThenNull(entityID);
                        } else {
                            xmlChar *organizationName = NULL;
                            xmlNodePtr organization;

                            if (organization =
                                    UUxmlFindChild(nodes->nodeTab[i], "OrganizationDisplayName"))
                                organizationName = xmlNodeGetContent(organization);
                            if (organizationName == NULL)
                                if (organization =
                                        UUxmlFindChild(nodes->nodeTab[i], "OrganizationName"))
                                    organizationName = xmlNodeGetContent(organization);
                            if (organizationName && *organizationName == 0)
                                xmlFreeThenNull(organizationName);

                            if (!(n = calloc(1, sizeof(*n))))
                                PANIC;
                            n->name = organizationName ? organizationName : entityID;
                            n->entityID = entityID;
                            /* If we try to insert a duplicate, just get rid of the duplicate */
                            if (UUAvlTreeInsert(idpTree, n) == -1) {
                                if (n->name != n->entityID)
                                    xmlFreeThenNull(n->name);
                                xmlFreeThenNull(n->entityID);
                                FreeThenNull(n);
                            } else {
                                any = TRUE;
                            }
                        }
                    }
                }
            }
            xmlXPathFreeObject(xpathObj);
            xpathObj = NULL;
        }
    }

    return any;
}

static void AdminShibbolethIDPTreeShow(struct TRANSLATE *t,
                                       struct UUAVLTREE *idpTree,
                                       char *selectName) {
    struct UUSOCKET *s = &t->ssocket;
    struct IDPNODE *p, *next;
    UUAVLTREEITERCTX uatic;

    WriteLine(s, "<select name='%s'>\n", selectName);

    for (p = (struct IDPNODE *)UUAvlTreeIterFirst(idpTree, &uatic); p; p = next) {
        next = (struct IDPNODE *)UUAvlTreeIterNext(idpTree, &uatic);
        UUSendZ(s, "<option value='");
        SendQuotedURL(t, (char *)p->entityID);
        UUSendZ(s, "'>");
        SendHTMLEncoded(s, (char *)p->name);
        UUSendZ(s, "</option>\n");
    }

    UUSendZ(s, "</select>\n");
}

void AdminShibbolethIDPTreeFree(struct UUAVLTREE *idpTree) {
    struct IDPNODE *p, *next;
    UUAVLTREEITERCTX uatic;

    for (p = (struct IDPNODE *)UUAvlTreeIterFirst(idpTree, &uatic); p; p = next) {
        next = (struct IDPNODE *)UUAvlTreeIterNext(idpTree, &uatic);
        UUAvlTreeDelete(idpTree, p);
        if (p->name != p->entityID)
            xmlFreeThenNull(p->name);
        xmlFreeThenNull(p->entityID);
        FreeThenNull(p);
    }

    UUAvlTreeFree(idpTree);
}

static void SendCertificateLink(struct UUSOCKET *s, int certNum, const char *sep, BOOL adminSSL) {
    if (adminSSL) {
        WriteLine(s, "%s<a href='/ssl?entry=%d'>%d</a>", sep, certNum, certNum);
    } else {
        WriteLine(s, "%s%d", sep, certNum);
    }
}

#ifndef _POSIX_HOST_NAME_MAX
#define _POSIX_HOST_NAME_MAX 256
#endif
void AdminShibboleth(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    BOOL refresh;
    const int ASREFRESH = 0;
    const int ASREFRESHONE = 1;
    const int ASIDP13 = 2;
    const int ASIDP20 = 3;
    const int ASEZPROXYMETADATAFOR = 4;
    struct FORMFIELD ffs[] = {
        {"refresh",            NULL, 0, 0, 64 },
        {"refreshOne",         NULL, 0, 0, 256},
        {"idp13",              NULL, 0, 0, 0  },
        {"idp20",              NULL, 0, 0, 0  },
        {"EZproxyMetadataFor", NULL, 0, 0, 0  },
        {NULL,                 NULL, 0, 0, 0  }
    };
    struct SHIBBOLETHSITE *ss;
    BOOL failed;
    char *me = (myUrlHttps ? myUrlHttps : myUrlHttp);
    BOOL active12 = shibbolethAvailable12 == SHIBBOLETHAVAILABLEON;
    BOOL active13 = shibbolethAvailable13 == SHIBBOLETHAVAILABLEON;
    BOOL active20 = shibbolethAvailable20 == SHIBBOLETHAVAILABLEON;
    char *text1320 = active13 ? (active20 ? "1.3/2.0" : "1.3") : "2.0";
    struct UUAVLTREE idpTree;
    BOOL redirect;
    BOOL shibbolethAttributes = FALSE;
    BOOL addEntityDescriptorFootNote = FALSE;
    char *ezproxyMetadataFor = NULL;
    xmlChar *location;
    char *refershOne;
    struct tm *tm, rtm;
    int failures = 0;
    BOOL adminSSL = t->session->priv || GroupMaskOverlap(gmAdminSSLUpdate, t->session->gm) ||
                    GroupMaskOverlap(gmAdminSSLView, t->session->gm);

    FindFormFields(query, post, ffs);

    ezproxyMetadataFor = ffs[ASEZPROXYMETADATAFOR].value;

    if (ezproxyMetadataFor) {
        for (ss = shibbolethSites; ss; ss = ss->next) {
            if (stricmp(ss->filename, ezproxyMetadataFor) == 0) {
                break;
            }
        }
        if (ss) {
            SAMLSendShibbolethMetadata(t, 0, ss, 0);
            return;
        }
    }

    if (ffs[ASIDP13].value) {
        shibbolethAttributes = TRUE;
        location = SAMLSSOLocation(ffs[ASIDP13].value, &ss, SHIBVERSION13, NULL, NULL);
        if (location) {
            HTTPCode(t, 302, 0);
            NoCacheHeaders(s);
            if (t->version) {
                UUSendZ(s, "Location: ");
                ShibbolethSendURLwithWAYF(s, "shibbolethattributes", (char *)location, ss->entityID,
                                          NULL);
                UUSendCRLF(s);
                HeaderToBody(t);
            }

            UUSendZ(s, TAG_DOCTYPE_HTML_HEAD);
            UUSendZ(s, "</head>\n<body>\n");
            UUSendZ(s, "To access this document, wait a moment or click <a href='");
            ShibbolethSendURLwithWAYF(s, "shibbolethattributes", (char *)location, ss->entityID,
                                      NULL);
            UUSendZ(s, "'>here</a> to continue\n");
            UUSendZ(s, "</body></html>\n");
            xmlFreeThenNull(location);
            return;
        }

        HTTPCode(t, 200, 0);
        if (t->version) {
            NoCacheHeaders(s);
        }
        HTTPContentType(t, "text/html; charset=utf-8", 1);

        AdminHeader(t, AHNOHTTPHEADER, "Manage Shibboleth");

        UUSendZ(s, "There is ");
        UUSendZ(s, "<span class='failure'>no</span>");
        WriteLine(s, " Shibboleth 1.3 SSO location in the metadata for <tt>entityID</tt> \"%s\".\n",
                  ffs[ASIDP13].value);

        UUSendZ(s, "<h2>Manage Metadata</h2>\n");
        UUSendZ(s,
                "<p>\n\
    <a href='/shibboleth'>Shibboleth</a>\n\
    </p>\n");
        UUSendZ(s,
                "<p>\n\
    <a href='/shibboleth?refresh=yes'>Refresh metadata</a>\n\
    </p>\n");
    }

    if (ffs[ASIDP20].value) {
        shibbolethAttributes = TRUE;
        location = SAMLSSOLocation(ffs[ASIDP20].value, &ss, SHIBVERSION20, &redirect, NULL);
        if (location) {
            char *ur = URLReference("shibbolethattributes", NULL);
            SAMLAuthnRequest(t, BAD_CAST location, ur, ss, redirect);
            FreeThenNull(ur);
            xmlFreeThenNull(location);
            return;
        }

        HTTPCode(t, 200, 0);
        if (t->version) {
            NoCacheHeaders(s);
        }
        HTTPContentType(t, "text/html; charset=utf-8", 1);

        AdminHeader(t, AHNOHTTPHEADER, "Manage Shibboleth");

        UUSendZ(s, "There is ");
        UUSendZ(s, "<span class='failure'>no</span>");
        WriteLine(s, " Shibboleth 2.x SSO location in the metadata for <tt>entityID</tt> \"%s\".\n",
                  ffs[ASIDP20].value);

        UUSendZ(s, "<h2>Manage Metadata</h2>\n");
        UUSendZ(s,
                "<p>\n\
    <a href='/shibboleth'>Shibboleth</a>\n\
    </p>\n");
        UUSendZ(s,
                "<p>\n\
    <a href='/shibboleth?refresh=yes'>Refresh metadata</a>\n\
    </p>\n");
    }

    refresh = ffs[ASREFRESH].value != NULL;
    refershOne = ffs[ASREFRESHONE].value;

    if (refresh) {
        time_t now;
        int failures = 0;
        int metadataCount = 0;
        BOOL retrieveShibbolethMetadataFailed;
        BOOL loadUpdateShibbolethMetadataFailed;

        HTTPCode(t, 200, 0);
        if (t->version) {
            NoCacheHeaders(s);
        }
        HTTPContentType(t, "text/html; charset=utf-8", 1);

        AdminHeader(t, AHNOHTTPHEADER, "Refresh Metadata");

        UUtime(&now);

        UUSendZ(s, "<H2>");
        tm = UUlocaltime_r(&(now), &rtm);
        UUSendZ(s, "Refreshing Metadata");
        WriteLine(s, " on %04d-%02d-%02d %02d:%02d:%02d</H2>", tm->tm_year + 1900, tm->tm_mon + 1,
                  tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);

        UUSendZ(s, "<p/>\n<dl>\n");

        for (ss = shibbolethSites; ss; ss = ss->next) {
            metadataCount++;
            retrieveShibbolethMetadataFailed = FALSE;
            loadUpdateShibbolethMetadataFailed = FALSE;

            UUSendZ(s, "<dt><tt>");
            SendHTMLEncoded(s, ss->filename);
            UUSendZ(s, "</tt></dt>\n<dd>\n<ul>");
            UUFlush(s);

            if (ss->url == NULL) {
                UUSendZ(s, "<li>There is no metadata retrieval URL defined.</li>\n");
            } else {
                retrieveShibbolethMetadataFailed |= !RetrieveShibbolethMetadata(ss, FALSE);
                WriteLine(s, "<li>%s was retrieved.</li>\n",
                          (!retrieveShibbolethMetadataFailed)
                              ? "<span class='success'>New metadata</span>"
                              : "<span class='failure'>No new metadata</span>");
            }
            ss->fileMTimeAsLoaded =
                0; /* force a reload of the document, and just as importantly, the trusted certs. */
            loadUpdateShibbolethMetadataFailed |= !LoadUpdateShibbolethMetadata(ss, FALSE);
            WriteLine(s, "<li>There is %s metadata available.</li>\n",
                      (!loadUpdateShibbolethMetadataFailed)
                          ? "<span class='success'>valid</span>"
                          : "<span class='failure'>no valid</span>");

            UUSendZ(s, "</ul>\n</dd>\n");

            if (retrieveShibbolethMetadataFailed || loadUpdateShibbolethMetadataFailed) {
                failures++;
            }
        }

        UUSendZ(s, "</dl>\n<p/>\n");

        if (failures != 0) {
            WriteLine(s, "There %s %d metadata item%s of which %d reported failures.\n",
                      WasWere(metadataCount, FALSE), metadataCount, SIfNotOne(metadataCount),
                      failures);
        } else {
            WriteLine(s, "There %s %d metadata item%s.  No failures were reported.\n",
                      WasWere(metadataCount, FALSE), metadataCount, SIfNotOne(metadataCount));
        }

        if (failures) {
            UUSendZ(s,
                    "Review <a href='/messages?last=100'>recent messages</a> for more information "
                    "on update failures.<p/>");
        }

        UUSendZ(s, "<p><center>Metadata Refresh Complete</center></p>\n");

        if (shibbolethSites) {
            UUSendZ(s, "<h2>Manage Metadata</h2>\n");
            UUSendZ(s, "<p><a href='/shibboleth'>Manage Shibboleth</a></p>\n");
        }

        AdminFooter(t, 0);

        return;
    } else if ((refershOne != NULL) && (refershOne[0] != 0)) {
        for (ss = shibbolethSites; ss; ss = ss->next) {
            if (ss->url == NULL) {
            } else if (strcmp(refershOne, ss->filename) == 0) {
                RetrieveShibbolethMetadata(ss, FALSE);
                ss->fileMTimeAsLoaded = 0; /* force a reload of the document, and just as
                                              importantly, the trusted certs. */
                LoadUpdateShibbolethMetadata(ss, FALSE);
            }
        }
    }

    HTTPCode(t, 200, 0);
    if (t->version) {
        NoCacheHeaders(s);
    }
    HTTPContentType(t, "text/html; charset=utf-8", 1);

    AdminHeader(t, AHNOHTTPHEADER, "Manage Shibboleth");

    if (optionShibboleth == 0) {
        UUSendZ(
            s,
            "<p>Shibboleth support is not currently active.  Refer to the OCLC " EZPROXYNAMEPROPER
            " web site for additional information on how to configure " EZPROXYNAMEPROPER
            " for use with Shibboleth.</p>\n");
    }

    UUSendZ(s,
            "<table class='bordered-table th-row-nobold'>\n<caption>Configuration "
            "Information</caption>\n");

    if (active12) {
        UUSendZ(s, "<tr><th scope='row'>Shibboleth 1.2 providerId</th><td>");
        SendHTMLEncoded(s, shibbolethProviderId ? shibbolethProviderId : "(none set)");
        UUSendZ(s, "</td></tr>\n");
        UUSendZ(s, "<tr><th scope='row'>Shibboleth 1.2 Assertion Consumer Service URL</th><td>");
        SendHTMLEncoded(s, me);
        SendHTMLEncoded(s, SHIBBOLETHSHIREURL);
        UUSendZ(s, "</td></tr>\n");
    }

    if (active13 || optionShibboleth == 0) {
        UUSendZ(s, "<tr><th scope='row'>Shibboleth 1.3 Assertion Consumer Service URL</th><td>");
        SendHTMLEncoded(s, me);
        UUSendZ(s, "/Shibboleth.sso/SAML/POST</td></tr>\n");
    }

    if (active20 || optionShibboleth == 0) {
        UUSendZ(s, "<tr><th scope='row'>Shibboleth 2.0 Assertion Consumer Service URL</th><td>");
        SendHTMLEncoded(s, me);
        UUSendZ(s, "/Shibboleth.sso/SAML2/POST</td></tr>\n");
    }

    UUSendZ(s, "</table></p>\n");

    if (adminSSL) {
        UUSendZ(s, "<p>To view the Shibboleth 1.3/2.0 metadata for this server, ");
        if (active13 || active20) {
            UUSendZ(
                s,
                "</p><ol><li>click on one of the following Certificate Metadata links.</li><li>");
        }
        UUSendZ(s,
                "start at <a href='/ssl'>Manage SSL (https) certificates</a>,\n\
    click on the appropriate certificate, and click on the option to view Shiboleth metadata.");
        if (active13 || active20) {
            UUSendZ(s, "</li></ol>\n");
        } else {
            UUSendZ(s, "</p>\n");
        }
    }

#define BEGIN_TABLE UUSendZ(s, "<p>\n<table class='bordered-table th-row-nobold'>")
#define COLUMN_1          \
    UUSendZ(s, "<tr>\n"); \
    UUSendZ(s, "<th scope='row'>\n")
#define COLUMN_2           \
    UUSendZ(s, "</th>\n"); \
    UUSendZ(s, "<td>\n")
#define NEW_ROW            \
    UUSendZ(s, "</td>\n"); \
    UUSendZ(s, "</tr>\n")
#define END_TABLE          \
    UUSendZ(s, "</td>\n"); \
    UUSendZ(s, "</tr>\n"); \
    UUSendZ(s, "</table></p>\n")
#define SPAN_FAILURE "<span class='failure'>"
#define SPAN_SUCCESS "<span class='success'>"
#define CLOSE_SPAN "</span>"

    UUSendZ(s, "<h2>Details for Each Configured Identity Provider</h2>\n");

    failed = FALSE;
    for (ss = shibbolethSites; ss; ss = ss->next) {
        int cert;

        char *errMsg = NULL;
        char metadataInterface[_POSIX_HOST_NAME_MAX];

        BEGIN_TABLE;
        UUSendZ(s, "<caption>Details for ");
        SendHTMLEncoded(s, ss->filename);
        UUSendZ(s, "</caption>\n");

        COLUMN_1;
        UUSendZ(s, "EZproxy Entity ID\n");
        if (ss->entityID == NULL) {
            COLUMN_2;
            UUSendZ(s, "None defined for this metadata.\n");
        } else {
            COLUMN_2;
            UUSendZ(s, "<tt>");
            SendHTMLEncoded(s, ss->entityID);
            UUSendZ(s, "</tt>");
        }

        NEW_ROW;
        COLUMN_1;
        WriteLine(s, "Certificate");
        COLUMN_2;
        int *certificates = ss->certificates;
        if (certificates) {
            const char *sep = "";
            for (; *certificates; certificates++) {
                SendCertificateLink(s, *certificates, sep, adminSSL);
                sep = ", ";
            }
        } else {
            SendCertificateLink(s, sslActiveIndex, "", adminSSL);
        }
        WriteLine(s, " | <a href='/shibboleth?EZproxyMetadataFor=");
        SendHTMLEncoded(s, ss->filename);
        WriteLine(s, "'>EZproxy Metadata</a>\n");

        IPInterfaceName(metadataInterface, sizeof metadataInterface, &(ss->ipInterface));
        if (stricmp("ANY", metadataInterface) != 0) {
            NEW_ROW;
            COLUMN_1;
            UUSendZ(s, "Interface");
            COLUMN_2;
            UUSendZ(s, "<tt>");
            SendHTMLEncoded(s, metadataInterface);
            UUSendZ(s, "</tt>");
            if (stricmp("ANY", metadataInterface) != 0) {
                metadataInterface[sizeof(metadataInterface) - 1] = 1;

                if (UUGetHostByAddr(&ss->ipInterface, metadataInterface,
                                    sizeof(metadataInterface)) != NULL) {
                    AllLower(metadataInterface);
                    UUSendZ(s, " | ");
                    UUSendZ(s, "<tt>");
                    SendHTMLEncoded(s, metadataInterface);
                    UUSendZ(s, "</tt>");
                }
            }
        }

        NEW_ROW;
        COLUMN_1;
        UUSendZ(s, "Identity Provider Metadata File");
        COLUMN_2;
        UUSendZ(s, "Metadata from <tt>");
        SendHTMLEncoded(s, ss->filename);
        UUSendZ(s, "</tt> ");
        if (ss->doc != NULL) {
            UUSendZ(s, "is " SPAN_SUCCESS "valid" CLOSE_SPAN);
            tm = UUlocaltime_r((time_t *)&(ss->fileMTimeAsLoaded), &rtm);
            WriteLine(s, " as of %04d-%02d-%02d %02d:%02d:%02d", tm->tm_year + 1900, tm->tm_mon + 1,
                      tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
        } else {
            failures++;
            UUSendZ(s, "is " SPAN_FAILURE "not valid" CLOSE_SPAN ".");
        }

        if (ss->url) {
            NEW_ROW;
            COLUMN_1;
            UUSendZ(s, "Identity Provider Update URL");
            COLUMN_2;
            UUSendZ(s, "<a href='");
            SendHTMLEncoded(s, ss->url);
            WriteLine(s, "'><tt>");
            SendHTMLEncoded(s, ss->url);
            UUSendZ(s, "</tt></a>  ");
            UUSendZ(s, "<input type=button onClick='location.href=\"/shibboleth?refreshOne=");
            SendHTMLEncoded(s, ss->filename);
            UUSendZ(s, "\"' value='Update'>");
        }

        if (ss->urlAttempted != 0) {
            if (ss->docFetchValid != METADATADOC_VALID) {
                NEW_ROW;
                COLUMN_1;
                UUSendZ(s, "Error Messages");
                COLUMN_2;
                if (ss->docFetchValid == METADATADOC_FETCHING) {
                    WriteLine(s, " Metadata update currently being retrieved");
                } else {
                    WriteLine(
                        s, " Retrieval of new metadata " SPAN_FAILURE "failed%s",
                        ((ss->docFetchValid == METADATADOC_INVALID_SIGNATURE)
                             ? " its signature check"
                             : ((ss->docFetchValid == METADATADOC_INVALID_XML) ? " to parse as XML"
                                                                               : "")));
                    UUSendZ(s, CLOSE_SPAN ".");
                    tm = UUlocaltime_r((time_t *)&(ss->urlAttempted), &rtm);
                    WriteLine(s, " The last attempt was on %04d-%02d-%02d %02d:%02d:%02d.",
                              tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour,
                              tm->tm_min, tm->tm_sec);
                    if (ss->fileErrno != 0) {
                        WriteLine(s, " File write I/O " SPAN_FAILURE "error %s" CLOSE_SPAN ".",
                                  errMsg = ErrorMessage(ss->fileErrno));
                        FreeThenNull(errMsg);
                    }
                    if (ss->url) {
                        if (ss->urlErrno != 0) {
                            WriteLine(s, " URL read I/O " SPAN_FAILURE "error %s" CLOSE_SPAN ".",
                                      errMsg = ErrorMessage(ss->urlErrno));
                            FreeThenNull(errMsg);
                        }
                        if (ss->urlUUErrno != 0) {
                            WriteLine(s, " ");
                            failed = TRUE;
                            failures++;
                            UUSendZ(s, SPAN_FAILURE);
                            switch (ss->urlUUErrno) {
                            case UUSEHOSTNOTFOUND: {
                                WriteLine(s, "Host not found");
                                break;
                            }
                            case UUSESOCKETFAIL: {
                                WriteLine(s, "Socket failure");
                                break;
                            }
                            case UUSECONNECTFAIL: {
                                WriteLine(s, "Connection failure");
                                break;
                            }
                            case UUSEBINDFAIL: {
                                WriteLine(s, "Bind failure");
                                break;
                            }
                            case UUSESSLFAIL: {
                                WriteLine(s, "SSL negotiation failure");
                                break;
                            }
                            default: {
                                WriteLine(s, "unknown failure");
                                break;
                            }
                            }
                            UUSendZ(s, CLOSE_SPAN ".");
                        }
                        if (ss->urlHTTPCode != 0) {
                            WriteLine(s, " HTTP ");
                            if (ss->urlHTTPCode > 299) {
                                failed = TRUE;
                                failures++;
                                UUSendZ(s, SPAN_FAILURE);
                            } else {
                                failed = FALSE;
                            }
                            WriteLine(s, "%d", ss->urlHTTPCode);
                            if (failed) {
                                UUSendZ(s, CLOSE_SPAN);
                            }
                            UUSendZ(s, ".");
                        }
                    }
                }
            }
        }
        END_TABLE;
    }
    if (failures) {
        UUSendZ(s,
                "Review <a href='/messages?last=100'>recent messages</a> for more information on "
                "update failures.<p/>");
    }

    if (active12) {
        UUSendZ(s, "<h2>Shibboleth 1.2 Attributes</h2>\n");
        UUSendZ(s, "<p>\n");
        UUSendZ(s, "<a href='");
        ShibbolethSendURLwithWAYF(s, "attributes", NULL, NULL, NULL);
        UUSendZ(s, "'>Show 1.2 Attributes</a>\n");
        UUSendZ(s, "</p>");
    }

    if (active13) {
        if (AdminShibbolethIDPTree(
                &idpTree,
                "//md:EntityDescriptor/md:IDPSSODescriptor/md:SingleSignOnService[@Binding = "
                "'urn:mace:shibboleth:1.0:profiles:AuthnRequest']/../..")) {
            UUSendZ(s, "<h2>Shibboleth 1.3 Attributes</h2>\n");
            UUSendZ(s, "<p><form action='/shibboleth' method='get'>\n");
            AdminShibbolethIDPTreeShow(t, &idpTree, "idp13");
            UUSendZ(s,
                    "<input type='submit' value='Show 1.3 Attributes from this Identity "
                    "Provider'></form></p>\n");
        }
        AdminShibbolethIDPTreeFree(&idpTree);
    }

    if (active20) {
        if (AdminShibbolethIDPTree(
                &idpTree,
                "//md:EntityDescriptor/md:IDPSSODescriptor/md:SingleSignOnService[@Binding = "
                "'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST']/../..")) {
            UUSendZ(s, "<h2>Shibboleth 2.0 Attributes</h2>\n");
            UUSendZ(s, "<p><form action='/shibboleth' method='get'>\n");
            AdminShibbolethIDPTreeShow(t, &idpTree, "idp20");
            UUSendZ(s,
                    "<input type='submit' value='Show 2.0 Attributes from this Identity "
                    "Provider'></form></p>\n");
        }
        AdminShibbolethIDPTreeFree(&idpTree);
    }

    if (active13 || active20) {
        UUSendZ(s, "<h2>Shibboleth Attributes via " EZPROXYUSR "</h2>\n");
        UUSendZ(s, "<p>If you have already set up " EZPROXYUSR " using ::Shibboleth with ");
        if (active13) {
            if (active20) {
                UUSendZ(s, "IDP13, WAYF13 or ");
            } else {
                UUSendZ(s, "IDP13 or WAYF13");
            }
        }
        if (active20)
            UUSendZ(s, "IDP20");
        WriteLine(
            s,
            ", <a href='/login?url=shibbolethattributes'>Show Shibboleth Attributes</a> should "
            "lead to a display of the attributes released by your Identity Provider.</p>\n");
    }

    if (optionProxyType == PTHOST && optionWildcardHTTPS) {
        UUSendZ(s,
                "<h2>Special Certificate for Proxy by Hostname</h2>\n<p>Your server is using proxy "
                "by hostname with a wildcard certificate named <strong>*.");
        SendHTMLEncoded(s, myName);
        UUSendZ(
            s,
            "</strong>.  This certificate may not be accepted for communication with other "
            "Shibboleth servers. If necessary, you can create a certificate named <strong>login.");
        SendHTMLEncoded(s, myName);
        UUSendZ(s,
                "</strong> using <a href='/ssl-new?shibboleth=on'>Create New SSL Certificate for "
                "Shibboleth Communication</a>.\n");
    }

    if (shibbolethSites) {
        UUSendZ(s, "<h2>Manage Metadata</h2>\n");
        UUSendZ(s,
                "<p>\n\
<a href='/shibboleth?refresh=yes'>Refresh metadata</a>\n\
</p>\n");
    }

    AdminFooter(t, 0);
}
