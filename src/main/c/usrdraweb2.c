/**
 * DRAWeb2 (ILS) user authentication
 *
 * The module implements DRAWeb2 user authentication.  DRA was a former
 * ILS vendor that was purchaesd by SirsiDynix.  Few DRAWeb2 sites still
 * exist.
 *
 * This authentication method supports common conditions and actions.
 */

#define __UUFILE__ "usrdraweb2.c"

#include "common.h"
#include "uustring.h"

#include "usr.h"
#include "usrdraweb2.h"

#define DRAWEB2TYPEMAX 31

struct DRAWEB2CTX {
    BOOL typeMatch;
    BOOL systemMatch;
    BOOL anyTypes;
    BOOL anySystem;
    BOOL seenHost;
    char type[DRAWEB2TYPEMAX + 1];
    char system[17];
    enum DATEFORMAT dateFormat;
    char userid[32];
};

static int UsrDraweb2Date(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct DRAWEB2CTX *draweb2Ctx = (struct DRAWEB2CTX *)context;

    if (draweb2Ctx->seenHost) {
        Log("For DRAWeb2, Date must appear before URL in " EZPROXYUSR);
        return 1;
    }

    if (stricmp(arg, "MDY") == 0) {
        draweb2Ctx->dateFormat = dateMDY;
    } else if (stricmp(arg, "DMY") == 0) {
        draweb2Ctx->dateFormat = dateDMY;
    } else if (stricmp(arg, "YMD") == 0) {
        draweb2Ctx->dateFormat = dateYMD;
    } else {
        Log("Invalid DRAWeb2 Date option %s", arg);
        return 1;
    }

    return 0;
}

static int UsrDraweb2IfSystem(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct DRAWEB2CTX *draweb2Ctx = (struct DRAWEB2CTX *)context;
    char *str;
    char *nextSystem;

    draweb2Ctx->anySystem = 1;
    for (str = NULL, nextSystem = StrTok_R(arg, ",", &str); nextSystem;
         nextSystem = StrTok_R(NULL, ",", &str)) {
        if (WildCompare(draweb2Ctx->system, nextSystem)) {
            draweb2Ctx->systemMatch = 1;
            return 0;
        }
    }

    return 1;
}

static int UsrDraweb2IfType(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct DRAWEB2CTX *draweb2Ctx = (struct DRAWEB2CTX *)context;
    char *str;
    char *nextType;

    draweb2Ctx->anyTypes = 1;
    for (str = NULL, nextType = StrTok_R(arg, ",", &str); nextType;
         nextType = StrTok_R(NULL, ",", &str)) {
        if (stricmp(draweb2Ctx->type, nextType) == 0) {
            draweb2Ctx->typeMatch = 1;
            return 0;
        }
    }
    return 1;
}

static int UsrDraweb2URL(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct DRAWEB2CTX *draweb2Ctx = (struct DRAWEB2CTX *)context;
    struct UUSOCKET rr, *r = &rr;
    PORT port;
    enum FINDUSERRESULT result;
    char line[100];
    char *equal;
    int month, day, year;
    time_t expires, now;
    struct tm tm;
    BOOL nameMatch;
    int phase;
    char *paction;
    char action[100];
    int rightPlace;
    char fields[1024];
    char sessionId[64];
    char *quote;
    char *slash;
    char *colon;
    BOOL gotSlash;
    char hostCopy[256];
    char useSsl;
    int v1, v2, v3;
    BOOL expired = 0;
    char *draweb2Host = arg;

    UUInitSocket(r, INVALID_SOCKET);

    draweb2Ctx->type[0] = -1;
    draweb2Ctx->type[1] = 0;
    draweb2Ctx->system[0] = 0;
    result = resultInvalid;

    if (draweb2Host == NULL || uip->user == NULL || *uip->user == 0 || strlen(uip->user) > 20 ||
        uip->pass == NULL || strlen(uip->pass) > 10)
        goto Finished;

    draweb2Host = SkipHTTPslashesAt(draweb2Host, &useSsl);

    slash = strchr(draweb2Host, '/');
    gotSlash = slash != NULL;
    if (slash)
        *slash = 0;

    StrCpy3(hostCopy, draweb2Host, sizeof(hostCopy));

    ParseHost(draweb2Host, &colon, NULL, TRUE);
    port = 0;
    if (colon) {
        *colon++ = 0;
        port = atoi(colon);
    }
    if (port == 0)
        port = 80;

    /* By now, the AdminLogin stripped control characters, so let through whatever else remains
       just as Web2 would technically allow anyway
    if (!IsAllAlphameric(user) || !IsAllAlphameric(pass))
        goto Finished;
    */
    result = resultRefused;

    if (!gotSlash)
        slash =
            "/web2/tramp2.exe/log_in?SETTING_KEY=Guest&servers=1home&guest=guest&screen=ezp1.html";

    result = resultInvalid;

    action[0] = 0;
    sessionId[0] = 0;

    for (phase = 0; phase < 3; phase++) {
        if (phase == 1 && action[0] == 0)
            break;

        if (phase == 2 && sessionId[0] == 0)
            break;

        if (gotSlash)
            *slash = 0;
        if (UUConnectWithSource2(r, draweb2Host, port, "DRAWeb2", uip->pInterface, useSsl))
            goto Finished;
        if (gotSlash)
            *slash = '/';

        if (phase == 0) {
            WriteLine(r, "GET %s HTTP/1.0\r\nHost: ", slash);
            UUSendZ(r, hostCopy);
            UUSendZ(r, "\r\n\r\n");
        } else if (phase == 1) {
            if (action[0] == 0)
                break;
            sprintf(fields, "screen=ezp1.html&%s=", draweb2Ctx->userid);
            AddEncodedField(fields, uip->user, NULL);
            strcat(fields, "&pin=");
            AddEncodedField(fields, uip->pass, NULL);
            WriteLine(r, "POST %s HTTP/1.0\r\nHost: ", action);
            UUSendZ(r, hostCopy);
            WriteLine(
                r,
                "\r\nContent-type: application/x-www-form-urlencoded\r\nContent-length: %d\r\n\r\n",
                strlen(fields));
            UUSendZ(r, fields);
        } else {
            strcpy(fields, "buttons=Logout%3Dlog_out&Logout=Logout");
            WriteLine(r, "POST %s/form/%s HTTP/1.0\r\n", slash, sessionId);
            WriteLine(
                r, "Content-type: application/x-www-form-urlencoded\r\nContent-length: %d\r\n\r\n",
                strlen(fields));
            UUSendZ(r, fields);
        }

        expires = 0;
        nameMatch = 0;

        rightPlace = 0;
        for (; ReadLine(r, line, sizeof(line));) {
            if (phase == 0) {
                if (StrIStr(line, "ezp2.html")) {
                    rightPlace = 1;
                    continue;
                }
                if (!rightPlace)
                    continue;
                if ((paction = StrIStr(line, "action=\""))) {
                    paction += 8;
                    quote = strchr(paction, '"');
                    if (quote == NULL)
                        continue;
                    StrCpy3(action, paction, (quote - paction) + 1);
                }
                continue;
            }

            if (phase == 2) {
                continue;
            }

            if (!rightPlace) {
                if (StrIStr(line, "ezp1.html"))
                    rightPlace = 1;
                continue;
            }

            if (!TrimBr(line))
                continue;

            /*
            for (p = line; p = StrIStr(p, "<br"); p++) {
                br = p;
                p = SkipWS(p + 3);
                if (*p == '/')
                    p = SkipWS(p + 1);
                if (*p == '>') {
                    *br = 0;
                    break;
                }
                br = NULL;
            }

            if (br == NULL)
                continue;
            */

            equal = strchr(line, '=');
            if (equal == NULL)
                continue;
            *equal++ = 0;

            if (strcmp(line, "CARDEXPIRATIONDATE") == 0) {
                if (strnicmp(equal, "NEVER", 5) == 0) {
                    /* special value to cover never */
                    expires = 1;
                } else {
                    if (sscanf(equal, "%04d%02d%02d", &year, &month, &day) != 3) {
                        if (sscanf(equal, "%d/%d/%d", &v1, &v2, &v3) != 3)
                            continue;
                        if (draweb2Ctx->dateFormat == dateMDY) {
                            month = v1;
                            day = v2;
                            year = v3;
                        }
                        if (draweb2Ctx->dateFormat == dateDMY) {
                            day = v1;
                            month = v2;
                            year = v3;
                        }
                        if (draweb2Ctx->dateFormat == dateYMD) {
                            year = v1;
                            month = v2;
                            day = v3;
                        }
                    }
                    if (year >= 1900)
                        year -= 1900;
                    if (year < 80)
                        year += 100;
                    memset(&tm, 0, sizeof(tm));
                    tm.tm_mon = month - 1;
                    tm.tm_mday = day;
                    tm.tm_year = year;
                    tm.tm_isdst = -1;
                    expires = mktime(&tm);
                }
                rightPlace++;
            }

            if (strcmp(line, "PATCLASS") == 0) {
                StrCpy3(draweb2Ctx->type, equal, DRAWEB2TYPEMAX);
                rightPlace++;
            }

            if (strcmp(line, "DRA_SESSION_ID") == 0) {
                StrCpy3(sessionId, equal, sizeof(sessionId));
            }

            if (strcmp(line, "LIBRARYSYSTEM") == 0) {
                StrCpy3(draweb2Ctx->system, equal, 17);
                rightPlace++;
            }
        }

        if (phase == 1 && rightPlace > 1) {
            UUtime(&now);
            result = resultValid;
            /* 1 is special value for "never expires" */
            if ((expires == 1) || (expires != 0 && expires >= now))
                expired = 0;
            else
                expired = 1;
        }
        UUStopSocket(r, 0);
    }

Finished:
    /*
    if (logDRAWEB2)
        Log("DRAWEB2 result %d", result);
    */
    if (result == resultValid && expired)
        result = resultExpired;

    uip->result = result;

    return result;
}

static int UsrDraweb2UserID(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct DRAWEB2CTX *draweb2Ctx = (struct DRAWEB2CTX *)context;

    if (draweb2Ctx->seenHost) {
        Log("For DRAWeb2, UserID must appear before URL");
        return 1;
    }

    if (strlen(arg) > 20)
        *(arg + 20) = 0;

    strcpy(draweb2Ctx->userid, "userid");
    if (*arg) {
        strcat(draweb2Ctx->userid, "=&");
        strcat(draweb2Ctx->userid, arg);
    }

    return 0;
}

int FindUserDraweb2(struct TRANSLATE *t,
                    struct USERINFO *uip,
                    char *file,
                    int f,
                    struct FILEREADLINEBUFFER *frb,
                    struct sockaddr_storage *pInterface) {
    struct DRAWEB2CTX *draweb2Ctx, draweb2CtxBuffer;
    struct USRDIRECTIVE udc[] = {
        {"Date",     UsrDraweb2Date,     0},
        {"IfSystem", UsrDraweb2IfSystem, 0},
        {"IfType",   UsrDraweb2IfType,   0},
        {"S",        UsrDraweb2IfSystem, 0},
        {"System",   UsrDraweb2IfSystem, 0},
        {"T",        UsrDraweb2IfType,   0},
        {"Type",     UsrDraweb2IfType,   0},
        {"U",        UsrDraweb2URL,      0},
        {"URL",      UsrDraweb2URL,      0},
        {"Userid",   UsrDraweb2UserID,   0},
        {NULL,       NULL,               0}
    };

    memset(&draweb2CtxBuffer, 0, sizeof(draweb2CtxBuffer));
    draweb2Ctx = &draweb2CtxBuffer;

    draweb2Ctx->dateFormat = dateMDY;

    strcpy(draweb2Ctx->userid, "userid");

    uip->result = resultInvalid;

    uip->skipping = uip->user == NULL || *uip->user == 0;

    uip->result = UsrHandler(t, "DRAWeb2", udc, draweb2Ctx, uip, file, f, frb, pInterface, NULL);

    return uip->result;
}
