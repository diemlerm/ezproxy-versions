#define __UUFILE__ "z3950.c"

#include "common.h"
#include "adminaudit.h"

void DoZ3950Proxy(struct TRANSLATE *t) {
    struct UUSOCKET *s = &t->ssocket, *r = t->wsocketPtr;
    char request[256], *p;
    int len;
    time_t now = 0;
    struct LOGINPORT *lp = t->loginPort;
    const int timeout = 15 * 60;
    BOOL connected = 0;
    struct SESSION *e = NULL;

    UUInitSocket(r, INVALID_SOCKET);

    if (lp == NULL) {
        Log("Reached Z3950 without LoginPort information?");
        return;
    }

    if (lp->z3950Socks) {
        for (p = request, len = 0;; p++) {
            if (UURecv(s, p, 1, 0) <= 0)
                goto Done;
            len++;
            if (len == sizeof(request) || *p == 'x' || (len > 8 && *p == 0))
                break;
        }
    }

    for (e = NULL; e = FindNextSessionByIP(e, &t->sin_addr);) {
        if (e->gm == NULL || GroupMaskOverlap(e->gm, lp->gm) == 0)
            continue;
        if (e->sciFinderAuthorized) {
            if (now == 0)
                UUtime(&now);
            if (e->sciFinderAuthorized + 120 < now)
                continue;
            break;
        }
    }

    if (e == NULL) {
        AuditEvent(t, AUDITZ3950DENIED, NULL, NULL, "Unauthorized IP");
        if (lp->z3950Socks) {
            request[0] = 0;
            request[1] = 0x5b;
            UUSend(s, request, 8, 0);
            UUFlush(s);
        }
        goto Done;
    }

    if (UUConnectWithSource(r, lp->z3950Host, lp->z3950Port, "DoZ3950Proxy", NULL) != 0) {
        AuditEvent(t, AUDITZ3950DENIED, NULL, NULL, "Remote connection failed to %s:%d",
                   lp->z3950Host, lp->z3950Port);
        goto Done;
    }

    if (lp->z3950Socks) {
        request[0] = 0;
        request[1] = 0x5a;
        UUSend(s, request, 8, 0);
        UUFlush(s);
    }

    t->session = e;

    s->timeout = timeout;
    r->timeout = timeout;

    AuditEvent(t, AUDITZ3950CONNECT, e->logUserFull, e, NULL);
    connected = 1;

    ProxyPingPong(t);

Done:
    if (connected)
        AuditEvent(t, AUDITZ3950DISCONNECT, e->logUserFull, e, NULL);

    UUStopSocket(&t->ssocket, UUSSTOPNICE);
    UUStopSocket(r, UUSSTOPNICE);

    EndTranslate(t, 0);
    ReleaseTranslate(t);
}
