#define __UUFILE__ "adminfiddler.c"

#include "common.h"
#include "uustring.h"

#include "adminfiddler.h"

#ifndef WIN32
#include <dirent.h>
#endif

#include <sys/stat.h>

#define READ_8(adr) ((unsigned char)*(adr))
#define READ_16(adr) (READ_8(adr) | (READ_8(adr + 1) << 8))
#define READ_32(adr) (READ_16(adr) | (READ_16((adr) + 2) << 16))

#define WRITE_8(buff, n)                                        \
    do {                                                        \
        *((unsigned char *)(buff)) = (unsigned char)((n)&0xff); \
    } while (0)
#define WRITE_16(buff, n)                                 \
    do {                                                  \
        WRITE_8((unsigned char *)(buff), n);              \
        WRITE_8(((unsigned char *)(buff)) + 1, (n) >> 8); \
    } while (0)
#define WRITE_32(buff, n)                                 \
    do {                                                  \
        WRITE_16((unsigned char *)(buff), (n)&0xffff);    \
        WRITE_16((unsigned char *)(buff) + 2, (n) >> 16); \
    } while (0)

#define FILESIGNATURE 0x04034b50
#define CENTRALDIRECTORYSIGNATURE 0x02014b50
#define ENDCENTRALDIRECTORYSIGNATURE 0x06054b50

struct FileHeader {
    struct FileHeader *next;
    char file[MAX_PATH];
    char zipName[MAX_PATH];
    int idx;
    uint32_t offset;
    BOOL valid;
    char fileHeader[30];
    char centralDirectoryHeader[46];
};

static BOOL DeleteFiles(char *base, char *file, int flags, void *context) {
    char path[MAX_PATH];

    sprintf(path, "%s%s%s", base, DIRSEP, file);
    unlink(path);
    return TRUE;
}

static BOOL CountFiles(char *base, char *file, int flags, void *context) {
    int *i = (int *)context;
    (*i)++;
    return TRUE;
}

static BOOL AddFile(char *base, char *file, int flags, void *context) {
    struct FileHeader **first = (struct FileHeader **)context;
    struct FileHeader *n, *a, *last = NULL;
    char zipName[MAX_PATH];
    int idx;

    sprintf(zipName, "raw/%s", file);

    n = calloc(1, sizeof(*n) + strlen(zipName));
    n->idx = idx;
    strcpy(n->zipName, zipName);
    strcpy(n->file, file);

    WRITE_32(n->fileHeader, FILESIGNATURE);
    WRITE_16(n->fileHeader + 26, strlen(n->zipName));

    WRITE_32(n->centralDirectoryHeader, CENTRALDIRECTORYSIGNATURE);
    WRITE_16(n->centralDirectoryHeader + 28, strlen(n->zipName));

    for (a = *first; a && strcmp(a->zipName, n->zipName) < 0; a = a->next) {
        last = a;
    }

    if (last == NULL) {
        n->next = *first;
        *first = n;
    } else {
        n->next = last->next;
        last->next = n;
    }

    return TRUE;
}

static BOOL FiddlerCleanupDirectory(char *base, char *file, int flags, void *context) {
    char path[MAX_PATH];

    if (FindSessionByKey(file) == NULL) {
        sprintf(path, "%s%s%s", base, DIRSEP, file);
        if (debugLevel > 0) {
            Log("Cleanup %s", path);
        }
        ScanDirectory(path, SCAN_DIRECTORY_FILE, DeleteFiles, NULL);
        rmdir(path);
    }

    return TRUE;
}

void FiddlerCleanup(void) {
    ScanDirectory(FIDDLERDIR, SCAN_DIRECTORY_DIR, FiddlerCleanupDirectory, NULL);
}

static BOOL SendArchive(struct TRANSLATE *t, char *rawDir, char *mimeType) {
    struct UUSOCKET *s = &t->ssocket;
    struct FileHeader *fh = NULL, *p, *next;
    char fn[MAX_PATH];
    unsigned long crc;
    unsigned long centralDirectoryOffset = 0;
    unsigned long offset = 0;
    unsigned long entries = 0;
    char buffer[4096];
    z_stream strm;
    int zResult;
    Bytef *outputBuffer = NULL;
    size_t outputBufferSize = 0;
    size_t newOutputBufferSize;
    struct stat stat;
    struct tm tm;
    uint16_t dosDate;
    uint16_t dosTime;

    ScanDirectory(rawDir, SCAN_DIRECTORY_FILE, AddFile, &fh);

    // See https://users.cs.jmu.edu/buchhofp/forensics/formats/pkzip.html
    // and https://pkware.cachefly.net/webdocs/casestudies/APPNOTE.TXT

    if (t->version) {
        HTTPCode(t, 200, 0);
        WriteLine(s, "Content-Type: %s\r\n", mimeType);
        HeaderToBody(t);
    }

    for (p = fh; p; p = p->next) {
        sprintf(fn, "%s%s%s", rawDir, DIRSEP, p->file);
        int f = UUopenSimpleFD(fn, O_RDONLY | O_BINARY, defaultFileMode);
        if (f < 0) {
            Log("Fiddler unable to open %s: %d", fn, errno);
            continue;
        }

        if (fstat(f, &stat) < 0) {
            UUcloseFD(f);
            Log("Fiddler unable to stat %s: %d", fn, errno);
            continue;
        }

        UUlocaltime_r(&stat.st_mtime, &tm);
        dosDate = (tm.tm_year - 80 << 9) | ((tm.tm_mon + 1) << 5) | (tm.tm_mday);
        dosTime = (tm.tm_hour << 11) | (tm.tm_min << 5) | (tm.tm_sec / 2);

        newOutputBufferSize = stat.st_size + 512;
        if (newOutputBufferSize > outputBufferSize) {
            if (!(outputBuffer = realloc(outputBuffer, newOutputBufferSize))) {
                UUcloseFD(f);
                Log("Fiddler unable to allocate buffer size %d for %s", newOutputBufferSize, fn);
                continue;
            }
            outputBufferSize = newOutputBufferSize;
        }

        crc = 0;
        int len = 0;
        int remain = 0;
        int got;

        strm.zalloc = Z_NULL;
        strm.zfree = Z_NULL;
        strm.opaque = Z_NULL;
        strm.avail_in = 0;
        strm.next_in = Z_NULL;
        strm.next_out = outputBuffer;
        strm.avail_out = outputBufferSize;

        zResult = deflateInit2(&strm, Z_DEFAULT_COMPRESSION, Z_DEFLATED, -MAX_WBITS, 8,
                               Z_DEFAULT_STRATEGY);
        if (zResult != Z_OK)
            PANIC;

        remain = len = stat.st_size;

        while (remain > 0 && (got = read(f, buffer, sizeof(buffer))) > 0) {
            if (got > remain) {
                got = remain;
            }
            crc = crc32(crc, buffer, got);
            strm.next_in = (Bytef *)buffer;
            strm.avail_in = got;
            zResult = deflate(&strm, Z_NO_FLUSH);
            remain -= got;
        }

        UUcloseFD(f);
        strm.avail_in = 0;
        zResult = deflate(&strm, Z_FINISH);
        deflateEnd(&strm);

        size_t compressedLen = strm.total_out;

        WRITE_16(p->fileHeader + 4, 20);  // Version 2.0
        WRITE_16(p->fileHeader + 8, 8);   // Uses deflate
        WRITE_16(p->fileHeader + 10, dosTime);
        WRITE_16(p->fileHeader + 12, dosDate);
        WRITE_32(p->fileHeader + 14, crc);
        WRITE_32(p->fileHeader + 18, compressedLen);
        WRITE_32(p->fileHeader + 22, len);

        WRITE_16(p->centralDirectoryHeader + 4, 20);  // Version 2.0
        WRITE_16(p->centralDirectoryHeader + 6, 20);  // Needs version 2.0
        WRITE_16(p->centralDirectoryHeader + 10, 8);  // Uses deflate
        WRITE_16(p->centralDirectoryHeader + 12, dosTime);
        WRITE_16(p->centralDirectoryHeader + 14, dosDate);
        WRITE_32(p->centralDirectoryHeader + 16, crc);
        WRITE_32(p->centralDirectoryHeader + 20, compressedLen);
        WRITE_32(p->centralDirectoryHeader + 24, len);
        WRITE_32(p->centralDirectoryHeader + 42, offset);  // Offset to local header

        UUSend2(s, p->fileHeader, sizeof(p->fileHeader), p->zipName, strlen(p->zipName), 0);
        UUSend(s, outputBuffer, compressedLen, 0);
        offset += sizeof(p->fileHeader) + strlen(p->zipName) + compressedLen;
        entries++;
        p->valid = TRUE;
    }

    centralDirectoryOffset = offset;

    for (p = fh; p; p = next) {
        if (p->valid) {
            UUSend2(s, p->centralDirectoryHeader, sizeof(p->centralDirectoryHeader), p->zipName,
                    strlen(p->zipName), 0);
            offset += sizeof(p->centralDirectoryHeader) + strlen(p->zipName);
        }
        next = p->next;
        FreeThenNull(p);
    }

    unsigned char endCentralDirectoryHeader[22] = {0};
    WRITE_32(endCentralDirectoryHeader, ENDCENTRALDIRECTORYSIGNATURE);
    WRITE_16(endCentralDirectoryHeader + 8, entries);   // disk entries
    WRITE_16(endCentralDirectoryHeader + 10, entries);  // total entries
    WRITE_32(endCentralDirectoryHeader + 12, offset - centralDirectoryOffset);
    WRITE_32(endCentralDirectoryHeader + 16, centralDirectoryOffset);
    UUSend(s, &endCentralDirectoryHeader, sizeof(endCentralDirectoryHeader), 0);
    FreeThenNull(outputBuffer);
}

void AdminFiddler(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    struct SESSION *e = t->session;

    const int AFSUBMIT = 0;
    const int AFENABLE = 1;
    const int AFDELETE = 2;

    struct FORMFIELD ffs[] = {
        {"submit", NULL, 0, 0, 0},
        {"enable", NULL, 0, 0, 0},
        {"delete", NULL, 0, 0, 0},
        {NULL, NULL, 0, 0}
    };

    char rawDir[MAX_PATH];
    sprintf(rawDir, "%s%s%s", FIDDLERDIR, DIRSEP, e->key);
    int fileCount = 0;
    ScanDirectory(rawDir, SCAN_DIRECTORY_FILE, CountFiles, &fileCount);
    int sessionCount = fileCount / 4;
    char *extension = strrchr(t->url, '.');

    FindFormFields(query, post, ffs);

    if (ffs[AFSUBMIT].value) {
        e->fiddlerActive = ffs[AFENABLE].value && *ffs[AFENABLE].value != 0;
        if (ffs[AFDELETE].value && *ffs[AFDELETE].value) {
            e->nextFiddlerSessionIndex = 0;
            ScanDirectory(rawDir, SCAN_DIRECTORY_FILE, DeleteFiles, NULL);
        }
        LocalRedirect(t, "/fiddler", 303, 0, NULL);
        return;
    }

    if (extension) {
        if (strcmp(extension, ".saz") == 0) {
            SendArchive(t, rawDir, "application/vnd.telerik-fiddler.SessionArchive");
            return;
        } else if (strcmp(extension, ".zip") == 0) {
            SendArchive(t, rawDir, "application/zip");
            return;
        }
    }

    AdminHeader(t, 0, "Fiddler Capture");

    if (!optionFiddler) {
        WriteLine(s, "<p>Option Fiddler is not enabled.</p>\n");
        goto Finished;
    }

    if (sessionCount > 0) {
        time_t now;
        struct tm tm;
        char ts[32];
        UUtime(&now);
        UUlocaltime_r(&now, &tm);
        strftime(ts, sizeof(ts), "%Y%m%d-%H%M%S", &tm);
        WriteLine(s,
                  "<p>%d session%s available for <a href='/fiddler/%s-%s.saz'>download</a>.</p>\n",
                  sessionCount, SIfNotOne(sessionCount), myName, ts);
    } else {
        WriteLine(s, "<p>No sessions available for download.</p>");
    }

    WriteLine(s, "<form method='post'>\n");
    WriteLine(s,
              "<input type='checkbox' name='enable' id='enable' %s> <label for='enable'>Enable "
              "capture (disables automatically if EZproxy is restarted)</label><br>\n",
              e->fiddlerActive ? "checked" : "");
    WriteLine(s,
              "<input type='checkbox' name='delete' id='delete'> <label for='delete'>Delete "
              "captured sessions</label><br>\n");
    WriteLine(s, "<input type='submit' name='submit' value='Apply'>\n");
    WriteLine(s, "</form>\n");

Finished:
    AdminFooter(t, 0);
}
