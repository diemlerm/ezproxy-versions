#ifndef __UNIXRWL_H__
#define __UNIXRWL_H__

#include "common.h"

struct RWL {
    char *name;
    pthread_mutex_t mutex;
    pthread_cond_t condRead;
    pthread_cond_t condWrite;
    pthread_t writeThread;
    int waitRead;
    int waitWrite;
    int activeRead;
    int activeWrite;
};

#endif  // __UNIXRWL_H__
