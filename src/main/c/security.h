#include "common.h"
#include "sqlite3.h"

#define SECURITYDIR "security"
#define SECURITYDB SECURITYDIR DIRSEP "security_v1.db"

extern int securityEvidenceRetention;
extern int securityResolvedRetention;
extern int securityVacuumDay;
extern const char *securityVacuumDayName;
extern int securityPurgeHour;
extern int securityPurgeMin;
extern time_t securityNextPurge;
extern time_t securityPurgeStarted;
extern time_t securityVacuumStarted;
extern BOOL securityForcePurge;
extern BOOL securityForceVacuum;

void SecurityStart();
void SecurityInit();
void SecurityEnqueueLogin(struct TRANSLATE *t, int auditEvent, const char *user, struct SESSION *e);
void SecurityEnqueueRequest(struct TRANSLATE *t);
void SecurityEnqueueUpdateBlocked();
sqlite3 *SecurityGetSharedDatabase();
BOOL SecurityUserBlocked(const char *user);
