/**
 * /restart admin URL to allow an administrator to restart EZproxy.
 */

#define __UUFILE__ "adminrestart.c"

#include "common.h"
#include "uustring.h"

#include "adminrestart.h"

void AdminRestart(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;

    const int ARCONFIRM = 0;
    const int ARPID = 1;

    struct FORMFIELD ffs[] = {
        {"confirm", NULL, 0, 0, 10},
        {"pid", NULL, 0, 0, 32},
        {NULL, NULL, 0, 0}
    };
    BOOL confirmed;

    FindFormFields(query, post, ffs);

    confirmed = ffs[ARCONFIRM].value && stricmp(ffs[ARCONFIRM].value, "restart") == 0 &&
                ffs[ARPID].value && atoi(ffs[ARPID].value) == guardian->cpid;

    AdminHeader(t, 0, "Restart EZproxy");

    if (guardian->gpid == 0) {
        UUSendZ(s, "<p>EZproxy is not running under Guardian; /restart is disabled.</p>\n");
        AdminFooter(t, 0);
        return;
    }

    if (confirmed) {
        UUSendZ(s, "EZproxy will restart in 5 seconds.\n");
        AdminFooter(t, 0);
        UUStopSocket(s, UUSSTOPNICE);
        LogRequest(t, logSPUs, NULL, NULL, NULL);
        SaveHosts();
        Log("Administrative user requested /restart, will restart in 5 seconds");
        SubSleep(5, 0);
        guardian->chargeStop = 1;
        SubSleep(60, 0);
        return;
    }

    UUSendZ(s, "<p>You have requested that " EZPROXYNAMEPROPER " be restarted.</p>\n");
    UUSendZ(s,
            "<p>This release of " EZPROXYNAMEPROPER " does not verify that the " EZPROXYNAMEPROPER
            " configuration is valid.  If there are errors in\n");
    UUSendZ(s, EZPROXYCFG " or any file included by " EZPROXYCFG ", " EZPROXYNAMEPROPER
                          " may shutdown.</p>\n");
    UUSendZ(s, "<p></p><form action='/restart' method='post'>\n");
    WriteLine(s, "<input type='hidden' name='pid' value='%d'>\n", guardian->cpid);
    UUSendZ(
        s, "If you still want " EZPROXYNAMEPROPER " to restart, <label>type RESTART in this box\n");
    UUSendZ(s, "<input type='text' name='confirm' size='8' maxlength='8'></label> then click\n");
    UUSendZ(s, "<input type='submit' value='here'></form>\n");
    AdminFooter(t, 0);
}
