#ifndef __USRCAS_H__
#define __USRCAS_H__

#include "common.h"

enum FINDUSERRESULT FindUserCAS(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                char *file,
                                int f,
                                struct FILEREADLINEBUFFER *frb,
                                struct sockaddr_storage *pInterface,
                                char *casLogin,
                                char *casServiceValidate);

#endif  // __USRCAS_H__
