#ifndef __ADMINCOOKIE_H__
#define __ADMINCOOKIE_H__

#include "common.h"

void AdminCookie(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINCOOKIE_H__
