#ifndef __ADMINFMG_H__
#define __ADMINFMG_H__

#include "common.h"

BOOL FMGHasInst(char *url);
void AdminFMG(struct TRANSLATE *t, char *query, char *post);

#endif  // __ADMINFMG_H__
