#ifndef __EFGENERAL_H__
#define __EFGENERAL_H__

#include "common.h"

struct EXPRESSIONFUNC {
    char *funcName;
    char *(*func)(struct EXPRESSIONCTX *ctx);
};

extern struct EXPRESSIONFUNC expressionFuncs[];

#endif  // __EFGENERAL_H__
