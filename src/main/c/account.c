#define __UUFILE__ "account.c"

#include "common.h"

#ifdef WIN32
/* io.h, fcntl.h, share.h and sys/stat.h needed for _sopen in Win32 */
#include <io.h>
#include <fcntl.h>
#include <share.h>
#include <sys/stat.h>
#include <sys/locking.h>
#endif

struct ACCOUNTCACHE {
    struct ACCOUNTCACHE *next;
    char *user;
    int where;
};

void AccountFileAccess(BOOL lock) {
    return;
#if 0
    if (lock)
        UUAcquireMutex(&mAccountFile);
    else
        UUReleaseMutex(&mAccountFile);
#endif
}

int AccountFindCache(char *user, int where) {
    static struct ACCOUNTCACHE *l = NULL, *s, *p, *n;

    for (s = NULL, p = l; p; s = p, p = p->next) {
        if (stricmp(p->user, user) == 0) {
            if (where >= 0) {
                p->where = where;
            }
            return p->where;
        }
    }

    if (where == -1)
        return -1;

    if (!(n = calloc(1, sizeof(struct ACCOUNTCACHE))))
        PANIC;
    n->next = NULL;
    if (!(n->user = strdup(user)))
        PANIC;
    n->where = where;
    if (s == NULL)
        l = n;
    else
        s->next = n;
    return where;
}

void AccountMakePass(char *md5UserPass, char *user, char *pass) {
    MD5_CTX md5Context;
    unsigned char *u;
    char userCopy[256], passCopy[256];
    char md5Signature[16];

    StrCpy3(userCopy, user, sizeof(userCopy));
    StrCpy3(passCopy, pass, sizeof(passCopy));
    AllLower(userCopy);
    AllLower(passCopy);
    MD5_Init(&md5Context);
    MD5_Update(&md5Context, (unsigned char *)userCopy, strlen(userCopy));
    MD5_Update(&md5Context, (unsigned char *)passCopy, strlen(passCopy));
    MD5_Final((unsigned char *)md5Signature, &md5Context);
    for (u = (unsigned char *)md5Signature; u < (unsigned char *)md5Signature + 16;
         u++, md5UserPass += 2)
        sprintf(md5UserPass, "%02x", *u);
}

enum ACCOUNTFINDRESULT AccountFind(char *user,
                                   char *pass,
                                   char *defaultPass,
                                   char *newPass,
                                   BOOL createIfNotFound) {
    static int accountFile = -1;
    enum ACCOUNTFINDRESULT result = accountNotFound;
    char line[128];
    char *colon = NULL;
    char md5UserPass[33];
    off_t where;
    BOOL tryingCache;

    AccountFileAccess(1);
    result = accountNotFound;

    if (user == NULL) {
        if (accountFile != -1) {
            close(accountFile);
            accountFile = -1;
        }
        goto Done;
    }

    if (accountFile == -1) {
#ifdef WIN32
        accountFile = _sopen(EZPROXYACC, _O_RDWR | _O_CREAT, _SH_DENYNO, _S_IREAD | _S_IWRITE);
#else
        accountFile = MoveFileDescriptorHigh(open(EZPROXYACC, O_RDWR | O_CREAT, defaultFileMode));
#endif
        if (accountFile == INVALID_SOCKET) {
            Log("Unable to open %s: %d", EZPROXYACC, errno);
            result = accountFileNotFound;
            goto Done;
        } else {
            if (debugLevel > 5)
                Log("%" SOCKET_FORMAT " Opened %s, errno=%d", accountFile, EZPROXYACC,
                    (accountFile == INVALID_SOCKET) ? errno : 0);
        }
    }

    where = AccountFindCache(user, -1);
    if (where == -1) {
        tryingCache = 0;
        lseek(accountFile, 0, SEEK_SET);
    } else {
        if (where > 0) {
            tryingCache = 3;
            lseek(accountFile, where - 1, SEEK_SET);
        } else {
            tryingCache = 2;
            lseek(accountFile, 0, SEEK_SET);
        }
    }

    for (;;) {
        if (tryingCache) {
            tryingCache--;
            if (tryingCache == 0) {
                lseek(accountFile, 0, SEEK_SET);
                continue;
            }
        }

        where = lseek(accountFile, 0, SEEK_CUR);
        if (!FileReadLine(accountFile, line, sizeof(line), NULL)) {
            if (tryingCache)
                continue;
            result = accountNotFound;
            if (createIfNotFound)
                break;
            goto Done;
        }
        if (tryingCache == 2) {
            if (line[0] != 0) {
                tryingCache = 1;
                continue;
            }
            continue;
        }

        colon = strchr(line, ':');
        if (colon == NULL)
            continue;
        *colon++ = 0;
        if (stricmp(line, user) != 0)
            continue;
        if (strlen(colon) < 32)
            continue;

        result = strncmp(colon, ACCOUNTDEFAULTMD5, 32) == 0 ? accountDefault : accountMatch;

        if (pass == NULL)
            break;

        if (result == accountDefault) {
            if (defaultPass != NULL && stricmp(pass, defaultPass) == 0)
                break;
        } else {
            AccountMakePass(md5UserPass, user, pass);
            if (strnicmp(md5UserPass, colon, 32) == 0)
                break;
        }
        result = accountWrong;
        goto Done;
    }

    if (result != accountNotFound && newPass == NULL)
        goto Done;

    if (newPass == NULL || (defaultPass != NULL && stricmp(newPass, defaultPass) == 0))
        strcpy(md5UserPass, ACCOUNTDEFAULTMD5);
    else
        AccountMakePass(md5UserPass, user, newPass);
    if (result == accountNotFound) {
        lseek(accountFile, where, SEEK_SET);
        sprintf(line, "%s:%s\n", user, md5UserPass);
        write(accountFile, line, strlen(line));
        result = accountCreated;
    } else {
        lseek(accountFile, where + (colon - line), SEEK_SET);
        write(accountFile, md5UserPass, 32);
    }

Done:
    AccountFileAccess(0);
    return result;
}

void AccountAdd() {}

void AccountChange() {}

void AccountConvert(char *nfi, BOOL overwriteFile) {
    FILE *fi;
    char line[256];
    char *colon, *otherColon;

    if (overwriteFile) {
        AccountFileAccess(1);
        AccountFileAccess(0);
    }

    fi = fopen(nfi, "r");
    if (fi == NULL) {
        fprintf(stderr, "Unable to open file %s\n", nfi);
        exit(1);
    }

    while (fgets(line, sizeof(line), fi)) {
        Trim(line, TRIM_LEAD | TRIM_CTRL);
        if (line[0] == 0 || line[0] == '#' || line[0] == ':')
            continue;
        colon = strchr(line, ':');
        if (colon) {
            *colon++ = 0;
            if (otherColon = strchr(colon, ':'))
                *otherColon = 0;
            if (*colon == 0)
                colon = NULL;
        }
        AccountFind(line, NULL, NULL, colon, 1);
    }
    fclose(fi);

    /* Close account file */
    AccountFind(NULL, NULL, NULL, NULL, 0);
}

void AccountMain(int argc, char **argv, int i) {
    char *cmd = NULL;
    char *nfi = NULL;

    for (i++; i < argc; i++) {
        if (cmd == NULL)
            cmd = argv[i];
        else if (nfi == NULL)
            nfi = argv[i];
    }
    if (cmd == NULL || nfi == NULL) {
        printf("Usage:\n");
        printf("%s account add (file)\n", argv[0]);
        printf("%s account overwrite (file)\n", argv[0]);
        exit(1);
    }

    if (stricmp(cmd, "add") == 0) {
        AccountConvert(nfi, 0);
    }

    if (stricmp(cmd, "overwrite") == 0) {
        AccountConvert(nfi, 1);
    }

    exit(0);
}
