#define __UUFILE__ "ssl.c"

#define OPENSSL_THREAD_DEFINES
#include <openssl/opensslconf.h>
#ifndef OPENSSL_THREADS
#error OpenSSL not compiled with thread support!
#endif

#include "common.h"
#include <openssl/conf.h>
#include <openssl/x509v3.h>
#include <sys/stat.h>

#ifndef WIN32
#include <dirent.h>
#endif

#define CA_LIST "root.pem"

int sslActiveIndex = -1;
X509 *sslActiveCertificate = NULL;

BOOL initSslCalled = 0;

SSL_CTX *sslCtx = NULL;

char *sslPass;

static int s_server_session_id_context = 1;

#ifndef HEADER_DH_H
#include <openssl/dh.h>
#endif

static struct dhparam {
    BIGNUM *(*const prime)(BIGNUM *);
    DH *dh;
    int min;
} dhparams[] = {
    {get_rfc3526_prime_8192, NULL, 6145},
    {get_rfc3526_prime_6144, NULL, 4097},
    {get_rfc3526_prime_4096, NULL, 3073},
    {get_rfc3526_prime_3072, NULL, 2049},
    {get_rfc3526_prime_2048, NULL, 1025},
    {get_rfc2409_prime_1024, NULL, 0   }
};

int RandNumber(int max) {
    int i, j;
    int mask;
    int bit;

    for (mask = 0, bit = 1;; bit <<= 1) {
        mask |= bit;
        if (mask >= max - 1)
            break;
    }

    for (;;) {
        if (RAND_bytes((unsigned char *)&j, sizeof(j)) == 0)
            RAND_pseudo_bytes((unsigned char *)&j, sizeof(j));
        i = j & mask;
        if (i < max)
            return i;
    }
}

/* Log SSL errors */
void LogSsl(char *string) {
    unsigned long l;
    char buf[200];
    const char *file, *data;
    int line, flags;
    unsigned long es;

    if (string) {
        Log("SSL error: %s", string);
    }

    es = CRYPTO_thread_id();

    while ((l = ERR_get_error_line_data(&file, &line, &data, &flags)) != 0) {
        ERR_error_string_n(l, buf, sizeof buf);
        Log("%lu:%s:%s:%d:%s", es, buf, file, line, (flags & ERR_TXT_STRING) ? data : "");
    }
}

BOOL UUSSLActive(void) {
    return sslActiveCertificate != NULL;
}

const char *UUActiveCertCN(void) {
    static char commonName[256] = {0};

    if (sslActiveCertificate == NULL)
        return NULL;

    commonName[0] = 0;
    X509_NAME_get_text_by_NID(X509_get_subject_name(sslActiveCertificate), NID_commonName,
                              commonName, sizeof(commonName) - 1);
    return commonName;
}

/**
 * Returns dynamic string in same format as UUSslSubjectAltNamesFromCRT
 * that must be free'd by caller.
 */
char *UUActiveCertSubjectAltNames(void) {
    if (sslActiveCertificate == NULL) {
        return NULL;
    }
    return UUSslSubjectAltNamesFromCRT(sslActiveCertificate);
}

char *UUActiveCertExpiration(void) {
    static char activeCertExpiration[64] = {0};
    BIO *mem = NULL;

    if (sslActiveCertificate == NULL)
        return NULL;

    if ((mem = BIO_new(BIO_s_mem()))) {
        ASN1_TIME_print(mem, X509_get_notAfter(sslActiveCertificate));
        if (BIO_gets(mem, activeCertExpiration, sizeof(activeCertExpiration)) <= 0)
            activeCertExpiration[0] = 0;
    }

    if (mem)
        BIO_free(mem);

    return activeCertExpiration;
}

/*
static int password_cb(char *buf,int num,int rwflag,void *userdata)
{
    if(num<(int)strlen(sslPass)+1)
        return(0);

    strcpy(buf, sslPass);
    return(strlen(sslPass));
}
*/

static void SslThreadSetup(void);
/* static void SslThreadCleanup(void); */

BOOL UUSslInitialized(void) {
    return sslCtx != NULL;
}

/* Like RAND_add_file, but can include just the end of the file to keep the startup speed
   OK with large files like ezproxy.log
*/
void UURandLoadFilePart(char *file, int max) {
    int f;
    char buffer[2048];
    int len;

    /* Throw in the unitialized buffer off the stack for a potentially more randomness */
    RAND_add(buffer, sizeof(buffer), 0);

    if (file == NULL)
        return;

    if (max < 0)
        max = 20480;

    f = SOPEN(file);
    if (f >= 0) {
        if (lseek(f, -max, SEEK_END) < 0)
            lseek(f, 0, SEEK_SET);
        /* Technically, this could send unitialized data since we probably read less than 1K in
           final packet, but that's OK
        */
        while ((len = read(f, buffer, sizeof(buffer))) > 0) {
            RAND_add(buffer, sizeof(buffer), 0);
            if (max > 0) {
                max -= len;
                if (max <= 0)
                    break;
            }
        }
        close(f);
    }
}

void UUInitSsl(void) {
    if (initSslCalled)
        return;

    UUAcquireMutex(&mInitSsl);

    if (initSslCalled == 0) {
        /* Global system initialization*/
        OPENSSL_no_config();
        SSL_library_init();
        SSL_load_error_strings();
        OpenSSL_add_all_digests();
        OpenSSL_add_all_algorithms();
        OpenSSL_add_all_ciphers();

        /* Load some extra randomness */
        RAND_load_file(EZPROXYRND, -1);
        RAND_load_file(EZPROXYCFG, -1);
        UURandLoadFilePart(LogSPUActiveFilename(NULL, 0), -1);
        RAND_load_file(HOSTFILE, -1);
        UURandLoadFilePart(EZPROXYUSR, -1);
        UURandLoadFilePart(EZPROXYMSG, -1);

        RAND_write_file(EZPROXYRND);

        SslThreadSetup();
        initSslCalled = 1;
    }
    UUReleaseMutex(&mInitSsl);
}

void UURandomForSsl(char *buffer, int len) {
    if (sslCtx == NULL)
        return;
    if (len == -1)
        len = strlen(buffer);
    RAND_add(buffer, len, 0);
}

void ApplyOpenSSLConfCmds(int index, char *fnCrt, SSL_CTX *ctx) {
    if (openSSLConfCmds) {
        SSL_CONF_CTX *cctx = SSL_CONF_CTX_new();
        SSL_CONF_CTX_set_flags(cctx, SSL_CONF_FLAG_FILE | SSL_CONF_FLAG_SERVER |
                                         SSL_CONF_FLAG_CLIENT | SSL_CONF_FLAG_CERTIFICATE |
                                         SSL_CONF_FLAG_SHOW_ERRORS);
        SSL_CONF_CTX_set_ssl_ctx(cctx, ctx);
        struct OPENSSLCONFCMD *occ;
        for (occ = openSSLConfCmds; occ; occ = occ->next) {
            // Do not allow the connection to the license server
            // to be altered by end-users.
            if (index == LICENSE_SERVER_CERT_IDX) {
                continue;
            }
            // index < 0 applies for the outgoing connection which we only
            // allow to be manipulated explicitly by exact index match
            if (index < 0) {
                if (occ->cert != index) {
                    continue;
                }
            } else {
                // For incoming use, allow default rules and
                // index specific rules.
                if (occ->cert != 0 && occ->cert != index) {
                    continue;
                }
            }
            ERR_clear_error();
            char *displayValue = occ->value ? occ->value : "NULL";
            int status;
            if ((status = SSL_CONF_cmd(cctx, occ->cmd, occ->value)) <= 0) {
                Log("SSLOpenSSLConfCmd %s %s failed for %s (%d)", occ->cmd, displayValue, fnCrt,
                    status);
                LogSsl(NULL);
            } else {
                if (debugLevel > 0) {
                    Log("SSLOpenSSLConfCmd %s %s applied to %s", occ->cmd, displayValue, fnCrt);
                }
            }
        }

        ERR_clear_error();
        if (SSL_CONF_CTX_finish(cctx) == 0) {
            Log("SSL_CONF_CTX_finish failed for %s");
            LogSsl(NULL);
        }

        SSL_CONF_CTX_free(cctx);
    }
}

SSL_CTX *UUSslLoadKey(int index,
                      BOOL logErrors,
                      X509 **cert,
                      BOOL cacheCert,
                      enum SSLLOADKEYERROR *slke) {
    const SSL_METHOD *meth;
    SSL_CTX *ctx;
    RSA *rsa;
    /* moved DH ciphers down in the list to address problem connecting to art12.gsu.edu */
    /* original cipher list ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+DH:+EXP:+eNULL */
    /* Sample cipher testing commands:
       40-bit test:  openssl s_client -connect host:port -cipher EXPORT40
       SSLv2 test:   openssl s_client -connect host:port -ssl2
    */
    char fnKey[MAX_PATH], fnCrt[MAX_PATH], fnCa[MAX_PATH];
    BIO *bioCrt = NULL;
    X509 *cert_x509 = NULL, *ca_x509 = NULL, *oldCert;
    struct stat buf;
    int indx;
    const char *name;
    SSL *ssl = NULL;
    static BOOL listCyphersOnce = TRUE;

    struct SslCtxCache {
        struct SslCtxCache *next;
        SSL_CTX *ctx;
        time_t mtime;
        int index;
    } *pscc;
    static struct SslCtxCache *scc = NULL;

    if (slke)
        *slke = SLKENONE;

    /*
         From http://www.openssl.org/docs/apps/ciphers.html ...

         HIGH: ``high'' encryption cipher suites. This currently means those with key lengths larger
       than 128 bits, and some cipher suites with 128-bit keys. [SJE, always available in EZproxy]

         MEDIUM: ``medium'' encryption cipher suites, currently some of those using 128 bit
       encryption. [SJE, always available in EZproxy]

         LOW: ``low'' encryption cipher suites, currently those using 64 or 56 bit encryption
       algorithms but excluding export cipher suites. [SJE, controlled by DisableSSL40bit and
       DisableSSL56bit in EZproxy]

         EXP, EXPORT: export encryption algorithms. Including 40 and 56 bits algorithms. [SJE,
       controlled by DisableSSL40bit in EZproxy]

         EXPORT40: 40 bit export encryption algorithms [SJE, controlled by DisableSSL40bit in
       EZproxy]

         EXPORT56: 56 bit export encryption algorithms. In OpenSSL 0.9.8c and later the set of 56
       bit export ciphers is empty unless OpenSSL has been explicitly configured with support for
       experimental ciphers. [SJE, never available in EZproxy]
         */
    /* strcpy(ciphers, "ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+DH"); */
    // strcpy(ciphers, "HIGH:MEDIUM:LOW:EXP:!ADH:!aNULL");

    if (debugLevel) {
        Log("inbound cipher spec: %s", SSLCipherSuiteInbound);
        Log("outbound cipher spec: %s", SSLCipherSuiteOutbound);
    }

    sprintf(fnKey, "%s%08d.key", SSLDIR, index);
    sprintf(fnCrt, "%s%08d.crt", SSLDIR, index);
    sprintf(fnCa, "%s%08d.ca", SSLDIR, index);
    if (stat(fnCrt, &buf) != 0)
        buf.st_mtime = 0;

    UUAcquireMutex(&mSslCtxCache);

    pscc = NULL;

    if (cacheCert) {
        for (pscc = scc; pscc; pscc = pscc->next) {
            if (pscc->index == index) {
                if (pscc->ctx) {
                    if (buf.st_mtime == pscc->mtime) {
                        UUReleaseMutex(&mSslCtxCache);
                        return pscc->ctx;
                    }
                    SSL_CTX_free(pscc->ctx);
                    pscc->ctx = NULL;
                }
                break;
            }
        }
    }

    UUInitSsl();

    /* Create our context*/
    if (!(meth = TLS_method()))
        PANIC;
    if (!(ctx = SSL_CTX_new(meth)))
        PANIC;
    // Chrome, Edge, Firefox were all tested on 2022-03-12 to confirm
    // that none of their current releases support anything
    // earlier than TLS 1.2. This may be overridden in config.txt
    // by adding either or both of:
    // SSLOpenSSLConfCmd -inbound MinProtocol TLSv1.1
    // SSLOpenSSLConfCmd -outbound MinProtocol TLSv1.1
    SSL_CTX_set_min_proto_version(ctx, TLS1_2_VERSION);

    /*
     *  We need to enable SSL_MODE_ACCEPT_MOVING_WRITE_BUFFER since we are buffering some
     * of the SSL data, which causes the write buffer to legitimately shift but can
     * make OpenSSL very angry if it hasn't been warned that we are doing this.
     */
    SSL_CTX_set_mode(ctx, SSL_CTX_get_mode(ctx) | SSL_MODE_ACCEPT_MOVING_WRITE_BUFFER);

    /*
     *  If we don't want to load any keys and certificates, skip out now without
     * applying cipher list.  This ensures that extreme settings won't block
     * license server communication or less secure content providers.
     */
    if (index < 0) {
        if (index == -1) {
            if (!SSL_CTX_set_cipher_list(ctx, SSLCipherSuiteOutbound)) {
                LogSsl("Couldn't set inbound ciphers");
                exit(1);
            }
        }
        ApplyOpenSSLConfCmds(index, "outgoing", ctx);
        UUReleaseMutex(&mSslCtxCache);
        return ctx;
    }

    if (!SSL_CTX_set_cipher_list(ctx, SSLCipherSuiteInbound)) {
        LogSsl("Couldn't set cipher list");
        exit(1);
    }

    /* Load our keys and certificates*/

    cert_x509 = NULL;

    // Leave bioCrt available so it can be scanned later for dhparam and ECDH curve
    if ((bioCrt = BIO_new(BIO_s_file())) == NULL || BIO_read_filename(bioCrt, fnCrt) <= 0 ||
        (cert_x509 = PEM_read_bio_X509(bioCrt, NULL, NULL, NULL)) == NULL ||
        !SSL_CTX_use_certificate(ctx, cert_x509)) {
        if (logErrors) {
            Log("Tried %s", fnCrt);
            LogSsl("Couldn't read certificate file");
        }
        if (cert_x509) {
            X509_free(cert_x509);
            cert_x509 = NULL;
        }
        if (bioCrt) {
            BIO_free(bioCrt);
            bioCrt = NULL;
        }
        if (ctx) {
            SSL_CTX_free(ctx);
            ctx = NULL;
        }
        UUReleaseMutex(&mSslCtxCache);
        if (slke)
            *slke = SLKEBADCRT;
        return NULL;
    }

    if (cert) {
        oldCert = *cert;
        *cert = cert_x509;
        if (oldCert)
            X509_free(oldCert);
    } else
        X509_free(cert_x509);

    /*
    if(!(SSL_CTX_use_certificate_file(ctx, fnCrt, SSL_FILETYPE_PEM))) {
        SSL_CTX_free(ctx);
        if (logErrors) {
            Log("Tried %s", fnCrt);
            LogSsl("Couldn't read certificate file");
        }
        return NULL;
    }
    */

    ERR_clear_error();

    if (!SSL_CTX_use_PrivateKey_file(ctx, fnKey, SSL_FILETYPE_PEM)) {
        const char *file, *data;
        int line, flags;
        int l;

        if (slke) {
            *slke = SLKEBADKEY;

            if ((l = ERR_peek_error_line_data(&file, &line, &data, &flags)) != 0) {
                char buf[200];
                ERR_error_string_n(l, buf, sizeof buf);
                if (strstr(buf, "key values mismatch"))
                    *slke = SLKECRTKEYMISMATCH;
            }
        }

        if (bioCrt) {
            BIO_free(bioCrt);
            bioCrt = NULL;
        }

        SSL_CTX_free(ctx);
        if (logErrors) {
            Log("Tried %s", fnKey);
            LogSsl("Couldn't read key file");
        }
        UUReleaseMutex(&mSslCtxCache);
        return NULL;
    }

    if (FileExists(fnCa)) {
        BIO *bioCa = NULL;
        if ((bioCa = BIO_new(BIO_s_file())) != NULL && BIO_read_filename(bioCa, fnCa) > 0) {
            while ((ca_x509 = PEM_read_bio_X509(bioCa, NULL, NULL, NULL)) != NULL) {
                if (!SSL_CTX_add_extra_chain_cert(ctx, ca_x509)) {
                    X509_free(ca_x509);
                    ca_x509 = NULL;
                }
            }
            if (bioCa) {
                BIO_free(bioCa);
                bioCa = NULL;
            }
        } else {
            Log("Unable to open %s", fnCa);
        }
    }

    /* Load the CAs we trust*/
    /*
    if(!(SSL_CTX_load_verify_locations(ctx,"ca-bundle.crt",0))) {
        LogSsl("Couldn't read CA list");
        exit(1);
    }
    */

    /* This shouldn't be needed in 0.9.6 or later
    SSL_CTX_set_verify_depth(ctx, 1);
    */

    if (SSL_CTX_need_tmp_RSA(ctx)) {
        rsa = RSA_generate_key(512, RSA_F4, NULL, NULL);
        if (!SSL_CTX_set_tmp_rsa(ctx, rsa)) {
            LogSsl("RSA_generate_key");
            exit(1);
        }
        RSA_free(rsa);
    }

    // If DH parameters were included in certificate file, attach to the context.
    BIO_reset(bioCrt);
    DH *dh = NULL;
    if ((dh = PEM_read_bio_DHparams(bioCrt, NULL, NULL, NULL))) {
        if (debugLevel > 0) {
            const BIGNUM *p;
            const BIGNUM *q;
            const BIGNUM *g;
            DH_get0_pqg(dh, &p, &q, &g);
            Log("Custom DH parameters (%d bits) loaded for %s", BN_num_bits(p), fnCrt);
        }
        SSL_CTX_set_tmp_dh(ctx, dh);
    }

    // If the ECDH curve is included in the certificate file, attach to context.
    BIO_reset(bioCrt);
    EC_GROUP *ecparams;
    int nid;
    EC_KEY *eckey;
    if ((ecparams = PEM_read_bio_ECPKParameters(bioCrt, NULL, NULL, NULL)) &&
        (nid = EC_GROUP_get_curve_name(ecparams)) && (eckey = EC_KEY_new_by_curve_name(nid))) {
        SSL_CTX_set_tmp_ecdh(ctx, eckey);
        if (debugLevel > 0) {
            Log("ECDH curve %s loaded for %s", OBJ_nid2sn(nid), fnCrt);
        }
    } else {
        // Otherwise, enable auto curve selection
        SSL_CTX_set_ecdh_auto(ctx, 1);
    }

    if (bioCrt) {
        BIO_free(bioCrt);
        bioCrt = NULL;
    }

    ApplyOpenSSLConfCmds(index, fnCrt, ctx);
    if (debugLevel && (listCyphersOnce == TRUE)) {
        listCyphersOnce = FALSE;
        Log("SSL cipher list in order by preference:");
        ssl = SSL_new(ctx);
        name = SSL_get_cipher_list(ssl, 0);
        for (indx = 0; name != NULL; indx++) {
            Log("  Cipher %-4d %s", indx, name);
            name = SSL_get_cipher_list(ssl, indx);
        }
        SSL_free(ssl);
        ssl = NULL;
    }

    if (cacheCert) {
        if (pscc == NULL) {
            if (!(pscc = calloc(1, sizeof(*pscc))))
                PANIC;
            pscc->next = scc;
            scc = pscc;
            pscc->index = index;
        }

        pscc->ctx = ctx;
        pscc->mtime = buf.st_mtime;
    }

    UUReleaseMutex(&mSslCtxCache);

    return ctx;
}

void UUInitSslKey(int newIndex, BOOL force) {
    SSL_CTX *ctx;
    int f;
    char line[256];
    int fileActiveIndex = -1;

    UUAcquireMutex(&mInitSslKey);

    f = SOPEN(SSLDIR "active");
    if (f >= 0) {
        FileReadLine(f, line, sizeof(line), NULL);
        close(f);
        fileActiveIndex = atoi(line);
        if (fileActiveIndex < 0)
            fileActiveIndex = 0;
    }

    if (newIndex <= 0) {
        if (sslCtx == NULL)
            newIndex = fileActiveIndex;
        else
            newIndex = sslActiveIndex;
    }

    if (sslCtx != NULL && newIndex == sslActiveIndex && force == 0)
        goto ReleaseMutex;

    if (newIndex <= 0) {
        static char notAvail = 1;
        if (UUSslEnabled()) {
            if (notAvail) {
                Log("SSL is not configured so https proxying will not be available");
                notAvail = 0;
            }
        }
        goto ReleaseMutex;
    }

    if (ctx = UUSslLoadKey(newIndex, 1, &sslActiveCertificate, 0, NULL)) {
        SSL_CTX_set_session_id_context(ctx, (void *)&s_server_session_id_context,
                                       sizeof(s_server_session_id_context));

        /* Log("Certificate %d active", newIndex); */
        sslActiveIndex = newIndex;
        sslCtx = ctx;
    }

ReleaseMutex:
    UUReleaseMutex(&mInitSslKey);
}

BOOL UUSslAvailable(void) {
    if (sslCtx)
        return 1;

    UUInitSslKey(0, 0);
    return sslCtx != NULL;
}

int UURecvSsl(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    int totalResult = 0;
    int status1, status2;
    time_t lastRecv;
    const struct SOCKETBUFFER *sbp;
    void *msg;
    int len;

    UUtime(&lastRecv);

    for (sbp = sbsi->sb; sbp < sbsi->sb + sbsi->count; sbp++) {
        for (msg = sbp->msg, len = sbp->len; len > 0;) {
            s->sslLast = SSLLASTRECV;
            status1 = SSL_read(s->ssl, msg, len);

            if (status1 > 0) {
                s->sslLastStatus = SSL_ERROR_NONE;
                UUtime(&lastRecv);
                s->lastActivity = lastRecv;
                msg = (void *)((char *)msg + status1);
                len -= status1;
                totalResult += status1;
                continue;
            }

            status2 = SSL_get_error(s->ssl, status1);
            s->sslLastStatus = status2;

            if (status2 == SSL_ERROR_ZERO_RETURN) {
                goto Failed;
            }

            if (status2 == SSL_ERROR_SYSCALL && ERR_get_error() == 0 && status1 == -1 &&
                (socket_errno == EINTR || socket_errno == EAGAIN))
                continue;

            if (status2 != SSL_ERROR_WANT_READ && status2 != SSL_ERROR_WANT_WRITE) {
                goto Failed;
            }

            s->lastRecvWant = status2;

            if (totalResult > 0)
                goto NoteReceived;

            if (uusflags & UUSNOWAIT) {
                if (totalResult > 0)
                    goto NoteReceived;
                SetErrno(WSAEWOULDBLOCK);
                return -1;
            }

            s->lastRecvWant = 0;

            if (UUWant(s->o, status2, lastRecv + s->timeout)) {
                s->savedRecvErrno = errno;
                goto Failed;
            }
        }
    }

    goto NoteReceived;

Failed:
    s->recvOK = 0;
    if (totalResult > 0)
        goto NoteReceived;
    SetErrno(s->savedRecvErrno);
    return -1;

NoteReceived:
    if (totalResult > 0) {
        UUtime(&s->lastActivity);
        s->recvCount += totalResult;
    }

    return totalResult;
}

int UUSendSsl(struct UUSOCKET *s, const struct UUSOCKETBUFFERS *sbsi, int uusflags) {
    int status1, status2;
    int totalResult = 0;
    time_t lastSend;
    struct SOCKETBUFFER *sbp;
    /* Make sure there is enough room to buffer a chunk header and a full buffered block */
    char sendBuffer[UUSOCKETSENDBUFFERSIZE + sizeof(s->sendChunkHeader)];
    char *msg;
    size_t len;
    char *p;
    size_t count, remain;
    struct UUSOCKETBUFFERS sbs;

    UUSocketBuffersCopy(&sbs, sbsi);

    UUtime(&lastSend);

    /* SSL_write has odd rules that drive the need to move the data
       into an intermediate buffer.  Review SSL_write for more information
       on how its behavior is different than write when only partial amounts
       get written (namely that on a partial write, you must be sure you give
       back all the unwritten data that it had before)
    */
Again:
    remain = sizeof(sendBuffer);
    sbp = sbs.sb + sbs.count - 1;
    /* If there is only one buffer or if the last buffer is bigger than our local one, just use
     * as-is */
    if (sbs.count == 1 || (sbs.count > 0 && sbp->len >= remain)) {
        msg = sbp->msg;
        len = sbp->len;
    } else {
        p = sendBuffer;
        for (; remain > 0 && sbp >= sbs.sb; sbp--) {
            if (sbp->len == 0)
                continue;
            count = UUMIN(sbp->len, remain);
            memmove(p, sbp->msg, count);
            p += count;
            remain -= count;
        }

        /* If p didn't advance, we have nothing more to write and we can return */
        if (p == sendBuffer)
            return totalResult;
        msg = sendBuffer;
        len = p - sendBuffer;
    }

Retry:
    s->sslLast = SSLLASTSEND;
    status1 = SSL_write(s->ssl, msg, len);
    if (status1 <= 0)
        goto Error;
    UUtime(&lastSend);
    s->lastActivity = lastSend;
    s->sslLastStatus = SSL_ERROR_NONE;
    s->sslWriteWanting = 0;
    s->sendCount += status1;
    totalResult += status1;
    UUSocketBuffersRemoveSendBytes(&sbs, status1);

    goto Again;

Error:
    status2 = SSL_get_error(s->ssl, status1);
    s->sslLastStatus = status2;

    if ((status2 == SSL_ERROR_SYSCALL) && (ERR_get_error() == 0) && (status1 == -1) &&
        (socket_errno == EINTR || socket_errno == EAGAIN))
        goto Retry;

    if (status2 != SSL_ERROR_WANT_READ && status2 != SSL_ERROR_WANT_WRITE) {
        if (debugLevel >= 50)
            LogSsl("UUSendSsl marking socket invalid on error");
        s->sendOK = 0;
        s->savedSendErrno = EIO;
        if (totalResult > 0)
            return totalResult;
        SetErrno(s->savedSendErrno);
        return -1;
    }

    s->sslWriteWanting = 1;
    s->lastSendWant = status2;

    if (uusflags & UUSNOWAIT) {
        if (totalResult > 0)
            return totalResult;
        SetErrno(WSAEWOULDBLOCK);
        return -1;
    }

    s->lastSendWant = 0;

    if (UUWant(s->o, status2, lastSend + s->timeout) == 0) {
        goto Again;
    }

    s->savedSendErrno = WSAETIMEDOUT;

    if (debugLevel >= 50)
        Log("UUSendSsl marking socket invalid on timeout %d", s->timeout);

    s->sendOK = 0;
    if (totalResult > 0)
        return totalResult;
    SetErrno(s->savedSendErrno);
    return -1;
}

/* Before 2004-07-08, EZproxy would present its certificate in outgoing request; it no longer does
   unless expressly told to do so
*/
void UUInitSocketSsl(struct UUSOCKET *s, SOCKET o, int flags, int sslIdx, const char *host) {
    int (*ssl_func)(SSL *s) = (flags & UUSSLCONNECT ? SSL_connect : SSL_accept);
    int status1, status2;
    time_t start;
    SSL_CTX *baseCtx = NULL;
    static SSL_CTX *sslCtxNoCert = NULL;

    UUInitSocketCore(s, STSSL);
    s->o = o;
    s->acceptedHttp = 0;
    /* Assume things are bad until they are proven good */
    s->sendOK = s->recvOK = 0;
    s->savedSendErrno = EINVAL;
    s->savedRecvErrno = EINVAL;
    s->ssl = NULL;

    if (sslIdx == 0) {
        if (flags & UUSSLCONNECT) {
            if (UUSslEnabled() == 0)
                goto Finished;
            if (sslCtxNoCert == NULL) {
                UUAcquireMutex(&mInitSslKey);
                /* Between the check and the mutex acquisition, the sslCtx may have been created by
                another thread, so don't recreate in that case */
                if (sslCtxNoCert) {
                    UUReleaseMutex(&mInitSslKey);
                } else {
                    sslCtxNoCert = UUSslLoadKey(-1, 1, NULL, 0, NULL);
                    UUReleaseMutex(&mInitSslKey);
                    if (sslCtxNoCert == NULL) {
                        Log("Unable to initialize SSL (2)");
                        goto Finished;
                    }
                }
            }
            baseCtx = sslCtxNoCert;
        } else {
            if (sslCtx == NULL) {
                UUInitSslKey(0, 0);
                if (sslCtx == NULL) {
                    Log("Unable to initialize SSL");
                    goto Finished;
                }
            }
            baseCtx = sslCtx;
        }
    } else {
        baseCtx = UUSslLoadKey(sslIdx, 1, NULL, 1, NULL);
        if (baseCtx == NULL)
            goto Finished;
    }

    s->ssl = SSL_new(baseCtx);

    if (s->ssl == NULL) {
        LogSsl("SSL_new failed");
        goto Finished;
    }

    if (host != NULL && (flags & UUSSLCONNECT)) {
        SSL_set_tlsext_host_name(s->ssl, host);
    }

    if (sslHonorCipherOrder && baseCtx && !(flags & UUSSLCONNECT)) {
        SSL_set_options(s->ssl, SSL_OP_CIPHER_SERVER_PREFERENCE);
    }

    s->bio = BIO_new_socket(o, BIO_NOCLOSE);
    if (s->bio == NULL) {
        LogSsl("BIO_new_socket failed");
        goto Finished;
    }

    SSL_set_bio(s->ssl, s->bio, s->bio);

    if (flags & UUSSLCONNECT) {
        SSL_set_connect_state(s->ssl);
    } else {
        SSL_set_accept_state(s->ssl);
    }

    UUtime(&start);

    for (;;) {
        status1 = (*ssl_func)(s->ssl);
        if (status1 > 0)
            break;
        if (status1 < 0) {
            status2 = SSL_get_error(s->ssl, status1);
            if (status2 == SSL_ERROR_SYSCALL && ERR_get_error() == 0 && status1 == -1 &&
                (socket_errno == EINTR || socket_errno == EAGAIN))
                continue;
            if (UUWant(s->o, status2, start + 60) == 0)
                continue;
            s->savedSendErrno = errno;
            s->savedRecvErrno = errno;
        }

        if ((flags & UUSSLHTTP) && ERR_GET_REASON(ERR_peek_error()) == SSL_R_HTTP_REQUEST) {
            s->acceptedHttp = 1;
            s->socketType = STSTREAM;
            s->sendOK = s->recvOK = 1;
            s->savedSendErrno = 0;
            s->savedRecvErrno = 0;
            SSL_set_shutdown(s->ssl, SSL_SENT_SHUTDOWN | SSL_RECEIVED_SHUTDOWN);
            if (SSL_shutdown(s->ssl) == 0)
                SSL_shutdown(s->ssl);
        } else {
            if (debugLevel > 0)
                LogSsl("SSL connect error");
        }

        SSL_free(s->ssl);
        s->ssl = NULL;
        goto Finished;
    }

    s->sslWriteWanting = 0;
    s->sendOK = s->recvOK = 1;
    s->savedSendErrno = 0;
    s->savedRecvErrno = 0;

    /* check_cert_chain(ssl,HOST);   */

Finished:;
    /* We now cache the extra SSL_CTX, so don't free them even when sslIdx defined */
    /*
    if (sslIdx && baseCtx) {
        SSL_CTX_free(baseCtx);
        baseCtx = NULL;
    }
    */
}

static UUMUTEX *sslMutexes;

void SslLockingCallback(int mode, int type, const char *file, int line) {
    if (mode & CRYPTO_LOCK) {
        UUAcquireMutex(sslMutexes + type);
    } else {
        UUReleaseMutex(sslMutexes + type);
    }
}

#ifndef WIN32
unsigned long SslThreadID(void) {
    return (unsigned long)pthread_self();
}
#endif

void SslThreadSetup(void) {
    int i;
    char name[32];

    sslMutexes = OPENSSL_malloc(CRYPTO_num_locks() * sizeof(UUMUTEX));
    if (sslMutexes == NULL)
        PANIC;

    for (i = 0; i < CRYPTO_num_locks(); i++) {
        sprintf(name, "sslMutexes%d", i);
        UUInitMutex(sslMutexes + i, name);
    }

    CRYPTO_set_locking_callback(SslLockingCallback);
#ifndef WIN32
    CRYPTO_set_id_callback((unsigned long (*)())SslThreadID);
#endif
}

void UUSslFilesFreeList(struct UUSSLFILES *head) {
    struct UUSSLFILES *p, *n;

    if (head == NULL)
        return;

    for (p = head->flink; p != head; p = n) {
        n = p->flink;
        FreeThenNull(p);
    }
    FreeThenNull(head);
}

struct UUSSLFILES *UUSslFiles(int *highest, BOOL returnList) {
    char *fn;
    char numTest[16];
    char *period;
    int cur;
    int flags;
    struct UUSSLFILES *head = NULL, *p, *n;
#ifdef WIN32
    char searchName[64];
    WIN32_FIND_DATA fileData;
    HANDLE hSearch;
#else
    DIR *dir;
    struct dirent *dirent;
#endif

    if (returnList) {
        if (!(head = calloc(1, sizeof(struct UUSSLFILES))))
            PANIC;
        head->flink = head->blink = head;
    }

    if (highest)
        *highest = 0;

    UUmkdir(SSLDIR);
#ifdef WIN32
    sprintf(searchName, "%s*.*", SSLDIR);
    hSearch = FindFirstFile(searchName, &fileData);
    if (hSearch == INVALID_HANDLE_VALUE) {
        Log("Unable to access ssl directory: %d", GetLastError());
        return head;
    }
    do {
        fn = fileData.cFileName;
#else
    dir = opendir(SSLDIR);
    if (dir == NULL) {
        Log("Unable to access ssl directory: %d", errno);
        return head;
    }

    while (dirent = readdir(dir)) {
        fn = dirent->d_name;
#endif
        period = strchr(fn, '.');
        if (period == NULL || period != fn + 8)
            continue;
        flags = 0;
        if (stricmp(period, ".cnf") == 0)
            flags = UUSSLFILESCNF;
        if (stricmp(period, ".err") == 0)
            flags = UUSSLFILESERR;
        if (stricmp(period, ".key") == 0)
            flags = UUSSLFILESKEY;
        if (stricmp(period, ".csr") == 0)
            flags = UUSSLFILESCSR;
        if (stricmp(period, ".crt") == 0)
            flags = UUSSLFILESCRT;
        if (flags == 0)
            continue;
        StrCpy3(numTest, fn, sizeof(numTest));
        numTest[8] = 0;
        if (IsAllDigits(numTest)) {
            cur = atoi(numTest);
            if (highest && cur > *highest)
                *highest = cur;
            if (returnList) {
                for (p = head->flink;; p = p->flink) {
                    if (p == head || p->serial < cur) {
                        if (!(n = calloc(1, sizeof(struct UUSSLFILES))))
                            PANIC;
                        n->flink = p;
                        n->blink = p->blink;
                        n->serial = cur;
                        (n->flink)->blink = (n->blink)->flink = n;
                        p = n;
                        break;
                    }
                    if (p->serial == cur)
                        break;
                }
                p->flags |= flags;
            }
        }
#ifdef WIN32
    } while (FindNextFile(hSearch, &fileData));
    FindClose(hSearch);
#else
    }
    closedir(dir);
#endif

    return head;
}

static void UUSslCnfAddField(FILE *f, char *field, char *value) {
    if (value && *value) {
        fprintf(f, "%s=%s\n", field, value);
    }
}

int UUSslCnf(int keysize,
             char *algorithm,
             char *country,
             char *state,
             char *locality,
             char *organization,
             char *unit,
             char *cn,
             char *email,
             char *alt1,
             char *alt2) {
    char fnCnf[64], fnKey[64];
    int fd;
    FILE *f;
    int serial;
    time_t now;
    char asciiDate[MAXASCIIDATE];

    if (alt1 != NULL && *alt1 == 0) {
        alt1 = NULL;
    }
    if (alt2 != NULL && *alt2 == 0) {
        alt2 = NULL;
    }
    if (alt1 == NULL && alt2 != NULL) {
        alt1 = alt2;
        alt2 = NULL;
    }

    UUSslFiles(&serial, 0);
    serial++;

    sprintf(fnCnf, "%s%08d.cnf", SSLDIR, serial);
    sprintf(fnKey, "%s%08d.key", SSLDIR, serial);

    fd = UUopenCreateFD(fnCnf, O_CREAT | O_WRONLY | O_TRUNC, defaultFileMode);
    if (fd < 0) {
        Log("Unable to create temporary file %s: %d", fnCnf, errno);
        unlink(fnCnf); /* in case file actually got created */
        return -1;
    }

    f = fdopen(fd, "w");
    if (f == NULL) {
        close(fd);
        Log("Unable to create temporary file %s: %d", fnCnf, errno);
        unlink(fnCnf); /* in case file actually got created */
        return -1;
    }

    // After this point, fd is controlled by f and calling fclose on f will close fd

    UUtime(&now);
    ASCIIDate(&now, asciiDate);
    fprintf(f,
            "#created %s\n"
            "[ req ]\n"
            "default_bits=%d\n"
            "default_md=%s\n"
            "default_keyfile=%s\n"
            "distinguished_name=req_distinguished_name\n"
            "attributes=req_attributes\n"
            "req_extensions=v3_req\n"
            "x509_extensions=v3_ca\n"
            "string_mask=nombstr\n"
            "prompt=no\n"
            "\n"
            "[ req_distinguished_name ]\n",
            asciiDate, keysize, algorithm, fnKey);

    UUSslCnfAddField(f, "CN", cn);
    UUSslCnfAddField(f, "C", country);
    UUSslCnfAddField(f, "ST", state);
    UUSslCnfAddField(f, "L", locality);
    UUSslCnfAddField(f, "O", organization);
    UUSslCnfAddField(f, "OU", unit);
    UUSslCnfAddField(f, "emailAddress", email);

    fprintf(f,
            "\n"
            "[ req_attributes ]\n"
            "#challengePassword=\n"
            "#unstructuredName=\n"
            "\n"
            "[ v3_req ]\n"
            "basicConstraints=CA:FALSE\n"
            "keyUsage=nonRepudiation, digitalSignature, keyEncipherment\n");

    if (alt1) {
        fprintf(f, "subjectAltName=@alternate_names\n");
    }

    fprintf(f,
            "\n"
            "[ v3_ca ]\n"
            "subjectKeyIdentifier=hash\n"
            "authorityKeyIdentifier=keyid:always,issuer:always\n"
            "basicConstraints=CA:true\n");

    if (alt1) {
        fprintf(f,
                "subjectAltName=@alternate_names\n"
                "\n"
                "[alternate_names]\n"
                "DNS.1=%s\n",
                alt1);
        if (alt2) {
            fprintf(f, "DNS.2=%s\n", alt2);
        }
    }

    if (fprintf(f, "\n") <= 0) {
        Log("Error while writing to %s: %d", fnCnf, errno);
        fclose(f);
        f = NULL;
        return -1;
    }

    fclose(f);
    return serial;
}

BOOL UUSslEnabled(void) {
    static int warned = 0;

    if (someKey)
        return 1;

    if (warned == 0) {
        if (someKey)
            return 1;
        warned = 1;
        Log("Due to export restrictions, SSL (https) features are\
  disabled. To evaluate SSL features, request a test license\
  from " EZPROXYHELPEMAIL ".");
    }
    return 0;
}

char *UUSslKeyCreated(int index, char *dateBuffer, size_t dateBufferSize) {
    char fn[MAX_PATH];
    char *created;
    int f;
    char line[80];

    created = "unknown";
    sprintf(fn, "%s%08d.cnf", SSLDIR, index);

    f = SOPEN(fn);
    if (f >= 0) {
        if (FileReadLine(f, line, sizeof(line), NULL) && strnicmp(line, "#created ", 9) == 0)
            created = SkipWS(line + 9);
        close(f);
    }

    StrCpy3(dateBuffer, created, dateBufferSize);
    return dateBuffer;
}

/**
 * This routine returns a dynamic string with all of the subjectAltNames found
 * in null byte separated form (e.g., name1\0name2\0\0).  The caller is
 * responsible for freeing the return value.  Returns NULL if no values found.
 */
static char *UUSslSubjectAltNames(STACK_OF(GENERAL_NAME) * san_names) {
    if (san_names == NULL) {
        return NULL;
    }
    char *result = NULL;
    int resultLen = 0;
    int i;
    int san_names_nb = sk_GENERAL_NAME_num(san_names);
    // Check each name within the extension
    for (i = 0; i < san_names_nb; i++) {
        const GENERAL_NAME *current_name = sk_GENERAL_NAME_value(san_names, i);

        if (current_name->type != GEN_DNS) {
            continue;
        }

        char *dns_name = (char *)ASN1_STRING_data(current_name->d.dNSName);
        size_t len = strlen(dns_name);
        // Make sure there isn't an embedded NUL character in the DNS name
        if (ASN1_STRING_length(current_name->d.dNSName) != len) {
            continue;
        }

        if (!(result = realloc(result, resultLen + len + 2)))
            PANIC;
        strcpy(result + resultLen, dns_name);
        resultLen += len + 1;
    }

    if (result) {
        *(result + resultLen) = 0;
    }

    return result;
}

/**
 * This routine returns a dynamic string with all of the subjectAltNames found
 * in null byte separated form (e.g., name1\0name2\0\0).  The caller is
 * responsible for freeing the return value.  Returns NULL if no values found.
 */
char *UUSslSubjectAltNamesFromCSR(X509_REQ *csr) {
    STACK_OF(X509_EXTENSION) *exts = X509_REQ_get_extensions(csr);
    STACK_OF(GENERAL_NAME) *san_names = X509V3_get_d2i(exts, NID_subject_alt_name, NULL, NULL);
    char *result = UUSslSubjectAltNames(san_names);
    sk_GENERAL_NAME_pop_free(san_names, GENERAL_NAME_free);
    sk_X509_EXTENSION_pop_free(exts, X509_EXTENSION_free);
    return result;
}

/**
 * This routine returns a dynamic string with all of the subjectAltNames found
 * in null byte separated form (e.g., name1\0name2\0\0).  The caller is
 * responsible for freeing the return value.  Returns NULL if no values found.
 */
char *UUSslSubjectAltNamesFromCRT(X509 *crt) {
    STACK_OF(GENERAL_NAME) *san_names = X509_get_ext_d2i(crt, NID_subject_alt_name, NULL, NULL);
    char *result = UUSslSubjectAltNames(san_names);
    sk_GENERAL_NAME_pop_free(san_names, GENERAL_NAME_free);
    return result;
}
