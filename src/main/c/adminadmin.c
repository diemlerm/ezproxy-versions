#define __UUFILE__ "adminadmin.c"

#include "common.h"
#include "uustring.h"

#include "adminadmin.h"
#include "adminaudit.h"
#include "admincas.h"
#include "adminmv.h"
#include "admintoken.h"
#include "identifier.h"

static void AdminAdminActivity(struct UUSOCKET *s, BOOL *show) {
    if (*show) {
        UUSendZ(s, "<h2>Current Activity</h2>\n");
        *show = FALSE;
    }
}

static void AdminAdminMiscellaneous(struct UUSOCKET *s, BOOL *show) {
    if (*show) {
        UUSendZ(s, "<h2>Miscellaneous</h2>\n");
        *show = FALSE;
    }
}

void AdminAdmin(struct TRANSLATE *t, char *query, char *post) {
    struct UUSOCKET *s = &t->ssocket;
    char *certExpire;
    char *fn;
    char *tt;
    struct SESSION *e = t->session;
    BOOL showActivity = TRUE;
    BOOL showMiscellaneous = TRUE;

    AdminHeader(t, AHNOTOPMENU, "EZproxy Administration");

    if (e->priv) {
        UUSendZ(s, "<div>\n");
        AdminStarted(s);
        UUSendZ(s, "</div>\n");

        certExpire = UUActiveCertExpiration();
        if (certExpire && *certExpire) {
            WriteLine(s, "<div>The active SSL certificate expires on %s</div>\n", certExpire);
        }

        if (!licenseValid) {
            WriteLine(s,
                      "<p class='failure'>This server has license issues. Please check "
                      "messages.txt for further information.</p>\n");
        }

        if (NumericHost(myName)) {
            WriteLine(s,
                      "<p class='warning'>The use of numeric server name \"%s\" will cause some "
                      "databases to fail.</p>\n",
                      myName);
        }

        if (strchr(myName, '.') == NULL) {
            UUSendZ(s, "<p class='warning'>The use of server name \"");
            SendHTMLEncoded(s, myName);
            UUSendZ(s, "\" which contains no periods will cause some databases to fail.</p>\n");
        }

        if (anyInvalidDomains)
            UUSendZ(s, "<p class='warning'>There is at least one invalid Domain line in " EZPROXYCFG
                       ".  Review " EZPROXYMSG " for details.</p>\n");

        AdminCheckMV(t);
        AdminAdminActivity(s, &showActivity);
    }

    if (e->priv || GroupMaskOverlap(gmAdminStatusUpdate, e->gm) ||
        GroupMaskOverlap(gmAdminStatusView, e->gm)) {
        AdminAdminActivity(s, &showActivity);
        UUSendZ(s, "<div><a href='/status'>View server status</a></div>\n");
    }

    if (e->priv || GroupMaskOverlap(gmAdminIntrusion, e->gm)) {
        AdminAdminActivity(s, &showActivity);
        UUSendZ(s, "<div><a href='/intrusion'>View and clear intrusion attempts</a></div>\n");
    }

    if (e->priv || GroupMaskOverlap(gmAdminUsageLimits, e->gm)) {
        AdminAdminActivity(s, &showActivity);
        UUSendZ(s,
                "<div><a href='/usagelimits'>View usage limits and clear suspensions</a></div>\n");
    }

    if (e->priv || GroupMaskOverlap(gmAdminAudit, e->gm)) {
        AdminAdminActivity(s, &showActivity);
        if (auditEventsMask)
            UUSendZ(s, "<div><a href='/audit'>View audit events</a></div>\n");
    }

    if (IdentifierGetSharedDatabase() != NULL &&
        (e->priv || GroupMaskOverlap(gmAdminIdentifier, e->gm))) {
        AdminAdminActivity(s, &showActivity);
        UUSendZ(s, "<div><a href='/identifier'>View identifiers</a></div>\n");
    }

    if (e->priv || GroupMaskOverlap(gmAdminSecurity, e->gm)) {
        AdminAdminActivity(s, &showActivity);
        UUSendZ(s, "<div><a href='/security'>View security rules</a></div>\n");
    }

    if (e->priv || GroupMaskOverlap(gmAdminMessages, e->gm) ||
        GroupMaskOverlap(gmAdminUsage, e->gm)) {
        AdminAdminActivity(s, &showActivity);
        UUSendZ(s, "<table class='tight-table th-row-nobold'>\n");
        UUAcquireMutex(&mMsgFile);
        OpenMsgFile();
        UUReleaseMutex(&mMsgFile);
        if (e->priv || GroupMaskOverlap(gmAdminMessages, e->gm)) {
            UUSendZ(s, "<tr><th scope='row'>View ");
            SendHTMLEncoded(s, msgFilename);
            UUSendZ(s,
                    "&nbsp;</th><td><a href='/messages?last=100' aria-label='messages last "
                    "100'>last 100</a>&nbsp;</td>"
                    "<td>| <a href='/messages?last=1000' aria-label='messages last 1,000'>last "
                    "1,000</a>&nbsp;</td>"
                    "<td>| <a href='/messages?last=10000' aria-label='messages last 10,000'>last "
                    "10,000</a>&nbsp;</td>"
                    "<td>| <a href='/messages' aria-label='messages all'>all</a></td></tr>\n");
        }
        if (e->priv || GroupMaskOverlap(gmAdminUsage, e->gm)) {
            UUSendZ(s, "<tr><th scope='row'>View ");
            fn = LogSPUActiveFilename(NULL, 0);
            SendHTMLEncoded(s, fn);
            UUSendZ(s,
                    "&nbsp;</th><td><a href='/usage?last=100' aria-label='web log last 100'>last "
                    "100</a>&nbsp;</td>"
                    "<td>| <a href='/usage?last=1000' aria-label='web log last 1,000'>last "
                    "1,000</a>&nbsp;</td>"
                    "<td>| <a href='/usage?last=10000' aria-label='web log last 10,000'>last "
                    "10,000</a>&nbsp;</td>"
                    "<td>| <a href='/usage' aria-label='web log all'>all</a></td></tr>\n");
        }
        UUSendZ(s, "</table>\n");

        if (e->priv || GroupMaskOverlap(gmAdminMessages, e->gm)) {
            UUSendZ(s, "<form class='top-bottom-padding' action='/messages'><nobr>\n");
            UUSendZ(s, "<label for='messages-find'>View ");
            SendHTMLEncoded(s, msgFilename);
            UUSendZ(s,
                    " lines containing </label><input name='find' id='messages-find'/> <input "
                    "type='submit' value='Find' aria-label='Find matching messages entries'/>\n");
            UUSendZ(s, "</nobr></form>\n");
        }

        if (e->priv || GroupMaskOverlap(gmAdminUsage, e->gm)) {
            UUSendZ(s, "<form class='top-bottom-padding' action='/usage'><nobr>\n");
            UUSendZ(s, "<label for='usage-find'>View ");
            SendHTMLEncoded(s, fn);
            UUSendZ(s,
                    " lines containing </label><input name='find' id='usage-find' /> <input "
                    "type='submit' value='Find' / aria-label='Find matching web log "
                    "entries'></td></tr>\n");
            UUSendZ(s, "</nobr></form>\n");
        }
    }

    if (e->priv || GroupMaskOverlap(gmAdminRestart, e->gm)) {
        AdminAdminActivity(s, &showActivity);
        UUSendZ(s, "<div><a href='/restart'>Restart EZproxy</a></div>\n");
    }

    if (!showActivity) {
        UUSendZ(s, "</p>\n");
    }

    UUSendZ(s, "<h2>Your Status</h2>\n<p>\n");
    UUSendZ(s, "<div><a href='/ip?admin=1'>View your IP address</a></div>\n");
    UUSendZ(s, "<div><a href='/logout'>Logout</a></div>\n");
    if (e->priv || GroupMaskOverlap(gmAdminAny, e->gm)) {
        UUSendZ(s, "<div><a href='/mygroups?admin=1'>View your group memberships</a></div>\n");
    }

    UUSendZ(s, "</p>\n");

    if (e->priv) {
        AdminAdminMiscellaneous(s, &showMiscellaneous);
    }

    if (e->priv || GroupMaskOverlap(gmAdminSSLUpdate, e->gm) ||
        GroupMaskOverlap(gmAdminSSLView, e->gm)) {
        AdminAdminMiscellaneous(s, &showMiscellaneous);
        UUSendZ(s, "<div><a href='/ssl'>Manage SSL (https) certificates</a></div>\n");
    }

    if (e->priv || GroupMaskOverlap(gmAdminGroups, e->gm)) {
        AdminAdminMiscellaneous(s, &showMiscellaneous);
        UUSendZ(s, "<div><a href='/groups'>View database group assignments</a></div>\n");
    }

    if (e->priv || GroupMaskOverlap(gmAdminUser, e->gm)) {
        AdminAdminMiscellaneous(s, &showMiscellaneous);
        UUSendZ(s, "<div><a href='/user'>Test " EZPROXYUSR " configuration</a></div>\n");
    }

    if (e->priv) {
        UUSendZ(s, "<div><a href='/network'>Test network connectivity</a></div>\n");
        UUSendZ(s, "<div><a href='/conflicts'>Check database definition conflicts</a></div>\n");
    }

    if (e->priv || GroupMaskOverlap(gmAdminLDAP, e->gm)) {
        AdminAdminMiscellaneous(s, &showMiscellaneous);
        UUSendZ(s, "<div><a href='/ldap'>Test LDAP</a></div>\n");
    }

    if (e->priv || GroupMaskOverlap(gmAdminDecryptVar, e->gm)) {
        AdminAdminMiscellaneous(s, &showMiscellaneous);
        if (anyEncryptVar)
            UUSendZ(s, "<div><a href='/decryptvar'>Decrypt variable</a></div>\n");
    }

    if (e->priv && gartnerKeys) {
        UUSendZ(s, "<div><a href='/gartnerkey'>Manage Gartner keys</a></div>\n");
    }

    if (e->priv || GroupMaskOverlap(gmAdminToken, e->gm)) {
        AdminAdminMiscellaneous(s, &showMiscellaneous);
        if (tt = DecryptTokensTitle())
            WriteLine(s, "<div><a href='/token'>%s</a></div>\n", tt);
    }

    if (e->priv) {
        if (optionRequireAuthenticate)
            UUSendZ(
                s,
                "<div><a href='/auth'>Force authentication for users of this computer</a></div>\n");
    }

    if (e->priv || GroupMaskOverlap(gmAdminShibboleth, e->gm)) {
        AdminAdminMiscellaneous(s, &showMiscellaneous);
        UUSendZ(s, "<div><a href='/shibboleth'>Manage Shibboleth</a></div>\n");
    }

    if (casServiceUrls != NULL && e->priv) {
        AdminAdminMiscellaneous(s, &showMiscellaneous);
        UUSendZ(s, "<div><a href='/cas/admin'>Manage CAS</a></div>\n");
    }

    if (optionFiddler && (e->priv || GroupMaskOverlap(gmAdminFiddler, e->gm))) {
        AdminAdminMiscellaneous(s, &showMiscellaneous);
        UUSendZ(s, "<div><a href='/fiddler'>Fiddler capture</a></div>\n");
    }

    AdminFooter(t, 0);
}
