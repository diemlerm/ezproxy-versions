/**
 * Core dispatcher for user authentication methods supporting common
 * conditions and actions.
 *
 * This module implements the core routines for dispatch user authentication methods
 * that support common conditions and actions, including the code for the actual
 * common conditions and actions.
 */

#define __UUFILE__ "usr.c"

#include "common.h"
#include "adminaudit.h"

#ifdef WIN32
/* io.h, fcntl.h and share.h needed for _sopen in Win32 */
#include <io.h>
#include <fcntl.h>
#include <share.h>
#include <lm.h>
#endif

#include <sys/stat.h>

#include "usr.h"
#include "usrfinduser.h"

#include "expression.h"

#define USRTESTCOMPAREOPERATORS "<=>~!"
#define USRTESTCOMPARETYPES "cdinsCDINS"

void ApplySPUEdits(struct TRANSLATE *t, struct USERINFO *uip) {
    struct SPUEDIT *spue;
    int offset;
    int currLen;
    int ovec[33]; /* 11 slots * 3 requires entries per slot */
    int nmat;
    char tempUrl[MAXURL];
    char *curr, *next;
    char *p, *pe;
    char *f;
    BOOL stop;
    BOOL encode, decode;
    char *retrace;
    char *name;
    size_t nameLen = 0;
    BOOL matched;
    char var[64];
    size_t varLen;
    int varStart, varMax;
    char *value;
    size_t valueLen = 0;
    char malformed = 0;
    char *colon;
    size_t max;
    enum IPACCESS ipAccess = IPAREMOTE;
    int ipIdx = 0;
    // struct sockadddr_storage peerIp;
    struct SPUEDITVAR *spuev;

    if (uip->url == NULL) {
        uip->spuEdited[0] = 0;
        return;
    }

    curr = uip->url;

    for (spue = spuEdits, stop = 0; spue && stop == 0; spue = spue->next) {
        offset = 0;
        matched = 0;
        malformed = 0;
        if (spue->cIpTypes != -1) {
            for (; ipIdx < spue->cIpTypes; ipIdx++) {
                if (compare_ip(&t->sin_addr, &ipTypes[ipIdx].start) >= 0 &&
                    compare_ip(&ipTypes[ipIdx].stop, &t->sin_addr) >= 0) {
                    ipAccess = ipTypes[ipIdx].ipAccess;
                }
            }
            if (ipAccess == IPAAUTO && spue->autoLoginIP == 0) {
                if (optionLogSPUEdit)
                    UsrLog(uip, "SPUEdit %s requires AutoLoginIP, not tested", spue->find);
                continue;
            }
            if (ipAccess == IPALOCAL && spue->excludeIP == 0) {
                if (optionLogSPUEdit)
                    UsrLog(uip, "SPUEdit %s requires ExcludeIP, not tested", spue->find);
                continue;
            }
            if (ipAccess == IPAREMOTE && spue->includeIP == 0) {
                if (optionLogSPUEdit)
                    UsrLog(uip, "SPUEdit %s requires IncludeIP, not tested", spue->find);
                continue;
            }
        }

        do {
            currLen = strlen(curr);
            nmat = pcre_exec(spue->findPattern, spue->findPatternExtra, curr, currLen, offset, 0,
                             ovec, sizeof(ovec) / sizeof(int));
            if (nmat <= 0) {
                if (matched == 0 && optionLogSPUEdit)
                    UsrLog(uip, "SPUEdit %s does not match %s", spue->find, curr);
                break;
            }

            if (matched == 0) {
                if (optionLogSPUEdit)
                    UsrLog(uip, "SPUEdit: %s matches %s", spue->find, curr);
                matched = 1;
            }

            if (spue->stop)
                stop = 1;

            if (spue->redirect)
                uip->spuRedirect = 1;

            /* The first time through, curr point to the original url, so we want next to be
               uip->spuEdited.  Whenver curr is spuEdited, then next must be tempUrl, so we have
            */
            if (curr == uip->spuEdited)
                next = tempUrl;
            else
                next = uip->spuEdited;

            p = next;
            pe = next + MAXURL - 1; /* leave room for final null */

            /* Copy everything that appears before the match */
            if (ovec[0] > 0) {
                memcpy(p, curr, ovec[0]);
                p += ovec[0];
            }

            for (f = spue->replace; *f && p < pe;) {
                if (*f != '$') {
                    if (*f == '\\') {
                        if (*f == '$') {
                            *p++ = '$'; /* single character insert has room */
                            f += 2;
                            continue;
                        }
                    }
                    *p++ = *f++; /* single character insert has room */
                    continue;
                }

                retrace = f;
                name = NULL;
                encode = decode = 0;

                for (f++; *f; f++) {
                    if (*f == 'e')
                        encode = 1;
                    else if (*f == 'd')
                        decode = 1;
                    else
                        break;
                }

                if (*f >= '0' && *f <= '9') {
                    name = f;
                    nameLen = 1;
                    f++;
                } else if (*f == '{') {
                    char *closeBrace = strchr(f, '}');
                    if (closeBrace) {
                        name = f + 1;
                        nameLen = closeBrace - f - 1;
                        f = closeBrace + 1;
                    }
                }
                /* If we didn't find a variable, just move a $ and retrace our steps */
                if (name == NULL || nameLen == 0) {
                    if (spue->malformed == 0) {
                        malformed = 1;
                        UsrLog(uip, "SPUEdit malformed replacement %s at %s", spue->replace,
                               retrace);
                    }
                    *p++ = '$'; /* single character insert has room */
                    f = retrace + 1;
                    continue;
                }

                varLen = UUMIN(nameLen, sizeof(var) - 1);
                memcpy(var, name, varLen);
                var[varLen] = 0;

                varStart = 0;
                varMax = -1;

                if (colon = strchr(var, ':')) {
                    *colon++ = 0;
                    varStart = atoi(colon);
                    /* A starting offset of 1 is really just no offset at all */
                    if (varStart == 1)
                        varStart = 0;
                    if (colon = strchr(colon, ':')) {
                        colon++;
                        varMax = atoi(colon);
                    }
                }

                value = NULL;

                for (spuev = spuEditVars; spuev; spuev = spuev->next) {
                    if (strnicmp(spuev->var, var, varLen) == 0 && varLen == spuev->varLen) {
                        value = spuev->val;
                        valueLen = strlen(spuev->val);
                        break;
                    }
                }

                if (strlen(var) == 1 && IsDigit(*name)) {
                    int i = *name - '0';
                    /* With one capture paren, nmat will be 2 (1st is always whole string match,
                     * which we provide by $0) */
                    if (i < nmat) {
                        i = i * 2;
                        if (ovec[i] >= 0) {
                            value = curr + ovec[i];
                            valueLen = ovec[i + 1] - ovec[i];
                        }
                    }
                } else if (stricmp(var, "ShibbolethProviderId") == 0) {
                    value = shibbolethProviderId;
                    if (value)
                        valueLen = strlen(value);
                    else {
                        if (spue->malformed == 0) {
                            malformed = 1;
                            UsrLog(uip,
                                   "SPUEdit: ShibbolethProviderId is not defined in " EZPROXYCFG);
                        }
                    }
                } else if (stricmp(var, "ShibbolethWAYF") == 0) {
                    value = shibbolethWAYF;
                    if (value)
                        valueLen = strlen(value);
                    else {
                        if (spue->malformed == 0) {
                            malformed = 1;
                            UsrLog(uip, "SPUEdit: ShibbolethWAYF is not defined in " EZPROXYCFG);
                        }
                    }
                } else if (value == NULL) {
                    if (spue->malformed == 0) {
                        malformed = 1;
                        UsrLog(uip, "SPUEdit: %s references non-existent %s", spue->replace, var);
                    }
                }

                if (value) {
                    max = pe - p;
                    /* negative offset means move from end of string */
                    if (varStart > 0) {
                        varStart--;
                        if ((size_t)varStart >= valueLen)
                            value = NULL;
                        else {
                            value += varStart;
                            valueLen -= varStart;
                        }
                    }

                    if (varStart < 0) {
                        varStart = -varStart;
                        if ((size_t)varStart < valueLen) {
                            value = value + valueLen - varStart;
                            valueLen = varStart;
                        }
                    }

                    if (varMax >= 0 && valueLen > (size_t)varMax)
                        valueLen = varMax;

                    if (value && valueLen) {
                        if (encode) {
                            p = AddEncodedFieldLimited(p, value, valueLen, pe);
                        } else if (decode) {
                            char *tmp;
                            size_t tmpLen;
                            if (!(tmp = malloc(valueLen + 1)))
                                PANIC;
                            memcpy(tmp, value, valueLen);
                            *(tmp + valueLen) = 0;
                            UnescapeString(tmp);
                            tmpLen = strlen(tmp);
                            if (tmpLen > max)
                                tmpLen = max;
                            memcpy(p, tmp, tmpLen);
                            p += tmpLen;
                            FreeThenNull(tmp);
                        } else {
                            if (valueLen > max)
                                valueLen = max;
                            memcpy(p, value, valueLen);
                            p += valueLen;
                        }
                    }
                }
            }

            /* Next time around, start matching where we ended in this loop */
            offset = p - next;

            /* And now append whatever was after the match */
            /* Copy what remains after the match */
            if (ovec[1] < currLen && p < pe) {
                max = UUMIN(currLen - ovec[1], pe - p);
                memcpy(p, curr + ovec[1], max);
                p += max;
            }

            *p = 0; /* room was held to insure this null will fit */
            curr = next;
        } while (spue->global);
        if (malformed && spue->malformed == 0)
            spue->malformed = 1;
        if (matched && optionLogSPUEdit) {
            UsrLog(uip, "SPUEdit changed to %s", curr);
        }
    }

    /* If curr is spuEdited, then the right URL is in place; otherwise, we need to copy */
    if (curr != uip->spuEdited)
        StrCpyOverlap(uip->spuEdited, curr);

    if (stop && optionLogSPUEdit)
        UsrLog(uip, "SPUEdit stop");

    if (uip->spuRedirect && optionLogSPUEdit)
        UsrLog(uip, "SPUEdit redirect");
}

static int UsrWildRECompare(struct USERINFO *uip, const char *s, const char *w) {
    BOOL re = 0;
    BOOL cs = 0;
    char *p;
    char *lower = NULL;
    const char *test;
    int result;
    xmlRegexpPtr xrp = NULL;

    if (s == NULL)
        s = "";

    if (w == NULL)
        w = "";

    for (;;) {
        if (p = StartsIWith(w, "-RE ")) {
            re = 1;
            w = p;
            continue;
        }

        if (p = StartsIWith(w, "-CS ")) {
            cs = 1;
            w = p;
            continue;
        }

        if (p = StartsIWith(w, "-- ")) {
            w = p;
            break;
        }

        break;
    }

    w = SkipWS(w);

    if (re == 0) {
        if (cs)
            return WildCompareCaseSensitive(s, w) ? 0 : 1;
        return WildCompare(s, w) ? 0 : 1;
    }

    if (cs == 0) {
        if (!(lower = strdup(s)))
            PANIC;
        AllLower(lower);
        test = lower;
    } else {
        test = s;
    }

    xrp = xmlRegexpCompile(BAD_CAST w);
    if (xrp == NULL) {
        UsrLog(uip, "Invalid regular expression ignored: %s", w);
        return 0;
    }

    result = xmlRegexpExec(xrp, BAD_CAST test) == 1 ? 0 : 1;

    xmlRegFreeRegexp(xrp);
    xrp = NULL;

    FreeThenNull(lower);

    return result;
}

static char *FirstNumeric(char *s) {
    for (; *s; s++) {
        if (IsDigit(*s) || *s == '.')
            break;
    }
    return s;
}

static int ParseTime(char *s) {
    int hour, min;
    char *colon;

    hour = atoi(s);
    min = ((colon = strchr(s, ':')) != NULL ? atoi(colon + 1) : 0);
    if (StrIStr(s, "a")) {
        if (hour == 12)
            hour = 0;
    }
    if (StrIStr(s, "p")) {
        if (hour < 12)
            hour += 12;
    }
    return hour * 60 + min;
}

time_t UserInfoNow(struct USERINFO *uip) {
    if (uip->now == 0)
        UUtime(&uip->now);
    return uip->now;
}

static struct tm *UserInfoTm(struct USERINFO *uip) {
    if (uip->tm.tm_year == 0) {
        time_t now = UserInfoNow(uip);
        UUlocaltime_r(&now, &uip->tm);
    }
    return &uip->tm;
}

char *UsrTestComparisonOperatorType(char *s, char *compareOperator, char *compareType) {
    char *myString = s;
    char myOperator;
    char myType;

    if (s == NULL)
        goto Defaults;

    s = SkipWS(s);

    if (*s == 0)
        goto Defaults;

    if (strchr(USRTESTCOMPAREOPERATORS, *s)) {
        myOperator = *s++;
        if (*s == 0 || IsWS(*s)) {
            myType = 's';
            myString = SkipWS(s);
            goto Finished;
        }

        if (strchr(USRTESTCOMPARETYPES, *s) == NULL)
            goto Defaults;

        myType = tolower(*s++);

        if (*s == 0 || IsWS(*s)) {
            myString = SkipWS(s);
            goto Finished;
        }
    }

Defaults:
    myOperator = '~';
    myType = 's';

Finished:
    if (compareOperator)
        *compareOperator = myOperator;

    if (compareType)
        *compareType = myType;

    return myString;
}

BOOL UsrTestCompare(char *value, char *testValue, char compareOperator, char compareType) {
    if (compareOperator == 0 || strchr(USRTESTCOMPAREOPERATORS, compareOperator) == NULL)
        compareOperator = '~';

    if (compareType == 0 || strchr(USRTESTCOMPARETYPES, compareType) == NULL)
        compareType = 's';

    if (isupper(compareType))
        compareType = tolower(compareType);

    if (compareType == 'n')
        compareType = 'd';

    if (compareType == 'd' || compareType == 'i') {
        value = FirstNumeric(value);
        testValue = FirstNumeric(testValue);
    }

    if (compareOperator == '~') {
        if (compareType == 'c')
            return WildCompareCaseSensitive(value, testValue);
        else
            return WildCompare(value, testValue);
    }

    if (compareType == 'i') {
        switch (compareOperator) {
        case '<':
            return atoi(value) < atoi(testValue);

        case '=':
            return atoi(value) == atoi(testValue);

        case '>':
            return atoi(value) > atoi(testValue);

        case '!':
            return atoi(value) != atoi(testValue);
        }
    } else if (compareType == 'd') {
        switch (compareOperator) {
        case '<':
            return atof(value) < atof(testValue);

        case '=':
            return atof(value) == atof(testValue);

        case '>':
            return atof(value) > atof(testValue);

        case '!':
            return atof(value) != atof(testValue);
        }
    } else if (compareType == 's') {
        switch (compareOperator) {
        case '<':
            return stricmp(value, testValue) < 0;

        case '=':
            return stricmp(value, testValue) == 0;

        case '>':
            return stricmp(value, testValue) > 0;

        case '!':
            return stricmp(value, testValue) != 0;
        }
    } else if (compareType == 'c') {
        switch (compareOperator) {
        case '<':
            return strcmp(value, testValue) < 0;

        case '=':
            return strcmp(value, testValue) == 0;

        case '>':
            return strcmp(value, testValue) > 0;

        case '!':
            return strcmp(value, testValue) != 0;
        }
    }
    return 0;
}

static int UsrAccept(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    if (uip->user && *uip->user) {
        uip->result = resultValid;
        return 0;
    }

    UsrLog(uip, EZPROXYUSR " allows Accept only if a username is submitted");

    return 1;
}

static int UsrAdmin(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    uip->admin = 1;
    return 0;
}

static int UsrAllowCountryChange(struct TRANSLATE *t,
                                 struct USERINFO *uip,
                                 void *context,
                                 char *arg) {
    uip->sessionFlags |= SESSION_FLAG_ALLOW_COUNTRY_CHANGE;
    return 0;
}

static int UsrAudit(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    char *p;

    if (p = StartsIWith(arg, "-expr ")) {
        char *val = ExpressionValue(t, uip, context, p);
        AuditEvent(t, AUDITINFOUSR, uip->user, t->session, "%s", val ? val : "");
        FreeThenNull(val);
    } else {
        AuditEvent(t, AUDITINFOUSR, uip->user, t->session, "%s", arg ? arg : "");
    }
    return 0;
}

static int UsrAutoLogin(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    if (!GroupMaskOverlap(uip->gm, uip->requiredGm))
        return 1;

    if (*arg)
        StrCpy3(uip->logUserReferer, arg, sizeof(uip->logUserReferer) - 17);
    else
        strcpy(uip->logUserReferer, "auto");
    strcat(uip->logUserReferer, "-");

    ipToStr(&t->sin_addr, AtEnd(uip->logUserReferer), INET6_ADDRSTRLEN);
    // UUinet_ntoa(AtEnd(uip->logUserReferer), t->sin_addr);

    StrCpy3(uip->logUser, uip->logUserReferer, sizeof(uip->logUser));

    uip->logUserAllowed = TRUE;

    uip->autoLoginBy = autoLoginByIP;
    uip->result = resultValid;
    if (t->session)
        t->session->reloginRequired = FALSE;
    return 0;
}

int UsrBanner(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    if (*arg == 0) {
        uip->loginBanner[0] = 0;
        return 0;
    }

    if (ValidMenu(arg)) {
        sprintf(uip->loginBanner, "%s%s", DOCSDIR, arg);
        return 0;
    }

    UsrLog(uip, EZPROXYUSR " references invalid banner %s", arg);
    return 1;
}

int UsrDocsCustom(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    if (*arg == 0) {
        t->docsCustomDir = NULL;
        return 0;
    }

    if (ValidDocsCustomDir(arg)) {
        t->docsCustomDir = StaticStringCopy(arg);
        return 0;
    }

    UsrLog(uip, EZPROXYUSR " references invalid DocsCustom %s", arg);
    return 1;
}

static int UsrDebug(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    uip->debug = 1;
    return 0;
}

int UsrDeny(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct UUSOCKET *s = &t->ssocket;
    char denyFile[MAXMENU];
    char *p;
    char *url;

    if (p = StartsIWith(arg, "-noaudit ")) {
        arg = SkipWS(p);
    } else {
        AuditEventLogin(t, AUDITLOGINDENIED, uip->user, NULL, uip->pass, arg);
    }

    if ((url = StartsIWith(arg, "-Redirect ")) && *(url = SkipWS(url))) {
        if (uip->logToSSocket) {
            WriteLine(s, "Deny access and redirect to ");
            SendHTMLEncoded(s, url);
            WriteLine(s, "<br>\n");
        } else {
            SimpleRedirect(t, url, 0);
        }
    } else {
        strcpy(denyFile, DENYHTM);
        if (*arg && ValidMenu(arg))
            sprintf(denyFile, "%s%s", DOCSDIR, arg);
        if (uip->logToSSocket) {
            WriteLine(s, "Deny access and send ");
            SendHTMLEncoded(s, denyFile);
            WriteLine(s, "<br>\n");
        } else {
            /* If we are denying access, don't let this connection be reused */
            t->denyClientKeepAlive = 0;
            HTMLHeader(t, htmlHeaderOmitCache);
            SendEditedFile(t, uip, denyFile, 1, uip->url, 1, NULL);
        }
    }
    uip->denied = uip->deniedNotified = 1;
    uip->result = resultNotified;
    if (uip->debug) {
        UsrLog(uip, "Stopping");
    }
    uip->skipping = 1;
    return 1;
}

static int UsrFalse(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return 1;
}

int UsrGroup(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    uip->gm = MakeGroupMask(uip->gm, arg, 0, '=');
    return 0;
}

static int UsrIf(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    char *val = NULL;
    int result;

    val = ExpressionValue(t, uip, context, arg);
    result = val && atoi(val) != 0 ? 0 : 1;
    FreeThenNull(val);
    return result;
}

int UsrIfAfter(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    time_t when;

    if (*arg == 0)
        return 1;

    when = ParseLocalDate(arg, 0);

    return UserInfoNow(uip) >= when ? 0 : 1;
}

int UsrIfAuth(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return UsrWildRECompare(uip, uip->auth, arg);
}

static int UsrIfAuthenticated(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return uip->result == resultValid ? 0 : 1;
}

int UsrIfBefore(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    time_t when;

    if (*arg == 0)
        return 1;

    when = ParseLocalDate(arg, 0);

    return UserInfoNow(uip) < when ? 0 : 1;
}

static int UsrIfCity(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    FindLocationByTranslate(t);
    return UsrWildRECompare(uip, t->location.city, arg);
}

static int UsrIfCountry(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    FindLocationByTranslate(t);
    return UsrWildRECompare(uip, t->location.countryCode, arg);
}

int UsrIfDay(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct tm *tm;
    struct DAYS {
        char *day;
        size_t min;
    };
    static struct DAYS *dp, days[] = {
                                {"Sunday",    2},
                                {"Monday",    1},
                                {"Tuesday",   2},
                                {"Wednesday", 1},
                                {"Thursday",  2},
                                {"Friday",    1},
                                {"Saturday",  2},
                                {NULL,        0}
    };
    char *dstr, *dtok;
    size_t dlen;
    int dnum;

    if (*arg == 0)
        return 1;

    tm = UserInfoTm(uip);

    for (dstr = NULL, dtok = StrTok_R(arg, ",;", &dstr); dtok; dtok = StrTok_R(NULL, ",;", &dstr)) {
        Trim(dtok, TRIM_LEAD | TRIM_TRAIL);
        if (dnum = atoi(dtok)) {
            if (dnum == tm->tm_mday) {
                return 0;
            }
        }
        dlen = strlen(dtok);
        for (dp = days; dp->day; dp++) {
            if (dlen >= dp->min && strnicmp(dtok, dp->day, dlen) == 0) {
                if (tm->tm_wday == dp - days)
                    return 0;
                break;
            }
        }
    }
    return 1;
}

static int UsrIfExpired(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    uip->expiredSeen = 1;

    if (uip->result == resultExpired) {
        uip->flags |= USRLASTEXPIRED;
        return 0;
    }

    return 1;
}

static int UsrIfGroupMember(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    int result;

    GROUPMASK gm = MakeGroupMask(NULL, arg, 0, '=');
    result = GroupMaskOverlap(uip->gm, gm) ? 0 : 1;
    GroupMaskFree(&gm);
    return result;
}

int UsrIfHeader(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    char *header = arg;
    char *testValue = strchr(header, ':');
    char *headerValue;
    int result;

    if (testValue = strchr(header, ':')) {
        *testValue++ = 0;
        Trim(testValue, TRIM_LEAD | TRIM_TRAIL);
    } else
        testValue = "";

    result = *testValue == 0 ? 0 : 1;

    Trim(header, TRIM_LEAD | TRIM_TRAIL);
    for (headerValue = t->requestHeaders; (headerValue = FindField(header, headerValue));) {
        LinkField(headerValue);
        if (UsrWildRECompare(uip, headerValue, testValue) == 0)
            return 0;
        result = 1;
    }
    return result;
}

int UsrIfHttp(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return t->useSsl == 0 ? 0 : 1;
}

int UsrIfHttps(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return t->useSsl == 1 ? 0 : 1;
}

static int UsrIfInvalid(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    if (uip->result == resultInvalid) {
        uip->flags |= USRLASTINVALID;
        return 0;
    }

    return 1;
}

static int UsrIfIP(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    char ipBuffer[INET6_ADDRSTRLEN];
    int result;
    char *saveForLog = NULL;

    if (uip->debug)
        if (!(saveForLog = strdup(arg)))
            PANIC;

    result = FindUserIP(t, arg) ? 0 : 1;

    if (uip->debug) {
        UsrLog(uip, "IP %s for %s result %s", saveForLog,
               ipToStr(&t->sin_addr, ipBuffer, sizeof ipBuffer), result == 0 ? "true" : "false");
        FreeThenNull(saveForLog);
    }

    return result;
}

int UsrIfLanguage(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    if (uip->saveLanguage == NULL) {
        if (uip->saveLanguage = FindField("Accept-Language", t->requestHeaders))
            LinkField(uip->saveLanguage);
        else
            uip->saveLanguage = "";
    }

    return UsrWildRECompare(uip, uip->saveLanguage, arg);
}

int UsrIfMonth(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct tm *tm;
    char **mp;
    int mnum;
    char *mstr, *mtok;
    size_t mlen;

    if (*arg == 0)
        return 1;

    tm = UserInfoTm(uip);

    for (mstr = NULL, mtok = StrTok_R(arg, ",;", &mstr); mtok; mtok = StrTok_R(NULL, ",;", &mstr)) {
        Trim(mtok, TRIM_LEAD | TRIM_TRAIL);
        if (mnum = atoi(mtok)) {
            if (mnum == tm->tm_mon + 1) {
                return 0;
            }
        }
        mlen = strlen(mtok);
        if (mlen < 3)
            continue;
        for (mp = monthNames, mnum = 1; *mp; mp++, mnum++) {
            if (strnicmp(mtok, *mp, mlen) == 0) {
                if (tm->tm_mon + 1 == mnum)
                    return 0;
            }
        }
    }
    return 1;
}

static int UsrIfNoGroups(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return GroupMaskIsNull(uip->gm) ? 0 : 1;
}

static int UsrIfQueryStringPass(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                void *context,
                                char *arg) {
    return uip->passFromQueryString ? 0 : 1;
}

static int UsrIfPassword(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return UsrWildRECompare(uip, uip->pass, arg);
}

static int UsrIfPIN(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return UsrWildRECompare(uip, uip->pin, arg);
}

int UsrIfReferer(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    if (uip->saveReferer == NULL) {
        if (uip->saveReferer = FindField("Referer", t->requestHeaders))
            LinkField(uip->saveReferer);
        else
            uip->saveReferer = "";
    }

    return UsrWildRECompare(uip, uip->saveReferer, arg);
}

static int UsrIfRefused(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    if (uip->result == resultRefused) {
        uip->flags |= USRLASTREFUSED;
        return 0;
    }

    return 1;
}

static int UsrIfRegion(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    FindLocationByTranslate(t);
    return UsrWildRECompare(uip, t->location.region, arg);
}

static int UsrIfTag(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return UsrWildRECompare(uip, t->logTag, arg);
}

int UsrIfTime(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct tm *tm;
    char *tstr, *ttok, *hyphen;
    int nowMins, startMins, stopMins;

    if (*arg == 0)
        return 1;

    tm = UserInfoTm(uip);

    nowMins = tm->tm_hour * 60 + tm->tm_min;

    for (tstr = NULL, ttok = StrTok_R(arg, ",;", &tstr); ttok; ttok = StrTok_R(NULL, ",;", &tstr)) {
        hyphen = strchr(ttok, '-');
        if (hyphen)
            *hyphen++ = 0;
        else
            hyphen = ttok;
        startMins = ParseTime(ttok);
        stopMins = ParseTime(hyphen);
        if (startMins <= nowMins && nowMins <= stopMins) {
            return 0;
        }
    }

    return 1;
}

static int UsrIfUnauthenticated(struct TRANSLATE *t,
                                struct USERINFO *uip,
                                void *context,
                                char *arg) {
    return uip->result != resultValid ? 0 : 1;
}

int UsrIfURL(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return UsrWildRECompare(uip, uip->url, arg);
}

static int UsrIfUser(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return UsrWildRECompare(uip, uip->user, arg);
}

static int UsrIfUsrVar(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    char *arg2;

    arg2 = SkipNonWS(arg);
    if (*arg2) {
        *arg2++ = 0;
        arg2 = SkipWS(arg2);
    }

    if (IsDigit(*arg))
        return UsrWildRECompare(uip, uip->vars[*arg - '0'], arg2);

    return 1;
}

int UsrIfYear(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct tm *tm;
    char *ystr, *ytok;
    int ynum;

    tm = UserInfoTm(uip);

    for (ystr = NULL, ytok = StrTok_R(arg, ",;", &ystr); ytok; ytok = StrTok_R(NULL, ",;", &ystr)) {
        Trim(ytok, TRIM_LEAD | TRIM_TRAIL);
        if (*ytok == 0 || !IsAllDigits(ytok))
            continue;
        ynum = atoi(ytok);
        if (ynum < 100) {
            if (tm->tm_year % 100 == ynum) {
                return 0;
            }
        }
        if (ynum == tm->tm_year + 1900) {
            return 0;
        }
    }
    return 1;
}

int UsrIgnore(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    if (uip->result == resultExpired && (uip->flags & USRLASTEXPIRED)) {
        uip->result = resultValid;
    }

    if (uip->result == resultInvalid && (uip->flags & USRLASTINVALID)) {
        uip->result = resultValid;
    }

    if (uip->result == resultRefused && (uip->flags & USRLASTREFUSED)) {
        uip->result = resultValid;
    }

    return 0;
}

void UsrNextLine(char *suffixLine, char **endOfLinePtr, char **nextLinePtr) {
    *endOfLinePtr = strchr(suffixLine, '\r');
    if (*endOfLinePtr == NULL) {
        *endOfLinePtr = strchr(suffixLine, '\n');
        if (*endOfLinePtr == NULL) {
            *endOfLinePtr = strchr(suffixLine, 0);
        }
    }
    *nextLinePtr = *endOfLinePtr;
    if (((*nextLinePtr)[0] == '\r') && ((*nextLinePtr)[1] == '\n')) {
        *nextLinePtr = *nextLinePtr + 2;
    } else if ((*nextLinePtr)[0] == '\n') {
        *nextLinePtr = (*nextLinePtr) + 1;
    }
}

void UsrLogLine(struct USERINFO *uip, char *prefix, char *infix, char *suffix) {
    struct TRANSLATE *t;

    if (uip && uip->logToSSocket) {
        t = uip->translate;
    } else {
        t = NULL;
    }
    if (t) {
        if (debugLevel > 7000) {
            Log("%s%s%s", prefix, infix, suffix);
        }
        if (uip->logToSSocket == 1) {
            UUSendZ(&t->ssocket, "<h2>Debug messages</h2><p><pre>\r\n");
            uip->logToSSocket = 2;
            WriteLine(&t->ssocket, "%" FMT_FILE_INDEX "s %" FMT_LINE_NUMBER "s\r\n", "FILE",
                      "LINE");
        }
        if (uip->logToSSocket != 3) {
            SendHTMLEncoded(&t->ssocket, prefix);
            SendHTMLEncoded(&t->ssocket, infix);
            SendHTMLEncoded(&t->ssocket, suffix);
            UUSendZ(&t->ssocket, "\r\n");
        } else {
            UUSendZ(&t->ssocket, "</pre></p>\n");
        }
        UUFlush(&t->ssocket);
    } else {
        Log("%s%s%s", prefix, infix, suffix);
    }
}

#define ROOM_FOR_SUFFIX 2
#define INDENTATION 5
void UsrLogCommon(struct USERINFO *uip, char indicator, const char *format, va_list *ap) {
    char *suffixLine, *nextSuffixLine, *suffixLineEnd, *suffix, *prefix;
    char c;
    char infix[INDENTATION + 1];
    int prefixSize;
    int suffixSize;
    char buffer[32760];
    int l;
    int indent;
    BOOL overflowLine = FALSE;

    prefix = buffer;
    prefix[0] = 0;
    if (uip->fileInclusion) {
        IncludeFileDisplayName(uip->fileInclusion, prefix, sizeof(buffer) - ROOM_FOR_SUFFIX);
        infix[0] = indicator;
        infix[1] = indicator;
        infix[2] = indicator;
        infix[3] = indicator;
        infix[4] = ' ';
        infix[5] = 0;
    } else {
        infix[0] = 0;
        infix[1] = 0;
        infix[2] = 0;
        infix[3] = 0;
        infix[4] = 0;
        infix[5] = 0;
    }
    prefixSize = strlen(prefix) + 1;

    suffix = prefix + prefixSize;
    suffix[0] = 0;
    suffixSize = sizeof(buffer) - prefixSize;
    _SNPRINTF_BEGIN(suffix, suffixSize);
    _vsnprintf(suffix, suffixSize, format, *ap);
    overflowLine = _SNPRINTF_IS_OVERFLOW(buffer, sizeof(buffer));
    _SNPRINTF_END(suffix, suffixSize);

    indent = 2;
    for (suffixLine = suffix; *suffixLine || (indent == 2); suffixLine = nextSuffixLine) {
        UsrNextLine(suffixLine, &suffixLineEnd, &nextSuffixLine);
        suffixLineEnd[0] = 0;
        l = suffixLineEnd - suffixLine;
        for (; l > -1;) {
            if (l > 132) {
                c = suffixLine[133];
                suffixLine[133] = 0;
                UsrLogLine(uip, prefix, &(infix[INDENTATION - indent]), suffixLine);
                suffixLine[133] = c;
                suffixLine = suffixLine + 133;
                l = suffixLineEnd - suffixLine;
                indent = 4;
            } else {
                UsrLogLine(uip, prefix, &(infix[INDENTATION - indent]), suffixLine);
                l = -1;
            }
        }
        indent = 3;
    }

    if (overflowLine) {
        UsrLogLine(uip, prefix, &(infix[INDENTATION - 5]), "The previous text was truncated.");
    }
}

int UsrLog(struct USERINFO *uip, const char *format, ...) {
    va_list ap;

    va_start(ap, format);
    if (uip != NULL) {
        UsrLogCommon(uip, '+', format, &ap);
    } else {
        LogVA(format, &ap);
    }
    va_end(ap);

    return 0;
}

int UsrListing(struct USERINFO *uip, BOOL skipping, const char *format, ...) {
    va_list ap;

    if (debugLevel >= 8000) {
        va_start(ap, format);
        UsrLogCommon(uip, (char)(skipping ? ' ' : '>'), format, &ap);
        va_end(ap);
    }

    return 0;
}

#define USERFILE_LISTVAR_FMT "%-40s %s"
#define USERFILE_LISTVAR_HEADING_FMT "%-40s %s"
void UsrLstVar2(struct TRANSLATE *t,
                struct VARIABLES *v,
                void *context,
                const char *name,
                const char *value,
                int count) {
    struct USERINFO *uip = (struct USERINFO *)context;

    UsrLog(uip, USERFILE_LISTVAR_FMT, name, value);
}

int UsrLstVar(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct VARIABLES *allVars;
    int count = 0;

    if (arg && *arg) {
        if (stricmp(arg, "session") == 0) {
            UsrLog(uip, USERFILE_LISTVAR_HEADING_FMT, "SESSION VARIABLE", "VALUE");
            if ((t->session != NULL) && (t->session->sessionVariables != NULL)) {
                count = VariablesForAll(t, t->session->sessionVariables, uip, UsrLstVar2);
            }
        } else if (stricmp(arg, "local") == 0) {
            UsrLog(uip, USERFILE_LISTVAR_HEADING_FMT, "LOCAL VARIABLE", "VALUE");
            if (t->localVariables != NULL) {
                count = VariablesForAll(t, t->localVariables, uip, UsrLstVar2);
            }
        } else {
            UsrLog(uip,
                   "invalid ListVariables parameter '%s'; expected 'session', 'local', or no "
                   "parameter meaning both kinds",
                   arg);
            count = 1;
        }
    } else {
        allVars = VariablesNew("all", FALSE);

        if (t->localVariables != NULL) {
            VariablesMerge(allVars, t->localVariables, NULL);
        }
        if ((t->session != NULL) && (t->session->sessionVariables != NULL)) {
            VariablesMerge(allVars, t->session->sessionVariables, NULL);
        }
        UsrLog(uip, USERFILE_LISTVAR_HEADING_FMT, "VARIABLE", "VALUE");
        count = VariablesForAll(t, allVars, uip, UsrLstVar2);

        VariablesFree(&allVars);
    }
    if (count == 0) {
        UsrLog(uip, "  NONE");
    }
    return 0;
}

static int UsrMsg(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    char *p;

    if (p = StartsIWith(arg, "-expr ")) {
        char *val = ExpressionValue(t, uip, context, p);
        if (val)
            UsrLog(uip, "%s", val);
        FreeThenNull(val);
    } else {
        UsrLog(uip, "%s", arg);
    }
    return 0;
}

static int UsrMsgAuth(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    UsrLog(uip, "This authentication method does not support MsgAuth");
    return 1;
}

static int UsrRedirect(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    struct UUSOCKET *s = &t->ssocket;
    char *val = ExpressionValue(t, uip, context, arg);

    if (val == NULL || *val == 0 || strcmp(val, "0") == 0) {
        UsrLog(uip, "Redirect requires an expression for the redirect URL");
        return 1;
    }

    if (uip->logToSSocket) {
        WriteLine(s, "Redirect to ");
        SendHTMLEncoded(s, val);
        WriteLine(s, "<br>\n");
    } else {
        SimpleRedirect(t, val, 0);
    }

    FreeThenNull(val);
    uip->denied = uip->deniedNotified = 1;
    uip->result = resultNotified;
    if (uip->debug) {
        UsrLog(uip, "Stopping");
    }
    uip->skipping = 1;
    return 0;
}

int UsrRelogin(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    int mins = atoi(arg);
    time_t now;

    uip->relogin = mins ? UUtime(&now) + mins * 60 : 0;
    return 0;
}

static int UsrRenew(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    if (t->session && t->session->autoLoginBy == autoLoginByNone) {
        t->session->reloginRequired = FALSE;
        uip->result = resultValid;
        StrCpy3(uip->logUser, t->session->logUserFull, sizeof(uip->logUser));
        return 0;
    }
    return 1;
}

static int UsrReject(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    if (uip->logToSSocket)
        WriteLine(&t->ssocket, "Reject access (send nothing to remote user)<br>\n");

    uip->denied = uip->deniedNotified = 1;
    uip->result = resultNotified;
    if (uip->debug) {
        UsrLog(uip, "Stopping");
    }
    uip->skipping = 1;
    return 1;
}

static int UsrSet(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    char *val = NULL;

    val = ExpressionValue(t, uip, context, arg);
    FreeThenNull(val);
    return 0;
}

int UsrSourceIP(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    if (*arg == 0) {
        memset(&uip->sourceIpInterface, 0, sizeof(uip->sourceIpInterface));
        return 0;
    }

    if (UUGetHostByName(arg, &uip->sourceIpInterface, 0, 0) != 0) {
        UsrLog(uip, "Invalid SourceIP: %s", arg);
        return 1;
    }

    anySourceIP = 1;
    return 0;
}

BOOL UsrSkipping(struct USERINFO *uip) {
    return uip->result == resultNotified || uip->denied || uip->skipping;
}

static int UsrStop(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    if (uip->debug) {
        UsrLog(uip, "Stopping");
    }
    uip->skipping = 1;
    return 1;
}

static int UsrTrue(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    return 0;
}

static int UsrUnknown(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    uip->result = resultInvalid;
    if (uip->debug) {
        UsrLog(uip, "Stopping");
    }
    uip->skipping = 1;
    return 0;
}

static int UsrUsrVar(struct TRANSLATE *t, struct USERINFO *uip, void *context, char *arg) {
    char *arg2;

    arg2 = SkipNonWS(arg);
    if (*arg2) {
        *arg2++ = 0;
        arg2 = SkipWS(arg2);
    }

    if (strlen(arg) == 1 && IsDigit(*arg)) {
        StrCpy3(uip->vars[*arg - '0'], arg2, sizeof(uip->vars[0]));
        return 0;
    }

    return 1;
}

char UsrDispatch(struct TRANSLATE *t,
                 char *title,
                 struct USRDIRECTIVE *uds,
                 void *context,
                 struct USERINFO *uip,
                 char *cmd,
                 size_t cmdLen,
                 char *arg) {
    struct USRDIRECTIVE *ud, udc[] = {
                                 {"Accept",             UsrAccept,             0                 },
                                 {"Admin",              UsrAdmin,              0                 },
                                 {"Allow",              UsrStop,               0                 },
                                 {"AllowCountryChange", UsrAllowCountryChange, 0                 },
                                 {"Audit",              UsrAudit,              0                 },
                                 {"Authenticated",      UsrIfAuthenticated,    0                 },
                                 {"AutoLogin",          UsrAutoLogin,          0                 },
                                 {"Banner",             UsrBanner,             0                 },
                                 {"City",               UsrIfCity,             0                 },
                                 {"Country",            UsrIfCountry,          0                 },
                                 {"Debug",              UsrDebug,              0                 },
                                 {"Deny",               UsrDeny,               0                 },
                                 {"DocsCustom",         UsrDocsCustom,         0                 },
                                 {"Expired",            UsrIfExpired,          0                 },
                                 {"False",              UsrFalse,              0                 },
                                 {"G",                  UsrGroup,              0                 },
                                 {"Group",              UsrGroup,              0                 },
                                 {"GroupMember",        UsrIfGroupMember,      0                 },
                                 {"Groups",             UsrGroup,              0                 },
                                 {"If",                 UsrIf,                 0                 },
                                 {"IfAfter",            UsrIfAfter,            0                 },
                                 {"IfAuth",             UsrIfAuth,             0                 },
                                 {"IfAuthenticated",    UsrIfAuthenticated,    0                 },
                                 {"IfBefore",           UsrIfBefore,           0                 },
                                 {"IfCity",             UsrIfCity,             0                 },
                                 {"IfCountry",          UsrIfCountry,          0                 },
                                 {"IfDay",              UsrIfDay,              0                 },
                                 {"IfExpired",          UsrIfExpired,          0                 },
                                 {"IfGroupMember",      UsrIfGroupMember,      0                 },
                                 {"IfHeader",           UsrIfHeader,           0                 },
                                 {"IfHttp",             UsrIfHttp,             0                 },
                                 {"IfHttps",            UsrIfHttps,            0                 },
                                 {"IfInvalid",          UsrIfInvalid,          0                 },
                                 {"IfIP",               UsrIfIP,               0                 },
                                 {"IfLanguage",         UsrIfLanguage,         0                 },
                                 {"IfMonth",            UsrIfMonth,            0                 },
                                 {"IfNoGroup",          UsrIfNoGroups,         0                 },
                                 {"IfNoGroups",         UsrIfNoGroups,         0                 },
                                 {"IfQueryStringPass",  UsrIfQueryStringPass,  0                 },
                                 {"IfPassword",         UsrIfPassword,         0                 },
                                 {"IfPIN",              UsrIfPIN,              0                 },
                                 {"IfReferer",          UsrIfReferer,          0                 },
                                 {"IfRefused",          UsrIfRefused,          0                 },
                                 {"IfRegion",           UsrIfRegion,           0                 },
                                 {"IfTag",              UsrIfTag,              0                 },
                                 {"IfTime",             UsrIfTime,             0                 },
                                 {"IfUnauthenticated",  UsrIfUnauthenticated,  0                 },
                                 {"IfURL",              UsrIfURL,              0                 },
                                 {"IfUser",             UsrIfUser,             0                 },
                                 {"IfUsrVar",           UsrIfUsrVar,           0                 },
                                 {"IfYear",             UsrIfYear,             0                 },
                                 {"Ignore",             UsrIgnore,             0                 },
                                 {"Invalid",            UsrIfInvalid,          0                 },
                                 {"IP",                 UsrIfIP,               0                 },
                                 {"ListVariables",      UsrLstVar,
                                  USRDIRECTIVE_FLAG_EVAL_1_PARM | USRDIRECTIVE_FLAG_EVAL_FALLBACK},
                                 {"Msg",                UsrMsg,                0                 },
                                 {"MsgAuth",            UsrMsgAuth,            0                 },
                                 {"NoGroup",            UsrIfNoGroups,         0                 },
                                 {"NoGroups",           UsrIfNoGroups,         0                 },
                                 {"Redirect",           UsrRedirect,           0                 },
                                 {"Refused",            UsrIfRefused,          0                 },
                                 {"Relogin",            UsrRelogin,            0                 },
                                 {"Renew",              UsrRenew,              0                 },
                                 {"Region",             UsrIfRegion,           0                 },
                                 {"Reject",             UsrReject,             0                 },
                                 {"Set",                UsrSet,                0                 },
                                 {"Skip",               UsrStop,               0                 },
                                 {"SourceIP",           UsrSourceIP,           0                 },
                                 {"Stop",               UsrStop,               0                 },
                                 {"True",               UsrTrue,               0                 },
                                 {"Unauthenticated",    UsrIfUnauthenticated,  0                 },
                                 {"Unknown",            UsrUnknown,            0                 },
                                 {"User",               UsrIfUser,             0                 },
                                 {"UsrVar",             UsrUsrVar,             0                 },
                                 {NULL,                 NULL,                  0                 }
    };
    int i;
    BOOL found = 0;
    char *argVal = NULL;
    char **argValList = NULL;
    int argValListIndex;
    int result;

    for (i = 0, found = 0; !found && i < 2; i++) {
        ud = (i == 0 ? uds : udc);
        if (ud == NULL)
            continue;
        for (; ud->directive; ud++) {
            if (MatchCommand(cmd, cmdLen, ud->directive, 0)) {
                found = 1;
                break;
            }
        }
    }
    if ((found == 0) || ((ud->func == NULL) && (ud->funcn == NULL))) {
        UsrLog(uip, "For %s, %s is unrecognized", title, cmd);
        return 2;
    }
    if (ud->flags & USRDIRECTIVE_FLAG_EVAL_1_PARM) {
        argVal = ExpressionValueReportError(
            t, uip, context, arg, ((ud->flags & USRDIRECTIVE_FLAG_EVAL_FALLBACK) ? FALSE : TRUE));
        if (argVal) {
            arg = argVal;
        } else if (!(ud->flags & USRDIRECTIVE_FLAG_EVAL_FALLBACK)) {
            ExpressionValueReportError(t, uip, context, arg, TRUE);
            arg = "";
        }
        result = (*ud->func)(t, uip, context, arg);
    } else if (ud->flags & USRDIRECTIVE_FLAG_EVAL_N_PARMS) {
        argValList = ExpressionValueList(t, uip, context, arg);
        result = (*ud->funcn)(t, uip, context, argValList);
    } else {
        result = (*ud->func)(t, uip, context, arg);
    }
    FreeThenNull(argVal);
    argValListIndex = 0;
    if (argValList) {
        for (argVal = argValList[argValListIndex]; argVal; argVal = argValList[++argValListIndex]) {
            FreeThenNull(argVal);
        }
        FreeThenNull(argValList);
    }
    return (result == 0) ? 1 : 0;
}

#define USRMAXBRACE 100

/* Combine USRCOMMON0 through USRCOMMON5 macros to create the proper
   body of UsrHandler
   We really only know cmdReset and its context by the caller, so this was
   moved out of uip structure.
*/
enum FINDUSERRESULT UsrHandler(struct TRANSLATE *t,
                               char *title,
                               struct USRDIRECTIVE *uds,
                               void *context,
                               struct USERINFO *uip,
                               char *file,
                               int f,
                               struct FILEREADLINEBUFFER *frb,
                               struct sockaddr_storage *pInterface,
                               void (*resetFunc)()) {
    char cmdAll[4096];
    char *cmdNext = NULL;
    char *cmd;
    char *cmdp;
    char cmdQuote = 0;
    char *arg;
    size_t cmdLen;
    BOOL cmdReset = 1;
    char cmdSkip = 0;
    char success, negate;
    int braceLevel = 0;
    int firstSkipBraceLevel = -1;
    char cmdSkips[USRMAXBRACE];
    char nextBrace = 0;
    uip->pInterface = pInterface;

    uip->skipping =
        ((uip->flags & USRUSERMAYBENULL) == 0) && (uip->user == NULL || *uip->user == 0);
    if (uip->debug && uip->skipping) {
        UsrLog(uip, "Stopping");
    }

    uip->result = resultRefused;

    /* If first directive is a 0-length string, it is an init function to call before we start */
    if (uds && uds->directive && *uds->directive == 0) {
        (*uds->func)(t, uip, context, NULL);
        uds++;
    }

    while (FileReadLine(f, cmdAll, sizeof(cmdAll), frb)) {
        if (uip->fileInclusion)
            uip->fileInclusion->lineNumber++;
        cmdQuote = 0;
        cmdp = SkipWS(cmdAll);
        if (*cmdp == '/') {
            if (uip->debug)
                UsrListing(uip, FALSE, "%s", cmdAll);
            break;
        }
        if (*cmdp == '#') {
            if (uip->debug)
                UsrListing(uip, TRUE, "%s", cmdAll);
            continue;
        }
        if (UsrSkipping(uip)) {
            if (uip->debug)
                UsrListing(uip, TRUE, "%s", cmdAll);
            continue;
        }
        if (cmdReset) {
            uip->flags = uip->flags & ~(USRLASTREFUSED | USRLASTINVALID | USRLASTEXPIRED);
            cmdSkip = braceLevel > 0 ? cmdSkips[braceLevel - 1] : 0;
        } else {
            cmdReset = 1;
        }
        if (uip->debug)
            UsrListing(uip, cmdSkip, "%s", cmdAll);
        {
            if (cmdReset && resetFunc != NULL) {
                (*resetFunc)(t, uip, context);
            }
            for (cmd = cmdAll; UsrSkipping(uip) == 0 && (nextBrace || cmd); cmd = cmdNext) {
                if (cmd)
                    cmd = SkipWS(cmd);
                if (nextBrace == '{' || *cmd == '{') {
                    if (nextBrace)
                        nextBrace = 0;
                    else
                        cmdNext = cmd + 1;
                    if (braceLevel < USRMAXBRACE) {
                        if (firstSkipBraceLevel == -1) {
                            if (cmdSkip) {
                                firstSkipBraceLevel = braceLevel;
                            }
                            cmdSkips[braceLevel++] = cmdSkip;
                        } else {
                            cmdSkips[braceLevel++] = 2;
                        }
                    }
                    continue;
                }
                if (nextBrace == '}' || *cmd == '}') {
                    if (nextBrace)
                        nextBrace = 0;
                    else
                        cmdNext = cmd + 1;
                    if (braceLevel > 0) {
                        cmdSkip = cmdSkips[--braceLevel];
                        if (braceLevel == firstSkipBraceLevel) {
                            firstSkipBraceLevel = -1;
                        }
                    }
                    continue;
                }
                for (cmdNext = cmd;; cmdNext++) {
                    if (*cmdNext == 0) {
                        cmdNext = NULL;
                        break;
                    }
                    if (cmdQuote) {
                        if (*cmdNext == cmdQuote) {
                            cmdQuote = 0;
                        }
                        continue;
                    }
                    if (*cmdNext == '\'' || *cmdNext == '"') {
                        cmdQuote = *cmdNext;
                        continue;
                    }
                    if (*cmdNext == '\\') {
                        char n = *(cmdNext + 1);
                        if (cmdQuote && (n == '\'' || n == '"')) {
                            cmdNext++;
                            continue;
                        }
                        if (n == '{' || n == '}' || n == ';') {
                            StrCpyOverlap(cmdNext, cmdNext + 1);
                            continue;
                        }
                    }
                    if (*cmdNext == ';') {
                        *cmdNext++ = 0;
                        cmdNext = SkipWS(cmdNext);
                        if (*cmdNext == 0) {
                            cmdReset = 0;
                            cmdNext = NULL;
                        }
                        break;
                    }
                    if (*cmdNext == '{' || *cmdNext == '}') {
                        nextBrace = *cmdNext;
                        *cmdNext++ = 0;
                        cmdNext = SkipWS(cmdNext);
                        break;
                    }
                }
                Trim(cmd, TRIM_LEAD | TRIM_TRAIL);
                ParseCommand(cmd, &cmdLen, &arg);
                if (MatchCommand(cmd, cmdLen, "Else", 0)) {
                    if (cmdSkip == 0) {
                        cmdSkip = 2;
                    } else if (cmdSkip == 1) {
                        cmdSkip = 0;
                    }
                    StrCpyOverlap(cmd, arg);
                    Trim(cmd, TRIM_LEAD | TRIM_TRAIL);
                    ParseCommand(cmd, &cmdLen, &arg);
                    if (cmdLen == 0)
                        continue;
                }
                if (cmdSkip)
                    continue;
                negate = 0;
                while (MatchCommand(cmd, cmdLen, "Not", 0)) {
                    negate = 1 - negate;
                    StrCpyOverlap(cmd, arg);
                    Trim(cmd, TRIM_LEAD | TRIM_TRAIL);
                    ParseCommand(cmd, &cmdLen, &arg);
                }
                /* cmdLen will be 0 if there was a Not command and nothing else */
                if (cmdLen == 0) {
                    cmdSkip = 1;
                    continue;
                }
                for (success = 0; success == 0; success = 1) {
                    {
                        success = UsrDispatch(t, title, uds, context, uip, cmd, cmdLen, arg);
                        break;
                    }
                }
                /*
                Here is the truth table for when we want to break the loop
                          Negate
                          0   1
                        +---+---+
                      0 | 1 | 0 |
                Success +---+---+
                      1 | 0 | 1 |
                        +---+---+
                Thus, we enter the "cmdSkip" state when negate == success
                or also on success == 2 which is unrecognized directive
                */
                if (negate == success || success == 2)
                    cmdSkip = 1;
            }
        }
    }

    uip->authIsReadOnly = NULL;
    uip->authSetValue = NULL;
    uip->authSetAggregateValue = NULL;
    uip->authGetValue = NULL;
    uip->authAggregateTest = NULL;

    return uip->result;
}

void InitUserInfo(struct TRANSLATE *t,
                  struct USERINFO *uip,
                  char *user,
                  char *pass,
                  char *url,
                  GROUPMASK gm) {
    memset(uip, 0, sizeof(*uip));
    uip->forceDebug = (debugLevel > 1);
    uip->translate = t;
    uip->user = user;
    uip->pass = pass;
    uip->url = url;
    if (t) {
        ApplySPUEdits(t, uip);
    }
    strcpy(uip->login, LOGINHTM);
    strcpy(uip->loginBu, LOGINBUHTM);
    uip->autoLoginBy = autoLoginByNone;
    uip->csrfValid = TRUE;

    if (gm == NULL)
        gm = GroupMaskNew(0);
    else
        GroupMaskClear(gm);

    uip->gm = gm;

    if (debugLevel > 12)
        Log("init user info, user '%s', pass '%s'", uip->user ? uip->user : "NULL",
            uip->pass ? "NON-NULL" : "NULL");
}
