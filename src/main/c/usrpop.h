#ifndef __USRPOP_H__
#define __USRPOP_H__

#include "common.h"

/* Returns 0 if valid, 1 if not, 3 if unable to connect to pop host */
int FindUserPop(char *popHost,
                char *user,
                char *pass,
                struct sockaddr_storage *pInterface,
                char useSsl,
                BOOL debug,
                BOOL noAPOP);

#endif  // __USRPOP_H__
