#!/bin/sh

set -xe

if [ "${TARGETDIR}" == "" ]; then
  TARGETDIRDEFAULT="`realpath ../../target`"
  if [ -d "${TARGETDIRDEFAULT}" ]; then
    export TARGETDIR="${TARGETDIRDEFAULT}"
  else
    echo TARGETDIR must be defined
    exit 1
  fi
fi

# LSB 5.0 is incompatible with RHEL; force use of 4.1
export LSBCC_LSBVERSION="4.1"

SRCDIR=$(dirname $(readlink -f "$0"))
BUILDDIR=${TARGETDIR}/build
mkdir -p ${BUILDDIR}
cd ${BUILDDIR}
# Copy special Makefile to allow running make from directly
# in build directory
cp ../../../src/main/c/Makefile.build Makefile

export RELSTR=beta_
export ISBETA=1
make -f ${SRCDIR}/Makefile dist

if [ "$1" == "all" ]; then
  make -f ${SRCDIR}/Makefile reset
  export RELSTR=rel_
  export ISBETA=0
  make -f ${SRCDIR}/Makefile dist
  make -f ${SRCDIR}/Makefile reset
fi
