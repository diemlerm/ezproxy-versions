# EZproxy

This project is used to build EZproxy executables.
It is structured as a multi-module project with each
module building the libraries required for a specific 
operating system and hardware architecture.  The supported operating
systems are Linux and Windows and the support hardware architecture
is x86-64.

Although EZproxy is written in C, the build process
is controlled by maven to simplify integration with Ubiquity
and Archiva.
